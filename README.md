# Ilmas backend

modello dati ilmas

## LESS

per compilare i file less in css 
lessc client/less/app.less > client/css/app.css

## INSTALLAZIONE

- clonare il repo (git clone https://amedeobuco@bitbucket.org/plurimedia/ilmas_admin.git)
- npm install
export NODE_OPTIONS="--max-old-space-size=8192"
-///x evitare errore memoria lanciarlo cosi in dev: 
export AMBIENTE=dev;NODE_OPTIONS="--max-old-space-size=8192"; nodemon app
- per WINDOWS lanciare solo nodemon app
- l'app gira su http://localhost:3000/

Per la prima installazione:
0. verifica di avere node 18.8.0
1. fai pull del branch
2. cancella node_modules e client/lib
3. npm i
4. cd client/lib/chart.js/
5. npm install
6. npm install -g gulp-cli
7. gulp build
8. nodemon app

Altrimenti:
0. verifica di avere node 18.8.0
1. fai pull del branch
2. nodemon app

## AMBIENTE DI TEST

1) creare una nuova app (una tantum)
heroku create --remote staging

2) scalare i dyno per utilizzarla
heroku ps:scale web=1 --remote staging

3) verificare .git/config
[remote "staging"]
	url = https://git.heroku.com/ilmas-admin-test.git
	fetch = +refs/heads/*:refs/remotes/staging/*

4) pushare il branch da testare
git push staging <nome-branch>:master

5) definire nella nuova app le var di ambiente (una tantum)
6) abilitare il nuovo url in auth0 (Ilmas admin test - una tantum)
7) al termine del test scalare i dyno così non ha costo
heroku ps:scale web=0 --remote staging


## AMBIENTE DI PRODUZIONE

deploy:git push heroku-prod master
Connessione DB: mongodb://plurimedia:Canonico27!@ds035046.mlab.com:35046/heroku_djbt36vv
 

## Lanciare uno script in ambiente di produzione

modificare package.json aggiundendo lo script che si desidera lanciare in produzione;
es:
 "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1",
    "postinstall": "bower install && node scripts/modificaprodotti_alimentatore"
  }
  committare il file in produzione
  lanciare il deploy
  ri-committare il file senza la modifica


## RUOLI

vengono impostati in auth0 in app_metadata come array
un utente può avere uno di questi ruoli

### operatore - adminProdotti - agente - genericUser

### di seguito i permessi
_____________________________________________________________
					|			    |		          PERMESSI	   	
	  RUOLO    		    | MODLULO 	   | LETTURA    | SCRITTURA
____________________|______________|____________|____________

### admin  			    | TUTTI		     |     V      |     X			---> admin@ilmas.com/pluri9900


### operatore  		  | Etichette    |     V      |     X			---> operatore@ilmas.com/operatore
###					        | DwhEtichette |     X      |     X


### adminProdotti  	| Componenti   |     V      |     V 		---> prodotti@ilmas.com/pluri9900
###  			  	      | Prodotti_all |     V      |     V
###  			  	      | Model/Categ  |     V      |     V
### 			  	      | Linee Comm   |     V      |     V
### 			  	      | Contatti	   |     V      |     V
### 			  	      | Offerte	     |     V      |     V
###					        | Etichette    |     V      |     X
###					        | DwhEtichette |     V      |     X
###					        | MailingList  |     V      |     X

### agente		  	  | Contatti	   |     V      |     V (solo i propri) ---> agente@ilmas.com/pluri9900
### 			  	      | Offerte	     |     V      |     V (solo i propri)


### commerciale  	  | Componenti   |     X      |     V   --->  commerciale@ilmas.com/pluri9900
###      		  	    | Prodotti_all |     X      |     X
### 			  	      | Model/Categ  |     X      |     X
### 			  	      | Linee Comm   |     X      |     X
### 			  	      | Contatti	   |     V      |     V  
### 			  	      | Offerte	     |     V      |     V
###					        | MailingList  |     V      |     V


### genericUser  	  | Componenti   |     V      |     X	--->  user@ilmas.com/pluri9900
### 			  	      | Prodotti 	   |     V      |     X
### 			  	      | Model/Categ  |     V      |     X
### 			  	      | Linee Comm   |     V      |     X
### 			  	      | Contatti	   |     V      |     V
### 			  	      | Offerte	     |     V      |     V
###					        | Etichette    |     V      |     X
###					        | DwhEtichette |     X      |     X
###					        | MailingList  |     V      |     X



## SCRIPT AGGIORNAMENTO PREZZI

node scripts/listino_prezzi/boot.js


## script per aggiornamento catalogo prodotti

lanciarli con script vscode

{
  "type": "node",
  "request": "launch",
  "name": "ilmas script",
  "program": "${workspaceFolder}/scripts/aggiornamento_codici_IN44_2022/2242_Smart_Track_M2022.js",
  "skipFiles": [
    "<node_internals>/**"
  ],
  "env": {
      "AMBIENTE": "test",
      "PATH": "/Users/damon/.nvm/versions/node/v14.18.1/bin"
  }
}

AMBIENTE test o production
