var fs = require('fs'),
    csv = require("fast-csv"),
    _ = require('underscore'),
    async = require('async'),
    mongoose = require('mongoose'),
    Prodotto = mongoose.model('Prodotto'),
    Categoria = mongoose.model('Categoria'),
    Modello = mongoose.model('Modello'),
    capitalize = function (string) {
        return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
    },
    capitalizeWords = function (string) {
        return string.replace(/\w\S*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    },
    _categoriaIlmas = function (data) {
        var cat = {
            codiceprodotto: data['Cod.'],
            uso: 'indoor',
            linea: 'ilmas'
        }; // mi segno codice prodotto per il successivo popolamento
        if (data.Categoria == 'INCASSI CARDAN')
            cat.nome = 'Incassi cardanici'
        if (data.Categoria == 'PROFILE')
            cat.nome = 'Profili'
        else
            cat.nome = capitalize(data.Categoria).trim();
        return cat;
    },
    _categoriaToshiba = function (data) {
        var cat = {
            codiceprodotto: data['Codice prodotto'],
            uso: 'indoor',
            linea: 'toshiba'
        };
        if (data.Modello === 'Led Panel')
            cat.nome = 'Apparecchio';
        else
            cat.nome = 'Lampada Led';

        return cat;
    },
    _modelloIlmas = function (data) {
        var mod = {
            categoria: _categoriaIlmas(data).nome,
            nome: capitalizeWords(data.Descrizione),
            codiceprodotto: data['Cod.'],
            uso: 'indoor',
            linea: 'ilmas'
        };
        return mod;
    },
    _modelloToshiba = function (data) {
        var mod = {
            codiceprodotto: data['Codice prodotto'],
            nome: capitalizeWords(data.Modello),
            categoria: _categoriaToshiba(data).nome,
            uso: 'indoor',
            linea: 'toshiba'
        };
        return mod;
    },
    _normalizzaIlmas = function (data) { // normalizza i dati per un import da excel ilmas
        _K = function (codice) {
                if (codice.indexOf('501') !== -1) // tradizionali
                    return '';
                else
                    return data.K;
            },
            _LM = function (codice) {
                if (codice.indexOf('501') !== -1) // tradizionali
                    return '';
                else
                    return data.lm;
            },
            _W = function (watt) {
                return watt.replace('w', '');
            },
            _fascio = function (fascio) {
                return fascio.replace('°', '');
            },
            _lampadina = function (codice) {
                if (codice.indexOf('501') !== -1) // tradizionali
                    return data.lm;
                else
                    return '';
            },
            _attacco = function (codice) {
                if (codice.indexOf('501') !== -1) // tradizionali
                    return data.K;
                else
                    return '';
            },
            _alimentatore = function (a) {
                if (a === '+AL STD')
                    return 'Driver standard';
                if (a === '+AL DIM')
                    return 'Driver dimmerabile';
                if (a === '+AL DALI')
                    return 'Driver dali';
                else
                    return a;
            }
        prodotto = {
            // categoria e modello li metto dopo perchè sono refs
            mdate: new Date(),
            pubblicato: true,
            codice: data['Cod.'],
            prezzo: parseFloat(data['€'].replace(',', '.')),
            linea: 'ilmas',
            destinazione: data.Utilizzo || 'indoor',
            tradizionale: data['Cod.'].indexOf('501') === 0 ? true : false,
            sottocategoria: capitalize(data.Tipologia),
            lampadina: _lampadina(data['Cod.']),
            attacco: _attacco(data['Cod.']),
            k: _K(data['Cod.']),
            lm: _LM(data['Cod.']),
            w: _W(data.W),
            ma: data.mA,
            v: data.Tensione,
            hz: data.Frequenza,
            fascio: _fascio(data.Fascio),
            irc: data['IRC/CRI'],
            satinato: data.SATINATO !== '' ? true : false,
            generazione: data.Generazione,
            macadam: data.Macadam,
            colore: capitalize(data.Colore),
            verniciatura: data.Verniciatura,
            moduloled: capitalizeWords(data['LED MODULE']),
            les: data.LES,
            tmax: data['Ta (temperatura ambiente massima)'].replace('°', ''),
            alimentatore: _alimentatore(data.ALIMENTATORE),
            alimentatore_incluso: data['Alimentatore incluso'] === 'INCLUSO' ? true : false,
            descrizione_applicazione: data['Descrizione applicazione'],
            orientabile: data.Orientabile,
            inclinabile: data.Inclinabile,
            corpo_it: capitalize(data['Caretteristiche di costruzione del CORPO']),
            corpo_en: capitalize(data['Caretteristiche di costruzione del CORPO EN']),
            ottica_it: capitalize(data['Caratteristiche costruzione ottica']),
            ottica_en: capitalize(data['Caratteristiche costruzione ottica EN']),
            adattatore_it: capitalize(data.Adattatore),
            adattatore_en: capitalize(data['Adattatore EN']),
            ce: data.CE === 'SI' ? true : false,
            f: data.F === 'SI' ? true : false,
            rischio_fotobiologico: data['Rischio fotobiologico'],
            classe_isolamento: data['Classe di isolamento'],
            ip: data['Grado IP'],
            foro: data.Foro,
            divieto_controsoffitti: data['divieto di installazione in controsoffitti termicamente isolati (non coprire)'] === 'Si' ? true : false,
            filo_incandescente: data['Filo incandescente'],
            keywords: _.flatten([
                                      _.map(data.Descrizione.split(' '), function (k) {
                    return k.toLowerCase()
                })
                                      , _K(data['Cod.']) + 'k',
                                      _LM(data['Cod.']) + 'lm',
                                      _W(data.W) + 'w', data['Cod.'],
                                      data.Colore.toLowerCase().split(' ')])
        };

        _.omit(prodotto, function (o) {
                return o === ""
            }) // tolgo tutte le proprietà vuote
        return prodotto;
    },
    _normalizzaToshiba = function (data) { // normalizza i dati per un import da excel toshiba
        prodotto = {
            // categoria e modello li metto dopo perchè sono refs
            mdate: new Date(),
            codice: data['Codice prodotto'],
            pubblicato: true,
            prezzo: parseFloat(data['Prezzo'].trim().replace(',', '.')),
            linea: 'toshiba',
            destinazione: 'indoor',
            k: data['Temperatura di colore'].replace('K', ''),
            lm: data['Flusso luminoso'].replace('lm', '').trim(),
            w: data['Consumo Energetico'].replace('W', ''),
            irc: data['Indice Colore'],
            dimmerabile: data['Dimmerabile'] === 'No' ? 0 : 1,
            classe_energetica: data['Classe energetica'],
            durata: data['Durata'].replace('h', '').trim(),
            keywords: _.flatten([
                                        _.map(data.Modello.split(' '), function (k) {
                    return k.toLowerCase()
                }),
                                        data['Temperatura di colore'].replace('K', 'k'),
                                        data['Flusso luminoso'].replace('lm', '').trim() + 'lm',
                                        data['Consumo Energetico'].replace('W', '') + 'w',
                                        data['Codice prodotto']
                                    ])

            //Over Led Ps Mini 1240 3000K 1500lm 1x17w Nero
        };

        if (data['Imballo'])
            prodotto.qtamin = parseInt(data['Imballo'])

        _.omit(prodotto, function (o) {
                return o === ""
            }) // tolgo tutte le proprietà vuote

        return prodotto;
    },
    _parseProdottiCsv = function (csvpath, tipo, res) {

        var prodotti = [],
            categorie = [],
            modelli = [],
            appoggio = {},
            stream = fs.createReadStream(csvpath);

        // dopodichè parso e salvo
        csv.fromStream(stream, {
                headers: true,
                delimiter: ',',
                discardUnmappedColumns: true
            })
            .validate(function (data) {
                if (tipo === 'ilmas') {
                    if (data['Cod.'] !== '')
                        return true;
                } else
                    return true;
            })
            .on("data", function (data) {
                if (tipo === 'ilmas') {


                    if (appoggio[data['Cod.']] === undefined) {
                        prodotti.push(_normalizzaIlmas(data));
                        appoggio[data['Cod.']] = true;
                    } else {
                        var p = _normalizzaIlmas(data);
                        p.codice = p.codice + '-DOPPIO';
                        prodotti.push(p);
                        // console.log('doppio',p.codice)
                    }

                    if (data.Categoria !== '')
                        categorie.push(_categoriaIlmas(data));

                    if (data.Descrizione !== '')
                        modelli.push(_modelloIlmas(data))
                }
                if (tipo === 'toshiba') {
                    prodotti.push(_normalizzaToshiba(data));
                    categorie.push(_categoriaToshiba(data));
                    modelli.push(_modelloToshiba(data))
                }

            })
            .on("end", function () {
                fs.unlinkSync(csvpath); // cancello il file
                // prima cancello tutti i prodotti di quell'import (quelli pubblicati, gli altri i lascio)
                console.log('inizio import di ' + prodotti.length + ' prodotti');
                var categorieDatabase, // inizializzo una variabile per memorizzare le categorie del db
                    modelliDatabase;

                Prodotto.remove({
                    linea: tipo,
                    pubblicato: true
                }, function (err) {
                    console.log('1) rimossi i prodotti della linea ' + tipo);
                    Modello.remove({
                        linea: tipo
                    }, function (err) {
                        console.log('2) rimossi i modelli della linea ' + tipo);
                        Categoria.remove({
                            linea: tipo
                        }, function (err) {
                            console.log('3) rimosse le categorie della linea ' + tipo);
                            // preparo il bulk di inserimento dei prodotti
                            var bulk = Prodotto.collection.initializeOrderedBulkOp();
                            async.each(prodotti, function (p, cb) {
                                bulk.insert(p);
                                cb();
                            }, function (err) {
                                console.log('4) pronto il bulk dei prodotti, inserisco');
                                bulk.execute(function (err, resp) {
                                    console.log('5) inseriti ' + resp.nInserted + ' prodotti');
                                    // preparo le categorie da inserire
                                    var cat = _.uniq(_.map(categorie, function (c) {
                                            var t = _.clone(c);
                                            delete t.codiceprodotto;
                                            return t;
                                        }), function (c) {
                                            return c.nome;
                                        }) // tolgo il codice prodotto prima di inserire;
                                    var bulkCategorie = Categoria.collection.initializeOrderedBulkOp();

                                    async.each(cat, function (c, cb) {
                                        bulkCategorie.insert(c);
                                        cb();
                                    }, function (err) {
                                        console.log('6) pronto il bulk delle categorie, inserisco');
                                        bulkCategorie.execute(function (err, resp) {
                                            console.log('7) inserite ' + resp.nInserted + ' categorie');
                                            var mod = _.uniq(_.map(modelli, function (m) {
                                                    var t = _.clone(m);
                                                    delete t.categoria;
                                                    delete t.codiceprodotto;
                                                    return t;
                                                }), function (m) {
                                                    return m.nome;
                                                }) // tolgo la categoria e il prodotto prima
                                            var bulkModelli = Modello.collection.initializeOrderedBulkOp();
                                            async.each(mod, function (m, cb) {
                                                bulkModelli.insert(m);
                                                cb();
                                            }, function (err) {
                                                console.log('8) pronto il bulk dei modelli, inserisco');
                                                bulkModelli.execute(function (err, resp) {
                                                    console.log('9) inseriti ' + resp.nInserted + ' modelli');

                                                    // associo l'id categoria ai prodotti già inseriti    
                                                    Categoria.find({}, function (err, catdb) {
                                                        categorieDatabase = catdb; // le tengo qui
                                                        // metto nella colelction delle categorie l'id;
                                                        _.map(categorie, function (c) {
                                                            c.idcategoria = _.find(catdb, function (cdb) {
                                                                return cdb.nome === c.nome
                                                            })._id;
                                                        }); // ora ho l'id

                                                        var bulkProdottiCategorie = Prodotto.collection.initializeOrderedBulkOp();
                                                        async.each(categorie, function (c, cb) {
                                                            bulkProdottiCategorie.find({
                                                                $or: [{
                                                                    codice: c.codiceprodotto
                                                                }, {
                                                                    codice: c.codiceprodotto + '-DOPPIO'
                                                                }]
                                                            }).update({
                                                                $set: {
                                                                    categoria: c.idcategoria
                                                                }
                                                            });
                                                            cb();
                                                        }, function (err) {
                                                            console.log('10) pronto il bulk per associazione idcategoria a prodotti');
                                                            bulkProdottiCategorie.execute(function (err, resp) {
                                                                console.log('11) aggiornate le categorie per ' + resp.nModified + ' prodotti');
                                                                // stessa cosa per i modelli
                                                                Modello.find({}, function (err, modellidb) {
                                                                    modelliDatabase = modellidb; // memorizzo;
                                                                    _.map(modelli, function (m) {
                                                                        var modellodb = _.find(modellidb, function (mdb) {
                                                                                return mdb.nome === m.nome
                                                                            }),
                                                                            categoriadb = _.find(categorieDatabase, function (cdb) {
                                                                                return cdb.nome === m.categoria
                                                                            });

                                                                        if (modellodb)
                                                                            m.idmodello = modellodb._id;
                                                                        else
                                                                            console.log('modello non trovato:' + m.nome)

                                                                        if (categoriadb)
                                                                            m.idcategoria = categoriadb._id;
                                                                        else
                                                                            console.log('categoria modello non trovata:' + m.categoria)

                                                                    }); // ora ho l'id posso fare l'update dei modelli sui prodotti e delle categoie sui modelli
                                                                    var bulkProdottiModelli = Prodotto.collection.initializeOrderedBulkOp();
                                                                    async.each(modelli, function (m, cb) {
                                                                        bulkProdottiModelli.find({
                                                                            $or: [{
                                                                                codice: m.codiceprodotto
                                                                            }, {
                                                                                codice: m.codiceprodotto + '-DOPPIO'
                                                                            }]
                                                                        }).update({
                                                                            $set: {
                                                                                modello: m.idmodello
                                                                            }
                                                                        });
                                                                        cb();
                                                                    }, function (err) {
                                                                        console.log('12) pronto il bulk per associazione idmodello a prodotti');
                                                                        bulkProdottiModelli.execute(function (err, resp) {
                                                                            console.log('13) aggiornati i modelli per ' + resp.nModified + ' prodotti');
                                                                            var bulkModelliCategorie = Modello.collection.initializeOrderedBulkOp();
                                                                            async.each(modelliDatabase, function (mdb, cb) {

                                                                                var idCategoria = _.find(modelli, function (m) {
                                                                                    return m.linea === tipo && m.nome === mdb.nome
                                                                                });
                                                                                if (idCategoria) {
                                                                                    idCategoria = idCategoria.idcategoria;
                                                                                    bulkModelliCategorie.find({
                                                                                        nome: mdb.nome
                                                                                    }).update({
                                                                                        $set: {
                                                                                            categoria: idCategoria
                                                                                        }
                                                                                    });
                                                                                    cb();
                                                                                } else
                                                                                    cb();

                                                                            }, function (err) {
                                                                                console.log('14) pronto il bulk per associazione idcategoria a modello');
                                                                                bulkModelliCategorie.execute(function (err, resp) {
                                                                                    console.log('15) aggiornati le categorie  per ' + resp.nModified + ' modelli');
                                                                                    console.log('finito')
                                                                                    res.sendStatus(200);
                                                                                });
                                                                            })
                                                                        })
                                                                    })
                                                                })
                                                            })
                                                        })
                                                    })
                                                })
                                            })
                                        })
                                    })
                                })
                            })
                        })
                    })
                })
            })
    };

exports.import = function (req, res) {
    /* riceve un file csv dei prodotti, con parametro = linea (ilmas toshiba ecc)  e lo parsa*/
    var filePath = req.files.file.path;
    _parseProdottiCsv(filePath, req.params.tipo, res)
}