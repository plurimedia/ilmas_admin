var mongoose = require('mongoose'),
     DwhEtichetta = mongoose.model('DwhEtichetta'),
      _ = require('underscore');
 

exports.etichetteGrigie = function (req, res) 
{
    var annoReport = req.params.anno;
    DwhEtichetta.find(
        {tipo:'etichetta_grigia', anno:annoReport}
    ) 
    .sort({mdate: -1})
    .exec(function (err, etichette)
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(etichette);
    });
}

exports.etichetteBianche = function (req, res) 
{
    var annoReport = req.params.anno;

    DwhEtichetta.find({tipo:'etichetta_bianca', anno:annoReport}) 
    .sort({mdate: -1})
    .exec(function (err, etichette)
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(etichette);
    });
}

exports.etichetteEstro = function (req, res) 
{
    var annoReport = req.params.anno;

    DwhEtichetta.find({tipo:'etichetta_estro', anno:annoReport}) 
    .sort({mdate: -1})
    .exec(function (err, etichette)
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(etichette);
    });
}

exports.save = function (req, res) 
{
    var data = req.body;
        dwhEtichetta = new DwhEtichetta(data);
        dwhEtichetta.mdate = new Date();
        dwhEtichetta.anno = dwhEtichetta.mdate.getFullYear()
        dwhEtichetta.mese = dwhEtichetta.mdate.getMonth()+1
        dwhEtichetta.giorno = dwhEtichetta.mdate.getDate()
    dwhEtichetta.save(function (err, result) 
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(result);
    });
}

exports.mesi = function (req, res) 
{
    var annoReport = req.params.anno;
    DwhEtichetta.aggregate( 
        [ 
            {
                $match: {
                    anno: annoReport
                }
            },
            {$group: {
            _id: {
                "mese": "$mese" 
            } 
        }}],
       function (err, result) 
       {
            if (err)
                res.status(500).send(err);
            else
            {
                var mesi =[];
                result.forEach
                ( 
                    function (res)
                    { 
                        mesi.push(parseFloat(res._id.mese))
                    }
                )
                mesi = _.sortBy(mesi, function(o) { return o }) 
                res.status(200).send(mesi);
            }
        }
    )
}

exports.etichetteGrigieInMesi = function (req, res) 
{
    var annoReport = req.params.anno;
    DwhEtichetta.aggregate( 
        [ 
            {
                $match: {
                    tipo:'etichetta_grigia',
                    anno: annoReport
                }
            },
            {
                $group: {
                    _id: {
                        "mese": "$mese" ,
                    },
                    count:{$sum:1}
                }
            }
        ],
       function (err, result) 
       {
            if (err)
                res.status(500).send(err);
            else
            {
                result = _.sortBy(result, function(o) { return o._id.mese }) 
                var value =[];
                 result.forEach
                ( 
                    function (res)
                    { 
                         value.push(res.count)
                    }
                )
                res.status(200).send(value);
            }
        }
    )
}

exports.etichetteBiancheInMesi = function (req, res) 
{
    var annoReport = req.params.anno;
    DwhEtichetta.aggregate( 
        [ 
            {
                $match: {
                    tipo:'etichetta_bianca',
                    anno: annoReport
                }
            },
            {
                $group: {
                    _id: {
                        "mese": "$mese" ,
                        
                    },
                    count:{$sum:1}
                }
            }
        ],
       function (err, result)
       {
            if (err)
                res.status(500).send(err);
            else
            {
                result = _.sortBy(result, 
                    function(o) { return o._id.mese }) 
                var value =[];
                result.forEach
                ( 
                    function (res)
                    { 
                         value.push(res.count)
                    }
                )
                res.status(200).send(value);
            }
        }
    )
}

exports.etichetteEstroInMesi = function (req, res) 
{
    var annoReport = req.params.anno;
    DwhEtichetta.aggregate( 
        [ 
            {
                $match: {
                    tipo:'etichetta_estro',
                    anno: annoReport
                }
            },
            {
                $group: {
                    _id: {
                        "mese": "$mese" ,
                        
                    },
                    count:{$sum:1}
                }
            }
        ],
       function (err, result)
       {
            if (err)
                res.status(500).send(err);
            else
            {
                result = _.sortBy(result, 
                    function(o) { return o._id.mese }) 
                var value =[];

                //questa soluzione potrebbe essere applicata alle altre etichette
                for(let i=0; i<12; i++){
                    value.push(0);
                }

                result.forEach
                ( 
                    function (res)
                    { 
                        let index = parseInt(res._id.mese) - 1;
                        value[index] = res.count;
                    }
                )
                res.status(200).send(value);
            }
        }
    )
}

exports.countFn = () => {
    return new Promise((resolve, reject) => {
        var date = new Date(),
            anno = date.getFullYear(),
            mese = date.getMonth()+1;

        var result =
        {
            countEtichette  : 0,
            countBianche    : 0,
            countGrigie     : 0
        }

        DwhEtichetta.count({anno:anno,mese:mese}) 
        .exec(function (err, countEtichette)
        {
            if (err)
                reject(err);
            else
            {
                result.countEtichette = countEtichette;
                DwhEtichetta.count({anno:anno,mese:mese,tipo:'etichetta_grigia'}) 
                .exec(function (err, countGrigie)
                {
                    if (err)
                        reject(err);
                    else
                    {
                        result.countGrigie = countGrigie;
                        DwhEtichetta.count({anno:anno,mese:mese,tipo:'etichetta_bianca'}) 
                        .exec(function (err, countBianche)
                        {
                            if (err)
                                reject(err);
                            else
                            {
                                result.countBianche = countBianche;
                                resolve(result);
                            }
                        }) 
                    }
                }) 
            }
        });

    })
}

exports.count = function (req, res) {
    exports.countFn().then((result) => {
        res.status(200).send(result);
      }).catch((err) => {
        res.status(500).send(err);
      })
}