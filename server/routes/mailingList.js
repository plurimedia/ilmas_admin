var mongoose = require('mongoose'),
	MailingList = mongoose.model('MailingList'),
	Contatto = mongoose.model('Contatto'),
_ = require('underscore')

/* ultime mail */
exports.last = function (req, res) 
{
	var query = {}; 
	 
	MailingList.find(query, function (err, result) {
		if (err)
			res.status(500).send(err);
		else{

			res.status(200).send(result);
		}
	}).limit(10).sort({mdate: -1})
}

exports.save = function (req, res)
{
 
	var data = req.body,
	    mail = new MailingList(data);
		mail.idate = new Date();  

 		if  (mail.nazioni.length>0)
 		{
 			var queryNazioni = {$or:[]};

			mail.nazioni.forEach
			( 
                function(n)
                {
                     queryNazioni.$or.push( { nazione : n} )
                }
	        )
 		}

 		if  (mail.province.length>0)
 		{
 			var queryProvince = {$or:[]};

			mail.province.forEach
			( 
                function(p)
                {
                     queryProvince.$or.push( { provincia : p} ) 
                }
	        )
		}
		 
		var queryContatti = {$and : []};
		var fieldContatti = {email:1};

		if (queryNazioni)
			queryContatti.$and.push( queryNazioni );

		if (queryProvince)
			queryContatti.$and.push( queryProvince );

		var invioMassivo = true;
		if (!queryNazioni && !queryProvince){
			queryContatti = {};
			invioMassivo=false;
		}



		//SE la mail è stata inviata con degli indirizzi "MASSIVI"(stato, prov)
		//salvo in DB tutti gli indirizzi relativi a quell'invio
		//altrimenti salvo la mail senza popolare il campo invioMassivo
		if (invioMassivo==true){
	        Contatto.find(
				queryContatti,fieldContatti,
				function (err, result) {
					if (err)
						res.status(500).send(err);
					else
					{ 
						async.each(result,
	       					function(res, callback)
	       					{
	       						mail.a_inviomassivo.push(res.email)
	       						callback();
	       					},
					        function(err)
					        {
					            if(err)
					            {
					                console.log(err); 
					                res.status(500).send(err);
					            }
					            else
					            {
					                mail.a_inviomassivo = _.uniq(mail.a_inviomassivo);

									mail.save(
										function (err, result) 
										{
											if (err){
									 			res.status(500).send(err); 
											}
											else { 
				 								res.status(200).send(mail);
											}
										}
									) 
					            }
					        }
	       				) 
					}
				}
			)
		}
		else{
			mail.save(
				function (err, result) 
				{
					if (err){
			 			res.status(500).send(err); 
					}
					else { 
						res.status(200).send(mail);
					}
				}
			) 
		}
	
}

exports.update = function(req, res)
{
	var modify = req.body;
		modify.mdate = new Date(); 
 		modify.m_user_id = req.user.sub;  
 
	MailingList.findById(req.params.id,  
		function (err, mail) { 

			mail.update(modify, {
				runValidators: true
			},
			function (err) {
				if (err) { 
					res.status(500).send(err);
				} else {
					res.status(200).send(modify);
				}
			})
		}
	)
}

exports.delete = function(req, res) {
	MailingList.remove({
		_id: req.params.id
	}, function (err) {
		if (err) {
			res.status(500).send(err);
		} else {
			res.status(200).end();
		}
	});
};
 
exports.search = function (req, res) 
{
	var search = req.query.search,
		query = {
				$or:[
					{ to:{ $all: search} },
				  	{ cc:{ $all: search} },
				  	{ ccn:{ $all: search} },
				  	{ oggetto: {$regex: search}}
				]
			};  

	MailingList.find(query, function (err, results) {
		if (err)
			res.status(500).send(err);
		else
			res.status(200).send(results);
	})
}

exports.get = function (req, res) {
    MailingList.findById(req.params.id, 
    function (err, mail) {
        if(err)
            res.status(500).send(err);
        
        res.status(200).send(mail)
    })
}