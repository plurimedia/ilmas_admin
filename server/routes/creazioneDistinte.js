var mongoose = require('mongoose'),
	CreazioneDistinte = mongoose.model('CreazioneDistinte');

exports.last = function (req, res) {
	CreazioneDistinte.find({})
		.limit(1)
		.sort({dt_inizio: -1})
		.exec(function (err, cd)
		{
			if (err)
				res.status(500).send(err);
			else
				res.status(200).send(cd[0]);
		});

}

// non è una route ma lascio qui tutte le operazioni sulla collection
exports.create = function (cb) {
	var cd = new CreazioneDistinte({dt_inizio:new Date(), esito:null, info: {} });
	console.log('riga appena creata', cd);
	cd.save(function (err, result) {
		console.log('riga appena salvata su db', cd);

		if (err)
			throw err;
		else
			cb(cd);
	});
}

exports.close = function (id, esito) {
	CreazioneDistinte.findById(id, function (err, cd) {
        cd.update({dt_fine:new Date(), esito: esito},{},
            function (err) {
				console.log('esito updatato');
			});
		});
}

