var mongoose = require('mongoose'),
	Comune = mongoose.model('Comune');

exports.search = function (req, res) 
{
	Comune.find(
		{ $or:[
				{nome:{$regex: req.query.nome, $options: 'i'}}, 
				{nome:req.query.nome}
		]}, 
		function (err, result) 
		{
			if (err)
				res.status(500).send(err);
			else{
				console.log(result)
				res.status(200).send(result);
			}
		}
	);
}