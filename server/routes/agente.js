var mongoose = require('mongoose'),
Agente = mongoose.model('Agente');

exports.last = function (req, res) 
{
    Agente.find({}, 
        function (err, results) 
        {
            if(err)
                res.status(500).send(err);

            res.status(200).send(results)
        }
    ).sort({mdate:-1})
}

exports.get = function (req, res) 
{
    Agente.findById(req.params.id, 
        function (err, agente) 
        {
            if(err)
                res.status(500).send(err);
        res.status(200).send(agente)
    })
}

exports.save = function (req, res) 
{
    var data = req.body;
    var agente = new Agente(data);
        agente.mdate = new Date(),
        agente.last_user = req.user;
        agente.searchText = agente.nome + " " + agente.cognome
    agente.save(function (err, result) 
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(result);
    });
}

exports.update = function(req, res) 
{
    var modify = req.body;
    modify.mdate = new Date(),
    modify.last_user = req.user;
    Agente.findById(req.params.id, 
        function (err, agente) 
        {
            agente.update(modify, 
            {
                runValidators: true
            }, 
            function (err) 
            {
                if (err)
                    res.status(500).send(err);
                else
                    res.status(200).send(modify);
            })
        }
    );
};

exports.delete = function(req, res)
{
    Agente.remove({
        _id: req.params.id
    }, 
    function (err) 
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).end();
    });
};
