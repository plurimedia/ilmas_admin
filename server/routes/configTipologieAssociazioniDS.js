var mongoose = require('mongoose'),
ConfigTipologieAssociazioniDS = mongoose.model('ConfigTipologieAssociazioniDS')


exports.get = function (req, res) 
{
    ConfigTipologieAssociazioniDS.findById(req.params.id, 
        function (err, result) 
        {
            if(err)
                res.status(500).send(err);
        res.status(200).send(result)
    })
}

exports.list = function (req, res) 
{
    ConfigTipologieAssociazioniDS.find({}, 
        function (err, results) 
        {
            if(err)
                res.status(500).send(err);
            res.status(200).send(results)
        }
    )
    .sort({tipo:1})

}
 
 
exports.save = function (req, res) 
{


    var data = req.body;
    var configTipologieAssociazioniDS = new ConfigTipologieAssociazioniDS(data);
    
    console.log(configTipologieAssociazioniDS);

    configTipologieAssociazioniDS.mdate = new Date(),
    configTipologieAssociazioniDS.last_user = req.user,
 
    configTipologieAssociazioniDS.save(function (err, result) 
    {
        if (err)
            res.status(500).send(err);
        else{ 
            res.status(200).send(result);
        }
    });
}

exports.update = function(req, res) 
{
     
    var modify = req.body;
    modify.mdate = new Date(),
    modify.last_user = req.user;
      

    ConfigTipologieAssociazioniDS.findById(req.params.id, 
        function (err, configTipologieAssociazioniDS) 
        {
            configTipologieAssociazioniDS.update(modify, 
            {
                runValidators: true
            }, 
            function (err) 
            {
                if (err)
                    res.status(500).send(err);
                else{ 
                    res.status(200).send(modify);
                }
            })
        }
    );
};

exports.delete = function(req, res)
{
    console.log("eccomi ", req.params.id)
    ConfigTipologieAssociazioniDS.remove({
        _id: req.params.id
    }, 
    function (err) 
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).end();
    });
};
     