var mongoose = require('mongoose'),
    Componente = mongoose.model('Componente'), 
    _ = require('underscore'),
    async = require('async'),
    CONFIG = require(process.cwd()+'/server/config.js');
 
_codificaCategoria = function(componente){
    if (componente.categoria == 'attivita')
        return "A"
    else if (componente.categoria == 'componente')
        return "C" 
    else if (componente.categoria == 'prodotto')
        return "P" 
}

_codificaTipo = function(componente){
    if (componente.tipo == 'corpo')
        return "CO"
    else if (componente.tipo == 'led')
        return "LE"
    else if (componente.tipo == 'driver')
        return "DR"
    else if (componente.tipo == 'anello')
        return "AN"
    else if (componente.tipo == 'blocco_staffa')
        return "BL"
    else if (componente.tipo == 'box')
        return "BO"
    else if (componente.tipo == 'cornice')
        return "CN"
    else if (componente.tipo == 'lampada_led')
        return "LL"
    else if (componente.tipo == 'staffa_laterale')
        return "SL"   
    else if (componente.tipo == 'verniciatura')
        return "VE"
    else if (componente.tipo == 'cartone')
        return "CA"
    else if (componente.tipo == 'imballo')
        return "IM"
    else if (componente.tipo == 'manodopera')
        return "MA"
    else if (componente.tipo == 'sacchetto')
        return "SA"
    else if (componente.tipo == 'vetro')
        return "VT"
    else if (componente.tipo == 'vetro_sabbiato')
        return "VS"
    else if (componente.tipo == 'riflettore')
        return "RI"
    else if (componente.tipo == 'lente')
        return "LN"
    else if (componente.tipo == 'raccordo')
        return "RA"
    else if (componente.tipo == 'trattamento_galvanico')
        return "TG"
    else if (componente.tipo == 'prodotto')
        return "PR"
    else if (componente.tipo == 'blocca_lente')
        return "BE"
    else if (componente.tipo == 'anello_interno')
        return "AI"
    else if (componente.tipo == 'dissipatore')
        return "DS"
    else if (componente.tipo == 'distanziale')
        return "TI"
    else if (componente.tipo == 'nottolino')
        return "NO"  
    else if (componente.tipo == 'portasnodo')
        return "FO"
    else if (componente.tipo == 'snodo')
        return "SN"
    else if (componente.tipo == 'corpo_dissipatore')
        return "CS"    
    else if (componente.tipo == 'wtp')
        return "WT"    
    else if (componente.tipo == 'adattatore_global')
        return "AG"    
    else if (componente.tipo == 'adattatore')
        return "AD"   
    else if (componente.tipo == 'raccordo_filettato')
        return "RF"    
    else if (componente.tipo == 'corpo_led')
        return "CL"    
    else if (componente.tipo == 'corpo_led_driver')
        return "CD"  
    else if (componente.tipo == 'casetta')
        return "CE" 
    else if (componente.tipo == 'griglia_per_casetta')
        return "GC"  
    else if (componente.tipo == 'minuteria')
        return "MI"  
    else if (componente.tipo == 'raccordo_fresato')
        return "RR"  
    else if (componente.tipo == 'pulsante_push_memory')
        return "PM"  
    else if (componente.tipo == 'molla')
        return "MO"  
    else if (componente.tipo == 'cornice_per_cartongesso_da_rasare')
        return "CC"  
    else if (componente.tipo == 'cavo')
        return "CV"  
    else if (componente.tipo == 'pomolo')
        return "PO"  
    else if (componente.tipo == 'basetta')
        return "BA"  
    else if (componente.tipo == 'molla_per_aggancio')
        return "MG"
    else if (componente.tipo == 'staffa')
        return "ST"
    else if (componente.tipo == 'prolunga_staffa')
        return "PS"
    else if (componente.tipo == 'supporto_lampada')
        return "SM"
    else if (componente.tipo == 'policarbonato')
        return "PL"
    else if (componente.tipo == 'guaina')
        return "GU"
    else if (componente.tipo == 'griglia')
        return "GR"
    else if (componente.tipo == 'fermacavo')
        return "FE"
    else if (componente.tipo == 'rosone')
        return "RO"
    else if (componente.tipo == 'cavo_di_sospensione')
        return "CP"
    else if (componente.tipo == 'modulo_led')
        return "ML"
    else if (componente.tipo == 'disco')
        return "DI"

}


/* ultimi prodotti aggiornati */
exports.last = function (req, res) 
{
    Componente.find({}) 
    .limit(10)
    .sort({mdate: -1})
    .exec(function (err, componenti)
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(componenti);
    });
}

exports.save = function (req, res) 
{
    console.log("save")
    var data = req.body;
    var componente = new Componente(data);
        componente.mdate = new Date(),
        componente.last_user = req.user;

        Componente.count() 
        .exec(function (err, countComponenti)
        {
            if (err){ 
                res.status(500).send(err);
            }
            else
            { 
                countComponenti=countComponenti+1
                componente.codice_interno = 
                _codificaCategoria(componente) + 
                _codificaTipo(componente) + 
                ("000000"+countComponenti).substring(("000000"+countComponenti).length-5, ("000000"+countComponenti).length)

                componente.save(function (err, result) 
                {
                    if (err)
                        res.status(500).send(err);
                    else
                        res.status(200).send(result);
                });

            }
        })
}

exports.update = function(req, res) 
{
    console.log("update")
    var modify = req.body;
    modify.mdate = new Date(),
    modify.last_user = req.user;
    Componente.findById(req.params.id,
        function (err, componente) 
        {
            /*questa variabile mi serve per capire se è stato modificato il prezzo*/
            var modMassive=false
            if (modify.prezzo != modify.prezzo_old){
                modMassive = true;
            } 
            modify.prezzo_old = modify.prezzo;

            var  old_cod_interno = _.clone(modify.codice_interno)
 

            var  new_cod_interno = 
            _.clone(_codificaCategoria(modify) + 
            _codificaTipo(modify) + 
            old_cod_interno.substring(3).replace('ndefined','') // quando finito di effettuare l'update x tutti i componenti togliere questo replace
            );

          
 
            modify.codice_interno = new_cod_interno

            componente.update(
                modify,
                {runValidators: true},
                function (err) {
                    if (err) {
                        res.status(500).send(err);
                    } else {

                        /*dopo il salvataggio del componente 
                        controllo se è stato modificato il prezzo.
                        se si vado a modificare il prezzo di tutti i componenti padre che includono a loro
                        volta questo componente figlio
                        */

                        if (modMassive){
                            var componentiDaAggiornare=[];

                            /*cerco tutti o componenti che hanno figli*/
                            Componente.find( 
                               { componenti_figlio: { $exists: true, $ne: [] } }   
                            )
                           .populate(
                                {
                                    path: 'componenti_figlio._id',
                                    model: 'Componente'  
                                }
                            )
                            .exec(function (err, componenti)
                            {
                                if (err){
                                    console.log(err)
                                    res.status(500).send(err);
                                } else {

                                    /*per ogni figlio controllo se questo è il componente che ho modificato e se il prezzo non sia stato fissato*/
                                    componenti.forEach(function(elem){
                                         elem.componenti_figlio.forEach(function(figlio){
                                            figlio = _.clone(figlio._id)
                                            if (figlio && figlio._id == modify._id && !elem.prezzofix){
                                                componentiDaAggiornare.push(elem);
                                            }
                                        })
                                    })

                                    async.each(componentiDaAggiornare, 
                                        function(obj, cb)
                                        {
                                            componenteNuovoPrezzo = _.clone(obj) 
                                            var prezzoNuovo = 0;
                                            componenteNuovoPrezzo.componenti_figlio.forEach(function(figlio){
                                                prezzoNuovo = prezzoNuovo + (figlio.qt * figlio._id.prezzo)
                                            })
                                            set = {mdate:new Date(),prezzo:prezzoNuovo};
                                            Componente.findOneAndUpdate(
                                                {_id:componenteNuovoPrezzo._id},{$set:set},
                                                function(err, cat){
                                                    cb();
                                                }
                                            )
                                        },
                                        function(err)
                                        { 
                                            if(err)
                                                res.status(500).send(err);

                                            res.status(200).send(modify);
                                        }
                                    )
                                }
                            });

                        }
                        else
                            res.status(200).send(modify);
                       
                    }
                }
            )
        }
    )
}
 
exports.delete = function(req, res) 
{
    Componente.remove({
        _id: req.params.id
    }, 
    function (err) 
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).end();
    });
};


exports.searchcodice = function (req, res) 
{
    var search = req.query.nome,
        query = {codice: {$regex: search}}; 
    Componente.find(query,'codice',
        function (err, results) 
        {
            if (err)
                res.status(500).send(err);
            else
                res.status(200).send(results);
    }).sort({codice: 1})
}

/* cerca prodotti */
exports.search = function (req, res) 
{
    var query = {};

    var search = req.query.search.split(' '),
        query = {keywords:{ $all: search}};

    if (req.query.fornitore){
        query.fornitore = req.query.fornitore
    }

    Componente.find(query) 
    .populate(
        {
            path: 'componenti_figlio._id',
            model: 'Componente'  
        }
    )
    .limit(40)
    .sort({mdate: -1})
    .exec(function (err, results) 
    {
        if (err)
            res.status(500).send(err);
        else{
            
            res.status(200).send(results);
        }
    })
} 

exports.get = function (req, res) 
{
    Componente.findOne({_id:req.params.id}) 
    .populate(
        {
            path: 'componenti_figlio._id',
            model: 'Componente'  
        }
    )
    .exec(function (err, componente)
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(componente);
    });
} 

exports.countFn = () => {
    return new Promise((resolve, reject) => {
        var result =
        {
            countComponenti : 0,
            countCorpi      : 0,
            countLed        : 0,
            countDriver     : 0
        }
        Componente.count() 
        .exec(function (err, countComponenti)
        {
            if (err)
                reject(err);
            else
            {
                result.countComponenti = countComponenti;
                Componente.count({tipo:'corpo'}) 
                .exec(function (err, countCorpi)
                {
                    if (err)
                        reject(err);
                    else
                    {
                        result.countCorpi = countCorpi;
                        Componente.count({tipo:'led'}) 
                        .exec(function (err, countLed)
                        {
                            if (err)
                                reject(err);
                            else
                            {
                                result.countLed = countLed;
                                Componente.count({tipo:'driver'}) 
                                .exec(function (err, countDriver)
                                {
                                    if (err)
                                        reject(err);
                                    else
                                    {
                                        result.countDriver = countDriver;
                                        resolve(result);
                                    }
                                })
                            }
                        })
                    }
                })
            }
        })
    })
}

exports.count = function (req, res) 
{
    exports.countFn().then((result) => {
        res.status(200).send(result)
    }).catch((err) => {
        res.status(500).send(err)
    })
}
