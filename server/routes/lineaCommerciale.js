var mongoose = require('mongoose'),
    LineaCommerciale = mongoose.model('LineaCommerciale'),
    LineaCommercialeInfoProdotto = mongoose.model('LineaCommercialeInfoProdotto'),
    Modello = mongoose.model('Modello'),
    Prodotto = mongoose.model('Prodotto'),
    _ = require('underscore');


function uniq_fast(a) {
    var seen = {};
    var out = [];
    var len = a.length;
    var j = 0;
    for(var i = 0; i < len; i++) {
         var item = a[i];
         if(seen[item] !== 1) {
               seen[item] = 1;
               out[j++] = item;
         }
    }
    return out;
}


function trasformCollectToString(array){
    array = _.collect(array,
        function(l){
            try{
                return l.toString();
            }
            catch(e){}
        }
    ) 
    return array;
}

function filterArray(src, filt) {
    var temp = {}, i, result = [];
     for (i = 0; i < filt.length; i++) {
        temp[filt[i]] = true;
    } 
    for (i = 0; i < src.length; i++) {
        if (!(src[i] in temp)) {
            result.push(src[i]);
        }
    }
    return(result);
}

function is_numeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

/* cerca LineaCommerciale */
exports.get = function (req, res) {
    console.log("start LineaCommerciale exports.get")

    LineaCommerciale.findOne({_id:req.params.id},
     function (err, result) {
        if(err)
            res.status(500).send(err);
        
        console.log("end LineaCommerciale exports.get")
       
        res.status(200).send(result)
    })
    .populate('categorieAssociate', 'nome')
    .populate('modelliAssociati', 'nome') 
 }

/* cerca LineaCommerciale */
exports.cercaProdotto = function (req, res) {
    console.log("start LineaCommerciale cercaProdotto")
 
    var codiceProdotto = req.params.codiceProdotto;
        idLinea = req.params.idLinea; 
  
    LineaCommerciale.findOne(
        {_id:idLinea},
        function (err, lineaCommerciale) {
            if(err){
                res.status(500).send(err);
            }
            else
            {
                //cerco l'_id del prodotto che l'utente ha digitato a video
                Prodotto.findOne( {codice:codiceProdotto},
                function (err, prodotto) {
                    if(err){
                        res.status(500).send(err);
                    }
                    else
                    { 
                        var result = []; 

                        if (prodotto)
                        {
                            prodotto = prodotto.toJSON();

                            var idProdotto = prodotto._id

                            var prodottiAssociati = lineaCommerciale.prodottiAssociati;
     
                            var esisteAssociazioneInLineaCommerciale
                                 = _.find(prodottiAssociati, 
                                function(p){ 
                                     return p.toString() ===  idProdotto.toString(); 
                                });

                            /*
                            Nel caso in cui troviamo il codice prodotto
                            andiamo a cercare se questo codice ha delle informazioni aggiuntive su
                            lineaCommercialeInfo
                            */
                            if (esisteAssociazioneInLineaCommerciale){

                                LineaCommercialeInfoProdotto.findOne(
                                    {linea:idLinea, prodotto: idProdotto},
                                    function (err, lineaCommercialeInfoProdotto) {
                                        if (err){
                                            res.status(500).send(err);
                                        }
                                        else
                                        {
                                            if (lineaCommercialeInfoProdotto)
                                            {
                                                prodotto.prezzoLinea = lineaCommercialeInfoProdotto.prezzoLinea;
                                                prodotto.codiceLinea = lineaCommercialeInfoProdotto.codiceLinea;
                                            }
                                            result.push(prodotto);
                                            res.status(200).send(result)
                                        }
                                    }
                                )
                            }
                            else
                                res.status(200).send(result)
                        }   
                        else
                            res.status(200).send(result)
                    }
                })
            }
        }
    )
}
 
    


/* salva categoria */
exports.save = function (req, res) {
    var data = req.body;
    var lineaCommerciale = new LineaCommerciale(data);
        lineaCommerciale.mdate = new Date();

    lineaCommerciale.save(function (err, result) {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(result);
    });
}

exports.update = function(req, res) { 
    var modify = req.body;
    modify.mdate = new Date();

    LineaCommerciale.findById(req.params.id, 
    function (err, lineaCommerciale) {
        lineaCommerciale.update(modify, {
            runValidators: true
        }, function (err) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send(modify);
            }
        })
    });
};

/* ultimi prodotti aggiornati */
exports.last = function (req, res) {
    console.log("start LineaCommerciale exports.ultimi")
    LineaCommerciale.find(
        {}, 
        {mdate:1,nome:1,note:1,foto:1, pubblicato:1, pubInfoLinea:1,pubSettore:1 })
    .limit(10).sort({mdate: -1})
    .exec(function (err, lineeCommerciali)
    {
        if (err)
            res.status(500).send(err);
        else{
             console.log("End LineaCommerciale exports.ultimi")
            res.status(200).send(lineeCommerciali);
        }
    });
}

 exports.getAll = function (req, res) {
    console.log("start LineaCommerciale exports.getAll")
    LineaCommerciale.find()
    .sort({nome: -1})
    .exec(function (err, lineeCommerciali)
    {
        if (err)
            res.status(500).send(err);
        else{
             console.log("End LineaCommerciale exports.getAll")
            res.status(200).send(lineeCommerciali);
        }
    });
}

exports.delete = function(req, res) {
    LineaCommerciale.remove({
        _id: req.params.id
    }, function (err) {
        if (err) {
            res.status(500).send(err);
        } else {
            res.status(200).end();
        }
    });
};

 exports.search = function (req, res) { 
 
    var search = req.query.search ,
        query = {$or:[
                        { nome: {$regex: search ,$options: 'i'}},
                        { note: {$regex: search ,$options: 'i'}}
                     ]}; 
 
    LineaCommerciale.find(query)
     .limit(40)
    .sort({mdate: -1})
    .exec(function (err, results) {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(results);
    })
}
/*
TipoAssociazione = Cateoria
    associazione della CATEGORIA stessa alla linea commerciale +
        tutti i MODELLI di questa categoria +
        tutti i PRODOTTI di questa categoria

TipoAssociazione = Modello
    associazione del MODELLO stesso alla linea commerciale +
        la CATEGORIA padre del modello +
        tutti i PRODOTTI di questo modello

TipoAssociazione = Codici Prodotto
    associazione dei codici PRODOTTO arrivati alla linea commerciale +
        per ogni prodotto il suo MODELLO padre + 
        per ogni modello padre la sua CATEGORIA
*/
exports.creaAssociazione = function (req, res) {
    var tipoAssociazione = req.body.cosa,
        lineaCommerciale_id = req.body.lineaCommerciale_id,
        codici = req.body.codici
         
    console.log("tipoAssociazione", tipoAssociazione);
    console.log("codici", codici); 
    console.log("lineaCommerciale_id", lineaCommerciale_id);

    //TipoAssociazione = CATEGORIA
    if (tipoAssociazione === 'categoria')
    {
        console.log("Inizio  associazione step CATEGORIA")
        var _categoriaDaAssociare = codici[0];

        LineaCommerciale.findById(lineaCommerciale_id, 
            function (err, lineaCommerciale) {
                
            //trasformo le categorie associate in stringa per poter effettuare operazioni con underscore
            lineaCommerciale.categorieAssociate = 
                trasformCollectToString(lineaCommerciale.categorieAssociate);

            lineaCommerciale.modelliAssociati = 
                    trasformCollectToString(lineaCommerciale.modelliAssociati);

            lineaCommerciale.prodottiAssociati = 
                    trasformCollectToString(lineaCommerciale.prodottiAssociati);

            lineaCommerciale.mdate = new Date(); 

            //inserisco la nuova linea
            lineaCommerciale.categorieAssociate.push(_categoriaDaAssociare);   
            
            //raggruppo titti i codici categria in modo da non avere doppioni
            lineaCommerciale.categorieAssociate = _.uniq(lineaCommerciale.categorieAssociate);
                
    
            //cerco tutti i modelli di questa categoria e 
            //per ognuno di questi creo l'associazione fra linea e modello
  
                Modello.find
                    ({categoria:_categoriaDaAssociare}, {_id:1} ) 
                    .exec(
                        function (err, modelliDaAssociare) {
                            if (err){
                                console.log(results);
                            }
                            else
                            {  
                                modelliDaAssociare =
                                _.collect(modelliDaAssociare,
                                    function(l){
                                         return l._id.toString();
                                    }
                                )

                                lineaCommerciale.modelliAssociati = _.union(lineaCommerciale.modelliAssociati, modelliDaAssociare);
                                
                                lineaCommerciale.modelliAssociati = _.uniq(lineaCommerciale.modelliAssociati);
         
                                //cerco tutti i prodotti di questa categoria e 
                                //per ognuno di questi creo l'associazione fra linea e prodotto
                                Prodotto.find(
                                {categoria:_categoriaDaAssociare},{_id:1},
                                function (err, prodottiDaAssociare) {
                                    if (err)
                                        console.log("err", err)
                                    else{
                                        prodottiDaAssociare =
                                        _.collect(prodottiDaAssociare,
                                            function(l){
                                                 return l._id.toString();
                                            }
                                        )  
 
                                       // lineaCommerciale.prodottiAssociati = 
                                       // _.union(lineaCommerciale.prodottiAssociati, prodottiDaAssociare);
                                        console.log("start concatenazione array nuovo con quello trovato in DB ", new Date())
                                        lineaCommerciale.prodottiAssociati = 
                                        lineaCommerciale.prodottiAssociati.concat(prodottiDaAssociare);
                                        console.log("termine concatenazione array nuovo con quello trovato in DB", new Date())
 
                                       // lineaCommerciale.prodottiAssociati = 
                                       // _.uniq(lineaCommerciale.prodottiAssociati);
 
                                        console.log("start uniq array dei prodotti", new Date())
                                        lineaCommerciale.prodottiAssociati = 
                                            uniq_fast(lineaCommerciale.prodottiAssociati);
                                        console.log("end uniq array dei prodotti", new Date())

                                        LineaCommerciale.update(
                                            {_id: lineaCommerciale_id},
                                            lineaCommerciale,
                                            function (err) {
                                                if (err) {
                                                    console.log(err);
                                                    
                                                } 
                                                else {
                                                    //finito l'associazione per categoria.
                                                    console.log("termine associazione step CATEGORIA")

                                                    res.status(200).send(lineaCommerciale);
                                                }
                                            } 
                                        )
                                    }
                                }
                            ) 
                        }
                    }
                )
            }
        ).lean();
    }
    else if (tipoAssociazione === 'modello')
    {
        console.log("Inizio  associazione step MODELLO")
        LineaCommerciale.findById(lineaCommerciale_id, 
            function (err, lineaCommerciale) {
                if (err) {
                    console.log(err);
                    
                } 
                else 
                { 
                    var _modelloDaAssociare = codici[0];

                    lineaCommerciale.mdate = new Date(); 

                    lineaCommerciale.categorieAssociate = 
                        trasformCollectToString(lineaCommerciale.categorieAssociate);

                    lineaCommerciale.modelliAssociati = 
                        trasformCollectToString(lineaCommerciale.modelliAssociati);
                    
                    lineaCommerciale.prodottiAssociati = 
                        trasformCollectToString(lineaCommerciale.prodottiAssociati);
 
                    //inserisco la nuova linea
                    lineaCommerciale.modelliAssociati.push(_modelloDaAssociare);   
                    
                    //raggruppo titti i codici modello in modo da non avere doppioni
                    lineaCommerciale.modelliAssociati = _.uniq(lineaCommerciale.modelliAssociati);
                    

                    //cerco la categoriaPadre di questo modello
                    //e setto la linea per la categoriaTrovata

                    Modello.findOne({_id:_modelloDaAssociare}) 
                    .exec(function (err, modello)
                    { 
                        if (err)
                            res.status(500).send(err);
                        else{
                            
                           var categoriaPadre = modello.categoria.toString();
 
                            //associo la categoria padre
                            lineaCommerciale.categorieAssociate.push(categoriaPadre);   
                  
                            //raggruppo titti i codici categria in modo da non avere doppioni
                            lineaCommerciale.categorieAssociate = _.uniq(lineaCommerciale.categorieAssociate);

                            //cerco tutti i prodotti di questa categoria e 
                            //per ognuno di questi creo l'associazione fra linea e prodotto
                            Prodotto.find(
                                {modello:_modelloDaAssociare},{_id:1},
                                function (err, prodottiDaAssociare) {
                                    if (err)
                                        console.log("err", err)
                                    else
                                    {
                                        prodottiDaAssociare =
                                        _.collect(prodottiDaAssociare,
                                            function(l){
                                                 return l._id.toString();
                                            }
                                        ) 
             
                                         lineaCommerciale.prodottiAssociati = 
                                            lineaCommerciale.prodottiAssociati.concat(prodottiDaAssociare);
                                         
                                         lineaCommerciale.prodottiAssociati = 
                                            uniq_fast(lineaCommerciale.prodottiAssociati);
 
                                        LineaCommerciale.update(
                                            {_id: lineaCommerciale_id},
                                            lineaCommerciale,
                                            function (err) {
                                                if (err) {
                                                    console.log(err);
                                                    
                                                } 
                                                else {
                                                    //finito l'associazione per categoria.
                                                    console.log("termine associazione step MODELLO")

                                                    res.status(200).send(lineaCommerciale);
                                                }
                                            } 
                                        )
                                    }
                                }
                            )}
                        }
                    );
                }
            }
        ).lean();
    }
    else if (tipoAssociazione === 'codici'){
        console.log("Inizio  associazione step CODOCI");
        LineaCommerciale.findById(lineaCommerciale_id, 
        function (err, lineaCommerciale) {
            if (err) {
                console.log(err);
            } 
            else 
            { 
                var _codiciDaAssociare = codici; 
 
                lineaCommerciale.mdate = new Date(); 
 
                lineaCommerciale.categorieAssociate = 
                    trasformCollectToString(lineaCommerciale.categorieAssociate);
 
                lineaCommerciale.modelliAssociati = 
                    trasformCollectToString(lineaCommerciale.modelliAssociati);

                lineaCommerciale.prodottiAssociati = 
                    trasformCollectToString(lineaCommerciale.prodottiAssociati);

                lineaCommerciale.prodottiAssociati = 
                    lineaCommerciale.prodottiAssociati.concat(_codiciDaAssociare);
                
                lineaCommerciale.prodottiAssociati = 
                    uniq_fast(lineaCommerciale.prodottiAssociati);

                //per ogno codice prodotto che mi arriva 
                //cerco il modelloPadre e setto la linea per il modello trovato
                async.each(_codiciDaAssociare, 
                    function(codiceProdotto,cb){  
                        Prodotto.findOne(
                        {_id:codiceProdotto},{modello:1, categoria:1},
                            function (err, modelloPadre_CategoriaNonno) {

                                if(err)
                                    console.log("errore", err)  
                                if ( modelloPadre_CategoriaNonno ){

                                    //associo la categoria padre
                                    lineaCommerciale.modelliAssociati.
                                         push(modelloPadre_CategoriaNonno.modello.toString());  

                                    lineaCommerciale.modelliAssociati = 
                                   _.uniq(lineaCommerciale.modelliAssociati); 

                                lineaCommerciale.categorieAssociate.
                                         push(modelloPadre_CategoriaNonno.categoria.toString());

                                lineaCommerciale.categorieAssociate = 
                                   _.uniq(lineaCommerciale.categorieAssociate); 
                                 
                                }
                                else{
                                    console.log("codiceProdotto", codiceProdotto, " non trovato ");
                              }
                                cb();
                            }
                        )
                    },
                    function(err){
                        if(err){
                             throw err;
                        }
                        else{
                            LineaCommerciale.update(
                            {_id: lineaCommerciale_id},
                                lineaCommerciale,
                                function (err) {
                                    if (err) {
                                        console.log(err);
                                    } 
                                    else {
                                        //finito l'associazione per categoria.
                                        console.log("termine associazione step CODICI")

                                        res.status(200).send(lineaCommerciale);
                                    }
                                }
                            ) 
                        }
                    }
                )  
            }
        }).lean()
    }
};


function eliminaModelli(lineaCommerciale_id, modelliDaEliminare){
if (modelliDaEliminare.length>0){
        LineaCommerciale.findById(lineaCommerciale_id, 
            function (err, lineaCommerciale) {
                if (err) {
                    console.log(err);
                    throw err;
                } 
                else 
                {   
                    var categorieDaEliminare =[],
                        prodottiDaEliminare  =[];

                    lineaCommerciale.categorieAssociate = 
                            trasformCollectToString(lineaCommerciale.categorieAssociate);

                    lineaCommerciale.modelliAssociati = 
                            trasformCollectToString(lineaCommerciale.modelliAssociati);  

                    lineaCommerciale.prodottiAssociati = 
                            trasformCollectToString(lineaCommerciale.prodottiAssociati);  

                    async.each(modelliDaEliminare, 
                        function(modelloDaEliminare,cb){
                            
                        //Elimino il MODELLO/i
                        var index = lineaCommerciale.modelliAssociati.indexOf(modelloDaEliminare);
                        if(index != -1)
                            lineaCommerciale.modelliAssociati.splice( index, 1 );

                        //Eliminoi i PRODOTTI di questo Modello/i e la categoria
                        Prodotto.find({modello:modelloDaEliminare}, {_id:1, modello:1, categoria:1}) 
                            .exec(function (err, prodottiDaEliminareParziale)
                                { 
                                    if (err)
                                        res.status(500).send(err);
                                    else
                                    {   
                                        //in prodottiDaEliminareParziale
                                        //troviamo sia i prodotti da eliminare
                                        //sia la categoria di questo modello
                                        //faccioun unica select per ottenere categoria e modello

                                        prodottiDaEliminare = 
                                        prodottiDaEliminare.concat(prodottiDaEliminareParziale);

                                        categorieDaEliminare = 
                                            categorieDaEliminare.concat(prodottiDaEliminareParziale);

                                         cb(); 
                                    }
                                }  
                            )  
                        },
                        function(err){
                            if(err){ 
                                console.log(err)
                                throw err;
                            }
                            else
                            {
                                prodottiDaEliminare = _.pluck(prodottiDaEliminare,"_id") 

                                prodottiDaEliminare = trasformCollectToString(prodottiDaEliminare);
 
                                lineaCommerciale.prodottiAssociati = 
                                    filterArray( lineaCommerciale.prodottiAssociati, prodottiDaEliminare);
  
                                categorieDaEliminare = _.pluck(categorieDaEliminare,"categoria") 
 
                                categorieDaEliminare = trasformCollectToString(categorieDaEliminare);
                                
                                lineaCommerciale.categorieAssociate = 
                                                filterArray( lineaCommerciale.categorieAssociate, categorieDaEliminare);


                                /* per ogni modello da eliminare recupero dal DB la categoria corrispondente, 
                                   verifico se nella colonna modelliAssociati esiste 
                                   almeno un modello che  a sua volta ha la stessa categoria.
                                   Se SI non posso cancellare la CATEGORIA da categorieAssociate
                                   se NO posso cancellare la CATEGORIA da categorieAssociate
                                */ 
                                 async.each(modelliDaEliminare, 
                                    function(modello,cb){ 
 
                                        // per ogni modello da eliminare recupero dal DB la categoria corrispondente  
                                        Modello.findOne({_id:modello}) 
                                        .exec(function (err, categoria)
                                            { 
                                                if (err)
                                                    res.status(500).send(err);
                                                else
                                                {   
                                                    async.each(lineaCommerciale.modelliAssociati, 
                                                        function(modello,callBack)
                                                        {
                                                            var query = {_id:modello, categoria:categoria.categoria}; 
                                                             Modello.findOne(  query  )
                                                            .exec(function (err, modelloFratello)
                                                            {
                                                                if (err)
                                                                    res.status(500).send(err);
                                                                else
                                                                    { 
                                                                       if  (modelloFratello){ 

                                                                            lineaCommerciale.categorieAssociate.
                                                                                push(categoria.categoria.toString()); 
  
                                                                        }
                                                                        callBack();  
                                                                    } 
                                                                }
                                                            )

                                                        },
                                                        function (err) {
                                                            if (err) {
                                                                console.log(err);
                                                            } 
                                                            else
                                                            {
                                                                lineaCommerciale.categorieAssociate = 
                                                                    _.uniq(lineaCommerciale.categorieAssociate);  
                                                                cb();
                                                            } 
                                                        }
                                                    ) 
                                                }
                                                
                                            }  
                                        ) 
                                    },
                                    function (err) {
                                        if (err) {
                                            console.log(err);
                                        } 
                                        else
                                        { 
                                            LineaCommerciale.update(
                                            {_id: lineaCommerciale_id}, 
                                                lineaCommerciale,
                                                function (err) {
                                                    if (err) {
                                                        console.log(err);
                                                    } 
                                                    else {
                                                        console.log("termine exports.eliminaAssociazione modelliDaEliminare") 
                                                     }
                                                }
                                            ) 
                                        }
                                    }
                                )
                            }
                        }  
                    )
                }
            }
        ).lean()
    }
}

function eliminaCategorie(lineaCommerciale_id, categorieDaEliminare){
    if (categorieDaEliminare.length>0){
        LineaCommerciale.findById(lineaCommerciale_id, 
            function (err, lineaCommerciale) {
                if (err) {
                    console.log(err);
                } 
                else 
                { 
                    var modelliDaEliminare  =[],
                        prodottiDaEliminare =[];

                    lineaCommerciale.categorieAssociate = 
                        trasformCollectToString(lineaCommerciale.categorieAssociate); 
 
                    lineaCommerciale.modelliAssociati = 
                        trasformCollectToString(lineaCommerciale.modelliAssociati);

                    async.each(categorieDaEliminare, 
                        function(categoriaDaEliminare,cb){
                            
                            //Elimino la CATEGORIA/e   
                            var index = lineaCommerciale.categorieAssociate.indexOf(categoriaDaEliminare);
                            if(index != -1)
                                lineaCommerciale.categorieAssociate.splice( index, 1 );

                            //per ogni categoria cerco i suoi MODELLI
                            Modello.find( {categoria:categoriaDaEliminare}, {_id:1}) 
                                .exec(function (err, modelliDaEliminareParziale)
                                {
                                    if (err)
                                        res.status(500).send(err);
                                    else
                                    { 
                                        modelliDaEliminare = _.union(modelliDaEliminare, modelliDaEliminareParziale)
                                     
                                        Prodotto.find( {categoria:categoriaDaEliminare}, {_id:1}) 
                                            .exec(function (err, prodottiDaEliminareParziale)
                                            { 
                                                if (err)
                                                    res.status(500).send(err);
                                                else
                                                { 
                                                    prodottiDaEliminare = 
                                                        prodottiDaEliminare.concat(prodottiDaEliminareParziale);

                                                    cb(); 
                                                }
                                            }  
                                        )
                                    }
                                }  
                            )  
                        },
                        function(err){
                            if(err){ 
                                throw err;
                            }
                            else
                            {
                                modelliDaEliminare = _.pluck(modelliDaEliminare,"_id") 

                                modelliDaEliminare = trasformCollectToString(modelliDaEliminare);
 
                                lineaCommerciale.modelliAssociati = 
                                    filterArray( lineaCommerciale.modelliAssociati, modelliDaEliminare);
                                 
                                prodottiDaEliminare = _.pluck(prodottiDaEliminare,"_id") 

                                prodottiDaEliminare = trasformCollectToString(prodottiDaEliminare);
 
                                lineaCommerciale.prodottiAssociati = 
                                    filterArray( lineaCommerciale.prodottiAssociati, prodottiDaEliminare);
                                
                                LineaCommerciale.update(
                                {_id: lineaCommerciale_id}, 
                                    lineaCommerciale,
                                    function (err) {
                                        if (err) {
                                            console.log(err);
                                        } 
                                        else {
                                            //finito l'associazione per categoria.
                                            console.log("termine exports.eliminaAssociazione categorieDaEliminare")
                                         }
                                    }
                                )
                            }
                        } 
                    )
                    
                }
            }
        ).lean()
    }
}

function eliminaProdotti(lineaCommerciale_id, prodottiDaEliminare){
    if (prodottiDaEliminare){
        var modelliDaEliminare  =[];
        LineaCommerciale.findById(lineaCommerciale_id, 
            function (err, lineaCommerciale) {
                if (err) {
                    console.log(err);
                } 
                else 
                { 
                    lineaCommerciale.prodottiAssociati = 
                        trasformCollectToString(lineaCommerciale.prodottiAssociati);
 
                    lineaCommerciale.modelliAssociati = 
                        trasformCollectToString(lineaCommerciale.modelliAssociati);

                    modelliAssociatiClone = _.clone(lineaCommerciale.modelliAssociati);

                    lineaCommerciale.categorieAssociate = 
                        trasformCollectToString(lineaCommerciale.categorieAssociate);

                    async.each(prodottiDaEliminare, 
                        function(prodottoDaEliminare,cb){

                            //recupero il modello di questo prodotto
                            Prodotto.findOne( {_id:prodottoDaEliminare}) 
                                .exec(function (err, modello)
                                {
                                    if (err)
                                        res.status(500).send(err);
                                    else
                                    { 
                                        async.each(lineaCommerciale.prodottiAssociati, 
                                            function(prodottoAssociato,callBack)
                                            {
                                                var query = {_id:prodottoAssociato, modello:modello.modello}; 
 
                                                Prodotto.findOne(  query  )
                                                .exec(function (err, prodottoFratello)
                                                {
                                                    if (err)
                                                        res.status(500).send(err);
                                                    else
                                                    { 
                                                        if (prodottoFratello && prodottoAssociato != prodottoDaEliminare) {
                                                                lineaCommerciale.modelliAssociati.push( modello.modello );
                                                        }
                                                        else
                                                        {
                                                            var index = lineaCommerciale.modelliAssociati.indexOf(modello.modello.toString());
                                                            if(index != -1)
                                                                lineaCommerciale.modelliAssociati.splice( index, 1 );
                                                                modelliDaEliminare.push(modello.modello.toString());
                                                        }
                                                        callBack();  
                                                    } 
                                                }) 
                                            },
                                            function (err) {
                                                if (err) {
                                                    console.log(err);
                                                } 
                                                else
                                                {
                                                    lineaCommerciale.modelliAssociati = 
                                                        _.uniq(lineaCommerciale.modelliAssociati);  
                                                    async.each(prodottiDaEliminare, 
                                                        function(prodottoDaEliminare,cb){

                                                            var index = lineaCommerciale.prodottiAssociati.indexOf(prodottoDaEliminare);
                                                            if(index != -1)
                                                            lineaCommerciale.prodottiAssociati.splice( index, 1 );
                                                            cb();
                                                        },
                                                        function (err) {
                                                            if (err) {
                                                                console.log(err);
                                                            } 
                                                            else {
                                                                async.each(modelliDaEliminare, 
                                                                    function(modelloDaEliminare,callback){

                                                                        //recupero la categoria di questo modello
                                                                        Modello.findOne( {_id:modelloDaEliminare}, {categoria:1}) 
                                                                            .exec(function (err, categoria)
                                                                            {
                                                                                if (err)
                                                                                    res.status(500).send(err);
                                                                                else
                                                                                {   
                                                                                     async.each(modelliAssociatiClone, 
                                                                                        function(modelloAssociatoClone,back)
                                                                                        {
                                                                                            var query = {_id:modelloAssociatoClone, categoria:categoria._id.toString()}; 
                                                                                          
                                                                                              Modello.findOne(  query  )
                                                                                            .exec(function (err, modelloFratello)
                                                                                            {
                                                                                                if (err)
                                                                                                    res.status(500).send(err);
                                                                                                else
                                                                                                {    
                                                                                                    if (modelloFratello && modelloAssociato != modelloDaEliminare) {
                                                                                                        lineaCommerciale.categorieAssociate.push( categoria.categoria.toString());
                                                                                                    }
                                                                                                    else
                                                                                                    {
                                                                                                        var index = lineaCommerciale.categorieAssociate.indexOf(categoria.categoria.toString());
                                                                                                        if(index != -1)
                                                                                                            lineaCommerciale.categorieAssociate.splice( index, 1 );
                                                                                                    } 
                                                                                                    back();  
                                                                                                } 
                                                                                            }) 
                                                                                        }      
                                                                                    ) 
                                                                                } 
                                                                            } 
                                                                        ) 
                                                                        callback();
                                                                    },
                                                                    function (err) {
                                                                        if (err) {
                                                                            console.log(err);
                                                                        }
                                                                    } 
                                                                )  
                                                            } 
                                                        }
                                                    )   

                                                } 
                                                cb();
                                            }
                                        ) 

                                    }

                                }  
                            )  
                           
                        },
                        function(err){
                            if(err){ 
                                throw err;
                            }
                            else
                            {
                              LineaCommerciale.update(
                                {_id: lineaCommerciale_id}, 
                                lineaCommerciale,
                                    function (err) {
                                        if (err) {
                                            console.log(err);
                                        } 
                                        else {
                                             console.log("termine exports.eliminaAssociazione eliminaProdotti")
                                         }
                                    }
                                )
                            }
                        }
                    )
                } 

            }
        ).lean()
    }
}

/*
TIPO DI ELIMINAZIONE = categorieDaEliminare
    Elimino la CATEGORIA/e + 
        tutti i modelli figli di questa CATEGORIA + 
        tutti i prodotti nipoti di questa CATEGORIA
 
TIPO DI ELIMINAZIONE = modelliDaEliminare
    Elimino il MODELLO/I + 
        la CATEGORIA PADRE (ma solo se nn ci sono altri modelli per questa categoria)+ 
        tutti i PRODOTTI di questo modello/i

TIPO DI ELIMINAZIONE = modelliDaEliminare
    Elimino il MODELLO/I + 
        la CATEGORIA PADRE (ma solo se nn ci sono altri modelli per questa categoria)+ 
        tutti i PRODOTTI di questo modello/i  
*/
exports.eliminaAssociazione = function (req, res) {

    console.log("start exports.eliminaAssociazione"); 
    var lineaCommerciale_id  = req.body.lineaCommerciale_id,
        categorieDaEliminare = req.body.categorieDaEliminare,
        modelliDaEliminare   = req.body.modelliDaEliminare,
        prodottiDaEliminare  = req.body.prodottiDaEliminare,
        lineaCommercialeRet;

    async.waterfall(
    [
        function(cb) {
             if (categorieDaEliminare.length>0)
                eliminaCategorie(lineaCommerciale_id,categorieDaEliminare);
            else if (modelliDaEliminare.length>0)
                eliminaModelli(lineaCommerciale_id,modelliDaEliminare);
            else if (prodottiDaEliminare.length>0)
                eliminaProdotti(lineaCommerciale_id,prodottiDaEliminare); 
             cb()
        }
    ],
    function(err) {
        if (err)
            console.log(err.message)
        else{
            res.status(200).send(lineaCommercialeRet);
        }
    }) 
};



 


exports.creaAssociazioneMassiva = function (req, res) {
    var  query = {   
         
        $or: [ 
            { generazione: 'G6' }, 
            { generazione: 'D1' }, 
            { generazione: 'V1'},  
            { generazione: '3HE'},
            { generazione: ''},
            { generazione: null},
            { criptato: true }
        ],
        codice:{$ne:null},
        codice:{$ne:''}, 
        kit: {
            $in: [
                true
            ]
        } 
    }; 
var codici = [];
    Prodotto.find(query)
    .exec(function (err, prodotti)
    {

        
 
        async.each(prodotti, 

            function(prodotto, cb){

                if ( is_numeric(  prodotto.codice.substr(0,4)  ) && 

                                (
                                (prodotto.codice.substr(4,1) == 'G' ||
                                prodotto.codice.substr(4,1) == 'H'  ||
                                prodotto.codice.substr(4,1) == 'J'  ||
                                prodotto.codice.substr(4,1) == 'K'  ||
                                prodotto.codice.substr(4,1) == 'L'  ||
                                prodotto.codice.substr(4,1) == 'M'  ||
                                prodotto.codice.substr(4,1) == 'N'  ||
                                prodotto.codice.substr(4,1) == 'P'  ||
                                prodotto.codice.substr(4,1) == 'S'  ||
                                prodotto.codice.substr(4,1) == 'T'  ||
                                prodotto.codice.substr(4,1) == 'U'  ||
                                prodotto.codice.substr(4,1) == 'V') ) )
                            {
                              codici.push(prodotto._id.toString()); 
                            }
                            else if (prodotto.codice.substr(0,3)=='KIT'){
                                codici.push(prodotto._id.toString()); 
                            }
               cb();

            }, 

            function(err){
                if(err)
                    return next(err);

                console.log("START ASSOCIAZIONE", codici.length)


    LineaCommerciale.findOne(
        {nome : "KIT"}, 
        function (err, lineaCommerciale) {
            if (err) {
                console.log(err);
            } 
            else 
            {   
                var _codiciDaAssociare = codici; 
 
                lineaCommerciale.mdate = new Date(); 
 
                lineaCommerciale.categorieAssociate = 
                    trasformCollectToString(lineaCommerciale.categorieAssociate);
 
                lineaCommerciale.modelliAssociati = 
                    trasformCollectToString(lineaCommerciale.modelliAssociati);

                lineaCommerciale.prodottiAssociati = 
                    trasformCollectToString(lineaCommerciale.prodottiAssociati);

                lineaCommerciale.prodottiAssociati = 
                    lineaCommerciale.prodottiAssociati.concat(_codiciDaAssociare);
                
                lineaCommerciale.prodottiAssociati = 
                    uniq_fast(lineaCommerciale.prodottiAssociati);

                //per ogno codice prodotto che mi arriva 
                //cerco il modelloPadre e setto la linea per il modello trovato
                async.each(_codiciDaAssociare, 
                    function(codiceProdotto,cb){  
                        Prodotto.findOne(
                        {_id:codiceProdotto},{modello:1, categoria:1},
                            function (err, modelloPadre_CategoriaNonno) {

                                if(err)
                                    console.log("errore", err)  

                                if ( modelloPadre_CategoriaNonno )
                                {
                                    //associo la categoria padre
                                    lineaCommerciale.modelliAssociati.
                                         push(modelloPadre_CategoriaNonno.modello.toString());  

                                    lineaCommerciale.modelliAssociati = 
                                    _.uniq(lineaCommerciale.modelliAssociati); 

                                    lineaCommerciale.categorieAssociate.
                                         push(modelloPadre_CategoriaNonno.categoria.toString());

                                    lineaCommerciale.categorieAssociate = 
                                    _.uniq(lineaCommerciale.categorieAssociate); 
                                 
                                }
                                else{
                                    console.log("codiceProdotto", codiceProdotto, " non trovato ");
                                }
                                cb();
                            }
                        )
                    },
                    function(err){
                        if(err){
                             throw err;
                        }
                        else{
                            LineaCommerciale.update(
                            {_id: lineaCommerciale._id},
                                lineaCommerciale,
                                function (err) {
                                    if (err) {
                                        console.log(err);
                                    } 
                                    else {
                                        //finito l'associazione per categoria.
                                        console.log("termine associazione step CODICI")

                                        res.status(200).send(lineaCommerciale);
                                    }
                                }
                            ) 
                        }
                    }
                )  
            }
        }).lean() 
    })
})}