var mongoose = require('mongoose'),
	Nazione = mongoose.model('Nazione');

exports.query = function (req, res) 
{
	Nazione.find({},
		function (err, result) 
		{
			if (err)
				res.status(500).send(err);
			else
				res.status(200).send(result);
		}
	);
}

