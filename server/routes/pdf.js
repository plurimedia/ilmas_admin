"use strict";

var CONFIG = require(process.cwd() + '/server/config.js'),
  _ = require('underscore'),
  fs = require('fs'),
  bwipjs = require('bwip-js'),
  mongoose = require('mongoose'),
  Immagine = mongoose.model('Immagine'),
  Prodotto = mongoose.model('Prodotto'),
  Colori = mongoose.model('Colori'),
  LineaCommercialeInfoProdotto = mongoose.model('LineaCommercialeInfoProdotto'),
  Mustache = require('mustache'),
  moment = require('moment'),
  numeral = require('numeral'),
  numeral_it = require('numeral/languages/it'),
  pdf = require('html-pdf'),
  s3 = require('s3'),
  formatters = require('./../routes/utils.js').formatters,
  Utils = require('../../server/routes/utils'),
  classeEnergetica = require('./../routes/utils.js').classeEnergetica,
  s3Client = s3.createClient({
    s3Options: {
      accessKeyId: CONFIG.AWS_ACCESS_KEY_ID,
      secretAccessKey: CONFIG.AWS_SECRET_KEY,
      region: CONFIG.AWS_REGION
    },
  }),
  pdfOptions = {
    format: 'A4',
    orientation: 'portrait',
    border: '1cm',
    header: {
      height: '3cm'
    },
    footer: {
      height: '1.5cm'
    },
    quality: '100',
    timeout: 200000
  },
  pdfTempPath = process.cwd() + '/.tmp/',
  footer = fs.readFileSync(process.cwd() + '/server/templates/pdf/piepagina.html', 'utf8'), // comune a tutti
  css = fs.readFileSync(process.cwd() + '/server/templates/pdf/pdf.css', 'utf8'); // comune a tutti

function getHeader(tipo) {
  let header;

  if (tipo === 1)
    header = fs.readFileSync(process.cwd() + '/server/templates/pdf/intestazione_etichettaGrigia.html', 'utf8');
  else if (tipo === 2)
    header = fs.readFileSync(process.cwd() + '/server/templates/pdf/intestazione_etichettaBianca.html', 'utf8');
  else if (tipo === 3)
    header = fs.readFileSync(process.cwd() + '/server/templates/pdf/intestazione_etichettaEstroGrigia.html', 'utf8');
  else if (tipo === 4)
    header = fs.readFileSync(process.cwd() + '/server/templates/pdf/intestazione_barCode.html', 'utf8');
  else if (tipo === 5) // in offerta ordine cc
    header = fs.readFileSync(process.cwd() + '/server/templates/pdf/intestazione_recapiti.html', 'utf8');
  else if (tipo === 6) // scheda tecnica
    header = fs.readFileSync(process.cwd() + '/server/templates/pdf/intestazione_scheda_tecnica.html', 'utf8');
  else if (tipo === 7) // scheda tecnica estro
    header = fs.readFileSync(process.cwd() + '/server/templates/pdf/intestazione_scheda_tecnica_estro.html', 'utf8');
  else if (tipo === 10) // classe energetica
    header = fs.readFileSync(process.cwd() + '/server/templates/pdf/intestazione_etichettaClasseEnergetica.html', 'utf8');

  return header;
}

function getTemplate(tipo) {
  let template;

  if (tipo === 1)
    template = fs.readFileSync(process.cwd() + '/server/templates/pdf/etichettaGrigia.html', 'utf8');
  else if (tipo === 2)
    template = fs.readFileSync(process.cwd() + '/server/templates/pdf/etichettaBianca.html', 'utf8');
  else if (tipo === 3)
    template = fs.readFileSync(process.cwd() + '/server/templates/pdf/etichettaEstroGrigia.html', 'utf8');
  else if (tipo === 4)
    template = fs.readFileSync(process.cwd() + '/server/templates/pdf/barcode.html', 'utf8');
  else if (tipo === 5)
    template = fs.readFileSync(process.cwd() + '/server/templates/pdf/offerta.html', 'utf8');
  else if (tipo === 6)
    template = fs.readFileSync(process.cwd() + '/server/templates/pdf/scheda_tecnica.html', 'utf8');
  else if (tipo === 7)
    template = fs.readFileSync(process.cwd() + '/server/templates/pdf/scheda_tecnica_estro.html', 'utf8');
  else if (tipo === 8)
    template = fs.readFileSync(process.cwd() + '/server/templates/pdf/schedaTecnicaBinario.html', 'utf8');
  else if (tipo === 9)
    template = fs.readFileSync(process.cwd() + '/server/templates/pdf/etichettaEstroLibera.html', 'utf8');
  else if (tipo === 10)
    template = fs.readFileSync(process.cwd() + '/server/templates/pdf/etichettaClasseEnergetica.html', 'utf8');

  return template;
}

function getFooter() {
  let footer;

  footer = fs.readFileSync(process.cwd() + '/server/templates/pdf/footer_scheda_tecnica.html', 'utf8');

  return footer;
}

function isValidString(s) {
  if (s && s !== null && s !== '')
    return true;
  else
    return false;
}

function normalizzaDatiProdotto(prodotto) {
  if (prodotto.classe_isolamento === '1') // sistemo la classe per mustache (non posso fare campo = valore (logicless))
    prodotto.classe1 = true;

  if (prodotto.classe_isolamento === '2')
    prodotto.classe2 = true;

  if (prodotto.classe_isolamento === '3')
    prodotto.classe3 = true;

  if (prodotto.descrizione_applicazione || prodotto.orientabile || prodotto.inclinabile)
    prodotto.mostra_descrizione_applicazione = true;

  if (prodotto.foro === '')
    delete prodotto.foro;

  if (prodotto.categoria && prodotto.categoria.nome &&
    (prodotto.categoria.nome.indexOf('Incassi') !== -1 ||
      prodotto.categoria.nome.indexOf('incassi') !== -1)) // per stampare l'icona degli incassi
    prodotto.incassi = true;

  if (prodotto.foro && prodotto.foro.indexOf('x') !== -1 || prodotto.foro && prodotto.foro.indexOf('X') !== -1)
    prodotto.foro_quadrato = true;

  if (prodotto.foro && prodotto.foro.indexOf('x') === -1 && prodotto.foro && prodotto.foro.indexOf('X') === -1)
    prodotto.foro_tondo = true;

  if (prodotto.accessori && prodotto.accessori.length)
    prodotto.mostraAccessori = true;

  prodotto = Utils.sistemaIcone(prodotto);
  prodotto = classeEnergetica(prodotto);

  return prodotto;
}

function normalizzaDatiReq(req) {
  if (req.body.classe_isolamento === '1') // sistemo la classe per mustache (non posso fare campo = valore (logicless))
    req.body.classe1 = true;

  if (req.body.classe_isolamento === '2')
    req.body.classe2 = true;

  if (req.body.classe_isolamento === '3')
    req.body.classe3 = true;

  if (req.body.descrizione_applicazione || req.body.orientabile || req.body.inclinabile)
    req.body.mostra_descrizione_applicazione = true;

  if (req.body.alimentatore || req.body.v || req.body.hz || req.body.ma || req.body.w)
    req.body.mostra_cablaggio = true;

  if (req.body.foro === '')
    delete req.body.foro;

  if (req.body.categoria &&
    (req.body.categoria.nome.indexOf('Incassi') !== -1 || req.body.categoria.nome.indexOf('incassi') !== -1)) // per stampare l'icona degli incassi
    req.body.incassi = true;

  if (req.body.foro && req.body.foro.indexOf('x') !== -1 || req.body.foro && req.body.foro.indexOf('X') !== -1)
    req.body.foro_quadrato = true;

  if (req.body.foro && req.body.foro.indexOf('x') === -1 && req.body.foro && req.body.foro.indexOf('X') === -1)
    req.body.foro_tondo = true;

  if (req.body.accessori && req.body.accessori.length)
    req.body.mostraAccessori = true;

  if (req.body.modello.importato === 'true') req.body.modello.importato = 'Importato e distribuito da';

  if (req.body.modello.importato === 'false' || _.isNull(req.body.modello.importato) || _.isUndefined(req.body.modello.importato)) req.body.modello.importato = '';

  req.body = Utils.sistemaIcone(req.body);
  req.body = classeEnergetica(req.body);

  return req;
}


exports.create = async function (req, res, next) {
  var tipo = req.query.tipopdf || req.params.tipopdf,  // tipo di pdf da generare
    en = req.query.en,
    anteprima = req.query.anteprima, // se va in .tmp oppure su s3
    header,
    template,
    filename,
    filepath,
    s3path;

  req.body.en = en; // metto la lingua

  // pdf offerta
  if (tipo === 'offerta') {
    header = getHeader(5);
    template = getTemplate(5); // scegli il template giusto
    filename = req.body.user_email + '.pdf';
    filepath = pdfTempPath; // in anteprima lo metto in .tmp sempre con lo stesso nome
    var i = 1; // aggiungo l'indice ai prodotti, non posso farlo da mustache nel tpl
    _.map(req.body.prodotti, function (p) { p.index = i; i++; return p });
    if (!anteprima)
      s3path = req.body._id + '/' + req.body.numero + '_' + (new Date()).toISOString().replace('T', '_').replace(/[^0-9_]/g, '') + '.pdf'; // path dove metterlo su s3

    pdfOptions = {
      format: 'A4',
      orientation: 'portrait',
      border: '1cm',
      header: {
        height: '3cm'
      },
      footer: {
        height: '1.5cm'
      },
      timeout: 200000
    }
  }

  // scheda tecnica
  if (tipo === 'schedatecnica') {
    filepath = pdfTempPath; // in anteprima lo metto in .tmp sempre con lo stesso nome
    header = getHeader(6);
    footer = getFooter();
    template = getTemplate(6); // scegli il template giusto
    filename = req.body.codice + '.pdf';

    req = normalizzaDatiReq(req);

    if (!anteprima)
      s3path = req.body._id + '/' + req.body.codice + '.pdf'; // path dove metterlo su s3

    pdfOptions = {
      format: 'A4',
      orientation: 'portrait',
      border: '1cm',
      header: {
        height: '3cm'
      },
      footer: {
        height: '1.5cm'
      },
      timeout: 200000
    }
  }

  // scheda tecnica binario
  if (tipo === 'schedaTecnicaBinario') {
    filepath = pdfTempPath; // in anteprima lo metto in .tmp sempre con lo stesso nome
    header = getHeader(6);
    footer = getFooter()
    template = getTemplate(8); // scegli il template giusto
    filename = req.body.codice + '.pdf';

    req = normalizzaDatiReq(req);

    if (!anteprima)
      s3path = req.body._id + '/' + req.body.codice + '.pdf'; // path dove metterlo su s3

    pdfOptions = {
      format: 'A4',
      orientation: 'portrait',
      border: '1cm',
      header: {
        height: '3cm'
      },
      footer: {
        height: '1.5cm'
      },
      timeout: 200000
    }
  }

  // scheda tecnica Estro
  if (tipo === 'schedatecnicaEstro') {
    let profilo, fonte, finitura;

    profilo = await Prodotto.findOne({ codice: req.body.profilo })
      .populate('modello', { 'nome': 1, 'peso': 1, 'volume': 1, 'ecolamp': 1, 'made_in': 1, 'importato': 1 })
      .populate('modello_cript', { 'nome': 1 })
      .populate('categoria', { 'nome': 1, 'tipo': 1 })
      .populate('accessori')
      .exec()
    fonte = await Prodotto.findOne({ codice: req.body.fonte })
      .populate('modello', { 'nome': 1, 'peso': 1, 'volume': 1, 'ecolamp': 1, 'made_in': 1, 'importato': 1, 'classe_energetica': 1 })
      .populate('modello_cript', { 'nome': 1 })
      .populate('categoria', { 'nome': 1, 'tipo': 1 })
      .populate('accessori')
      .exec()
    finitura = await Colori.findOne({ codice: req.body.finitura }).exec()

    filepath = pdfTempPath; // in anteprima lo metto in .tmp sempre con lo stesso nome
    header = getHeader(7);
    footer = getFooter()
    template = getTemplate(7); // scegli il template giusto
    filename = req.body.codice_finito + '.pdf';

    //flag per renderizzare frase ad hoc per i profili dentro l'if sulla scheda tecnica (vd template per posizione)
    if (req.body.profilo === 'JH' || req.body.profilo === 'JP' || req.body.profilo === 'JN' || req.body.profilo === 'JQ' || req.body.profilo === 'JO' || req.body.profilo === 'JG' || req.body.profilo === 'JJ' || req.body.profilo === 'JT') {
      profilo.frase = true;
    }

    //flag per nascondere note fonte luminosa dove necessario
    if (req.body.profilo === 'JT' || req.body.profilo === 'CR' || req.body.profilo === 'AR') {
      profilo.noteFonte = false;
    } else {
      profilo.noteFonte = true;
    }

    req.body.profilo = normalizzaDatiProdotto(profilo);
    req.body.fonte = normalizzaDatiProdotto(fonte);

    req.body.finitura = finitura
    req.body.lunghezza = req.body.lunghezza ? req.body.lunghezza : (parseInt(req.body.numero_moduli) * 140)
    req.body.peso = parseFloat((profilo.peso ? profilo.peso : 0) +
      (fonte.peso ? fonte.peso : 0)).toFixed(2)

    if (!anteprima)
      s3path = profilo._id + '/' + req.body.codice_finito + '.pdf'; // path dove metterlo su s3

    pdfOptions = {
      format: 'A4',
      orientation: 'portrait',
      border: '1cm',
      header: {
        height: '3cm'
      },
      footer: {
        height: '1.5cm'
      },
      timeout: 200000
    }
  }
  // aggiungere le etichette estro

  // pdf etichetta grigia
  if (tipo === 'etichetta_grigia') {
    filepath = pdfTempPath; // in anteprima lo metto in .tmp sempre con lo stesso nome
    header = getHeader(1);
    template = getTemplate(1);
    filename = tipo + req.body.codice + '.pdf';
    s3path = req.body._id + '/' + filename; // path dove metterlo su s3

    pdfOptions = {
      width: '5cm',
      height: '2.5cm',
      orientation: 'landscape',
      quality: '100',
      timeout: 200000
    }
    req = normalizzaDatiReq(req);
    req.body.modello.nome = req.body.modello.nome.substring(0, 24);
  }

  // pdf etichetta bianca
  if (tipo === 'etichetta_bianca') {
    filepath = pdfTempPath; // in anteprima lo metto in .tmp sempre con lo stesso nome
    header = getHeader(2);
    template = getTemplate(2);
    filename = tipo + req.body.codice + '.pdf';
    s3path = req.body._id + '/' + filename; // path dove metterlo su s3
    if (req.body.codiceEan)
      req.body.urlEan = 'http://bwipjs-api.metafloor.com/?bcid=ean13&text=' + req.body.codiceEan + '&scaleX=2&scaleY=1&textsize=7'
      // req.body.urlEan = req.protocol + '://' + req.get('host') + '/barcode?bcid=ean13&text=' + req.body.codiceEan + '&scaleX=2&scaleY=1&textsize=7'

    pdfOptions = {
      width: '10cm',
      height: '9.8cm',
      orientation: 'landscape',
      quality: '100',
      base: req.protocol + '://' + req.get('host'),
      timeout: 200000
    }

    req = normalizzaDatiReq(req);
  }

  if (tipo === 'etichettaEstroGrigia') {
    if (req.body.profilo && req.body.fonte && req.body.finitura) {
      //dati completi dei prodotti piu finitura
      req.body.profilo = normalizzaDatiProdotto(req.body.profilo);
      req.body.fonte = normalizzaDatiProdotto(req.body.fonte);
      filepath = pdfTempPath; // in anteprima lo metto in .tmp sempre con lo stesso nome
      header = getHeader(3); // abbiamo scoperto che l'header è inutile
      template = getTemplate(3);
      filename = tipo + req.body.profilo.codice + '-' + req.body.fonte.codice + '-' + req.body.finitura.codice + '.pdf';
      s3path = req.body._id + '/' + filename; // path dove metterlo su s3

      pdfOptions = {
        width: '10.6cm',
        height: '1.5cm',
        orientation: 'landscape',
        quality: '100',
        timeout: 200000
      }
    }
  }

  // pdf etichetta grigia con campi liberi
  if (tipo === 'etichettaEstroLibera') {
    if (req.body.prodotto && req.body.prodotto.codice) { // minimo controllo, magari serve più stretto
      filepath = pdfTempPath; // in anteprima lo metto in .tmp sempre con lo stesso nome
      header = getHeader(3); // abbiamo scoperto che l'header è inutile
      template = getTemplate(9);
      filename = tipo + req.body.prodotto.codice + '.pdf';

      if (req.body.prodotto.classe === '1') req.body.prodotto.classe1 = true
      if (req.body.prodotto.classe === '2') req.body.prodotto.classe2 = true
      if (req.body.prodotto.classe === '3') req.body.prodotto.classe3 = true

      s3path = req.body._id + '/' + filename; // path dove metterlo su s3
      pdfOptions = {
        width: '10.6cm',
        height: '1.5cm',
        orientation: 'landscape',
        quality: '100',
        timeout: 200000
      }
    }
  }

  // pdf bar code
  if (tipo === 'etichetta_barCode') {
    filepath = pdfTempPath; // in anteprima lo metto in .tmp sempre con lo stesso nome
    header = getHeader(4);
    template = getTemplate(4);
    filename = tipo + req.body.codice + '.pdf';
    s3path = req.body._id + '/' + filename; // path dove metterlo su s3
    if (req.body.codiceEan)
      req.body.urlEan = 'http://bwipjs-api.metafloor.com/?bcid=ean13&text=' + req.body.codiceEan + '&scale=3&rotate=N&includetext'
      // req.body.urlEan = req.protocol + '://' + req.get('host') + '/barcode?bcid=ean13&text=' + req.body.codiceEan + '&scale=3&rotate=N&includetext'
    // console.log('req.body.urlEan2='+req.body.urlEan, 'req.protocol + :// + req.get(host)', req.protocol + '://' + req.get('host'))

    pdfOptions = {
      width: '5cm',
      height: '5cm',
      orientation: 'landscape',
      quality: '100',
      base: req.protocol + '://' + req.get('host'),
      timeout: 200000
    }

    req = normalizzaDatiReq(req);
  }

  // pdf classe energetica
  if (tipo === 'etichetta_classeEnergetica') {
    filepath = pdfTempPath; // in anteprima lo metto in .tmp sempre con lo stesso nome
    header = getHeader(10);
    template = getTemplate(10);
    filename = tipo + req.body.codice + '.pdf';
    s3path = req.body._id + '/' + filename; // path dove metterlo su s3

    pdfOptions = {
      width: '5cm',
      height: '5cm',
      orientation: 'landscape',
      quality: '100',
      timeout: 200000
    }

    req = normalizzaDatiReq(req);
  }

  var data = _.extend(req.body, { css: css, header: header, footer: footer, formatters });


  var html = Mustache.render(template, data);

  // console.log('html', html)

  pdf.create(html, pdfOptions).toFile(filepath + filename, function (err, response) {
    if (err) {
      console.log('err', err);
      return next(err);
    }

    // se è un'anteprima mi fermo li, lo faccio vedere mandando il path assoluto
    if (anteprima) {
      res.status(200).send({ file: filename }) // rimando il link diretto per vederlo
      return;
    }

    // se non è un anteprima, lo invio su S3 per usi futuri...(es allegato mail)
    var fileParams = {
      localFile: filepath + filename,
      s3Params: {
        Key: s3path,
        Bucket: CONFIG.AWS_S3_BUCKET,
        ACL: 'public-read'
      }
    },
      uploader = s3Client.uploadFile(fileParams);

    uploader.on('error', function (err) {
      res.status(500).send(err.stack)
    });

    uploader.on('end', function (r) {
      res.status(200).send({ file: CONFIG.AWS_S3_BUCKET_PATH + '/' + s3path }) // mando al client l'url s3
    });
  });
}

// api per generare la scheda tecnica del codice di un prodotto
// torna l'url del file generato su s3 (rimane solo per tot giorni)
exports.prodottoSchedaTecnica = function (req, res) {
  var idProdotto = req.params.idprodotto;

  Prodotto.findOne({ _id: idProdotto })
    .populate('modello', { 'nome': 1, 'peso': 1, 'volume': 1, 'ecolamp': 1 })
    .populate('modello_cript', { 'nome': 1 })
    .populate('categoria', 'nome')
    .populate('accessori')
    .exec(function (err, prodotto) {
      if (err) {
        res.status(500).send(err);
        return;
      }

      if (prodotto) {
        if (req.params.lingua == 'en')
          prodotto.en = req.params.lingua;

        if (prodotto.accessori && prodotto.accessori.length)
          prodotto.accessori = prodotto.accessori.filter(a => a._id);

        prodotto = normalizzaDatiProdotto(prodotto);

        var tipoScheda = getTemplate(6);

        var id_binari = [
          { _id: '5711f38e5f09a60300e387b0' },
          { _id: '5711f4135f09a60300e387b2' },
          { _id: '5711f4485f09a60300e387b5' },
          { _id: '5711f3f85f09a60300e387b1' },
          { _id: '59cbae665a6aad0400841465' },
          { _id: '59db30769b066b0400cd5fb4' },
          { _id: '59dc8b3439173e040027a22f' },
          { _id: '59db543239173e040027a161' },
          { _id: '59db52f839173e040027a159' },
          { _id: '59db52d139173e040027a158' },
          { _id: '59db4c2b39173e040027a148' },
          { _id: '59db4b4c39173e040027a141' },
          { _id: '59db3c8c9b066b0400cd6005' },
          { _id: '59ccc39650af6204004504a1' },
          { _id: '59ccb0b450af62040045046b' },
          { _id: '59ccaf8a50af62040045045e' }
        ]

        id_binari.forEach
          (
            function (idBinario) {
              if (prodotto.modello._id == idBinario._id) {
                tipoScheda = getTemplate(8);
                return;
              }
            }
          )

        var date = new Date(),
          timestamp = date.valueOf(),
          header = getHeader(7),
          footer = getFooter(),
          template = tipoScheda,
          data = _.extend(prodotto, { css: css, header: header, footer: footer, formatters }),
          html = Mustache.render(template, data),
          localPath = pdfTempPath + timestamp + '.pdf', // sul server lo salvo col timestamp in /temp
          s3Path = 'temp/' + timestamp + '-' + prodotto.codice + '.pdf'; // cartella temporanea e timestamp

        var pdfOptions = {
          format: 'A4',
          orientation: 'portrait',
          border: '1cm',
          header: {
            height: '3cm'
          },
          footer: {
            height: '1.5cm'
          },
          timeout: 200000
        }

        pdf.create(html, pdfOptions).toFile(localPath, function (err, response) {
          if (err) {
            res.status(500).send(err);
            return;
          }

          // lo mando su s3 in una cartella che vuoterò periodicamente
          var fileParams = {
            localFile: localPath,
            s3Params: {
              Key: s3Path,
              Bucket: CONFIG.AWS_S3_BUCKET,
              ACL: 'public-read'
            }
          },
            uploader = s3Client.uploadFile(fileParams);

          uploader.on('error', function (err) {
            res.status(500).send(err.stack)
          });

          uploader.on('end', function (r) {
            res.status(200).json({ url: CONFIG.AWS_S3_BUCKET_PATH + '/' + s3Path }) // mando al client l'url s3 dove l'ho messo
          });
        });
      }
    });
}

// api per generare la scheda tecnica del codiceLineaProdotto
// torna l'url del file generato su s3 (rimane solo per tot giorni)
exports.prodottoSchedaTecnicaInfoLinea = function (req, res) {
  var idProdotto = req.params.idprodotto
  codiceLinea = req.params.codiceLinea;

  if (!idProdotto || !codiceLinea)
    res.status(500).send('Errore invio dati');

  Prodotto.findOne({ _id: idProdotto })
    .populate('modello', { 'nome': 1, 'peso': 1, 'volume': 1, 'ecolamp': 1, 'sostituibilita_LED': 1, 'sostituibilita_driver': 1 })
    .populate('modello_cript', { 'nome': 1 })
    .populate('categoria', 'nome')
    .populate('accessori')
    .exec(function (err, prodotto) {
      if (err) {
        res.status(500).send(err);
        return;
      }

      if (prodotto) {
        if (req.params.lingua == 'en')
          prodotto.en = req.params.lingua;

        var newAccessori = [];
        prodotto.accessori.forEach
          (
            function (accessorio) {
              if (accessorio) newAccessori.push(accessorio)
            }
          )

        prodotto.accessori = newAccessori
        prodotto = normalizzaDatiProdotto(prodotto);

        LineaCommercialeInfoProdotto.findOne({ codiceLinea: codiceLinea })
          .exec(function (err, lineaCommercialeInfoProdotto) {
            if (isValidString(lineaCommercialeInfoProdotto.codiceLinea))
              prodotto.codice = lineaCommercialeInfoProdotto.codiceLinea;

            if (isValidString(lineaCommercialeInfoProdotto.prezzoLinea))
              prodotto.prezzo = lineaCommercialeInfoProdotto.prezzoLinea;

            var date = new Date(),
              timestamp = date.valueOf(),
              header = getHeader(5),
              template = getTemplate(6),
              data = _.extend(prodotto, { css: css, header: header, footer: footer, formatters }),
              html = Mustache.render(template, data);
            localPath = pdfTempPath + timestamp + '.pdf', // sul server lo salvo col timestamp in /temp
              s3Path = 'temp/' + timestamp + '-' + prodotto.codice + '.pdf'; // cartella temporanea e timestamp

            pdfOptions = {
              format: 'A4',
              orientation: 'portrait',
              border: '1cm',
              header: {
                height: '3cm'
              },
              footer: {
                height: '1.5cm'
              },
              timeout: 200000
            }
            pdf.create(html, pdfOptions).toFile(localPath, function (err, response) {
              if (err)
                res.status(500).send(err);

              // lo mando su s3 in una cartella sche vuoterò periodicamente
              var fileParams = {
                localFile: localPath,
                s3Params: {
                  Key: s3Path,
                  Bucket: CONFIG.AWS_S3_BUCKET,
                  ACL: 'public-read'
                }
              },
                uploader = s3Client.uploadFile(fileParams);

              uploader.on('error', function (err) {
                res.status(500).send(err.stack)
              });

              uploader.on('end', function (r) {
                // torno l'url del pdf
                res.status(200).json({ url: CONFIG.AWS_S3_BUCKET_PATH + '/' + s3Path }) // mando al client l'url s3 dove l'ho messo
              });
            });
          })
      }
    });
}

exports.barcode = function (req, res) {
  if (req.url.indexOf('/barcode?bcid=') != 0) {
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end('BWIPJS: Unknown request format.', 'utf8');
  }
  else
    bwipjs.request(req, res, { sizelimit:1024*1024 }); // Executes asynchronously
}
