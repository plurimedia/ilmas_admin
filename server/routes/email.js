var CONFIG = require(process.cwd() + '/server/config.js'),
    mandrill = require('mandrill-api/mandrill'),
    mandrill_client = new mandrill.Mandrill(CONFIG.MANDRILL_API_KEY),
    juice = require('juice'), // mette gli stili in linea in un html
    fs = require('fs'),
    Mustache = require('mustache'),
    template_css = fs.readFileSync(process.cwd() + '/server/templates/email/email.css', 'utf8'), // css base (usiamo zurb per email: http://foundation.zurb.com/emails)
    template_header = fs.readFileSync(process.cwd() + '/server/templates/email/header.html', 'utf8'), // header comune a tutte le mail
    template_footer = fs.readFileSync(process.cwd() + '/server/templates/email/footer.html', 'utf8'), // footer comune a tutte le mail
    template_offerta = fs.readFileSync(process.cwd() + '/server/templates/email/offerta.html', 'utf8'), // specifico
    template_mailingList = fs.readFileSync(process.cwd() + '/server/templates/email/mailingList.html', 'utf8'), // specifico
    template_exportFileKite = fs.readFileSync(process.cwd() + '/server/templates/email/mailingExportFikeKite.html', 'utf8'), // specifico
    template_exportDistintaBase = fs.readFileSync(process.cwd() + '/server/templates/email/mailingExportDistintaBase.html', 'utf8'), // specifico
    template_attivita_approvazione = fs.readFileSync(process.cwd() + '/server/templates/email/attivita_approvazione.html', 'utf8'), // specifico
    template_configuratore = fs.readFileSync(process.cwd() + '/server/templates/email/configuratore.html', 'utf8'),


    _ = require('underscore');

exports._send = function (req, res, cb) {

    /*
    	arrivano: contenuto (oefferta, ordine cc...) e parametri di invio della mail
    */
        var tipoEmail="";
        if (req.query && req.query.tipo)
            tipoEmail = req.query.tipo;
        else
            tipoEmail = req.body.tipo

        var emailContent = req.body.contenuto, // contenuto della mail
        emailParams = req.body.email, // parametri di invio
        _to = function () {
            var to = [],
                _formattaIndirizzi = function (indirizzi, tipo) {

                    return _.map(indirizzi, function (ind) {

                        if (CONFIG.ENV !== 'production' && 1==2)
                        {
                            //in test ed in dev tutte le mail
                            //vengono inviate a me.
                            ind = "amedeo.buco@plurimedia.it"
                        }

                        return {
                            email: ind,
                            type: tipo
                        }
                    })
                };

            // mandrill li vuole in questo formato
            if (emailParams.a.length)
                to = _.union(to, _formattaIndirizzi(emailParams.a, 'to'));

            if (emailParams.cc.length)
                to = _.union(to, _formattaIndirizzi(emailParams.cc, 'cc'));
            if (emailParams.ccn.length)
                to = _.union(to, _formattaIndirizzi(emailParams.ccn, 'bcc'));


            //gli invvi massivi vengono inviati come se fossero ccn
            //attualmente la funzionalità è presente solo in mailing_list
            if (emailParams.a_inviomassivo && emailParams.a_inviomassivo.length)
                    to = _.union(to, _formattaIndirizzi(emailParams.a_inviomassivo, 'bcc'));
            return to;

        },
        // mi arrivano array di nomifiles, creo l'url
        _fileUrls = function (files) {
            return _.map(files, function (f) {
                return {
                    url: CONFIG.AWS_S3_BUCKET_PATH + '/' + emailContent._id + '/' + f,
                    name: f
                }
            })
        },
        templateContenuto;

    // stabilisco il template del contenuto in base al tipo di mail
    if (tipoEmail === 'offerta') {
        templateContenuto = template_offerta;
    }
    if (tipoEmail === 'mailingList') {
        templateContenuto = template_mailingList;
    }
    if (tipoEmail === 'exportFileKite') {
        templateContenuto = template_exportFileKite;
    }
    if (tipoEmail === 'attivita_approvazione') {
        templateContenuto = template_attivita_approvazione;
    }
    if (tipoEmail === 'export_distintabase') {
        templateContenuto = template_exportDistintaBase;
    }

    // se ci sono allegati, prepara gli url per mustache
    if (emailParams.files)
        emailParams.files = _fileUrls(emailParams.files);

    // se ci sono schede tecniche, prepara gli url per mustache
    if (emailParams.schede)
        emailParams.schede = _fileUrls(emailParams.schede);

    if (req.query && req.query.en)
        emailParams.en = req.query.en;

    var header = Mustache.render(template_header, {
            css: template_css
        }),
        message = { // preparo il messaggio
            html: juice(Mustache.render(templateContenuto, {
                contenuto: emailContent, //dati (offerta, ordine ecc...)
                email: emailParams, // dati della mail
                header: header, // comune
                footer: template_footer // comune
            })),
            subject: emailParams.oggetto,
            from_email: emailParams.from_email,
            from_name: emailParams.from_name,
            to: _to(),
            headers: {
                "Reply-To": emailParams.reply_to
            },
            track_opens: 1,
            track_clicks: 1
        };


    // mando!
    mandrill_client.messages.send({
            message: message,
            async: false, // singola mail non serve un batch
        },
        function (result) {
            if (res && res.status)
                res.status(200).send(result);
            /*else{
                console.log(result);

                console.log("errore in fase di invio mail", _to(), res);

                //cb(null, result);
            }*/
        },
        function (e) {

             // Mandrill returns the error as an object with name and message keys
            //if (res && res.status)
             //   res.status(500).send('Errore mail: ' + e.name + ' - ' + e.message);
            //else
            //    cb(e);

        }
    );

}

exports.send_no_route = function (req, cb) {
    exports._send(req, {}, cb);
}

exports.send = function (req, res) {
    exports._send(req, res);
}

/* controlla lo stato di una mail */
exports.stato = function (req, res) {
    mandrill_client.messages.info({
            id: req.params.id
        },
        function (result) {
            res.status(200).send(result)
        },
        function (err) {
            res.status(200).send(err)
        }
    )
}

exports.configuratoreSend = function (req, res) {

    const name = req.body.name;
    const emailUser = req.body.emailUser;
    const number = req.body.number;
    const company = req.body.company;
    const country = req.body.country;
    const notes = req.body.notes;
    const contenuto = req.body.contenuto;
    const tmpToArray = process.env.CONFIGURATORE_EMAILS ? process.env.CONFIGURATORE_EMAILS : CONFIG.CONFIGURATORE_EMAILS;

    const toArray =   Array.isArray(tmpToArray)? tmpToArray : tmpToArray.split(",");
    console.log("toArray", toArray);
    var emailArray = Array.isArray(toArray)? toArray.map(item => {
        return {
          "email": item
        }
      }) : [{"email": toArray}]
    //console.log('contenuto: ', contenuto);
    var header = Mustache.render(template_header, {
            css: template_css
        }),
        message = { // preparo il messaggio
            html: juice(Mustache.render(template_configuratore, {
                name: name,
                emailUser: emailUser,
                number: number,
                company: company,
                country: country,
                notes: notes,
                contenuto: contenuto,
                header: header, // comune
                footer: template_footer // comune
            })),
            subject: "Richiesta offerta da configuratore ESTRO: " + emailUser,
            from_email: "tech@plurimedia.it",
            from_name: "tech@plurimedia.it",
            to: emailArray,
            //process.env.CONFIGURATORE_EMAILS
            //from: "tech@plurimedia.it",
            //to: ["milosh.nobili@plurimedia.it"]
        };


        // mando!
        mandrill_client.messages.send({
            message: message,
            async: false, // singola mail non serve un batch
        },
        function (result) {
            if (res && res.status)
                res.status(200).send(result);
            else{
                console.log(result);
                res.status(500).send(result);
            }
        }
    );

}
