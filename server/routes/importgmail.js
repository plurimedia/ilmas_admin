var fs = require('fs'),
    csv = require("fast-csv"),
    _ = require('underscore'),
    async = require('async'),
    mongoose = require('mongoose'),
    Contatto = mongoose.model('Contatto'),
    Comune = mongoose.model('Comune'),
    Nazione = mongoose.model('Nazione'),
    /* controlla i dati */
    _checkGmailData = function (data) {

        var contatto = {
                mdate: new Date()
            },
            _nome = function (data) {
                if (data['First Name'] !== "" && data['Last Name'] !== "")
                    return data['First Name'] + ' ' + data['Last Name'];
                if (data['First Name'] === "" && data['Last Name'] !== "")
                    return data['Last Name'];
                if (data['First Name'] !== "" && data['Last Name'] === "")
                    return data['First Name'];
                else
                    return "";
            },
            _Try = function (data) {

                /* cerca di riempire i dati dai json ufficiali di comune e nazione */
                var comuneGmail = data['Business City'],
                    nazioneGmail = data['Business Country'],
                    provinciaGmail = data['Business State'],
                    capGmail = data['Business Postal Code'],
                    strlength = comuneGmail.split(' ').length,
                    comunesafe,
                    result = {},
                    _capitalize = function (str) {
                        return str.replace(/\w\S*/g, function (txt) {
                            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
                        });
                    };

                /* cerco se esistono comune e nazione nel db */
                async.series([
                    function (callback) {

                            Comune.findOne({
                                nome: _capitalize(comuneGmail)
                            }, function (err, comune) {
                                if (err)
                                    throw err;

                                if (comune) {
                                    result.comune = comune.nome;
                                    result.provincia = comune.provincia;
                                    result.cap = comune.cap;
                                    result.nazione = 'Italia';
                                } else {
                                    result.comune = comuneGmail;
                                    result.provincia = provinciaGmail;
                                    result.cap = capGmail;
                                }

                                callback();

                            })

                    },
                    function (callback) {
                            Nazione.findOne({
                                nome: _capitalize(nazioneGmail)
                            }, function (err, nazione) {
                                if (err)
                                    throw err;

                                if (nazione) {
                                    result.nazione = nazione.nome;
                                } else {
                                    result.nazione = nazioneGmail;
                                }

                            })
                    }
                ],
                    // optional callback
                    function (err, results) {
                        return result;
                    });


            };

        contatto.nome = _nome(data)
        contatto.email = data['E-mail Address'];
        contatto.indirizzo = data['Business Address'];
        contatto.piva = "";
        contatto.cf = "";
        contatto.telefono = data['Primary Phone'];
        contatto.fax = data['Home Fax'];
        contatto.note = "Importato da gmail";
        contatto.azienda = data['Company'].toLowerCase();
        contatto.keywords = _.compact(_.flatten([
            _.map(_nome(data).split(' '),function(k){return k.toLowerCase()}),
            contatto.email,
            contatto.email.split('@')[0],
            contatto.email.split('@')[1].split('.')[0],
            _.map(contatto.azienda.split(' '), function(k){return k.toLowerCase()}),
            data['Business City'].toLowerCase(),
            data['Business Country'].toLowerCase()
        ]));

        return _.extend(contatto, _Try(data));

    },
    /* IMPORT da csv gmail  */
    _parseGmailCsv = function (csvpath, res) {

        var gmailContacts = [],
            gmailContactsInserted = [],
            gmailContactsRejected = [],
            gmailContactsInvalid = [],
            gmailContactsDuplicates = [];

        var stream = fs.createReadStream(csvpath);
        // dopodichè parso e salvo
        csv.fromStream(stream, {
                headers: true,
                delimiter: ',',
                discardUnmappedColumns: true
            })
            .validate(function (data) {
                if (data['E-mail Address'] === '') {
                    gmailContactsInvalid.push(data);
                    return false
                } else
                    return true;
            })
            .on("data", function (data) {
                gmailContacts.push(_checkGmailData(data))
            })
            .on("end", function () {

                fs.unlinkSync(csvpath); // cancello il file

                /* import nel db */

                async.each(gmailContacts,
                    function (contatto, callback) {
                        /* controllo se esiste già, ovvero se ha nome e mail uguali */
                        Contatto.findOne({
                            nome: contatto.nome,
                            email: contatto.email
                        }, function (err, result) {
                            if (err)
                                res.status(500).send(err)

                            /* esiste già*/
                            if (result) {
                                gmailContactsDuplicates.push(contatto)
                                callback();
                            }
                            /* inserisco */
                            else {

                                var c = new Contatto(contatto);

                                c.save(function (err, result) {

                                    if (err) {
                                        /* contatto che non passa la validazione */
                                        gmailContactsRejected.push(_.omit(contatto, 'mdate', 'tags'))
                                        callback();
                                    } else {
                                        gmailContactsInserted.push(contatto)
                                        callback();
                                    }
                                });
                            }

                        })

                    },
                    /* finito di processarli tutti */
                    function (err) {
                        if (err)
                            res.status(500).end();

                        else {
                            csv
                                .writeToPath(".tmp/gmailErrors.csv", gmailContactsRejected, {
                                    headers: true
                                })
                                .on("finish", function () {
                                    res.status(200).send({
                                        inserted: gmailContactsInserted.length,
                                        existing: gmailContactsDuplicates.length,
                                        rejected: {
                                            count: gmailContactsRejected.length,
                                            log: 'gmailErrors.csv'
                                        }
                                    });
                                });
                        }
                    }
                );
            });
    };




exports.import = function (req, res) {
    // riceve un file csv di gmail e lo parsa per riempire 
    var filePath = req.files.file.path;
    _parseGmailCsv(filePath, res);
}
