var mongoose = require('mongoose'),
News = mongoose.model('News');
cloudinary = require('cloudinary'),
async = require('async');
Immagine = mongoose.model('Immagine'),

cloudinary.config({
    cloud_name: CONFIG.CLOUDINARY_CLOUD_NAME,
    api_key: CONFIG.CLOUDINARY_APY_KEY,
    api_secret: CONFIG.CLOUDINARY_API_SECRET
});

exports.last = function (req, res) 
{
    News.find({}, 
        function (err, results) 
        {
            if(err)
                res.status(500).send(err);

            res.status(200).send(results)
        }
    )
    .sort({mdate:-1})
    .limit(40)
}

exports.get = function (req, res) 
{
    News.findById(req.params.id, 
        function (err, news) 
        {
            if(err)
                res.status(500).send(err);
        res.status(200).send(news)
    })
}

exports.save = function (req, res) 
{
    var data = req.body;
    var news = new News(data);
        news.mdate = new Date(),
        news.last_user = req.user;
       
    news.save(function (err, result) 
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(result);
    });
}

exports.update = function(req, res) 
{
    var modify = req.body;
    modify.mdate = new Date(),
    modify.last_user = req.user;
    News.findById(req.params.id, 
        function (err, news) 
        {
            news.update(modify, 
            {
                runValidators: true
            }, 
            function (err) 
            {
                if (err)
                    res.status(500).send(err);
                else
                    res.status(200).send(modify);
            })
        }
    );
};

exports.delete = function(req, res)
{
    News.remove({
        _id: req.params.id
    }, 
    function (err) 
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).end();
    });
};

exports.cerca = function (req, res) 
{
    News.find(
        { $or:
            [
                {descrizione_it:{$regex: req.query.search, $options: 'i'}},
                {descrizione_en:{$regex: req.query.search, $options: 'i'}},
                {titolo_it:{$regex: req.query.search, $options: 'i'}},
                {titolo_en:{$regex: req.query.search, $options: 'i'}},
                {abstract_it:{$regex: req.query.search, $options: 'i'}},
                {abstract_en:{$regex: req.query.search, $options: 'i'}}
            ]
        }, 
        function (err, result) {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(result);
    })
   
}

