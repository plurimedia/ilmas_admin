var mongoose = require('mongoose'),
    Dashboard = mongoose.model('Dashboard'),
    moment = require('moment');

const Modello = require('./modello')
const Categoria = require('./categoria')
const Componente = require('./componente')
const DwhEtichetta = require('./dwhEtichetta')
const Contatto = require('./contatto')
const Offerta = require('./offerta')
const Prodotto = require('./prodotto')

// test, cancellare
exports.build = async (req, res) => {
    const totModelli = await Modello.countFn('aProdotti')
    const totModelliEstro = await Modello.countFn('aComponenti')
    const totCategorie = await Categoria.countFn('aProdotti')
    const totCategorieEstro = await Categoria.countFn('aComponenti')
    const totComponenti = await Componente.countFn()
    const totEtichette = await DwhEtichetta.countFn()
    const totContatti = await Contatto.countFn()
    const totOfferte = await Offerta.countFn()
    const totEan = 0 // la funzione non torna mai niente di diverso, lo metto hardcoded
    const totAProdotti = await Prodotto.countFn('aProdotti')
    const totAComponenti = await Prodotto.countFn('aComponenti')
    console.log('totModelli', totModelli, totModelliEstro, totCategorie, totCategorieEstro, totComponenti, totEtichette, totContatti, totOfferte, totAProdotti, totAComponenti)
    res.status(200).send('ok')
}

exports.get = function (req, res) {
    Dashboard.findOne({}, async function (err, dashResult) {
        if (err)
            res.status(200).send({mdate: '16/07/1978'});
        else {
            if (dashResult && moment(dashResult.mdate, 'DD/MM/YYYY').format('DD/MM/YYYY') === moment().format('DD/MM/YYYY')) {
                // ho il record del giorno
                res.status(200).send(dashResult);
            } else {
                // non ho il record del giorno
                const creaDashboard = async () => {
                    const totModelli = await Modello.countFn('aProdotti')
                    const totModelliEstro = await Modello.countFn('aComponenti')
                    const totCategorie = await Categoria.countFn('aProdotti')
                    const totCategorieEstro = await Categoria.countFn('aComponenti')
                    const totComponenti = await Componente.countFn()
                    const totEtichette = await DwhEtichetta.countFn()
                    const totContatti = await Contatto.countFn()
                    const totOfferte = await Offerta.countFn()
                    const totCodEan = { countEan: 0 } // la funzione non torna mai niente di diverso, lo metto hardcoded
                    const totProdotti = await Prodotto.countFn('aProdotti')
                    const totProdottiEstro = await Prodotto.countFn('aComponenti')
                    console.log('totModelli', totModelli, totModelliEstro, totCategorie, totCategorieEstro, totComponenti, totEtichette, totContatti, totOfferte, totProdotti, totProdottiEstro)
    
                    const objectToSave = {
                        countEan:totCodEan.countEan,
                        countModelli:totModelli.countModelli,
                        countModelliEstro:totModelliEstro.countModelli,
                        countCategorie:totCategorie.countCategorie,
                        countCategorieEstro:totCategorieEstro.countCategorie,
                        countCorpi:totComponenti.countCorpi,
                        countLed:totComponenti.countLed,
                        countDriver:totComponenti.countDriver,
                        countComponenti:totComponenti.countComponenti,
                        countBianche:totEtichette.countBianche,
                        countGrigie:totEtichette.countGrigie,
                        countEtichette:totEtichette.countEtichette,
                        countContatti:totContatti.countContatti,
                        countOfferte:totOfferte.countOfferte,
                        countG6:totProdotti.countG6,
                        countG5:totProdotti.countG5,
                        countG4:totProdotti.countG4,
                        countD1:totProdotti.countD1,
                        countProdotti:totProdotti.countProdotti,
                        countG6Estro:totProdottiEstro.countG6,
                        countG5Estro:totProdottiEstro.countG5,
                        countG4Estro:totProdottiEstro.countG4,
                        countD1Estro:totProdottiEstro.countD1,
                        countProdottiEstro:totProdottiEstro.countProdotti,
                        
                    };
    
                    saveFn(objectToSave)
                        .then((result) => {
                            // res.status(200).send(dashResult);
                        })
                        .catch((err) => {
                            // res.status(500).send(err);
                            console.error('ATTENZIONE errore nella creazione della dashboard', err)
                        })
    
                }

                creaDashboard()
                res.status(200).send(dashResult || {});
            }
            
        }
    });

}

const saveFn = (toSave) => {
    return new Promise((resolve, reject) => {
        var dash = new Dashboard(toSave);

        Dashboard.deleteOne({}, function(err, response){
    
            if (err)
                res.status(500).send(err);
            else
    
                dash.save(
                    function (err, result)
                    {
                        if (err)
                            reject(err);
    
                        else
                            resolve(result);
    
                    }
                );
        })
    })
}
exports.save = function (req, res) {
    saveFn(req.body)
        .then((result) => {
            res.status(200).send(result);
        })
        .catch((err) => {
            res.status(500).send(err);
        })
}
