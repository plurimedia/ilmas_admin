var mongoose = require('mongoose'),
Colori = mongoose.model('Colori')

exports.get = function (req, res)
{
    Colori.findById(req.params.id,
        function (err, result)
        {
            if (err)
                res.status(500).send(err);
            else
                res.status(200).send(result)
        })
}

exports.getByCodice = function (req, res)
{
    var search = req.params.codice;

    Colori.findOne({codice:search},
        function (err, result)
        {
            if (err)
                res.status(500).send(err);
            else if (!result)
                res.status(500).send({ error:'Codice non trovato' });
            else
                res.status(200).send(result);
        })
}

exports.list = function (req, res)
{
    Colori.find({},
        function (err, results)
        {
            if (err)
                res.status(500).send(err);
            else
                res.status(200).send(results)
        }
    )
    .sort({ tipo: 1 })
}

exports.save = function (req, res)
{
    var data = req.body;
    var colori = new Colori(data);

    colori.mdate = new Date(),
    colori.last_user = req.user,

    colori.save(function (err, result)
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(result);
    });
}

exports.update = function(req, res)
{
    var modify = req.body;
    modify.mdate = new Date(),
    modify.last_user = req.user;

    Colori.findById(req.params.id,
        function (err, colori)
        {
            colori.update(modify,
            {
                runValidators: true
            },
            function (err)
            {
                if (err)
                    res.status(500).send(err);
                else
                    res.status(200).send(modify);
            })
        });
};

exports.delete = function(req, res)
{
    Colori.remove({
        _id: req.params.id
    },
    function (err)
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).end();
    });
};

exports.cerca = function (req, res)
{
    Colori.find(
        { $or:
            [
                { codice: { $regex: req.query.search, $options: 'i' } },
                { descrizione_cover: { $regex: req.query.search, $options: 'i' } },
                { descrizione_estruso: { $regex: req.query.search, $options: 'i' } }
            ]
        },
        function (err, result)
        {
            if (err)
                res.status(500).send(err);
            else
                res.status(200).send(result);
        });
}

exports.finiture_estro = function (req, res)
{
    Colori.aggregate([
        { $match: { codice: { $exists: true } } },
        { $group: { _id: "$codice" } },
        { $sort: { _id: 1 } }
    ])
    .exec(function (err, results)
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(results);
    })
}
