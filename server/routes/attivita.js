var mongoose = require('mongoose'),
    Attivita = mongoose.model('Attivita'),
    Email = require('../routes/email');


exports.get = function (req, res) 
{
    Attivita.findById(req.params.id, 
        function (err, attivita) 
        {
            if(err)
                res.status(500).send(err);
        res.status(200).send(attivita)
    })
}

exports.list = function (req, res) 
{
    Attivita.find({}, 
        function (err, results) 
        {
            if(err)
                res.status(500).send(err);
            res.status(200).send(results)
        }
    )
    .limit(10)
    .sort({titolo:1})

}

exports.last = function (req, res) 
{
    Attivita.find({}, 
        function (err, results) 
        {
            if(err)
                res.status(500).send(err);
            res.status(200).send(results)
        }
    )
    .limit(10)
    .sort({mdate:-1})
} 
 
exports.save = function (req, res) 
{
    var inviaMailApprovazioneAtivita = false;
    var inviaMailEffortAttivita = false;

    var data = req.body;
    var attivita = new Attivita(data);
        attivita.mdate = new Date(),
        attivita.cdate = new Date(),
        attivita.last_user = req.user,
        attivita.create_user = req.user;

    if (attivita.stato=='chiuso'){
        attivita.data_rilascio = new Date()
    }
    if (!attivita.mailApprovazioneInviata && attivita.approvata){
        attivita.mailApprovazioneInviata = true
        inviaMailApprovazioneAtivita=true;
    }
    if (!attivita.mailEffortInviata && attivita.effort_preventivo){
        attivita.mailEffortInviata = true
        inviaMailEffortAttivita=true;
    }
    
    attivita.save(function (err, result) 
    {
        if (err)
            res.status(500).send(err);
        else{
            if (inviaMailApprovazioneAtivita){
                console.log("invio mail inviaMailApprovazioneAtivita")
                _inviaMailApprovazioneAtivita(attivita);
            }
            if (inviaMailEffortAttivita){
                console.log("invio mail inviaMailEffortAttivita")
                _inviaMailEffortAtivita(attivita);
            }
            res.status(200).send(result);
        }
    });
}

exports.update = function(req, res) 
{
    var inviaMailApprovazioneAtivita = false;
    var inviaMailEffortAttivita = false;

    var modify = req.body;
    modify.mdate = new Date(),
    modify.last_user = req.user;
    if (modify.stato=='chiuso'){
        modify.data_rilascio = new Date()
    }

    if (!modify.mailApprovazioneInviata && modify.approvata){
        modify.mailApprovazioneInviata = true
        inviaMailApprovazioneAtivita = true;
    }
    if (!modify.mailEffortInviata && modify.effort_preventivo){
        modify.mailEffortInviata = true
        inviaMailEffortAttivita=true;
    }

    Attivita.findById(req.params.id, 
        function (err, attivita) 
        {
            attivita.update(modify, 
            {
                runValidators: true
            }, 
            function (err) 
            {
                if (err)
                    res.status(500).send(err);
                else{
                    if (inviaMailApprovazioneAtivita){
                        console.log("invio mail")
                        _inviaMailApprovazioneAtivita(modify);
                    }
                    if (inviaMailEffortAttivita){
                        console.log("invio mail inviaMailEffortAttivita")
                        _inviaMailEffortAtivita(modify);
                    }
                    res.status(200).send(modify);
                }
            })
        }
    );
};

exports.delete = function(req, res)
{
    Attivita.remove({
        _id: req.params.id
    }, 
    function (err) 
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).end();
    });
};
 
exports.count = function (req, res) 
{   
    var query = {
        cdate:
            {
            $gte: new Date(new Date().getFullYear()+'-01-01T01:00:00.000Z') ,
            $lte: new Date(new Date().getFullYear()+'-12-31T22:00:00.000Z')
        }
    }
    
    var result = { 
        countAttivita : 0 
    }

    Attivita.count(query) 
    .exec(function (err, countAttivita)
    {
        if (err)
            res.status(500).send(err);
        else
        {
            result.countAttivita = countAttivita;
            res.status(200).send(result);
        }
    });
}

/* Cerca una categoria in Like*/
exports.cerca = function (req, res) 
{  
    Attivita.find(
        { $or:
            [
                {codice:{$regex: req.query.search, $options: 'i'}}, 
                {titolo:{$regex: req.query.search, $options: 'i'}}, 
                {descrizione:{$regex: req.query.search, $options: 'i'}}, 
                {stato:{$regex: req.query.search, $options: 'i'}}, 
            ]
        }, 
        function (err, result) 
        {
            if (err){ 
                res.status(500).send(err);
            }
            else{
                res.status(200).send(result);
            }
        }
    );
}


_inviaMailApprovazioneAtivita = function (attivita)  
{ 
  
    var emailParams = {
        a : CONFIG.EMAIL_ATTIVITA_APPROVAZIONE_TO,
        cc: CONFIG.EMAIL_ATTIVITA_APPROVAZIONE_CC,
        ccn:[],
        from_email : CONFIG.EMAIL_KITE_FROM,
        from_name : CONFIG.EMAIL_KITE_NAME,
        oggetto:'Approvazione attività ' + attivita.codice ,
        messaggio : "L'attività " + attivita.codice + ' ' + attivita.titolo + " è stata approvata.<br><br>Per conoscere tutti i dettagli puoi collegarti al back-end."
    }; 

    var bodyContent = {
        tipo: 'attivita_approvazione', email: emailParams
    }

    Email.send({body:bodyContent},
        function (err) 
        { 
            console.log("errore invio mail - export approvazione attività" +  err);
        }
    )
}

_inviaMailEffortAtivita = function (attivita)  
{ 
  
    var emailParams = {
        a : CONFIG.EMAIL_ATTIVITA_EFFORT_TO,
        cc: CONFIG.EMAIL_ATTIVITA_EFFORT_CC,
        ccn:[],
        from_email : CONFIG.EMAIL_KITE_FROM,
        from_name : CONFIG.EMAIL_KITE_NAME,
        oggetto:'Effort attività ' + attivita.codice,
        messaggio : "L'attività " + attivita.codice + ' ' + attivita.titolo + " è stata tempificata.<br><br>Per conoscere tutti i dettagli puoi collegarti al back-end."
    }; 

    var bodyContent = {
        tipo: 'attivita_approvazione', email: emailParams
    }

    Email.send({body:bodyContent},
        function (err) 
        { 
            console.log("errore invio mail - effort attività" +  err);
        }
    )
}