var mongoose = require('mongoose'),
    Categoria = mongoose.model('Categoria');


exports.get = function (req, res) 
{
    Categoria.findById(req.params.id, 
        function (err, categoria) 
        {
            if(err)
                res.status(500).send(err);
        res.status(200).send(categoria)
    })
}

exports.list = function (req, res) 
{
    var query = {}
    if (req.query.serie) 
        query.serie = req.query.serie
    else
        query.serie = req.serieIlmas

    Categoria.find(query, 
        function (err, results) 
        {
            if(err)
                res.status(500).send(err);
            res.status(200).send(results)
        }
    ).sort({nome:1})
}

exports.last = function (req, res) 
{
    var query = {}
    if (req.query.serie) 
        query.serie = req.query.serie
    else
        query.serie = req.serieIlmas

    Categoria.find(query, 
        function (err, results) 
        {
            if(err)
                res.status(500).send(err);
            res.status(200).send(results)
        }
    ).sort({mdate:-1})
}


/* salva categoria */
exports.save = function (req, res) 
{
    var data = req.body;
    var categoria = new Categoria(data);
        categoria.mdate = new Date(),
        categoria.last_user = req.user;
    categoria.save(function (err, result) 
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(result);
    });
}

exports.update = function(req, res) 
{
    var modify = req.body;
    modify.mdate = new Date(),
    modify.last_user = req.user;
    Categoria.findById(req.params.id, 
        function (err, categoria) 
        {
            categoria.update(modify, 
            {
                runValidators: true
            }, 
            function (err) 
            {
                if (err)
                    res.status(500).send(err);
                else
                    res.status(200).send(modify);
            })
        }
    );
};

exports.delete = function(req, res)
{
    Categoria.remove({
        _id: req.params.id
    }, 
    function (err) 
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).end();
    });
};
 
exports.countFn = (serie) => {
    return new Promise((resolve, reject) => {
        var result = { 
            countCategorie : 0 
        }
        var query = {}
        query.serie = serie

        Categoria.countDocuments(query) 
        .exec(function (err, countCategorie)
        {
            if (err)
                reject(err);
            else
            {
                result.countCategorie = countCategorie;
                resolve(result);
            }
        });
    })
}

exports.count = function (req, res) 
{
    var result = { 
        countCategorie : 0 
    }
    var query = {}
    if (req.query.serie) 
        query.serie = req.query.serie
    else
        query.serie = req.serieIlmas

    exports.countFn(query.serie).then((result) => {
        res.status(200).send(result);
    }).catch((err) => {
        res.status(500).send(err);
    })
}

/* Cerca una categoria in Like*/
exports.cerca = function (req, res) 
{
    var query = {$and:[{ $or:
        [{nome:{$regex: req.query.nome, $options: 'i'}}, {nome:req.query.nome}]
    }]}

    if (req.query.serie) 
        query.$and.push({serie: req.query.serie})
    else
        query.$and.push({serie: req.serieIlmas})
        
    Categoria.find(
        query, 
        function (err, result) 
        {
            if (err)
                res.status(500).send(err);
            else
                res.status(200).send(result);
        }
    );
}