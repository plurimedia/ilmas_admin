var fs = require('fs'),
    csv = require("fast-csv"),
    _ = require('underscore'),
    async = require('async'),
    mongoose = require('mongoose'),
    Componente = mongoose.model('Componente'),
      
    componenti = [],
     
    _creaRecord = function (data) { // normalizza i dati per un import da excel ilmas
         
        componente = {
            // categoria e modello li metto dopo perchè sono refs
            mdate: new Date(),
            pubblicato: true,
            codice: data['Codice'],
            nome: data['nome'],
            tipo: data['tipo']
        }; 
       
        return componente;
    }, 

    _keyWords = function (p) { 

        var result = [p.codice,p.codice.toLowerCase()], // metti codice e codice lowercase
        _string = function (s) { // tutte le parole di una stringa senza il trattino, upper e lower
             var keywords = _.filter(s.split(' '),function(k){return k!=='-'});
            return [
                    keywords,
                    _.collect(keywords,function(k){return k.toLowerCase()}),
                    _.collect(keywords,function(k){return k.toUpperCase()})
            ]
        },
        _value = function (v,u){
            return [v+u,v+u.toUpperCase(),v+u.substr(0,1).toUpperCase()+u.substr(1)];
        },
        _keywordsCodice = function (codice) { // metto anche il codice in pezzi (ricerca per parti di codice)
             
            
           var c1 = p.codice.substr(0,3),
            c2 = p.codice.substr(0,7),
            c3 = p.codice.substr(0,8),
            c4 = p.codice.substr(0,9);

            return _.compact([c1,c2,c3,c4]);
             
        }; 
 
         result.push(_string(p.nome)); 

         result.push(_keywordsCodice(p.codice))

        return _.unique(_.flatten(result));
    },


    _parseComponentiCsv = function (csvpath) {

        var componenti = [], 
            stream = fs.createReadStream(csvpath);

        // dopodichè parso e salvo
        csv.fromStream(stream, {
                headers: true,
                delimiter: ',',
                discardUnmappedColumns: true
            })
            .on("data", function (data) { 
              componenti.push(_creaRecord(data)); 
            })
            .on("end", function () {

            // preparo il bulk di inserimento dei prodotti
            var bulk = Componente.collection.initializeOrderedBulkOp();
            async.each(componenti, function (p, cb) {
                p.keywords= _keyWords(p)
                bulk.insert(p);
                cb();
            }, function (err) {
                 bulk.execute(function (err, resp) {
                    console.log('5) inseriti ' + resp.nInserted + ' prodotti');
                      
                    })
                })
            }) 
    };

exports.import = function (req, res) {
      //_parseComponentiCsv("server/routes/corpo.csv")
}