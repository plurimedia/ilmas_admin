var mongoose = require('mongoose'),
     DwhSchedaTecnica = mongoose.model('DwhSchedaTecnica'),
     _ = require('underscore');

exports.dwhSchedeTecniche = function (req, res) 
{
    var annoReport = req.params.anno; 
    DwhSchedaTecnica.aggregate( 
        [ 
            {
                $match: {
                    anno: annoReport
                }
            },
            {$group: {
                _id: {
                    "codice": "$codice" ,
                    "totaleAnno": "$totaleAnno"
                 } 
            }},
            { $sort : { totaleAnno : -1} }
        ],
       function (err, result) 
       {
            if (err)
                res.status(500).send(err);
            else
            { 
                result =  _.uniq(result, false, function(p)
                { 
                    return p._id.codice; 
                });
                result = _.clone(_.map(result, function(p)
                { 
                    return p._id; 
                }))
                var totale = 0;
                result.forEach( 
                    function(p){ 
                        totale = totale + p.totaleAnno
                    }
                )
                result.forEach( 
                    function(p)
                    { 
                        p.percent = (p.totaleAnno * 100 / totale).toFixed(2)

                    }
                )
                result = _.clone(_.sortBy(result, 'totaleAnno').reverse()); 
                result =  _.first(result, [20]) 
                var resultToSend =
                {
                    result:result,
                    totaleRicerche:totale
                }
                res.status(200).send(resultToSend);
            }
        }
    )
} 

