var mongoose = require('mongoose'),
	Settings = mongoose.model('Settings');


exports.save = function (req, res) {
	var data = req.body;
		data.mdate = new Date();

	var setting = new Settings(data);

	// cancello sempre e ricreo

	Settings.remove({}, function (err){
		if(err)
			res.status(500).send(err);
		setting.save(function (err, result) {
			if (err)
				res.status(500).send(err);
			else
				res.status(200).send(result);
		});

	})
}

exports.get = function (req, res) {
	Settings.findOne(function (err, result) {
		if (err)
			res.status(500).send(err);
		else
			res.status(200).send(result);
	});
}