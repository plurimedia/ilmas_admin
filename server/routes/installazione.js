var mongoose = require('mongoose'),
    Installazione = mongoose.model('Installazione');

exports.get = function (req, res) {
    Installazione.findOne({_id:req.params.id})
    .populate('progetti_simili')
     .exec(function (err, installazione)
    {
        if (err)
            res.status(500).send(err);
        else
        {
            res.status(200).send(installazione)
        }
    })
}

 

exports.last = function (req, res) 
{

    Installazione.find({})
    .populate('progetti_simili')
    .sort({mdate:-1})
    .limit(40)
    .exec(function (err, results)
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(results)
        
    })
    //.sort({mdate:-1})
}
 
exports.save = function (req, res) 
{
    console.log('exports.save')
    var data = req.body;
    var installazione = new Installazione(data);
        installazione.mdate = new Date(),
        installazione.last_user = req.user;

        console.log("installazione ", installazione)
    installazione.save(function (err, result) 
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(result);
    });
}

exports.update = function(req, res) 
{
    console.log('exports.update')
    var modify = req.body;
    modify.mdate = new Date(),
    modify.last_user = req.user;
    console.log("installazione ", modify)
    Installazione.findById(req.params.id, 
        function (err, installazione) 
        {
            installazione.update(modify, 
            {
                runValidators: true
            }, 
            function (err) 
            {
                if (err)
                    res.status(500).send(err);
                else
                    res.status(200).send(modify);
            })
        }
    );
};

exports.delete = function(req, res)
{
    console.log('exports.delete')
    Installazione.remove({
        _id: req.params.id
    }, 
    function (err) 
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).end();
    });
};
  
exports.cerca = function (req, res) 
{   
    Installazione.find({ $or:
        [
            {descrizione_it:{$regex: req.query.search, $options: 'i'}}, 
            {descrizione_en:{$regex: req.query.search, $options: 'i'}},
            {prodotti_it:{$regex: req.query.search, $options: 'i'}}, 
            {prodotti_en:{$regex: req.query.search, $options: 'i'}}
        ]
    })
    .populate('progetti_simili')
        .exec(function (err, installazione)
    {
        if (err)
            res.status(500).send(err);
        else
        {
            res.status(200).send(installazione)
        }
    })
}


// caricamento massivo delle foto
exports.upload = function (req, res) {
    var immaginiInstallazione = []
    // mi arriva un array di files da caricare
    var files = req.files.file,
        i = 0;

    async.each(files,
        function (file, callback) {

            cloudinary.uploader.upload(file.path, function (result) {
                if (!result)
                    res.status(500).end();

                var immagine = new Immagine({
                    nome: file.name.split('|')[0],
                    tipo: file.name.split('|')[1],
                    id: result.public_id
                })
                immaginiInstallazione.push(immagine)
                callback();
            });
        },
        // finite tutte
        function (err) {
            console.log("immaginiInstallazione ", immaginiInstallazione)
            res.status(200).send(immaginiInstallazione);
        }
    );
}