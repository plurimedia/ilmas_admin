var mongoose = require('mongoose'),
Evento = mongoose.model('Evento');
cloudinary = require('cloudinary'),
async = require('async');
Immagine = mongoose.model('Immagine'),

cloudinary.config({
    cloud_name: CONFIG.CLOUDINARY_CLOUD_NAME,
    api_key: CONFIG.CLOUDINARY_APY_KEY,
    api_secret: CONFIG.CLOUDINARY_API_SECRET
});

exports.last = function (req, res) 
{
    Evento.find({}, 
        function (err, results) 
        {
            if(err)
                res.status(500).send(err);

            res.status(200).send(results)
        }
    ).sort({mdate:-1})
}

exports.get = function (req, res) 
{
    Evento.findById(req.params.id, 
        function (err, evento) 
        {
            if(err)
                res.status(500).send(err);
        res.status(200).send(evento)
    })
}

exports.save = function (req, res) 
{
    var data = req.body;
    var evento = new Evento(data);
        evento.mdate = new Date(),
        evento.last_user = req.user;
       
    evento.save(function (err, result) 
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(result);
    });
}

exports.update = function(req, res) 
{
    var modify = req.body;
    modify.mdate = new Date(),
    modify.last_user = req.user;
    Evento.findById(req.params.id, 
        function (err, evento) 
        {
            evento.update(modify, 
            {
                runValidators: true
            }, 
            function (err) 
            {
                if (err)
                    res.status(500).send(err);
                else
                    res.status(200).send(modify);
            })
        }
    );
};

exports.delete = function(req, res)
{
    Evento.remove({
        _id: req.params.id
    }, 
    function (err) 
    {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).end();
    });
};

// caricamento massivo delle foto
exports.upload = function (req, res) {
    
    var immaginiEvento = []
    // mi arriva un array di files da caricare
    var files = req.files.file,
        i = 0;

    async.each(files,
        function (file, callback) {

            cloudinary.uploader.upload(file.path, function (result) {
                if (!result)
                    res.status(500).end();

                var immagine = new Immagine({
                    nome: file.name.split('|')[0],
                    tipo: file.name.split('|')[1],
                    id: result.public_id
                })
                immaginiEvento.push(immagine)
                callback();

                /*
                immagine.save(function (err, result) {
                    if (err)
                        res.status(500).send(err)

                    // immagine caricata su cloundinary e salvato il riferimento sul db
                    fs.unlinkSync(file.path); // cancello da .tmp
                    i++;
                    res.write(i.toString())
                    callback();
                })
                */
            });
        },
        // finite tutte
        function (err) {
            console.log("immaginiEvento ", immaginiEvento)
            res.status(200).send(immaginiEvento);
        }
    );
}