var mongoose = require('mongoose'),
	Offerta = mongoose.model('Offerta');


/* ultime offerte aggiornati */
exports.last = function (req, res) 
{
	var query = {};
	var isAgente = false;
	try{
		isAgente = req.user && req.user.app_metadata.roles && req.user.app_metadata.roles.indexOf('agente') !== -1 ? true : false;
	}
	catch(e){}
	if (isAgente==true)
		query = {user_id:req.user.sub};//gli agenti vedono solo le proprie offerte
	Offerta.find(query, function (err, result) {
		if (err)
			res.status(500).send(err);
		else
			res.status(200).send(result);
	}).limit(10).sort({mdate: -1})
}

exports.save = function (req, res)
{
	var data = req.body;
	data.user_id = req.user.sub;
	if(data.sconto === 0)
		delete data.sconto; // lo tolgo
	var date = new Date();
	var offerta = new Offerta(data);
		offerta.mdate = date;
		offerta.numero = date.valueOf(); // numero === timestamp
		//data inserimento record
		offerta.idate = date;
	offerta.save(
		function (err, result) 
		{
			if (err)
				res.status(500).send(err);
				
			else 
				res.status(200).send(result);
			
		}
	);
}

exports.update = function(req, res) 
{
	var modify = req.body;
	modify.mdate = new Date();
	//utente che ha effettuato l'ultima modifica
	modify.m_user_id = req.user.sub;
	if(modify.sconto ===0)
		delete modify.sconto;
	Offerta.findById(req.params.id, 
		function (err, offerta) 
		{
			offerta.update(modify, 
			{
				runValidators: true
			}, 
			function (err) 
			{
				if (err) 
					res.status(500).send(err);
				 else 
					res.status(200).send(modify);
			})
		}
	);
};

exports.delete = function(req, res) 
{
	Offerta.remove({
		_id: req.params.id
	}, 
	function (err) {
		if (err) 
			res.status(500).send(err);
		 else 
			res.status(200).end();
	});
};

/* cerca offerte */
exports.search = function (req, res) 
{
	var search = req.query.search,
		query = {
				$or:[
						{ numero: {$regex: search,$options: 'i'}},
					  	{ destinatario_nome: {$regex: search,$options: 'i'}},
					  	{ destinatario_azienda: {$regex: search,$options: 'i'}}
					 ]
			};
	var isAgente = req.query.isAgente;
	try{
		isAgente = req.user && req.user.app_metadata.roles && req.user.app_metadata.roles.indexOf('agente') !== -1 ? true : false;
	}
	catch(e){}

	if (isAgente==true)
		query = {
				$or:[
						{ numero: {$regex: search,$options: 'i'}},
					  	{ destinatario_nome: {$regex: search,$options: 'i'}},
					  	{ destinatario_azienda: {$regex: search,$options: 'i'}}
					 ],
				$and:[
						{ user_id:req.user.sub} //gli agenti vedono solo le proprie offerte
  					 ]
			};
	Offerta.find(query, 
		function (err, results) 
		{
			if (err)
				res.status(500).send(err);
			else
				res.status(200).send(results);
		}
	)
}

exports.countFn = () => {
	return new Promise((resolve, reject) => {
		var result = { countOfferte : 0 }
    Offerta.count() 
    .exec(function (err, countOfferte)
    {
        if (err)
            reject(err);
        else
        {
            result.countOfferte = countOfferte;
            resolve(result);
        }
    });
	})
}
exports.count = function (req, res) 
{   
	exports.countFn().then((result) => {
		res.status(200).send(result);
	}).catch((err) => {
		res.status(500).send(err);
	})
}