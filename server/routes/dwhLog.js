var mongoose = require('mongoose'),
	DwhLog = mongoose.model('DwhLog');
 
exports.save = function (req, res)
{
	var data = req.body,
	 	dateIns = new Date(),
		dwhLog = new DwhLog(data);
	dwhLog.anno = dateIns.getFullYear(),
    dwhLog.mese = dateIns.getMonth()+1,
    dwhLog.giorno = dateIns.getDate();
	dwhLog.save(function (err, result) 
	{
		if (err)
			res.status(500).send(err); 
		else 
			res.status(200).send(result);
	});
} 


exports.last = function (req, res) 
{
	var query = {};  
	DwhLog.find(query, 
		function (err, result) {
			if (err)
				res.status(500).send(err);
			else
				res.status(200).send(result);
	}).limit(10).sort({idate:-1})
}
