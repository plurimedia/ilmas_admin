var CONFIG = require(process.cwd() + '/server/config.js'),
    cloudinary = require('cloudinary'),
    mongoose = require('mongoose'),
    Immagine = mongoose.model('Immagine'),
    Prodotto = mongoose.model('Prodotto'),
    Categoria = mongoose.model('Categoria'),
    Modello = mongoose.model('Modello'),
    LineaCommerciale = mongoose.model('LineaCommerciale'),
    fs = require('fs'),
    csv = require("fast-csv"),
    _ = require('underscore'),
    async = require('async');


cloudinary.config({
    cloud_name: CONFIG.CLOUDINARY_CLOUD_NAME,
    api_key: CONFIG.CLOUDINARY_APY_KEY,
    api_secret: CONFIG.CLOUDINARY_API_SECRET
});

// modifica (nome)
exports.update = function (req, res) {

    var modify = req.body;
    modify.mdate = new Date();

    Immagine.findById(req.params.id, function (err, immagine) {
        immagine.update(modify, {
            runValidators: true
        }, function (err) {
            if (err) {
                res.status(500).send(err);
            } else {
                res.status(200).send(modify);
            }
        })
    });
};


exports.delete = function (req, res) {

    var idCloudinary = req.query.idcloudinary,
        idImmagine = req.query.idimmagine,
        tipo = req.query.tipo,
        prodottimodificati = [],
        modellimodificati = [],
        categoriemodificate = [];

    // tolgo da cloundinary
    cloudinary.uploader.destroy(idCloudinary, function (result) {

        if (!result)
            res.status(500).end()

        // poi cancello da db 
        Immagine.remove({
            _id: idImmagine
        }, function (err) {
            if (err) {
                res.status(500).send(err);
            } else {

                var query = {};
                query[tipo] = idCloudinary; // disegno foto o fotometria
                var updatequery = {
                    $unset: {}
                };
                updatequery.$unset[tipo] = ''; // foto, fotometria o disegno

                async.parallel([
                        // controllo prodotti
                        function (cb) {
                        Prodotto.find(query, function (err, result) {
                            if (result.length) {
                                var bulk = Prodotto.collection.initializeOrderedBulkOp();
                                prodottimodificati = _.pluck(result, 'codice');
                                async.each(_.pluck(result, '_id'),
                                    function (id, cb) {

                                        bulk.find({
                                            _id: id
                                        }).update(updatequery);
                                        cb(); // del bulk

                                    },
                                    function (err) {
                                        bulk.execute(function (err, resp) {
                                            cb(); // del parallel
                                        })
                                    })
                            } else {
                                cb(); // non ho niente da aggiornare
                            }
                        })

                        },
                        // controllo modelli
                        function (cb) {
                        Modello.find(query, function (err, result) {

                            if (result.length) {
                                var bulk = Modello.collection.initializeOrderedBulkOp();
                                modellimodificati = _.pluck(result, 'nome');

                                async.each(_.pluck(result, '_id'),
                                    function (id, cb) {

                                        bulk.find({
                                            _id: id
                                        }).update(updatequery);
                                        cb(); // del bulk

                                    },
                                    function (err) {
                                        bulk.execute(function (err, resp) {
                                            cb(); // del parallel
                                        })
                                    })
                            } else {
                                cb();
                            }
                        })
                        },
                        // controllo categorie
                        function (cb) {
                        Categoria.find(query, function (err, result) {

                            if (result.length) {
                                var bulk = Categoria.collection.initializeOrderedBulkOp();
                                categoriemodificate = _.pluck(result, 'nome');

                                async.each(_.pluck(result, '_id'),
                                    function (id, cb) {

                                        bulk.find({
                                            _id: id
                                        }).update(updatequery);
                                        cb(); // del bulk

                                    },
                                    function (err) {
                                        bulk.execute(function (err, resp) {
                                            cb(); // del parallel
                                        })
                                    })
                            } else {
                                cb();
                            }

                        })
                        }
                    ], function (err) {
                    // finito di controllare tutto
                    var modifiche = _.map(_.compact(_.flatten(_.union(prodottimodificati, modellimodificati, categoriemodificate))), function (m) {
                        var row = {};
                        row['Codice o nome'] = m;
                        return row;
                    });

                     csv
                        .writeToPath(".tmp/imageDeleted.csv", modifiche, {
                            headers: true
                        })
                        .on("finish", function () {
                            res.status(200).send({
                                modified: {
                                    count: modifiche.length,
                                    log: 'imageDeleted.csv'
                                }
                            });
                        });
                })

            }
        });
    });
}

/* ultime immagini aggiornate */
exports.last = function (req, res) {
     Immagine.find({}, function (err, result) {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(result);
    }).limit(20).sort({
        mdate: -1
    })
}

/* ultime immagini aggiornate per tipo */
exports.lastTipo = function (req, res) {
    Immagine.find({
        tipo: req.params.tipo
    }, function (err, result) {
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(result);
    }).limit(10).sort({
        mdate: -1
    })
}


// caricamento massivo delle foto
exports.upload = function (req, res) {

    // mi arriva un array di files da caricare
    var files = req.files.file,
        i = 0;


    async.each(files,
        function (file, callback) {

            cloudinary.uploader.upload(file.path, function (result) {
                if (!result)
                    res.status(500).end();

                var immagine = new Immagine({
                    nome: file.name.split('|')[0],
                    tipo: file.name.split('|')[1],
                    id: result.public_id
                })

                immagine.save(function (err, result) {
                    if (err)
                        res.status(500).send(err)

                    // immagine caricata su cloundinary e salvato il riferimento sul db
                    fs.unlinkSync(file.path); // cancello da .tmp
                    i++;
                    res.write(i.toString())
                    callback();
                })
            });
        },
        // finite tutte
        function (err) {
            res.status(200).end()
        }
    );
}

exports.search = function (req, res) {

    var tipo = req.body.cerca.tipo,
        nome = req.body.cerca.nome,
        query = {};

    if (tipo && !nome)
        query = {
            tipo: tipo
        };
    if (!tipo && nome)
        query = {
            nome: {
                $regex: nome,
                $options: "$i"
            }
        };
    if (tipo && nome)
        query = {
            $and: [{
                nome: {
                    $regex: nome,
                    $options: "$i"
                }
            }, {
                tipo: tipo
            }]
        };

    Immagine.find(query, function (err, results) {

        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(results);
    }).limit(20)
}

/* aggiornamento in blocco di un'immagine su categorie, modelli e codici */
exports.batchupdate = function (req, res) {

    var cosa = req.body.cosa,
        codici = req.body.codici,
        foto = req.body.foto,
        tipo = req.body.tipo,
        date = new Date(),
        set = {
            mdate: date
        };
    set[tipo] = foto;

console.log("cosa",cosa);
console.log("codici",codici);
console.log("foto",foto);
console.log("tipo",tipo);
console.log("date",date);

    if (cosa === 'categoria' || cosa === 'categoriaEstro') { // aggiorno la foto di una categoria
        Categoria.findOneAndUpdate({
            _id: codici[0]
        }, {
            $set: set
        }, function (err, cat) {
            if (err)
                res.status(500).send(err)

            res.status(200).end()
        })
    }
    else if (cosa === 'modello' || cosa === 'modelloEstro') { // aggiorno la foto di un modello
        Modello.findOneAndUpdate({
            _id: codici[0]
        }, {
            $set: set
        }, function (err, modello) {
            if (err)
                res.status(500).send(err)

            res.status(200).end()
        })
    }
    else if (cosa === 'codici' || cosa === 'modulo' || cosa === 'moduloEstro' || cosa === 'colore') { // aggiorno le foto di n-codici prodotto    

        async.each(codici, function (codice, cb) {

            Prodotto.findOneAndUpdate({
                _id: codice
            }, {
                $set: set
            }, function (err, cat) {
                if (err)
                    res.status(500).send(err)
                else
                    cb();
            })
        }, function (err) { // finito di aggiornare tutti i codici
            if (err)
                res.status(500).send(err)
         
            res.status(200).end()
        })
    }
    else if (cosa === 'modellocodici' || cosa === 'modellocodiciEstro') { // aggiorno la foto di tutti i codici di un modello (disegno e foto)


        Prodotto.find({
            modello: codici[0] 

        }, '_id codice', function (err, prodotti) {
         
            console.log('2) aggiorno i codici del modello', codici[0]);
            console.log('2) codici da aggiornare', _.pluck(prodotti, 'codice'));

            async.each(_.pluck(prodotti, '_id'), function (codice, cb) {
                Prodotto.findOneAndUpdate({
                    _id: codice
                }, {
                    $set: set
                }, function (err, cat) {
                    if (err)
                        res.status(500).send(err)
                    else
                        cb();
                })
            }, function (err) { // finito di aggiornare tutti i codici
                if (err)
                    res.status(500).send(err)

                res.status(200).end();
            })
        })
    }
    else if (cosa === 'lineaCommerciale') { // aggiorno la foto di una lineaCommerciale
        set = {
            mdate: date,
            foto: foto
        };
        LineaCommerciale.findOneAndUpdate({
            _id: codici[0]
        }, {
            $set: set
        }, function (err, cat) {
            if (err)
                res.status(500).send(err)

            res.status(200).end()
        })
    }
};

exports.get = function (req, res) {
    Immagine.findOne({id:req.params.id})
    .exec(function (err, immagine)
    { 
        if (err)
            res.status(500).send(err);
        else
            res.status(200).send(immagine)
    }
       
    );
}