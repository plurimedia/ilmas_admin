var mongoose = require('mongoose'),
  ObjectId = require('mongoose').Types.ObjectId,
  Prodotto = mongoose.model('Prodotto'),
  CodiceEan = mongoose.model('CodiceEan'),
  Modello = mongoose.model('Modello'),
  Sottocategoria = mongoose.model('Sottocategoria'),
  Utils = require('../routes/utils'),
  _ = require('underscore'),
  axios = require('axios'),
  async = require('async'),
  fs = require("fs"),
  csv = require("fast-csv"),
  numeral = require('numeral'),
  numeral_it = require('numeral/languages/it'),
  Email = require('../routes/email'),
  modello_route = require('../routes/modello'),
  CONFIG = require(process.cwd() + '/server/config.js');

_dataDecorrenza = function () {
  var dataDecorrenza = new Date();
  var giorno = dataDecorrenza.getDate();
  var mese = dataDecorrenza.getMonth() + 1;
  var anno = dataDecorrenza.getFullYear();
  return giorno + "/" + mese + "/" + anno;
}

_formatterField = function (value, formatter, type) {
  result = '';
  if (value) {
    if (type == 'first')
      result = formatter + '' + value + ' ';
    else
      result = value + '' + formatter + ' ';
  }
  return result;
}

_manageQueryProdotto = function (query, generazione, search, _idProdotto) {
  var queryResult = _.clone(query)

  if (search) queryResult = { keywords: { $all: search } };

  if (generazione) {
    var filterGen = { $in: [Utils.prefixGen() + generazione] };

    if (generazione == Utils.ultimaGenerazione())
      filterGen = { $in: Utils.keyUltimeGenerazioni({ prefix: 'true' }) };
    else if (generazione == Utils.archivioGenerazione())
      filterGen = { $in: Utils.keyArchivioGenerazioni({ prefix: 'true' }) };

    if (search)
      _.extend(queryResult, { $and: [{ keywords: filterGen }] });
    else
      _.extend(queryResult, { keywords: filterGen });
  }

  if (_idProdotto) _.extend(queryResult, { _id: _idProdotto });

  return queryResult;
}

/* cerca sottocategorie nei prodotti */
exports.sottocategorie = function (req, res) {
  var cerca = req.query.nome;
  /* li trovo tutti e poi cerco il match sui risultati*/
  Sottocategoria.distinct('nome', function (err, results) {
    if (err)
      res.status(500).send(err);

    /* cerco */
    var sottocategoriaFound = _.filter(results, function (sottocategoria) {
      var reg = new RegExp(cerca, 'i')
      return sottocategoria.match(reg)
    });
    res.send(_.map(sottocategoriaFound, function (s) { return { nome: s } }))
  })
}

/* ultimi prodotti aggiornati */
exports.last = function (req, res) {
  console.log('exports.last', req.query, req.params)
  var query = {}, search = [];

  if (req.query.serie)
    search.push(req.query.serie)
  else
    search.push(req.serieIlmas)

  query = _manageQueryProdotto(query, req.params.generazione, search, null);

  Prodotto.find(query)
    .populate('modello', { 'nome': 1, 'peso': 1, 'volume': 1, 'ecolamp': 1 })
    .populate('categoria', 'nome')
    .limit(10)
    .sort({ mdate: -1 })
    .exec(function (err, prodotti) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(prodotti);
    });
}

function postProdotto(obj, serie, id) {
  console.log('KITE: postProdotto', serie)
  return new Promise(function (resolve, reject) {
    var importUrl = CONFIG.URL_KYTE_DISTINTA;
    if (serie === 'aProdotti') {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + obj.token
      };

      if (obj && obj.tracciatoRecordKITE && obj.tracciatoRecordKITE.componentiDistintaBase)
        delete obj.tracciatoRecordKITE.componentiDistintaBase;

      var emailParams = {
        a: ['lorenzo.galotti@plurimedia.it'],
        cc: ['tech@plurimedia.it'],
        ccn: [],
        from_email: 'tech@plurimedia.it',
        from_name: 'Lorenzo',
        oggetto: 'Verifica invio kite ' + obj.prodotto.codice,
        messaggio: 'Sto inviando a kite il seguente prodotto: ' + obj.prodotto.codice + ' con token: ' + obj.token + ' a URL: ' + importUrl
      };

      var bodyContent = { tipo: 'attivita_approvazione', email: emailParams }

      Email.send({ body: bodyContent })

      axios.post(importUrl, {
        "type": "prodotto", // se qui faccio degli errori il sistema non mi dice nulla di strano
        "format": "json",
        "value": { "prodotto": obj.prodotto }
      }, { headers: headers })
        .then(function (response) {
          console.log('KITE fine post prodotto data', response.data)
          if (response.data.result_msg === 'OK') {
            console.log('KITE update prodotto', id);
            // Prodotto.updateOne({ _id: ObjectId(id) }, { da_inviare_kite: 0 });
            Prodotto.updateOne({ _id: id }, { da_inviare_kite: 0 }, function (err, pr) {
              if (err) console.log(err)
              else console.log("Updated: ", pr);
            });
            // Prodotto.updateOne({ _id: id }, { $set: { da_inviare_kite: 0 }});
            resolve();
          }
          else
            console.log('KITE post prodotto error 1', response.data.result_msg, response.data.msg)
        })
        .catch(function (error) {
          console.log('KITE post prodotto error 2', error);
          reject(error);
        });
    }
    else
      resolve()
  });
}

function generaDistintaBaseProdotto(prodotto) {
  console.log('DENTRO generadistintabaseprodotto');
  return new Promise(function (resolve, reject) {
    Modello.findById(prodotto.modello,
      function (err, modello) {
        console.log('genera distinta, trovato modello', modello, err);
        if (modello.configDistintaBase && modello.configDistintaBase.length &&
          !prodotto.kit && !prodotto.criptato && !prodotto.speciale) {
          console.log('DENTRO if riga 190', modello, modello.configDistintaBase, [prodotto])
          modello_route.creaDistintaBase(modello, modello.configDistintaBase, [prodotto])
            //.then(modello_route.inviaDistintaKyte)
            .then(function (err, result) {
              if (err) console.error(err);
              //res.status(500).send(err);
              console.log('prima del find IF: ', result);
              Prodotto.find({ _id: prodotto._id }) //populate copiate dalla get, magari non servono tutte
                .populate('modello', { 'nome': 1, 'peso': 1, 'volume': 1, 'ecolamp': 1, 'conto_contabile_acquisti': 1, 'conto_contabile_vendite': 1, 'modello_gm': 1, 'provvigioni': 1 })
                .populate('modello_cript', { 'nome': 1 })
                .populate('categoria', { 'nome': 1, 'voce_doganale': 1, 'linea_prodotto': 1 })
                .populate(
                  {
                    path: 'componenti._id',
                    model: 'Componente',
                    populate: { path: 'componenti_figlio._id', model: 'Componente', }
                  }
                )

                .exec(function (err, prodotto_kyte) {
                  console.log('Dentro exec IF', prodotto_kyte[0], err);
                  if (err) console.error(err);

                  var object = _.clone(_manageDatiVSKITE(prodotto_kyte[0]));

                  Utils.getToken()
                    .then(function (tok) { console.log('dentro get token IF'); return postProdotto({ token: tok, prodotto: object }, prodotto_kyte[0].serie, prodotto._id); })
                    .then(function () { console.log('dentro get token secondo THEN'); resolve(); })
                    .catch(function () {
                      console.log('dentro get token CATCH IF');
                      modello_route.inviaMailDistinta(null, prodotto.codice, [{ line: 'Generazione distinta prodotto : OK!' }, { line: 'Invio dati al KITE : Errore!' }]);
                      reject();
                    });

                });

              //res.status(200).send('distinta creata correttamente');
            })
            .catch(function (err) {
              modello_route.inviaMailDistinta(null, prodotto.codice, [{ line: 'Generazione distinta prodotto : OK!' }, { line: 'Invio dati al KITE : Errore!' }]);
              reject();

              console.log('dopo creazione distinta con errore', err);
            })
        }
        else {
          console.log('prima del find ELSE: ');
          Prodotto.find({ _id: prodotto._id }) //populate copiate dalla get, magari non servono tutte
            .populate('modello', { 'nome': 1, 'peso': 1, 'volume': 1, 'ecolamp': 1, 'conto_contabile_acquisti': 1, 'conto_contabile_vendite': 1, 'modello_gm': 1, 'provvigioni': 1 })
            .populate('modello_cript', { 'nome': 1 })
            .populate('categoria', { 'nome': 1, 'voce_doganale': 1, 'linea_prodotto': 1 })
            .populate({
              path: 'componenti._id',
              model: 'Componente',
              populate: { path: 'componenti_figlio._id', model: 'Componente' }
            })
            .exec(function (err, prodotto_kyte) {
              console.log('dentro EXEC else', prodotto_kyte[0]);
              if (err) console.error(err);

              var object = _.clone(_manageDatiVSKITE(prodotto_kyte[0]));

              Utils.getToken()
                .then(function (tok) { console.log('dentro get token ELSE'); return postProdotto({ token: tok, prodotto: object }, prodotto_kyte[0].serie, prodotto._id); })
                .then(function () { console.log('dentro get token secondo THEN'); resolve(); })
                .catch(function () {
                  console.log('dentro get token secondo CATCH');
                  modello_route.inviaMailDistinta(null, prodotto.codice, [{ line: 'Generazione distinta prodotto : OK!' }, { line: 'Invio dati al KITE : Errore!' }]);
                  reject();
                });
            });
        }
      });
  })
}

exports.save = function (req, res) {
  console.log('SALVATAGGIO IN CORSO');
  var data = req.body;
  var prodotto = new Prodotto(data);

  prodotto.mdate = new Date();
  prodotto.last_user = req.user;
  prodotto.codice = _.clone(prodotto.codice.toUpperCase().trim());
  if (prodotto.serie === 'aProdotti') prodotto.da_inviare_kite = 1;

  prodotto.save(function (err, result) {
    if (err) {
      res.status(500).send(err);
      return;
    }

    generaDistintaBaseProdotto(prodotto)
      .then(function () {
        console.log('THEN generaDistintaBaseProdotto save');
        //possiamo lasciarla asincrona
      }, function (err) {
        console.log('errore??', err);
      });

    //salvo la sottocategoria in una tabella esterna,
    //per evitare di interrogare la TAB prodotti quando ho bisogno delle sottocategorie
    console.log('PRODOTTO SOTTOCATEGORIA: ', prodotto.sottocategoria);
    if (prodotto.sottocategoria) {
      var sottocategoria =
      {
        mdate: new Date(),
        nome: prodotto.sottocategoria
      }
      Sottocategoria.findOne({ nome: prodotto.sottocategoria },
        function (err, foundSottocategoria) {
          if (foundSottocategoria) {
            foundSottocategoria.update(sottocategoria,
              function (err) {
                if (err)
                  res.status(500).send(err);
                else
                  res.status(200).send(result);
              })
          }
          else {
            foundSottocategoria = new Sottocategoria(sottocategoria);
            foundSottocategoria.save(function (err, result) {
              if (err)
                res.status(500).send(err);
              else
                res.status(200).send(result);
            })
          }
        })
    }
    else
      res.status(200).send(result);
  });
}

exports.update = function (req, res) {
  var modify = req.body;
  modify.mdate = new Date();
  modify.last_user = req.user;
  modify.codice = _.clone(modify.codice.toUpperCase().trim());
  if (modify.serie === 'aProdotti') modify.da_inviare_kite = 1;

  Prodotto.findById(req.params.id, function (err, prodotto) {
    prodotto.update(modify, { runValidators: true },
      function (err) {
        if (err) {
          console.log("1 err ", err)
          res.status(500).send(err);
          return;
        }

        generaDistintaBaseProdotto(prodotto)
          .then(function () {
            console.log('THEN generaDistintaBaseProdotto update');
            //possiamo lasciarla asincrona
          }, function (err) {
            console.log('errore??', err);
          });
        //console.log('prima della mia nuova funzione')
        //generaDistintaBaseProdotto(prodotto)
        //    .then(function() {
        //salvo la sottocategoria in una tabella esterna,
        //per evitare di interrogare la TAB prodotti quando ho bisogno delle sottocategorie
        if (modify.sottocategoria) {
          var sottocategoria = { mdate: new Date(), nome: modify.sottocategoria }

          Sottocategoria.findOne({ nome: modify.sottocategoria },
            function (err, foundSottocategoria) {
              if (foundSottocategoria) {
                foundSottocategoria.update(sottocategoria,
                  function (err) {
                    if (err) {
                      console.log("err ", err)
                      res.status(500).send(err);
                    }
                    else
                      res.status(200).send(modify);
                  })
              }
              else {
                foundSottocategoria = new Sottocategoria(sottocategoria);
                foundSottocategoria.save(function (err, result) {
                  if (err) {
                    console.log("err ", err)
                    res.status(500).send(err);
                  }
                  else
                    res.status(200).send(modify);
                })
              }
            })
        }
        else
          res.status(200).send(modify);
        //    });
        //console.log('dopo la mia nuova funzione')
      })
  })
}

exports.updateImage = function (req, res) {
  var prodotto = req.body.prodotto,
    foto = req.body.foto,
    tipo = req.body.tipo,
    date = new Date(),
    set = { mdate: date };

  set[tipo] = foto;

  Prodotto.findOneAndUpdate({ _id: prodotto }, { $set: set },
    function (err, cat) {
      if (err)
        res.status(500).send(err)
      else
        res.status(200).end();
    })
};

exports.delete = function (req, res) {
  Prodotto.remove({ _id: req.params.id }, function (err) {
    if (err)
      res.status(500).send(err);
    else
      res.status(200).end();
  });
};

/* cerca codice */
exports.searchcodice = function (req, res) {
  var search = req.query.nome;

  query = _manageQueryProdotto({}, null, search.split(' '), null);

  if (req.query.limit > 0)
    Prodotto.find(query, 'codice',
      function (err, results) {
        if (err)
          res.status(500).send(err);
        else
          res.status(200).send(results);
      }).limit(parseInt(req.query.limit))
  else
    Prodotto.find(query, 'codice',
      function (err, results) {
        if (err)
          res.status(500).send(err);
        else
          res.status(200).send(results);
      })
}

exports.searchCodiceValido = function (req, res) {
  var search = req.query.nome;
  query = _manageQueryProdotto({}, null, search.split(' '), null);

  Prodotto.find(query, 'codice',
    function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results);
    })
}

/* cerca prodotti */
exports.search = function (req, res) {
  var search = req.query.search.split(' '), queryCategorie = {};

  if (req.query.isAgente == 'true') search.push("pubblicato")

  if (req.query.serie)
    search.push(req.query.serie)
  else
    search.push(req.serieIlmas)

  var generazione = req.query.generazione;
  if (req.query.isOfferta || req.query.ultimeGenerazioni)
    generazione = Utils.ultimaGenerazione()

  var query = _manageQueryProdotto({}, generazione, search, null);

  // alimenta le etichette bianche
  Prodotto.find(query)
    .populate('modello', { 'nome': 1, 'peso': 1, 'volume': 1, 'ecolamp': 1, 'made_in': 1, 'importato': 1, 'sostituibilita_LED': 1, 'sostituibilita_driver': 1, 'classe_energetica': 1 })
    .populate('modello_cript', { 'nome': 1 })
    .populate('categoria', { 'nome': 1, 'tipo': 1 })
    .populate('accessori')
    .limit(40)
    .exec(function (err, results) {
      if (err)
        res.status(500).send(err);
      else {
        if (req.query['categoria.tipo'])
          results = _.filter(results, (el) => { return el.categoria.tipo === req.query['categoria.tipo'] })

        res.status(200).send(results);
      }
    })
}

exports.profili_estro = function (req, res) {
  Prodotto.aggregate([
    { $match: { codice: { $exists: true }, categoria: ObjectId("5ee72bd946816c00040df08e"), serie: 'aComponenti' } },
    { $group: { _id: "$codice" } },
    { $sort: { _id: 1 } }
  ])
    .exec(function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results);
    })
}

exports.fonti_estro = function (req, res) {
  Prodotto.aggregate([
    { $match: { codice: { $exists: true }, categoria: ObjectId("5ee72b6646816c00040df089"), serie: 'aComponenti' } },
    { $group: { _id: "$codice" } },
    { $sort: { _id: 1 } }
  ])
    .exec(function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results);
    })
}

exports.getByCodice = function (req, res) {
  var search = req.params.codice;

  Prodotto.findOne({ codice: search },
    function (err, result) {
      if (err)
        res.status(500).send(err);
      else if (!result)
        res.status(500).send({ error: 'Codice non trovato' });
      else
        res.status(200).send(result);
    })
}

exports.codiciean = function (req, res) {
  var search = req.query.search, query = {};

  if (search) {
    search = search.split(' ')
    query = { keywords: { '$all': search }, }
  }

  CodiceEan.find(query)
    .limit(40)
    .exec(function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results);
    })
}

exports.getEanByidCodice = function (req, res) {
  CodiceEan.findOne({ idCodice: new ObjectId(req.query.idCodice) })
    .exec(function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results);
    })
}

/* cerca prodotti popolando il modello dell'accessorio*/
exports.accessori = function (req, res) {
  var query = _manageQueryProdotto({}, null, null, req.params.id);

  Prodotto.findOne(query)
    .populate('modello', { 'nome': 1, 'peso': 1, 'volume': 1, 'ecolamp': 1, 'classe_energetica': 1 })
    .populate('categoria', 'nome')
    .populate('accessori')
    .exec(function (err, prodotto) {
      var newaccessori = [];

      async.each(prodotto.accessori,
        function (item, cb) {
          if (item) {
            query = _manageQueryProdotto({}, null, null, item);

            Prodotto.findOne(query).populate('modello')
              .exec(function (err, accessorio) {
                if (err) {
                  res.status(500).send(err);
                  return;
                }

                newaccessori.push(accessorio)
                cb();
              })
          }
          else
            cb();
        },
        function (err) {
          if (err) return next(err);
          res.status(200).send(newaccessori)
        })
    })
}

/* cerca i moduli di un prodotto di un particolare modello */
exports.moduli = function (req, res) {
  var modello = req.query.modello,
    query = { modello: modello };

  Prodotto.find(query,
    function (err, results) {
      var moduli;

      if (err) {
        res.status(500).send(err);
        return;
      }

      moduli = _.filter(results,
        function (p) {
          return p.moduloled && p.moduloled !== ''
        });
      res.status(200).send(moduli);
    }).lean();
}

/* cerca i colori di un prodotto di un particolare modello */
exports.colori = function (req, res) {
  var modello = req.query.modello,
    query = { modello: modello };

  Prodotto.find(query,
    function (err, results) {
      var colori;

      if (err) {
        res.status(500).send(err);
        return;
      }

      colori = _.filter(results,
        function (p) {
          return p.colore && p.colore !== ''
        });

      res.status(200).send(colori);
    }).lean();
}

/* cerca i fasci di un prodotto di un particolare modello */
exports.fasci = function (req, res) {
  var modello = req.query.modello,
    query = { modello: modello };

  Prodotto.find(query,
    function (err, results) {
      var fasci;

      if (err) {
        res.status(500).send(err);
        return;
      }

      fasci = _.filter(results,
        function (p) {
          return p.fascio && p.fascio !== ''
        });

      res.status(200).send(fasci);
    }).lean();
}

/* aggiorna in blocco tutti i codici simili con lo stesso file ldt*/
exports.batchUpdateLdt = function (req, res) {
  console.log("batchUpdateLdt ")
  // mi arriva il codice esteso
  var codice = req.body.codice, // 0672ADG1E
    substrCount = codice.length == 9 ? 2 : 1,
    search = codice.substr(0, codice.length - substrCount), //0672ADG
    ldt = req.body.file_ldt;

  Prodotto.find({ codice: { $regex: search } }, function (err, prodotti) {
    if (err) {
      res.status(500).send(err);
      return;
    }

    async.each(prodotti,
      function (prodotto, cb) {
        prodotto.mdate = new Date(); // aggiorno la data di modifica
        prodotto.file_ldt = ldt;
        Prodotto.findOneAndUpdate({ _id: prodotto._id }, prodotto, function () {
          cb();
        })
      },
      function (err) {
        if (err)
          res.status(500).send(err);
        else
          res.status(200).end()
      })
  })
}

// questa get alimenta le schede tecniche
exports.get = function (req, res) {
  console.log("exports.get")
  query = _manageQueryProdotto({}, null, null, req.params.id);
  Prodotto.findOne(query)
    .populate('modello', { 'nome': 1, 'peso': 1, 'volume': 1, 'ecolamp': 1, 'sostituibilita_LED': 1, 'sostituibilita_driver': 1, 'classe_energetica': 1 })
    .populate('modello_cript', { 'nome': 1 })
    .populate('categoria', { 'nome': 1, 'voce_doganale': 1, 'tipo': 1, 'linea_prodotto': 1 })
    // .populate('Componente')
    .populate(
      {
        path: 'componenti._id',
        model: 'Componente',
        populate: {
          path: 'componenti_figlio._id',
          model: 'Componente',
        }
      }
    )
    .exec(function (err, prodotto) {
      if (err) {
        res.status(500).send(err);
        return;
      }

      var newaccessori = [],
        p = _.clone(prodotto);

      if (prodotto.accessori)
        async.each(prodotto.accessori,
          function (item, cb) {
            if (item) {
              query = _manageQueryProdotto({}, null, null, item);
              Prodotto.findOne(query)
                .populate('modello')
                .exec(
                  function (err, accessorio) {
                    if (err) {
                      res.status(500).send(err);
                    }
                    newaccessori.push(accessorio);
                    cb();
                  }
                )
            }
            else
              cb();
          },
          function (err) {
            if (err) return next(err);

            var prodotto = p.toJSON();

            prodotto.accessori = newaccessori
            prodotto.tracciatoRecordKITE = _manageDatiVSKITE(p);

            res.status(200).send(prodotto)
          })
      else res.status(200).send(prodotto)
    });
}

/*export DistintaBase CHINA 1*/
exports.csvDistintaBase = function (req, res) {
  console.log("start creazione excel");
  var fileName = "exportChina1.csv",
    filePath = process.cwd() + '/.tmp/';

  csvStream = csv.format({ headers: true }),
    writableStream = fs.createWriteStream(filePath + fileName);
  writableStream.on("finish",
    function () {
      res.status(200).send({ file: req.headers.referer + fileName });
    }
  );
  csvStream.pipe(writableStream);

  let aggr = Prodotto.aggregate([
    {
      "$project": {
        "Prodotto": "$$ROOT"
      }
    },
    {
      "$match": {
        "Prodotto.generazione": {
          "$nin": [
            "G4",
            "G5",
            "IN39"
          ]
        },
        "Prodotto.criptato": false,
        "Prodotto.speciale": false
      }
    },
    {
      "$lookup": {
        "localField": "Prodotto.modello",
        "from": "Modello",
        "foreignField": "_id",
        "as": "Modello"
      }
    },
    {
      "$unwind": {
        "path": "$Modello",
        "preserveNullAndEmptyArrays": false
      }
    },
    {
      "$match": {
        "Modello.china1": true
      }
    },
    {
      "$unwind": {
        "path": "$Prodotto.componenti",
        "preserveNullAndEmptyArrays": false
      }
    },
    {
      "$lookup": {
        "from": "Componente",
        "localField": "Prodotto.componenti._id",
        "foreignField": "_id",
        "as": "dettaglio"
      }
    },
    {
      "$unwind": {
        "path": "$dettaglio",
        "preserveNullAndEmptyArrays": false
      }
    },
    {
      "$project": {
        "Prodotto.codice": 1.0,
        "Prodotto.componenti.qt": 1.0,
        //  "Modello.china1" : 1.0, 
        "dettaglio.codice": 1.0
      }
    }
  ]);

  var inizio = new Date();
  console.log("inizio", inizio);

  (async function () {
    let doc, cursor;
    cursor = aggr.cursor({ batchSize: 500 }).exec(
      (err) => { console.log("errore??", err) }
    );
    // console.log("arriva??",cursor)
    while ((doc = await cursor.next())) {
      console.log("id", doc);
      csvStream.write(
        {
          'CODICE PRODOTTO': doc.Prodotto.codice,
          'CODICE COMPONENTE': doc.dettaglio.codice,
          'QT': doc.Prodotto.componenti.qt
        }
      );
    }
  })()
    .then(() => {
      console.log("done with cursor");
      console.log("iniziato", inizio);
      console.log("finito", new Date());
      csvStream.end();

    })

  /*  .exec(function(err, results)
   {
       if (err)
           res.status(500).send(err);
       else {
           console.log("arriva",results);

           results.forEach(function(elem){
               if(elem.dettaglio_modello.china1){
                   csvStream.write(
                       {
                           'CODICE PRODOTTO'   :   elem.codice,
                           'CODICE COMPONENTE' :   elem.dettaglio_componenti.codice,
                           'QT'                :   elem.componenti.qt,
                           'efesr': elem.dettaglio_modello.china1
                       }
                   );
               }
               
           });

           csvStream.end();            
       }
   }) */

}

_inviaMail = function (req, res) {

  var emailParams = {
    a: CONFIG.EMAIL_KITE_TO,
    cc: CONFIG.EMAIL_KITE_CC,
    ccn: [],
    from_email: CONFIG.EMAIL_KITE_FROM,
    from_name: CONFIG.EMAIL_KITE_NAME,
    oggetto: 'Download file per KITE',
    messaggio: '',
    urlFile: req.url
  };

  var bodyContent = {
    tipo: 'exportFileKite', email: emailParams
  }

  Email.send({ body: bodyContent },
    function (err) {
      if (err)
        console.log("errore invio mail - export file kite" + err);
    }
  )
}

exports.xlsProdottiKite = function (req, res) {
  console.log("start creazione excel");

  var fileName = "data_kite.csv",
    filePath = process.cwd() + '/.tmp/';
  csvStream = csv.format({ headers: true }),
    writableStream = fs.createWriteStream(filePath + fileName);

  writableStream.on("finish",
    function () {
      req.url = req.headers.referer + fileName;

      console.log("url" + req.url);

      _inviaMail(req, res)
    });

  csvStream.pipe(writableStream);

  var query = {
    exportMetel: true,
    kit: { $in: [false, null] },
    criptato: { $in: [false, null] },
    speciale: { $in: [false, null] }
  }

  //in dev e in test riduco i dati in base ai test
  // if (CONFIG.ENV != 'production'){
  //  _.extend(query, {codice:'18092XF0'});
  // }

  res.status(200).end();
  /*
  Prodotto.count(query,
      function(err, count){
          if(err){
              console.log('errore fase 1',err)
              res.status(500).send(err);
          }
          else
          {*/
  //in una prima fase pensavamo che facendo più select(limit basso) riuscivamo a gestire
  // la connessione al DB e quindi non mandare in socket close il DB,
  //successivamente abbiamo scoperto che facendo un'unica select con una mole molto grande di risultati
  //il problema non si presentava.
  /*
  var limit  = 300000;
  var cycleCount = Math.floor(count / limit);
  if ((count % limit) >0) cycleCount = cycleCount+1;
  var index = 0,
      resultTotale = [];
  */

  cycleCount = 1

  async.times(cycleCount,
    function (iteree, cb) {
      Prodotto.find(query, {
        codice: 1, sottocategoria: 1, colore: 1,
        lm: 1, k: 1, luci: 1, w: 1, ma: 1, fascio: 1, irc: 1,
        alimentatore: 1, satinato: 1, v: 1,
        hz: 1, moduloled: 1, generazione: 1,
        prezzo: 1, les: 1, ugr: 1, modello: 1,
        categoria: 1, exportMetel: 1, codice_prodotto_fornitore: 1,
        lunghezza: 1, note: 1, dimmerabile: 1, attacco: 1, corrente_v: 1, criptato: 1, speciale: 1, kit: 1
      },
        function (err, resultParziale) {
          if (err) {
            console.log('errore fase 2', err)
            res.status(500).send(err);
          }
          else {
            resultParziale.forEach(
              function (el) {
                var object = _.clone(_manageDatiVSKITE(el))

                var rigaExcel = {};

                mappaCampiToExportKITE.forEach(
                  function (item) {

                    var chiave = item.label,
                      valore = object[item.nomeCampo]
                    if (valore)
                      valore = valore.toString().trim();
                    rigaExcel[chiave] = valore;
                  }
                );

                csvStream.write(rigaExcel)
              }
            )
          }
          cb()
        }
      )
        .lean()
        .populate('modello', { 'nome': 1 })
        .populate('categoria', { 'voce_doganale': 1, 'nome': 1 })
        .populate('categoria', { 'nome': 1, 'voce_doganale': 1, 'linea_prodotto': 1 })
        .sort({ mdate: -1 });
    },
    function (err, result) {
      if (err) {
        console.log('errore fase 3', err)
        res.status(500).send(err);
      }
      else {
        console.log("END stream")
        csvStream.end();
      }
    }
  )
  /*}
}
)*/
}

_manageDatiVSKITE = function (element) {
  var object = {}

  mappaCampiToExportKITE.forEach(function (item) {
    var chiave = item.nomeCampo;
    if (item.trasformazioneDato) {
      var fun = item.trasformazioneDato
      valore = fun(element)
    }
    else
      valore = element[item.nomeCampo]

    if (valore && chiave != 'componentiDistintaBase')
      valore = valore.toString().trim()

    object[chiave] = valore;
  });

  if (element.criptato && element.modello_cript) {
    if (element.modello_cript.nome)
      object.Descrizione = element.modello_cript.nome
  }
  return object;
}

_managePrezzo = function (p) {
  numeral.language('it', numeral_it);
  numeral.language('it')

  var prezzo = p.prezzo || 0;

  return numeral(prezzo).format('0,0.00');
}

_manageVoceDoganale = function (p) {
  var res = "";

  if (p.categoria && p.categoria.voce_doganale)
    res = p.categoria.voce_doganale;

  return res;
}

_manageGruppoMer = function (p) {
  var res = "";
  if (p.codice)
    res = p.codice.substring(0, 4);

  return res;
}

_manageCategoriaNome = function (p) {
  var res = "";
  if (p.categoria && p.categoria.nome)
    res = p.categoria.nome;

  return res;
}

_manageCategoriaLinea = function (p) {
  var res = "";
  if (p.categoria && p.categoria.linea_prodotto)
    res = p.categoria.linea_prodotto;

  return res;
}

_manageModelloGM = function (p) {
  var res = "";
  if (p.modello && p.modello.modello_gm)
    res = p.modello.modello_gm;

  return res;
}

_manageModelloCCA = function (p) {
  var res = "";
  if (p.modello && p.modello.conto_contabile_acquisti)
    res = p.modello.conto_contabile_acquisti;

  return res;
}

_manageModelloCCV = function (p) {
  var res = "";
  if (p.modello && p.modello.conto_contabile_vendite)
    res = p.modello.conto_contabile_vendite;

  return res;
}

_manageModelloProvv = function (p) {
  var res = "";
  if (p.modello && p.modello.provvigioni)
    res = p.modello.provvigioni;

  return res;
}

_manageGruppo = function (p) {
  if (p && p.categoria && p.modello.nome) {
    var res = p.modello.nome.substring(0, 4);

    if (p.categoria._id == '59db2bed9b066b0400cd5f90' || // Accessori binario
      p.categoria._id == '59cb5c985a6aad0400841387' || // Drivers
      p.categoria._id == '59e612005bb0a904001e3d10' || // Accessori profili
      p.categoria._id == '59e6103b5bb0a904001e3d03' || // Accessori Strip Led
      p.categoria._id == '570b5473eff59523af4fcd2b' || // Accessori
      p.categoria._id == '59cb5f485a6aad0400841396' // Interfaccia
    )
      res = "9999";
    else if (p.categoria._id == '56b8aaa80de23c95600bd224') // Profili
      res = "9995";
    else if (p.categoria._id == '56b8ab022a376026658f22e3')  // Lampada Led
      res = "9993";
    else if (p.categoria._id == '57e0eb8eb0b11603005a1bf7')  // Sorgenti non led
      res = "9997";

    return res;
  }

  return ""
}

var _manageDescrizione = function (prod) {
  var prodotto = _.clone(prod);

  var result = _manageModello(prodotto);

  return result
}

_manageDescrizione2 = function (p) {
  var result2 = "";

  var prodotto2 = _.clone(p);

  if (prodotto2.colore)
    result2 = _manageColoreIt(p)

  if (prodotto2.satinato == true)
    result2 = result2 + ' SAT '

  if (prodotto2.moduloled)
    result2 = result2 + ' ' + prodotto2.moduloled

  if (prodotto2.lunghezza)
    result2 = result2 + ' ' + _manageLunghezza(prodotto2)

  if (prodotto2.codice_prodotto_fornitore)
    result2 = result2 + ' ' + prodotto2.codice_prodotto_fornitore

  if (prodotto2.w)
    result2 = result2 + ' ' + _managePotenza(prodotto2)

  if (prodotto2.k)
    result2 = result2 + ' ' + prodotto2.k + 'K'

  if (prodotto2.irc)
    result2 = result2 + ' CRI' + prodotto2.irc

  if (prodotto2.lm)
    result2 = result2 + ' ' + prodotto2.lm + 'lm'

  if (prodotto2.fascio)
    result2 = result2 + ' ' + prodotto2.fascio + '°'

  return result2
}

_manageDescrizione3 = function (p) {
  var result3 = "";

  if (p.ma)
    result3 = result3 + ' ' + p.ma + 'mA'

  if (p.corrente_v)
    result3 = result3 + ' ' + p.corrente_v + 'V'

  if (p.alimentatore || p.dimmerabile)
    result3 = result3 + ' ' + _manageAlimentatore(p)

  if (p.sottocategoria)
    result3 = result3 + ' ' + _manageSottocategoria(p);

  if (p.attacco)
    result3 = result3 + ' ' + p.attacco;

  return result3
}

_manageSottocategoria = function (p) {
  var res = "";

  if (p.sottocategoria) {
    if (p.sottocategoria.toLowerCase() == 'advanced' ||
      p.sottocategoria.toLowerCase() == 'excite')
      return "Adv"

    if (p.sottocategoria.toLowerCase() == 'cheese')
      return "Gold cheese"

    if (p.sottocategoria.toLowerCase() == 'bread')
      return "Gold+ bread"

    if (p.sottocategoria.toLowerCase() == 'meat' ||
      p.sottocategoria.toLowerCase() == 'fashion' ||
      p.sottocategoria.toLowerCase() == 'meat+' ||
      p.sottocategoria.toLowerCase() == 'fresh meat' ||
      p.sottocategoria.toLowerCase() == 'gold cheese' ||
      p.sottocategoria.toLowerCase() == 'gold+ bread' ||
      p.sottocategoria.toLowerCase() == 'ottica alluminio alta efficienza ottica 17°' ||
      p.sottocategoria.toLowerCase() == 'ottica alluminio alta efficienza ottica 25°' ||
      p.sottocategoria.toLowerCase() == 'ottica alluminio alta efficienza ottica 33°' ||
      p.sottocategoria.toLowerCase() == 'arled philips' ||
      p.sottocategoria.toLowerCase() == 'art' ||
      p.sottocategoria.toLowerCase() == 'vivid' ||
      p.sottocategoria.toLowerCase() == 'sunlike')
      return p.sottocategoria
  }

  return "";
}

_manageModello = function (p) {
  var res = "";

  if (p.modello && p.modello.nome) {
    res = p.modello.nome;

    if (p.ugr)
      res += " - UGR" + p.ugr;
  }

  return res;
}

_manageAlimentatore = function (prodotto) {
  var res = "";

  if (prodotto.alimentatore) {
    res = prodotto.alimentatore.toLowerCase()
      .replace('driver dimmerabile', '+Alim. DIM')
      .replace('driver dali', '+Alim. DALI')
      .replace('driver standard', '+Alim. STD')
  }
  else if (prodotto.dimmerabile)
    res = "Dimm."

  return res;
}

_manageColoreEn = function (p) {
  var colore_en = "";

  if (p.colore) {
    mapColor = Utils.mapColor();
    colore_en = mapColor[p.colore.trim().toLowerCase()]
  }

  return colore_en
}

_manageColoreIt = function (p) {
  var colore_it = "";
  if (p.colore) {
    colore_it = p.colore.toLowerCase()
      .replace('tutto bianco', 'tutto bi');
  }

  return colore_it
}

_manageSatinato = function (p) {
  if (p.satinato != true) return false;

  return true;
}

_manageMetel = function (p) {
  if (!p.exportMetel) return false;

  return true;
}

_manageLunghezza = function (p) {
  if (p.lunghezza) return p.lunghezza + ' m'
}

_managePotenza = function (p) {
  if (p.luci && p.w) return p.luci + 'X' + p.w + 'W'
  if (p.w) return p.w + 'W'
}

_manageDistintaBase = function (p) {
  var componenti_elaborati = Utils.cleanComponenti(p.componenti)
  var componenti_raggruppati = _.groupBy(componenti_elaborati, function (el) { var r = el.tipoAssociazione; delete el.tipoAssociazione; return r; })
  componenti_elaborati = []

  for (var a in componenti_raggruppati)
    componenti_elaborati.push({ tipoAssociazione: a, componenti: componenti_raggruppati[a] });

  return componenti_elaborati

  /* deve tornare questo
[
      {
      "tipoAssociazione" : "Corpo",
      "componenti" :
          [
              {
                  "codice" : "123",
                  "tipo" : "corpo",
                  "qt" : 1,
                  "componenti" :[
                          {
                      "codice" : "123",
                          "qt" : 1,
                          "tipo" : "verniciatura"
                      },
                  ]
              },
              {
                  "codice" : "123",
                  "qt" : 2,
                   "tipo" : "corpo"
              },
              {
                  "codice" : "123",
                  "qt" : 3,
                   "tipo" : "corpo"
              },
          ]
      },
      {
      "tipoAssociazione" : "ADATTATORE",
      "componenti" :
          [
              {
                  "codice" : "123",
                  "qt" : 1,
                  "tipo" : "adattatore"
              }
          ]

      },
      {
      "tipoAssociazione" : "OTTICA",
      "componenti" :
          [
              {
                  "codice" : "123",
                  "qt" : 1,
                   "tipo" : "manodopera"
              }
          ]
      },
  ]
  */
  /*da questo
  [
 {
   "_id" : {codice:'XXXXXX'},
   "tipoAssociazione" : "CORPO",
   "qt" : NumberInt("1")
 },
 {
   "_id" : ObjectId("5af2ce7369a7f8040032996c"),
   "tipoAssociazione" : "DRIVER",
   "qt" : NumberInt("1")
 }
]
  */
}

var mappaCampiToExportKITE = [
  {
    label: 'codice',
    nomeCampo: 'codice'
  },
  {
    label: 'Descrizione',
    nomeCampo: 'Descrizione',
    trasformazioneDato: _manageDescrizione
  },
  {
    label: 'DESCR2',
    nomeCampo: 'DESCR2',
    trasformazioneDato: _manageDescrizione2
  },
  {
    label: 'DESCR3',
    nomeCampo: 'DESCR3',
    trasformazioneDato: _manageDescrizione3
  },
  {
    label: 'colore',
    nomeCampo: 'colore',
    trasformazioneDato: _manageColoreIt
  },
  {
    label: 'lm',
    nomeCampo: 'lm'
  },
  {
    label: 'k',
    nomeCampo: 'k'
  },
  {
    label: 'Potenza',
    nomeCampo: 'potenza',
    trasformazioneDato: _managePotenza
  },
  {
    label: 'ma',
    nomeCampo: 'ma'
  },
  {
    label: 'fascio',
    nomeCampo: 'fascio'
  },
  {
    label: 'irc',
    nomeCampo: 'irc'
  },
  {
    label: 'alimentatore',
    nomeCampo: 'alimentatore'
  },
  {
    label: 'satinato',
    nomeCampo: 'satinato'
  },
  {
    label: 'v',
    nomeCampo: 'v'
  },
  {
    label: 'hz',
    nomeCampo: 'hz'
  },
  {
    label: 'moduloled',
    nomeCampo: 'moduloled'
  },
  {
    label: 'generazione',
    nomeCampo: 'generazione'
  },
  {
    label: 'prezzo',
    nomeCampo: 'prezzo',
    trasformazioneDato: _managePrezzo
  },
  {
    label: 'les',
    nomeCampo: 'les'
  },
  {
    label: 'ugr',
    nomeCampo: 'ugr'
  },
  {
    label: 'modello',
    nomeCampo: 'modello',
    trasformazioneDato: _manageModello
  },
  {
    label: 'ColoreEn',
    nomeCampo: 'ColoreEn',
    trasformazioneDato: _manageColoreEn
  },
  {
    label: 'metel',
    nomeCampo: 'exportMetel',
    trasformazioneDato: _manageMetel
  },
  {
    label: 'voce doganale',
    nomeCampo: 'voce_doganale',
    trasformazioneDato: _manageVoceDoganale
  },
  {
    label: 'lunghezza',
    nomeCampo: 'lunghezza',
    trasformazioneDato: _manageLunghezza
  },
  {
    label: 'note',
    nomeCampo: 'note'
  },
  {
    label: 'codice prodotto fornitore',
    nomeCampo: 'codice_prodotto_fornitore'
  },
  {
    label: 'Linea',
    nomeCampo: 'categoria_nome',
    trasformazioneDato: _manageCategoriaNome
  },
  {
    label: 'Gruppo',
    nomeCampo: 'gruppo',
    trasformazioneDato: _manageGruppo
  },
  {
    label: 'Attacco',
    nomeCampo: 'attacco'
  },
  {
    label: 'corrente V',
    nomeCampo: 'corrente_v'
  },
  {
    label: 'componenti',
    nomeCampo: 'componentiDistintaBase',
    trasformazioneDato: _manageDistintaBase
  },
  {
    label: 'criptato',
    nomeCampo: 'criptato'
  },
  {
    label: 'speciale',
    nomeCampo: 'speciale'
  },
  {
    label: 'kit',
    nomeCampo: 'kit'
  },
  {
    label: 'linea_prodotto',
    nomeCampo: 'linea_prodotto',
    trasformazioneDato: _manageCategoriaLinea
  },
  {
    label: 'gruppo_merceologico',
    nomeCampo: 'gruppo_merceologico',
    trasformazioneDato: _manageGruppoMer
  },
  {
    label: 'modello_gm',
    nomeCampo: 'modello_gm',
    trasformazioneDato: _manageModelloGM
  },
  {
    label: 'conto_contabile_acquisti',
    nomeCampo: 'conto_contabile_acquisti',
    trasformazioneDato: _manageModelloCCA
  },
  {
    label: 'conto_contabile_vendite',
    nomeCampo: 'conto_contabile_vendite',
    trasformazioneDato: _manageModelloCCV
  },
  {
    label: 'provvigioni',
    nomeCampo: 'provvigioni',
    trasformazioneDato: _manageModelloProvv
  },
  {
    label: 'codice_fornitore',
    nomeCampo: 'codice_fornitore'
  },
  {
    label: 'descr_codice_fornitore',
    nomeCampo: 'descr_codice_fornitore'
  },
  {
    label: 'fornitore',
    nomeCampo: 'fornitore'
  },
  {
    label: 'listino',
    nomeCampo: 'listino'
  },
  {
    label: 'provenienza',
    nomeCampo: 'provenienza'
  },
  {
    label: 'costo',
    nomeCampo: 'costo'
  }
]

/*export METEL*/
exports.csvMetel = function (req, res) {
  var anno = req.params.anno,
    fileName = "exportMetel" + anno + ".xls",
    filePath = process.cwd() + '/.tmp/';
  csvStream = csv.format({ headers: true }),
    writableStream = fs.createWriteStream(filePath + fileName);
  writableStream.on("finish",
    function () {
      res.status(200).send({ file: req.headers.referer + fileName });
    });

  csvStream.pipe(writableStream);

  var query =
  {
    linea: 'ilmas',
    pubblicato: true,
    exportMetel: true,
    mdate: {
      $gte: new Date(anno + '-01-01T16:00:00.000Z'),
      $lte: new Date(anno + '-12-31T16:00:00.000Z')
    }
  };
  var field =
  {
    codice: 1,
    modello: 1,
    colore: 1,
    w: 1,
    moduloled: 1,
    k: 1,
    irc: 1,
    lm: 1,
    prezzo: 1,
    ma: 1,
    fascio: 1
  }

  Prodotto.count(query,
    function (err, count) {
      if (err)
        res.status(500).send(err);
      else {
        var limit = 10000;
        var cycleCount = Math.floor(count / limit);
        if ((count % limit) > 0) cycleCount = cycleCount + 1;
        var index = 0, resultTotale = [];
        async.times(cycleCount,
          function (iteree, cb) {
            Prodotto.find(query, field,
              function (err, resultParziale) {
                if (err)
                  res.status(500).send(err);
                else {
                  async.each(resultParziale,
                    function (prodotto) {
                      csvStream.write({
                        'SIGLA MARCHIO': 'ILA',
                        'NLIST': '165',
                        'DELISTINO': '1',
                        'DATADECORRENZA': _dataDecorrenza(),
                        'Codice': prodotto.codice ? prodotto.codice : '',
                        'CODICE EAN': ' 0000000000000',
                        'Descrizione': prodotto.modello ? prodotto.modello.nome + " " : "" + prodotto.colore,
                        'DESCR2': _formatterField(prodotto.w, 'W', 'last') +
                          prodotto.moduloled + " " +
                          _formatterField(prodotto.k, 'K', 'last') +
                          _formatterField(prodotto.irc, 'IRC', 'first') +
                          _formatterField(prodotto.lm, 'lm', 'last') +
                          _formatterField(prodotto.fascio, '°', 'last') +
                          _formatterField(prodotto.ma, 'mA', 'last') +
                          _formatterField(prodotto.corrente_v, 'V', 'last'),
                        'DESCR3': '',
                        'QUANTITA CARTONE': '00001',
                        'QUANTITA MULTIPLA': '00001',
                        'QUANTITA MASSIMA': '999999',
                        'lead time': '3',
                        'PREZZO AL GROSSISTA': prodotto.prezzo,
                        'PREZZO AL PUBBLICO': prodotto.prezzo,
                        'MOLTIPLICATORE PREZZO': '1',
                        'CODICE VALUTA': 'EUR',
                        'UNITA DI MISURA': 'PCE',
                        'PRODOTTO COMPOSTO': '0',
                        'STATO DEL PRODOTTO': '3',
                        'DATA ULTIMA VARIAZIONE': _dataDecorrenza()
                      });
                    })

                  cb();
                }
              })
              .lean()
              .populate('modello', 'nome')
              .skip((index++) * limit).limit(limit).sort({ mdate: 1 });
          },
          function (err, result) {
            if (err)
              res.status(500).send(err);
            else
              csvStream.end();
          })
      }
    })
}

exports.batchUpdateCategoria = function (req, res) {
  // mi arriva il codice esteso
  var modelloId = req.body.modelloId,
    categoriaId = req.body.categoriaId;
  Prodotto.find(
    { modello: modelloId },
    function (err, prodotti) {
      if (err)
        res.status(500).send(err);
      async.each(prodotti,
        function (prodotto, cb) {
          prodotto.mdate = new Date(); // aggiorno la data di modifica
          prodotto.categoria = categoriaId;
          Prodotto.findOneAndUpdate({ _id: prodotto._id }, prodotto, function () {
            cb();
          })
        },
        function (err) {
          if (err)
            res.status(500).send(err);
          res.status(200).end()
        })
    })
}

exports.prodottiInModello = function (req, res) {
  var modelloId = req.params.id;
  Prodotto.find({ modello: modelloId },
    function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results);
    })
}

/* ping prodotti */
exports.ping = function (req, res) {
  Prodotto.find({})
    .limit(1)
    .exec(function (err, prodotti) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).end()
    });
}

exports.cercaScheda = function (req, res) {
  var cerca = req.query.nome;
  /* li trovo tutti e poi cerco il match sui risultati*/
  Contatto.distinct('azienda',
    function (err, results) {
      var aziendeFound = _.filter(results,
        function (azienda) {
          var reg = new RegExp(cerca, 'i')
          return azienda.match(reg)
        });
      res.send(_.map(aziendeFound, function (a) { return { nome: a } }))
    })
}

exports.countean = function (req, res) {
  var result = { countEan: 0 }
  res.status(200).send(result);
}

exports.countFn = (serie) => {
  return new Promise((resolve, reject) => {
    var result =
    {
      countProdotti: 0,
      countG6: 0,
      countD1: 0,
      countG5: 0,
      countG4: 0
    }
    var query = {}, search = []
  
    search.push(serie)
    // serie = req.query.serie
    query = _manageQueryProdotto({}, null, search, null);
    console.log('inizio a contare')
    Prodotto.countDocuments({ serie: serie })
      .exec(function (err, countProdotti) {
        console.log('primo conto', new Date(), query)
        if (err)
          reject(err);
        else {
          result.countProdotti = countProdotti;
          query = _manageQueryProdotto({}, 'G7', search, null);
          Prodotto.countDocuments({ serie: serie, generazione: 'G7' })
            .exec(function (err, countG7) {
              console.log('secondo conto', new Date(), query)
              if (err)
                reject(err);
              else {
                result.countG7 = countG7;
                query = _manageQueryProdotto({}, 'G6', search, null);
                Prodotto.countDocuments({ serie: serie, generazione: 'G6' })
                  .exec(function (err, countG6) {
                    console.log('terzo conto', new Date(), query)
                    if (err)
                      reject(err);
                    else {
                      result.countG6 = countG6;
                      query = _manageQueryProdotto({}, 'G5', search, null);
                      Prodotto.countDocuments(
                        { serie: serie, generazione: { $in: ["G5","G4","IN39","IN40"] } })
                        .exec(function (err, countG5) {
                          console.log('quarto conto', new Date(), query)
                          if (err)
                            reject(err);
                          else {
                            result.countG5 = countG5;
                            query = _manageQueryProdotto({}, 'G4', search, null);
                            Prodotto.countDocuments({ serie: serie, generazione: 'G4' })
                              .exec(function (err, countG4) {
                                console.log('quinto conto', new Date(), query)
                                if (err)
                                  reject(err);
                                else {
                                  result.countG4 = countG4;
                                  query = _manageQueryProdotto({}, 'D1', search, null);
                                  Prodotto.countDocuments({ serie: serie, generazione: 'D1' })
                                    .exec(function (err, countD1) {
                                      console.log('sesto conto', new Date(), query)
                                      if (err)
                                        reject(err);
                                      else {
                                        result.countD1 = countD1;
                                        resolve(result);
                                      }
                                    })
                                }
                              })
                          }
                        })
                    }
                  })
              }
            })
        }
      })
    })
}
exports.count = function (req, res) {
  var query = {}
  if (req.query.serie)
    query.serie = req.query.serie
  else
    query.serie = req.serieIlmas

  exports.countFn(query.serie).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(500).send(err);
  })
}

exports.prova_kite = function (req, res) {
  Utils.getToken()
    .then(function (tok) {
      console.log('dentro get tokeni prova');
      res.status(200).send({ ok: 1 })
    })
    .catch(function (err) {
      console.log('dentro get token prova CATCH', err);
      res.status(200).send({ ok: 0, err: err })
    });
}
