var mongoose = require('mongoose'),
	Contatto = mongoose.model('Contatto'),
	_ = require('underscore'),
	fs = require("fs");
    csv = require("fast-csv"),
    async = require('async'),
    CONFIG = require(process.cwd()+'/server/config.js');


/* cerca per ID */
exports.searchById = function (req, res) 
{
	Contatto.findOne({_id:req.params.id}, function (err, result) {
		if (err)
			res.status(500).send(err);
		else
			res.status(200).send(result);
	})
}

/* cerca generico */
exports.search = function (req, res) 
{ 
	var isAgente = false;
	var search = req.query.search.split(' '),
		query = {keywords:{ $all: search}};
	try{
		isAgente = req.user && req.user.app_metadata.roles && req.user.app_metadata.roles.indexOf('agente') !== -1 ? true : false;
	}
	catch(e){}
 	if (isAgente==true)
		query = {
			$and:[
				{ i_user_id:req.user.sub},
			  	{ keywords:{ $all: search}}
			]
		};
	Contatto.find(query, 
		function (err, results) 
		{
			if (err)
				res.status(500).send(err);
			else
				res.status(200).send(results);
	}).limit(40).sort({nome: 1})
}

exports.searchByEmail = function (req, res) 
{
 	var search = req.query.search;
	var query = {email:{$regex: search,$options: 'i'}};
	try{
		isAgente = req.user && req.user.app_metadata.roles && req.user.app_metadata.roles.indexOf('agente') !== -1 ? true : false;
	}
	catch(e){}
	if (isAgente==true)
		query = {
			$and:[
				{ i_user_id:req.user.sub}, //l'agente può visualizzare solo i suoi contatti
			  	{ email:{$regex: search,$options: 'i'}}
			] 
		};  
	Contatto.find(
			query,
			{_id:0,email:1}, 
	function (err, results) 
	{
		if (err)
			res.status(500).send(err);
		else
			res.status(200).send(_.unique(results, 
			function (r)
			{ // mando gli indirizzo unici trovati
				return r.email;
			}));
	});
}

/* cerca tags nei contatti */
exports.tags = function (req, res) 
{
	var cerca = req.query.nome;
	/* li trovo tutti e poi cerco il match sui risultati*/
	Contatto.distinct('tags', 
		function (err, results) 
		{
		var tagsFound = _.filter(results, 
			function (tag) 
			{
				var reg = new RegExp(cerca,'i')
				return tag.match(reg)
			});
		res.send(tagsFound)
	})
}

exports.aziende = function (req, res) 
{
	var cerca = req.query.nome;
	/* li trovo tutti e poi cerco il match sui risultati*/
	Contatto.distinct('azienda', 
		function (err, results) 
		{
			var aziendeFound = _.filter(results, 
				function (azienda) 
				{
					var reg = new RegExp(cerca,'i')
					return azienda.match(reg)
				});
			res.send(_.map(aziendeFound, 
				function(a){
					return {nome:a}
				})
			)
		}
	)
}

exports.last = function (req, res) {
	console.log("exports.last")
	var query = {};
	var isAgente = false;
	try{
		isAgente = req.user && req.user.app_metadata.roles && req.user.app_metadata.roles.indexOf('agente') !== -1 ? true : false;
	}
	catch(e){}
	if (isAgente==true)
		query =  { i_user_id:req.user.sub};//l'agente può visualizzare solo i suoi contatti
	Contatto.find(query, 
		function (err, result) 
		{
			if (err)
				res.status(500).send(err);
			else
				res.status(200).send(result);
		}
	).limit(10).sort({mdate: -1})
}

exports.save = function (req, res) 
{
	console.log("exports.save")
	var data = req.body,
    contatto = new Contatto(data);
	contatto.mdate = new Date();
	contatto.idate = new Date();
	contatto.i_user_id = req.user.sub;
	contatto.save(function (err, result) 
	{
		if (err)
			res.status(500).send(err);
		else
			res.status(200).send(result);
	});
}

exports.update = function(req, res) 
{
	var modify = req.body;
	modify.mdate = new Date();
	modify.m_user_id = req.user.sub;
	Contatto.findById(req.params.id,
		function (err, contatto) 
		{
			contatto.update(modify, 
			{
				runValidators: true
			}, 
			function (err) 
			{
				if (err) 
					res.status(400).send(err);
				else
					res.status(200).send(modify);
			})
		}
	);
};

exports.delete = function(req, res) 
{
	Contatto.remove({
		_id: req.params.id
	}, 
	function (err) 
	{
		if (err)
			res.status(500).send(err);
		else
			res.status(200).end();
	});
};

exports.doppi = function (req, res) 
{
	var fileName = "exportContattiDoppi.xls",
        filePath = process.cwd()+'/.tmp/';
        csvStream = csv.format({headers: true}),
        writableStream = fs.createWriteStream(filePath+fileName);
        writableStream.on("finish",
            function()
            { 
                res.status(200).send({file:req.headers.referer+fileName});
            }
        );

    csvStream.pipe(writableStream);
	Contatto.aggregate([{
        $group: {
        	_id: {
        		//"nome": "$nome",
        		"email": "$email" 
        	},
        	count: {
        		$sum: 1
        	}
    	}}],
        function (err, result) 
        {
    		if(err)
    			return next(err)
    		else
    		{ 
    			contatti
    			var contatti = _.pluck(result, '_id');
    			var count 	 = _.pluck(result, 'count');  
				var item=0;
				async.each(contatti, 
                    function(contatto){ 
                 		if (count[item++]>1){
                    		csvStream.write(
                        		{
                           			'NOME'  :   contatto.nome, 
                           			'EMAIL' :   contatto.email
                        		}
                        	);
                    	}
                	}
                )
    			csvStream.end();
    		}
    	}
    );
} 

exports.nazioni = function (req, res) 
{
	Contatto.aggregate(
		[
			{$match: {
	            nazione: {$ne : null}
	        }},
	        {$group: {
	        	_id: {
	        		"nome": "$nazione" 
	        	} 
	    	}}
    	],
       function (err, result) 
       {
			if (err)
				res.status(500).send(err);
			else
				res.status(200).send(result);
		}
    )
}

exports.province = function (req, res) 
{
	Contatto.aggregate( 
		[
			{$match: {
	            provincia: {$ne : null}
	        }},
	        {$group: {
        	_id: {
        		"nome": "$provincia" 
        	} 
    	}}],
       function (err, result) 
       {
			if (err)
				res.status(500).send(err);
			else
				res.status(200).send(result);
		}
    )
}

exports.countFn = () => {
	return new Promise((resolve, reject) => {
    var result = { 
    	countContatti : 0 
    }
    Contatto.count() 
    .exec(function (err, countContatti)
    {
        if (err)
            reject(err);
        else{
            result.countContatti = countContatti;
            resolve(result);
        }
    });
	})
}

exports.count = function (req, res) 
{
	exports.countFn().then((result) => {
		res.status(200).send(result);
	}).catch((err) => {
		res.status(500).send(err);
	})
}