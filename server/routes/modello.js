var mongoose = require('mongoose'),
  Modello = mongoose.model('Modello'),
  Prodotto = mongoose.model('Prodotto'),
  async = require('async'),
  _ = require('underscore'),
  csv = require("fast-csv"),
  fs = require("fs"),
  moment = require('moment'),
  axios = require('axios'),
  Email = require('../routes/email'),
  CONFIG = require(process.cwd() + '/server/config.js'),
  Utils = require('../routes/utils'),
  //mi importo la route perchè ho lasciato li le operazioni sulla collection
  CreazioneDistinte = require('../routes/creazioneDistinte');
ObjectId = require('mongoose').Types.ObjectId;

exports.get = function (req, res) {
  Modello.findOne({ _id: req.params.id })
    .populate('categoria', 'nome')
    .populate('accessori')
    .populate('configDistintaBase.regole.componenti.id', 'codice')
    .exec(function (err, modello) {
      if (err)
        res.status(500).send(err);
      else {

        //////
        /*
            Prodotto.aggregate([
                {
                    $match : { modello : modello._id }
                },
                {
                    $group: {
                        _id:{modello:"$modello", generazione:"$generazione"}, count:{$sum:1}
                    } 
                }
            ])
            .exec(function(err,totaleProdotti){ 
     
                if (err){
                    console.log(err)
                    return next(err);
                }
                else
                {
                    var newaccessori = [];
                        m = _.clone(modello)

                    async.each(modello.accessori, 
                        function(item, cb)
                        {
                            Prodotto.findOne({_id:item})
                            .populate('modello')
                            .exec(
                                function(err,accessorio){
                                    if (err)
                                        res.status(500).send(err);
                                    if (accessorio && accessorio.modello) 
                                        nomemodello = accessorio.modello.nome;
                                    else
                                        nomemodello = '';
                                    if (accessorio && accessorio.foto)
                                        nomeFoto = accessorio.foto;
                                    else
                                        nomeFoto = '';
                                        newaccessori.push(accessorio)
                                    cb();
                                }
                            )
                        }, 
                        function(err)
                        {
                            if(err)
                                return next(err);
                            var modello = m.toJSON();
                               modello.accessori = newaccessori
                              totaleProdotti =  _.sortBy(totaleProdotti, function(totale){ return totale._id.generazione });

                               modello.totProdotti = totaleProdotti
                            res.status(200).send(modello)
                        }
                    )
                }
            }
       ) 
       */

        //recupero gli accesori per modello
        var modelliAccessori = []

        if (modello.accessori) {
          async.each(modello.accessori,
            function (pAccessorioId, cb) {
              if (pAccessorioId && pAccessorioId._id) {
                Prodotto.findOne(
                  {
                    _id: ObjectId(pAccessorioId._id),
                    '$and': [{ keywords: { '$in': Utils.keyUltimeGenerazioni({ prefix: 'true' }) } }]
                  },
                  { codice: 1, colore: 1 })
                  .exec(function (err, res) {
                    if (err)
                      res.status(500).send(err);
                    else {
                      if (res)
                        modelliAccessori.push(res)
                      cb()
                    }
                  })
              }
            },
            function (err) {
              if (!err) {
                modello.accessori = modelliAccessori;
                res.status(200).send(modello)
              }
            }
          )
        }
        else
          res.status(200).send(modello)
      }
    }
    );
}

/* Lista tutti i modelli */
exports.modelli = function (req, res) {
  var query = {}
  if (req.query.serie)
    query.serie = req.query.serie
  else
    query.serie = req.serieIlmas

  Modello.find(query)
    .populate('categoria', 'nome')
    .sort({ ordinamento: 1, nome: 1, categoria: 1 })
    .exec(function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results);
    })
}

exports.last = function (req, res) {
  var query = {}
  if (req.query.serie)
    query.serie = req.query.serie
  else
    query.serie = req.serieIlmas

  Modello.find(query)
    .populate('categoria', 'nome')
    .sort({ mdate: -1, ordinamento: 1, nome: 1, categoria: 1 })
    .limit(10)
    .exec(function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results);
    })
}

/* Cerca un modello in Like*/
exports.cerca = function (req, res) {
  var query = { $and: [{ $or: [{ nome: { $regex: req.query.nome, $options: 'i' } }, { nome: req.query.nome }] }] }
  if (req.query.serie)
    query.$and.push({ serie: req.query.serie })
  else
    query.$and.push({ serie: req.serieIlmas })

  Modello.find(query,
    function (err, result) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(result);
    })
    .populate('categoria', 'nome')
    .populate('configDistintaBase.regole.componenti.id', 'codice');
}

exports.save = function (req, res) {
  var data = req.body;
  var modello = new Modello(data);
  modello.mdate = new Date(),
    modello.last_user = req.user;
  modello.save(
    function (err, result) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(result);
    }
  )
}

var Stepper = function () {

  this.current = 0;
  this.step = [
    {
      funzione: 'getProdotti',
      descrizione: 'selezione dei prodotti sul DB',
      log: [],
      errore: '',
      eseguito: false
    },
    {
      funzione: 'saveProdotti',
      descrizione: 'salvataggio dei prodotti modificati sul DB',
      log: [],
      errore: '',
      eseguito: false
    },
    {
      funzione: 'getToken',
      descrizione: 'recupero del token di autorizzazione da KITE',
      log: [],
      errore: '',
      eseguito: false
    },
    {
      funzione: 'getProdottiProcessati',
      descrizione: 'recupero dei dati minimi dei prodotti appena processati',
      log: [],
      errore: '',
      eseguito: false
    },
    {
      funzione: 'generazioneDistinta',
      descrizione: 'trasformazione dei dati della distinta per i prodotti processati',
      log: [],
      errore: '',
      eseguito: false
    },
    {
      funzione: 'postDistinta',
      descrizione: 'invio dell\'oggetto distinta al KITE',
      log: [],
      errore: '',
      eseguito: false
    },
    { //non usato
      funzione: 'inviaMail',
      descrizione: 'invio di questa mail ;)',
      log: [],
      errore: '',
      eseguito: false
    }
  ]

  this.getResult = () => {

    let retValue = [];

    // la roba commentata qui sotto genera un po di dettagli
    /*for (item of this.step) {
        if (item.eseguito) {
            retValue.push({ line : '*****  ' + item.descrizione + "  *****"});
            for (line of item.log) {
                retValue.push({ line : line});
            }
        }
    }*/
    if (this.step[1].eseguito)
      retValue.push({ line: 'Generazione distinta : OK!' });
    else
      retValue.push({ line: 'Generazione distinta : Errore!' });

    if (this.step[5].eseguito)
      retValue.push({ line: 'Invio dati al KITE : OK!' });
    else
      retValue.push({ line: 'Invio dati al KITE : Errore!' });

    return retValue;
  };
  this.next = () => { if (this.step[this.current]) { this.step[this.current].eseguito = true; this.current++; } };
  this.pushLog = (obj) => { try { if (this.step[this.current] && this.step[this.current].log) this.step[this.current].log.push(obj); } catch (e) { console.error('Errore stepper aggiunta log', this.step, this.current, e); } };
  this.setErr = (err) => { if (this.step[this.current]) this.step[this.current].errore = err; };
};
var stepGenerazioneDistinta = new Stepper();

exports.generaDistinta = function (req, res) {
  var id = req.params.id, stato_id = '';

  CreazioneDistinte.create(function (ret) { console.log('obj creadistinta', ret); stato_id = ret; });

  Modello.findOne({ _id: req.params.id })
    .populate('categoria', 'nome')
    .populate('configDistintaBase.regole.componenti.id', 'codice')
    .exec(function (err, modello) {
      if (err)
        res.status(500).send(err);
      else {
        res.status(200).send('Processo di creazione distinta lanciato!');

        /*var spawn = require("child_process").spawn,child, temp="";

        child = spawn("node",["scripts/generaDistintaBase/start.js", JSON.stringify(modello), JSON.stringify(modello.configDistintaBase)]);
    
        child.stdout.on("data",function(data){
            console.info(""+data);
            var strData = data.toString('utf-8');
            temp = temp + strData ;
        });
    
        child.stderr.on("data",function(data){
            console.info("Script Errors: " + data);
        });
    
        child.on("exit",function(){
            console.info("Script finished");
        });
    
        child.stdin.end(); //end input*/

        creaDistintaBase(modello, modello.configDistintaBase, null, req)
          // .then(inviaDistintaKyte)
          .then(function (err, result) {
            if (err)
              console.error(err);
            //res.status(500).send(err);

            CreazioneDistinte.close(stato_id, 'OK');
            // inviaMailDistinta(modello.nome, null, stepGenerazioneDistinta.getResult());
            //res.status(200).send('distinta creata correttamente');
          })
          .catch(function (err) {
            CreazioneDistinte.close(stato_id, 'KO');
            // inviaMailDistinta(modello.nome, null, stepGenerazioneDistinta.getResult());
          })
      }
    });
}

var inviaMailDistinta = function (codice_modello, codice_prodotto, step) {
  var emailParams = {
    a: CONFIG.EMAIL_DISTINTA_KITE_TO,
    cc: CONFIG.EMAIL_DISTINTA_KITE_CC,
    ccn: [],
    from_email: CONFIG.EMAIL_KITE_FROM,
    from_name: CONFIG.EMAIL_KITE_NAME,
    oggetto: codice_modello ? 'Creazione e invio a KITE della distinta base per il modello ' + codice_modello : 'Creazione e invio a KITE prodotto ' + codice_prodotto,
    messaggio: codice_modello ? 'Report creazione distinta base per il modello ' + codice_modello : 'Report creazione distinta base per prodotto ' + codice_prodotto,
    step: step
  };

  var bodyContent = {
    tipo: 'export_distintabase', email: emailParams
  }

  Email.send_no_route({ body: bodyContent },
    function (err, result) {
      if (err)
        console.log("errore invio mail - export distinta base KITE" + err);

      console.log('mail inviata a ' + JSON.stringify(CONFIG.EMAIL_DISTINTA_KITE_TO) + ' ' + JSON.stringify(CONFIG.EMAIL_DISTINTA_KITE_CC));
      //stepGenerazioneDistinta.next();
      //resolve();
    }
  )
}

// export per processo esterno
exports.creaDistintaBase = creaDistintaBase;
// exports.inviaDistintaKyte = inviaDistintaKyte;
exports.inviaMailDistinta = inviaMailDistinta;

function inviaDistintaKyte(processati) {
  var importUrl = CONFIG.URL_KYTE_DISTINTA;

  var getProdottiProcessati = function (token) {
    return new Promise(function (resolve, reject) {
      Prodotto.find({ codice: { $in: processati } }, { codice: 1, componenti: 1 })
        .populate(
          {
            path: 'componenti._id',
            model: 'Componente',
            populate: {
              path: 'componenti_figlio._id',
              model: 'Componente',
            }
          }
        )
        .exec(function (err, prodotti) {
          if (err)
            reject(err);

          stepGenerazioneDistinta.pushLog('numero prodotti processati ' + prodotti.length);
          stepGenerazioneDistinta.next();

          var prodotti_elaborati = []
          for (let p of prodotti) {
            //p.componentiDistintaBase = Utils.cleanComponenti(p.componenti); inutile??
            delete p.componenti
            p.codiceProdotto = p.codice
            delete p.codice
            var componenti_elaborati = Utils.cleanComponenti(p.componenti)
            var componenti_raggruppati = _.groupBy(componenti_elaborati, function (el) { var r = el.tipoAssociazione; delete el.tipoAssociazione; return r; })
            componenti_elaborati = []
            //console.log(componenti_raggruppati);
            for (var a in componenti_raggruppati) {
              componenti_elaborati.push({ tipoAssociazione: a, componenti: componenti_raggruppati[a] });
            }
            var prodotto_elaborato = { codiceProdotto: p.codice, componentiDistintaBase: componenti_elaborati };
            prodotti_elaborati.push(prodotto_elaborato)
            //console.log('prodotto pulito 3', JSON.stringify(prodotto_elaborato))
          }

          stepGenerazioneDistinta.pushLog('numero prodotti elaborati ' + prodotti_elaborati.length);
          stepGenerazioneDistinta.next();
          console.log("prodotti_elaborati")
          resolve({ token: token, prodotti: prodotti_elaborati });

        });
    });
  }

  var postDistinta = function (obj) {

    console.log("oggetto spedito ", obj.prodotti)

    return new Promise(function (resolve, reject) {
      var headers = {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + obj.token
      };
      axios.post(importUrl, {
        "type": "distinta", // se qui faccio degli errori il sistema non mi dice nulla di strano
        "format": "json",
        "value": { "distinteBase": obj.prodotti }
      }, { headers: headers })
        .then(function (response) {

          if (response.data.result_msg === 'OK') {
            resolve();
            stepGenerazioneDistinta.pushLog('messaggio dal server ' + response.data.result_msg);
            stepGenerazioneDistinta.next();
          } else {
            stepGenerazioneDistinta.setErr(response.data.result_msg);
            console.log('post distinta error 1', response.data.result_msg, response.data.msg)

            reject("error");
          }
        })
        .catch(function (error) {
          stepGenerazioneDistinta.setErr(error);
          console.log('post distinta error 2');//, error);
          reject(error);
        });
    });
  }

  return new Promise(function (resolve, reject) {
    var logToken = function (t) {
      console.log('token ottenuto', t);
    }
    var risolvi = function () {
      resolve();
      //inviaMail ();
    }
    var failureCallback = function (err) {
      console.error('errore nella catena dei then ', err)
      resolve();
    }

    Utils.getToken(stepGenerazioneDistinta)
      //.then(logToken)
      .then(getProdottiProcessati)
      .then(postDistinta)
      .then(risolvi)
      .catch(failureCallback)
  });
}

function creaDistintaBase(modello, distinta, prodotti, req) {
  var processati = []; // array in cui finisce il codice di ogni prodotto a cui è stata applicata almeno una regola

  function saveProdotto(p, req) {
    console.log('[DISTINTA] : inizio procedura salvataggio prodotto');
    return new Promise(function (resolve, reject) {
      var fieldTuUpdate = fieldTuUpdate = { componenti: p.componenti, mdate: new Date() }
      if (req && req.user) {
        fieldTuUpdate = { componenti: p.componenti, mdate: new Date(), last_user: req.user }
      }
      Prodotto.update(
        { _id: p._id },
        { $set: fieldTuUpdate },
        {
          runValidators: false
        },
        function (err) {
          if (err) {
            console.log("err ", err)
            reject(err);
          } else {
            resolve();
          }
        })
    })
  }

  function checkCodice(codice, regola) {
    var found = true;
    if ((regola.lunghezzaMaxCodice && codice.length === regola.lunghezzaMaxCodice) || !regola.lunghezzaMaxCodice) {
      //console.log('codice verificato per lunghezza')
      //console.log('chiavi regole codice', JSON.stringify(regola.regoleCodice))
      for (n of _.keys(regola.regoleCodice)) {
        if (n.indexOf('pos') === 0) { //sto if messo perchè venendo l'oggetto da db si porta dietro alcune proprietà "inaspettate"
          var lettera = regola.regoleCodice[n], pos = parseInt(n.replace('pos', ''));
          //console.log('checkcodice', 'lettera', lettera, 'pos', pos, 'chiave', n, 'chiave senza pos', n.replace('pos', ''), 'charAt pos-1', codice.charAt(pos - 1))

          if ((lettera + 'x' === 'x' || lettera === undefined)) {
            //console.log('lettera undefined, non faccio niente')
          } else {
            if (codice.charAt(pos - 1).toLowerCase() != lettera.toLowerCase()) {//voglio in indice 1. Ci sono dei valori vuoti, per questo testo lettera. Testo lettera in quel modo assurdo perchè un valore potrebbe essere 0, per cui tornare sempre false
              found = false;
            } else {
              //console.log('verificato', codice.charAt(pos - 1), lettera, codice.charAt(pos - 1) == lettera)
            }
          }

        }
      }
    } else {
      //console.log('codice non verificato per lunghezza')
      found = false;
    }

    //console.log('codice verificato:', found)
    return found;
  }

  function applicaRegola(prodotto, tipoAssociazione, regola) {
    console.log('[DISTINTA] : applico una regola al prodotto');
    for (componente of regola.componenti) {
      var comp = {
        "tipoAssociazione": tipoAssociazione.toUpperCase(),
        "qt": componente.qt,
        "_id": componente.id
      }

      prodotto.componenti.push(comp);
    }

    return prodotto;
  }

  function aggiungiProcessato(c) {
    if (!_.find(processati, function (el) {
      return c === el;
    })) {
      processati.push(c);
    }// else throw 'giaProcessato'
  }

  function elaboraSingoloProdotto(p) {
    if ((!p.kit && !p.criptato && !p.speciale && Utils.isInNuoveGenerazioni(p))) {
      p.componenti = [];
      for (conf of distinta) {
        //console.log('associazione:', conf.tipoAssociazione, p.codice)
        //let applicataConf = false;
        for (const [idx, regola] of conf.regole.entries()) {
          let applicataRegola = false;
          if (conf.casoElse && idx === conf.regole.length - 1) { // caso else : ultima regola della serie solo se casoElse su conf è true, altrimenti l'ultima è una regola come le altre
            //console.log('applico caso else')
            // console.log('caso else', conf.casoElse , idx , conf.regole.length);
            if (!applicataRegola) {
              p = applicaRegola(p, conf.tipoAssociazione, regola);
              applicataRegola = true;// qui probabilmente è inutile perchè l'else è l'ultimo caso della serie
              //console.log('caso else', _.pick(p, 'codice', 'componenti'));
            }
          } else { // caso normale
            //console.log('applico caso normale')
            // console.log('codice da verificare', p.codice, JSON.stringify(regola.regoleCodice));
            if (!applicataRegola && checkCodice(p.codice, regola)) {
              //console.log('codice verificato', p.codice, JSON.stringify(regola.regoleCodice));
              p = applicaRegola(p, conf.tipoAssociazione, regola);
              applicataRegola = true;
              //console.log('caso normale', _.pick(p, 'codice', 'componenti'));
            }
          }
          if (applicataRegola)
            aggiungiProcessato(p.codice);
        }
        //if (p.componenti.length > 2)
        //    console.log('prodotto', _.pick(p, 'codice', 'componenti'));
      }
    }
  }

  var itemsProcessed = 0

  return new Promise(

    function (resolve, reject) {

      if (!prodotti || prodotti.length == 0) {
        var query =
        {
          modello: modello._id,
          keywords: { $in: Utils.keyUltimeGenerazioni({ prefix: 'true' }) },
          kit: { $in: [false, null] },
          criptato: { $in: [false, null] },
          speciale: { $in: [false, null] }
        }
        Prodotto.find(query,
          function (err, result) {
            if (err)
              console.log('errore in getProdotti ()', err)
            else {
              stepGenerazioneDistinta.pushLog('selezionati ' + result.length + ' prodotti');
              stepGenerazioneDistinta.next();
              console.log('prima del ciclo async')
              async.each(result,
                function (p, cb) {
                  console.log('prima elaboraSingoloProdotto')
                  elaboraSingoloProdotto(p);
                  console.log('dopo elaboraSingoloProdotto')
                  if (_.find(processati,
                    function (el) {
                      return p.codice === el;
                    })) {
                    console.log('stepGenerazioneDistinta')
                    stepGenerazioneDistinta.pushLog('processato ' + p.codice);
                    console.log('riga 656')
                    saveProdotto(p, req)
                      .then(
                        function () {
                          console.log('dopo il save')
                          cb();
                          itemsProcessed++;
                          if (itemsProcessed === result.length) {
                            resolve(processati);
                          }

                        })
                      .catch(function (err) {
                        console.error(err);
                        reject(err)
                      })
                  }
                  else {
                    //var processati == vuoto
                    resolve(processati)
                  }
                })
              stepGenerazioneDistinta.next();
            }
          }
        ).limit(CONFIG.LIMIT_DISTINTA ? CONFIG.LIMIT_DISTINTA : 0)

      }
      else
        async.each(prodotti,
          function (p, cb) {
            console.log('elaborazione prodotto singolo da lista di prodotti')
            elaboraSingoloProdotto(p);

            if (_.find(processati, function (el) {
              return p.codice === el;
            })) {

              //stepGenerazioneDistinta.pushLog('processato ' + p.codice);
              saveProdotto(p, req)
                .then(function (err) {
                  if (err) console.error(err);
                  console.log('eseguito update con distinta', itemsProcessed, prodotti.length)


                  itemsProcessed++;
                  if (itemsProcessed === prodotti.length) {
                    console.log('tutto finito??');
                    resolve(processati);
                  }
                  cb();
                })
            }
          });

      //console.log('dopo getProdotti')

    })
}

exports.update = function (req, res) {
  var modify = req.body;
  modify.mdate = new Date(),
    modify.last_user = req.user;
  var accessoriPerModello = modify.accessori;
  var accessoriPerModelloFiltered = [];
  Modello.findById(req.params.id,
    function (err, modello) {
      //console.log(err, 'Modifico ' + modello.nome);
      // genera distinta base a seconda delle regole che ci siamo detti nel modello

      modify.accessori.filter(function (acc) {
        if (acc && acc != 'undefined') {
          accessoriPerModelloFiltered.push(acc)
        }
        return true;
      })
      modify.accessori = accessoriPerModelloFiltered;
      modello.update(modify,
        {
          runValidators: true
        },
        function (err) {
          if (err)
            res.status(500).send(err);
          else {
            var accessoriPerModelloId = [];
            accessoriPerModelloFiltered = [];

            if (accessoriPerModello.length > 0) {
              accessoriPerModelloId = _.pluck(accessoriPerModello, '_id');


              accessoriPerModelloId.filter(function (acc) {
                if (acc && acc != 'undefined') {
                  accessoriPerModelloFiltered.push(acc)
                }
                return true;
              })
            };


            var modifyProdotto = {};
            modifyProdotto.mdate = new Date();
            modifyProdotto.accessori = accessoriPerModelloFiltered;
            Prodotto.update(
              { modello: modello._id },
              { $set: modifyProdotto },
              {
                runValidators: false,
                multi: true
              },
              function (err) {
                if (err)
                  res.status(500).send(err);
                else {
                  res.status(200).send();
                }
              })



          }
        }
      )
    })
}



/**
 exports.update = function(req, res)
{
    var modify = req.body;
    modify.mdate = new Date(),
    modify.last_user = req.user;
    var accessoriPerModello = modify.accessori; 
    
    Modello.findById(req.params.id, 
        function (err, modello) 
        {
            modello.update(modify, 
            {
                runValidators: true
            },
            function (err) 
            {
                if (err) 
                    res.status(500).send(err);
                else
                { 
                    var accessoriPerModelloId = [];
                    if (accessoriPerModello.length>0)
                           accessoriPerModelloId = _.pluck(accessoriPerModello,'_id');
                    Prodotto.find(  {modello : modello._id},    
                        function (err, prodotti) 
                        {
                            if (err)
                                res.status(500).send(err);
                            else
                            {
                                 async.each(prodotti, function(prodotto, cb)
                                 { 
                                    var modifyProdotto = prodotto;
                                        modifyProdotto.mdate = new Date();
                                        modifyProdotto.accessori = accessoriPerModelloId;
                                    prodotto.update(modifyProdotto, 
                                    {
                                        runValidators: false
                                    }, 
                                    function (err) 
                                    {
                                        if (err)
                                            res.status(500).send(err);
                                    })
                                    cb();
                                }, 
                                function(err)
                                {
                                    if(err)
                                        res.status(500).send(err);
                                })
                            }
                        }
                    )
                    res.status(200).send(modify);
                }
            }
        )
    })
}
 */
exports.delete = function (req, res) {
  Modello.remove(
    {
      _id: req.params.id
    },
    function (err) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).end();
    })
};

exports.countFn = (serie) => {
  return new Promise((resolve, reject) => {
    var result = { countModelli: 0 }
    var query = {}
  
    query.serie = serie
  
    // console.log(req.query, query)
  
    Modello.countDocuments(query)
      .exec(function (err, countModelli) {
        if (err)
          reject(err);
        else {
          result.countModelli = countModelli;
          resolve(result);
        }
      });
    })
}

exports.count = function (req, res) {
  var query = {}
  if (req.query.serie)
    query.serie = req.query.serie
  else
    query.serie = req.serieIlmas

  exports.countFn(query.serie).then((result) => {
    res.status(200).send(result);
  }).catch((err) => {
    res.status(500).send(err);
  })
  /* console.log(req.query, query)

  Modello.countDocuments(query)
    .exec(function (err, countModelli) {
      if (err)
        res.status(500).send(err);
      else {
        result.countModelli = countModelli;
        res.status(200).send(result);
      }
    }); */
}

exports.csvModelli = function (req, res) {
  console.log("start creazione excel");
  var fileName = "exportModelli.csv",
    filePath = process.cwd() + '/.tmp/';
  csvStream = csv.format({ headers: true }),
    writableStream = fs.createWriteStream(filePath + fileName);

  csvStream.pipe(writableStream);

  var query = {};
  var field = {
    _id: 1,
    nome: 1,
    peso: 1,
    volume: 1,
    ecolamp: 1
  };

  res.setHeader('Content-disposition', 'attachment; filename=exportModelli.csv');
  res.setHeader('content-type', 'text/csv');
  csvStream.pipe(res);

  Modello.find(query, field,
    function (err, result) {
      console.log(result);
      if (err)
        res.status(500).send(err);
      else {
        async.each(result,
          function (modello, _cb) {
            csvStream.write({
              _id: modello._id,
              nome: modello.nome,
              peso: modello.peso,
              volume: modello.volume,
              ecolamp: modello.ecolamp,
            });
            _cb(null);
          }, function (err) {
            console.log('qui dovrei aver finito');
            if (err)
              res.status(500).send(err);
            else {
              csvStream.end();
              //res.status(200).end();
            }
          });
      }
    }
  )
    //.lean()
    .populate('categoria', 'nome');

}
