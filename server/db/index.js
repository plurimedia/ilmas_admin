let fs = require('fs'),
    path = require('path'),
    mongoose = require('mongoose'), // mongoose wrapper for mongo
    mongoUrl,                       // Stringa di connessione per Mongo
    mongoOptions,                   // Opzioni di connessione per Mongo
    // certFileDB,                     // Certificato SSL
    CONFIG = require(process.cwd() + '/server/config.js')

/* @@@@@@ mongo @@@@@@ */
certFileDB = fs.readFileSync('./ibm_mongodb_ca.pem'); // Carica certificato SSL
// Controllo se le variabili di OpenShift sono popolate
if(process.env.MONGOUSERNAME && process.env.MONGOPWD && process.env.MONGOHOST && process.env.MONGODBNAME) {
    mongoUrl = "mongodb://" + process.env.MONGOUSERNAME +
                    ":" + process.env.MONGOPWD +
                    "@" + process.env.MONGOHOST +
                    "/" + process.env.MONGODBNAME

    if(process.env.MONGOOPTIONS) {
        var mongoCliOptions = process.env.MONGOOPTIONS
        // Quest'applicazione utilizza un container Docker custom, pertanto alcuni caratteri nelle variabili globali
        // vengono trascritti nel codice unicode ed è impossibile l'utilizzo del backslash (\) come funzione
        // di escape.
        mongoCliOptions  = mongoCliOptions.replace(/\\u0026/g, "&");
        mongoUrl = mongoUrl + "?" + mongoCliOptions
    }
} else {
    mongoUrl = CONFIG.MONGO // Variabile impostata nei config.js
}

mongoOptions = {
    sslValidate: true,
    tls: true,
    tlsCAFile: path.join(__dirname, "../../ibm_mongodb_ca.pem"),
    useUnifiedTopology: true,
    useNewUrlParser: true
}

mongoose.Promise = global.Promise;
// mongoose.set('debug', CONFIG.DEBUG);

exports.setup = () => {
    mongoose.connect(mongoUrl, mongoOptions, function (err) {
        if (err)
            throw err;

        console.info('connected to ', mongoUrl);
        console.info('@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ambiente', CONFIG.ENV);
        console.info('config', CONFIG);
    });
}
/* exports.setup = () => {
  let connectCount = 0
  connect = function () {
      connectCount++
      mongoOptions = {
          retryWrites: false,
          sslValidate: true
      };
      if (connectCount === 2) {
          delete mongoOptions.tls
          delete mongoOptions.tlsCAFile
          mongoOptions.sslCA = certFileDB
      } else {
          delete mongoOptions.sslCA
          mongoOptions.tls = true
          mongoOptions.tlsCAFile = path.join(__dirname, "../../ibm_mongodb_ca.pem")
      }
      console.info('connecting to ', mongoUrl);
      console.info('with options ', mongoOptions);
      mongoose.connect(mongoUrl, mongoOptions);
  }
  
  // Mongo connection
  connect();
  mongoose.connection.on('error', function (err) {
  console.info('failed to connect at', mongoUrl);
  if (connectCount !== 2)
      throw err;
  });
  mongoose.connection.on('disconnected', connect);
  mongoose.connection.on('open', function () {
  console.info('connected to ', mongoUrl);
  console.info('ambiente',CONFIG.ENV);
  console.info('config',CONFIG);
  });
  
  mongoose.set('debug', CONFIG.DEBUG);
  
} */
