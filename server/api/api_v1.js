"use strict";

const { parse } = require('path');
const { forEach } = require('underscore');

var mongoose = require('mongoose'),
  _ = require('underscore'),
  async = require('async'),
  Pdf = require(process.cwd() + '/server/routes/pdf.js'),
  Util = require(process.cwd() + '/server/routes/utils.js'),
  numeral = require('numeral'),
  s3 = require('s3'),
  s3Client = s3.createClient({
    s3Options: {
      accessKeyId: CONFIG.AWS_ACCESS_KEY_ID,
      secretAccessKey: CONFIG.AWS_SECRET_KEY,
      region: CONFIG.AWS_REGION
    },
  }),
  slugify = require('slugify'),
  Mustache = require('mustache'),
  fs = require('fs'),
  pdf = require('html-pdf'),
  moment = require('moment'),
  DwhRicerca = mongoose.model('DwhRicerca'),
  DwhSchedaTecnica = mongoose.model('DwhSchedaTecnica'),
  DwhSchedaDettaglio = mongoose.model('DwhSchedaDettaglio'),
  Categoria = mongoose.model('Categoria'),
  Modello = mongoose.model('Modello'),
  Prodotto = mongoose.model('Prodotto'),
  Agente = mongoose.model('Agente'),
  Evento = mongoose.model('Evento'),
  Installazione = mongoose.model('Installazione'),
  LineaCommercialeInfoProdotto = mongoose.model('LineaCommercialeInfoProdotto'),
  formatters = require('./../routes/utils.js').formatters,
  classeEnergetica = require('./../routes/utils.js').classeEnergetica,
  News = mongoose.model('News'),
  ApiLog = mongoose.model('ApiLog'),
  Colori = mongoose.model('Colori'),
  allCategorie = [];

var _splitDate = function (date, what) {
  if (what == 'anno')
    return moment(date, "YYYY-MM-DD").year()
  else if (what == 'mese')
    return moment(date, "YYYY-MM-DD").month()
  else if (what == 'giorno')
    return moment(date, "YYYY-MM-DD").date()
}

function isValidString(s) {
  return (s && s !== null && s !== '');
}

const getCategorie = () => {
  if (allCategorie?.length) return Promise.resolve(allCategorie)

  return new Promise((resolve, reject) => {
    Categoria.find({ pubblicato: true }, Util.fieldSitoCategoria).lean()
      .then((cats) => {
        cats.forEach(c => {
          delete c.last_user
          c.slug = slugify(c.nome || '', {
            replacement: '-',    // replace spaces with replacement
            lower: true          // result in lower case
          })
          c.slug_en = slugify(c.nome_en || '', {
            replacement: '-',    // replace spaces with replacement
            lower: true          // result in lower case
          })
        })
        allCategorie = cats
        resolve (cats)
      })
  })
}

function normalizzaDatiProdotto(prodotto) {
  if (prodotto.classe_isolamento === '1') // sistemo la classe per mustache (non posso fare campo = valore (logicless))
    prodotto.classe1 = true;

  if (prodotto.classe_isolamento === '2')
    prodotto.classe2 = true;

  if (prodotto.classe_isolamento === '3')
    prodotto.classe3 = true;

  if (prodotto.descrizione_applicazione || prodotto.orientabile || prodotto.inclinabile)
    prodotto.mostra_descrizione_applicazione = true;

  if (prodotto.foro === '')
    delete prodotto.foro;

  if (prodotto.categoria &&
    (prodotto.categoria.nome.indexOf('Incassi') !== -1 ||
      prodotto.categoria.nome.indexOf('incassi') !== -1)) // per stampare l'icona degli incassi
    prodotto.incassi = true;

  if (prodotto.foro && prodotto.foro.indexOf('x') !== -1 || prodotto.foro && prodotto.foro.indexOf('X') !== -1)
    prodotto.foro_quadrato = true;

  if (prodotto.foro && prodotto.foro.indexOf('x') === -1 && prodotto.foro && prodotto.foro.indexOf('X') === -1)
    prodotto.foro_tondo = true;

  if (prodotto.accessori && prodotto.accessori.length && Array.isArray(prodotto.accessori))
    prodotto.mostraAccessori = true;

  prodotto = Util.sistemaIcone(prodotto);
  prodotto = classeEnergetica(prodotto);

  return prodotto;
}

exports.categoriaList = function (req, res) {
  var query = { pubblicato: true }
  if (req.query.food)
    _.extend(query, { food: true })

  if (req.query.serie)
    query.serie = req.query.serie
  else
    query.serie = req.serieIlmas

  Categoria.find(query,
    Util.fieldSitoCategoria,
    function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results)
    }
  ).sort({ ordinamento_sito: 1 })
}

exports.categoriaEstroList = function (req, res) {
  var query = { linea: "estro" }

  Categoria.find(query,
    // Util.fieldSitoCategoria,
    function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results)
    }
  ).sort({ ordinamento_sito: 1 })
}

/*
    categoria di una linea (ilmas, toshiba ...)
*/
exports.categorieLinea = function (req, res) {
  var query = { linea: req.params.linea }
  if (req.query.serie)
    query.serie = req.query.serie
  else
    query.serie = req.serieIlmas

  Categoria.find(query, function (err, result) {
    if (err)
      res.status(500).send(err);
    else
      res.status(200).json(result)
  }).sort({ nome: 1 })
}

/*
categorie per linea e uso
*/
exports.categorieLineaUso = function (req, res) {
  var query = {
    linea: req.params.linea, uso: req.params.uso
  }
  if (req.query.serie)
    query.serie = req.query.serie
  else
    query.serie = req.serieIlmas

  Categoria.find(query, function (err, result) {
    if (err) {
      console.log("error:", err);
      res.status(500).send(err);
    }
    else
      res.status(200).json(result);
  }).sort({ nome: 1 })
}

/*
modelli per linea e uso
*/
exports.modelliLineaUso = function (req, res) {
  Modello.find({ linea: req.params.linea, uso: req.params.uso }, function (err, result) {
    if (err) {
      console.log("error:", err);
      res.status(500).send(err);
    }
    else
      res.status(200).json(result);
  }).sort({ nome: 1 })
}

/*
prodotti per linea e uso
*/
exports.prodottiLineaUso = function (req, res) {
  var query = {
    linea: req.params.linea,
    destinazione: req.params.uso,
    pubblicato: true,
    categoria: req.params.categoria,
    modello: req.params.modello
  };

  var field = {
    codice: 1,
    foto: 1,
    fotoQuotata: 1,
    disegno: 1,
    fotometria: 1,
    prezzo: 1,
    qtamin: 1,
    tradizionale: 1,
    k: 1,
    lm: 1,
    w: 1,
    ma: 1,
    v: 1,
    hz: 1,
    fascio: 1,
    irc: 1,
    dimmerabile: 1,
    durata: 1,
    classe_energetica: 1,
    colore: 1,
    corpo_it: 1,
    corpo_en: 1,
    orientabile: 1,
    f: 1,
    ip: 1,
    ce: 1,
    classe_isolamento: 1,
    ottica_it: 1,
    ottica_en: 1,
    adattatore_it: 1,
    adattatore_en: 1,
    verniciatura: 1,
    descrizione_applicazione: 1,
    note: 1
  }

  Prodotto.count(query,
    function (err, count) {
      if (err) {
        console.log("error:", err);
        res.status(500).send(err);
      }
      else {
        var limit = 100;

        var cycleCount = Math.floor(count / limit);

        if ((count % limit) > 0) cycleCount = cycleCount + 1;

        var index = 0,
          resultTotale = [];

        async.times(cycleCount,

          function (iteree, cb) {

            Prodotto.find(query, field,
              function (err, resultParziale) {
                if (err) {
                  console.log("error:", err);
                  res.status(500).send(err);
                }
                else {
                  async.each(resultParziale,
                    function (prodotto) {
                      if (prodotto.prezzo)
                        prodotto.prezzo = '€' + numeral(prodotto.prezzo).format('€0,0.00');
                    }
                  )
                  resultTotale = _.union(resultTotale, resultParziale);
                  cb();
                }
              }).lean().skip((index++) * limit).limit(limit).sort({ codice: 1 });
          },
          function (err, result) {
            if (err) {
              console.log("error:", err);
              res.status(500).send(err);
            }
            else
              res.status(200).json(resultTotale);
          }
        )
      }
    }
  );
}

/*
prodotti per linea e uso
*/
exports.countProdottiLineaUso = function (req, res) {
  var query = {
    linea: req.params.linea,
    destinazione: req.params.uso,
    pubblicato: true,
    categoria: req.params.categoria,
    modello: req.params.modello
  };

  Prodotto.count(query,
    function (err, count) {
      if (err) {
        console.log("error:", err);
        res.status(500).send(err);
      }
      else
        res.status(200).json({ count: count });
    }
  );
}

let cache = {
  CategoriaIdProfili: null,
  CategoriaIdFonti: null
}
exports.getCategoriaIdProfili = function (req, res) {
  if (cache.CategoriaIdProfili) {
    res.status(200).send(cache.CategoriaIdProfili);
    return
  }

  Categoria.findOne({
    nome: "Profili"
  })
    .then((result) => {
      cache.CategoriaIdProfili = result._id
      res.status(200).send(result._id);
    })
    .catch(err => {
      console.log("Err on getCategoriaIdProfili", err);
      res.status(500).send(err);
    })
}

exports.getCategoriaIdFonti = function (req, res) {
  if (cache.CategoriaIdFonti) {
    res.status(200).send(cache.CategoriaIdFonti);
    return
  }

  Categoria.findOne({
    nome: "Fonti luminose"
  })
    .then((result) => {
      cache.CategoriaIdFonti = result._id
      res.status(200).send(result._id);
    })
    .catch(err => {
      console.log("Err on getCategoriaIdFonti", err);
      res.status(500).send(err);
    })
}

exports.getApplicazioni = function (req, res) {
  const idProfili = req.query.id;
  /*  Modello.aggregate(
       [
           { 
               "$lookup" : { 
                   "from" : "Categoria", 
                   "localField" : "categoria", 
                   "foreignField" : "_id", 
                   "as" : "categoria"
               }
           }, 
           { 
               "$unwind" : { 
                   "path" : "$categoria"
               }
           }, 
           { 
               "$match" : { 
                   "pubblicato" : true, 
                   "categoria.nome" : "Profili"
               }
           }
       ]
   ) */
  Modello.find({
    pubblicato: true,
    categoria: mongoose.Types.ObjectId(idProfili)
  })
    .sort({ ordinamento_sito: 1 })
    .then((result) => {
      res.status(200).send(result);
    })
    .catch(err => {
      console.log("Err on getApplicazioni", err);
      res.status(500).send(err);
    })
}

exports.getProfili = function (req, res) {
  const modelloId = req.query.id;
  // 16/2/2023: ho tolto il filtro per pubblicato perché
  // pare che Mongo non usasse l'indice: lo faccio in js
  Prodotto.find({
    modello: mongoose.Types.ObjectId(modelloId)
  })
    .sort({ codice: 1 })
    .then((result) => {
      res.status(200).send(result.filter(p => p.pubblicato));
    })
    .catch(err => {
      console.log("Err on getProfili", err);
      res.status(500).send(err);
    })
}

function filterDiffusoriWithRules(array, codice) {
  let filteredArray = [];
  _.forEach(array, (el) => {
    if (rulesProfili(codice, el)) {
      if (!hasElement(filteredArray, el.modello))
        filteredArray.push(el.modello);
    }
  });

  return filteredArray;
}

function filterLedWithRules(array, codice) {
  let filteredArray = [];
  _.forEach(array, (el) => {
    if (rulesProfili(codice, el)) {
      filteredArray.push(el);
    }
  });

  return filteredArray;
}

function hasElement(array, element) {
  let length = array.length;
  let index = 0;
  let flag = false;
  _.forEach(array, (el) => {
    if (el._id.equals(element._id)) {
      flag = true;
    }
    index++;
  });
  if (index === length)
    return flag;
}

function rulesProfili(codice, element) {

  let rule1 = ["JA", "JB", "JD", "JE", "JF", "JY", "JV", "JI", "JL", "KE", "JC", "KP", "HC", "KN", "HG", "JG", "JH", "JO", "JN", "JP", "KO", "JQ", "JS", "HS", "KQ", "KS", "AW", "BW", "MW", "KR", "HR", "KT", "JM"];
  let subRule1 = ["F", "D"]

  let rule2 = ["JX", "JZ", "JW", "KZ", "KC", "KD"];
  let subRule2_1 = ["S", "D", "R"]
  let subRule2_2 = ["L", "E"]
  let subRule2_3 = ["S0"]

  let rule3 = ["SA", "SZ", "SB", "SE", "SD", "SY", "SC", "SR", "SG", "SJ", "WA", "WB", "SM", "CR", "AR"];
  let subRule3_1 = ["T", "R"]
  let subRule3_2 = ["L", "E", "A", "B", "D", "F", "G", "H", "O", "P", "R", "T"]
  let subRule3_3 = ["S0"]

  let rule4 = ["KF", "KH", "KI", "KV", "KM", "KG", "KL"];
  let subRule4_1 = ["F", "D", "T", "R"]
  let subRule4_2 = ["L", "E", "A", "B", "D", "F", "G", "H", "O", "P", "R", "T"]
  let subRule4_3 = ["S0"]

  let rule4_1 = ["KX"];
  let subRule4_1_1 = ["F", "D", "T", "R", "I"]


  let rule5 = ["JT"];
  let subRule5_1 = ["T", "R"]
  let subRule5_2 = ["L", "E", "A", "D", "F", "G", "H", "P", "R", "T"]
  let subRule5_3 = ["S0"]

  let rule6 = ["MA", "MC", "MV", "ME"];
  let subRule6_1 = ["S", "D", "T", "R", "I"]
  let subRule6_2 = ["L"]
  let subRule6_3 = ["S0"]

  let rule7 = ["HB", "JR", "JJ", "KY", "HW"];
  let subRule7_1 = ["F", "D"]
  let subRule7_2 = ["R"]
  let subRule7_3 = ["I"]

  let rule8 = ["JI", "JV", "JL", "KE"];
  let subRule8 = ["R"]

  let rule9 = ["JT", "SA", "SB", "SC", "SD", "SE", "SY", "SM", "SZ", "SR", "SG", "SJ", "WA", "WB", "KF", "KH", "KV", "KI", "KM", "KG", "KX", "KL", "CR", "AR", "JZ", "KZ", "JW", "KN"]
  let subRule9_1 = ["I", "S"]

  let rule10 = ["JT", "CR", "AR"]
  let subRule10_1 = ["I"]

  if (_.includes(rule1, codice)) {
    //escludi 1 carattere F o D
    if (_.includes(subRule1, element.codice.charAt(0)))
      return false;

  }
  if (_.includes(rule2, codice)) {
    //escludi 1 carattere S,D o R
    //2 carattere L o E
    //primi 2 caratteri S0
    if (_.includes(subRule2_1, element.codice.charAt(0)))
      return false;
    else if (_.includes(subRule2_2, element.codice.charAt(1)))
      return false;
    else if (_.includes(subRule2_3, element.codice.substr(0, 2)))
      return false;

  }
  if (_.includes(rule3, codice)) {
    //escludi 1 carattere T o R
    //2 carattere "L","E","A","B","D","F","G","H","O","P","R","T"
    //primi 2 caratteri S0
    if (_.includes(subRule3_1, element.codice.charAt(0)))
      return false;
    else if (_.includes(subRule3_2, element.codice.charAt(1)))
      return false;
    else if (_.includes(subRule3_3, element.codice.substr(0, 2)))
      return false;
  }
  if (_.includes(rule4, codice)) {
    //escludi 1 carattere F,D,T o R
    //2 carattere "L","E","A","B","D","F","G","H","O","P","R","T"
    //primi 2 caratteri S0
    if (_.includes(subRule4_1, element.codice.charAt(0)))
      return false;
    else if (_.includes(subRule4_2, element.codice.charAt(1)))
      return false;
    else if (_.includes(subRule4_3, element.codice.substr(0, 2)))
      return false;

  }
  if (_.includes(rule4_1, codice)) {
    //escludi 1 carattere F,D,T, R o I
    //2 carattere "L","E","A","B","D","F","G","H","O","P","R","T"
    //primi 2 caratteri S0
    if (_.includes(subRule4_1_1, element.codice.charAt(0)))
      return false;
    else if (_.includes(subRule4_2, element.codice.charAt(1)))
      return false;
    else if (_.includes(subRule4_3, element.codice.substr(0, 2)))
      return false;
  }
  if (_.includes(rule5, codice)) {
    //escludi 1 carattere T o R
    //2 carattere "L","E","A","D","F","G","H","P","R","T"
    //primi 2 caratteri S0
    if (_.includes(subRule5_1, element.codice.charAt(0)))
      return false;
    else if (_.includes(subRule5_2, element.codice.charAt(1)))
      return false;
    else if (_.includes(subRule5_3, element.codice.substr(0, 2)))
      return false;

  }
  if (_.includes(rule6, codice)) {
    //escludi 1 carattere "S","D","T","R","I"
    //2 carattere "L"
    //primi 2 caratteri "S0"
    if (_.includes(subRule6_1, element.codice.charAt(0)))
      return false;
    else if (_.includes(subRule6_2, element.codice.charAt(1)))
      return false;
    else if (_.includes(subRule6_3, element.codice.substr(0, 2)))
      return false;
  }
  if (_.includes(rule7, codice)) {
    //escludi 1 carattere F o D
    if (_.includes(subRule7_1, element.codice.charAt(0)))
      return false;
    //escludi 1 carattere R e contemporaneamente 4 carattere I
    if (_.includes(subRule7_2, element.codice.charAt(0)) &&
      _.includes(subRule7_3, element.codice.charAt(3)))
      return false;
  }

  if (_.includes(rule8, codice)) {
    //esclude primo carattere R
    if (_.includes(subRule8, element.codice.charAt(0)))
      return false;
  }

  if (_.includes(rule9, codice)) {
    // esclude 2 carattere I e 2 carattere S
    if (_.includes(subRule9_1, element.codice.charAt(1)))
      return false;
  }

  if (_.includes(rule10, codice)) {
    // esclude primo carattere I
    if (_.includes(subRule10_1, element.codice.charAt(0)))
      return false;
  }

  return true;

}

exports.getDiffusori = function (req, res) {
  const codice = req.query.codice;
  const idFonti = req.query.id;
  /* Prodotto.aggregate(
      [
          { 
              "$lookup" : { 
                  "from" : "Categoria", 
                  "localField" : "categoria", 
                  "foreignField" : "_id", 
                  "as" : "categoria"
              }
          }, 
          { 
              "$unwind" : { 
                  "path" : "$categoria"
              }
          }, 
          { 
              "$match" : { 
                  "pubblicato" : true, 
                  "categoria.nome" : "Fonti luminose"
              }
          }, 
          { 
              "$lookup" : { 
                  "from" : "Modello", 
                  "localField" : "modello", 
                  "foreignField" : "_id", 
                  "as" : "modello"
              }
          }, 
          { 
              "$unwind" : { 
                  "path" : "$modello"
              }
          }
      ]
  ) */
  /*  Prodotto.find({
       pubblicato : true, 
       categoria: idFonti
   }) */
  Prodotto.aggregate(
    [
      {
        "$match": {
          "pubblicato": true,
          "categoria": mongoose.Types.ObjectId(idFonti)
        }
      },
      {
        "$lookup": {
          "from": "Modello",
          "localField": "modello",
          "foreignField": "_id",
          "as": "modello"
        }
      },
      {
        "$unwind": {
          "path": "$modello"
        }
      },
      {
        "$sort": {
          "modello.ordinamento_sito": 1
        }
      }
    ]
  )
    .then((result) => {
      let newResult = filterDiffusoriWithRules(result, codice);
      res.status(200).send(newResult);
    })
    .catch(err => {
      console.log("Err on getDiffusori", err);
      res.status(500).send(err);
    })
}

exports.getLedTemperatura = function (req, res) {
  const id = req.query.id;
  const codiceProfilo = req.query.codiceProfilo;
  /* Prodotto.aggregate(
      [
          { 
              "$lookup" : { 
                  "from" : "Modello", 
                  "localField" : "modello", 
                  "foreignField" : "_id", 
                  "as" : "modello"
              }
          }, 
          { 
              "$unwind" : { 
                  "path" : "$modello"
              }
          }, 
          { 
              "$match" : { 
                  "pubblicato" : true, 
                  "modello._id" : mongoose.Types.ObjectId(id)
              }
          },
          {
              "$project" : {
                  "k" : "$k"
              }
          }
      ] 
  )*/
  Prodotto.find({
    pubblicato: true,
    modello: mongoose.Types.ObjectId(id)
  })
    .then((resultBase) => {
      let result = filterLedWithRules(resultBase, codiceProfilo);

      let arrayToSend = [];

      let arrayTemperaturaTmp = _.map(result, (obj) => {
        return obj.k;
      });

      let arrayTemperatura = _.uniq(arrayTemperaturaTmp);

      let arrTemp = arrayTemperatura.filter(item => {
        return item !== "" && !_.isNull(item) && !_.isUndefined(item);
      });
      arrayToSend.push(arrTemp);

      console.log("arrayToSend: ", arrayToSend);
      res.status(200).send(arrayToSend);
    })
    .catch(err => {
      console.log("Err on getLedTemperatura", err);
      res.status(500).send(err);
    })

}

function checkExists(array, value) {
  let flag = false;
  if (array.length > 0) {
    _.forEach(array, (obj) => {
      if (obj.w === value.w) {
        if (obj.led_it === value.led_it) {
          if (obj.irc === value.irc) {
            flag = true;
          }
        }
      }
    });
  }
  return flag;
}

function formatW(str) {
  if (typeof str === 'string') {
    str = str.replace(/,/g, '.');
  }
  return str;
}

exports.getLedPotenza = function (req, res) {
  let id = req.query.id;
  let codiceProfilo = req.query.codiceProfilo;
  let k = req.query.k;
  /* Prodotto.aggregate(
      [
          { 
              "$lookup" : { 
                  "from" : "Modello", 
                  "localField" : "modello", 
                  "foreignField" : "_id", 
                  "as" : "modello"
              }
          }, 
          { 
              "$unwind" : { 
                  "path" : "$modello"
              }
          }, 
          { 
              "$match" : { 
                  "pubblicato" : true, 
                  "modello._id" : mongoose.Types.ObjectId(id),
                  "k": k
              }
          },
          {
              "$project" : {                   
                  "potenza" : {
                      "w" : "$w",
                      "led_it" : "$led_it",
                      "led_en" : "$led_en",
                      "irc" : "$irc"
                  }
              }
          }
      ]
  ) */
  Prodotto.find({
    pubblicato: true,
    modello: mongoose.Types.ObjectId(id),
    k: k
  })
    .then((resultBase) => {
      let result = filterLedWithRules(resultBase, codiceProfilo);

      let arrayToSend = [];
      let arrayPotenzaTmp = _.map(result, (obj) => {
        let objPotenza = {};
        objPotenza.w = parseFloat(formatW(obj.w));
        objPotenza.led_it = obj.led_it;
        objPotenza.led_en = obj.led_en;
        objPotenza.irc = obj.irc;

        return objPotenza;
      });

      let arrayPotenza = [];

      _.forEach(arrayPotenzaTmp, (obj) => {
        if (!checkExists(arrayPotenza, obj)) {
          arrayPotenza.push(obj);
        }

      });

      arrayToSend.push(arrayPotenza);
      console.log("arrayToSend: ", arrayToSend);

      res.status(200).send(arrayToSend);
    })
    .catch(err => {
      console.log("Err on getLedPotenza", err);
      res.status(500).send(err);
    })
}

/* exports.getLedPotenza2 = function (req, res) {
    let id = req.query.id;
    let k = req.query.k;
    Prodotto.aggregate(
        [
            { 
                "$lookup" : { 
                    "from" : "Modello", 
                    "localField" : "modello", 
                    "foreignField" : "_id", 
                    "as" : "modello"
                }
            }, 
            { 
                "$unwind" : { 
                    "path" : "$modello"
                }
            }, 
            { 
                "$match" : { 
                    "pubblicato" : true, 
                    "modello._id" : mongoose.Types.ObjectId(id),
                    "k": k
                }
            },
            {
                "$project" : {                   
                    "potenza" : {
                        "w" : "$w",
                        "led_it" : "$led_it",
                        "led_en" : "$led_en",
                        "irc" : "$irc"
                    }
                }
            }
        ]
    )
    .then((result) =>{
        let arrayToSend = [];
        let arrayPotenzaTmp = _.map(result, (obj) => {
            return obj.potenza;
        });

        let arrayPotenza = [];

        _.forEach(arrayPotenzaTmp, (obj) => {
            if (!checkExists(arrayPotenza, obj)) {
                arrayPotenza.push(obj);
            }

        });

        let arrPotenza = arrayPotenza;
        arrPotenza = arrPotenza.filter(item => item);

        let newObjArray = arrPotenza.map(function(obj) {
            for (var i in obj) { 
              if (typeof obj[i] === 'string') {
                obj[i] = obj[i].replace(/,/g, '.');
              }
            }
            return obj;
        });

        newObjArray.forEach( obj => {
            obj.w = parseFloat(obj.w)
        })
        
        arrayToSend.push(arrPotenza);
        console.log("arrayToSend: ", arrayToSend);

        res.status(200).send(arrayToSend);
    })
    .catch(err =>{
        console.log("Err on getLed", err);
        res.status(500).send(err);
    })
} */

function reFormatW(str) {
  if (typeof str === 'string') {
    str = str.replace('.', ',');
  }
  return str;
}

exports.getLedFascio = function (req, res) {
  let id = req.query.id;
  let codiceProfilo = req.query.codiceProfilo;
  let k = req.query.k;
  let w = reFormatW(req.query.w);
  let led_it = req.query.led_it;
  /* Prodotto.aggregate(
      [
          { 
              "$lookup" : { 
                  "from" : "Modello", 
                  "localField" : "modello", 
                  "foreignField" : "_id", 
                  "as" : "modello"
              }
          }, 
          { 
              "$unwind" : { 
                  "path" : "$modello"
              }
          }, 
          { 
              "$match" : { 
                  "pubblicato" : true, 
                  "modello._id" : mongoose.Types.ObjectId(id),
                  "k": k,
                  "w": potenza.w,
                  "led_it": potenza.led_it
                 // "irc"
              }
          },
          {
              "$project" : {
                  "fascio" : "$fascio"
              }
          }
      ]
  ) */
  Prodotto.find({
    pubblicato: true,
    modello: mongoose.Types.ObjectId(id),
    k: k,
    w: w,
    led_it: led_it
  })
    .then((resultBase) => {
      let result = filterLedWithRules(resultBase, codiceProfilo);

      let arrayToSend = [];
      let arrayFascioTmp = _.map(result, (obj) => {
        return obj.fascio;
      });

      let arrayFascio = _.uniq(arrayFascioTmp);

      let arrFascio = arrayFascio.filter(item => {
        return item !== "" && !_.isNull(item) && !_.isUndefined(item);
      });

      arrayToSend.push(arrFascio);

      res.status(200).send(arrayToSend);
    })
    .catch(err => {
      console.log("Err on getLedFascio", err);
      res.status(500).send(err);
    })
}

function rulesTagli(codiceParam) {
  const codice = codiceParam.toUpperCase();
  let taglio = 0;

  if (codice.charAt(1) === 'A'
    || codice.charAt(1) === 'X') {
    taglio = 35.8;
  }
  else if (codice.charAt(1) === 'B'
    || codice.charAt(1) === 'C'
    || codice.charAt(1) === 'D'
    || codice.charAt(1) === 'E'
    || codice.charAt(1) === 'F') {
    taglio = 35;
  }
  else if (codice.charAt(1) === 'H'
    || codice.charAt(1) === 'O'
    || codice.charAt(1) === 'P') {
    taglio = 100;
  }
  else if (codice.charAt(1) === 'G'
    || codice.charAt(1) === 'M') {
    taglio = 50;
  }
  else if (codice.charAt(1) === 'T') {
    taglio = 62.5;
  }
  else if (codice.charAt(1) === 'R'
    || codice.startsWith('OI')
    || codice.startsWith('TI')) {
    taglio = 25;
  }
  else if (codice.charAt(1) === 'V') {
    taglio = 29;
  }
  else if (codice.startsWith('OS')
    || codice.startsWith('TS')) {
    taglio = 42;
  }
  else if (codice.charAt(1) === 'L'
    || codice.startsWith('S0')) {
    taglio = 140;
  }
  else if (codice.startsWith('I0')) {
    taglio = 10;
  }

  return taglio;
}

exports.getFonteLuminosaCode = function (req, res) {
  const query = { modello: req.query.modelloId };

  if (!_.isNull(req.query.k) && !_.isUndefined(req.query.k) && req.query.k !== "")
    query.k = req.query.k;

  if (!_.isNull(req.query.w) && !_.isUndefined(req.query.w) && req.query.w !== "") {
    let str = req.query.w;
    if (str.indexOf(".") !== -1)
      str = str.replace(".", ",");
    query.w = str;
  }

  if (!_.isNull(req.query.led_it) && !_.isUndefined(req.query.led_it) && req.query.led_it !== "")
    query.led_it = req.query.led_it;

  if (!_.isNull(req.query.irc) && !_.isUndefined(req.query.irc) && req.query.irc !== "")
    query.irc = req.query.irc;

  if (!_.isNull(req.query.fascio) && !_.isUndefined(req.query.fascio) && req.query.fascio !== "")
    query.fascio = req.query.fascio;

  Prodotto.find(query)
    .then((result) => {
      if (result.length === 0) {
        let str = query.w;
        if (str.indexOf(",") !== -1) str = str.replace(",", ".");
        query.w = str;

        Prodotto.find(query)
          .then((result2) => {
            const risultato = result2[0] ? result2[0].toObject() : [];
            let taglio = rulesTagli(risultato.codice);
            risultato.taglio = taglio;

            res.status(200).send(risultato);
          })
          .catch((err) => {
            console.log("err on getFonteLuminosaCode");
            res.status(500).send(err);
          })
      }
      else {
        const risultato = result[0] ? result[0].toObject() : [];
        let taglio = rulesTagli(risultato.codice);
        risultato.taglio = taglio;

        console.log("Result taglio: ", taglio, risultato);

        res.status(200).send(risultato);
      }
    })
    .catch((err) => {
      console.log("err on getFonteLuminosaCode");
      res.status(500).send(err);
    })
}

function rulesColori(arrayColors, codiceProfiloParam, codiceFonteParam, finituraProfilo) {
  const codiceProfilo = codiceProfiloParam.toUpperCase();
  const codiceFonte = codiceFonteParam.toUpperCase();
  let _arrayColors = arrayColors;
  let newArray = [];
  let result = [];

  if (codiceProfilo.startsWith('KX')) {
    let tmp = _.where(_arrayColors, { codice: "00" });
    newArray.push(tmp[0]);
  }
  else if (codiceProfilo.startsWith('JT')) {
    let tmp = _.where(_arrayColors, { codice: "0A" });
    newArray.push(tmp[0]);
  }
  else {
    newArray = _.without(_arrayColors, _.findWhere(_arrayColors, { codice: "00" }));
  }

  if (newArray.length > 1) {
    if (codiceFonte.startsWith('O')
      || codiceFonte.startsWith('T')) {
      result = _.filter(newArray, function (el) {
        return el.codice.startsWith("0");
      })
    }
    else {
      result = _.filter(newArray, function (el) {
        return !el.codice.startsWith("0");
      })
    }
  }

  if (result.length === 0)
    result = newArray;

  if (finituraProfilo) {
    const tmp = result;
    result = _.filter(tmp, function (el) {
      //se mi arriva la finituraProfilo principale restituisco solo finiture profilo con
      //la stessa lettera. (es. arriva 0B, prende tutte le finiture xB)
      return el.codice.charAt(1) === finituraProfilo.charAt(1);
    })
  }

  return result;

}

exports.getFiniture = function (req, res) {
  const codiceProfilo = req.query.codiceProfilo;
  const codiceFonte = req.query.codiceFonte;
  const finituraProfilo = req.query.finituraProfilo;

  Colori.find().sort({ ordinamento: 1 })
    .then((result) => {
      const newResult = rulesColori(result, codiceProfilo, codiceFonte, finituraProfilo);
      res.status(200).send(newResult);
    })
    .catch(err => {
      console.log("Err on getFiniture", err);
      res.status(500).send(err);
    })

}

exports.modelliCategoriaOLD = function (req, res) {
  console.log("AAAA exports.modelliCategoria ", req.query, req.serieIlmas, req.params)
  var query = { pubblicato: true }
  if (req.query.food) {
    _.extend(query, { food: true })
  }
  if (req.query.serie)
    query.serie = req.query.serie
  else
    query.serie = req.serieIlmas

  Modello.find(query)
    .populate('categoria', Util.fieldSitoCategoria)
    .exec(function (err, result) {
      if (err) {
        res.status(500).send(err);
  console.log("BBBB exports.modelliCategoria ", err)
        return;
      }

      var modelli = _.sortBy(_.filter(result, function (modello) {
        if (modello && modello.categoria) {
          try {
            var catSlugIt = slugify(modello.categoria.nome || '', {
              replacement: '-',    // replace spaces with replacement
              lower: true          // result in lower case
            })
            var catSlugEn = slugify(modello.categoria.nome_en || '', {
              replacement: '-',    // replace spaces with replacement
              lower: true          // result in lower case
            })
            return (catSlugIt === req.params.categoria || catSlugEn === req.params.categoria) && modello.pubblicato
          }
          catch (exception) {
            console.log("errore in exports.modelliCategoria ", exception)
          }
        }
      }
      ), function (m) {
        return m.ordinamento_sito
      })

      var modelliNoDatiSensibili = JSON.parse(JSON.stringify(modelli));
      modelliNoDatiSensibili = modelliNoDatiSensibili.filter(function (m) {
        delete m.configDistintaBase
        delete m.last_user
        delete m.categoria.last_user
        return true;
      });
      console.log("resultold", modelliNoDatiSensibili[0])
      res.status(200).json(modelliNoDatiSensibili)
    })
}

exports.modelliCategoria = async function (req, res) {
  var query = { pubblicato: true }
  if (req.query.food) _.extend(query, { food: true })
  if (req.query.serie)
    query.serie = req.query.serie
  else
    query.serie = req.serieIlmas

  const categorie = await getCategorie()
  const categoria = categorie.find(c => c.slug === req.params.categoria || c.slug_en === req.params.categoria)
  if (!categoria) {
    console.error('Categoria non trovata', req.params.categoria)
    res.status(200).json([])
    return
  }
  query.categoria = categoria._id

  Modello.find(query).lean()
    .exec(function (err, result) {
      if (err) {
        res.status(500).send(err)
        return
      }

      result = _.sortBy(result, 'ordinamento_sito')

      result.forEach(m => {
        delete m.configDistintaBase
        delete m.last_user
        m.categoria = categoria
      })

      res.status(200).json(result)
    })
}

exports.modello = function (req, res) {
  Modello.findOne({ _id: req.params.idmodello }, function (err, result) {
    if (err)
      res.status(500).send(err);
    else
      res.status(200).json(result);
  })
}

/* Cerca un modello in Like*/
exports.cercaModello = function (req, res) {
  var query = { $and: [{ $or: [{ nome: { $regex: req.query.nome, $options: 'i' } }, { nome: req.query.nome }] }] }
  /*if (req.query.linea)
      query.$and.push({ linea: req.query.linea })
  else
      query.$and.push({ linea: { $ne: 'estro' } })*/
  if (req.query.serie)
    query.$and.push({ serie: req.serieEstro })
  else
    query.$and.push({ serie: req.serieIlmas })

  Modello.find(
    query,
    function (err, result) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(result);
    })
    .populate('categoria');
}

exports.codiciModelloOLD = function (req, res) {
  // console.log('exports.codiciModello 1', req.params.modello)
  Prodotto.find({ modello: req.params.modello, pubblicato: true },
    Util.fieldSitoProdotto,
    {
      keywords: { $in: Util.keyUltimeGenerazioni({ prefix: 'true' }) }
    })
    .populate('modello', Util.fieldSitoModello)
    .populate('categoria', Util.fieldSitoCategoria)
    .exec(function (err, prodotti) {
      if (err) {
        console.log('ERRORE riga 1248', err)
        res.status(500).send(err);
      } else {
        // console.log('exports.codiciModello 2', prodotti.length, prodotti[0])
        Util.fieldSitoModello.categoria = 1;
        Modello.findOne({ _id: req.params.modello }, Util.fieldSitoModello)
          .populate('categoria', Util.fieldSitoCategoria)
          .exec(function (err, modello) {
            if (err) {
              console.log('ERRORE riga 1259', err)
              res.status(500).send(err);
            }
            else {
              var result = {}
              result.modello = modello
              result.prodotti = prodotti
              res.status(200).json(result);
            }
          })
      }
    })
}

exports.codiciModello = function (req, res) {
  // console.log('exports.codiciModello new 1', req.params.modello)
  // 22/2/2023: ho tolto il filtro per pubblicato perché
  // pare che Mongo non usasse l'indice: lo faccio in js
  Prodotto.find({ modello: req.params.modello },
    Util.fieldSitoProdotto,
    {
      keywords: { $in: Util.keyUltimeGenerazioni({ prefix: 'true' }) }
    })
    .lean()
    .exec(function (err, prodotti) {
      if (err) {
        console.log('ERRORE riga 1248', err)
        res.status(500).send(err);
      } else {
        Util.fieldSitoModello.categoria = 1;
        Modello.findOne({ _id: req.params.modello }, Util.fieldSitoModello)
          .populate('categoria', Util.fieldSitoCategoria)
          .exec(function (err, modello) {
            if (err) {
              console.log('ERRORE riga 1259', err)
              res.status(500).send(err);
            }
            else {
              prodotti = prodotti.filter(p => p.pubblicato);
              var result = {}
              result.modello = modello
              prodotti.forEach(p => {
                p.categoria = modello.categoria
                p.modello = modello
              })

              // console.log('exports.codiciModello new 2', prodotti.length, prodotti[0])
              result.prodotti = prodotti
              res.status(200).json(result);
            }
          })
      }
    })
}

/* utilizzata al momento da autocomplete ricerca scheda tecnica */
exports.searchProdotti = function (req, res) {
  var search = req.params.search.split(' '),
    query = { codice: { $all: search }, pubblicato: true };

  LineaCommercialeInfoProdotto.findOne({ codiceLinea: search })
    .exec(function (err, lineaCommercialeInfoProdotto) {
      if (err)
        res.status(500).send(err);
      else {
        if (lineaCommercialeInfoProdotto)
          query = { _id: lineaCommercialeInfoProdotto.prodotto };

        Prodotto.find(query)
          .populate('modello', 'nome')
          .limit(10)
          //.sort({codice: -1})
          .lean()
          .exec(
            function (err, results) {
              if (err)
                res.status(500).send(err);
              else {
                results.forEach(
                  function (prodotto) {
                    if (lineaCommercialeInfoProdotto && isValidString(lineaCommercialeInfoProdotto.codiceLinea)) {
                      prodotto.codiceLinea = lineaCommercialeInfoProdotto.codiceLinea;
                      prodotto.codiceFound = lineaCommercialeInfoProdotto.codiceLinea;
                    }
                    else
                      prodotto.codiceFound = prodotto.codice
                  }
                )
                res.status(200).send(results);
              }
            }
          )
      }
    })
}

//usata da ricerca di ilmas_app
exports.search = function (req, res) {
  var search = req.query.search;

  if (search)
    search = search.split(' ');
  else
    search = [];

  search.push('pubblicato')

  query = {
    keywords: { $all: search }
  };

  Prodotto.find(query)
    .populate('modello', 'nome')
    .populate('categoria', 'nome')
    .limit(40)
    .sort({ mdate: -1 })
    .exec(function (err, listaProdotti) {
      if (err)
        res.status(500).send(err);
      else {
        /*
         SALVO IL LOG DELLA RICERCA IN DB
         //cerco se esiste un altro record con quella parola
         //dopoduchè salvo il record in DB aumentando il contatore del numero delle
         */

        var data = new Date();
        search = search.join(',');

        DwhRicerca.findOne(
          {
            ricerca: search,
            anno: data.getFullYear() // il totale globale viene conteggiato su base annua
          }
        )
          .limit(1)
          .sort({ mdate: -1 })
          .exec(
            function (err, ultimoRecord) {
              if (err)
                res.status(500).send(err);
              else {
                var totale = 1;

                if (ultimoRecord)
                  totale = parseInt(ultimoRecord.totaleAnno) + 1;

                var dwhRicerca = {
                  ricerca: search
                }
                dwhRicerca = new DwhRicerca(req.body);
                dwhRicerca.mdate = data;
                dwhRicerca.anno = data.getFullYear();
                dwhRicerca.mese = data.getMonth() + 1;
                dwhRicerca.giorno = data.getDate();
                dwhRicerca.ricerca = search
                dwhRicerca.totaleAnno = totale;
                dwhRicerca.totaleRisultati = listaProdotti.length;

                dwhRicerca.save(function (err, result) {
                  if (err)
                    res.status(500).send(err);
                  else
                    res.status(200).send(listaProdotti);
                });
              }
            }
          )
      }
    })
}

exports.searchv2 = function (req, res) {
  var search = req.body.search;

  if (search)
    search = search.split(' ');
  else
    search = [];

  search.push('pubblicato')

  var query = { $and: [{ keywords: { $all: search } }, { pubblicato: true }] };
  /*if (req.query.linea)
      query.$and.push({ linea: req.query.linea })
  else
      query.$and.push({ linea: { $ne: 'estro' } })*/
  if (req.query.serie)
    query.$and.push({ serie: req.serieEstro })
  else
    query.$and.push({ serie: req.serieIlmas })

  if (req.body.advanced) {
    _.each(_.keys(req.body.advanced), function (el) {
      var obj = {}
      obj[el] = req.body.advanced[el]
      query.$and.push(obj)
    })
  }

  var conto = 0;

  //la count e la query aggregata le parallelizzo perchè tanto il count finirà sicuramente prima di inviare i risultati al client
  Prodotto.countDocuments(query,
    function (err, count) {
      if (err) {
        console.log("error:", err);
        res.status(500).send(err);
      }
      else {
        conto = count;
      }
    });

  Prodotto.aggregate(
    [
      { $match: query },
      { "$group": { "_id": { fascio: "$fascio", colore: "$colore", alimentatore: "$alimentatore", moduloled: "$moduloled", irc: "$irc", lm: "$lm", k: "$k", w: "$w", ma: "$ma", les: "$les" } } }
    ])//.find(query,{fascio:1,colore:1,alimentatore:1,moduloled:1,irc:1,lm:1,k:1,w:1,ma:1,les:1})
    .exec(function (err, lista) {
      if (err)
        res.status(500).send(err);
      else {
        var advanced = {};
        advanced.fascio = _.uniq(_.map(lista, (el) => { return el._id.fascio; }));
        advanced.colore = _.uniq(_.map(lista, (el) => { return el._id.colore; }));
        advanced.alimentatore = _.uniq(_.map(lista, (el) => {
          if (!el._id.alimentatore) {
            return "Non definito";
          } else if (el._id.alimentatore == "") {
            return "Nessun alimentatore";
          }
          else {

            return el._id.alimentatore;
          }
        }));

        advanced.alimentatoreTmp = [];
        advanced.alimentatore.forEach(function (e) {
          if (e) {
            // NOTA BENE : Amedeo non ricorda perchè sono stati scritti i replace qui sotto, e io non ne capisco lo scopo. Commento il primo perchè altrimenti non avremo mai nelle tendine il Driver dimmerabile
            // se la cosa crea problemi in futuro, analizzare meglio la questione. Dani
            var testoLabel = e;
            testoLabel = testoLabel.replace("Driver dimmerabile Push Memory", " push memory")
            testoLabel = testoLabel.replace("Driver dimmerabbile", "")
            testoLabel = testoLabel.replace("Driver", "")
            advanced.alimentatoreTmp.push({ label: testoLabel, value: e });
          }
        })
        advanced.alimentatore = advanced.alimentatoreTmp;

        advanced.moduloled = _.uniq(_.map(lista, (el) => {

          if (!el._id || !el._id.moduloled || el._id.moduloled == 'undefined' || el._id.moduloled == '' || el._id.moduloled == null) {
            //  return "Non definito";
          }
          else {
            return el._id.moduloled;
          }
        }));

        advanced.irc = _.uniq(_.map(lista, (el) => {
          if (!el._id.irc) {
            // return "-Non definito";
          }
          else if (el._id.irc == "") {
            // return "Non definito";
          }
          else
            return el._id.irc;
        }));
        advanced.lm = _.uniq(_.map(lista, (el) => { return el._id.lm; }));
        advanced.k = _.uniq(_.map(lista, (el) => {
          if (el._id.k == "") {
            // return "-Non definito";
          } else return el._id.k;
        }));
        advanced.w = _.uniq(_.map(lista, (el) => { return el._id.w; }));
        advanced.ma = _.uniq(_.map(lista, (el) => { return el._id.ma; }));
        advanced.les = _.uniq(_.map(lista, (el) => {
          if (!el._id.les) {
            //return "-Non definito";
          }
          else return el._id.les;
        }));

        advanced.fascio.sort();
        advanced.fascio.forEach(function (e, index) {
          advanced.fascio[index] = { "label": e, "value": e }
        })
        advanced.fascio.unshift({ label: "-scegli-", value: 'NN' })

        advanced.colore.sort();
        advanced.colore.forEach(function (e, index) {
          advanced.colore[index] = { "label": e, "value": e }
        })
        advanced.colore.unshift({ label: "-scegli-", value: 'NN' })

        advanced.alimentatore.sort();
        advanced.alimentatore.unshift({ label: "-scegli-", value: 'NN' })

        advanced.moduloled.sort();
        advanced.moduloled.forEach(function (e, index) {
          if (!e)
            advanced.moduloled.splice(index, 1);
          else
            advanced.moduloled[index] = { "label": e, "value": e }
        })
        advanced.moduloled.unshift({ label: "-scegli-", value: 'NN' })

        advanced.irc.sort();
        advanced.irc.forEach(function (e, index) {
          advanced.irc[index] = { "label": e, "value": e }
        })
        advanced.irc.unshift({ label: "-scegli-", value: 'NN' })

        advanced.lm.sort();
        advanced.lm.forEach(function (e, index) {
          advanced.lm[index] = { "label": e, "value": e }
        })
        advanced.lm.unshift({ label: "-scegli-", value: 'NN' })

        advanced.k.sort();
        advanced.k.forEach(function (e, index) {
          advanced.k[index] = { "label": e, "value": e }
        })
        advanced.k.unshift({ label: "-scegli-", value: 'NN' })

        advanced.w.sort();
        advanced.w.forEach(function (e, index) {
          advanced.w[index] = { "label": e, "value": e }
        })
        advanced.w.unshift({ label: "-scegli-", value: 'NN' })

        advanced.ma.sort();
        advanced.ma.forEach(function (e, index) {
          advanced.ma[index] = { "label": e, "value": e }
        })
        advanced.ma.unshift({ label: "-scegli-", value: 'NN' })

        advanced.les.sort();
        advanced.les.forEach(function (e, index) {
          if (!e)
            advanced.les.splice(index, 1);
          else
            advanced.les[index] = { "label": e, "value": e }
        })
        advanced.les.unshift({ label: "-scegli-", value: 'NN' })

        var iRecPerPage = 10
        var currentPage = req.body.currentPage
        if (!currentPage)
          currentPage = 0

        var pageCount = Math.ceil(conto / iRecPerPage);

        //alimenta i risultati della ricerca prodotti da codice (non configuratore)
        Prodotto.find(query)
          //.sort({irc: 1, sottocategoria: 1, lm: 1, w: 1})
          .sort({ lm: 1, w: 1 })
          .populate('modello', { 'nome': 1, 'file_pdf': 1, 'file_sostituibilita': 1, 'sostituibilita_LED': 1, 'sostituibilita_driver': 1 })
          .populate('categoria', 'nome')
          //.limit(40)
          .skip(parseInt(currentPage) * iRecPerPage)
          .limit(iRecPerPage)
          //.sort({mdate: -1})
          .exec(function (err, listaProdotti) {
            if (err)
              res.status(500).send(err);
            else {
              /*
               SALVO IL LOG DELLA RICERCA IN DB solo se siamo alla pagina 0
               //cerco se esiste un altro record con quella parola
               //dopoduchè salvo il record in DB aumentando il contatore del numero delle
               */
              var data = new Date();
              search = search.join(',')
              if (currentPage == 0) {
                DwhRicerca.findOne(
                  {
                    ricerca: search,
                    anno: data.getFullYear() // il totale globale viene conteggiato su base annua
                  }
                )
                  .limit(1)
                  .sort({ mdate: -1 })
                  .exec(
                    function (err, ultimoRecord) {
                      if (err)
                        res.status(500).send(err);
                      else {
                        var totale = 1;

                        if (ultimoRecord)
                          totale = parseInt(ultimoRecord.totaleAnno) + 1;


                        var dwhRicerca = {
                          ricerca: search
                        }
                        dwhRicerca = new DwhRicerca(req.body);
                        dwhRicerca.mdate = data;
                        dwhRicerca.anno = data.getFullYear();
                        dwhRicerca.mese = data.getMonth() + 1;
                        dwhRicerca.giorno = data.getDate();
                        dwhRicerca.ricerca = search
                        dwhRicerca.totaleAnno = totale;
                        dwhRicerca.totaleRisultati = listaProdotti.length;

                        dwhRicerca.save(function (err, result) {
                          if (err) {
                            console.log("err in  api del be", err)
                            res.status(500).send(err);
                          }
                          else
                            res.status(200).send({
                              advanced: advanced,
                              prodotti: listaProdotti,
                              tot: conto,
                              currentPage: currentPage,
                              pageCount: pageCount
                            });
                        });
                      }
                    }
                  )

              } else {
                res.status(200).send({
                  advanced: advanced,
                  prodotti: listaProdotti,
                  tot: conto,
                  currentPage: currentPage,
                  pageCount: pageCount
                });
              }
            }
          });

      }
    })
}

// alimenta le schede tecniche se richieste dal sito con il configuratore prodotti standard
exports.prodottoSchedaTecnica = function (req, res) {
  var pdfTempPath = process.cwd() + '/.tmp/',
    headerRecapiti = fs.readFileSync(process.cwd() + '/server/templates/pdf/intestazione_recapiti.html', 'utf8'), // in offerta ordine cc...
    headerSchedaTecnica = fs.readFileSync(process.cwd() + '/server/templates/pdf/intestazione_scheda_tecnica.html', 'utf8'), // scheda tecnica...
    headerSchedaTecnicaEstro = fs.readFileSync(process.cwd() + '/server/templates/pdf/intestazione_scheda_tecnica_estro.html', 'utf8'), // scheda tecnica estro
    footerSchedaTecnica = fs.readFileSync(process.cwd() + '/server/templates/pdf/footer_scheda_tecnica.html', 'utf8'), // footer scheda tecnica cc...
    footer = fs.readFileSync(process.cwd() + '/server/templates/pdf/piepagina.html', 'utf8'), // comune a tutti
    templateSchedaTecnica = fs.readFileSync(process.cwd() + '/server/templates/pdf/scheda_tecnica.html', 'utf8'), // specifico offerta
    templateSchedaTecnicaBinario = fs.readFileSync(process.cwd() + '/server/templates/pdf/schedaTecnicaBinario.html', 'utf8'), // specifico offerta
    css = fs.readFileSync(process.cwd() + '/server/templates/pdf/pdf.css', 'utf8'), // comune a tutti
    idProdotto = req.params.idprodotto;

  Prodotto.findOne({ _id: idProdotto })
    .populate('modello', { 'nome': 1, 'peso': 1, 'volume': 1, 'ecolamp': 1, 'sostituibilita_LED': 1, 'sostituibilita_driver': 1, 'classe_energetica': 1 })
    .populate('modello_cript', { 'nome': 1 })
    .populate('categoria', 'nome')
    .populate('accessori')
    .exec(function (err, prodotto) {
      if (err) {
        res.status(500).send(err);
        return;
      }

      if (!prodotto) {
        res.status(404).send();
        return;
      }

      if (req.params.lingua && req.params.lingua == 'en')
        prodotto.en = 'en';

      if (prodotto.accessori && prodotto.accessori.length && Array.isArray(prodotto.accessori))
        try {
          prodotto.accessori = prodotto.accessori.filter(a => a && a._id);
        } catch (e) {
          console.log('errore accessori', prodotto, e);
        }

      prodotto = normalizzaDatiProdotto(prodotto);

      var tipoScheda = templateSchedaTecnica;
      var id_binari = [
        { _id: "5711f38e5f09a60300e387b0" },
        { _id: "5711f4135f09a60300e387b2" },
        { _id: "5711f4485f09a60300e387b5" },
        { _id: "5711f3f85f09a60300e387b1" },
        { _id: "59cbae665a6aad0400841465" },
        { _id: "59db30769b066b0400cd5fb4" },
        { _id: "59dc8b3439173e040027a22f" },
        { _id: "59db543239173e040027a161" },
        { _id: "59db52f839173e040027a159" },
        { _id: "59db52d139173e040027a158" },
        { _id: "59db4c2b39173e040027a148" },
        { _id: "59db4b4c39173e040027a141" },
        { _id: "59db3c8c9b066b0400cd6005" },
        { _id: "59ccc39650af6204004504a1" },
        { _id: "59ccb0b450af62040045046b" },
        { _id: "59ccaf8a50af62040045045e" }
      ];

      try {
        id_binari.forEach
          (
            function (idBinario) {
              if (prodotto.modello._id == idBinario._id) {
                tipoScheda = templateSchedaTecnicaBinario;
                return;
              }
            }
          )
      } catch (e) {
        console.log('errore for', e);
      }

      var date = new Date(),
        timestamp = date.valueOf(),
        header = headerSchedaTecnica,
        footer = footerSchedaTecnica,
        template = tipoScheda,
        data = _.extend(prodotto, { css: css, header: header, footer: footer, formatters }),
        html = Mustache.render(template, data),
        localPath = pdfTempPath + timestamp + '.pdf', // sul server lo salvo col timestamp in /temp
        s3Path = 'temp/' + timestamp + '-' + prodotto.codice + '.pdf'; // cartella temporanea e timestamp

      var pdfOptions = {
        format: 'A4',
        orientation: 'portrait',
        border: '1cm',
        header: {
          height: "3cm"
        },
        footer: {
          height: "1.5cm"
        },
        timeout: 200000
      };

      pdf.create(html, pdfOptions).toFile(localPath, function (err, response) {
        if (err) {
          res.status(500).send(err);
          return;
        }

        // lo mando su s3 in una cartella sche vuoterò periodicamente
        var fileParams = {
          localFile: localPath,
          s3Params: {
            Key: s3Path,
            Bucket: CONFIG.AWS_S3_BUCKET,
            ACL: 'public-read'
          }
        }, uploader;

        try {
          uploader = s3Client.uploadFile(fileParams);
        } catch (e) {
          console.log('errore uploader', e)
        }

        uploader.on('error', function (err) {
          res.status(500).send(err.stack)
        });

        uploader.on('end', function (r) {
          /*
          SALVO IL LOG DELLA RICERCA IN DB
              //cerco se esiste un altro record con quel codice
              //dopoduchè salvo il record in DB aumentando il contatore del numero delle
          */
          var provenienza = req.params.provenienza;
          if (!provenienza)
            provenienza = "sito";

          var data = new Date();

          DwhSchedaTecnica.findOne({
            codice: prodotto.codice,
            anno: data.getFullYear() // il totale globale viene conteggiato su base annua
          })
            .limit(1)
            .sort({ lm: 1, mdate: -1 })
            .exec(function (err, ultimoRecord) {
              if (err) {
                res.status(500).send(err);
                console.log("Errore500PaginaURL1", err);
                return;
              }

              var totale = 1;

              if (ultimoRecord)
                totale = parseInt(ultimoRecord.totaleAnno) + 1;

              var scheda = new DwhSchedaTecnica(req.body);
              scheda.mdate = data;
              scheda.codice = prodotto.codice;
              scheda.prodotto = prodotto;
              scheda.anno = data.getFullYear();
              scheda.mese = data.getMonth() + 1;
              scheda.giorno = data.getDate();
              scheda.provenienza = provenienza;
              scheda.totaleAnno = totale;
              scheda.save(function (err, result) {
                if (err) {
                  res.status(500).send(err);
                  console.log("Errore500PaginaURL", err);
                } else
                  res.status(200).json({ url: CONFIG.AWS_S3_BUCKET_PATH + '/' + s3Path }) // mando al client l'url s3 dove l'ho messo
              });
            })
        });
      });
    });
}

exports.prodottoSingolo = function (req, res) {
  var _idProdotto = req.params.idprodotto;

  //restituisce il dettaglio del prodotto
  Prodotto.findOne({ _id: _idProdotto })
    .populate('modello', { 'nome': 1, 'file_pdf': 1 })
    .populate('categoria', { 'nome': 1, 'tipo': 1 })
    .exec(function (err, prodotto) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(prodotto);
    })
}

exports.dettaglioProdotto = function (req, res) {
  var _idProdotto = req.params.idprodotto;

  //restituisce il dettaglio del prodotto
  Prodotto.findOne({ _id: _idProdotto })
    .populate('modello', { 'nome': 1, 'file_pdf': 1 })
    .populate('categoria', 'nome')
    .exec(function (err, prodotto) {
      if (err) {
        res.status(500).send(err);
        return;
      }

      var data = new Date();

      DwhSchedaDettaglio.findOne({
        codice: prodotto.codice,
        anno: data.getFullYear() // il totale globale viene conteggiato su base annua
      })
        .limit(1)
        .exec(function (err, ultimoRecord) {
          if (err) {
            res.status(500).send(err);
            return;
          }

          var totale = 1;

          if (ultimoRecord)
            totale = parseInt(ultimoRecord.totaleAnno) + 1;

          var scheda = new DwhSchedaDettaglio(req.body);
          scheda.mdate = data;
          scheda.codice = prodotto.codice;
          scheda.prodotto = prodotto;
          scheda.anno = data.getFullYear();
          scheda.mese = data.getMonth() + 1;
          scheda.giorno = data.getDate();
          scheda.totaleAnno = totale;
          scheda.save(function (err, result) {
            if (err)
              res.status(500).send(err);
            else
              res.status(200).send(prodotto);
          });
        })
    })
}

/*
il metodo viene chiamato giornalmente da KITE da un cron e restituisce i dati
della distinta base china 1 del giorno passato in input.
I dati vanno passati nell'header del browser
@input :
    login, vedi CONFIG
    pwd,  vedi CONFIG
    date(format YYYY-MM-DD)
@output :
    json esempio:

    {
        "distintaBase_china1":
        [
            {
              "codice_prodotto": "0798HFE1",
              "componenti":
              [
                {
                  "_id":
                  {
                    "codice": "0798XXE1",
                    "tipo": "corpo"
                  },
                  "qt": 1
                },
                {
                  "_id":
                  {
                    "codice": "9000B02",
                    "tipo": "driver"
                  },
                  "qt": 1
                }
              ]
            }
        ]
    }
*/
//NN più usata
exports.distintaBaseChina1 = function (req, res) {
  var loginApiCall = req.headers.login,
    pwdApiCall = req.headers.pwd,
    dataApiCall = req.headers.date,
    path = req.url;

  if (loginApiCall != CONFIG.API_LOGIN_KITE || pwdApiCall != CONFIG.API_PWD_KITE) {
    var erroreObj =
    {
      "errors": [
        {
          "userMessage": "Login o password errata",
          "code": 401
        }
      ]
    }

    var data =
    {
      idate: new Date(),
      login: loginApiCall,
      pwd: pwdApiCall,
      path: path,
      result: "401"
    }
    var apiLog = new ApiLog(data);
    apiLog.save(function (err, result) {
      if (err)
        res.status(500).send(err)
      else
        res.status(401).send(erroreObj);
    })

  }
  else if (!moment(dataApiCall, "YYYY-MM-DD", true).isValid()) {
    var erroreObj =
    {
      "errors": [
        {
          "userMessage": "Data distinta base errata",
          "code": 400
        }
      ]
    }
    var data =
    {
      idate: new Date(),
      login: loginApiCall,
      pwd: pwdApiCall,
      path: path,
      result: "400"
    }
    var apiLog = new ApiLog(data);
    apiLog.save(function (err, result) {
      if (err)
        res.status(500).send(err)
      else
        res.status(400).send(erroreObj);
    })
  }
  else {

    var query =
    {
      componenti: { $ne: null },
      distintabase: 'china_1',
      $or: [
        { generazione: 'G6' },
        { generazione: 'D1' },
        { generazione: 'V1' },
        { generazione: '3HE' },
        { generazione: 'D2' },
        { kit: true },
        { criptato: true }
      ],
      mdate:
      {
        $gte: new Date(Date.UTC(_splitDate(dataApiCall, 'anno'), _splitDate(dataApiCall, 'mese'), _splitDate(dataApiCall, 'giorno'), 0, 0, 0, 0)),
        $lte: new Date(Date.UTC(_splitDate(dataApiCall, 'anno'), _splitDate(dataApiCall, 'mese'), _splitDate(dataApiCall, 'giorno'), 23, 59, 59, 99))
      }
    };

    var fields =
    {
      codice: 1,
      componenti: 1
    };
    Prodotto.find(query, fields)
      .populate(
        {
          path: 'componenti._id',
          model: 'Componente',
          select: { _id: 0, codice: 1, tipo: 1 }
        }
      )
      .exec(function (err, prodotti) {
        if (err)
          res.status(500).send(err);
        else {
          var distintaBase_china1Object =
          {
            distintaBase_china1: []
          };
          async.each(prodotti,
            function (prodotto, cb) {
              var p = {};
              p.codice_prodotto = prodotto.codice;
              p.componenti = prodotto.componenti;
              distintaBase_china1Object.distintaBase_china1.push(p);//per ogni prodotto popolo il json del risultato finale
              cb();
            },
            function (err) {
              if (err)
                return next(err);
              var data =
              {
                idate: new Date(),
                login: loginApiCall,
                pwd: pwdApiCall,
                path: path,
                result: "200",
                logs: distintaBase_china1Object
              }
              var apiLog = new ApiLog(data);
              apiLog.save(function (err, result) {
                if (err)
                  res.status(500).send(err)
                else
                  res.status(200).json(distintaBase_china1Object)
              })
            })
        }
      })
  }
}

/*
 il metodo viene chiamato giornalmente da KITE da un cron e restituisce i dati della tab modelli
 I dati vanno passati nell'header del browser
 @input :
 login, vedi CONFIG
 pwd,  vedi CONFIG
 date(format YYYY-MM-DD)
 @output :

 {
 "modelli": [
 {
 "_id": "5887383286d21104003f2ac1",
 "nome": "1154 MINI BELL",
 "linea": "ilmas",
 "uso": "indoor",
 "categoria": {
 "_id": "56b8aaa80de23c95600bd220",
 "nome": "Sospensioni"
 },
 "ecolamp": "Fascia2",
 "volume": 20,
 "peso": 10,
 "mdate": "2017-05-18T09:54:52.629Z"
 },
 {
 "_id": "58b408ef076bbc0400698153",
 "nome": "2916 Cristal",
 "linea": "ilmas",
 "uso": "indoor",
 "categoria": {
 "_id": "56b8aaa80de23c95600bd21d",
 "nome": "Incassi orientabili"
 },
 "ecolamp": "Fascia2",
 "volume": 20,
 "peso": 0.5,
 "mdate": "2017-05-18T10:29:00.057Z"
 }
 ]
 }
 */
exports.tabellaModelli = function (req, res) {
  var loginApiCall = req.headers.login,
    pwdApiCall = req.headers.pwd,
    dataApiCall = req.headers.date,
    path = req.url;

  if (loginApiCall != CONFIG.API_LOGIN_KITE || pwdApiCall != CONFIG.API_PWD_KITE) {
    var erroreObj =
    {
      "errors": [
        {
          "userMessage": "Login o password errata",
          "code": 401
        }
      ]
    }

    var data =
    {
      idate: new Date(),
      login: loginApiCall,
      pwd: pwdApiCall,
      path: path,
      result: "401"
    }
    var apiLog = new ApiLog(data);
    apiLog.save(function (err, result) {
      if (err)
        res.status(500).send(err)
      else
        res.status(401).send(erroreObj);
    })

  }
  else if (!moment(dataApiCall, "YYYY-MM-DD", true).isValid()) {
    var erroreObj =
    {
      "errors": [
        {
          "userMessage": "Formato Data errato",
          "code": 400
        }
      ]
    }
    var data =
    {
      idate: new Date(),
      login: loginApiCall,
      pwd: pwdApiCall,
      path: path,
      result: "400"
    }
    var apiLog = new ApiLog(data);
    apiLog.save(function (err, result) {
      if (err)
        res.status(500).send(err)
      else
        res.status(400).send(erroreObj);
    })
  }
  else {
    var query =
    {
      mdate:
      {
        $gte: new Date(Date.UTC(_splitDate(dataApiCall, 'anno'), _splitDate(dataApiCall, 'mese'), _splitDate(dataApiCall, 'giorno'), 0, 0, 0, 0)),
        $lte: new Date(Date.UTC(_splitDate(dataApiCall, 'anno'), _splitDate(dataApiCall, 'mese'), _splitDate(dataApiCall, 'giorno'), 23, 59, 59, 99))
      }
    };
    var fields =
    {
      nome: 1,
      linea: 1,
      uso: 1,
      categoria: 1,
      peso: 1,
      volume: 1,
      ecolamp: 1,
      mdate: 1
    };
    Modello.find(query, fields)
      .populate('categoria', 'nome')
      .sort({ nome: 1 })
      .exec(function (err, modelliList) {
        if (err)
          res.status(500).send(err);
        else {
          var modelliObject = { modelli: modelliList };

          var data =
          {
            idate: new Date(),
            login: loginApiCall,
            pwd: pwdApiCall,
            path: path,
            result: "200",
            logs: modelliObject
          }
          var apiLog = new ApiLog(data);
          apiLog.save(function (err, result) {
            if (err)
              res.status(500).send(err)
            else
              res.status(200).json(modelliObject)
          })
        }
      })
  }
}

/*
 il metodo viene chiamato giornalmente da KITE da un cron e restituisce i dati della tab prodotti
 I dati vanno passati nell'header del browser
 @input :
 login, vedi CONFIG
 pwd,  vedi CONFIG
 date(format YYYY-MM-DD)
 @output :

  "prodotti": [
    {
      "_id": "591d4dcb46530c04002afba7",
      "codice": "KIT0672Z9",
      "linea": "ilmas",
      "destinazione": "indoor",
      "modello": {
        "_id": "570cdd4b1fb26a0300b0c1f8",
        "nome": "0672 Astuto Led AR111"
      },
      "k": "4000",
      "lm": "4390",
      "w": "29",
      "fascio": "50",
      "v": "220-240",
      "hz": "50-60",
      "generazione": "G6",
      "macadam": "3",
      "colore": "Bianco",
      "verniciatura": "A polvere epossidica stabilizzata UV",
      "tmax": "30°",
      "alimentatore_incluso": true,
      "ce": true,
      "f": true,
      "inclinabile": "60",
      "rischio_fotobiologico": "RG1",
      "filo_incandescente": "850",
      "classe_isolamento": "2",
      "ip": "20",
      "foro": "152",
      "ma": "800",
      "divieto_controsoffitti": true,
      "alimentatore": "standard",
      "les": "LES19",
      "ottica_it": "Riflettore in alluminio metallizzato puro 99.98 con vetro di protezione",
      "corpo_it": "Corpo pressofusione in alluminio verniciato bianco con molle in acciaio",
      "prezzo": 139,
      "mdate": "2017-05-18T07:36:24.269Z"
    },
    {
      "_id": "591da30f46530c04002afc32",
      "codice": "1242DFJ1",
      "linea": "ilmas",
      "destinazione": "indoor",
      "tradizionale": false,
      "sottocategoria": "Advanced",
      "lampadina": "",
      "attacco": "",
      "k": "3000",
      "lm": "5300",
      "w": "37",
      "ma": "1050",
      "v": "220-240",
      "hz": "50-60",
      "fascio": "33",
      "irc": ">80",
      "satinato": false,
      "generazione": "G4",
      "macadam": "3",
      "colore": "Bianco",
      "verniciatura": "A polvere epossidica stabilizzata UV",
      "les": "LES 19",
      "tmax": "30",
      "alimentatore": "",
      "alimentatore_incluso": true,
      "descrizione_applicazione": "Proiettore a binario orientabile",
      "orientabile": "355",
      "inclinabile": "",
      "corpo_it": "Corpo in pressofusione di alluminio",
      "corpo_en": "Die-cast aluminium body",
      "ottica_it": "Riflettore in alluminio metallizzato puro 99,98 con vetro di protezione",
      "ottica_en": "Pure 99,98 aluminium reflector metallized with safety glass",
      "adattatore_it": "Adattatore universale 3 fasi",
      "adattatore_en": "3-circuit universal adapter",
      "ce": true,
      "f": true,
      "rischio_fotobiologico": "RG1",
      "classe_isolamento": "1",
      "ip": "20",
      "foro": "",
      "divieto_controsoffitti": false,
      "filo_incandescente": "850",
      "modello": {
        "_id": "56b8aaa90de23c95600bd22d",
        "nome": "1242 Over Led Ps Maxi"
      },
      "prezzo": 190,
      "exportMetel" : false,
      "mdate": "2017-05-18T13:35:11.550Z"
    }
  ]
}
}*/

exports.tabellaProdotti = function (req, res) {
  var loginApiCall = req.headers.login,
    pwdApiCall = req.headers.pwd,
    dataApiCall = req.headers.date,
    path = req.url;

  if (loginApiCall != CONFIG.API_LOGIN_KITE || pwdApiCall != CONFIG.API_PWD_KITE) {
    var erroreObj =
    {
      "errors": [
        {
          "userMessage": "Login o password errata",
          "code": 401
        }
      ]
    }

    var data =
    {
      idate: new Date(),
      login: loginApiCall,
      pwd: pwdApiCall,
      path: path,
      result: "401"
    }
    var apiLog = new ApiLog(data);
    apiLog.save(function (err, result) {
      if (err)
        res.status(500).send(err)
      else
        res.status(401).send(erroreObj);
    })
  }
  else if (!moment(dataApiCall, "YYYY-MM-DD", true).isValid()) {
    var erroreObj =
    {
      "errors": [
        {
          "userMessage": "Formato data errato",
          "code": 400
        }
      ]
    }
    var data =
    {
      idate: new Date(),
      login: loginApiCall,
      pwd: pwdApiCall,
      path: path,
      result: "400"
    }
    var apiLog = new ApiLog(data);
    apiLog.save(function (err, result) {
      if (err)
        res.status(500).send(err)
      else
        res.status(400).send(erroreObj);
    })
  }
  else {
    var query =
    {
      mdate:
      {
        $gte: new Date(Date.UTC(_splitDate(dataApiCall, 'anno'), _splitDate(dataApiCall, 'mese'), _splitDate(dataApiCall, 'giorno'), 0, 0, 0, 0)),
        $lte: new Date(Date.UTC(_splitDate(dataApiCall, 'anno'), _splitDate(dataApiCall, 'mese'), _splitDate(dataApiCall, 'giorno'), 23, 59, 59, 99))
      }
    };
    var fields =
    {
      codice: 1,
      prezzo: 1,
      qtamin: 1,
      linea: 1,
      destinazione: 1,
      tradizionale: 1,
      sottocategoria: 1,
      modello: 1,
      lampadina: 1,
      k: 1,
      lm: 1,
      w: 1,
      ma: 1,
      v: 1,
      hz: 1,
      fascio: 1,
      irc: 1,
      dimmerabile: 1,
      durata: 1,
      attacco: 1,
      classe_energetica: 1,
      note: 1,
      satinato: 1,
      generazione: 1,
      macadam: 1,
      colore: 1,
      verniciatura: 1,
      moduloled: 1,
      les: 1,
      ugr: 1,
      tmax: 1,
      alimentatore: 1,
      alimentatore_incluso: 1,
      descrizione_applicazione: 1,
      orientabile: 1,
      inclinabile: 1,
      corpo_it: 1,
      corpo_en: 1,
      ottica_it: 1,
      ottica_en: 1,
      adattatore_it: 1,
      adattatore_en: 1,
      ce: 1,
      f: 1,
      rischio_fotobiologico: 1,
      classe_isolamento: 1,
      ip: 1,
      foro: 1,
      lunghezza: 1,
      vita_utile: 1,
      vita_utile_desc: 1,
      valore_di_perdita: 1,
      divieto_controsoffitti: 1,
      filo_incandescente: 1,
      ik: 1,
      flusso_lum_app: 1,
      potenza_sistema: 1,
      exportMetel: 1,
      mdate: 1
    };
    Prodotto.find(query, fields)
      .populate('modello', 'nome')
      .sort({ mdate: 1 })
      .exec(function (err, prodottiList) {
        if (err)
          res.status(500).send(err);
        else {
          var prodottiObject = { prodotti: prodottiList };

          var data =
          {
            idate: new Date(),
            login: loginApiCall,
            pwd: pwdApiCall,
            path: path,
            result: "200",
            logs: prodottiObject
          }
          var apiLog = new ApiLog(data);
          apiLog.save(function (err, result) {
            if (err)
              res.status(500).send(err)
            else
              res.status(200).json(prodottiObject)
          })
        }
      })
  }
}

/*
il metodo viene chiamato giornalmente da KITE da un cron e restituisce i dati della tab categoria
 I dati vanno passati nell'header del browser
@input :
    login, vedi CONFIG
    pwd,  vedi CONFIG
@output :
{
  "categorie": [
    {
      "_id": "570b5473eff59523af4fcd2b",
      "uso": "indoor",
      "linea": "ilmas",
      "nome": "Accessori",
      "mdate": "2017-03-29T08:32:35.873Z"
    },
    {
      "_id": "570b550deff59523af4fcd2c",
      "uso": "indoor",
      "linea": "ilmas",
      "nome": "Binari",
      "mdate": "2016-09-02T15:11:12.925Z"
    }
  ]
}
*/
exports.tabellaCategorie = function (req, res) {
  var loginApiCall = req.headers.login,
    pwdApiCall = req.headers.pwd,
    dataApiCall = req.headers.date,
    path = req.url;

  if (loginApiCall != CONFIG.API_LOGIN_KITE || pwdApiCall != CONFIG.API_PWD_KITE) {
    var erroreObj =
    {
      "errors": [
        {
          "userMessage": "Login o password errata",
          "code": 401
        }
      ]
    }

    var data =
    {
      idate: new Date(),
      login: loginApiCall,
      pwd: pwdApiCall,
      path: path,
      result: "401"
    }
    var apiLog = new ApiLog(data);
    apiLog.save(function (err, result) {
      if (err)
        res.status(500).send(err)
      else
        res.status(401).send(erroreObj);
    })

  }
  else if (!moment(dataApiCall, "YYYY-MM-DD", true).isValid()) {
    var erroreObj =
    {
      "errors": [
        {
          "userMessage": "Formato data errato",
          "code": 400
        }
      ]
    }
    var data =
    {
      idate: new Date(),
      login: loginApiCall,
      pwd: pwdApiCall,
      path: path,
      result: "400"
    }
    var apiLog = new ApiLog(data);
    apiLog.save(function (err, result) {
      if (err)
        res.status(500).send(err)
      else
        res.status(400).send(erroreObj);
    })
  }
  else {
    var query =
    {
      mdate:
      {
        $gte: new Date(Date.UTC(_splitDate(dataApiCall, 'anno'), _splitDate(dataApiCall, 'mese'), _splitDate(dataApiCall, 'giorno'), 0, 0, 0, 0)),
        $lte: new Date(Date.UTC(_splitDate(dataApiCall, 'anno'), _splitDate(dataApiCall, 'mese'), _splitDate(dataApiCall, 'giorno'), 23, 59, 59, 99))
      }
    };
    var fields =
    {
      nome: 1,
      linea: 1,
      uso: 1,
      mdate: 1
    };
    Categoria.find(query, fields)
      .sort({ nome: 1 })
      .exec(function (err, categoriaList) {
        if (err)
          res.status(500).send(err);
        else {
          var categoriaObject = { categorie: categoriaList }

          var data =
          {
            idate: new Date(),
            login: loginApiCall,
            pwd: pwdApiCall,
            path: path,
            result: "200",
            logs: categoriaObject
          }
          var apiLog = new ApiLog(data);
          apiLog.save(function (err, result) {
            if (err)
              res.status(500).send(err)
            else
              res.status(200).json(categoriaObject)
          })
        }
      })
  }
}

exports.agentiList = function (req, res) {
  Agente.find({ pubblicato: true },
    { ragioneSociale: 1, via: 1, cap: 1, comune: 1, prov: 1, nazione: 1, tel: 1, email: 1, area: 1, coordinates: 1, contatti: 1 },
    function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results)
    }).sort({ ragioneSociale: 1 })
}

exports.eventiList = function (req, res) {
  Evento.find({ pubblicato: true },
    {
      titolo_it: 1,
      titolo_en: 1,
      dataDa: 1,
      dataA: 1,
      posizione: 1,
      sitoWeb: 1,
      metaDescription_it: 1,
      metaDescription_en: 1,
      testo_it: 1,
      testo_en: 1,
      imgPrincipale: 1,
      imgLista: 1
    },
    function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results)
    }).sort({ posizione: 1, dataDa: 1 })
}

exports.getAgente = function (req, res) {
  Agente.findOne({ _id: req.params.idAgente, pubblicato: true },
    { ragioneSociale: 1, via: 1, cap: 1, comune: 1, prov: 1, nazione: 1, tel: 1, email: 1, area: 1, coordinates: 1, contatti: 1 },
    function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results)
    }).sort({ ragioneSociale: 1 })
}

exports.getEvento = function (req, res) {
  Evento.findOne({ _id: req.params.idEvento, pubblicato: true },
    {
      titolo_it: 1,
      titolo_en: 1,
      dataDa: 1,
      dataA: 1,
      posizione: 1,
      sitoWeb: 1,
      metaDescription_it: 1,
      metaDescription_en: 1,
      testo_it: 1,
      testo_en: 1,
      imgPrincipale: 1,
      imgLista: 1
    },
    function (err, results) {
      if (err)
        res.status(500).send(err);

      res.status(200).send(results)
    })
}

exports.installazioniList = function (req, res) {
  var limit = 0;
  if (req.params.limit) limit = req.params.limit
  if (isNaN(limit)) limit = 0;

  var query = { $and: [{ pubblicato: true }] }

  //qui è normale che ci sia linea invece di serie
  if (req.query.linea)
    query.$and.push({ linea: req.query.linea })
  // else
  // query.$and.push({ linea: { $ne: 'estro' } })


  Installazione.find(query,
    {
      descrizione_it: 1,
      descrizione_en: 1,
      anno: 1,
      prodotti_it: 1,
      prodotti_en: 1,
      foto: 1,
      progetti_simili: 1,
      dataPubblicazione: 1
    })
    .populate('progetti_simili', {
      descrizione_it: 1,
      descrizione_en: 1,
      anno: 1,
      prodotti_it: 1,
      prodotti_en: 1,
      foto: 1,
      progetti_simili: 1
    })
    .sort({ dataPubblicazione: -1 })
    .limit(parseInt(limit))
    .exec(function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results)
    })
}

exports.getInstallazione = function (req, res) {
  Installazione.findOne({ _id: req.params.idInstallazione, pubblicato: true },
    {
      descrizione_it: 1,
      descrizione_en: 1,
      anno: 1,
      prodotti_it: 1,
      prodotti_en: 1,
      foto: 1,
      progetti_simili: 1
    })
    .populate('progetti_simili', {
      descrizione_it: 1,
      descrizione_en: 1,
      anno: 1,
      prodotti_it: 1,
      prodotti_en: 1,
      foto: 1,
      progetti_simili: 1
    })
    .exec(function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results)
    })
}

exports.newsList = function (req, res) {
  News.find({ pubblicato: true },
    {
      titolo_it: 1,
      titolo_en: 1,
      dataPubblicazione: 1,
      abstract_it: 1,
      abstract_en: 1,
      descrizione_it: 1,
      descrizione_en: 1,
      url: 1,
      file_pdf: 1
    })
    .sort({ dataPubblicazione: -1 })
    .exec(function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results)
    })
}

exports.getNews = function (req, res) {
  News.findOne({ _id: req.params.idNews, pubblicato: true },
    {
      titolo_it: 1,
      titolo_en: 1,
      dataPubblicazione: 1,
      abstract_it: 1,
      abstract_en: 1,
      descrizione_it: 1,
      descrizione_en: 1,
      url: 1,
      file_pdf: 1
    })
    .exec(function (err, results) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(results)
    })
}

exports.getProdottiAutocomplete = function (req, res) {
  let query = { codice: { $regex: req.query.query, $options: 'i' } }
  if (req.query.serie)
    query.serie = req.query.serie
  else
    query.serie = req.serieIlmas

  if (req.query.categoria)
    query.categoria = req.query.categoria

  Prodotto.find(query, { _id: 1, codice: 1 })
    .sort({ codice: 1 })
    .exec(function (err, prodottiList) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(prodottiList)
    })
}

exports.getFinitureAutocomplete = function (req, res) {
  Colori.find(
    {
      $or:
        [
          { codice: { $regex: req.query.query, $options: 'i' } },
          { descrizione_cover: { $regex: req.query.query, $options: 'i' } },
          { descrizione_estruso: { $regex: req.query.query, $options: 'i' } }
        ]
    },
    function (err, result) {
      if (err)
        res.status(500).send(err);
      else
        res.status(200).send(result);
    });
}

exports.prodottoSchedaTecnicaEstro = function (req, res) {
  var codiceEstroFinito = function () {
    String.prototype.lpad = function (padString, length) {
      var str = this;
      while (str.length < length)
        str = padString + str;
      return str;
    }

    let ret = ''
    if (prodotto.st_codice_binario && prodotto.st_codice_fonteluminosa &&
      (prodotto.st_lunghezza || prodotto.st_numero_moduli) &&
      prodotto.st_finitura && prodotto.st_finitura.length === 2) {
      ret += prodotto.st_codice_binario + prodotto.st_codice_fonteluminosa
      ret += prodotto.st_lunghezza ? prodotto.st_lunghezza.toString().lpad('0', 5) : prodotto.st_numero_moduli.toString().lpad('0', 5)
      ret += prodotto.st_finitura
    }
    return ret
  }

  req.body.tipopdf = 'schedatecnicaEstro';
  req.body.codice_finito = codiceEstroFinito()
  Pdf.create(req, res);
}
