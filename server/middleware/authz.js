
module.exports = function (user) {
    var roles = user.app_metadata ? user.app_metadata.roles : false;
    return {
        adminProdotti: function (){ 
            return roles && roles.indexOf('prodotti')!==-1 ? true : false;
        },
        commerciale: function (){
            return roles && roles.indexOf('commerciale')!==-1 ? true : false;
        },
        admin: function (){ 
            return roles && roles.indexOf('admin')!==-1 ? true : false;
        },
    }
};