var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	ApiLogSchema = new Schema({
		idate: {
			type: Date,
			default: Date.now
		},
		login: { 
			type: String 
		},
		pwd: { 
			type: String 
		},
		path: {
			type: String 
		},
		result: {
			type: String 
		},
		logs: {
            type: Schema.Types.Mixed
        }

	});
ApiLogSchema.index({idate: -1});
mongoose.model('ApiLog', ApiLogSchema, 'ApiLog');