var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	SottocategoriaSchema = new Schema({
		mdate: {
			type: Date,
			default: Date.now
		},
		nome: {
			type: String,
			required: true,
            unique: true
		}
	});

SottocategoriaSchema.index({nome:1});
mongoose.model('Sottocategoria', SottocategoriaSchema, 'Sottocategoria');

