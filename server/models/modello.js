var mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ModelloSchema = new Schema({
    mdate: {
      type: Date,
      default: Date.now
    },
    nome: {
      type: String, // es over led ps
      required: true
    },
    nome_en: {
      type: String
    },
    descrizione: {
      type: String
    },
    descrizione_en: {
      type: String
    },
    linea: { // ilmas/toshiba ecc...
      type: String
    },
    foto: { // di cloudinary
      type: String
    },
    disegno: { // di cloudinary
      type: String
    },
    uso: {
      type: String
    },
    categoria: {
      type: Schema.ObjectId, // ref a categoria
      ref: 'Categoria'
    },
    pubblicato: { // si/no
      type: Boolean
    },
    accessori: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Prodotto'
      }
    ],
    ordinamento_sito: {
      type: String
    },
    classe_energetica: {
      type: String
    },
    peso: {
      type: Number
    },
    volume: {
      type: Number
    },
    ecolamp: {
      type: String
    },
    last_user: { // utente che ha effettuato l'ultima modifica
      type: Schema.Types.Mixed
    },
    importato: {
      type: String
    },
    sostituibilita_LED: {
      type: Boolean,
      default: true
    },
    sostituibilita_driver: {
      type: Boolean,
      default: true
    },
    made_in: {
      type: String,
      default: null
    },
    import: {
      type: Boolean
    },
    montaggio: {
      type: Number
    },
    sacchetto: {
      type: String
    },
    pluriball: {
      type: Boolean
    },
    cartone: {
      type: String
    },
    file_pdf: {
      nome: { type: String },
      url: { type: String } // url completo su s3
    },
    food: {
      type: Boolean
    },
    china1: {
      type: Boolean
    },
    file_distinta: {
      nome: { type: String },
      url: { type: String } // url completo su s3
    },
    file_sostituibilita: {
      nome: { type: String },
      url: { type: String } // url completo su s3
    },
    serie: { //rappresenta una "generazione" di prodotti. Nel 2020 stiamo passando alla generazione aComponenti, quelli prima sono aProdotti
      type: String,
      default: "aProdotti"
    },
    /* gruppo_merceologico: {
        type: String,
        default: null
    }, */
    modello_gm: {
      type: String,
      default: null
    },
    conto_contabile_acquisti: {
      type: String,
      default: null
    },
    conto_contabile_vendite: {
      type: String,
      default: null
    },
    provvigioni: {
      type: String,
      default: null
    },
    configDistintaBase:
      [
        {
          tipoAssociazione: { type: String },
          tipoEsterno: { type: Boolean },//interno false o esterna true
          nomeModelloEsterno: { type: String },
          casoElse: { type: Boolean }, //l'ultima riga è il caso else
          regole: [
            {
              lunghezzaMaxCodice: { type: Number },
              regoleCodice: {
                pos1: { type: String },
                pos2: { type: String },
                pos3: { type: String },
                pos4: { type: String },
                pos5: { type: String },
                pos6: { type: String },
                pos7: { type: String },
                pos8: { type: String },
                pos9: { type: String },
                pos10: { type: String },
                pos11: { type: String },
                pos12: { type: String },
                pos13: { type: String },
                pos14: { type: String },
                pos15: { type: String },
              },
              componenti: [
                {
                  id: {
                    type: Schema.ObjectId, // ref a componente
                    ref: 'Componente'
                  },
                  qt: { type: Number }
                }
              ]
            }
          ]
        }
      ]
  });

ModelloSchema.index({ mdate: -1 });
ModelloSchema.index({ nome: 1, serie: 1 }, { unique: true });

mongoose.model('Modello', ModelloSchema, 'Modello');
