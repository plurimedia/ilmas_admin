var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	ColoriSchema = new Schema({
		mdate: {
			type: Date,
			default: Date.now
		},
		codice: {
			type: String,
			required: true
		},
		codice_colore_cover: {
			type: String,
			required: false
		},
		codice_colore_cover_hex: {
			type: String,
			required: false
		},
		descrizione_cover: {
			type: String,
			required: false
		},
		descrizione_cover_en: {
			type: String,
			required: false
		},
		codice_colore_estruso: {
			type: String,
			required: false
		},
		codice_colore_estruso_hex: {
			type: String,
			required: false
		},
		descrizione_estruso: {
			type: String,
			required: false
		},
		ordinamento: {
			type: Number,
		},
		descrizione_estruso_en: {
			type: String,
			required: false
		},
		finitura: {
			type: String
		},
		finitura_en: {
			type: String
		},
		last_user: { // utente che ha effettuato l'ultima modifica
      type: Schema.Types.Mixed
    }
	});

mongoose.model('Colori', ColoriSchema, 'Colori');