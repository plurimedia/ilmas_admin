var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	CreazioneDistinteSchema = new Schema({
		dt_inizio: {
			type: Date,
			default: Date.now
		},
		dt_fine: {
			type: Date
		},
		info : {
			type: Schema.Types.Mixed
		},
		esito : {
			type: String
		}
	});

mongoose.model('CreazioneDistinte', CreazioneDistinteSchema, 'CreazioneDistinte');