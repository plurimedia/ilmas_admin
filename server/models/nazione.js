var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	NazioneSchema = new Schema({
		mdate: {
			type: Date,
			default: Date.now
		},
		nome: {
			type: String,
			required: true
		},
		codice: {
			type: String,
			required: true
		}
	});

mongoose.model('Nazione', NazioneSchema, 'Nazione');