var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	AttivitaSchema = new Schema({
		mdate: {
			type: Date,
			default: Date.now
		}, 
		cdate: {
			type: Date,
			default: Date.now
		}, 
		last_user: { // utente che ha effettuato l'ultima modifica
			type: Schema.Types.Mixed
		},
		create_user: { // utente che ha creato la  modifica
			type: Schema.Types.Mixed
		},
		codice: { // ilmas/toshiba ecc...
			type: String
		},
		titolo: {
			type: String,
			required: true
		},
		descrizione: {
			type: String
		},
		stato: {
			type: String
		},
		effort_preventivo: { 
			type: String
		},
		effort_consuntivo: { 
			type: String
		}, 
        approvata: { 
			type: Boolean
		},
		mailApprovazioneInviata: { 
			type: Boolean
		},
		mailEffortInviata: { 
			type: Boolean
		},
		data_rilascio: { 
			type: Date
		},
		data_approvazione: { 
			type: Date
		},
		allegati: [{
			type: Schema.Types.Mixed
		}],
		priorita: {
			type: String
		}
	});

mongoose.model('Attivita', AttivitaSchema, 'Attivita');