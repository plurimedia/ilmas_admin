var mongoose = require('mongoose'),
    Schema = mongoose.Schema,

    DwhRicercaSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        ricerca: {
              type: String
        },
        anno: {
            type: String
        },
        mese: {
            type: String  
        },
        giorno: {
            type: String
        },
        totaleAnno: {
            type: Number,
             default: 1
        },
        totaleRisultati: {
            type: Number,
             default: 0
        } 
    });

DwhRicercaSchema.index({mdate: -1});
DwhRicercaSchema.index({anno:1});
mongoose.model('DwhRicerca', DwhRicercaSchema, 'DwhRicerca');
