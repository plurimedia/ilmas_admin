var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	validator = require('validator'), // libreria esterna per validare gli input
	OffertaSchema = new Schema({
		idate: {
			type: Date
		},
		mdate: {
			type: Date,
			default: Date.now
		},
		m_user_id: { //utente che ha modificato l'offerta
			type: String
		},
		numero: {
			type: String,
			required: true,
			unique: true
		},
		stato: {
			type: Number,
			required: true
		},
		user: { // tengo tutto comunque, "utente che ha creato l'offerta"
			type: Schema.Types.Mixed,
			required: true
		},
		user_id: { // tengo separato per le ricerche, "utente che ha creato l'offerta"
			type: String,
			required: true
		},
		user_email: { // tengo separato per le ricerche, "utente che ha creato l'offerta"
			type: String,
			required: true,
			validate: [ validator.isEmail, 'Email non valida' ]
		},
		user_nome: { // tengo separato per le ricerche, "utente che ha creato l'offerta"
			type: String,
			required: true
		},
		data: {
			type: Date
		},
		dal: {
			type: Date
		},
		al: {
			type: Date
		},
		destinatario : {
			type: String,
			required: true,
			validate: [ validator.isEmail, 'Email non valida' ]
		},
		destinatario_nome : {
			type: String
		},
		destinatario_azienda : {
			type: String
		},
		attenzione : {
			type: String,
			required: true
		},
		oggetto : {
			type: String,
			required: true
		},
		prodotti: {
			type: [Schema.Types.Mixed],
			required: true
		},
		totaleprodotti: {
			type: Number,
			required: true
		},
		totaleofferta: {
			type: Number,
			required: true
		},
		sconto: {
			type: Number
		},
		termini : {
			type: String
		},
		termini_en : {
			type: String
		},
		note_cliente : {
			type: String
		},
		note : {
			type: String
		},
		email: { // tutta la mail
			type: Schema.Types.Mixed
		},
		email_id : { // id di mandrill
			type: String
		},
		linguaOfferta : {  //it -  en
			type: String
		}
		
	});

mongoose.model('Offerta', OffertaSchema, 'Offerta');