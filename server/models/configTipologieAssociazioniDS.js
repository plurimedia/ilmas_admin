var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	ConfigTipologieAssociazioniDSSchema = new Schema({
		mdate: {
			type: Date,
			default: Date.now
		},
		tipo: {
			type: String,
			required: true
		},
		last_user: { // utente che ha effettuato l'ultima modifica
            type: Schema.Types.Mixed
        }
	});

mongoose.model('ConfigTipologieAssociazioniDS', ConfigTipologieAssociazioniDSSchema, 'ConfigTipologieAssociazioniDS');