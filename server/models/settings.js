var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	SettingsSchema = new Schema({
		mdate: {
			type: Date,
			default: Date.now
		},
		offerte : {
			type: Schema.Types.Mixed,
			required: true
		}
	});

mongoose.model('Settings', SettingsSchema, 'Settings');