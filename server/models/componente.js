var mongoose = require('mongoose'),
    _ = require('underscore'), 
  
    Schema = mongoose.Schema,
    
    ComponenteSchema = new Schema({
        codice: {
            type: String,
            required: true,
            unique: true
        },
        nome: {
            type: String,
            required: true,
        },
        keywords: [
            { type: String }
        ],
        tipo: {
            type: String,
            required: true,
        },
        mdate: {
            type: Date,
            default: Date.now
        },
        lettera: {
            type: String
        },
        fornitore: { // campo  usato solo nella prima versione della distinta base
            type: String
        },
        last_user: { // utente che ha effettuato l'ultima modifica
            type: Schema.Types.Mixed
        },
        componenti_figlio: [
            {
                qt: {type: Number, default: 1},
                _id: {
                    type : Schema.Types.ObjectId,
                    ref: 'Componente'
                }
            }
        ],
        prezzo: {
            type: Number,
            default: 0
        },
        prezzo_old: {
            type: Number,
            default: 0
        },
        prezzofix: { // si/no (led)
            type: Boolean
        },
        categoria: {
            type: String,
            required: true,
        },
        cod_fornitore: {
            type: String
        },
        codice_interno: {
            type: String,
            required: true,
        },
        gruppo:{
            type: String,
            required: true,
        }
    }), 

    _keyWords = function (p) {

        var result = [p.codice,p.codice.toLowerCase()], // metti codice e codice lowercase
            _string = function (s) { // tutte le parole di una stringa senza il trattino, upper e lower
                if (s){
                 var keywords = _.filter(s.split(' '),function(k){return k!=='-'});
               
                return [
                        keywords,
                        _.collect(keywords,function(k){return k.toLowerCase()}),
                        _.collect(keywords,function(k){return k.toUpperCase()})
                ]
                }
            },
            _value = function (v,u){
                return [v+u,v+u.toUpperCase(),v+u.substr(0,1).toUpperCase()+u.substr(1)];
            },
        _keywordsCodice = function (codice) { // metto anche il codice in pezzi (ricerca per parti di codice)
            var tmp = [];
            if (codice.length>3){ 
                for (i=3;i<=codice.length;i++){
                    tmp.push(codice.substring(0,i)) 
                } 
            }
            else{
                tmp.push(codice)
            }
            
            return _.compact(tmp);
        };

         result.push(_string(p.nome));

         result.push(_keywordsCodice(p.codice.toUpperCase()))

         result.push(_keywordsCodice(p.codice.toLowerCase()))

         result.push(p.tipo.toLowerCase())

         result.push(p.tipo.toUpperCase())

         result.push(_keywordsCodice(p.nome.toLowerCase()))

         result.push(_keywordsCodice(p.nome.toUpperCase()))

        return _.unique(_.flatten(result));
    };

    

ComponenteSchema.index({codice: 1}, { unique: true });
ComponenteSchema.index({keywords: 1});
ComponenteSchema.index({mdate: -1});




 

// prima di un update calcolo le keyword
ComponenteSchema.pre('update',
    function(next){ 
        //var componente = this._update['$set'];
        var componente = this._update;
        var p = _.clone(componente);  
        componente.keywords = _keyWords(p);  

        next(); 
       
    }
);
// prima di un save calcolo le keyword
ComponenteSchema.pre('save', 
    function(next){ 
        var componente = this; 
        var p = _.clone(componente); 
        componente.keywords = _keyWords(p); 
        next(); 
    }
);




mongoose.model('Componente', ComponenteSchema, 'Componente');