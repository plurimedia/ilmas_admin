var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    EventoSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        last_user: { // utente che ha effettuato l'ultima modifica
			type: Schema.Types.Mixed
		},
        titolo_it: {
            type: String, 
            required: true,
        },
        titolo_en: {
            type: String, 
            required: true,
        },
        dataDa: {
            type: Date,
            required: true,
        },
        dataA: {
            type: Date,
        },
        posizione: {
            type: String
        },
        sitoWeb: {
            type: String,
        },
        metaDescription_it: {
            type: String,
        },
        metaDescription_en: {
            type: String,
        },
        testo_it: {
            type: String,
        },
        testo_en: {
            type: String,
        },
        imgPrincipale: { // di cloudinary
            type: String
        },
        imgLista: { // di cloudinary
            type: String
        },
        pubblicato: { // si/no
            type: Boolean
        },
        
    }
)

EventoSchema.index({mdate: -1});
mongoose.model('Evento', EventoSchema, 'Evento');