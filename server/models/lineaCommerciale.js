var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	LineaCommercialeSchema = new Schema({
		mdate: {
			type: Date,
			default: Date.now
		},
		nome: {
			type: String, 
			required: true,
 		},
		note: {
			type: String 
 		},
 		foto: { // di cloudinary
			type: String,
			default: null
		},
 		categorieAssociate: [
            {
                type : Schema.Types.ObjectId,
                ref: 'Categoria'
            }
        ], 
        modelliAssociati: [
            {
                type : Schema.Types.ObjectId,
                ref: 'Modello'
            }
        ], 
	    prodottiAssociati: [
	        {
	            type : Schema.Types.ObjectId,
	            ref: 'Prodotto'
	        }
	    ],
	    pubblicato: {
	        type: Boolean,
	        default: false
	    },
        pubInfoLinea: {
            type: Boolean,
            default: false
        },
        pubSettore: {
            type: Boolean,
            default: false
        }
	}); 

LineaCommercialeSchema.index({categorieAssociate: 1});
LineaCommercialeSchema.index({modelliAssociati: 1});
LineaCommercialeSchema.index({nome: 1});
LineaCommercialeSchema.index({mdate: -1});

mongoose.model('LineaCommerciale', LineaCommercialeSchema, 'LineaCommerciale');

