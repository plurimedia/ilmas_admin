var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    DwhSchedaDettaglioSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        codice: {
              type: String // codicie prodotto per semplificare le ricerche
        },
        prodotto: {
            type: Schema.ObjectId, // ref a prodotto
            ref: 'Prodotto'
        },
        anno: {
            type: String
        },
        mese: {
            type: String  
        },
        giorno: {
            type: String
        },
        totaleAnno: {
            type: Number,
            default: 1
        }
    });

DwhSchedaDettaglioSchema.index({anno:1});
DwhSchedaDettaglioSchema.index({codice:1});
mongoose.model('DwhSchedaDettaglio', DwhSchedaDettaglioSchema, 'DwhSchedaDettaglio');
