var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	ImmagineSchema = new Schema({
		mdate: {
			type: Date,
			default: Date.now
		},
		nome: {
			type: String,
			required: true
		},
		tipo: {
			type: String,
			required: true
		},
		id: { // di cloudinary
			type: String,
			required: true
		}
	});

ImmagineSchema.index({tipo: 1});
mongoose.model('Immagine', ImmagineSchema, 'Immagine');
