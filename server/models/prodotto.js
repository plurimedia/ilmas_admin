var mongoose = require('mongoose'),
  _ = require('underscore'),
  Utils = require('../routes/utils'),
  Schema = mongoose.Schema,
  Modello = mongoose.model('Modello'),
  Categoria = mongoose.model('Categoria'),

  ProdottoSchema = new Schema({
    mdate: {
      type: Date,
      default: Date.now
    },
    pubblicato: {
      type: Boolean,
      default: false
    },
    codice: {
      type: String,
      required: true,
      unique: true
    },
    codiceEan: {
      type: String
    },
    foto: {
      type: String // id di cloudinary
    },
    fotoQuotata: {
      type: String // id di cloudinary
    },
    disegno: {
      type: String // id di cloudinary
    },
    fotometria: {
      type: String // id di cloudinary
    },
    prezzo: {
      type: Number,
      default: 0
    },
    peso: {
      type: Number
    },
    qtamin: { // qta minima ordinabile
      type: Number
    },
    linea: { // toshiba, estro, ilmas
      type: String,
      required: true
    },
    destinazione: { // indoor, outdoor
      type: String,
      required: true
    },
    tradizionale: { // si/no (led)
      type: Boolean
    },
    categoria: {
      type: Schema.ObjectId, // ref a categoria
      ref: 'Categoria'
    },
    sottocategoria: { // fish, meat...
      type: String
    },
    modello: {
      type: Schema.ObjectId, // ref a modello
      ref: 'Modello'
    },
    modello_cript: {
      type: Schema.ObjectId, // ref a modello
      ref: 'Modello'
    },
    lampadina: { // solo per i tradizionali
      type: String
    },
    k: { // gradi kelvin
      type: String
    },
    lm: { // lumen
      type: String
    },
    w: { // potenza
      type: String
    },
    luci: { // Numero luci
      type: Number
    },
    ma: { // corrente
      type: String
    },
    corrente_v: { // corrente in V
      type: String
    },
    v: { // tensione
      type: String
    },
    hz: { // frequenza
      type: String
    },
    fascio: { // in gradi
      type: String
    },
    irc: { // Indice di resa cromatica
      type: String
    },
    dimmerabile: { // si/no
      type: Boolean
    },
    durata: { // durata (ore)
      type: String
    },
    attacco: { // E27
      type: String
    },
    note: {
      type: String
    },
    satinato: { // si/no
      type: Boolean
    },
    generazione: {
      type: String
    },
    macadam: {
      type: String
    },
    colore: {
      type: String
    },
    verniciatura: {
      type: String
    },
    verniciatura_en: {
      type: String
    },
    moduloled: {
      type: String
    },
    les: {
      type: String
    },
    ugr: {
      type: String
    },
    tmax: { // temperatura ambiente massima
      type: String
    },
    alimentatore: {
      type: String
    },
    alimentatore_incluso: {
      type: Boolean
    },
    descrizione_applicazione: {
      type: String
    },
    descrizione_applicazione_en: {
      type: String
    },
    orientabile: {
      type: String
    },
    inclinabile: {
      type: String
    },
    corpo_it: {
      type: String
    },
    corpo_en: {
      type: String
    },
    ottica_it: {
      type: String
    },
    ottica_en: {
      type: String
    },
    led_it: {
      type: String
    },
    led_en: {
      type: String
    },
    adattatore_it: {
      type: String
    },
    adattatore_en: {
      type: String
    },
    ce: {
      type: Boolean
    },
    f: {
      type: Boolean
    },
    rischio_fotobiologico: {
      type: String
    },
    classe_isolamento: {
      type: String
    },
    classe_energetica: {
      type: String,
      default: ''
    },
    ip: {
      type: String
    },
    foro: {
      type: String
    },
    lunghezza: {
      type: String
    },
    vita_utile: { // ore
      type: Number
    },
    vita_utile_desc: { // ore
      type: String
    },
    valore_di_perdita: {
      type: String
    },
    divieto_controsoffitti: {
      type: Boolean
    },
    filo_incandescente: {
      type: String
    },
    keywords: [
      { type: String }
    ],
    file_ldt: {
      nome: { type: String },
      url: { type: String } // url completo su s3
    },
    file_pdf: {
      nome: { type: String },
      url: { type: String } // url completo su s3
    },
    accessori: [
      {
        type: Schema.Types.ObjectId,
        ref: 'Prodotto'
      }
    ],
    componenti: [
      {
        qt: { type: Number, default: 1 },
        _id: {
          type: Schema.ObjectId,
          ref: 'Componente'
        },
        tipoAssociazione: {
          type: String
        }
      }
    ],
    exportMetel: {
      type: Boolean,
      default: false
    },
    ik: {
      type: String
    },
    flusso_lum_app: {
      type: String
    },
    potenza_sistema: {
      type: Number
    },
    note_private: {
      type: String
    },
    padre: {
      type: String
    },
    trascodifica: {
      type: String
    },
    distintabase: {
      type: String
    },
    last_user: { // utente che ha effettuato l'ultima modifica
      type: Schema.Types.Mixed
    },
    kit: {
      type: Boolean,
      default: false
    },
    criptato: {
      type: Boolean,
      default: false
    },
    codice_prodotto_fornitore: {
      type: String
    },
    speciale: {
      type: Boolean,
      default: false
    },
    serie: { //rappresenta una "generazione" di prodotti. Nel 2020 stiamo passando alla generazione aComponenti, quelli prima sono aProdotti
      type: String,
      default: "aProdotti"
    },
    provenienza: {
      type: String,
      default: null
    },
    listino: {
      type: String,
      default: null
    },
    codice_fornitore: {
      type: String,
      default: null
    },
    descr_codice_fornitore: {
      type: String,
      default: null
    },
    costo: {
      type: String,
      default: null
    },
    fornitore: {
      type: String,
      default: null
    },
    da_inviare_kite: {
      type: Number,
      default: 0
    }
  });

ProdottoSchema.index({ mdate: -1 });
ProdottoSchema.index({ "mdate": -1, "keywords": 1 });
ProdottoSchema.index({ keywords: 1 });
ProdottoSchema.index({ moduloled: 1 });
ProdottoSchema.index({ colore: 1 });
ProdottoSchema.index({ generazione: 1 });
ProdottoSchema.index({ codice: 1 });
ProdottoSchema.index({ modello: 1, pubblicato: 1 });
ProdottoSchema.index({ kit: 1 });
ProdottoSchema.index({ criptato: 1 });
ProdottoSchema.index({ speciale: 1 });
ProdottoSchema.index({ generazione: 1 });
ProdottoSchema.index({ modello: 1 });
ProdottoSchema.index({ linea: 1 });
ProdottoSchema.index({ categoria: 1 });

// prima di un update ricalcolo le keyword
ProdottoSchema.pre('update', function (next) {

  //var prodotto = this._update['$set']; 
  var prodotto = this._update;

  if (prodotto && !prodotto.file_ldt)
    this._update['$unset'] = { file_ldt: '' }

  if (prodotto && !prodotto.file_pdf)
    this._update['$unset'] = { file_pdf: '' }

  Modello.findOne({ _id: prodotto.modello },

    function (err, result) {

      if (!err) {
        var p = _.clone(prodotto);

        if (result) {
          p.modello = result; // trovo il nome del modello da cui dipendono le keywords
        }

        Categoria.findOne({ _id: prodotto.categoria },

          function (err, result) {

            if (result) {
              p.categoria = result; // trovo il nome della categoria da cui dipendono le keywords
              prodotto.keywords = Utils.keyWords(p);
            }
            next();
          }
        )
      }
    }
  )
});

// anche al salvataggio
ProdottoSchema.pre('save', function (next) {
  var prodotto = this;

  Modello.findOne({ _id: prodotto.modello },

    function (err, result) {

      if (!err) {
        var p = _.clone(prodotto);

        if (result)
          p.modello = result; // trovo il nome del modello da cui dipendono le keywords

        Categoria.findOne({ _id: prodotto.categoria },

          function (err, result) {

            if (result) {
              p.categoria = result; // trovo il nome della categoria da cui dipendono le keywords
              prodotto.keywords = Utils.keyWords(p);
            }

            next();
          }
        )
      }
    }
  )
});

mongoose.model('Prodotto', ProdottoSchema, 'Prodotto');
