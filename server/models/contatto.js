var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	async = require('async'),
    _ = require('underscore'),
	validator = require('validator'), // libreria esterna per validare gli input
	ContattoSchema = new Schema({
		mdate: {
			type: Date,
			default: Date.now
		},
		idate: {
			type: Date
 		},
 		m_user_id: { //utente che ha modificato il record
			type: String
		},
		i_user_id: { //utente che ha inserito il record
			type: String
		},
		nome: {
			type: String
		},
		azienda: {
			type: String
		},
		email: {
 			type: String,
			required: true,
			validate: [ validator.isEmail, 'Email non valida' ]
		},
		indirizzo: {
			type: String
		},
		comune: {
			type: String
		},
		cap: {
			type: String
		},
		provincia: {
			type: String
		},
		nazione: {
			type: String
		},
		piva: {
			type: String
		},
		cf: {
			type: String
		},
		telefono: {
			type: String
		},
		fax: {
			type: String
		},
		note: {
			type: String
		},
		tags: {
			type: [String]
		},
        keywords: [
        	{type: String}
        ]

	}),
    _keyWords = function (c) {
        // genero le keywords: alcuni campi potrebbe non esserci...
        var result = [];
        
        result.push(c.email);
        result.push(c.email.split('@')[0])
        result.push(c.email.split('@')[1].split('.')[0])
        
        if(c.nome)
            result.push(_.map(c.nome.split(' '),function(k){return k.toLowerCase()}));
        if(c.nome)
            result.push(_.map(c.nome.split(' '),function(k){return k.toUpperCase()}));
        if(c.nome)
            result.push(_.map(c.nome.split(' '),function(k){return k}));

        if(c.azienda)
            result.push(_.map(c.azienda.split(' '),function(k){return k.toLowerCase()}));
        if(c.azienda)
            result.push(_.map(c.azienda.split(' '),function(k){return k.toUpperCase()}));
        if(c.azienda)
            result.push(_.map(c.azienda.split(' '),function(k){return k}));
 

        if(c.tags.length)
            result.push(c.tags)
         
		if(c.comune)
            result.push(_.map(c.comune.split(' '),function(k){return k.toLowerCase()}));
        if(c.comune)
            result.push(_.map(c.comune.split(' '),function(k){return k.toUpperCase()}));
        if(c.comune)
            result.push(_.map(c.comune.split(' '),function(k){return k}));

 
        if(c.nazione)
            result.push(_.map(c.nazione.split(' '),function(k){return k.toLowerCase()}));
        if(c.nazione)
            result.push(_.map(c.nazione.split(' '),function(k){return k.toUpperCase()}));
        if(c.nazione)
            result.push(_.map(c.nazione.split(' '),function(k){return k}));

            
        return _.flatten(result);
    };



// prima di un update ricalcolo le keyword
ContattoSchema.pre('update', function(next){
	//var contatto = this._update['$set'];
	var contatto = this._update;
    contatto.keywords = _keyWords(contatto);
    next();
});

// anche al salvataggio
ContattoSchema.pre('save', function(next){
    var contatto = this;
    contatto.keywords = _keyWords(contatto);
    next();
});

ContattoSchema.index({keywords: 1});
ContattoSchema.index({mdate: -1});
ContattoSchema.index({email: -1});
ContattoSchema.index({nazione: -1});
ContattoSchema.index({provincia: -1});
mongoose.model('Contatto', ContattoSchema, 'Contatto');
