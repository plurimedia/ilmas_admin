var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    
    CodiceEanSchema = new Schema({
        codiceEan: {
            type: String
        },
        keywords: [
            { type: String }
        ],
        codice: {
            type: String
        },
        idCodice: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Prodotto'
        }
    });
 
CodiceEanSchema.index({keywords: 1});
mongoose.model('CodiceEan', CodiceEanSchema, 'CodiceEan');
