var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    DwhEtichettaSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        tipo: {
            type: String //Bianca o Grigia
        },
        prodotto: {
            type: Schema.ObjectId, // ref a prodotto, lasciato a null per gli Estro
            ref: 'Prodotto'
        },
         anno: {
            type: String
        },
         mese: {
            type: String  
        },
        giorno: {
            type: String
        }
    });

DwhEtichettaSchema.index({idate: -1});
DwhEtichettaSchema.index({mese: 1});
DwhEtichettaSchema.index({anno: 1});
DwhEtichettaSchema.index({tipo: 1});
mongoose.model('DwhEtichetta', DwhEtichettaSchema, 'DwhEtichetta');
