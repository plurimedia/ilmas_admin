var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	DwhLogSchema = new Schema({
		idate: {
			type: Date,
			default: Date.now
		},
		anno: { 
			type: String
 		},
		mese: { 
			type: String 
		},
		giorno: { 
			type: String 
		},
		user_id: { 
			type: String 
		},
		user_email: {
			type: String, 
 		},
		user_nome: {
			type: String 
		},
		path: {
			type: String 
		}

	});
DwhLogSchema.index({idate: -1});
mongoose.model('DwhLog', DwhLogSchema, 'DwhLog');