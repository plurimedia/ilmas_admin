var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    NewsSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        last_user: { // utente che ha effettuato l'ultima modifica
			type: Schema.Types.Mixed
		},
        titolo_it: {
            type: String, 
            required: true,
        },
        titolo_en: {
            type: String 
        },
        dataPubblicazione: {
            type: Date,
            required: true,
        }, 
        abstract_it: {
            type: String,
        },
        abstract_en: {
            type: String,
        },
        descrizione_it: {
            type: String,
        },
        descrizione_en: {
            type: String,
        },
        url: {
            type: String,
        },
        pubblicato: { // si/no
            type: Boolean
        },
        file_pdf: {
            nome: {type: String},
            url: {type: String } // url completo su s3
        },
    }
)

NewsSchema.index({mdate: -1});
mongoose.model('News', NewsSchema, 'News');