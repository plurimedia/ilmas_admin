var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	ComuneSchema = new Schema({
		mdate: {
			type: Date,
			default: Date.now
		},
		nome: {
			type: String,
			required: true
		},
		provincia: {
			type: String,
			required: true
		},
		cap: {
			type: String,
			required: true
		},
		regione: {
			type: String,
			required: true
		}
	});

mongoose.model('Comune', ComuneSchema, 'Comune');