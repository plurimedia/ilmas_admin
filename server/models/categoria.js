var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    CategoriaSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        }, 
        nome: {
            type: String, // es over led ps
            required: true
        },
        nome_en: {
            type: String,
        },
        linea: { // ilmas/toshiba ecc...
            type: String
        },
        foto: { // di cloudinary
            type: String
        },
        uso: { // outdoor/indoor/entrambi
            type: String
        },
        pubblicato: { // si/no
            type: Boolean
        },
        ordinamento_sito: {
            type: Number,
            default: 1
        },
        voce_doganale: { 
            type: String
        },
        tipo: { 
            type: String
        },
        last_user: { // utente che ha effettuato l'ultima modifica
            type: Schema.Types.Mixed
        },
        food: {
            type: Boolean
        },
        serie: { //rappresenta una "generazione" di prodotti. Nel 2020 stiamo passando alla generazione aComponenti, quelli prima sono aProdotti
            type: String,
            default: "aProdotti"
        },
        linea_prodotto: {
            type: String,
            default: null
        }
    });

CategoriaSchema.index({ nome: 1, serie: 1}, { unique: true });

mongoose.model('Categoria', CategoriaSchema, 'Categoria');

