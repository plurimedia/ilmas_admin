var moment = require('moment')

var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    DashboardSchema = new Schema({
        mdate: {
            type: String,
            default: moment().format('DD/MM/YYYY'),
            required: true
        },
        idate: {
            type: Date,
            default: Date.now
        },
        countCategorie: {
            type: Number,
            required: true
        },
        countCategorieEstro: {
            type: Number,
            required: true
        },
        countComponenti: {
            type: Number,
            required: true
        },
        countCorpi: {
            type: Number,
            required: true
        },
        countLed: {
            type: Number,
            required: true
        },
        countDriver: {
            type: Number,
            required: true
        },
        countEtichette: {
            type: Number,
            required: true
        },
        countBianche: {
            type: Number,
            required: true
        },
        countGrigie: {
            type: Number,
            required: true
        },
        countContatti: {
            type: Number,
            required: true
        },
        countOfferte: {
            type: Number,
            required: true
        },
        countProdotti: {
            type: Number,
            required: true
        },
        countG6: {
            type: Number,
            required: true

        },
        countG5: {
            type: Number,
            required: true

        },
        countG4: {
            type: Number,
            required: true

        },
        countD1: {
            type: Number,
            required: true

        },
        countProdottiEstro: {
            type: Number,
            required: true
        },
        countG6Estro: {
            type: Number,
            required: true

        },
        countG5Estro: {
            type: Number,
            required: true

        },
        countG4Estro: {
            type: Number,
            required: true

        },
        countD1Estro: {
            type: Number,
            required: true

        },
        countModelli: {
            type: Number,
            required: true
        },
        countModelliEstro: {
            type: Number,
            required: true
        },
        countEan: {
            type: Number,
            required: true
        }
    });

DashboardSchema.index({mdate: -1});
mongoose.model('Dashboard', DashboardSchema, 'Dashboard');
