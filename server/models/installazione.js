var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    InstallazioneSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        last_user: { // utente che ha effettuato l'ultima modifica
			type: Schema.Types.Mixed
		},
        descrizione_it: {
            type: String, 
            required: true,
        },
        descrizione_en: {
            type: String, 
            required: true,
        },
        anno: {
            type: Number,
        },
        dataPubblicazione: {
            type: Date
        },
        prodotti_it: {
            type: String
        },
        prodotti_en: {
            type: String
        },
        linea: { // toshiba, estro, ilmas
            type: String,
            required: true
        },
        foto: [{
            type: Schema.Types.Mixed
        }],
        progetti_simili: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Installazione'
        }],
        pubblicato: { // si/no
            type: Boolean
        }
    }
)

InstallazioneSchema.index({mdate: -1});
mongoose.model('Installazione', InstallazioneSchema, 'Installazione');