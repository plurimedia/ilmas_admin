var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	validator = require('validator'), // libreria esterna per validare gli input
	MailingListSchema = new Schema({
		idate: {
			type: Date,
			default: Date.now
		},
		mdate: {
			type: Date,
			default: Date.now
		},
		i_user_id: { //utente che ha creato la mail
			type: String
		}, 
		from_email:{ //mittente email
            	type: String,
				required: true,
				validate: [ validator.isEmail, 'Email non valida' ]
        },
        from_nome: { //mittente nome
			type: String
		},
		stato: {
			type: Number
		},
		email_id : { // id di mandrill
			type: String
		},
		a:[
            {
            	type: String,
				required: true,
				validate: [ validator.isEmail, 'Email non valida' ]
            }
        ],
		cc:[
            {
            	type: String
 				
            }
        ],
		ccn:[
            {
            	type: String
 				
            }
        ],
		oggetto : {
			type: String,
			required: true
		},
		messaggio : {
			type: String,
			required: true
		}, 
		files:[
            {
            	type: String 
            }
        ],
        nazioni:[ //elenco delle nazioni per gli invii massivi
            {
            	type: String 
            }
        ],
        province:[ //elenco delle province per gli invii massivi
            {
            	type: String 
            }
        ],
        nazioneDesc : {
			type: String
 		} ,
        provinciaDesc : {
			type: String
 		},
 		a_inviomassivo:[ //elenco delle email per gli invii massivi
            {
            	type: String 
            }
        ]
        
	});

mongoose.model('MailingList', MailingListSchema, 'MailingList');