var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    DwhSchedaTecnicaSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        codice: {
              type: String // codicie prodotto per semplificare le ricerche
        },
        prodotto: {
            type: Schema.ObjectId, // ref a prodotto
            ref: 'Prodotto'
        },
        anno: {
            type: String
        },
        mese: {
            type: String  
        },
        giorno: {
            type: String
        },
        provenienza: {
            type: String //sito oppure app
        },
        totaleAnno: {
            type: Number,
            default: 1
        }
    });

DwhSchedaTecnicaSchema.index({anno:1});
DwhSchedaTecnicaSchema.index({codice:1});
mongoose.model('DwhSchedaTecnica', DwhSchedaTecnicaSchema, 'DwhSchedaTecnica');
