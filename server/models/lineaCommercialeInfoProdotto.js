var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
    
	LineaCommercialeInfoProdottoSchema = new Schema({
		mdate: {
			type: Date,
			default: Date.now
		},
		linea: {
            type: Schema.ObjectId, // ref a LineaCommerciale
            ref: 'LineaCommerciale'
        },
        prodotto: {
            type: Schema.ObjectId, // ref a Prodotto
            ref: 'Prodotto'
        },
		prezzoLinea: {
            type: Number,
            default: 0
        },
		codiceLinea: {
            type: String
         }
		   
	});

    
 mongoose.model('LineaCommercialeInfoProdotto', LineaCommercialeInfoProdottoSchema, 
    'LineaCommercialeInfoProdotto');

  