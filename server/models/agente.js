var mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    AgenteSchema = new Schema({
        mdate: {
            type: Date,
            default: Date.now
        },
        last_user: { // utente che ha effettuato l'ultima modifica
			type: Schema.Types.Mixed
		},
        ragioneSociale: {
            type: String, 
            required: true,
        },
        via: {
            type: String
        },
        cap: {
            type: String,
        },
        comune: {
            type: String,
        },
        prov: {
            type: String,
        },
        nazione: {
            type: String,
        },
        tel: {
            type: String,
        },
        email: {
            type: String,
        },
        area: {
            type: String,
        },
        coordinate: {
            lat: {
                type: Number
            },
            long: {
                type:  Number
            }
        },
        coordinates:[
            {
                lat: {
                    type: String
                },
                long: {
                    type:  String
                }
            }
        ],
        contatti:[
            {
                nome: {
                    type: String
                },
                cognome: {
                    type:  String
                },
                email: {
                    type:  String
                },
                tel: {
                    type:  String
                }
            }
        ],
        pubblicato: { // si/no
            type: Boolean
        },
        searchText: {
            type: String,
        }
    }
)

AgenteSchema.pre('update', function(next){
    //var agente = this._update['$set']; 
    var agente = this._update; 
    agente.searchText = agente.ragioneSociale + " " + agente.comune + " " + agente.prov + " " + agente.nazione;
    next(); 
});

AgenteSchema.pre('save', function(next){
    var agente = this;
    agente.searchText = agente.ragioneSociale + " " + agente.comune + " " + agente.prov + " " + agente.nazione;
    next(); 
});

AgenteSchema.index({mdate: -1});
mongoose.model('Agente', AgenteSchema, 'Agente');