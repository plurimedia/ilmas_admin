/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/modificaprodotti_metel;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ModelloSchema = require('../server/models/modello'),
    Modello = mongoose.model('Modello'),
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    Prodotto.find({}).lean()
    .exec(function(err,prodotti){

        console.log('aggiorno '+prodotti.length+' prodotti')

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, function(prodotto,cb){

            var update = {};

             
            console.log("aggiornamento prodotto id = ", prodotto._id);

            update.$set = {};
            update.$set.exportMetel = true;
            bulk.find( { _id: prodotto._id } ).updateOne(update);
            aggiornati ++;
             

            cb();
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0) {
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

        })
    })

    function isModelloToRefactor(modello){
        if (
            modello.match(/OVER/gi)     ||
            modello.match(/Spring/gi)   ||
            modello.match(/summer/gi)   ||
            modello.match(/orion/gi)    ||
            modello.match(/fold/gi)     ||
            modello.match(/tek/gi)      ||
            modello.match(/bell/gi)     ||
            modello.match(/dial 1/gi)   ||
            modello.match(/nuvola/gi)   ||
            modello.match(/star led/gi)
           ){
             console.log("modello To REFACTOR:", modello);
            return true;
        }
        else{
            return false;
        }
    }
}



);
