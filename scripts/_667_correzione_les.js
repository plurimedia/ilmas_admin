/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=10000 scripts/_667_correzione_les;
 */


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    fs      = require('fs'),
    aggiornati = 0,
    parsati = 0,
    logGlobale="";

var transform = {
    "LES15" : "LES 15",
    "LES 19" : undefined,
    "LES 15" : undefined,
    "SAM10" : "SAM 10",
    "LES 10" : undefined,
    "LES 23" : undefined,
    "LES 15 " : "LES 15",
    "LES19" : "LES 19",
    "les  19" : "LES 19",
    "Les 19" : "LES 19",
    "SAM 10" : undefined,
    "SAM15" : "SAM 15",
    "SAM14" : "SAM 14",
    "SAM 15" : undefined
}


console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

        console.log("Connessione aperta, parte il batch."); /// this gets printed to console

        var query = {"les":{$exists:true}}

        Prodotto.find(query).lean()
            //.skip(30000)
            //.limit(10000)
            .exec(
            function(err,prodotti){


                console.log(prodotti.length);

                var bulk = Prodotto.collection.initializeOrderedBulkOp();
                async.each(prodotti,
                    function(prodotto,cb)
                    {
                        parsati++;

                        var p = _.clone(prodotto);

                        if (p.les)
                            console.log(p.les, "**"+transform[p.les]+"**");

                        if (p.les && transform[p.les]) {
                            var l = transform[p.les];
                            console.log(p.les, l);

                            //p.les = l;

                            var logStr = _.clone(aggiornati) +  " codice " + " " +  p.codice + " " +  l;

                            var update = {};
                            update.$set = {};
                            update.$set.les = l;
                            bulk.find( { _id: p._id } ).updateOne(update);

                            aggiornati ++;

                            console.log(logStr);

                            logGlobale = logGlobale + logStr + "\n";
                        }

                        cb();
                    },
                    function(err)
                    {
                        if(err){
                            console.log(err)
                            throw err;
                        }

                        if(aggiornati > 0) {

                            bulk.execute(function(err, result){
                                if(err)
                                    throw err;

                                console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                                mongoose.connection.close();

                                fs.writeFile(".tmp/_667_correzione_les.txt",

                                    logGlobale,

                                    function(err) {
                                        if(err) {
                                            console.log("Log was not saved!", err);
                                            throw err;
                                        }
                                        else
                                            console.log("Log was saved!");
                                    }
                                );

                            })
                            console.log('finito, chiudo la connessione: parsati '+parsati+' aggiornati '+aggiornati+' prodotti')

                        }
                        else {
                            console.log('Non ci sono prodotti da aggiornare parsati '+parsati)
                            mongoose.connection.close();
                        }
                    }
                )
            })


    }
);
