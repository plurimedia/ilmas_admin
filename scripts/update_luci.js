db.Prodotto.find(
    {
        w: { $exists: true },
        w:{$ne:null},
        w:{$ne:''}, 
        luci: { $exists: false },
        generazione: {
            $in: [
                 'G6','V1','',null
            ]
        }
    },
        { _id: 1, w:1 , codice:1}).forEach((p)=> {

    let isnum= /^\d+$/.test(p.w), isnum2;

    if (isnum) return;

    if (p.w){
        if (p.w.toLowerCase().indexOf("x") === -1){
            //non esiste nessuna x
            return;
        }
        //chiedere a alessandro...
        /*
        if (p.w.length<3 || p.w.toLowerCase().indexOf("x") === -1)
        {
            print ('Malformed w 1:',p.codice, " ", p.w.length, " " , p._id, " ",  p.w);
            return;
        }
        */

        let parts= p.w.toLowerCase().split('x');

        if (parts.length !== 2)
        {
            print ('Malformed 2  codice:', p.codice, " luci:" , p.luci, " w:" , p.w );
            return;
        }

        p.luci= parts[0].trim();
        p.w= parts[1].trim();

        //isnum= /^\d+$/.test(p.w);
        //isnum2= /^\d+$/.test(p.luci);

        isnum= isNaN(p.w)
        isnum2=isNaN(p.luci);

        if (isnum || isnum2)
        {
            print ('Malformed 3  codice:', p.codice, " luci:" , p.luci, " w:" , p.w );
            return;
        }

        if (p.luci > 1){
            db.Prodotto.updateOne({ _id: p._id }, { $set: { w: p.w, luci: p.luci } });
            return;
        } else{
            db.Prodotto.updateOne({ _id: p._id }, { $set: { w: p.w } });
            return;
        }
    }
});
