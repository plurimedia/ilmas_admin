const { forEach, initial } = require('underscore');
const Excel = require('exceljs'); 
const util = require('util'); 

var mongoose = require('mongoose'),
CategoriaSchema = require('../server/models/categoria'),
ModelloSchema = require('../server/models/modello'),
ProdottoSchema = require('../server/models/prodotto'),
Prodotto = mongoose.model('Prodotto'),
fs = require('fs'),
pdf = require('html-pdf'),
CONFIG = require('../server/config.js'),
db = CONFIG.MONGO,
_ = require('underscore'),
Mustache = require('mustache'),
classeEnergetica = require('../server/routes/utils.js').classeEnergetica,
Util = require('../server/routes/utils.js'),
formatters = require('../server/routes/utils.js').formatters,
headerSchedaTecnica = fs.readFileSync('./server/templates/pdf/intestazione_scheda_tecnica.html','utf8'), // scheda tecnica...
footerSchedaTecnica = fs.readFileSync('./server/templates/pdf/footer_scheda_tecnica.html','utf8'), // footer scheda tecnica cc...
tpl = [],
css = fs.readFileSync('./server/templates/pdf/pdf.css','utf8'); // comune a tutti

tpl[0] = fs.readFileSync('./server/templates/pdf/scheda_tecnica.html', 'utf8')
tpl[1] = fs.readFileSync('./server/templates/pdf/schedaTecnicaBinario.html', 'utf8')

mongoose.connect(db);

function normalizzaDatiProdotto(prodotto) {
    if (prodotto.classe_isolamento === '1') // sistemo la classe per mustache (non posso fare campo = valore (logicless))
        prodotto.classe1 = true;

    if (prodotto.classe_isolamento === '2')
        prodotto.classe2 = true;

    if (prodotto.classe_isolamento === '3')
        prodotto.classe3 = true;

    if (prodotto.descrizione_applicazione || prodotto.orientabile || prodotto.inclinabile)
        prodotto.mostra_descrizione_applicazione = true;

    if (prodotto.foro === '')
        delete prodotto.foro;

    if (prodotto.categoria &&
        (prodotto.categoria.nome.indexOf('Incassi') !== -1 ||
         prodotto.categoria.nome.indexOf('incassi') !== -1)) // per stampare l'icona degli incassi
        prodotto.incassi = true;

    if (prodotto.foro && prodotto.foro.indexOf('x') !== -1 || prodotto.foro && prodotto.foro.indexOf('X') !== -1)
        prodotto.foro_quadrato = true;

    if (prodotto.foro && prodotto.foro.indexOf('x') === -1 && prodotto.foro && prodotto.foro.indexOf('X') === -1)
        prodotto.foro_tondo = true;

    if (prodotto.accessori && prodotto.accessori.length && Array.isArray(prodotto.accessori))
        prodotto.mostraAccessori = true;

    prodotto = Util.sistemaIcone(prodotto);
    prodotto = classeEnergetica(prodotto);

    return prodotto;
}
  
const filename = './schedeTecniche/prova.xlsx';
const workbook = new Excel.Workbook();
workbook.creator ='test'; 
workbook.modified ='42';

let prodotti = [];

(async function() {
   const workbookReader = new Excel.stream.xlsx.WorkbookReader(filename);
   for await (const worksheetReader of workbookReader) {
    for await (const row of worksheetReader) {
     let prodotto = await Prodotto.findOne({ codice: row.values[1] })
       .populate('modello', { 'nome': 1, 'peso': 1, 'volume': 1, 'ecolamp': 1 })
       .populate('modello_cript', { 'nome': 1 })
       .populate('categoria', 'nome')
       .populate('accessori')
       .exec();
         
        if (!prodotto) {
          console.log('prodotto non trovato: ', row.values[1]);
        } else {
          prodotto.en = 'en';

          if (prodotto.accessori && prodotto.accessori.length && Array.isArray(prodotto.accessori))
            try {
              prodotto.accessori = prodotto.accessori.filter(a => a && a._id);
            } catch (e) {
              console.log('errore accessori', prodotto, e);
            }

          prodotto = normalizzaDatiProdotto(prodotto);

          prodotto.tipoScheda = 0;
          var id_binari = [
            { _id: "5711f38e5f09a60300e387b0" },
            { _id: "5711f4135f09a60300e387b2" },
            { _id: "5711f4485f09a60300e387b5" },
            { _id: "5711f3f85f09a60300e387b1" },
            { _id: "59cbae665a6aad0400841465" },
            { _id: "59db30769b066b0400cd5fb4" },
            { _id: "59dc8b3439173e040027a22f" },
            { _id: "59db543239173e040027a161" },
            { _id: "59db52f839173e040027a159" },
            { _id: "59db52d139173e040027a158" },
            { _id: "59db4c2b39173e040027a148" },
            { _id: "59db4b4c39173e040027a141" },
            { _id: "59db3c8c9b066b0400cd6005" },
            { _id: "59ccc39650af6204004504a1" },
            { _id: "59ccb0b450af62040045046b" },
            { _id: "59ccaf8a50af62040045045e" }
          ];

          try {
            id_binari.forEach(
              function (idBinario) {
                if (prodotto.modello._id == idBinario._id) {
                  prodotto.tipoScheda = 1;
                  return;
                }
              }
            );
            prodotti.push(prodotto);
          } catch (e) {
            console.log('errore for', e);
          }
       }
    }
   }

     var date = new Date(),
       timestamp = date.valueOf();

    for (let prodotto of prodotti) {
        let data = _.extend(prodotto, { css: css, header: headerSchedaTecnica, footer: footerSchedaTecnica, formatters }),
          html = Mustache.render(tpl[prodotto.tipoScheda], data),
          localPath = './' + prodotto.codice + '.pdf';

        const creaPdf = async function() {
          var pdfOptions = {
            format: 'A4',
            orientation: 'portrait',
            border: '1cm',
            header: { height: "3cm" },
            footer: { height: "1.5cm" },
            timeout: 200000,
            directory: '/Users/archimede/mywork/ilmas_admin/schedeTecniche',
            filename: prodotto.codice + '.pdf'
          };

          let create = util.promisify(pdf.create);
          let creator = await create(html, pdfOptions);
        }

        await creaPdf();
      }
})();
