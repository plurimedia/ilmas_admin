/*
    reindicizza il db prodotti con nuove caratteristiche di ricerca
    lanciare con: 
    node --max-old-space-size=2000 scripts/modificaprodotti_0731.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    Utils = require('../server/routes/utils'),
    aggiornati = 0;

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console
    console.log(db)


    var query =
            { 
                modello: {
                    $in: [
                         '56b8aaa90de23c95600bd257'
                    ]
                },
                generazione:{
                    $in: [
                         'G6'
                    ]
                },
                $where: 'this.codice.length == 8', 
            }

    console.log(query);

    Prodotto.find(query).lean()
    .exec(function(err,prodotti){
        
        console.log('aggiorno '+prodotti.length+' prodotti')
        
        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, 

            function(prodotto,cb){
            
            var cloneP = _.clone(prodotto); 

            if  
                ( cloneP.codice.substring(4,5)=='L' ||
                cloneP.codice.substring(4,5)=='N' ||
                cloneP.codice.substring(4,5)=='G' ||
                cloneP.codice.substring(4,5)=='J'
                ) {

                console.log(aggiornati, cloneP.codice);

                bulk.find( { codice: cloneP.codice } ).remove() 

                aggiornati ++;
            
            }

            cb();
        }, function(err){

            if(err)
                throw err;
            
            if(aggiornati > 0 ) {

                bulk.execute(function(err, result){
                    if(err)
                        throw err;
                    
                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();                
                        
                })                
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();    
            }

        }
    )
    })
});