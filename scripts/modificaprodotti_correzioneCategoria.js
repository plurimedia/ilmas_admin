 /*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/modificaprodotti_correzioneCategoria;
 
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ModelloSchema = require('../server/models/modello'),
    Modello = mongoose.model('Modello'),
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    CategoriaSchema = require('../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    aggiornati = 0;

    prodottiNoModello = 0;
    prodottiNoCategoria = 0;
    prodottiErrati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    Prodotto.find({}).lean()
    .populate('modello')
    .populate('categoria')
    .populate(
            {
            path: 'modello.categoria',
            model: 'Categoria' 
        }
    )
    .exec(function(err,prodotti){

        console.log('aggiorno '+prodotti.length+' prodotti')

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, function(prodotto,cb){
  
                      if (!prodotto.modello){
                            prodottiNoModello ++;
                            console.log("prodotto ", prodotto.codice, " nn ha il modello associato, ", prodottiNoModello)
                        }

                      else if (!prodotto.categoria){
                            prodottiNoCategoria ++;
                            console.log("prodotto ", prodotto.codice, " nn ha la categoria associato, ", prodottiNoCategoria)
                        }
                        else   if ( prodotto.categoria._id.toString() !== prodotto.modello.categoria.toString())
                        {

                            console.log("prodotto ", prodotto.codice, " ha la categoria errata, ", prodottiErrati)

                            var update = {};
                            update.$set = {};
                            update.$set.categoria = prodotto.modello.categoria;
                            bulk.find( { _id: prodotto._id } ).updateOne(update);
                            prodottiErrati ++;
 
                        }
        
                       
            cb();
        }, function(err){

            if(err)
                throw err;

            if(prodottiErrati > 0) { 
                bulk.execute(function(err, result){
                    if(err)
                        throw err;
                    console.log("prodottiNoModello = ", prodottiNoModello);
                    console.log("prodottiNoCategoria = ", prodottiNoCategoria);

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

        })
    }
    )

     
}



);
