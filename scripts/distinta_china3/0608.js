/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/0608.js;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
   
    mapLed = require('./ledAllModel'),
    mapLampada = {
        "2XF":"5a819a44a01985040021f398",   //95002A0
        "2XN":"5a819a57a01985040021f399",   //95002B0
        "3XF":"5a81bb6da01985040021f44b",   //95003A0
        "3XN":"5a81bb7ea01985040021f44c",   //9500BA0
        "2YF":"5a81bba5a01985040021f44f",   //95002D0
        "2YN":"5a81bbd4a01985040021f451",   //95002E0
        "3YF":"5a81bc58a01985040021f45b",   //95003D0
        "3YN":"5a81bc67a01985040021f45c",   //95003E0
        "1FF":"5a82cfabf10c4d0400818178",   //95001M0
        "1FN":"5a82d04ef10c4d0400818181",   //95001N0
        "2FF":"5a82d025f10c4d040081817e",   //95002H0
        "2FN":"5a82d0a1f10c4d0400818185",   //95002K0
        "3FF":"5a82d0d9f10c4d0400818189",   //95003J0
        "3FN":"5a82d0f8f10c4d040081818c",   //95003L0
        "7WF":"5a82d15ef10c4d040081818f",   //01001760545A
        "7WN":"5a82d18cf10c4d0400818193",   //01001760053A
        "8WF":"5a82d1b5f10c4d0400818195",   //01001760522A
        "8WN":"5a82d1dcf10c4d0400818196",   //01001760692A
        "7LF":"5a82d239f10c4d0400818197",   //01001315197A
        "8LF":"5a82d263f10c4d0400818198"    //01001315199A
    }

    aggiornati = 0;
    mongoose.connect(db);

    _getQt = function(p){
    if (p.luci >= 2){
        return p.luci
    }
    else
        return 1;
}

//////
//COMPONENTI STANDARD
//////
mapCorpo1 = {
    "1": "5a816937778395040052137f", // 0608B2
    "2": "5a816937778395040052137f", // 0608B2
    "0": "5a816937778395040052137f", // 0608B2
    "9": "5a8168e2778395040052137c"  // 0608B1
}
mapCorpo2 = {
    "1": "5a81707377839504005213ab", // 0608F1
    "2": "5a81705a77839504005213aa", // 0608F2
    "0": "5a81708377839504005213ac", // 0608F0
    "9": "5a81707377839504005213ab"  // 0608F1
}
mapCorpo3 = {
    "1": "5a816d487783950400521395", // 0608A1
    "2": "5a816d657783950400521397", // 0608A2
    "0": "5a816d7c7783950400521398", // 0608A0
    "9": "5a816d487783950400521395"  // 0608A1
}

mapCorpo4 = "5a8170f777839504005213ad" //RACCBS

mapCorpo5 = {
    "1": "5a816c8d7783950400521391", // 0608BS1
    "2": "5a816ca17783950400521392", // 0608BS2
    "0": "5a816ca17783950400521392", // 0608BS2
    "9": "5a816c8d7783950400521391"  // 0608BS1
}

mapCorpo6 = "5a952cd8b638e50400d8fb1e" //0608SLX


//////
//COMPONENTI MODULO F111
//////
mapCorpo1F111 = {
    "1":"5a82eb5af10c4d04008181b1",   //f111c1
    "9":"5a82eb5af10c4d04008181b1",   //f111c1
    "0":"5a82eca41879400400cbb3f8",   //f111c0
    "2":"5a82ec8b1879400400cbb3f7"   //f111c2
}
mapCorpo2F111 = {
    "1":"5a82edd51879400400cbb3fe",  //F111A1
    "9":"5a82edd51879400400cbb3fe",  //F111A1
    "0":"5a82ee161879400400cbb400",  //F111A0
    "2":"5a82ee021879400400cbb3ff"   //F111A2
}
mapOttica1F111 = {
    "D":"5a82f055cf845a0400031566",   //f111r17
    "H":"5a82f05ecf845a0400031567",   //f111r26
    "N":"5a82f06acf845a0400031568",   //f111r40
    "U":"5a82f06acf845a0400031568"   //f111r40
}
mapOttica2F111 = {
    "D":"5a82ef001879400400cbb402",  //f111v
    "H":"5a82ef001879400400cbb402",  //f111v
    "N":"5a82ef001879400400cbb402",   //f111v
    "U":"5a82ef0c1879400400cbb403"   //f111vs
}

///////
//COMPONENTI MODULO F112
//////
mapCorpo1F112 = {
    "1":"5a8aff8a16137504008b40ac",  //F112C1
    "9":"5a8aff8a16137504008b40ac",  //F112C1
    "0":"5a8affbb16137504008b40ae",  //F112C0
    "2":"5a8affa916137504008b40ad"   //F112C2
}
mapCorpo2F112 = {
    "1":"5a8affec16137504008b40af",  //F112A1
    "9":"5a8affec16137504008b40af",  //F112A1
    "0":"5a8b001516137504008b40b1",  //F112A0
    "2":"5a8b000016137504008b40b0"   //F112A2
}

mapOttica1F112 = {
    "B":"5a8bedccedb6fd040008c3b0", //F112L12
    "F":"5a8bedd8edb6fd040008c3b1",  //f112L24
    "M":"5a8bede5edb6fd040008c3b2",  //f112L38
} 

mongoose.connect(db);
mongoose.connection.once('open', function()
{
    console.log("Connessione aperta, parte il batch.");

    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("56b8aaa90de23c95600bd25a"), // 0608 Bob Std Led
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }

    };
    //   codice:'CRS08G32'

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                var componenti = [];
                

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapCorpo1[(pClone.codice.substring(7,8))])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Corpo',
                        qt: 1,
                        _id: doc._id
                    }
                )

                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapCorpo2[(pClone.codice.substring(7,8))])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Corpo',
                        qt: 1,
                        _id: doc._id
                    }
                )

                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapCorpo3[(pClone.codice.substring(7,8))])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Corpo',
                        qt: 1,
                        _id: doc._id
                    }
                )

                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapCorpo4)
                });
                componenti.push(
                    {
                        tipoAssociazione:'Corpo',
                        qt: 4,
                        _id: doc._id
                    }
                )

                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapCorpo5[(pClone.codice.substring(7,8))])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Corpo',
                        qt: 1,
                        _id: doc._id
                    }
                ) 
                 doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapCorpo6)
                });
                componenti.push(
                    {
                        tipoAssociazione:'Corpo',
                        qt: 1,
                        _id: doc._id
                    }
                )

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //MONTAGGIO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId("5a8171eb77839504005213ae")
                });
                componenti.push(
                    {
                        tipoAssociazione:'Montaggio',
                        qt: 1,
                        _id: doc._id //M0608
                    }
                )

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //IMBALLO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId("5a81847677839504005213d0")
                });
                componenti.push(
                    {
                        tipoAssociazione:'Imballo',
                        qt: 1,
                        _id: doc._id //I0608
                    }
                )

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //F111
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (pClone.moduloled && pClone.moduloled.startsWith('F111'))
                {

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapCorpo1F111[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapCorpo2F111[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapOttica1F111[(pClone.codice.substring(6,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapOttica2F111[(pClone.codice.substring(6,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapLed[(pClone.codice.substring(4,5))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId("5a82ef251879400400cbb404")
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                                qt: 1,
                                _id: doc._id //montaggio F111
                        }
                    )
                }
                else if (pClone.moduloled && pClone.moduloled.startsWith('F112'))
                { 
                  
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapCorpo1F112[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 1,
                            _id: doc._id
                        }
                    )
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapCorpo2F112[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    if (mapOttica1F112[(pClone.codice.substring(6,7))]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapOttica1F112[(pClone.codice.substring(6,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 1,
                            _id: doc._id
                        }
                    )
                    }
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapLed[(pClone.codice.substring(4,5))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 1,
                            _id: doc._id
                        }
                    )
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId("5a8bf3ededb6fd040008c3c4")
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112' ,
                            qt: 1,
                            _id: doc._id //montaggio
                        }
                    )
                    
                }
                else if (mapLampada[(prodotto.codice.substring(4,7))]){
                    
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapLampada[(pClone.codice.substring(4,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Lampada',
                                qt: _getQt(pClone),
                                _id: doc._id
                        }
                    ) 
                }

                if (componenti.length){

                    console.log(aggiornati, pClone.codice)
                                
                    var update = {};
                    update.$set = {};
                    
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_2";
                        
                    bulk.find({_id: pClone._id}).updateOne(update);

                    aggiornati++;
                    
 
                }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
