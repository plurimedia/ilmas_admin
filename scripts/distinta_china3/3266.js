/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/3266.js;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
   
    lampadeDLedOsram = require('./lampadeDLedOsram'),
    lampadeDLedOsramQt=1
 
    lampadeToshiba = require('./lampadeToshiba'),
    lampadeToshibaQt=1

    aggiornati = 0;
    mongoose.connect(db);
     

//////
//COMPONENTI STANDARD
//////
mapCorpo1 = {
    "1": "5a9d23042bfffb04000f1c6f", // 3266C1
    "2": "5a9d23102bfffb04000f1c71", // 3266C2
    "0": "5a9d231d2bfffb04000f1c72" // 3266C0
}
mapCorpo1Qt=1


//////
//COMPONENTI V1 PLUS
//////
mapModuloV1Plus = {
    "ZBG":"5a9d18f52bfffb04000f1c51",   //V501ZBG2
    "ZBV":"5a9d19132bfffb04000f1c52",   //V501ZBV2
    "ZBU":"5a9d2f622bfffb04000f1c9c",   //V501ZBU2
    "7BG":"5a9d192b2bfffb04000f1c53",   //V5017BG2
    "7BV":"5a9d19482bfffb04000f1c54",   //V5017BV2
    "7BU":"5a9d2f702bfffb04000f1c9d",   //V5017BU2
    "8BG":"5a9d19752bfffb04000f1c55",   //V5018BG2
    "8BV":"5a9d198f2bfffb04000f1c57",   //V5018BV2
    "8BU":"5a9d2f7b2bfffb04000f1c9e"   //V5018BU2

}
mapModuloV1PlusQt=1

mapModuloV2 = {
    "1BE":"5a9d31862bfffb04000f1ca9",  //V5021BE2
    "1BN":"5a9d31942bfffb04000f1caa",  //V5021BN2
    "1BU":"5a9d314c2bfffb04000f1ca6",  //V5021BU2
    "2BE":"5a9d31a62bfffb04000f1cab",  //V5022BE2
    "2BN":"5a9d31b12bfffb04000f1cac",  //V5022BN2
    "2BU":"5a9d31592bfffb04000f1ca7",  //V5022BU2
    "3BE":"5a9d31d22bfffb04000f1cad",  //V5023BE2
    "3BN":"5a9d31de2bfffb04000f1cae",  //V5023BN2
    "3BU":"5a9d31662bfffb04000f1ca8"  //V5023BU2
}
mapModuloV2Qt=1

mapDriver = {
    "E":"5a9d1a4c2bfffb04000f1c5c",  //400126122350
    "D":"5a9d1aaa2bfffb04000f1c5e",  //400126122400
    "L":"5a9d1ac62bfffb04000f1c5f"   //400126122403
}
mapDriverQt=1

mongoose.connect(db);

mongoose.connection.once('open', function()
{
        console.log("Connessione aperta, parte il batch."); /// this gets printed to console


    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("5813730d4f8ebc0300f82859"), //3266
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }
    };
     

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                var componenti = [];

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapCorpo1[(pClone.codice.substring(7,8))])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Corpo',
                        qt: mapCorpo1Qt,
                        _id: doc._id
                    }
                ) 

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //V1 PLUS
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                
                if (mapModuloV1Plus[(pClone.codice.substring(4,7))]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapModuloV1Plus[(pClone.codice.substring(4,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo V1 Plus',
                            qt: mapModuloV1PlusQt,
                            _id: doc._id
                        }
                    )
                }
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //V2 PLUS
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                else if (mapModuloV2[(pClone.codice.substring(4,7))]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapModuloV2[(pClone.codice.substring(4,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo V2',
                            qt: mapModuloV2Qt,
                            _id: doc._id
                        }
                    )
                }
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //Lampada Diled Osram
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                else if (lampadeDLedOsram[(pClone.codice.substring(4,7))]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(lampadeDLedOsram[(pClone.codice.substring(4,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Lampada Diled Osram',
                            qt: lampadeDLedOsramQt,
                            _id: doc._id
                        }
                    )
                }
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //Lampada Toshiba Gu10
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                else if (lampadeToshiba[(pClone.codice.substring(4,7))]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(lampadeToshiba[(pClone.codice.substring(4,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Lampada Toshiba Gu10',
                            qt: lampadeToshibaQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //DRIVER
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (pClone.codice.length==9 && mapDriver[(pClone.codice.substring(8,9))] 
                    && (mapModuloV1Plus[(pClone.codice.substring(4,7))] || mapModuloV2[(pClone.codice.substring(4,7))]) ){

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapDriver[(pClone.codice.substring(8,9))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Driver',
                            qt:mapDriverQt,
                            _id: doc._id
                        }
                    )

                }

                if (componenti.length){

                    console.log(aggiornati, pClone.codice)
                                
                    var update = {};
                    update.$set = {};
                    
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_2";
                        
                    bulk.find({_id: pClone._id}).updateOne(update);

                    aggiornati++;
                    
 
                }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
