/*
LANCIARE LA PROCEDURA
node --max-old-space-size=20000 scripts/modificaprodotti_keywords;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ModelloSchema = require('../server/models/modello'),
    CategoriaSchema = require('../server/models/categoria'),
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    Utils = require('../server/routes/utils'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    Prodotto.find({}).lean()
    .populate('modello')
    
    .exec(function(err,prodotti){

        //console.log('aggiorno '+prodotti.length+' prodotti')

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, function(prodotto,cb){

            var update = {}; 

            console.log(aggiornati, "  - aggiornamento prodotto id = " , prodotto.codice);
            
            if (prodotto.codice){
                var p = _.clone(prodotto); 
                 

                update.$set = {};
                update.$set.keywords =   Utils.keyWords(p);
                //update.$set.pubblicato=false;
                bulk.find( { _id: prodotto._id } ).updateOne(update);
                aggiornati ++; 
               
            }
            cb();
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0) {
             
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
               
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

        })
    })

      
}



);
