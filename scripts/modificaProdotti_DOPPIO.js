/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/modificaProdotti_DOPPIO.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console
    var query =
    {
        codice:{
            $in: [
                /-DOPPIO$/,/-FRUIT$/,/-FISH$/
            ]
        } 
    }
    Prodotto.find(query).lean()
    .exec(function(err,prodotti){

        console.log('aggiorno '+prodotti.length+' prodotti')

        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        async.each(prodotti, 
            function(prodotto,cb)
            {
            var update = {};

            console.log(aggiornati, prodotto.codice);

            if (prodotto.codice)
            { 
                bulk.find( { _id: prodotto._id } ).remove();
                aggiornati ++; 
            }
            cb();
            },
             function(err)
            {

            if(err)
                throw err;

            if(aggiornati > 0 ) {
             
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: eliminati '+aggiornati+' prodotti')
                   
                    mongoose.connection.close();
                })
               
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

        })
    }) 
      
}



);
