/*
LANCIARE LA PROCEDURA
node --max-old-space-size=5000 scripts/installazioni/installazioniUpdateRelated.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    InstallazioneSchema = require('../../server/models/installazione'),
    Installazione = mongoose.model('Installazione'),
    aggiornati = 0;

     var pathInstallazioni = 'scripts/installazioni/installazioni.json';
     var installazioniAll = JSON.parse(fs.readFileSync(pathInstallazioni, 'utf8'));
    var ListaRecordDaSalvareInDB = []
    var aggiornati=0
    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console
    var bulk = Installazione.collection.initializeOrderedBulkOp();

    
    async.each(installazioniAll,  
        
        function(element, cb) {

            if (element.related){

                let arrayInstallazioniCollegate = []

                async.each(element.related,  
                    function(relazioneSingle, cb2) {

                      //  console.log("yyyyyyyy", relazioneSingle)

                        let nomeInstallazioneCollegata = relazioneSingle.name;
                        console.log("yyyyyyyy", nomeInstallazioneCollegata)

                        Installazione.findOne({name:nomeInstallazioneCollegata}).lean()
                        .exec(
                            function(err, installaz){
                                if (!err){
                                    console.log("trovata in DB", installaz.name)

                                    arrayInstallazioniCollegate.push(installaz._id)
                                }
                                cb2()
                            }
                        )
                    },
                    function(err){
                        console.log(element)
                        var update = {};
                        update.$set = {};
                        
                        update.$set.progetti_simili = arrayInstallazioniCollegate;
                        console.log("arrayInstallazioniCollegate ", arrayInstallazioniCollegate)
                        bulk.find({name: element.name}).updateOne(update);

                        aggiornati++;
                        console.log("aggiornati ", aggiornati)

                        cb()
                    })
            }
           
      },
       function(err){

        if(err)
            throw err;

            if(aggiornati > 0) {
         
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
 
            
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

    });
 
      
});
