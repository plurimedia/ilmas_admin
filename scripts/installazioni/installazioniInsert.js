/*
LANCIARE LA PROCEDURA
node --max-old-space-size=5000 scripts/installazioni/installazioniInsert.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    InstallazioneSchema = require('../../server/models/installazione'),
    Installazione = mongoose.model('Installazione'),
    aggiornati = 0;

     var pathInstallazioni = 'scripts/installazioni/installazioni.json';
     var installazioniAll = JSON.parse(fs.readFileSync(pathInstallazioni, 'utf8'));
    var ListaRecordDaSalvareInDB = []
    var aggiornati=0
    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console
    var bulk = Installazione.collection.initializeOrderedBulkOp();

    
    async.each(installazioniAll,  
        
        function(element, cb) {

            let fotoDaSalvare = []
            let annoFile = element.date.substring(0,4)
            let fotoListaFile = element.images

            //cerco la foto principale in quelle piccole
            let trovataFotoPrincipaleInQuellePiccole = _.find(fotoListaFile, function (obj) { 
                return obj.small == element.banner; 
            })

            //se nn la trovo allora la inserisco
            if (!trovataFotoPrincipaleInQuellePiccole){
                fotoListaFile.push({'small':element.banner})
            }

            fotoListaFile.forEach(function(foto){
                let tipoFoto='lista'
                if (foto.small==element.banner){
                    tipoFoto='principale'
                }
                
                fotoDaSalvare.push(
                    {
                        tipo:tipoFoto,
                        foto:foto.small
                    }
                )
            }) 

            var recordNew = {
                descrizione_it: element.title + ' ' + element.client.name,
                descrizione_en: element.title + ' ' + element.client.name,
                anno: annoFile,
                prodotti_it: element.prodottonome,
                prodotti_en: element.prodottonome,
                foto: fotoDaSalvare,
                pubblicato: true,
                name:element.name
            }
            ListaRecordDaSalvareInDB.push(recordNew)

            console.log(recordNew)

            bulk.insert(recordNew)
            aggiornati++
            cb()
      },
       function(err){

        if(err)
            throw err;

            if(aggiornati > 0) {
            
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
            
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

    });

      console.log("Chiudo connessione")
      mongoose.connection.close();
      
});
