/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/0596.js;
*/


//CORPO/OTTICA/MODULO LED (SE C'è)/LED/ADATTATORE(SE C'è)/DRIVER/MONTAGGIO/IMBALLO(SE C'è)

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),

    aggiornati = 0;
    mongoose.connect(db);
 
    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),

//////
//CORPO
//////
mapCorpo = [
    "5abcd945840de104005bc854", // 0596XXX1
    "5abcd9bb840de104005bc85d" //0596R
]

//////
//OTTICA
//////
mapOttica = {
    "E" :"5abcda43840de104005bc864",  //0596L20
    "K" :"5abcda4d840de104005bc865",  //0596L30
    "N" :"5abcda57840de104005bc866"   //0596L40
}
mapOtticaQt=1

//////
//LED
//////
mapLed = {
    "5":"5915b5ba5c28050400609f67",
    "6":"5915b60c5c28050400609f6b",
    "7":"5915b64d5c28050400609f6e",
    "R":"592eb14fb480230400431447",
    "Z":"592eb17db480230400431448",
    "8":"5915b68a5c28050400609f70",
    "Q":"5819c0e4d5718e690d56a92d",
    "W":"592ed840b48023040043146d"
},
mapLedQt=1;

///////
//DRIVER
//////
mapDriver = {
    "9_E":"5abcdc35840de104005bc873", //400126127484
    "9_D":"5aafa093a30ad1040093ae01", //400126127490
    "9_L":"5aafa093a30ad1040093ae01"  //400126127490
} 
driverQt=1



 



mongoose.connection.once('open', function()
{
    console.log("Connessione aperta, parte il batch.");

    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("59aff297d9bffb040079b4e4"), // 0596
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        } // codice:'18887LF1E'

    };

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
            var pClone = _.clone(prodotto)
            pClone.componenti = [];
            var componenti = [];

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //CORPO
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                mapCorpo.forEach(function(element){
                
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(element)
                });
                componenti.push(
                    {
                        tipoAssociazione:'Corpo',
                        qt: mapCorpo,
                        _id: doc._id
                    }
                )
            })
          
           

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //OTTICA
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
 
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(mapOttica[pClone.codice.substring(6,7)])
            });
            componenti.push(
                {
                    tipoAssociazione:'Ottica',
                    qt: mapOtticaQt,
                    _id: doc._id
                }
            )
 
             

             //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //LED
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            if (mapLed[pClone.codice.substring(4,5)]){
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapLed[pClone.codice.substring(4,5)])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Led',
                        qt: mapLedQt,
                        _id: doc._id
                    }
                )
            }
            

             //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //DRIVER
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            var chiave = pClone.codice.length + "_" + pClone.codice.substring(8,9);
            driver = mapDriver[chiave]
            if (driver){
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapDriver[chiave])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Driver',
                            qt: driverQt,
                            _id: doc._id
                    }
                ) 
            }
            

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //MONTAGGIO
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Montaggio',
                    qt: 1,
                    _id: doc._id 
                }
            )

               

            if (componenti.length){

                console.log(aggiornati, pClone.codice)
                            
                var update = {};
                update.$set = {};
                
                update.$set.componenti = componenti;
                update.$set.distintabase = "china_2";
                    
                bulk.find({_id: pClone._id}).updateOne(update);

                aggiornati++;
                

            }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
