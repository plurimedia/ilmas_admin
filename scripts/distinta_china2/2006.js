/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/2006.js;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
   
    mapLed = require('./led2006Model'),
    mapLedQt=1,

    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),
     

    aggiornati = 0;
    mongoose.connect(db);

    

//////
//CORPO
//////
mapCorpo = {
    "9":
        [
            "5a9fc281fdceec04005ad6b1", //KLAI2
            "5a9fc474fdceec04005ad6c0", //KLAB2
            "5a9fcc12fdceec04005ad6ea", //KLSNO2
            "5a9fcf25fdceec04005ad6f7", //KLCWTP
            "5a9fc48cfdceec04005ad6c1", //KLDIS
            "5a9fc8eefdceec04005ad6d3", //KLPS2 
            "5a9e80f0a29ce004003c3f4e", ///KDISTA2
            "5a9e82f7a29ce004003c3f59",// KNOT2
            "5a9e969bfdceec04005ad592", //KRACCFIL
        ],
        "10":
        [
            "5a9fc281fdceec04005ad6b1", //KLAI2
            "5a9fc474fdceec04005ad6c0", //KLAB2
            "5a9fcc12fdceec04005ad6ea", //KLSNO2
            "5aa7e56a143eae0400f99bae", //KLCGALV
            "5a9fc48cfdceec04005ad6c1", //KLDIS
            "5a9fc8eefdceec04005ad6d3", //KLPS2 
            "5a9e80f0a29ce004003c3f4e", //KDISTA2
            "5a9e82f7a29ce004003c3f59", //KNOT2
            "5a9e969bfdceec04005ad592" //KRACCFIL
        ]
}
mapCorpoQt=1

mapCorpo11 = {
    "9016": [
        "5a9fc273fdceec04005ad6b0", //KLAI1
        "5a9fc46afdceec04005ad6bf", // KLAB1
        "5a9fcbfefdceec04005ad6e9", //  KLSNO1
        "5a9fd015fdceec04005ad6f8", // KLCRAL
        "5a9fc48cfdceec04005ad6c1", // KLDIS
        "5a9fc8c6fdceec04005ad6d2", // KLPS1
        "5a9e80a5a29ce004003c3f4a", // KDISTA1
        "5a9e82e4a29ce004003c3f58", // KNOT1
        "5a9e969bfdceec04005ad592" // KRACCFIL
    ],
    "9006": [
        "5a9fc295fdceec04005ad6b2", //KLAI0
        "5a9fc45efdceec04005ad6be", // KLAB0
        "5a9fcc2efdceec04005ad6eb", //  KLSNO0
        "5a9fd015fdceec04005ad6f8", // KLCRAL
        "5a9fc48cfdceec04005ad6c1", // KLDIS
        "5a9fc90bfdceec04005ad6d4", // KLPS0
        "5a9e812ba29ce004003c3f50", // KDISTA0
        "5a9e8313a29ce004003c3f5a", // KNOT0
        "5a9e969bfdceec04005ad592", // KRACCFIL
    ],
    "9005": [
        "5a9fc281fdceec04005ad6b1", //KLAI2
        "5a9fc474fdceec04005ad6c0", // KLAB2
        "5a9fcc12fdceec04005ad6ea", //  KLSNO2
        "5a9fd015fdceec04005ad6f8", // KLCRAL
        "5a9fc48cfdceec04005ad6c1", // KLDIS
        "5a9fc8eefdceec04005ad6d3", // KLPS2
        "5a9e80f0a29ce004003c3f4e", // KDISTA2
        "5a9e82f7a29ce004003c3f59", // KNOT2
        "5a9e969bfdceec04005ad592", // KRACCFIL
    ],
    "other": [
        "5a9fc281fdceec04005ad6b1", //KLAI2
        "5a9fc474fdceec04005ad6c0", //KLAB2  
        "5a9fcc12fdceec04005ad6ea", //KLSNO2
        "5a9fd015fdceec04005ad6f8", //KLCRAL  
        "5a9fc48cfdceec04005ad6c1", //KLDIS  
        "5a9fc8eefdceec04005ad6d3", //KLPS2  
        "5a9e80f0a29ce004003c3f4e", //KDISTA2  
        "5a9e82f7a29ce004003c3f59", //KNOT2  
        "5a9e969bfdceec04005ad592", //KRACCFIL  
    ]
}

mapCorpo11Qt=1

mapCorpo8 = {
    "1": [
        "5a9fcdb6fdceec04005ad6f1", //KLCD1
        "5a9fc46afdceec04005ad6bf", // KLAB1
        "5a9fc273fdceec04005ad6b0", //  KLAI1
        "5a9e80a5a29ce004003c3f4a", // KDISTA1
        "5a9fcbfefdceec04005ad6e9", // KLSNO1
        "5a9fc8c6fdceec04005ad6d2", // KLPS1
        "5a9e82e4a29ce004003c3f58", // KNOT1
        "5a9e969bfdceec04005ad592", // KRACCFIL
    ],
    "2": [
        "5a9fcdccfdceec04005ad6f2", //KLCD2
        "5a9fc474fdceec04005ad6c0", // KLAB2
        "5a9fc281fdceec04005ad6b1", //  KLAI2
        "5a9e80f0a29ce004003c3f4e", // KDISTA2
        "5a9fcc12fdceec04005ad6ea", // KLSNO2
        "5a9fc8eefdceec04005ad6d3", // KLPS2
        "5a9e82f7a29ce004003c3f59", // KNOT2
        "5a9e969bfdceec04005ad592", // KRACCFIL
    ],
    "0": [
        "5a9fcdddfdceec04005ad6f3", //KLCD0
        "5a9fc45efdceec04005ad6be", // KLAB0
        "5a9fc295fdceec04005ad6b2", //  KLAI0
        "5a9e812ba29ce004003c3f50", // KDISTA0
        "5a9ea91ffdceec04005ad5bd", // KSSNO0
        "5a9fc90bfdceec04005ad6d4", // KLPS0
        "5a9e8313a29ce004003c3f5a", // KNOT0
        "5a9e969bfdceec04005ad592", // KRACCFIL
    ]
}
mapCorpo8Qt=1

//////
//Ottica
//////
mapOttica = {
    "B":"5a9fc2e8fdceec04005ad6b3",  //KLL12
    "M":"5a9fc302fdceec04005ad6b6",  //KLL38
    "F":"5a9fc2f6fdceec04005ad6b5",   //KLL24
    "T":"5a9fc31bfdceec04005ad6b7" //KLR60
}

mapOtticaQt=1

//////
//Adattatore Global
//////
mapAdattatore9_10 = {
    "C":"5aaa7de9a97d4f04009548c4",   //400126127475
    "D":"5a9fd475fdceec04005ad707",   //400126127473DI
    "S":"5a9fd475fdceec04005ad707"   //400126127473DI
} 
mapAdattatore9_10Qt=1

mapAdattatore11 = {
    "9016_C": "5aaa7e80fc42390400dbcb25", //400126127474
    "9016_D": "5a9fd4cdfdceec04005ad708", //400126127471DI
    "9016_S": "5a9fd4cdfdceec04005ad708", //400126127471DI

    "9006_C": "5aaa7ea6fc42390400dbcb26", //400126127476
    "9006_D": "5a9fd4eafdceec04005ad709", //400126127472DI
    "9006_S": "5a9fd4eafdceec04005ad709", //400126127472DI

    "other_C":"5aaa7de9a97d4f04009548c4",  //400126127475
    "other_D":"5a9fd475fdceec04005ad707",  //400126127473DI
    "other_S":"5a9fd475fdceec04005ad707",  //400126127473DI
}
mapAdattatore11Qt=1

mapAdattatore8 = {
    "1_C": "5aaa7e80fc42390400dbcb25", //400126127474 
    "1_D": "5a9fd4cdfdceec04005ad708", //400126127471DI 
    "1_S": "5a9fd4cdfdceec04005ad708", //400126127471DI 
    "2_C": "5a9fd475fdceec04005ad707", //400126127473DI 
    "2_D": "5a9fd475fdceec04005ad707", //400126127473DI 
    "2_S": "5a9fd475fdceec04005ad707", //400126127473DI 

    "0_C": "5aaa7ea6fc42390400dbcb26",   //400126127476
    "0_D": "5a9fd4eafdceec04005ad709",   //400126127472DI
    "0_S": "5a9fd4eafdceec04005ad709"   //400126127472DI
}
mapAdattatore8Qt=1


mongoose.connection.once('open', function()
{
    console.log("Connessione aperta, parte il batch.");

    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("5821d5261164470300324b5c"), // 2006
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }

    };

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                var componenti = [];
                
                if ( pClone.codice.length==9 || pClone.codice.length==10 || pClone.codice.length==11){

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

                if (pClone.codice.length==9 || pClone.codice.length==10){
                    var corpi=mapCorpo[(pClone.codice.length)];
                    if (corpi && corpi.length){
                        corpi.forEach(function(element){

                            doc = new Componente({
                                _id: new mongoose.Types.ObjectId(element)
                            });
                            componenti.push(
                                {
                                    tipoAssociazione:'Corpo',
                                    qt: mapCorpoQt,
                                    _id: doc._id
                                }
                            )
                        })
                    }
                }
                else if (pClone.codice.length==11){
                    var corpi=mapCorpo11[(pClone.codice.substring(7,11))];

                    if (!corpi || !corpi.length){
                        corpi=mapCorpo11[("other")];
                    }

                    corpi.forEach(function(element){

                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: mapCorpo11Qt,
                                _id: doc._id
                            }
                        )
                    })
                }  
                 
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //ADATTATORE
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

                if (pClone.codice.length==9 || pClone.codice.length==10){

                    var adattatore9_10=_.clone(mapAdattatore9_10[(pClone.codice.substring(5,6))]);

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(adattatore9_10)
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Adattatore global',
                            qt: mapAdattatore9_10Qt,
                            _id: doc._id
                        }
                    )
                }
                else if (pClone.codice.length==11){

                    var adattatore11=mapAdattatore11[(pClone.codice.substring(7,11)+'_'+pClone.codice.substring(5,6))];

                    if (!adattatore11 || !adattatore11.length){
                        adattatore11=mapAdattatore11[("other"+'_'+pClone.codice.substring(5,6))];
                    }

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(adattatore11)
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Adattatore',
                            qt: mapAdattatore11Qt,
                            _id: doc._id
                        }
                    )
                }
            }
            else if ( pClone.codice.length==8){

                 //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

                var corpi=mapCorpo8[(pClone.codice.substring(7,8))];

                corpi.forEach(function(element){

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(element)
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Corpo',
                            qt: mapCorpo8Qt,
                            _id: doc._id
                        }
                    )
                })


                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //ADATTATORE
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 

                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapAdattatore8[ pClone.codice.substring(7,8)+"_"+pClone.codice.substring(5,6)])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Adattatore global',
                        qt: mapAdattatore8Qt,
                        _id: doc._id
                    }
                )
            }
             

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //OTTICA
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

            doc = new Componente({
                _id: new mongoose.Types.ObjectId(mapOttica[(pClone.codice.substring(6,7))])
            });
            componenti.push(
                {
                    tipoAssociazione:'Ottica',
                    qt: mapOtticaQt,
                    _id: doc._id
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //LED
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

            doc = new Componente({
                _id: new mongoose.Types.ObjectId(mapLed[(pClone.codice.substring(4,5))])
            });
            componenti.push(
                {
                    tipoAssociazione:'Led',
                    qt: mapLedQt,
                    _id: doc._id
                }
            )


            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //MONTAGGIO
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Montaggio',
                    qt: 1,
                    _id: doc._id //M0608
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //CARTONE
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['cartone'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Imballo',
                    qt: 1,
                    _id: doc._id
                }
            ) 

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //sacchetto
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Imballo',
                    qt: 1,
                    _id: doc._id
                }
            )
                 

            if (componenti.length){

                console.log(aggiornati, pClone.codice)
                            
                var update = {};
                update.$set = {};
                
                update.$set.componenti = componenti;
                update.$set.distintabase = "china_2";
                    
                bulk.find({_id: pClone._id}).updateOne(update);

                aggiornati++;
                

            }

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
