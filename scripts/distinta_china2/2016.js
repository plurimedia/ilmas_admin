/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/2016.js;
*/
//CORPO/OTTICA/MODULO LED (SE C'è)/LED/ADATTATORE(SE C'è)/DRIVER/MONTAGGIO/IMBALLO(SE C'è)

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
       
    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),

    aggiornati = 0;
    mongoose.connect(db); 

//////
//COMPONENTI STANDARD
//////
mapCorpo =  {
    "11_9016":[
        {
            "id":"5af30ab869a7f80400329a09",//KSPLUSC1
            "qt":"1"
        },  
        {
            "id":"5a9e78e585347d04003f233f",//KSDIS
            "qt":"1"
        },  
        {
            "id":"5a9e786e85347d04003f2339",//KSAB1
            "qt":"1"
        },
        {
            "id":"5a9e78aa85347d04003f233c",//KSAI1
            "qt":"1"
        },  
        {
            "id":"5af0440aec11ff040073a4fd",//KDISTP1
            "qt":"1"
        },  
        {
            "id":"5a9ea8fdfdceec04005ad5bb",//KSSNO1
            "qt":"1"
        },
        {
            "id":"5a9ea87ffdceec04005ad5ba",//KSPS1
            "qt":"1"
        },  
        {
            "id":"5a9e82e4a29ce004003c3f58",//KNOT1
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//KMINUTERIA
            "qt":"1"
        }
    ],
    "11_9006":[
        {
            "id":"5af30ae569a7f80400329a0b",//KSPLUSC0
            "qt":"1"
        },  
        {
            "id":"5a9e78e585347d04003f233f",//KSDIS
            "qt":"1"
        },  
        {
            "id":"5a9e788485347d04003f233b",//KSAB0
            "qt":"1"
        },
        {
            "id":"5a9e78be85347d04003f233e",//KSAI0
            "qt":"1"
        },  
        {
            "id":"5af311a069a7f80400329a27",//KDISTP0
            "qt":"1"
        },  
        {
            "id":"5a9ea91ffdceec04005ad5bd",//KSSNO0
            "qt":"1"
        },
        {
            "id":"5a9eaa0cfdceec04005ad5cb",//KSPS0
            "qt":"1"
        },  
        {
            "id":"5a9e8313a29ce004003c3f5a",//KNOT0
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFILL
            "qt":"1"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"1"
        } 
    ],
    "11_9005":[
        {
            "id":"5af30ac869a7f80400329a0a",//KSPLUSC2
            "qt":"1"
        },  
        {
            "id":"5a9e78e585347d04003f233f",//KSDIS
            "qt":"1"
        },  
        {
            "id":"5a9e787985347d04003f233a",//KSAB2
            "qt":"1"
        },
        {
            "id":"5a9e78b485347d04003f233d",//KSAI2
            "qt":"1"
        },  
        {
            "id":"5af3116a69a7f80400329a25",//KDISTP2
            "qt":"1"
        },  
        {
            "id":"5a9ea90dfdceec04005ad5bc",//KSSNO2
            "qt":"1"
        },
        {
            "id":"5a9fa843fdceec04005ad688",//KSPS2
            "qt":"1"
        },  
        {
            "id":"5a9e82f7a29ce004003c3f59",//KNOT2
            "qt":"1"
        }
        ,  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFILL
            "qt":"1"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"1"
        } 
    ],
    "11_other":[
        {
            "id":"5af30b1069a7f80400329a0c",//KSPLUSCRAL
            "qt":"1"
        },  
        {
            "id":"5a9e78e585347d04003f233f",//KSDIS
            "qt":"1"
        },  
        {
            "id":"5a9e787985347d04003f233a",//KSAB2
            "qt":"1"
        },
        {
            "id":"5a9e78aa85347d04003f233c",//KSAI1
            "qt":"1"
        },  
        {
            "id":"5af3116a69a7f80400329a25",//KDISTP2
            "qt":"1"
        },  
        {
            "id":"5a9ea90dfdceec04005ad5bc",//KSSNO2
            "qt":"1"
        },
        {
            "id":"5a9fa843fdceec04005ad688",//KSPS2
            "qt":"1"
        },  
        {
            "id":"5a9e82f7a29ce004003c3f59",//KNOT2
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"1"
        } 
    ],
    "10":[
        {
            "id":"5af30b6369a7f80400329a0d",//KSPLUSCGALV
            "qt":"1"
        },  
        {
            "id":"5a9e78e585347d04003f233f",//KSDIS
            "qt":"1"
        },  
        {
            "id":"5a9e787985347d04003f233a",//KSAB2
            "qt":"1"
        },
        {
            "id":"5a9e78aa85347d04003f233c",//KSAI1
            "qt":"1"
        },  
        {
            "id":"5af3116a69a7f80400329a25",//KDISTP2
            "qt":"1"
        },  
        {
            "id":"5a9ea90dfdceec04005ad5bc",//KSSNO2
            "qt":"1"
        },
        {
            "id":"5a9fa843fdceec04005ad688",//KSPS2
            "qt":"1"
        },  
        {
            "id":"5a9e82f7a29ce004003c3f59",//KNOT2
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"1"
        } 
    ]
}

mapOttica = {
    "M":"5a9e6a0860218204000d684d", //KSL38
    "F":"5a9e69fd60218204000d684c", //KSL24
    "T":"5a9e6a2060218204000d684e"  //KSR60
 }
 mapOtticaQt=1

 mapLed={
     "5":"5a9e9f3afdceec04005ad5a1",
     "6":"5a9e9f5dfdceec04005ad5a2",
     "7":"5a9e9fb0fdceec04005ad5a5",
     "R":"5a9e9f05fdceec04005ad5a0",
     "Z":"5a9e9f86fdceec04005ad5a3",
     "8":"5a9e9fd3fdceec04005ad5a7",
     "Q":"5819c0e4d5718e690d56a92d",
     "W":"592ed840b48023040043146d"
 }
mapLedQt=1
 
mapAdattatore = {
    "12_9016":"5abb423eb4609a0400b9f663",   //9605A111  
    "other":"5abb424ab4609a0400b9f664"  //9605A112
   
 }
 mapAdattatoreQt=1

 mapDriver =  "5ab8f0fe4798ca0400a4897b",   //400126127486  
    
 mapDriverQt=1

mongoose.connection.once('open', function()
{
        console.log("Connessione aperta, parte il batch."); /// this gets printed to console


    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("58734cf8043a1d040098d694"), //	2016
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }
        //,codice:'20118CT9006D'
    };
     

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                pClone.componenti = [];
                
                var componenti = [];

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var corpi = [], chiave="";

                chiave = pClone.codice.length;
                if (mapCorpo[chiave]==null){
                    chiave = "11_" + pClone.codice.substring(7,11)
                }

                if (mapCorpo[chiave]==null){
                    chiave = "11_other"
                } 

                corpi=_.clone(mapCorpo[chiave]);
                
                if (corpi){
                    corpi.forEach(function(element){
                        
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element.id)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: element.qt,
                                _id: doc._id
                            }
                        )
                    })
                } 

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //OTTICA
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (mapOttica[pClone.codice.substring(6,7)]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapOttica[pClone.codice.substring(6,7)])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Ottica',
                            qt: mapLedQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //LED
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (mapLed[pClone.codice.substring(4,5)]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapLed[pClone.codice.substring(4,5)])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Led',
                            qt: mapLedQt,
                            _id: doc._id
                        }
                    )
                }

                 //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //ADATTATORE
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var chiave=chiave="12_"+pClone.codice.substring(11,12) 
                if (!mapAdattatore[chiave]){
                   chiave="other" 
                }
                if (mapAdattatore[chiave]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapAdattatore[chiave])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Adattatore',
                            mapDriverQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //DRIVER
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
             
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapDriver)
                });
                componenti.push(
                    {
                        tipoAssociazione:'Driver',
                        mapDriverQt,
                        _id: doc._id
                    }
                )

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //MONTAGGIO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Montaggio',
                        qt: 1,
                        _id: doc._id
                    }
                )

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //IMBALLO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['cartone'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'imballo',
                        qt: 1,
                        _id: doc._id
                    }
                )
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'imballo',
                        qt: 1,
                        _id: doc._id
                    }
                )

                if (componenti.length){

                    console.log(aggiornati, pClone.codice)
                                
                    var update = {};
                    update.$set = {};
                    
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_2";
                        
                    bulk.find({_id: pClone._id}).updateOne(update);

                    aggiornati++;
 
                }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
