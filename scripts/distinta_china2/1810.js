/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/1810.js;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
   
    mapLed = require('./ledAllModel'),
    mapLampada = {
        "2XF":"5a819a44a01985040021f398",   //95002A0
        "2XN":"5a819a57a01985040021f399",   //95002B0
        "3XF":"5a81bb6da01985040021f44b",   //95003A0
        "3XN":"5a81bb7ea01985040021f44c",   //9500BA0
        "2YF":"5a81bba5a01985040021f44f",   //95002D0
        "2YN":"5a81bbd4a01985040021f451",   //95002E0
        "3YF":"5a81bc58a01985040021f45b",   //95003D0
        "3YN":"5a81bc67a01985040021f45c",   //95003E0
        "1FF":"5a82cfabf10c4d0400818178",   //95001M0
        "1FN":"5a82d04ef10c4d0400818181",   //95001N0
        "2FF":"5a82d025f10c4d040081817e",   //95002H0
        "2FN":"5a82d0a1f10c4d0400818185",   //95002K0
        "3FF":"5a82d0d9f10c4d0400818189",   //95003J0
        "3FN":"5a82d0f8f10c4d040081818c",   //95003L0
        "7WF":"5a82d15ef10c4d040081818f",   //01001760545A
        "7WN":"5a82d18cf10c4d0400818193",   //01001760053A
        "8WF":"5a82d1b5f10c4d0400818195",   //01001760522A
        "8WN":"5a82d1dcf10c4d0400818196",   //01001760692A
        "7LF":"5a82d239f10c4d0400818197",   //01001315197A
        "8LF":"5a82d263f10c4d0400818198"    //01001315199A
    },
    mapLampadaQt=3

    aggiornati = 0;
    mongoose.connect(db);

     

//////
//COMPONENTI STANDARD
//////
mapCorpo = {
    "1": "5aaf9dfca30ad1040093adf2", // 1810XXX1
    "0": "5aaf9e09a30ad1040093adf3" //1810XXX0
}
  
///////
//DRIVER
//////
mapDriver = {
    "9" : "5aafc476a30ad1040093ae6c" //9000E09 
}
driverQt=3

mongoose.connection.once('open', function()
{
    console.log("Connessione aperta, parte il batch.");

    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("56b8aaa90de23c95600bd278"), // 1810
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }

    };
    //   codice:'CRS08G32'

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
            var pClone = _.clone(prodotto)
            var componenti = [];
            

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //CORPO
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            if (mapCorpo[(pClone.codice.substring(7,8))]){
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapCorpo[(pClone.codice.substring(7,8))])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Corpo',
                        qt: 1,
                        _id: doc._id
                    }
                )
            }

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //DRIVER
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            
            var driver = mapDriver[pClone.codice.length]
            if (driver){
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(driver)
                });
                componenti.push(
                    {
                        tipoAssociazione:'Driver',
                            qt: driverQt,
                            _id: doc._id
                    }
                ) 
            }

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //Lampada
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            if (mapLampada[(pClone.codice.substring(4,7))]){
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapLampada[(pClone.codice.substring(4,7))])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Lampada',
                            qt: mapLampadaQt,
                            _id: doc._id
                    }
                ) 
            }

            var cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel');

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //MONTAGGIO
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Montaggio',
                    qt: 1,
                    _id: doc._id 
                }
            )

               

            if (componenti.length){

                console.log(aggiornati, pClone.codice)
                            
                var update = {};
                update.$set = {};
                
                update.$set.componenti = componenti;
                update.$set.distintabase = "china_2";
                    
                bulk.find({_id: pClone._id}).updateOne(update);

                aggiornati++;
                

            }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
