/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/F112.js;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),

      cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),

    mapLed = require('./ledAllModel'),
     

    aggiornati = 0;

mongoose.connect(db);

mapCorpo1 = {
    "1":"5a8aff8a16137504008b40ac",  //F112C1
    "0":"5a8affbb16137504008b40ae",  //F112C0
    "2":"5a8affa916137504008b40ad"   //F112C2
}
mapCorpo2 = {
    "1":"5a8affec16137504008b40af",  //F112A1
    "0":"5a8b001516137504008b40b1",  //F112A0
    "2":"5a8b000016137504008b40b0"   //F112A2
}
mapOttica1 = {
    "B":"5a8bedccedb6fd040008c3b0", //F112L12
    "F":"5a8bedd8edb6fd040008c3b1",  //f112L24
    "M":"5a8bede5edb6fd040008c3b2",  //f112L38
}

mongoose.connection.once('open', function()
{
    console.log("Connessione aperta, parte il batch.");

    var query = { generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: { $in: [
            ObjectId("56b8aaa90de23c95600bd226") //F112
        ] },
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        },
        codice:'F112QEM0D'
    };

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
            if (prodotto.codice)
            {
               

                var componenti = [];
                var pClone = _.clone(prodotto);

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapCorpo1[(pClone.codice.substring(7,8))])
                });

                componenti.push(
                    {
                        tipoAssociazione:'Corpo',
                        qt: 1,
                        _id: doc._id
                    }
                )

                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapCorpo2[(pClone.codice.substring(7,8))])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Corpo',
                        qt: 1,
                        _id: doc._id
                    }
                )
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //OTTICA
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (mapOttica1[(pClone.codice.substring(6,7))]){
                    componenti.push(
                        {
                            tipoAssociazione:'Ottica',
                            qt: 1,
                            _id: new mongoose.Types.ObjectId(mapOttica1[(pClone.codice.substring(6,7))])
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //LED
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapLed[(pClone.codice.substring(4,5))])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Led',
                        qt: 1,
                        _id: doc._id
                    }
                )

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //MONTAGGIO
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Montaggio',
                    qt: 1,
                    _id: doc._id
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //CARTONE
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['cartone'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Imballo',
                    qt: 1,
                    _id: doc._id
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //sacchetto
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Imballo',
                    qt: 1,
                    _id: doc._id
                }
            )

                aggiornati++;

                console.log("componenti ", pClone.codice, componenti)

                var update = {};
                update.$set = {};
                update.$set.componenti = componenti;
                update.$set.distintabase = "china_2";
                bulk.find( { _id: pClone._id } ).updateOne(update);
            }

            cb();
        }, function(err)
        {
            if (err) {
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(err, result)
                {
                    if (err) {
                        console.log(err)
                    throw err;}

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })
    })
});
