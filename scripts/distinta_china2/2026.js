/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/2026.js;
*/
//CORPO/OTTICA/MODULO LED (SE C'è)/LED/ADATTATORE(SE C'è)/DRIVER/MONTAGGIO/IMBALLO(SE C'è)

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
       
    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),

    aggiornati = 0;
    mongoose.connect(db); 

//////
//COMPONENTI STANDARD
//////
mapCorpo =  {
    "11_12_9016":[
        {
            "id":"5a9fc46afdceec04005ad6bf",//KLAB1
            "qt":"1"
        },  
        {
            "id":"5a9fc273fdceec04005ad6b0",//KLAI1
            "qt":"1"
        },  
        {
            "id":"5a9fc48cfdceec04005ad6c1",//KLDIS
            "qt":"1"
        },
        {
            "id":"5af198ecaf96280400178239",//KLCA1
            "qt":"1"
        },  
        {
            "id":"5af199eaaf96280400178244",//2026B1
            "qt":"1"
        },  
        {
            "id":"5af2ccc969a7f80400329966",//KLGRI1
            "qt":"1"
        },
        {
            "id":"5af19b7baf9628040017824f",//KLS1
            "qt":"1"
        },  
        {
            "id":"5af19671af96280400178232",//KMOLLANG
            "qt":"2"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"1"
        },  
        {
            "id":"5af2ce0669a7f80400329969",//FERMACAVO
            "qt":"1"
        },  
        {
            "id":"5af2ba1669a7f8040032994b",//KMOLLAT
            "qt":"2"
        },
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        } 
    ],
    "11_12_9006":[
        {
            "id":"5a9fc45efdceec04005ad6be",//KLAB0
            "qt":"1"
        },  
        {
            "id":"5a9fc295fdceec04005ad6b2",//KLAI0
            "qt":"1"
        },  
        {
            "id":"5a9fc48cfdceec04005ad6c1",//KLDIS
            "qt":"1"
        },
        {
            "id":"5af19919af9628040017823b",//KLCA0
            "qt":"1"
        },  
        {
            "id":"5af19a11af96280400178246",//2026B0
            "qt":"1"
        },  
        {
            "id":"5af2cd0569a7f80400329968",//KLGRI0
            "qt":"1"
        },
        {
            "id":"5af19baaaf96280400178251",//KLS0
            "qt":"1"
        },  
        {
            "id":"5af19671af96280400178232",//KMOLLANG
            "qt":"2"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"1"
        },  
        {
            "id":"5af2ce0669a7f80400329969",//FERMACAVO
            "qt":"1"
        },  
        {
            "id":"5af2ba1669a7f8040032994b",//KMOLLAT
            "qt":"2"
        },
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        } 
    ],
    "11_12_9005":[
        {
            "id":"5a9fc474fdceec04005ad6c0",//KLAB2
            "qt":"1"
        },  
        {
            "id":"5a9fc281fdceec04005ad6b1",//KLAI2
            "qt":"1"
        },  
        {
            "id":"5a9fc48cfdceec04005ad6c1",//KLDIS
            "qt":"1"
        },
        {
            "id":"5af19901af9628040017823a",//KLCA2
            "qt":"1"
        },  
        {
            "id":"5af199fbaf96280400178245",//2026B2
            "qt":"1"
        },  
        {
            "id":"5af2cceb69a7f80400329967",//KLGRI2
            "qt":"1"
        },
        {
            "id":"5af19b93af96280400178250",//KLS2
            "qt":"1"
        },  
        {
            "id":"5af19671af96280400178232",//KMOLLANG
            "qt":"2"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"1"
        },  
        {
            "id":"5af2ce0669a7f80400329969",//FERMACAVO
            "qt":"1"
        },  
        {
            "id":"5af2ba1669a7f8040032994b",//KMOLLAT
            "qt":"2"
        },
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        } 
    ],
    "11_12_other":[
        {
            "id":"5a9fc474fdceec04005ad6c0",//KLAB2
            "qt":"1"
        },  
        {
            "id":"5a9fc281fdceec04005ad6b1",//KLAI2
            "qt":"1"
        },  
        {
            "id":"5a9fc48cfdceec04005ad6c1",//KLDIS
            "qt":"1"
        },
        {
            "id":"5af19933af9628040017823c",//KLCARAL
            "qt":"1"
        },  
        {
            "id":"5af19a3caf96280400178247",//2026BRAL
            "qt":"1"
        },  
        {
            "id":"5af2cceb69a7f80400329967",//KLGRI2
            "qt":"1"
        },
        {
            "id":"5af19bc8af96280400178252",//KLSRAL
            "qt":"1"
        },  
        {
            "id":"5af19671af96280400178232",//KMOLLANG
            "qt":"2"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"1"
        },  
        {
            "id":"5af2ce0669a7f80400329969",//FERMACAVO
            "qt":"1"
        },  
        {
            "id":"5af2ba1669a7f8040032994b",//KMOLLAT
            "qt":"2"
        },
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        } 
    ],
    "9_10":[
        {
            "id":"5a9fc474fdceec04005ad6c0",//KLAB2
            "qt":"1"
        },  
        {
            "id":"5a9fc281fdceec04005ad6b1",//KLAI2
            "qt":"1"
        },  
        {
            "id":"5a9fc48cfdceec04005ad6c1",//KLDIS
            "qt":"1"
        },
        {
            "id":"5af19959af9628040017823d",//KLCAGALV
            "qt":"1"
        },  
        {
            "id":"5af19a64af96280400178248",//2026BGALV
            "qt":"1"
        },  
        {
            "id":"5af2cceb69a7f80400329967",//KLGRI2
            "qt":"1"
        },
        {
            "id":"5af19baaaf96280400178251",//KLS0
            "qt":"1"
        },  
        {
            "id":"5af19671af96280400178232",//KMOLLANG
            "qt":"2"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"1"
        },  
        {
            "id":"5af2ce0669a7f80400329969",//FERMACAVO
            "qt":"1"
        },  
        {
            "id":"5af2ba1669a7f8040032994b",//KMOLLAT
            "qt":"2"
        },
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        } 
    ]
}

mapOttica = {
    "B":"5a9fc2e8fdceec04005ad6b3", //KLL12
    "M":"5a9fc302fdceec04005ad6b6", //KLL38
    "F":"5a9e69fd60218204000d684c", //KSL24
    "T":"5a9e6a2060218204000d684e"  //KSR60
 }
 mapOtticaQt=1

 mapLed={
    "5":"5915b5ba5c28050400609f67",
    "6":"5915b60c5c28050400609f6b",
    "7":"5915b64d5c28050400609f6e",
    "R":"592eb14fb480230400431447",
    "Z":"592eb17db480230400431448",
    "8":"5915b68a5c28050400609f70",
    "Q":"5819c0e4d5718e690d56a92d",
    "W":"592ed840b48023040043146d"
 }
mapLedQt="1";
 
mapDriver = {
    "10_E":"5abcdc35840de104005bc873",   //400126127484
    "10_D":"5aafa093a30ad1040093ae01",  //400126127490
    "10_L":"5aafa093a30ad1040093ae01",  //400126127490

    "12_E":"5abcdc35840de104005bc873",   //400126127484
    "12_D":"5aafa093a30ad1040093ae01",  //400126127490
    "12_L":"5aafa093a30ad1040093ae01"  //400126127490
 }
 mapDriverQt=1


mongoose.connection.once('open', function()
{
        console.log("Connessione aperta, parte il batch."); /// this gets printed to console


    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("583bfb098957d2040071c175"), //	2026
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }
        //,codice:'20118CT9006D'
    };
     

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                pClone.componenti = [];
                
                var componenti = [];

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var corpi = [], chiave="";

                if (pClone.codice.length==11 || pClone.codice.length==12){
                    chiave = "11_12_" + pClone.codice.substring(7,11)
                }
                else if (pClone.codice.length==9 || pClone.codice.length==10){
                    chiave = "9_10"
                }

                corpi=_.clone(mapCorpo[chiave]);
                if (!corpi){
                    corpi=_.clone(mapCorpo["11_12_other"]);
                }
                
                if (corpi){
                    corpi.forEach(function(element){
                        
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element.id)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: element.qt,
                                _id: doc._id
                            }
                        )
                    })
                } 

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //OTTICA
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (mapOttica[pClone.codice.substring(6,7)]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapOttica[pClone.codice.substring(6,7)])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Ottica',
                            qt: mapOtticaQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //LED
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (mapLed[pClone.codice.substring(4,5)]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapLed[pClone.codice.substring(4,5)])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Led',
                            qt: mapLedQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //DRIVER
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var chiave="10_"+pClone.codice.substring(9,10) 
                if (!mapDriver[chiave]){
                   chiave="12_"+pClone.codice.substring(11,12) 
                }
                if (mapDriver[chiave]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapDriver[chiave])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Driver',
                            mapDriverQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //MONTAGGIO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Montaggio',
                        qt: 1,
                        _id: doc._id
                    }
                )

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //IMBALLO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['cartone'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'imballo',
                        qt: 1,
                        _id: doc._id
                    }
                )
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'imballo',
                        qt: 1,
                        _id: doc._id
                    }
                )

                if (componenti.length){

                    console.log(aggiornati, pClone.codice)
                                
                    var update = {};
                    update.$set = {};
                    
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_2";
                        
                    bulk.find({_id: pClone._id}).updateOne(update);

                    aggiornati++;
 
                }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
