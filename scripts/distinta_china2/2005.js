/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/2005.js;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
   
    mapLed = require('./led2025Model'),
    mapLedQt=1,

    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),
     

    aggiornati = 0;
    mongoose.connect(db);

    

//////
//CORPO
//////
mapCorpo = {
    "9":
        [
            "5a9e78b485347d04003f233d", //KSAI2
            "5a9e787985347d04003f233a", //KSAB2
            "5a9ea90dfdceec04005ad5bc", //KSSNO2
            "5a9e6db860218204000d685d", //KSCGALV
            "5a9e78e585347d04003f233f", //KSDIS
            "5a9fa843fdceec04005ad688", //KSPS2 
            "5a9e80f0a29ce004003c3f4e", ///KDISTA2
            "5a9e82f7a29ce004003c3f59",// KNOT2
            "5a9e969bfdceec04005ad592", //KRACCFIL
       ]
}
mapCorpoQt=1

mapCorpo11 = {
    "9016": [
        "5a9e78b485347d04003f233d", //KSAI1
        "5a9e786e85347d04003f2339", // KSAB1
        "5a9ea8fdfdceec04005ad5bb", //  KSSNO1
        "5a9e6d9960218204000d685b", // KSCRAL
        "5a9e78e585347d04003f233f", // KSDIS
        "5a9ea87ffdceec04005ad5ba", // KSPS1
        "5a9e80a5a29ce004003c3f4a", // KDISTA1
        "5a9e82e4a29ce004003c3f58", // KNOT1
        "5a9e969bfdceec04005ad592", // KRACCFIL
    ],
 
    "9005": [
        "5a9e78b485347d04003f233d", //KSAI2
        "5a9e787985347d04003f233a", // KSAB2
        "5a9ea90dfdceec04005ad5bc", //  KSSNO2
        "5a9e6d9960218204000d685b", // KSCRAL
        "5a9e78e585347d04003f233f", // KSDIS
        "5a9fa843fdceec04005ad688", // KSPS2
        "5a9e80f0a29ce004003c3f4e", // KDISTA2
        "5a9e82f7a29ce004003c3f59", // KNOT2
        "5a9e969bfdceec04005ad592", // KRACCFIL
    ],
    "other": [
        "5a9e78b485347d04003f233d", //KSAI2
        "5a9e787985347d04003f233a", //KSAB2  
        "5a9ea90dfdceec04005ad5bc", //  KSSNO2   
        "5a9e979ffdceec04005ad596", //KSCRAL  
        "5a9e78e585347d04003f233f", //KSDIS  
        "5a9fa843fdceec04005ad688", //KSPS2  
        "5a9e80f0a29ce004003c3f4e", //KDISTA2  
        "5a9e82f7a29ce004003c3f59", //KNOT2  
        "5a9e969bfdceec04005ad592", //KRACCFIL  
    ]
}

mapCorpo11Qt=1


//////
//Ottica
//////
mapOttica = {
    "F":"5a9e69fd60218204000d684c",  //KSL24
    "M":"5a9e6a0860218204000d684d",  //KSL38
    "T":"5a9e6a2060218204000d684e"   //KSR60
} 

mapOtticaQt=1


//Adattatore
//////
mapAdattatore = {
    "9":"5ae3379eaa676e0400097d3d",   //9909-4-DRI/B
    "10":"5ae2dab7eae0130400952ee2"   //9909-DRI/B 
} 
 mapAdattatoreQt=1

 mapAdattatore11 = {
    "9016": "5ae3378daa676e0400097d3c", //9909-4-DRI/W
    "other":"5ae3379eaa676e0400097d3d"  //9909-4-DRI/B 
}

mapAdattatore11Qt=1

mapAdattatore12 = {
    "9016": "5ae2dac7eae0130400952ee3", // 9909-DRI/W
    "other":"5ae2dab7eae0130400952ee2"  //9909-DRI/B  
}
mapAdattatore12Qt=1


mongoose.connection.once('open', function()
{
    console.log("Connessione aperta, parte il batch.");

    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("5847e60d0e147c0400fde02a"), // 2005
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }
    };

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                var componenti = [];
                
                if ( pClone.codice.length==9 || pClone.codice.length==10 || pClone.codice.length==11 || pClone.codice.length==12){

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

                if (pClone.codice.length==9 || pClone.codice.length==10){
                    var corpi=mapCorpo[9];
                    if (corpi && corpi.length){
                        corpi.forEach(function(element){

                            doc = new Componente({
                                _id: new mongoose.Types.ObjectId(element)
                            });
                            componenti.push(
                                {
                                    tipoAssociazione:'Corpo',
                                    qt: mapCorpoQt,
                                    _id: doc._id
                                }
                            )
                        })
                    }
                }
                else if (pClone.codice.length==11 || pClone.codice.length==12){
                    var corpi=mapCorpo11[(pClone.codice.substring(7,11))];

                    if (!corpi || !corpi.length){
                        corpi=mapCorpo11[("other")];
                    }
                    console.log(corpi)

                    corpi.forEach(function(element){

                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: mapCorpo11Qt,
                                _id: doc._id
                            }
                        )
                    })
                }  
                 
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //ADATTATORE
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

                if ( (pClone.codice.length==9 || pClone.codice.length==10)){

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapAdattatore[(pClone.codice.length)])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Adattatore',
                            qt: mapAdattatoreQt,
                            _id: doc._id
                        }
                    )
                }
                else if (pClone.codice.length==11){

                    var adattatore11=mapAdattatore11[(pClone.codice.substring(7,11))];

                    if (!adattatore11 || !adattatore11.length){
                        adattatore11=mapAdattatore11[("other")];
                    }

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(adattatore11)
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Adattatore',
                            qt: mapAdattatore11Qt,
                            _id: doc._id
                        }
                    )
                }
                else if (pClone.codice.length==12){

                    var adattatore12=mapAdattatore12[(pClone.codice.substring(7,11))];

                    if (!adattatore12 || !adattatore12.length){
                        adattatore12=mapAdattatore12[("other")];
                    }

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(adattatore12)
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Adattatore',
                            qt: mapAdattatore11Qt,
                            _id: doc._id
                        }
                    )
                }
            }
            else if ( pClone.codice.length==8){

                 //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

                var corpi=mapCorpo8[(pClone.codice.substring(7,8))];

                corpi.forEach(function(element){

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(element)
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Corpo',
                            qt: mapCorpo8Qt,
                            _id: doc._id
                        }
                    )
                })


                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //ADATTATORE
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 

                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapAdattatore8[(pClone.codice.substring(7,8))])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Adattatore',
                        qt: mapAdattatore8Qt,
                        _id: doc._id
                    }
                )
            }
             

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //OTTICA
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

            doc = new Componente({
                _id: new mongoose.Types.ObjectId(mapOttica[(pClone.codice.substring(6,7))])
            });
            componenti.push(
                {
                    tipoAssociazione:'Ottica',
                    qt: mapOtticaQt,
                    _id: doc._id
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //LED
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

            doc = new Componente({
                _id: new mongoose.Types.ObjectId(mapLed[(pClone.codice.substring(4,5))])
            });
            componenti.push(
                {
                    tipoAssociazione:'Led',
                    qt: mapLedQt,
                    _id: doc._id
                }
            )


            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //MONTAGGIO
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Montaggio',
                    qt: 1,
                    _id: doc._id //M0608
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //CARTONE
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['cartone'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Imballo',
                    qt: 1,
                    _id: doc._id
                }
            ) 

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //sacchetto
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Imballo',
                    qt: 1,
                    _id: doc._id
                }
            )
                 

            if (componenti.length){

                console.log(aggiornati, pClone.codice)
                            
                var update = {};
                update.$set = {};
                
                update.$set.componenti = componenti;
                update.$set.distintabase = "china_2";
                    
                bulk.find({_id: pClone._id}).updateOne(update);

                aggiornati++;
                

            }

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
