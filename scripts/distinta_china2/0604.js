/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/0604.js;
*/
//CORPO/OTTICA/MODULO LED (SE C'è)/LED/ADATTATORE(SE C'è)/DRIVER/MONTAGGIO/IMBALLO(SE C'è)

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
   
      
    mapModuloV1Plus = require('./moduloV1Plus'),
    mapModuloV1PlusQt=1

    mapModuloV2= require('./moduloV2'),
    mapModuloV2Qt=1,

    mapMolla= require('./molla'),
    mapMollaQt=2,

    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),

    aggiornati = 0;
    mongoose.connect(db); 

//////
//COMPONENTI STANDARD
//////
mapCorpo =  {
    "1":[
        "5acc88af8909c40400be6b28", //0604B2
        "5acc9af28909c40400be6b50", //0604F1
        "5acc9bb88909c40400be6b59", //0604A1
        "5acc9c2f8909c40400be6b5e", //0604BS1
        "5a952cd8b638e50400d8fb1e" //RACCBS
    ],
    "2":[
        "5acc88af8909c40400be6b28", //0604B2
        "5acc9b078909c40400be6b52", //0604F2
        "5acc9bb88909c40400be6b59", //0604A1
        "5acc9c258909c40400be6b5d", //0604BS2
        "5a952cd8b638e50400d8fb1e" //RACCBS
    ],
    "0":[
        "5acc88af8909c40400be6b28", //0604B2
        "5acc88c58909c40400be6b29", //0604F0
        "5acc9be78909c40400be6b5c", //0604A0
        "5acc9c258909c40400be6b5d", //0604BS2
        "5a952cd8b638e50400d8fb1e" //RACCBS
    ],
    "9":[
        "5acc88998909c40400be6b27", //0604B1
        "5acc9af28909c40400be6b50", //0604F1
        "5acc9bb88909c40400be6b59", //0604A1
        "5acc9c2f8909c40400be6b5e", //0604BS1
        "5a952cd8b638e50400d8fb1e" //RACCBS
    ]
}
mapCorpoQt=1
 
mapDriver = {
    "9_E":"5a9d1a4c2bfffb04000f1c5c",  //400126122350
    "9_D":"5a9d1aaa2bfffb04000f1c5e",  //400126122400
    "9_L":"5a9d1ac62bfffb04000f1c5f"   //400126122403
}
mapDriverQt=1


mongoose.connection.once('open', function()
{
        console.log("Connessione aperta, parte il batch."); /// this gets printed to console


    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("56b8aaa90de23c95600bd285"), //	0604
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }
    };
     

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                var componenti = [];

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var corpi = []
                corpi=_.clone(mapCorpo[pClone.codice.substring(7,8)]);  

                if (corpi){
                    corpi.forEach(function(element){
                        
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: mapCorpoQt,
                                _id: doc._id
                            }
                        )
                    })
                }

                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapMolla)
                });
                componenti.push(
                    {
                        tipoAssociazione:'Corpo',
                        qt: mapMollaQt,
                        _id: doc._id
                    }
                ) 

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //V1 PLUS
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                
                if (mapModuloV1Plus[(pClone.codice.substring(4,7))]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapModuloV1Plus[(pClone.codice.substring(4,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo V1 Plus',
                            qt: mapModuloV1PlusQt,
                            _id: doc._id
                        }
                    )
                }
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //V2 PLUS
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                else if (mapModuloV2[(pClone.codice.substring(4,7))]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapModuloV2[(pClone.codice.substring(4,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo V2',
                            qt: mapModuloV2Qt,
                            _id: doc._id
                        }
                    )
                }

                 

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //DRIVER
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
              
                if (mapDriver[pClone.codice.length+'_'+pClone.codice.substring(8,9)]){
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapDriver[pClone.codice.length+'_'+pClone.codice.substring(8,9)])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Driver',
                        qt:mapDriverQt,
                        _id: doc._id
                    }
                )
            }


                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //MONTAGGIO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Montaggio',
                        qt: 1,
                        _id: doc._id
                    }
                )
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //IMBALLO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Imballo',
                        qt: 1,
                        _id: doc._id
                    }
                )

                if (componenti.length){

                    console.log(aggiornati, pClone.codice)
                                
                    var update = {};
                    update.$set = {};
                    
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_2";
                        
                    bulk.find({_id: pClone._id}).updateOne(update);

                    aggiornati++;
 
                }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
