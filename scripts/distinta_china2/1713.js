/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/1713.js;

*/
//CORPO/OTTICA/MODULO LED (SE C'è)/LED/ADATTATORE(SE C'è)/DRIVER/MONTAGGIO/IMBALLO(SE C'è)

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
       
    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),

    aggiornati = 0;
    mongoose.connect(db); 

    F111 = require('./componentiF111'),
    F112 = require('./componentiF112');

//////
//COMPONENTI STANDARD
//////
mapCorpo =  {
    "1":[
        {
            "id":"5af5656f55ec9f040082df1b",//1713XXX1
            "qt":"1"
        },
        {
            "id":"5af2ce0669a7f80400329969",//FERMACAVO
            "qt":"2"
        }

    ],
    "0":[
        {
            "id":"5af5657955ec9f040082df1c",//1713XXX0
            "qt":"1"
        },  
        {
            "id":"5af2ce0669a7f80400329969",//FERMACAVO
            "qt":"2"
        }
    ]
}

mapOttica = {
    "D":"5a82f055cf845a0400031566", //F111R17
    "H":"5a82f05ecf845a0400031567", //F111R26
    "N":"5a82f06acf845a0400031568", //F111R40
    "B":"5a8bedccedb6fd040008c3b0",  //F112L12
    "F":"5a8bedd8edb6fd040008c3b1",  //F112L24
    "M":"5a8bede5edb6fd040008c3b2"  //F112L38
    
 }
 mapOtticaQt=2

 mapLed={
    "I":"5819bee7d5718e690d56a920",
    "A":"5819bf2ad5718e690d56a921",
    "B":"5819bf5fd5718e690d56a922",
    "O":"5819c112d5718e690d56a92f",
    "S":"5819c130d5718e690d56a930",
    "T":"5819c159d5718e690d56a931",
    "Q":"5819c0e4d5718e690d56a92d",
    "W":"592ed840b48023040043146d"
}
mapLedQt="2";
 
mapDriver = {
    "E":"5abcdc35840de104005bc873",   //400126127484
    "D":"5aafa093a30ad1040093ae01",  //400126127490
    "L":"5aafa093a30ad1040093ae01",  //400126127490
 }
 mapDriverQt=2


mongoose.connection.once('open', function()
{
        console.log("Connessione aperta, parte il batch."); /// this gets printed to console


    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("56b8aaa90de23c95600bd27a"), //	1713
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }
        //,codice:'20118CT9006D'
    };
     

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                pClone.componenti = [];
                
                var componenti = [];

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var corpi = [], chiave="";

                chiave = pClone.codice.substring(7,8)
                corpi=_.clone(mapCorpo[chiave]);
                if (!corpi){
                    corpi=_.clone(mapCorpo["11_12_other"]);
                }
                
                if (corpi){
                    corpi.forEach(function(element){
                        
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element.id)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: element.qt,
                                _id: doc._id
                            }
                        )
                    })
                } 

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //OTTICA
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (mapOttica[pClone.codice.substring(6,7)]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapOttica[pClone.codice.substring(6,7)])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Ottica',
                            qt: mapLedQt,
                            _id: doc._id
                        }
                    )
                }

                

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //LED
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (mapLed[pClone.codice.substring(4,5)]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapLed[pClone.codice.substring(4,5)])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Led',
                            qt: mapLedQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //DRIVER
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var chiave=pClone.codice.substring(8,9) 
                if (mapDriver[chiave]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapDriver[chiave])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Driver',
                            qt:mapDriverQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //MONTAGGIO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Montaggio',
                        qt: 1,
                        _id: doc._id
                    }
                )

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //IMBALLO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['cartone'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'imballo',
                        qt: 1,
                        _id: doc._id
                    }
                )
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'imballo',
                        qt: 1,
                        _id: doc._id
                    }
                )

                if (componenti.length){

                    console.log(aggiornati, pClone.codice)
                                
                    var update = {};
                    update.$set = {};
                    
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_2";
                        
                    bulk.find({_id: pClone._id}).updateOne(update);

                    aggiornati++;
 
                }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
