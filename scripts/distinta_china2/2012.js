/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/2012.js;
*/
//CORPO/OTTICA/MODULO LED (SE C'è)/LED/ADATTATORE(SE C'è)/DRIVER/MONTAGGIO/IMBALLO(SE C'è)

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
       
    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),

    aggiornati = 0;
    mongoose.connect(db); 

//////
//COMPONENTI STANDARD
//////
mapCorpo =  {
    "11_12_9016":[
        {
            "id":"5a9e786e85347d04003f2339",//KSAB1
            "qt":"2"
        },  
        {
            "id":"5a9e78aa85347d04003f233c",//KSAI1
            "qt":"2"
        },  
        {
            "id":"5a9e78e585347d04003f233f",//KDIS
            "qt":"2"
        },
        {
            "id":"5af02340ec11ff040073a4b5",//KSCA1
            "qt":"2"
        },  
        {
            "id":"5af042d9ec11ff040073a4ee",//2012B1
            "qt":"1"
        },  
        {
            "id":"5af2cc5c69a7f80400329963",//KSGRI1
            "qt":"2"
        },
        {
            "id":"5af03280ec11ff040073a4d5",//KSS1
            "qt":"2"
        },  
        {
            "id":"5af19671af96280400178232",//KMOLLANG
            "qt":"4"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"2"
        },  
        {
            "id":"5af2ce0669a7f80400329969",//FERMACAVO
            "qt":"2"
        },  
        {
            "id":"5ad9a4fe47688a04007d0054",//KCAVO
            "qt":"2"
        },
        {
            "id":"5ad9a60747688a04007d005e",//KGUAINA
            "qt":"2"
        },
        {
            "id":"5af2ba1669a7f8040032994b",//KMOLLAT
            "qt":"4"
        },
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"2"
        }
    ],
    "11_12_9006":[
        {
            "id":"5a9e788485347d04003f233b",//KSAB0
            "qt":"2"
        },  
        {
            "id":"5a9e78be85347d04003f233e",//KSAI0
            "qt":"2"
        },  
        {
            "id":"5a9e78e585347d04003f233f",//KSDIS
            "qt":"2"
        },
        {
            "id":"5af02f39ec11ff040073a4ca",//KSCA0
            "qt":"2"
        },  
        {
            "id":"5af042feec11ff040073a4f0",//2012B0
            "qt":"1"
        },  
        {
            "id":"5af2cc7069a7f80400329964",//KSGRI0
            "qt":"2"
        },
        {
            "id":"5af032a3ec11ff040073a4d7",//KSS0
            "qt":"2"
        },  
        {
            "id":"5af19671af96280400178232",//KMOLLANG
            "qt":"4"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"2"
        },
        {
            "id":"5af2ce0669a7f80400329969",//FERMACAVO
            "qt":"2"
        },  
        {
            "id":"5ad9a4fe47688a04007d0054",//KCAVO
            "qt":"2"
        },
        {
            "id":"5ad9a60747688a04007d005e",//KGUAINA
            "qt":"2"
        },
        {
            "id":"5af2ba1669a7f8040032994b",//KMOLLAT
            "qt":"4"
        },
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"2"
        }
    ],
    "11_12_9005":[
        {
            "id":"5a9e787985347d04003f233a",//KSAB2
            "qt":"2"
        },  
        {
            "id":"5a9e78b485347d04003f233d",//KSAI2
            "qt":"2"
        },  
        {
            "id":"5a9e78e585347d04003f233f",//KSDIS
            "qt":"2"
        },
        {
            "id":"5af02f06ec11ff040073a4c9",//KSCA2
            "qt":"2"
        },  
        {
            "id":"5af042eaec11ff040073a4ef",//2012B2
            "qt":"1"
        },  
        {
            "id":"5af2cc8569a7f80400329965",//KSGRI2
            "qt":"2"
        },
        {
            "id":"5af03292ec11ff040073a4d6",//KSS2
            "qt":"2"
        },  
        {
            "id":"5af19671af96280400178232",//KMOLLANG
            "qt":"4"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"2"
        },
        {
            "id":"5af2ce0669a7f80400329969",//FERMACAVO
            "qt":"2"
        },  
        {
            "id":"5ad9a4fe47688a04007d0054",//KCAVO
            "qt":"2"
        },
        {
            "id":"5ad9a60747688a04007d005e",//KGUAINA
            "qt":"2"
        },
        {
            "id":"5af2ba1669a7f8040032994b",//KMOLLAT
            "qt":"4"
        },
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"2"
        }
    ],
    "11_12_other":[
        {
            "id":"5a9e787985347d04003f233a",//KSAB2
            "qt":"2"
        },  
        {
            "id":"5a9e78b485347d04003f233d",//KSAI2
            "qt":"2"
        },  
        {
            "id":"5a9e78e585347d04003f233f",//KSDIS
            "qt":"2"
        },
        {
            "id":"5af02f4bec11ff040073a4cb",//KSCARAL
            "qt":"2"
        },  
        {
            "id":"5af0431cec11ff040073a4f1",//2012BRAL
            "qt":"1"
        },  
        {
            "id":"5af2cc8569a7f80400329965",//KSGRI2
            "qt":"2"
        },
        {
            "id":"5af032b2ec11ff040073a4d8",//KSSRAL
            "qt":"2"
        },  
        {
            "id":"5af19671af96280400178232",//KMOLLANG
            "qt":"4"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"2"
        },
        {
            "id":"5af2ce0669a7f80400329969",//FERMACAVO
            "qt":"2"
        },  
        {
            "id":"5ad9a4fe47688a04007d0054",//KCAVO
            "qt":"2"
        },
        {
            "id":"5ad9a60747688a04007d005e",//KGUAINA
            "qt":"2"
        },
        {
            "id":"5af2ba1669a7f8040032994b",//KMOLLAT
            "qt":"4"
        },
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"2"
        }
    ],
    "9_10":[
        {
            "id":"5a9e787985347d04003f233a",//KSAB2
            "qt":"2"
        },
        {
            "id":"5a9e78b485347d04003f233d",//KSAI2
            "qt":"2"
        },
        {
            "id":"5a9e78e585347d04003f233f",//KSDIS
            "qt":"2"
        },
        {
            "id":"5af02f5bec11ff040073a4cc",//KSCAGALV
            "qt":"2"
        },
        {
            "id":"5af0432dec11ff040073a4f2",//2012BGALV
            "qt":"1"
        },
        {
            "id":"5af2cc8569a7f80400329965",//KSGRI2
            "qt":"2"
        },
        {
            "id":"5af03292ec11ff040073a4d6",//KSS2
            "qt":"2"
        },
        {
            "id":"5af19671af96280400178232",//KMOLLANG
            "qt":"4"
        },
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"2"
        },
        {
            "id":"5af2ce0669a7f80400329969",//FERMACAVO
            "qt":"2"
        }, 
        {
            "id":"5ad9a4fe47688a04007d0054",//KCAVO
            "qt":"2"
        },
        {
            "id":"5ad9a60747688a04007d005e",//KGUAINA
            "qt":"2"
        },
        {
            "id":"5af2ba1669a7f8040032994b",//KMOLLAT
            "qt":"4"
        },
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"2"
        }
    ]
}

mapOttica = {
    "M":"5a9e6a0860218204000d684d", //KSL38
    "F":"5a9e69fd60218204000d684c", //KSL24
    "T":"5a9e6a2060218204000d684e"  //KSR60
 }
 mapOtticaQt=2

 mapLed={
     "5":"5a9e9f3afdceec04005ad5a1",
     "6":"5a9e9f5dfdceec04005ad5a2",
     "7":"5a9e9fb0fdceec04005ad5a5",
     "R":"5a9e9f05fdceec04005ad5a0",
     "Z":"5a9e9f86fdceec04005ad5a3",
     "8":"5a9e9fd3fdceec04005ad5a7",
     "Q":"5819c0e4d5718e690d56a92d",
     "W":"592ed840b48023040043146d"
 }
mapLedQt="2";
 
mapDriver = {
    "10_E":"5abcdc35840de104005bc873",   //400126127484
    "10_D":"5aafa093a30ad1040093ae01",  //400126127490
    "10_L":"5aafa093a30ad1040093ae01",  //400126127490

    "12_E":"5abcdc35840de104005bc873",   //400126127484
    "12_D":"5aafa093a30ad1040093ae01",  //400126127490
    "12_L":"5aafa093a30ad1040093ae01"  //400126127490
 }
 mapDriverQt=2


mongoose.connection.once('open', function()
{
        console.log("Connessione aperta, parte il batch."); /// this gets printed to console


    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("58247983de6dd4040049c3b5"), //	2011
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }
        //,codice:'20118CT9006D'
    };
     

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                pClone.componenti = [];
                
                var componenti = [];

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var corpi = [], chiave="";

                if (pClone.codice.length==11 || pClone.codice.length==12){
                    chiave = "11_12_" + pClone.codice.substring(7,11)
                }
                else if (pClone.codice.length==9 || pClone.codice.length==10){
                    chiave = "9_10"
                }

                corpi=_.clone(mapCorpo[chiave]);
                if (!corpi){
                    corpi=_.clone(mapCorpo["11_12_other"]);
                }
                
                if (corpi){
                    corpi.forEach(function(element){
                        
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element.id)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: element.qt,
                                _id: doc._id
                            }
                        )
                    })
                } 

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //OTTICA
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (mapOttica[pClone.codice.substring(6,7)]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapOttica[pClone.codice.substring(6,7)])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Ottica',
                            qt: mapLedQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //LED
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (mapLed[pClone.codice.substring(4,5)]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapLed[pClone.codice.substring(4,5)])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Led',
                            qt: mapLedQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //DRIVER
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var chiave="10_"+pClone.codice.substring(9,10) 
                if (!mapDriver[chiave]){
                   chiave="12_"+pClone.codice.substring(11,12) 
                }
                if (mapDriver[chiave]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapDriver[chiave])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Driver',
                            qt:mapDriverQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //MONTAGGIO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Montaggio',
                        qt: 1,
                        _id: doc._id
                    }
                )

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //IMBALLO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['cartone'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'imballo',
                        qt: 1,
                        _id: doc._id
                    }
                )
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'imballo',
                        qt: 1,
                        _id: doc._id
                    }
                )

                if (componenti.length){

                    console.log(aggiornati, pClone.codice)
                                
                    var update = {};
                    update.$set = {};
                    
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_2";
                        
                    bulk.find({_id: pClone._id}).updateOne(update);

                    aggiornati++;
 
                }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
