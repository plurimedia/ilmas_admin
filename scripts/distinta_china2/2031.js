/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/2031.js;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
   
    mapLed = require('./led2031Model'),
    mapLedQt=1,

    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),
     

    aggiornati = 0;
    mongoose.connect(db);

    

//////
//CORPO
//////
 
mapCorpo = { 
    "8-1": [
        "5aaa8818fc42390400dbcb42", //KXLCD1
        "5aaa8e91fc42390400dbcb6f", // KXLAB1
        "5aaa8d45fc42390400dbcb62", //  KXLAI1
        "5a9e80a5a29ce004003c3f4a", // KDISTA1
        "5a9fcbfefdceec04005ad6e9", // KLSNO1
        "5aaf9c24a30ad1040093ade7", // KXLPS1
        "5a9e82e4a29ce004003c3f58", // KNOT1
        "5a9e969bfdceec04005ad592" // KRACCFIL
    ],
    "8-2": [
        "5aaa8a2ffc42390400dbcb4b", //KXLCD2
        "5aaa8e87fc42390400dbcb6e", // KXLAB2
        "5aaa8e4afc42390400dbcb68", // KXLAI2
        "5a9e80f0a29ce004003c3f4e", // KDISTA2
        "5a9fcc12fdceec04005ad6ea", // KLSNO2
        "5aaf9c42a30ad1040093ade8", // KXLPS2
        "5a9e82f7a29ce004003c3f59", // KNOT2
        "5a9e969bfdceec04005ad592" // KRACCFIL
    ],
    "8-0": [
        "5aaa8b0afc42390400dbcb55", //KXLCD0
        "5a9fc45efdceec04005ad6be", // KLAB0
        "5a9fc295fdceec04005ad6b2", //  KLAI0
        "5a9e812ba29ce004003c3f50", // KDISTA0
        "5a9fcc2efdceec04005ad6eb", // KLSNO0
        "5aaf9c5da30ad1040093adea", // KXLPS0
        "5a9e8313a29ce004003c3f5a", // KNOT0
        "5a9e969bfdceec04005ad592" // KRACCFIL
    ]
}
mapCorpoQt=1

//////
//Adattatore Global
////// 
mapAdattatore = {
    "8-1": "5a9fd4cdfdceec04005ad708", //400126127471DI 
    "8-2": "5a9fd475fdceec04005ad707", //400126127473DI 
    "8-0": "5a9fd4eafdceec04005ad709" // 400126127472DI
}
mapAdattatoreQt=1

//////
//Ottica
//////
mapOttica = {
    "B":"5a8bedccedb6fd040008c3b0",  //F112L12
    "M":"5a8bede5edb6fd040008c3b2",  //F112L38
    "F":"5a8bedd8edb6fd040008c3b1",  //F112L24
    "T":"5aaa8761fc42390400dbcb3c" //KXLR60
}

mapOtticaQt=1




mongoose.connection.once('open', function()
{
    console.log("Connessione aperta, parte il batch.");

    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("59b92161d5eaa0040053d681"), // 2031
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        } 

    };

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                var componenti = []; 

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 

                var corpi=mapCorpo[pClone.codice.length+'-'+pClone.codice.substring(7,8)]; 
 
                if (corpi){
                    corpi.forEach(function(element){
console.log("element ", element)
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: mapCorpo,
                                _id: doc._id
                            }
                        )
                    })
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //ADATTATORE
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

                var adattatore=mapAdattatore[pClone.codice.length+'-'+pClone.codice.substring(7,8)]
                if (adattatore){

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(adattatore)
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Adattatore Global',
                            qt: mapAdattatoreQt,
                            _id: doc._id
                        }
                    )
                }
             

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //OTTICA
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

            doc = new Componente({
                _id: new mongoose.Types.ObjectId(mapOttica[pClone.codice.substring(6,7)])
            });
            componenti.push(
                {
                    tipoAssociazione:'Ottica',
                    qt: mapOtticaQt,
                    _id: doc._id
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //LED
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

            doc = new Componente({
                _id: new mongoose.Types.ObjectId(mapLed[pClone.codice.substring(4,5)])
            });
            componenti.push(
                {
                    tipoAssociazione:'Led',
                    qt: mapLedQt,
                    _id: doc._id
                }
            )


            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //MONTAGGIO
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Montaggio',
                    qt: 1,
                    _id: doc._id //M0608
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //CARTONE
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['cartone'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Imballo',
                    qt: 1,
                    _id: doc._id
                }
            ) 

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //sacchetto
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Imballo',
                    qt: 1,
                    _id: doc._id
                }
            )
            if (componenti.length){

                console.log(aggiornati, pClone.codice)
                            
                var update = {};
                update.$set = {};
                
                update.$set.componenti = componenti;
                update.$set.distintabase = "china_2";
                    
                bulk.find({_id: pClone._id}).updateOne(update);

                aggiornati++;
                

            }

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
