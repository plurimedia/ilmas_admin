/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/1334.js;
*/
//CORPO/OTTICA/MODULO LED (SE C'è)/LED/ADATTATORE(SE C'è)/DRIVER/MONTAGGIO/IMBALLO(SE C'è)

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
    mapLed = require('./ledAllModel'),    
    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),

    aggiornati = 0;
    mongoose.connect(db); 

//////
//COMPONENTI STANDARD
//////
mapCorpo =  {
    "1":[
            {
                "id":"5ad06781afa9b70400ee5f23",//1332XXX1
                "qt":"1"
            }
        ],
    "2":[
        {
            "id":"5ad0678cafa9b70400ee5f24",//1332XXX2
            "qt":"1"
        }
    ],
    "0":[
        {
            "id":"5ad06797afa9b70400ee5f25",//1332XXX0
            "qt":"1"
        }
    ]  
}  
 
//////
//COMPONENTI MODULO F111
//////
mapCorpo1F111 = {
    "1":"5a82eb5af10c4d04008181b1",   //f111c1
    "9":"5a82eb5af10c4d04008181b1",   //f111c1
    "0":"5a82eca41879400400cbb3f8",   //f111c0
    "2":"5a82ec8b1879400400cbb3f7"   //f111c2
}
mapCorpo2F111 = {
    "1":"5a82edd51879400400cbb3fe",  //F111A1
    "9":"5a82edd51879400400cbb3fe",  //F111A1
    "0":"5a82ee161879400400cbb400",  //F111A0
    "2":"5a82ee021879400400cbb3ff"   //F111A2
}
mapOttica1F111 = {
    "D":"5a82f055cf845a0400031566",   //f111r17
    "H":"5a82f05ecf845a0400031567",   //f111r26
    "N":"5a82f06acf845a0400031568",   //f111r40
    "U":"5a82f06acf845a0400031568"   //f111r40
}
mapOttica2F111 = {
    "D":"5a82ef001879400400cbb402",  //f111v
    "H":"5a82ef001879400400cbb402",  //f111v
    "N":"5a82ef001879400400cbb402",   //f111v
    "U":"5a82ef0c1879400400cbb403"   //f111vs
}

///////
//COMPONENTI MODULO F112
//////
mapCorpo1F112 = {
    "1":"5a8aff8a16137504008b40ac",  //F112C1
    "9":"5a8aff8a16137504008b40ac",  //F112C1
    "0":"5a8affbb16137504008b40ae",  //F112C0
    "2":"5a8affa916137504008b40ad"   //F112C2
}
mapCorpo2F112 = {
    "1":"5a8affec16137504008b40af",  //F112A1
    "9":"5a8affec16137504008b40af",  //F112A1
    "0":"5a8b001516137504008b40b1",  //F112A0
    "2":"5a8b000016137504008b40b0"   //F112A2
}

mapOttica1F112 = {
    "B":"5a8bedccedb6fd040008c3b0", //F112L12
    "F":"5a8bedd8edb6fd040008c3b1",  //f112L24
    "M":"5a8bede5edb6fd040008c3b2",  //f112L38
} 
 
mapDriver = {
    "9_E":"5ab8f0fe4798ca0400a4897b",  //400126127486
    "9_D":"5ab8f1ba4798ca0400a4897c",  //400126127492
    "9_L":"5ab8f1ba4798ca0400a4897c",  //400126127492  
 }
 mapDriverQt=1


mongoose.connection.once('open', function()
{
        console.log("Connessione aperta, parte il batch."); /// this gets printed to console


    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("59aeaee9dd826e040027a43d"), //	1334
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }
    };
     

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                var componenti = [];

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var corpi = []
                corpi=_.clone(mapCorpo[pClone.codice.substring(7,8)]);  

                if (corpi){
                    corpi.forEach(function(element){
                        
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element.id)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: element.qt,
                                _id: doc._id
                            }
                        )
                    })
                } 
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //F111
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (pClone.moduloled && pClone.moduloled.startsWith('F111'))
                {

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapCorpo1F111[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapCorpo2F111[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapOttica1F111[(pClone.codice.substring(6,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapOttica2F111[(pClone.codice.substring(6,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapLed[(pClone.codice.substring(4,5))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id 
                        }
                    )
                }
                else if (pClone.moduloled && pClone.moduloled.startsWith('F112'))
                { 
                  
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapCorpo1F112[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 1,
                            _id: doc._id
                        }
                    )
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapCorpo2F112[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    if (mapOttica1F112[(pClone.codice.substring(6,7))]){
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(mapOttica1F112[(pClone.codice.substring(6,7))])
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Modulo F112',
                                qt: 1,
                                _id: doc._id
                            }
                        )
                    }
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapLed[(pClone.codice.substring(4,5))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 1,
                            _id: doc._id
                        }
                    )
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 1,
                            _id: doc._id 
                        }
                    )
                }
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //DRIVER
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var chiave=pClone.codice.length+"_"+pClone.codice.substring(8,9);
                if (mapDriver[chiave]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapDriver[chiave])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Driver',
                            qt:mapDriverQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //MONTAGGIO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Montaggio',
                        qt: 1,
                        _id: doc._id
                    }
                )
                 

                if (componenti.length){

                    console.log(aggiornati, pClone.codice)
                                
                    var update = {};
                    update.$set = {};
                    
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_2";
                        
                    bulk.find({_id: pClone._id}).updateOne(update);

                    aggiornati++;
 
                }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
