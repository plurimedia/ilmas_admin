/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/0607.js;
*/
//CORPO/OTTICA/MODULO LED (SE C'è)/LED/ADATTATORE(SE C'è)/DRIVER/MONTAGGIO/IMBALLO(SE C'è)

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
   
      
    mapModuloV1Plus = require('./moduloV1Plus'),
    mapModuloV1PlusQt=3

    mapModuloV2= require('./moduloV2'),
    mapModuloV2Qt=3,

    mapMolla= require('./molla'), 

    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),

    aggiornati = 0;
    mongoose.connect(db); 

//////
//COMPONENTI STANDARD
//////
mapCorpo =  {
    "1":[
            {
                "id":"5accdf5b62dff304009af774",//0607B2
                "qt":"1"
            },  
            {
                "id":"5acce0af62dff304009af77b",//0607F1
                "qt":"1"
            },
            {
                "id":"5acc9bb88909c40400be6b59",//0604A1
                "qt":"3"
            },
            {
                "id":"5acc9c2f8909c40400be6b5e",//0604BS1
                "qt":"3"
            },
            {
                "id":"5a952cd8b638e50400d8fb1e",//RACCBS
                "qt":"3"
            },
            {
                "id":mapMolla,//molla
                "qt":"4"
            }
        ],
    "2":[
        {
            "id":"5accdf5b62dff304009af774",//0607B2
            "qt":"1"
        },  
        {
            "id":"5acce0c562dff304009af77c",//0607F2
            "qt":"1"
        },
        {
            "id":"5acc9bb88909c40400be6b59",//0604A2
            "qt":"3"
        },
        {
            "id":"5acc9bb88909c40400be6b59",//0604BS2
            "qt":"3"
        },
        {
            "id":"5a952cd8b638e50400d8fb1e",//RACCBS
            "qt":"3"
        },
        {
            "id":mapMolla,//molla
            "qt":"4"
        }
    ], 
    "0":[
        {
            "id":"5accdf5b62dff304009af774",//0607B2
            "qt":"1"
        },  
        {
            "id":"5acce0e262dff304009af77d",//0607F0
            "qt":"1"
        },
        {
            "id":"5acc9be78909c40400be6b5c",//0604A0
            "qt":"3"
        },
        {
            "id":"5acc9c258909c40400be6b5d",//0604BS2
            "qt":"3"
        },
        {
            "id":"5a952cd8b638e50400d8fb1e",//RACCBS
            "qt":"3"
        },
        {
            "id":mapMolla,//molla
            "qt":"4"
        }
    ],
    "9":[
        {
            "id":"5accdf4862dff304009af773",//0607B1
            "qt":"1"
        },  
        {
            "id":"5acce0af62dff304009af77b",//0607F1
            "qt":"1"
        },
        {
            "id":"5acc9bb88909c40400be6b59",//0604A1
            "qt":"3"
        },
        {
            "id":"5acc9c2f8909c40400be6b5e",//0604BS1
            "qt":"3"
        },
        {
            "id":"5a952cd8b638e50400d8fb1e",//RACCBS
            "qt":"3"
        },
        {
            "id":mapMolla,//molla
            "qt":"4"
        }
    ]
} 
 
mapDriver = {
    "9_E":"5a9d1a4c2bfffb04000f1c5c",  //400126122350
    "9_D":"5a9d1aaa2bfffb04000f1c5e",  //400126122400
    "9_L":"5a9d1ac62bfffb04000f1c5f"   //400126122403
}
mapDriverQt=3


mongoose.connection.once('open', function()
{
        console.log("Connessione aperta, parte il batch."); /// this gets printed to console


    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("56b8aaa90de23c95600bd289"), //	0607
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }
    };
     

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                var componenti = [];

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var corpi = []
                corpi=_.clone(mapCorpo[pClone.codice.substring(7,8)]);  

                if (corpi){
                    corpi.forEach(function(element){
                        
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element.id)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: element.qt,
                                _id: doc._id
                            }
                        )
                    })
                } 

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //V1 PLUS
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                
                if (mapModuloV1Plus[(pClone.codice.substring(4,7))]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapModuloV1Plus[(pClone.codice.substring(4,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo V1 Plus',
                            qt: mapModuloV1PlusQt,
                            _id: doc._id
                        }
                    )
                }
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //V2 PLUS
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                else if (mapModuloV2[(pClone.codice.substring(4,7))]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapModuloV2[(pClone.codice.substring(4,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo V2',
                            qt: mapModuloV2Qt,
                            _id: doc._id
                        }
                    )
                }

                 

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //DRIVER
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
              
                if (mapDriver[pClone.codice.length+'_'+pClone.codice.substring(8,9)]){
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapDriver[pClone.codice.length+'_'+pClone.codice.substring(8,9)])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Driver',
                        qt:mapDriverQt,
                        _id: doc._id
                    }
                )
            }


                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //MONTAGGIO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Montaggio',
                        qt: 1,
                        _id: doc._id
                    }
                )
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //IMBALLO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Imballo',
                        qt: 1,
                        _id: doc._id
                    }
                )

                if (componenti.length){

                    console.log(aggiornati, pClone.codice)
                                
                    var update = {};
                    update.$set = {};
                    
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_2";
                        
                    bulk.find({_id: pClone._id}).updateOne(update);

                    aggiornati++;
 
                }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
