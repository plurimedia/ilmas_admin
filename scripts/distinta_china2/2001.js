/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=10000 scripts/distinta_china2/2001.js;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
   
    
    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),
     

    aggiornati = 0;
   // mongoose.connect(db);
    mongoose.connect(db, {
        replset: {
          socketOptions: {
            connectTimeoutMS: 60*1000
          }
        }
      }, function(err, db) {
      });
    

//////
//CORPO
//////
 
//vale solo per lunghrzza 8 OR 9 (non cara == P)
mapCorpo = { 
    "8_9_1": [
        "5a9fa6e1fdceec04005ad685", //KSCD1
        "5a9e786e85347d04003f2339", // KSAB1
        "5a9e78aa85347d04003f233c", //  KSAI1
        "5abb3eb7b4609a0400b9f63b", // KDISTB1
        "5a9ea8fdfdceec04005ad5bb", // KSSNO1
        "5a9ea87ffdceec04005ad5ba", // KSPS1
        "5a9e82e4a29ce004003c3f58", // KNOT1
        "5abb402fb4609a0400b9f645", // KCAS1
        "5abb410cb4609a0400b9f654",// KCASGRI1
        "5abb3f14b4609a0400b9f63d", //KRACCFRE
        "5a9e969bfdceec04005ad592",//KRACCFIL
        "5abb3f2cb4609a0400b9f63e" //MINUTERIA
    ],
    "8_9_2": [
        "5a9fa72afdceec04005ad686", //KSCD2
        "5a9e787985347d04003f233a", //KSAB2
        "5a9e78b485347d04003f233d", //KSAI2
        "5abb3eccb4609a0400b9f63c", //KDISTB2
        "5a9ea90dfdceec04005ad5bc", //KSSNO2
        "5a9fa843fdceec04005ad688", //KSPS2
        "5a9e82f7a29ce004003c3f59", //KNOT2
        "5abb4055b4609a0400b9f646", //KCAS2
        "5abb4124b4609a0400b9f656", //KCASGRI2
        "5abb3f14b4609a0400b9f63d", //KRACCFRE
        "5a9e969bfdceec04005ad592", //KRACCFIL
        "5abb3f2cb4609a0400b9f63e" //MINUTERIA
    ],
    "8_9_0": [
        "5a9fa745fdceec04005ad687", //KSCD0
        "5a9e788485347d04003f233b", //KSAB0
        "5a9e78be85347d04003f233e", //KSAI0
        "5abb3eccb4609a0400b9f63c", //KDISTB2
        "5a9ea91ffdceec04005ad5bd", //KSSNO0
        "5a9eaa0cfdceec04005ad5cb", //KSPS0
        "5a9e8313a29ce004003c3f5a", //KNOT0
        "5abb406db4609a0400b9f647", //KCAS0
        "5abb4138b4609a0400b9f659",//KCASGRI0
        "5abb3f14b4609a0400b9f63d", //KRACCFRE
        "5a9e969bfdceec04005ad592", //KRACCFIL
        "5abb3f2cb4609a0400b9f63e", //MINUTERIA
    ],
    "11_12_9016": [
       "5a9e6d9960218204000d685b", // KSCRAL
        "5a9e78e585347d04003f233f", //KSDIS
        "5a9e786e85347d04003f2339", //KSAB1
        "5a9e78aa85347d04003f233c", //KSAI1
        "5abb3eb7b4609a0400b9f63b", //KDISTB1
        "5a9ea8fdfdceec04005ad5bb", //KSSNO1
        "5a9ea87ffdceec04005ad5ba", //KSPS1
        "5a9e82e4a29ce004003c3f58", //KNOT1
        "5abb402fb4609a0400b9f645", //KCAS1
        "5abb410cb4609a0400b9f654", //KCASGRI1
        "5abb3f14b4609a0400b9f63d", //KRACCFRE
        "5a9e969bfdceec04005ad592", //KRACCFIL
        "5abb3f2cb4609a0400b9f63e", //MINUTERIA
    ],
    "11_12_9006": [
        "5a9e6d9960218204000d685b", // KSCRAL
        "5a9e78e585347d04003f233f", //KSDIS
        "5a9e788485347d04003f233b", //KSAB0
        "5a9e78be85347d04003f233e", //KSAI0
        "5abb93256ce9a604006f99f5", //KDISTB0
        "5a9ea91ffdceec04005ad5bd", //KSSNO0
        "5a9eaa0cfdceec04005ad5cb", //KSPS0
        "5a9e8313a29ce004003c3f5a", //KNOT0
        "5abb406db4609a0400b9f647", //KCAS0
        "5abb4138b4609a0400b9f659", //KCASGRI0
        "5abb3f14b4609a0400b9f63d", //KRACCFRE
        "5a9e969bfdceec04005ad592", //KRACCFIL
        "5abb3f2cb4609a0400b9f63e", //MINUTERIA
    ],
    "11_12_other": [
        "5a9e6d9960218204000d685b", // KSCRAL
        "5a9e78e585347d04003f233f", //KSDIS
        "5a9e787985347d04003f233a", //KSAB2
        "5a9e78b485347d04003f233d", //KSAI2
        "5abb3eccb4609a0400b9f63c", //KDISTB2
        "5a9ea90dfdceec04005ad5bc", //KSSNO2
        "5a9fa843fdceec04005ad688", //
        "5a9e82f7a29ce004003c3f59", //KNOT2
        "5abb749b6ce9a604006f99a3", //KCASRAL
        "5abb74696ce9a604006f99a2", //KCASGRIRAL
        "5abb3f14b4609a0400b9f63d", //KRACCFRE
        "5a9e969bfdceec04005ad592", //KRACCFIL
        "5abb3f2cb4609a0400b9f63e", //MINUTERIA
    ],
    "9_P":"5abb69106ce9a604006f998c",
    "12_P":"5abb69106ce9a604006f998c",
}
mapCorpoQt=1

//////
//Adattatore Global
////// 
mapAdattatore = {
    "8_9_1":"5abb423eb4609a0400b9f663", //9605A111 
    "8_9_2":"5abb424ab4609a0400b9f664", //9605A112 
    "8_9_0":"5abb424ab4609a0400b9f664", // 9605A112
    "11_12_9016":"5abb423eb4609a0400b9f663", //9605A111 
    "11-12-other":"5abb424ab4609a0400b9f664"//9605A112 
}
mapAdattatoreQt=1

//////
//Ottica
//////
mapOttica = {
    "M":"5a9e6a0860218204000d684d",  //KSL38
    "F":"5a9e69fd60218204000d684c",  //KSL24
    "T":"5a9e6a2060218204000d684e" //KSR60
}

mapOtticaQt=1

mapLed = {
    
    "5":"5a9e9f3afdceec04005ad5a1",
    "6":"5a9e9f5dfdceec04005ad5a2",
    "7":"5a9e9fb0fdceec04005ad5a5",
    "R":"5a9e9f05fdceec04005ad5a0",
    "Z":"5a9e9f86fdceec04005ad5a3",
    "8":"5a9e9fd3fdceec04005ad5a7",
    "Q":"5819c0e4d5718e690d56a92d",
    "W":"592ed840b48023040043146d",
   
},
mapLedQt=1,

mapDriver= {
    "8":"5ab8f0fe4798ca0400a4897b", //400126127486
    "9_D":"5ab8f1ba4798ca0400a4897c", //400126127492
    "9_P":"5ab8f1ba4798ca0400a4897c", //400126127492
  
    "11":"5ab8f0fe4798ca0400a4897b", //400126127486
    "12_D":"5ab8f1ba4798ca0400a4897c", //400126127492
    "12_P":"5ab8f1ba4798ca0400a4897c", //400126127492
},
mapDriverQt=1
 


mongoose.connection.once('open', function()
{
    console.log("Connessione aperta, parte il batch.");

    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("573436d4c0f52e030063bb3d"), // 2001
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }  
        // , codice:'20015BM2'
    };

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
     
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)
       
        prodotti =  _.filter(_.clone(prodotti), function(obj){
            return (
                    (obj.codice.length==8 || (obj.codice.length==9 && (obj.codice.substring(8,9)=='D' || obj.codice.substring(8,9)=='P')))   ||
                    (obj.codice.length==11 || (obj.codice.length==12 && (obj.codice.substring(11,12)=='D' || obj.codice.substring(11,12)=='P')))
                )
        })

       // prodotti = (_.clone(prodotti).slice(50, 100));
         
        async.each(prodotti, 
            
            function(prodotto, cb)
            { 
                var pClone = _.clone(prodotto)

                var componenti = []; 
                pClone.componenti = [];

                var corpi = [], pushM = null, adattatore=null, driver=null;
                if (pClone.codice.length==8 || pClone.codice.length==9){
                    corpi=_.clone(mapCorpo['8_9_'+pClone.codice.substring(7,8)]);  
                    
                    adattatore=_.clone(mapAdattatore["8_9_"+pClone.codice.substring(7,8)])
                    
                    if (pClone.codice.length==8)
                        driver = _.clone(mapDriver["8"])
 
                    if (pClone.codice.length=9){
                        pushM = _.clone(mapCorpo['9_'+pClone.codice.substring(8,9)]);

                        driver = _.clone(mapDriver[pClone.codice.length+"_"+pClone.codice.substring(8,9)])
                    }

                }
                if (pClone.codice.length==11 || pClone.codice.length==12){
                    corpi=_.clone(mapCorpo['11_12_'+pClone.codice.substring(7,11)]);  
                    if (!corpi){
                        corpi=_.clone(mapCorpo["11_12_other"]); 
                    }

                    adattatore=_.clone(mapAdattatore["11_12_"+pClone.codice.substring(7,11)])
                   
                    if (!adattatore){
                        adattatore=_.clone(mapAdattatore["11-12-other"]); 
                    }
                   
                    if (pClone.codice.length==11)
                        driver = _.clone(mapDriver["11"])

                    if (pClone.codice.length=12){
                        pushM = _.clone(mapCorpo['12_'+pClone.codice.substring(11,12)])

                        driver = _.clone(mapDriver[pClone.codice.length+"_"+pClone.codice.substring(11,12)])
                    }
                }
                 
                if (pushM){
                    corpi.push(pushM);
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ 
                if (corpi){
                    corpi.forEach(function(element){
                        
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: mapCorpo,
                                _id: doc._id
                            }
                        )
                    })
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //ADATTATORE
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

                if (adattatore){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(adattatore)
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Adattatore',
                            qt: mapAdattatoreQt,
                            _id: doc._id
                        }
                    )
                }
             

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //OTTICA
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

            doc = new Componente({
                _id: new mongoose.Types.ObjectId(mapOttica[pClone.codice.substring(6,7)])
            });
            componenti.push(
                {
                    tipoAssociazione:'Ottica',
                    qt: mapOtticaQt,
                    _id: doc._id
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //LED
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

            doc = new Componente({
                _id: new mongoose.Types.ObjectId(mapLed[pClone.codice.substring(4,5)])
            });
            componenti.push(
                {
                    tipoAssociazione:'Led',
                    qt: mapLedQt,
                    _id: doc._id
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //DRIVER
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            if (driver){
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(driver)
                });
                componenti.push(
                    {
                        tipoAssociazione:'Driver', 
                        qt: mapLedQt,
                        _id: doc._id
                    }
                )
            }

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //MONTAGGIO
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Montaggio',
                    qt: 1,
                    _id: doc._id //M0608
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //CARTONE
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['cartone'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Imballo',
                    qt: 1,
                    _id: doc._id
                }
            ) 

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //sacchetto
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Imballo',
                    qt: 1,
                    _id: doc._id
                }
            )
            
            if (componenti.length){

                console.log(aggiornati, pClone.codice, componenti.length)
                            
                var update = {};
                update.$set = {};
                
                update.$set.componenti = componenti;
                update.$set.distintabase = "china_2";
                    
                bulk.find({_id: pClone._id}).updateOne(update);

                aggiornati++;
                

            }

          

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  
console.log(result)
                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
