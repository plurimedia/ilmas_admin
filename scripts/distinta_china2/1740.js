/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/1740.js;
*/
//CORPO/OTTICA/MODULO LED (SE C'è)/LED/ADATTATORE(SE C'è)/DRIVER/MONTAGGIO/IMBALLO(SE C'è)

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
       
    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),

    aggiornati = 0;
    mongoose.connect(db); 

//////
//COMPONENTI STANDARD
//////
mapCorpo =  {
    "1":[
            {
                "id":"5ad4a49396c88604001195a9",//1710B2
                "qt":"1"
            },  
            {
                "id":"5ace1fd0504c990400f041fc",//1610FX
                "qt":"1"
            },  
            {
                "id":"5a816d487783950400521395",//0608A1
                "qt":"3"
            },
            {
                "id":"5a952cd8b638e50400d8fb1e",//RACCBS
                "qt":"3"
            },
            {
                "id":"5ad9b191a80ab604004a012e",//1708BS1
                "qt":"3"
            }  
        ],
        "2":[
            {
                "id":"5ad4a49396c88604001195a9",//1710B2
                "qt":"1"
            },  
            {
                "id":"5ace1fd0504c990400f041fc",//1610FX
                "qt":"1"
            },  
            {
                "id":"5a816d657783950400521397",//0608A2
                "qt":"3"
            },
            {
                "id":"5a952cd8b638e50400d8fb1e",//RACCBS
                "qt":"3"
            },
            {
                "id":"5ad9b1d0a80ab604004a012f",//1708BS2
                "qt":"3"
            }  
        ],
    "0":[
        {
            "id":"5ad4a49396c88604001195a9",//1710B2
            "qt":"1"
        },  
        {
            "id":"5ace1fd0504c990400f041fc",//1610FX
            "qt":"1"
        },  
        {
            "id":"5a816d7c7783950400521398",//0608A0
            "qt":"3"
        },
        {
            "id":"5a952cd8b638e50400d8fb1e",//RACCBS
            "qt":"3"
        },
        {
            "id":"5ad9b1d0a80ab604004a012f",//1708BS2
            "qt":"3"
        }  
    ],
    "9":[
        {
            "id":"5ad4a48696c88604001195a8",//1710B1
            "qt":"1"
        },  
        {
            "id":"5ace1fd0504c990400f041fc",//1610FX
            "qt":"1"
        },  
        {
            "id":"5a816d487783950400521395",//0608A1
            "qt":"3"
        },
        {
            "id":"5a952cd8b638e50400d8fb1e",//RACCBS
            "qt":"3"
        },
        {
            "id":"5ad9b191a80ab604004a012e",//1708BS1
            "qt":"3"
        }   
    ]
}  


F111 = require('./componentiF111'),
F112 = require('./componentiF112'),


mapLed = require('./ledAllModel'),
mapLedQt=2

mapLampada = {
    "2XF":"5a819a44a01985040021f398",   //95002A0
    "2XN":"5a819a57a01985040021f399",   //95002B0
    "3XF":"5a81bb6da01985040021f44b",   //95003A0
    "3XN":"5a81bb7ea01985040021f44c",   //9500BA0
    "2YF":"5a81bba5a01985040021f44f",   //95002D0
    "2YN":"5a81bbd4a01985040021f451",   //95002E0
    "3YF":"5a81bc58a01985040021f45b",   //95003D0
    "3YN":"5a81bc67a01985040021f45c",   //95003E0
    "1FF":"5a82cfabf10c4d0400818178",   //95001M0
    "1FN":"5a82d04ef10c4d0400818181",   //95001N0
    "2FF":"5a82d025f10c4d040081817e",   //95002H0
    "2FN":"5a82d0a1f10c4d0400818185",   //95002K0
    "3FF":"5a82d0d9f10c4d0400818189",   //95003J0
    "3FN":"5a82d0f8f10c4d040081818c",   //95003L0
    "7WF":"5a82d15ef10c4d040081818f",   //01001760545A
    "7WN":"5a82d18cf10c4d0400818193",   //01001760053A
    "8WF":"5a82d1b5f10c4d0400818195",   //01001760522A
    "8WN":"5a82d1dcf10c4d0400818196",   //01001760692A
    "7LF":"5a82d239f10c4d0400818197",   //01001315197A
    "8LF":"5a82d263f10c4d0400818198"    //01001315199A
},
mapLampadaQt=2
 


///////
//DRIVER
//////
mapDriver = {
    "9_F11_E":"5abcdc35840de104005bc873", //400126127484
    "9_F11_D":"5aafa093a30ad1040093ae01", //400126127490
    "9_F11_L":"5aafa093a30ad1040093ae01", //400126127490

    "9_1F":"5aafc476a30ad1040093ae6c", //9000E09
    "9_2F":"5aafc476a30ad1040093ae6c", //9000E09
    "9_3F":"5aafc476a30ad1040093ae6c", //9000E09
} 
driverQt=2




mongoose.connection.once('open', function()
{
        console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("56b8aaa90de23c95600bd272"), //	1740
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }
    };
     

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                pClone.componenti = [];
                
                var componenti = [];

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var corpi = []
                corpi=_.clone(mapCorpo[pClone.codice.substring(7,8)]);  
                
                if (corpi){
                    corpi.forEach(function(element){
                        
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element.id)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: element.qt,
                                _id: doc._id
                            }
                        )
                    })
                } 
                if (pClone.moduloled && pClone.moduloled.startsWith('F111'))
                {
    
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(F111.mapCorpo1[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 2,
                            _id: doc._id
                        }
                    )
    
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(F111.mapCorpo2[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 2,
                            _id: doc._id
                        }
                    )
    
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(F111.mapOttica1[(pClone.codice.substring(6,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 2,
                            _id: doc._id
                        }
                    )
    
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(F111.mapOttica2[(pClone.codice.substring(6,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 2,
                            _id: doc._id
                        }
                    )
     
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 2,
                            _id: doc._id 
                        }
                    )
                }
                else if (pClone.moduloled && pClone.moduloled.startsWith('F112'))
                { 
                  
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(F112.mapCorpo1[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt:2,
                            _id: doc._id
                        }
                    )
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(F112.mapCorpo2[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 2,
                            _id: doc._id
                        }
                    )
    
                    if (F112.mapOttica1[(pClone.codice.substring(6,7))]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(F112.mapOttica1[(pClone.codice.substring(6,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 2,
                            _id: doc._id
                        }
                    )
                    }
                
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 2,
                            _id: doc._id 
                        }
                    )
                } 
                else if (mapLampada[(prodotto.codice.substring(4,7))]){
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    //LED
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    if (mapLed[pClone.codice.substring(4,5)]){
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(mapLed[pClone.codice.substring(4,5)])
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Lampada Led',
                                qt: mapLedQt,
                                _id: doc._id
                            }
                        )
                    }
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //DRIVER
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var chiave = pClone.codice.length + "_";
                if (pClone.moduloled)
                    chiave =   chiave + pClone.moduloled.substring(0,3) + "_"
                chiave = chiave+pClone.codice.substring(8,9);

                driver = mapDriver[chiave]
                if (!driver){
                    chiave = pClone.codice.length + "_"+pClone.codice.substring(4,6);
                    driver = mapDriver[chiave];
                }
                if (driver){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapDriver[chiave])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Driver',
                                qt: driverQt,
                                _id: doc._id
                        }
                    ) 
                }

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //MONTAGGIO
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Montaggio',
                    qt: 1,
                    _id: doc._id 
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //CARTONE
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['cartone'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Imballo',
                    qt: 1,
                    _id: doc._id
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //sacchetto
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Imballo',
                    qt: 1,
                    _id: doc._id
                }
            )

                if (componenti.length){

                    console.log(aggiornati, pClone.codice)
                                
                    var update = {};
                    update.$set = {};
                    
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_2";
                        
                    bulk.find({_id: pClone._id}).updateOne(update);

                    aggiornati++;
 
                }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
