/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/2019.js;
*/
//CORPO/OTTICA/MODULO LED (SE C'è)/LED/ADATTATORE(SE C'è)/DRIVER/MONTAGGIO/IMBALLO(SE C'è)

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
       
    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),

    aggiornati = 0;
    mongoose.connect(db); 

//////
//COMPONENTI STANDARD
//////
mapCorpo =  {
    "11_9016":[
        {
            "id":"5af30ab869a7f80400329a09",//KSPLUSC1
            "qt":"1"
        },  
        {
            "id":"5a9fc48cfdceec04005ad6c1",//KLDIS
            "qt":"1"
        },  
        {
            "id":"5a9fc46afdceec04005ad6bf",//KLAB1
            "qt":"1"
        },
        {
            "id":"5a9fc273fdceec04005ad6b0",//KLAI1
            "qt":"1"
        },  
        {
            "id":"5af0440aec11ff040073a4fd",//KDISTP1
            "qt":"1"
        },  
        {
            "id":"5a9ea8fdfdceec04005ad5bb",//KLSNO1
            "qt":"1"
        },
        {
            "id":"5a9fc8c6fdceec04005ad6d2",//KLPS1
            "qt":"1"
        },  
        {
            "id":"5a9e82e4a29ce004003c3f58",//KNOT1
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//KMINUTERIA
            "qt":"1"
        }
    ],
    "11_9006":[
        {
            "id":"5af30ae569a7f80400329a0b",//KSPLUSC0
            "qt":"1"
        },  
        {
            "id":"5a9fc48cfdceec04005ad6c1",//KLDIS
            "qt":"1"
        },  
        {
            "id":"5a9fc45efdceec04005ad6be",//KLAB0
            "qt":"1"
        },
        {
            "id":"5a9e78be85347d04003f233e",//KSAI0
            "qt":"1"
        },  
        {
            "id":"5af311a069a7f80400329a27",//KDISTP0
            "qt":"1"
        },  
        {
            "id":"5a9fcc2efdceec04005ad6eb",//KLSNO0
            "qt":"1"
        },
        {
            "id":"5a9fc90bfdceec04005ad6d4",//KLPS0
            "qt":"1"
        },  
        {
            "id":"5a9e8313a29ce004003c3f5a",//KNOT0
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFILL
            "qt":"1"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"1"
        } 
    ],
    "11_9005":[
        {
            "id":"5af30c8169a7f80400329a15",//KLPLUSC2
            "qt":"1"
        },  
        {
            "id":"5a9fc48cfdceec04005ad6c1",//KLDIS
            "qt":"1"
        },  
        {
            "id":"5a9fc474fdceec04005ad6c0",//KLAB2
            "qt":"1"
        },
        {
            "id":"5a9fc281fdceec04005ad6b1",//KLAI2
            "qt":"1"
        },  
        {
            "id":"5af3116a69a7f80400329a25",//KDISTP2
            "qt":"1"
        },  
        {
            "id":"5a9fcc12fdceec04005ad6ea",//KLSNO2
            "qt":"1"
        },
        {
            "id":"5a9fc8eefdceec04005ad6d3",//KLPS2
            "qt":"1"
        },  
        {
            "id":"5a9e82f7a29ce004003c3f59",//KNOT2
            "qt":"1"
        }
        ,  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFILL
            "qt":"1"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"1"
        } 
    ],
    "11_other":[
        {
            "id":"5af30b1069a7f80400329a0c",//KLPLUSCRAL
            "qt":"1"
        },  
        {
            "id":"5a9fc48cfdceec04005ad6c1",//KLDIS
            "qt":"1"
        },  
        {
            "id":"5a9fc474fdceec04005ad6c0",//KLAB2
            "qt":"1"
        },
        {
            "id":"5a9fc273fdceec04005ad6b0",//KLAI1
            "qt":"1"
        },  
        {
            "id":"5af3116a69a7f80400329a25",//KDISTP2
            "qt":"1"
        },  
        {
            "id":"5a9fcc12fdceec04005ad6ea",//KLSNO2
            "qt":"1"
        },
        {
            "id":"5a9fc8eefdceec04005ad6d3",//KLPS2
            "qt":"1"
        },  
        {
            "id":"5a9e82f7a29ce004003c3f59",//KNOT2
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"1"
        } 
    ],
    "10":[
        {
            "id":"5af30b6369a7f80400329a0d",//KLPLUSCGALV
            "qt":"1"
        },  
        {
            "id":"5a9fc48cfdceec04005ad6c1",//KLDIS
            "qt":"1"
        },  
        {
            "id":"5a9fc474fdceec04005ad6c0",//KLAB2
            "qt":"1"
        },
        {
            "id":"5a9fc273fdceec04005ad6b0",//KLAI1
            "qt":"1"
        },  
        {
            "id":"5af3116a69a7f80400329a25",//KDISTP2
            "qt":"1"
        },  
        {
            "id":"5a9fcc12fdceec04005ad6ea",//KLSNO2
            "qt":"1"
        },
        {
            "id":"5a9fc8eefdceec04005ad6d3",//KLPS2
            "qt":"1"
        },  
        {
            "id":"5a9e82f7a29ce004003c3f59",//KNOT2
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        },  
        {
            "id":"5abb3f2cb4609a0400b9f63e",//MINUTERIA
            "qt":"1"
        } 
    ]
}

mapOttica = {
    "B":"5a9fc2e8fdceec04005ad6b3", //KLL12
    "M":"5a9fc302fdceec04005ad6b6", //KLL38
    "F":"5a9e69fd60218204000d684c", //KSL24
    "T":"5a9e6a2060218204000d684e"  //KSR60
 }
 mapOtticaQt=1

 mapLed={
    "5":"5915b5ba5c28050400609f67",
    "6":"5915b60c5c28050400609f6b",
    "7":"5915b64d5c28050400609f6e",
    "R":"592eb14fb480230400431447",
    "Z":"592eb17db480230400431448",
    "8":"5915b68a5c28050400609f70",
    "Q":"5819c0e4d5718e690d56a92d",
    "W":"592ed840b48023040043146d"
 }
mapLedQt=1
 
mapAdattatore = {
    "12_9016":"5abb423eb4609a0400b9f663",   //9605A111  
    "other":"5abb424ab4609a0400b9f664"  //9605A112
   
 }
 mapAdattatoreQt=1

 mapDriver =  "5ab8f0fe4798ca0400a4897b",   //400126127486  
    
 mapDriverQt=1

mongoose.connection.once('open', function()
{
        console.log("Connessione aperta, parte il batch."); /// this gets printed to console


    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("58247969de6dd4040049c3b4"), //	2019
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }
        //,codice:'20118CT9006D'
    };
     

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                pClone.componenti = [];
                
                var componenti = [];

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var corpi = [], chiave="";

                chiave = pClone.codice.length;
                if (mapCorpo[chiave]==null){
                    chiave = "11_" + pClone.codice.substring(7,11)
                }

                if (mapCorpo[chiave]==null){
                    chiave = "11_other"
                } 

                corpi=_.clone(mapCorpo[chiave]);
                
                if (corpi){
                    corpi.forEach(function(element){
                        
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element.id)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: element.qt,
                                _id: doc._id
                            }
                        )
                    })
                } 

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //OTTICA
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (mapOttica[pClone.codice.substring(6,7)]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapOttica[pClone.codice.substring(6,7)])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Ottica',
                            qt: mapOtticaQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //LED
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (mapLed[pClone.codice.substring(4,5)]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapLed[pClone.codice.substring(4,5)])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Led',
                            qt: mapLedQt,
                            _id: doc._id
                        }
                    )
                }

                 //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //ADATTATORE
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var chiave=chiave="12_"+pClone.codice.substring(11,12) 
                if (!mapAdattatore[chiave]){
                   chiave="other" 
                }
                if (mapAdattatore[chiave]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapAdattatore[chiave])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Adattatore',
                            mapDriverQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //DRIVER
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
             
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(mapDriver)
                });
                componenti.push(
                    {
                        tipoAssociazione:'Driver',
                        mapDriverQt,
                        _id: doc._id
                    }
                )

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //MONTAGGIO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Montaggio',
                        qt: 1,
                        _id: doc._id
                    }
                )

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //IMBALLO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['cartone'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'imballo',
                        qt: 1,
                        _id: doc._id
                    }
                )
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'imballo',
                        qt: 1,
                        _id: doc._id
                    }
                )

                if (componenti.length){

                    console.log(aggiornati, pClone.codice)
                                
                    var update = {};
                    update.$set = {};
                    
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_2";
                        
                    bulk.find({_id: pClone._id}).updateOne(update);

                    aggiornati++;
 
                }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
