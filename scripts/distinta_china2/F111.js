/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/F111.js;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),

    mapLed = require('./ledAllModel'),
     cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),

    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mapCorpo1 = {
    "1":"5a82eb5af10c4d04008181b1",   //f111c1
    "0":"5a82eca41879400400cbb3f8",   //f111c0
    "2":"5a82ec8b1879400400cbb3f7"   //f111c2
}
mapCorpo2 = {
    "1":"5a82edd51879400400cbb3fe",  //F111A1
    "0":"5a82ee161879400400cbb400",  //F111A0
    "2":"5a82ee021879400400cbb3ff"   //F111A2
}
mapOttica1 = {
    "D":"5a82f055cf845a0400031566",   //f111r17
    "H":"5a82f05ecf845a0400031567",   //f111r26
    "N":"5a82f06acf845a0400031568",   //f111r40
    "U":"5a82f06acf845a0400031568"   //f111r40
}
mapOttica2 = {
    "D":"5a82ef001879400400cbb402",  //f111v
    "H":"5a82ef001879400400cbb402",  //f111v
    "N":"5a82ef001879400400cbb402",   //f111v
    "U":"5a82ef0c1879400400cbb403"   //f111vs
}


mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch.");

    var query = { generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: { $in: [
            ObjectId("56b8aaa90de23c95600bd225") //F111
        ] },
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }
    };

   console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err,prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        console.log("prodotti.length", prodotti.length)

        async.each(prodotti,

            function(prodotto,cb){

            if (prodotto.codice ){

                    console.log(prodotto.codice)

                    var componenti = [];

                    var pClone = _.clone(prodotto);

                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    //CORPO
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapCorpo1[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Corpo',
                                qt: 1,
                                _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapCorpo2[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Corpo',
                                qt: 1,
                                _id: doc._id
                        }
                    )

                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    //OTTICA
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapOttica1[(pClone.codice.substring(6,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Ottica',
                                qt: 1,
                                _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapOttica2[(pClone.codice.substring(6,7))])
                    });
                    componenti.push(
                        {
                                tipoAssociazione:'Ottica',
                                qt: 1,
                                _id: doc._id
                        }
                    )

                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                    //LED
                    //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapLed[(pClone.codice.substring(4,5))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Led',
                                qt: 1,
                                _id: doc._id
                        }
                    )

                  //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //MONTAGGIO
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Montaggio',
                    qt: 1,
                    _id: doc._id //M0608
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //CARTONE
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['cartone'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Imballo',
                    qt: 1,
                    _id: doc._id
                }
            )

            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            //sacchetto
            //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
            doc = new Componente({
                _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
            });
            componenti.push(
                {
                    tipoAssociazione:'Imballo',
                    qt: 1,
                    _id: doc._id
                }
            )

                    aggiornati++;

                    var update = {};
                    update.$set = {};
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_2";
                    bulk.find( { _id: pClone._id } ).updateOne(update);
                }

            cb();
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0  ) {

                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })

            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })
    })
});
