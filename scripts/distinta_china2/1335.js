/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/1335.js;
*/
//CORPO/OTTICA/MODULO LED (SE C'è)/LED/ADATTATORE(SE C'è)/DRIVER/MONTAGGIO/IMBALLO(SE C'è)

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
       

    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),

    aggiornati = 0;
    mongoose.connect(db); 

//////
//COMPONENTI STANDARD
//////
mapCorpo =  {
    "G1":[
            {
                "id":"5819ba09e234a6b60cc9a43b",//1331XXG1
                "qt":"1"
            },  
            {
                "id":"5a9e82e4a29ce004003c3f58",//KNOT1
                "qt":"1"
            },  
            {
                "id":"5a9e969bfdceec04005ad592",//KRACCFIL
                "qt":"1"
            }
        ],
    "G2":[
        {
            "id":"5819ba09e234a6b60cc9a43c",//1331XXG2
            "qt":"1"
        },  
        {
            "id":"5a9e82f7a29ce004003c3f59",//KNOT2
            "qt":"1"
        } ,  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        }
    ],
    "G0":[
        {
            "id":"5ad06eceafa9b70400ee5f44",//1331XXG0
            "qt":"1"
        },  
        {
            "id":"5a9e82f7a29ce004003c3f59",//KNOT2
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        }
    ]  ,
    "J1":[
        {
            "id":"5819ba09e234a6b60cc9a43d",//1331XXJ1
            "qt":"1"
        },  
        {
            "id":"5a9e82e4a29ce004003c3f58",//KNOT1
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        }
    ]  ,
    "J2":[
        {
            "id":"5819ba09e234a6b60cc9a43e",//1331XXJ2
            "qt":"1"
        },  
        {
            "id":"5a9e82f7a29ce004003c3f59",//KNOT2
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        }
    ] ,
    "J0":[
        {
            "id":"5ad06f65afa9b70400ee5f45",//1331XXJ0
            "qt":"1"
        },  
        {
            "id":"5a9e82f7a29ce004003c3f59",//KNOT2
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        }
    ]
    ,
    "R1":[
        {
            "id":"5819ba09e234a6b60cc9a43f",//1331XXR1
            "qt":"1"
        },  
        {
            "id":"5a9e82e4a29ce004003c3f58",//KNOT1
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        }
    ]  ,
    "R2":[
        {
            "id":"5819ba09e234a6b60cc9a440",//1331XXR2
            "qt":"1"
        },  
        {
            "id":"5a9e82f7a29ce004003c3f59",//KNOT2
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        }
    ] ,
    "R0":[
        {
            "id":"5ad06fe0afa9b70400ee5f46",//1331XXR0
            "qt":"1"
        },  
        {
            "id":"5a9e82f7a29ce004003c3f59",//KNOT2
            "qt":"1"
        },  
        {
            "id":"5a9e969bfdceec04005ad592",//KRACCFIL
            "qt":"1"
        }
    ]
}  

mapLed=
{
    "I":"5819bee7d5718e690d56a920",
    "A":"5819bf2ad5718e690d56a921",
    "B":"5819bf5fd5718e690d56a922",
    "O":"5819c112d5718e690d56a92f",
    "S":"5819c130d5718e690d56a930",
    "T":"5819c159d5718e690d56a931",
    "Q":"5819c0e4d5718e690d56a92d",
    "W":"592ed840b48023040043146d"
}
mapLedQt="1";
 
mapAdattatore = {
    "B_1":"5a9ea0c3fdceec04005ad5a9",  //400126127477
    "B_2":"5a9e9d7dfdceec04005ad59a",  //400126127478
    "B_0":"5a9e9d7dfdceec04005ad59a",  //400126127478 

    "C_1":"5aaa7e80fc42390400dbcb25",  //400126127474
    "C_2":"5aaa7de9a97d4f04009548c4",  //400126127475
    "C_0":"5aaa7de9a97d4f04009548c4",  //400126127475 

    "D_1":"5a9d1a4c2bfffb04000f1c5c",  //400126127474
    "D_2":"5aaa7de9a97d4f04009548c4",  //400126127475
    "D_0":"5aaa7de9a97d4f04009548c4",  //400126127475

 }
mapAdattatoreQt=1


mongoose.connection.once('open', function()
{
        console.log("Connessione aperta, parte il batch."); /// this gets printed to console


    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("594b8a0f21ef9f04004e4170"), //	1335
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }
    };
     

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                pClone.componenti = [];
                
                var componenti = [];

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var corpi = []
                corpi=_.clone(mapCorpo[pClone.codice.substring(6,8)]);  

                if (corpi){
                    corpi.forEach(function(element){
                        
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element.id)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: element.qt,
                                _id: doc._id
                            }
                        )
                    })
                } 

                
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //LED
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (mapLed[pClone.codice.substring(4,5)]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapLed[pClone.codice.substring(4,5)])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Led',
                            qt: mapLedQt,
                            _id: doc._id
                        }
                    )
                }
            
                 

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //ADATTATORE
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var chiave=pClone.codice.substring(4,5)+"_"+pClone.codice.substring(7,8);
                if (mapAdattatore[chiave]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapAdattatore[chiave])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Adattatore',
                            qt:mapAdattatoreQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //MONTAGGIO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Montaggio',
                        qt: 1,
                        _id: doc._id
                    }
                )
                 

                if (componenti.length){

                    console.log(aggiornati, pClone.codice)
                                
                    var update = {};
                    update.$set = {};
                    
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_2";
                        
                    bulk.find({_id: pClone._id}).updateOne(update);

                    aggiornati++;
 
                }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
