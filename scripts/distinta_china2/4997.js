/*
LANCIARE LA PROCEDURA
    node --max-old-space-size=5000 scripts/distinta_china2/4997.js;
*/
//CORPO/OTTICA/MODULO LED (SE C'è)/LED/ADATTATORE(SE C'è)/DRIVER/MONTAGGIO/IMBALLO(SE C'è)

var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ObjectId = require('mongodb').ObjectID,

    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),

    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),

    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
       
    cartone_sacchetto_montaggioAll = require('./cartone_sacchetto_montaggioAllmodel'),
    F111 = require('./componentiF111'),
    F112 = require('./componentiF112'),

    aggiornati = 0;
    mongoose.connect(db); 

//////
//COMPONENTI STANDARD
//////
mapCorpo =  {
    "1":[
        {
            "id":"5afaf13c0fa1160400dd22f7",//4997C1
            "qt":"1"
        },

        {
            "id":"5afaf0780fa1160400dd22f0",//4996R1
            "qt":"1"
        },

        {
            "id":"5afd4cb5fbc8b60400e72df4",//CSOSP1
            "qt":"1"
        },
         
        
    ],
    "0":[
        {
            "id":"5afaf2100fa1160400dd22fa",//4997C0
            "qt":"1"
        }, 
        {
            "id":"5afaf09d0fa1160400dd22f2",//4996R0
            "qt":"1"
        }, 
        {
            "id":"5afd4ce2fbc8b60400e72df5",//CSOSP2
            "qt":"1"
        }  
        
    ],
    "2":[
        {
            "id":"5afaf1fa0fa1160400dd22f9",//4997C2
            "qt":"1"
        },
        , 
        {
            "id":"5afaf0890fa1160400dd22f1",//4996R2
            "qt":"1"
        }, 
        {
            "id":"5afd4ce2fbc8b60400e72df5",//CSOSP2
            "qt":"1"
        }  
        
    ]
}

mapCorpo2 =  {
    "C1":[
        {
            "id":"5819ba09e234a6b60cc9a3c9",//0797XXC1
            "qt":"1"
        }
    ],
    "C2":[
        {
            "id":"5819ba09e234a6b60cc9a3ca",//0797XXC2
            "qt":"1"
        }
    ]
    ,
    "C0":[
        {
            "id":"5819ba09e234a6b60cc9a3ca",//0797XXC2
            "qt":"1"
        }
    ],
    "K1":[
        {
            "id":"5819ba09e234a6b60cc9a3cc",//0797XXK1
            "qt":"1"
        }
    ],
    "K2":[
        {
            "id":"5819ba09e234a6b60cc9a3cd",//0797XXK2
            "qt":"1"
        }
    ],
    "K0":[
        {
            "id":"5819ba09e234a6b60cc9a3cb",//0797XXK0
            "qt":"1"
        }
    ],
    "R1":[
        {
            "id":"5819ba09e234a6b60cc9a3cf",//0797XXR1
            "qt":"1"
        }
    ],
    "R2":[
        {
            "id":"5819ba09e234a6b60cc9a3d0",//0797XXR2
            "qt":"1"
        }
    ],
    "R0":[
        {
            "id":"5819ba09e234a6b60cc9a3ce",//0797XXR0
            "qt":"1"
        }
    ],

    
}

 
mapOttica = {};
mapLampada = {};

 mapLed={
    "I":"5819bee7d5718e690d56a920",
    "A":"5819bf2ad5718e690d56a921",
    "B":"5819bf5fd5718e690d56a922",
    "O":"5819c112d5718e690d56a92f",
    "S":"5819c130d5718e690d56a930",
    "T":"5819c159d5718e690d56a931",
    "Q":"5819c0e4d5718e690d56a92d",
    "W":"592ed840b48023040043146d"
 }
mapLedQt="1";
 

mapDriver = {
    "E":"5ab8f0fe4798ca0400a4897b",   //400126127486
    "D":"5ab8f1ba4798ca0400a4897c",   //400126127492
    "P":"5ab8f1ba4798ca0400a4897c",   //400126127492
    
 }
 mapDriverQt=1

 


mongoose.connection.once('open', function()
{
        console.log("Connessione aperta, parte il batch."); /// this gets printed to console


    var query = { 
        generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
        modello: ObjectId("56b8aaa90de23c95600bd2c5"), //	4997
        distintabase: { $ne: "china_1" },
        kit: {
            $in: [
                false, null
            ]
        },
        criptato: {
            $in: [
                false, null
            ]
        },
        speciale: {
            $in: [
                false, null
            ]
        }
        //,codice:'20118CT9006D'
    };
     

    console.log("query ", query);

    Prodotto.find(query)
    .lean()
    .exec(function(err, prodotti)
    {
        var bulk = Prodotto.collection.initializeOrderedBulkOp();

        console.log("prodotti.length", prodotti.length)

        async.each(prodotti, function(prodotto, cb)
        {
               
                var pClone = _.clone(prodotto)
                pClone.componenti = [];
                
                var componenti = [];

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //CORPO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var corpi = [], chiave="";

                 
                chiave = pClone.codice.substring(7,8)
                corpi=_.clone(mapCorpo[chiave]);
                 
                if (corpi){
                    corpi.forEach(function(element){
                        
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element.id)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: element.qt,
                                _id: doc._id
                            }
                        )
                    })
                } 
                corpi = [];
                chiave = pClone.codice.substring(6,8)
                corpi=_.clone(mapCorpo2[chiave]); 
                if (corpi){
                    corpi.forEach(function(element){
                        
                        doc = new Componente({
                            _id: new mongoose.Types.ObjectId(element.id)
                        });
                        componenti.push(
                            {
                                tipoAssociazione:'Corpo',
                                qt: element.qt,
                                _id: doc._id
                            }
                        )
                    })
                } 

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //OTTICA
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (mapOttica[pClone.codice.substring(6,7)]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapOttica[pClone.codice.substring(6,7)])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Ottica',
                            qt: mapOtticaQt,
                            _id: doc._id
                        }
                    )
                }

                if (pClone.codice.substring(6,8)=='D1' ||
                    pClone.codice.substring(6,8)=='H1' ||
                    pClone.codice.substring(6,8)=='N1' ||

                    pClone.codice.substring(6,8)=='D2' ||
                    pClone.codice.substring(6,8)=='H2' ||
                    pClone.codice.substring(6,8)=='N2' ||

                    pClone.codice.substring(6,8)=='D0' ||
                    pClone.codice.substring(6,8)=='H0' ||
                    pClone.codice.substring(6,8)=='N0'

                    )
                {

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(F111.mapCorpo1[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(F111.mapCorpo2[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(F111.mapOttica1[(pClone.codice.substring(6,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(F111.mapOttica2[(pClone.codice.substring(6,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id
                        }
                    )
    
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F111',
                            qt: 1,
                            _id: doc._id 
                        }
                    )
                }
                else  
                    if (pClone.codice.substring(6,8)=='B1' ||
                        pClone.codice.substring(6,8)=='F1' ||
                        pClone.codice.substring(6,8)=='M1' ||

                        pClone.codice.substring(6,8)=='B2' ||
                        pClone.codice.substring(6,8)=='F2' ||
                        pClone.codice.substring(6,8)=='M2' ||

                        pClone.codice.substring(6,8)=='B0' ||
                        pClone.codice.substring(6,8)=='F0' ||
                        pClone.codice.substring(6,8)=='M0'
                    )
                {
                
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(F112.mapCorpo1[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 1,
                            _id: doc._id
                        }
                    )
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(F112.mapCorpo2[(pClone.codice.substring(7,8))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 1,
                            _id: doc._id
                        }
                    )

                    if (F112.mapOttica1[(pClone.codice.substring(6,7))]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(F112.mapOttica1[(pClone.codice.substring(6,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 1,
                            _id: doc._id
                        }
                    )
                    }
                
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Modulo F112',
                            qt: 1,
                            _id: doc._id 
                        }
                    )
                } 
                else if (mapLampada[(prodotto.codice.substring(4,7))]){
                    
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapLampada[(pClone.codice.substring(4,7))])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Lampada',
                                qt: mapLampadaQt,
                                _id: doc._id
                        }
                    ) 
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //LED
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                if (mapLed[pClone.codice.substring(4,5)]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapLed[pClone.codice.substring(4,5)])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Led',
                            qt: mapLedQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //DRIVER
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                var chiave=pClone.codice.substring(8,9)  
                if (mapDriver[chiave]){
                    doc = new Componente({
                        _id: new mongoose.Types.ObjectId(mapDriver[chiave])
                    });
                    componenti.push(
                        {
                            tipoAssociazione:'Driver',
                            mapDriverQt,
                            _id: doc._id
                        }
                    )
                }

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //MONTAGGIO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['montaggio'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'Montaggio',
                        qt: 1,
                        _id: doc._id
                    }
                )

                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                //IMBALLO
                //@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['cartone'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'imballo',
                        qt: 1,
                        _id: doc._id
                    }
                )
                doc = new Componente({
                    _id: new mongoose.Types.ObjectId(cartone_sacchetto_montaggioAll['sacchetto'])
                });
                componenti.push(
                    {
                        tipoAssociazione:'imballo',
                        qt: 1,
                        _id: doc._id
                    }
                )

                if (componenti.length){

                    console.log(aggiornati, pClone.codice)
                                
                    var update = {};
                    update.$set = {};
                    
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_2";
                        
                    bulk.find({_id: pClone._id}).updateOne(update);

                    aggiornati++;
 
                }
            

            cb();
        }, function(err)
        {
            if (err){
                console.log(err)
                throw err;
            }

            if (aggiornati > 0)
            {
                bulk.execute(function(result)
                {  

                    mongoose.connection.close();

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                  
                })
            }
            else
            {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })

    })
});
