/*
LANCIARE LA PROCEDURA
node --max-old-space-size=5000 scripts/componenti_china2
*/

var MongoClient = require('mongodb').MongoClient, aggiornati = 0,
    CONFIG = require('../server/config.js'),
    ObjectId = require('mongodb').ObjectID,
    async = require('async');

async function connect()
{
    try {
        let dbc= await MongoClient.connect(CONFIG.MONGO);
        return dbc;
    } catch (e) {
        console.log("Connect error", e);
        throw err;
    }
}

async function getProdotti(db)
{
    try {
        let a = await db.collection("Prodotto").find({ modello: { $in: [ObjectId("59ae6797144c280400213f92"), ObjectId("59ae704a6ce53e0400a8f7f9")] } }, { componenti: 1 }).toArray();
        return a;
    } catch(e) {
        console.log('Errore getProdotti', e);
    }
}

async function process()
{
   let db= await connect();

   let p= await getProdotti(db);

   var bulk= db.collection("Prodotto").initializeUnorderedBulkOp();

   for (let prodotto of p)
   {
       var modifica= false, update;
console.log('elaborazione prodotto', prodotto._id);

       if (prodotto.componenti && prodotto.componenti.length)
       {
           for (let c of prodotto.componenti)
           {
               if (c.tipoAssociazione && c.tipoAssociazione.toLowerCase()==='montaggio')
               {
console.log('elaborazione componente', c._id);
                   c._id= ObjectId('5aa798df2324140400aaaa1e');
                   modifica= true;
               }
           };

           if (modifica)
           {
               bulk.find({ _id: prodotto._id }).updateOne({ $set: { componenti: prodotto.componenti } });
               aggiornati++;
           }
       }
   };

   if (aggiornati > 0)
   {
       var r= await bulk.execute();
       console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti');
       db.close();
   }
   else
       db.close();

   return;
}

process();
