/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/modificamodelli_pubblica;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ModelloSchema = require('../server/models/modello'),
    Modello = mongoose.model('Modello'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    Modello.find().lean()
    .exec(function(err,modelli){

        console.log('aggiorno '+modelli.length+' prodotti')

        var bulk = Modello.collection.initializeOrderedBulkOp();
        async.each(modelli, function(modello,cb){

            var update = {};
 

            console.log("aggiornamento modello id = ", modello.nome);
            
             var p = _.clone(modello); 
 
                update.$set = {};
                update.$set.pubblicato = true;
                bulk.find( { _id: modello._id } ).updateOne(update);
                aggiornati ++; 

                
          
            cb();
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0) {
             
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' modelli')
                    mongoose.connection.close();

                })
               
            }
            else {
                console.log('Non ci sono modelli da aggiornare')
                mongoose.connection.close();
            }

        })
    }) 
}



);
