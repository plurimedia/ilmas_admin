/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/modificaprodotti_pubblica;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    Prodotto.find().lean()
    .exec(function(err,prodotti){ 

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, function(prodotto,cb){

            var update = {}; 
            
            if (prodotto.codice){

            if  (prodotto.codice.indexOf("C&S")===0 || 
                prodotto.codice.indexOf("ILMPZZ")===0 ||
                prodotto.codice.indexOf("AUR")===0 || 
                prodotto.codice.indexOf("DPE")===0 || 
                prodotto.codice.indexOf("RES")===0 || 
                prodotto.codice.indexOf("CLA")===0 || 
                prodotto.codice.indexOf("CPR")===0 || 
                prodotto.codice.indexOf("KAS")===0 || 
                prodotto.codice.indexOf("ALC")===0 || 
                prodotto.codice.indexOf("PEN")===0 || 
                prodotto.codice.indexOf("ZUI")===0 || 
                prodotto.codice.indexOf("MOR")===0 || 
                prodotto.codice.indexOf("TWI")===0 ||
                prodotto.codice.indexOf("CRS")===0 
                ){ 

            console.log("aggiornamento prodotto id = ", prodotto.codice);
            
             var p = _.clone(prodotto); 
 
                update.$set = {};
                update.$set.exportMetel = false;
                update.$set.pubblicato  = false;
                bulk.find( { _id: prodotto._id } ).updateOne(update);
                aggiornati ++; 

                }
            }
          
            cb();
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0) {
             
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
               
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

        })
    }) 
}



);
