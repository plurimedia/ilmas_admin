/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/0608_Bob_Std_Arled_Osram.js;
 */


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    fs      = require('fs'),
    aggiornati = 0,
    logGlobale="";


/*
 LISTINO PREZZI
 */


/*
matrix : matrice dei cambiamenti dei prezzi. Esempio :

var matrix = {
    prezzi : [
        { trig : ["IC", "AC", "BC", "OC", "SC", "TC"], prezzo:97 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:97 },
        { trig : ["IF", "AF", "BF", "OF", "SF", "TF"], prezzo:97 },
        { trig : ["WC"], prezzo:117 },
        { trig : ["WD"], prezzo:117 },
        { trig : ["WF"], prezzo:117 },
        { trig : ["QC"], prezzo:132 },
        { trig : ["QD"], prezzo:132 },
        { trig : ["QF"], prezzo:132 }
    ],
    aumenti : {
        E : [20,29,39,20,29,39,20,29,39],
        D : [60,60,87,60,60,87,60,60,87],
        L : [67,67,87,67,67,87,67,67,87]
    }
}

id_modello : id mongo del modello preso in esame

nome_modello : nome testuale del modello per i log

txt : nome fisico del file (ora inutile)

tag : tag che viene inserito sul record per individuare i prodotti cambiati da un certo file

modulo_led : OPZIONALE, aggiunge il criterio passato nella query come moduloled

check_dgt : posizione IN BASE ZERO e numero di caratteri di confronto per la prima parte della matrice (prezzi, trig). Esempi :

    undefined : prende il 5 e 6 carattere del codice
    4 : prende il quinto carattere
    46 : prende dal quinto al settimo carattere


    lenght_max-->effettua un check sulla lunghezza max del codice e va a sostituire il controllo di default(8 e 9)
  
*/

exports.execute = function (matrix, id_modello, nome_modello, txt, tag, modulo_led, check_dgt, gen_input , lenght_max) {
    console.log(db)

    mongoose.connect(db);

    mongoose.connection.once('open', function () {

            console.log("Connessione aperta, parte il batch "+nome_modello); /// this gets printed to console

            var gen = gen_input;
            if (!gen)
                gen = ['G6', 'D1'];

            var query =
            {
                generazione: {$in: gen},
               
                modello: {
                    $in: [
                        id_modello
                    ]
                }
            }

            if (modulo_led)
                query.moduloled = modulo_led;

            var controlla_caratteri, char_description;

            console.log("query ", query);
            
            if (check_dgt) {
                console.log(check_dgt, check_dgt.length, Array.from(check_dgt));
                if (check_dgt.length == 1) {
                    controlla_caratteri = function (c) {
                        return c.substring(parseInt(Array.from(check_dgt)[0]), parseInt(Array.from(check_dgt)[0])+1);
                    }
                    char_description = parseInt(Array.from(check_dgt)[0])+1+'° car';
                } else if (check_dgt.length == 2) {
                    controlla_caratteri = function (c) {
                        return c.substring(parseInt(Array.from(check_dgt)[0]), parseInt(Array.from(check_dgt)[1]));
                    }
                    char_description = Array.from(check_dgt)[0]+'° e '+Array.from(check_dgt)[1]+'° car';
                }
            } else {
                controlla_caratteri = function (c) {
                    return c.substring(4, 6);
                }
                char_description = '5° e 6° car';
            }



            Prodotto.find(query).lean().sort({codice: 1})
                .exec(
                function (err, prodotti) {

                    var bulk = Prodotto.collection.initializeOrderedBulkOp();

                        var lenghtMax = lenght_max || 9;

                        console.log("lenghtMax", lenghtMax)

                        async.each(prodotti,
                            function (prodotto, cb) {
                                var gruppo = 0;
                                
                                if ((lenght_max && prodotto.codice.length == lenghtMax) || 
                                   (!lenght_max && prodotto.codice.length <= lenghtMax) ){

                                        var p = _.clone(prodotto);

                                        var logStr = "";

                                        var check_digits = controlla_caratteri(p.codice);//p.codice.substring(4, 6);
                                       _.each(matrix.prezzi, function (pr, idx) {
                                            if (_.indexOf(pr.trig, check_digits) > -1) {

                                                if (p.prezzo_old==null){
                                                    p.prezzo_old = _.clone(p.prezzo);
                                                } 

                                                p.prezzo = pr.prezzo;

                                                gruppo = idx + 1;

                                                logStr = _.clone(aggiornati) + " codice " + p.codice + " "+ char_description +" " + check_digits +
                                                    " old_prezzo " + p.prezzo_old + " primo cambio prezzo  " + p.prezzo;

                                                
                                            }
                                        });

                                    if (gruppo > 0) { 

                                        var check_digit = p.codice.substring(8, 9);

                                        if (lenghtMax && prodotto.codice.substring(lenghtMax-1,lenghtMax))
                                            check_digit = p.codice.substring(lenghtMax-1,lenghtMax)

                                        if (check_digit) {
                                            
                                            _.each(_.keys(matrix.aumenti), function (k) {
                                                if (k == check_digit) {
                                                    maggiorazione = matrix.aumenti[k][gruppo - 1];

                                                    p.prezzo = p.prezzo + maggiorazione;

                                                    logStr = logStr + " " + k + " secondo cambio prezzo +  " + maggiorazione + " " + p.prezzo;
                                                }
                                            });

                                        }

                                        logStr = logStr + " gruppo = " + gruppo;

                                        var update = {};
                                        update.$set = {};
                                        update.$set.prezzo = p.prezzo;
                                        update.$set.prezzo_old = p.prezzo_old;
                                        update.$set.mdate = new Date();
                                        update.$set.trascodificaPrezzo = tag

                                        bulk.find({_id: p._id}).updateOne(update);

                                        aggiornati++;

                                        console.log(logStr);

                                        logGlobale = logGlobale + logStr + "\n";
                                    }
                                    
                                }
                                cb();
                            },
                            function (err) {
                                if (err) {
                                    console.log(err)
                                    throw err;
                                }

                                if (aggiornati > 0)  {

                                    bulk.execute(function (err, result) {
                                        if (err)
                                            throw err;

                                        console.log('finito, chiudo la connessione: aggiornati ' + aggiornati + ' prodotti per '+nome_modello)
                                        mongoose.connection.close();

                                        fs.writeFile(".tmp/"+txt+".txt",

                                            logGlobale,

                                            function (err) {
                                                if (err) {
                                                    console.log("Log was not saved!", err);
                                                    throw err;
                                                }
                                                else
                                                    console.log("Log was saved!");
                                            }
                                        );

                                    })

                                }
                                else {
                                    console.log('Non ci sono prodotti da aggiornare')
                                    mongoose.connection.close();
                                }
                            }
                        )
                     
                })


        }
    );
}
