var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    aggiornati = 0;


var id_modello = '56b8aaa90de23c95600bd232', modulo_led;

mongoose.connect(db);

mongoose.connection.once('open', function () {

        console.log("Connessione aperta, parte il batch "+id_modello); /// this gets printed to console

        var query =
        {
            generazione: {$in: ['G6', 'D1']},
            modello: {
                $in: [
                    id_modello
                ]
            },prezzo_old: { $exists: true }
        }

        if (modulo_led)
            query.moduloled = modulo_led;


        Prodotto.find(query).lean()
            .exec(
            function (err, prodotti) {

                var bulk = Prodotto.collection.initializeOrderedBulkOp();

                async.each(prodotti,
                    function (prodotto, cb) {

                        var p = _.clone(prodotto);

                        var update = {};
                        update.$set = {};
                        update.$unset = {};
                        update.$set.prezzo = p.prezzo_old;
                        //update.$set.prezzo_old = undefined;
                        update.$set.mdate = new Date();

                        update.$unset.prezzo_old = "";

                        bulk.find({_id: p._id}).updateOne(update);

                        aggiornati++;

                        cb();
                    },
                    function (err) {
                        if (err) {
                            console.log(err)
                            throw err;
                        }

                        if (aggiornati > 0)  {

                            bulk.execute(function (err, result) {
                                if (err)
                                    throw err;

                                console.log('finito, chiudo la connessione: aggiornati ' + aggiornati + ' prodotti per '+id_modello)
                                mongoose.connection.close();

                            })

                        }
                        else {
                            console.log('Non ci sono prodotti da aggiornare')
                            mongoose.connection.close();
                        }
                    }
                )

            })


    }
);
