/*
 node --max-old-space-size=2000 scripts/listino_prezzi/boot.js;
*/

var fs = require("fs"),
    _ = require('underscore'),
    async = require('async'),
    q = require('q');

var purgeList = function (list) {
    return _.reject(list, function (n) {
        return n == '.DS_Store' || n == 'CVS'
    });
}

var exDir = './scripts/listino_prezzi';
var choices = [], dir_choices = [], n = 0, summary = "";

//for (var i = 0; i < 100; i++) {
//    console.log(i, String.fromCharCode(48+i));
//}

if (fs.existsSync(exDir))
    fs.readdir(exDir, function (err, list) {
        if (list.length) {
            list = purgeList(list);
            list.forEach(function (el, idx) {
                if (fs.lstatSync(exDir + '/' + el).isDirectory()) {
                    var res = String.fromCharCode(48+n);
                    choices.push(res);
                    dir_choices.push(el);
                    console.log("[" + n + '] ' + el);
                    n++;
                }
            });

            var	query = require('cli-interact').getChar;
            var answer;

            answer = query('Tell me one of '+choices.toString(), choices.toString());
            console.log('you answered:', dir_choices[_.indexOf(choices, answer)]);
            summary = answer + " ";

            exDir = './scripts/listino_prezzi/' + dir_choices[_.indexOf(choices, answer)];
            choices = [];
            dir_choices = [];
            n = 0;

            fs.readdir(exDir, function (err, list2) {
                if (list2.length) {
                    list2 = purgeList(list2);
                    list2.forEach(function (el2) {
                        if (fs.lstatSync(exDir + '/' + el2).isFile()) {
                            //var res = String.fromCharCode(48 + n);
                            choices.push(n);
                            dir_choices.push(el2);
                            console.log("[" + n + '] ' + el2);
                            n++;
                        }
                    });

                    var query = require('cli-interact').getChar;
                    var answer;

                    choices.push('All');

                    answer = query('Tell me one of ' + choices.toString(), choices.toString());

                    if (answer === 'All') {
                        console.log('[DEBUG] eseguirò tutto');

                        function doAll () {
                            var promises = [];

                            dir_choices.forEach(function (pr) {
                                var deferred = q.defer();

                                console.log('[DEBUG] eseguo ' + exDir + '/' + pr);

                                var exec = require('child_process').exec;

                                var child = exec('node --max-old-space-size=2000 ' + exDir + '/' + pr);

                                child.stdout.on('data', function (data) {
                                    console.log('stdout: ' + data);
                                });
                                child.stderr.on('data', function (data) {
                                    console.log('stderr: ' + data);
                                });
                                child.on('exit', function () {
                                    console.log('exit event');
                                    deferred.resolve();
                                });

                                child.on('close', function (code) {
                                    console.log('closing code: ' + code);
                                });

                                promises.push(deferred.promise);

                                /*exec('node --max-old-space-size=2000 ' + exDir + '/' + pr, function (error, stdout, stderr) {
                                 if (error) {
                                 console.error('stderr', stderr);
                                 deferred.reject();
                                 }
                                 console.log('stdout', stdout);

                                 deferred.resolve();
                                 });*/
                            });

                            return q.all(promises)
                        }

                        doAll()
                            .then(function() { console.log('finite le chiamate'); })
                            .fail(console.error)
                            .done(function() { console.log('finito tutto'); });

                        /*var esecuzione = function (pr) {
                            var deferred = Q.defer();

                            console.log('[DEBUG] eseguo '+exDir+'/'+pr);

                            var exec = require('child_process').exec;
                            exec('node --max-old-space-size=2000 '+exDir+'/'+pr, function(error, stdout, stderr) {
                                if (error) {
                                    console.error('stderr', stderr);
                                    deferred.reject();
                                }
                                console.log('stdout', stdout);

                                deferred.resolve();
                            });

                            return deferred.promise;
                        }

                        async.map(dir_choices, esecuzione, function(e,r) { if(e) console.log(e); else console.log("finito tutto " + r);} );*/

                        /*_.each(dir_choices, function(pr) {

                            console.log('[DEBUG] eseguo '+exDir+'/'+pr);


                        });*/

                    }
                    else {
                        console.log('you answered:', answer, dir_choices[answer]);

                        summary = summary + answer + " (" + dir_choices[answer] + ")";

                        var exec = require('child_process').exec;
                        exec('node --max-old-space-size=2000 '+exDir+'/'+dir_choices[answer], function(error, stdout, stderr) {
                                if (error) {
                                    console.error('stderr', stderr);
                                    throw error;
                                }
                                console.log('stdout', stdout);
                                console.log('[DEBUG] terminato '+summary);
                            });


                        //console.log('lancio '+'node --max-old-space-size=2000 '+exDir+'/'+dir_choices[answer]);
                        //var child = spawn('node --max-old-space-size=2000 '+exDir+'/'+dir_choices[answer]);


                    }
                }
            });

        }
    });



/*var query = require('cli-interact').getYesNo;
var answer = query('Is it true');
console.log('you answered:', answer);*/