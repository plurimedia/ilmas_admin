/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/moduli/F112_Led_Module.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:95 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:95 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:95 },
        { trig : ["WB", "WC"], prezzo:115 },
        { trig : ["WD"], prezzo:115 },
        { trig : ["WE"], prezzo:115 },
        { trig : ["QB", "QC"], prezzo:130 },
        { trig : ["QD"], prezzo:130 },
        { trig : ["QE"], prezzo:130 }
    ],
    aumenti : {
        E : [20,29,39,20,29,39,20,29,39],
        D : [60,60,87,60,60,87,60,60,87],
        L : [67,67,87,67,67,87,67,67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd226', 'F112 Led Module', 'F112_Led_Module', 'dani_20170713_03');

