/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/moduli/F111_Led_Module.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:90 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:90 },
        { trig : ["WB", "WC"], prezzo:110 },
        { trig : ["WD"], prezzo:110 },
        { trig : ["QB", "QC"], prezzo:125 },
        { trig : ["QD"], prezzo:125 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd225', 'F111 Led Module', 'F111_Led_Module', 'dani_20170713_01');

