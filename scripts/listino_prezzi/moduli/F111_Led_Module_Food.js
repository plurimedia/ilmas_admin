/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/moduli/F111_Led_Module_Food.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["LD"], prezzo:125 },
        { trig : ["ND"], prezzo:125 },
        { trig : ["TD"], prezzo:90 },
        { trig : ["SD"], prezzo:90 },
        { trig : ["GD"], prezzo:125 },
        { trig : ["JD"], prezzo:125 }
    ],
    aumenti : {
        E : [29,29,29,29,29,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd225', 'F111 Led Module', 'F111_Led_Module_Food', 'dani_20170713_02');

