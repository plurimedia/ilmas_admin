/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/moduli/F112_Led_Module_Food.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["LD"], prezzo:130 },
        { trig : ["LE"], prezzo:130 },
        { trig : ["ND"], prezzo:130 },
        { trig : ["NE"], prezzo:130 },
        { trig : ["TD"], prezzo:95 },
        { trig : ["TE"], prezzo:95 },
        { trig : ["SD"], prezzo:95 },
        { trig : ["SE"], prezzo:95 },
        { trig : ["GD"], prezzo:95 },
        { trig : ["GE"], prezzo:95 },
        { trig : ["JD"], prezzo:95 },
        { trig : ["JE"], prezzo:95 }
    ],
    aumenti : {
        E : [29,39,29,39,29,39,29,39,29,39,29,39],
        D : [60,87,60,87,60,87,60,87,60,87,60,87],
        L : [67,87,67,87,67,87,67,87,67,87,67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd226', 'F112 Led Module', 'F112_Led_Module_Food', 'dani_20170713_04');

