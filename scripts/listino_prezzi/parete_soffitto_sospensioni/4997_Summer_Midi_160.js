/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/4997_Summer_Midi_160.js;
*/

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IC", "AC", "BC", "OC", "SC", "TC"], prezzo:232 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:241 },
        { trig : ["WC"], prezzo:252 },
        { trig : ["WD"], prezzo:261 },
        { trig : ["QC"], prezzo:267 },
        { trig : ["QD"], prezzo:276 }
    ],
    aumenti : {
        D : [40,31,40,31,40,31],
        L : [47,38,47,38,47,38]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2c5', '4997 Summer Midi 160', '4997_Summer_Midi_160', 'ame_20170725_35');
