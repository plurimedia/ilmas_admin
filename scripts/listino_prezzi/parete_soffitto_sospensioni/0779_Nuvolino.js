/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/0779_Nuvolino.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["1B", "2B", "3B"], prezzo:81},
    ],
    aumenti : {
        E : [20,20,20],
        D : [60,60,60],
        L : [67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2b5', '0779 Nuvolino', '0779_Nuvolino', 'ame_20170914_10', 'V2 Led Module',null,['V1']);

