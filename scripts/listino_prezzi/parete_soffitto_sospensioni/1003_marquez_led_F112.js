/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/1003_marquez_led_F112.js;
*/

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB","AB","CB","OB","SB","TB","IC","AC","BC","OC","SC","TC"], prezzo:444 },
        { trig : ["ID","AD","BD","OD","SD","TD"], prezzo:444 },
        { trig : ["IE","AE", "BE", "OE", "SE", "TE"], prezzo:444 },
        { trig : ["WB","WC"], prezzo:504 },
        { trig : ["WD"], prezzo:504 },
        { trig : ["WE"], prezzo:504 },
        { trig : ["QB","QC"], prezzo:549 },
        { trig : ["QD"], prezzo:549 },
        { trig : ["QE"], prezzo:549 }
    ],
    aumenti : {
        E : [60,87,117,60,87,117,60,87,117],
        D : [180,180,261,180,180,261,180,180,261],
        L : [201,201,261,201,201,261,201,201,261]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2cb', '1003 Marquez Led', '1003_Marquez_Led_F112', 'ame_20170725_11', 'F112 Led Module');

