/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/0776_NUVOLA_LED_F111.js;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    fs      = require('fs'),
    aggiornati = 0,
    logGlobale="",
    trascodificaPrezzo="ame_20170725_03";

/*
LISTINO PREZZI
*/

var _1Prezzo     = 420,
    _2Prezzo     = 438,
    _3Prezzo     = 460,
    _4Prezzo     = 478,
    _5Prezzo     = 490,
    _6Prezzo     = 508,
     
    _1MaggiorazioneD  = 80,
    _1MaggiorazioneL  = 94,
   
    _2MaggiorazioneD  = 62,
    _2MaggiorazioneL  = 76,
 
    _3MaggiorazioneD  = 80,
    _3MaggiorazioneL  = 94,
   
    _4MaggiorazioneD  = 62,
    _4MaggiorazioneL  = 76,
 
    _5MaggiorazioneD  = 80,
    _5MaggiorazioneL  = 94,
 
    _6MaggiorazioneD  = 62,
    _6MaggiorazioneL  = 76; 
 

console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    var query = 
    {
         
        generazione: { $in: [
             "G6", 
             "D1" 
             ] },
        modello: { $in: [
         "56b8aaa90de23c95600bd2b2" //"0776 Nuvola Led"
        ] },
        moduloled:'F111 Led Module'
    }

    console.log(query)

    Prodotto.find(query).lean().sort({codice:1})
    .exec(
        function(err,prodotti){ 

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
 
        async.each(prodotti, 
            function(prodotto,cb)
            {
                var gruppo=0; 

                if (prodotto.codice.length==8 || prodotto.codice.length==9)
                {
                    var logStr = "";

                    var p = _.clone(prodotto);

                     if (p.prezzo_old==null){
                        p.prezzo_old = _.clone(p.prezzo); 
                    }
     
                    if (
                        p.codice.substring(4,6)=='IB' ||
                        p.codice.substring(4,6)=='AB' ||
                        p.codice.substring(4,6)=='BB' ||
                        p.codice.substring(4,6)=='OB' ||
                        p.codice.substring(4,6)=='SB' ||
                        p.codice.substring(4,6)=='TB' ||
                        p.codice.substring(4,6)=='IC' ||
                        p.codice.substring(4,6)=='AC' ||
                        p.codice.substring(4,6)=='BC' ||
                        p.codice.substring(4,6)=='OC' ||
                        p.codice.substring(4,6)=='SC' ||
                        p.codice.substring(4,6)=='TC'

                        ){ 
                            p.prezzo = _1Prezzo;
                            gruppo = 1;
                    }
                    else if (
                        p.codice.substring(4,6)=='ID' ||
                        p.codice.substring(4,6)=='AD' ||
                        p.codice.substring(4,6)=='BD' ||
                        p.codice.substring(4,6)=='OD' ||
                        p.codice.substring(4,6)=='SD' ||
                        p.codice.substring(4,6)=='TD'
                    )
                    {
                        p.prezzo = _2Prezzo;
                        gruppo = 2;
                    }
                    else if (
                        p.codice.substring(4,6)=='WB' ||
                        p.codice.substring(4,6)=='WC' 
                    )
                    {
                        p.prezzo = _3Prezzo;
                        gruppo = 3;
                    }
                    else if (p.codice.substring(4,6)=='WD')
                    {
                        p.prezzo = _4Prezzo;
                        gruppo = 4;
                    }
                    else if (
                        p.codice.substring(4,6)=='QB' ||
                        p.codice.substring(4,6)=='QC'
                        )
                    {
                        p.prezzo = _5Prezzo;
                        gruppo = 5;
                    }
                    else if (p.codice.substring(4,6)=='QD')
                    {
                        p.prezzo = _6Prezzo;
                        gruppo = 6;
                    }
                    

                    if (gruppo>0)
                    {

                        logStr = _.clone(aggiornati) + " codice " + p.codice +  " 5° e 6° car " + p.codice.substring(4,6) +
                                    " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo 
                    }  

                    if (p.codice.length==9 && gruppo>0)
                    {
                         if (p.codice.substring(8,9)=='D')
                        {
                            if (gruppo == 1){
                                maggiorazione = _1MaggiorazioneD
                            }
                            else  if (gruppo == 2){
                                maggiorazione = _2MaggiorazioneD
                            }
                            else  if (gruppo == 3){
                                maggiorazione = _3MaggiorazioneD
                            }
                            else  if (gruppo == 4){
                                maggiorazione = _4MaggiorazioneD
                            }
                            else  if (gruppo == 5){
                                maggiorazione = _5MaggiorazioneD
                            }
                            else  if (gruppo == 6){
                                maggiorazione = _6MaggiorazioneD
                            }
                            p.prezzo = p.prezzo + maggiorazione

                            logStr = logStr + " D secon cambio prezzo +  " + maggiorazione + " " + p.prezzo
                        }
                        else if (p.codice.substring(8,9)=='L')
                        {
                           if (gruppo == 1){
                                maggiorazione = _1MaggiorazioneL
                            }
                            else  if (gruppo == 2){
                                maggiorazione = _2MaggiorazioneL
                            }
                            else  if (gruppo == 3){
                                maggiorazione = _3MaggiorazioneL
                            }
                            else  if (gruppo == 4){
                                maggiorazione = _4MaggiorazioneL
                            }
                            else  if (gruppo == 5){
                                maggiorazione = _5MaggiorazioneL
                            }
                            else  if (gruppo == 6){
                                maggiorazione = _6MaggiorazioneL
                            }
                            
                            p.prezzo = p.prezzo + maggiorazione

                            logStr = logStr + " L secon cambio prezzo +  " + maggiorazione + " " + p.prezzo
                        }
                    }


                    if (gruppo>0){

                        logStr = logStr + " gruppo " + gruppo

                        aggiornati++;
                    
                        var update = {};
                        update.$set = {};
                        update.$set.prezzo = p.prezzo;
                        update.$set.prezzo_old = p.prezzo_old;
                        update.$set.trascodificaPrezzo = trascodificaPrezzo
                        update.$set.mdate = new Date();

                        bulk.find( { _id: p._id } ).updateOne(update);
                        
                      console.log(logStr); 

                        logGlobale = logGlobale + logStr + "\n"; 
                    }
                   
                }
                cb();
            },
            function(err)
            {
                if(err){
                   console.log(err)
                    throw err;
                } 
 
                if(aggiornati > 0) {
                 
                    bulk.execute(function(err, result){
                        if(err)
                            throw err;

                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                        mongoose.connection.close(); 

                        fs.writeFile(".tmp/0776_NUVOLA_LED_F111.txt",

                             logGlobale,

                             function(err) {
                                if(err) {
                                    console.log("Log was not saved!", err);
                                    throw err;
                                }
                                else
                                    console.log("Log was saved!");
                            }
                        );

                    })
                   
                }
                else {
                    console.log('Non ci sono prodotti da aggiornare')
                    mongoose.connection.close();
                }
            }
        )
    })

 
}
);
