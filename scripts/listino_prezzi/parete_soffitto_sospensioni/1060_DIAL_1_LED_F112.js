/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/1060_DIAL_1_LED_F112.js;
*/
var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:290 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:299 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:309 },
        { trig : ["WB", "WC"], prezzo:310 },
        { trig : ["WD"], prezzo:319 },
        { trig : ["WE"], prezzo:329 },
        { trig : ["QB", "QC"], prezzo:325 },
        { trig : ["QD"], prezzo:334 },
        { trig : ["QE"], prezzo:344 }
    ],
    aumenti : {
        D : [40,31,48,40,31,48,40,31,48],
        L : [47,38,48,47,38,48,47,38,48]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2b6', '1060 Dial 1 Led', '1060_Dial_1_Led_F112', 'ame_20170725_17', 'F112 Led Module');
