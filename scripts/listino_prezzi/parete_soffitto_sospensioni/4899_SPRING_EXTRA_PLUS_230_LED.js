/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/4899_SPRING_EXTRA_PLUS_230_LED.js;
*/

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["DF", "EF", "FF", "UF", "VF"], prezzo:301 },
        { trig : ["XF"], prezzo:321 }
    ],
    aumenti : {
        D : [48,48],
        L : [48,48]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2a2', '4899 Spring Extra Plus 230', '4899_Spring_Extra_Plus_230', 'dani_20170901_01');
