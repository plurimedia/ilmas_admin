/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/1006_MARQUEZ_LED_F111.js;
*/

var process = require('../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB","AB","CB","OB","SB","TB","IC","AC","BC","OC","SC","TC"], prezzo:850 },
        { trig : ["ID","AD","BD","OD","SD","TD"], prezzo:850 },
        { trig : ["WC"], prezzo:970 },
        { trig : ["WD"], prezzo:970 },
        { trig : ["QC"], prezzo:1060 },
        { trig : ["QD"], prezzo:1060 }
    ],
    aumenti : {
        E : [120,174,120,174,120,174],
        D : [360,360,360,360,360,360],
        L : [402,402,402,402,402,402]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2cd', '1006 Marquez Led', '1006_MARQUEZ_LED_F111', 'ame_20170725_14', 'F111 Led Module');

