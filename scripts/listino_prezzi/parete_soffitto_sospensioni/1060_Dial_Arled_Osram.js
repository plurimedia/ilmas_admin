/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/1060_Dial_Arled_Osram.js;
*/

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["2X","3X"], prezzo:217 },
        { trig : ["2Y","3Y"], prezzo:224 },
        { trig : ["1F", "2F", "3F"], prezzo:226 }
    ],
    aumenti : {
        E : [0,0,39],
        D : [0,0,87],
        L : [0,0,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2ba', '1060 Dial Arled Osram', '1060_Dial_Arled_Osram', 'dani_20170814_13');

