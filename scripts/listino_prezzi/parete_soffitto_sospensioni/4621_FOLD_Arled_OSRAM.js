/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/4621_FOLD_Arled_OSRAM.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    fs      = require('fs'),
    aggiornati = 0,
    logGlobale="",
    trascodificaPrezzo="ame_20170725_30";


/*
LISTINO PREZZI
*/

var _1Prezzo     = 150,
    _2Prezzo     = 157,
    _2Prezzo     = 159,
    
    _1MaggiorazioneE  = 0,
    _1MaggiorazioneD  = 0,
    _1MaggiorazioneL  = 0,
   
    _2MaggiorazioneE  = 0,
    _2MaggiorazioneD  = 0,
    _2MaggiorazioneL  = 0,

    _3MaggiorazioneE  = 39,
    _3MaggiorazioneD  = 87,
    _3MaggiorazioneL  = 87; 

console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    var query = 
    {
         
        generazione: { $in: [
             "G6", 
             "D1" 
             ] },
        modello: { $in: [
         "56b8aaa90de23c95600bd2a6" //4621 Fold Arled Osram
        ] }
    }

    console.log(query)

    Prodotto.find(query).lean().sort({codice:1})
    .exec(
        function(err,prodotti){ 

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
 
        async.each(prodotti, 
            function(prodotto,cb)
            {
                var gruppo=0; 

                if (prodotto.codice.length==8 || prodotto.codice.length==9)
                {
                    var logStr = "";

                    var p = _.clone(prodotto);

                     if (p.prezzo_old==null){
                        p.prezzo_old = _.clone(p.prezzo); 
                    }
     
                    if (
                        p.codice.substring(4,6)=='2X' ||
                        p.codice.substring(4,6)=='3X'
                        ){ 
                            p.prezzo = _1Prezzo;
                            gruppo = 1;
                    }
                    else if (
                        p.codice.substring(4,6)=='2Y' ||
                        p.codice.substring(4,6)=='3Y'
                    )
                    {
                        p.prezzo = _2Prezzo;
                        gruppo = 2;
                    }
                    else if (
                        p.codice.substring(4,6)=='1F' ||
                        p.codice.substring(4,6)=='2F' ||
                        p.codice.substring(4,6)=='3F'
                    )
                    {
                        p.prezzo = _3Prezzo;
                        gruppo = 3;
                    } 

                    if (gruppo>0)
                    {

                        logStr = _.clone(aggiornati) + " codice " + p.codice +  " 5° e 6° car " + p.codice.substring(4,6) +
                                    " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo 
                    }  

                    if (p.codice.length==9 && gruppo>0)
                    {
                        if (p.codice.substring(8,9)=='E')
                        {
                            if (gruppo == 1){
                                maggiorazione = _1MaggiorazioneE
                            }
                            else  if (gruppo == 2){
                                maggiorazione = _2MaggiorazioneE
                            }
                            else  if (gruppo == 3){
                                maggiorazione = _3MaggiorazioneE
                            } 
                            p.prezzo = p.prezzo + maggiorazione

                            logStr = logStr + " E secon cambio prezzo +  " + maggiorazione + " " + p.prezzo
                        }
                        else if (p.codice.substring(8,9)=='D')
                        {
                            if (gruppo == 1){
                                maggiorazione = _1MaggiorazioneD
                            }
                            else  if (gruppo == 2){
                                maggiorazione = _2MaggiorazioneD
                            }
                            else  if (gruppo == 3){
                                maggiorazione = _3MaggiorazioneD
                            } 
                            p.prezzo = p.prezzo + maggiorazione

                            logStr = logStr + " D secon cambio prezzo +  " + maggiorazione + " " + p.prezzo
                        }
                        else if (p.codice.substring(8,9)=='L')
                        {
                           if (gruppo == 1){
                                maggiorazione = _1MaggiorazioneL
                            }
                            else  if (gruppo == 2){
                                maggiorazione = _2MaggiorazioneL
                            }
                            else  if (gruppo == 3){
                                maggiorazione = _3MaggiorazioneL
                            } 
                            p.prezzo = p.prezzo + maggiorazione

                            logStr = logStr + " L secon cambio prezzo +  " + maggiorazione + " " + p.prezzo
                        }
                    }


                    if (gruppo>0){

                        logStr = logStr + " gruppo " + gruppo

                        aggiornati++;
                    
                        var update = {};
                        update.$set = {};
                        update.$set.prezzo = p.prezzo;
                        update.$set.prezzo_old = p.prezzo_old;
                        update.$set.trascodificaPrezzo = trascodificaPrezzo
                        update.$set.mdate = new Date();

                        bulk.find( { _id: p._id } ).updateOne(update);
                        
                       console.log(logStr); 

                        logGlobale = logGlobale + logStr + "\n"; 
                    }
                   
                }
                cb();
            },
            function(err)
            {
                if(err){
                   console.log(err)
                    throw err;
                } 
 
                if(aggiornati > 0) {
                 
                    bulk.execute(function(err, result){
                        if(err)
                            throw err;

                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                        mongoose.connection.close(); 

                        fs.writeFile(".tmp/4621_FOLD_Arled_OSRAM.txt",

                             logGlobale,

                             function(err) {
                                if(err) {
                                    console.log("Log was not saved!", err);
                                    throw err;
                                }
                                else
                                    console.log("Log was saved!");
                            }
                        );

                    })
                   
                }
                else {
                    console.log('Non ci sono prodotti da aggiornare')
                    mongoose.connection.close();
                }
            }
        )
    })

 
}
);
