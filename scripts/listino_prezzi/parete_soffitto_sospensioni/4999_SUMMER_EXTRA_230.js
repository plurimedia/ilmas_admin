/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/4999_SUMMER_EXTRA_230.js;
*/

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IC", "AC", "BC", "OC", "SC", "TC"], prezzo:297 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:306 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:316 },
        { trig : ["WC"], prezzo:317 },
        { trig : ["WD"], prezzo:326 },
        { trig : ["WE"], prezzo:336 },
        { trig : ["QC"], prezzo:332 },
        { trig : ["QD"], prezzo:341 },
        { trig : ["QE"], prezzo:351 }
    ],
    aumenti : {
        D : [40,31,48,40,31,48,40,31,48],
        L : [47,38,48,47,38,48,47,38,48]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2c8', '4999 Summer Extra 230', '4999_SUMMER_EXTRA_230', 'ame_20170725_35');
