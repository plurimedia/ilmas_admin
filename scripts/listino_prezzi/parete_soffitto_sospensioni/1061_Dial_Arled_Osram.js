/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/1061_Dial_Arled_Osram.js;
*/

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["2X","3X"], prezzo:282 },
        { trig : ["2Y","3Y"], prezzo:296 },
        { trig : ["1F", "2F", "3F"], prezzo:300 }
    ],
    aumenti : {
        E : [0,0,78],
        D : [0,0,174],
        L : [0,0,174]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2bb', '1061 Dial Arled Osram', '1061_Dial_Arled_Osram', 'dani_20170814_14');

