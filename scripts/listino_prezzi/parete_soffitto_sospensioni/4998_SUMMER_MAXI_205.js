/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/4998_SUMMER_MAXI_205.js;
*/

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:259 },
        { trig : ["IS", "AS", "BS", "OS", "SS", "TS"], prezzo:269 },
        { trig : ["WD"], prezzo:279 },
        { trig : ["WS"], prezzo:289 },
        { trig : ["QD"], prezzo:294 },
        { trig : ["QS"], prezzo:304 }
    ],
    aumenti : {
        D : [31,48,31,48,31,48],
        L : [38,48,38,48,38,48]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2c6', '4998 Summer Maxi 205', '4998_SUMMER_MAXI_205', 'ame_20170725_35');
