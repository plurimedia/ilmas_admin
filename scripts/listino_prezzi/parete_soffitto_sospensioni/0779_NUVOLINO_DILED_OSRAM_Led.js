/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/0779_NUVOLINO_DILED_OSRAM_Led.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    fs      = require('fs'),
    aggiornati = 0,
    logGlobale="",
    trascodificaPrezzo="ame_20170725_07";


/*
LISTINO PREZZI
*/

var _1Prezzo     = 93;
 
 

console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    var query = 
    { 
        generazione: { $in: [
             "G6", 
             "D1"
             ] },
        modello: { $in: [
        "56b8aaa90de23c95600bd2b5" // "0779 Nuvolino",
        ] },
        moduloled:'Diled Osram'
    }

    console.log(query)

    Prodotto.find(query).lean().sort({codice:1})
    .exec(
        function(err,prodotti){ 

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
 
        async.each(prodotti, 
            function(prodotto,cb)
            {
                var gruppo=0; 

                if (prodotto.codice.length==8 || prodotto.codice.length==9)
                {
                    var logStr = "";

                    var p = _.clone(prodotto);

                     if (p.prezzo_old==null){
                        p.prezzo_old = _.clone(p.prezzo); 
                    }
     
                    if (
                        p.codice.substring(4,6)=='2V' ||
                        p.codice.substring(4,6)=='3V' 

                        ){ 
                            p.prezzo = _1Prezzo;
                            gruppo = 1;
                    }  

                    if (gruppo>0)
                    {

                        logStr = _.clone(aggiornati) + " codice " + p.codice +  " 5° e 6° car " + p.codice.substring(4,6) +
                                    " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo 
                     

                        logStr = logStr + " gruppo " + gruppo

                        aggiornati++;
                    
                        var update = {};
                        update.$set = {};
                        update.$set.prezzo = p.prezzo;
                        update.$set.prezzo_old = p.prezzo_old;
                        update.$set.trascodificaPrezzo = trascodificaPrezzo
                        update.$set.mdate = new Date();

                        bulk.find( { _id: p._id } ).updateOne(update);
                        
                      console.log(logStr); 

                        logGlobale = logGlobale + logStr + "\n"; 
                    }
                   
                }
                cb();
            },
            function(err)
            {
                if(err){
                   console.log(err)
                    throw err;
                } 
 
                if(aggiornati > 0) {
                 
                    bulk.execute(function(err, result){
                        if(err)
                            throw err;

                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                        mongoose.connection.close(); 

                        fs.writeFile(".tmp/0777_NUVOLA_LED_F112.txt",

                             logGlobale,

                             function(err) {
                                if(err) {
                                    console.log("Log was not saved!", err);
                                    throw err;
                                }
                                else
                                    console.log("Log was saved!");
                            }
                        );

                    })
                   
                }
                else {
                    console.log('Non ci sono prodotti da aggiornare')
                    mongoose.connection.close();
                }
            }
        )
    })

 
}
);
