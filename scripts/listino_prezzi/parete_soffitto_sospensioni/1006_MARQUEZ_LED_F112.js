/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/1006_MARQUEZ_LED_F112.js;
*/

var process = require('../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB","AB","CB","OB","SB","TB","IC","AC","BC","OC","SC","TC"], prezzo:880 },
        { trig : ["ID","AD","BD","OD","SD","TD"], prezzo:880 },
        { trig : ["IE","AE", "BE", "OE", "SE", "TE"], prezzo:880 },
        { trig : ["WC","WB"], prezzo:1000 },
        { trig : ["WD"], prezzo:1000 },
        { trig : ["WE"], prezzo:1000 },
        { trig : ["QC","QB"], prezzo:1090 },
        { trig : ["QD"], prezzo:1090 },
        { trig : ["QE"], prezzo:1090 }
    ],
    aumenti : {
        E : [120,174,234,120,174,234,120,174,234],
        D : [360,360,522,360,360,522,360,360,522],
        L : [402,402,522,402,402,522,402,402,522]
    }
}

 process.execute(matrix, '56b8aaa90de23c95600bd2cd', '1006 Marquez Led', '1006_MARQUEZ_LED_F112', 'ame_20170725_15', 'F112 Led Module');

