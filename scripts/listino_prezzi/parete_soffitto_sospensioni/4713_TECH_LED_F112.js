/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/4713_TECH_LED_F112.js;
*/
var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:485 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:512 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:542 },
        { trig : ["WB", "WC"], prezzo:545 },
        { trig : ["WD"], prezzo:572 },
        { trig : ["WE"], prezzo:602 },
        { trig : ["QB", "QC"], prezzo:590 },
        { trig : ["QD"], prezzo:617 },
        { trig : ["QE"], prezzo:647 }
    ],
    aumenti : {
        D : [120,93,144,120,93,144,120,93,144],
        L : [141,114,144,141,114,144,141,114,144]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2ab', '4713 TECH LED', '4713_TECH_LED', 'ame_20170725_42', 'F112 Led Module');

