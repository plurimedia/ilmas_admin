/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/1002_marquez_led_F112.js;
*/

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:310 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:310 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:310 },
        { trig : ["WB", "WC"], prezzo:350 },
        { trig : ["WD"], prezzo:350 },
        { trig : ["WE"], prezzo:350 },
        { trig : ["QB", "QC"], prezzo:380 },
        { trig : ["QD"], prezzo:380 },
        { trig : ["QE"], prezzo:380 }
    ],
    aumenti : {
        E : [40,58,78,40,58,78,40,58,78],
        D : [120,120,174,120,120,174,120,120,174],
        L : [134,134,174,134,134,174,134,134,174]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2ca', '1002 Marquez Led', '1002_Marquez_Led_F112', 'ame_20170725_09', 'F112 Led Module');
