/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/1063_Dial_Arled_Osram.js;
*/


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["2X","3X"], prezzo:474 },
        { trig : ["2Y","3Y"], prezzo:502 },
        { trig : ["1F", "2F", "3F"], prezzo:510 }
    ],
    aumenti : {
        E : [0,0,156],
        D : [0,0,348],
        L : [0,0,348]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2bd', '1063 Dial Arled Osram', '1063_Dial_Arled_Osram', 'dani_20170814_16');

