/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/1062_DIAL_3_LED_F112.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    fs      = require('fs'),
    aggiornati = 0,
    logGlobale="",
    trascodificaPrezzo="ame_20170725_21";


/*
LISTINO PREZZI
*/

var _1Prezzo     = 510,
    _2Prezzo     = 510,
    _3Prezzo     = 510,
    _4Prezzo     = 570,
    _5Prezzo     = 570,
    _6Prezzo     = 570,
    _7Prezzo     = 615, 
    _8Prezzo     = 615, 
    _9Prezzo     = 615,
     
    _1MaggiorazioneE  = 60,
    _1MaggiorazioneD  = 180,
    _1MaggiorazioneL  = 201,
   
    _2MaggiorazioneE  = 87,
    _2MaggiorazioneD  = 180,
    _2MaggiorazioneL  = 201,
 
    _3MaggiorazioneE  = 117,
    _3MaggiorazioneD  = 261,
    _3MaggiorazioneL  = 261,
   
    _4MaggiorazioneE  = 60,
    _4MaggiorazioneD  = 180,
    _4MaggiorazioneL  = 201,
    
    _5MaggiorazioneE  = 87,
    _5MaggiorazioneD  = 180,
    _5MaggiorazioneL  = 201,
    
    _6MaggiorazioneE  = 117,
    _6MaggiorazioneD  = 261,
    _6MaggiorazioneL  = 261,

    _7MaggiorazioneE  = 60,
    _7MaggiorazioneD  = 180,
    _7MaggiorazioneL  = 201, 

    _8MaggiorazioneE  = 87,
    _8MaggiorazioneD  = 180,
    _8MaggiorazioneL  = 201, 

    _9MaggiorazioneE  = 117,
    _9MaggiorazioneD  = 261,
    _9MaggiorazioneL  = 261;
 

console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    var query = 
    { 
        generazione: { $in: [
             "G6", 
             "D1"
             ] },
        modello: { $in: [
         "56b8aaa90de23c95600bd2b8" // "1062 Dial 3 Led"
        ] },
        moduloled:'F112 Led Module'
    }

    console.log(query)

    Prodotto.find(query).lean().sort({codice:1})
    .exec(
        function(err,prodotti){ 

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
 
        async.each(prodotti, 
            function(prodotto,cb)
            {
                var gruppo=0; 

                if (prodotto.codice.length==8 || prodotto.codice.length==9)
                {
                    var logStr = "";

                    var p = _.clone(prodotto);

                     if (p.prezzo_old==null){
                        p.prezzo_old = _.clone(p.prezzo); 
                    }
     
                    if (
                        p.codice.substring(4,6)=='IB' ||
                        p.codice.substring(4,6)=='AB' ||
                        p.codice.substring(4,6)=='BB' ||
                        p.codice.substring(4,6)=='OB' ||
                        p.codice.substring(4,6)=='SB' ||
                        p.codice.substring(4,6)=='TB' ||
                        p.codice.substring(4,6)=='IC' ||
                        p.codice.substring(4,6)=='AC' ||
                        p.codice.substring(4,6)=='BC' ||
                        p.codice.substring(4,6)=='OC' ||
                        p.codice.substring(4,6)=='SC' ||
                        p.codice.substring(4,6)=='TC'

                        ){ 
                            p.prezzo = _1Prezzo;
                            gruppo = 1;
                    }
                    else if (
                        p.codice.substring(4,6)=='ID' ||
                        p.codice.substring(4,6)=='AD' ||
                        p.codice.substring(4,6)=='BD' ||
                        p.codice.substring(4,6)=='OD' ||
                        p.codice.substring(4,6)=='SD' ||
                        p.codice.substring(4,6)=='TD'
                    )
                    {
                        p.prezzo = _2Prezzo;
                        gruppo = 2;
                    }
                    else if (
                        p.codice.substring(4,6)=='IE' ||
                        p.codice.substring(4,6)=='AE' ||
                        p.codice.substring(4,6)=='BE' ||
                        p.codice.substring(4,6)=='OE' ||
                        p.codice.substring(4,6)=='SE' ||
                        p.codice.substring(4,6)=='TE'
                    )
                    {
                        p.prezzo = _3Prezzo;
                        gruppo = 3;
                    }
                    else if (
                        p.codice.substring(4,6)=='WB' ||
                        p.codice.substring(4,6)=='WC')
                    {
                        p.prezzo = _4Prezzo;
                        gruppo = 4;
                    }
                    else if (
                        p.codice.substring(4,6)=='WD' 
                        )
                    {
                        p.prezzo = _5Prezzo;
                        gruppo = 5;
                    }
                    else if (p.codice.substring(4,6)=='WE')
                    {
                        p.prezzo = _6Prezzo;
                        gruppo = 6;
                    }

                     else if (
                        p.codice.substring(4,6)=='QB' ||
                        p.codice.substring(4,6)=='QC')
                    {
                        p.prezzo = _7Prezzo;
                        gruppo = 7;
                    }
                    else if (p.codice.substring(4,6)=='QD')
                    {
                        p.prezzo = _8Prezzo;
                        gruppo = 8;
                    }
                     else if (p.codice.substring(4,6)=='QE')
                    {
                        p.prezzo = _9Prezzo;
                        gruppo = 9;
                    }
                    

                    if (gruppo>0)
                    {

                        logStr = _.clone(aggiornati) + " codice " + p.codice +  " 5° e 6° car " + p.codice.substring(4,6) +
                                    " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo 
                    }  

                    if (p.codice.length==9 && gruppo>0)
                    {

                         if (p.codice.substring(8,9)=='E')
                        {
                            if (gruppo == 1){
                                maggiorazione = _1MaggiorazioneE
                            }
                            else  if (gruppo == 2){
                                maggiorazione = _2MaggiorazioneE
                            }
                            else  if (gruppo == 3){
                                maggiorazione = _3MaggiorazioneE
                            }
                            else  if (gruppo == 4){
                                maggiorazione = _4MaggiorazioneE
                            }
                            else  if (gruppo == 5){
                                maggiorazione = _5MaggiorazioneE
                            }
                            else  if (gruppo == 6){
                                maggiorazione = _6MaggiorazioneE
                            }
                            else  if (gruppo == 7){
                                maggiorazione = _7MaggiorazioneE
                            }
                            else  if (gruppo == 8){
                                maggiorazione = _8MaggiorazioneE
                            }
                            else  if (gruppo == 9){
                                maggiorazione = _9MaggiorazioneE
                            }
                            p.prezzo = p.prezzo + maggiorazione

                            logStr = logStr + " E secon cambio prezzo +  " + maggiorazione + " " + p.prezzo
                        }
                        else if (p.codice.substring(8,9)=='D')
                        {
                            if (gruppo == 1){
                                maggiorazione = _1MaggiorazioneD
                            }
                            else  if (gruppo == 2){
                                maggiorazione = _2MaggiorazioneD
                            }
                            else  if (gruppo == 3){
                                maggiorazione = _3MaggiorazioneD
                            }
                            else  if (gruppo == 4){
                                maggiorazione = _4MaggiorazioneD
                            }
                            else  if (gruppo == 5){
                                maggiorazione = _5MaggiorazioneD
                            }
                            else  if (gruppo == 6){
                                maggiorazione = _6MaggiorazioneD
                            }
                            else  if (gruppo == 7){
                                maggiorazione = _7MaggiorazioneD
                            }
                            else  if (gruppo == 8){
                                maggiorazione = _8MaggiorazioneD
                            }
                            else  if (gruppo == 9){
                                maggiorazione = _9MaggiorazioneD
                            }
                            p.prezzo = p.prezzo + maggiorazione

                            logStr = logStr + " D secon cambio prezzo +  " + maggiorazione + " " + p.prezzo
                        }
                        else if (p.codice.substring(8,9)=='L')
                        {
                           if (gruppo == 1){
                                maggiorazione = _1MaggiorazioneL
                            }
                            else  if (gruppo == 2){
                                maggiorazione = _2MaggiorazioneL
                            }
                            else  if (gruppo == 3){
                                maggiorazione = _3MaggiorazioneL
                            }
                            else  if (gruppo == 4){
                                maggiorazione = _4MaggiorazioneL
                            }
                            else  if (gruppo == 5){
                                maggiorazione = _5MaggiorazioneL
                            }
                            else  if (gruppo == 6){
                                maggiorazione = _6MaggiorazioneL
                            }
                            else  if (gruppo == 7){
                                maggiorazione = _7MaggiorazioneL
                            }
                            else  if (gruppo == 8){
                                maggiorazione = _8MaggiorazioneL
                            }
                            else  if (gruppo == 9){
                                maggiorazione = _9MaggiorazioneL
                            }
                            
                            p.prezzo = p.prezzo + maggiorazione

                            logStr = logStr + " L secon cambio prezzo +  " + maggiorazione + " " + p.prezzo
                        }
                    }


                    if (gruppo>0){

                        logStr = logStr + " gruppo " + gruppo

                        aggiornati++;
                    
                        var update = {};
                        update.$set = {};
                        update.$set.prezzo = p.prezzo;
                        update.$set.prezzo_old = p.prezzo_old;
                        update.$set.trascodificaPrezzo = trascodificaPrezzo
                        update.$set.mdate = new Date();

                        bulk.find( { _id: p._id } ).updateOne(update);
                        
                      console.log(logStr); 

                        logGlobale = logGlobale + logStr + "\n"; 
                    }
                   
                }
                cb();
            },
            function(err)
            {
                if(err){
                   console.log(err)
                    throw err;
                } 
 
                if(aggiornati > 0) {
                 
                    bulk.execute(function(err, result){
                        if(err)
                            throw err;

                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                        mongoose.connection.close(); 

                        fs.writeFile(".tmp/1062_DIAL_3_LED_F112.txt",

                             logGlobale,

                             function(err) {
                                if(err) {
                                    console.log("Log was not saved!", err);
                                    throw err;
                                }
                                else
                                    console.log("Log was saved!");
                            }
                        );

                    })
                   
                }
                else {
                    console.log('Non ci sono prodotti da aggiornare')
                    mongoose.connection.close();
                }
            }
        )
    })

 
}
);
