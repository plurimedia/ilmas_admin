/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/4623_FOLD_F111.js;
*/

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:466 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:493 },
        { trig : ["WB", "WC"], prezzo:526 },
        { trig : ["WD"], prezzo:553 },
        { trig : ["QB", "QC"], prezzo:571 },
        { trig : ["QD"], prezzo:598 }
    ],
    aumenti : {
        D : [120,93,120,93,120,93],
        L : [141,114,141,114,141,114]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2a5', '4623 Fold LED', '4623_Fold_LED_F111', 'ame_20170725_35', 'F111 Led Module');
