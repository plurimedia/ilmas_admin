/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/1060_DIAL_1_LED_F111.js;
*/

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:285 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:294 },
        { trig : ["WB", "WC"], prezzo:305 },
        { trig : ["WD"], prezzo:314 },
        { trig : ["QB", "QC"], prezzo:320 },
        { trig : ["QD"], prezzo:329 }
    ],
    aumenti : {
        D : [40,31,40,31,40,31],
        L : [47,38,47,38,47,38]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2b6', '1060 Dial 1 Led', '1060_Dial_1_Led_F111', 'ame_20170725_16', 'F111 Led Module');

