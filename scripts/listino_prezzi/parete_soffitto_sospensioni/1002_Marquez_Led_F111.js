/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/1002_Marquez_Led_F111.js;
*/

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:300 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:300 },
        { trig : ["WB", "WC"], prezzo:340 },
        { trig : ["WD"], prezzo:340 },
        { trig : ["QB", "QC"], prezzo:370 },
        { trig : ["QD"], prezzo:370 }
    ],
    aumenti : {
        E : [40,58,40,58,40,58],
        D : [120,120,120,120,120,110],
        L : [134,134,134,134,134,134]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2ca', '1002 Marquez Led', '1002_Marquez_Led_F111', 'ame_20170725_08', 'F111 Led Module');


