/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/4623_FOLD_F112.js;
*/
var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:481 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:508 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:538 },
        { trig : ["WB", "WC"], prezzo:541 },
        { trig : ["WD"], prezzo:568 },
        { trig : ["WE"], prezzo:598 },
        { trig : ["QB", "QC"], prezzo:586 },
        { trig : ["QD"], prezzo:613 },
        { trig : ["QE"], prezzo:643 }
    ],
    aumenti : {
        D : [120,93,144,120,93,144,120,93,144],
        L : [141,114,144,141,114,144,141,114,144]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2a5', '4623 Fold LED', '4623_Fold_LED', 'ame_20170725_36', 'F112 Led Module');


