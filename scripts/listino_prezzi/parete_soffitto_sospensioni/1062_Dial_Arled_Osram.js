/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/1062_Dial_Arled_Osram.js;
*/

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["2X","3X"], prezzo:351 },
        { trig : ["2Y","3Y"], prezzo:372 },
        { trig : ["1F", "2F", "3F"], prezzo:378 }
    ],
    aumenti : {
        E : [0,0,117],
        D : [0,0,261],
        L : [0,0,261]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2bc', '1062 Dial Arled Osram', '1062_Dial_Arled_Osram', 'dani_20170814_15');

