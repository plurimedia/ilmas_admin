/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/4713_Tech_Led_F111.js;
*/

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:470 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:497 },
        { trig : ["WB", "WC"], prezzo:530 },
        { trig : ["WD"], prezzo:557 },
        { trig : ["QB", "QC"], prezzo:575 },
        { trig : ["QD"], prezzo:602 }
    ],
    aumenti : {
        D : [120,93,120,93,120,93],
        L : [141,114,141,114,141,114]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2ab', '4713 TECH LED', '4713_TECH_LED_F111', 'ame_20170725_41', 'F111 Led Module');
