/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/1004_MARQUEZ_LED_F111.js;
*/

var process = require('../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB","AB","CB","OB","SB","TB","IC","AC","BC","OC","SC","TC"], prezzo:560 },
        { trig : ["ID","AD","BD","OD","SD","TD"], prezzo:560 },
        { trig : ["WC"], prezzo:640 },
        { trig : ["WD"], prezzo:640 },
        { trig : ["QC"], prezzo:700 },
        { trig : ["QD"], prezzo:700 }
    ],
    aumenti : {
        E : [80,116,80,116,80,116],
        D : [240,240,240,240,240,240],
        L : [268,268,268,268,268,268]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2cc', '1004 Marquez Led', '1004_MARQUEZ_LED_F111', 'ame_20170725_12', 'F111 Led Module');

