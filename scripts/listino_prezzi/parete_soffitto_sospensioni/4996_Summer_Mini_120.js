/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/4996_Summer_Mini_120.js;
*/

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "OB", "SB", "TB"], prezzo:180 },
        { trig : ["IC", "AC", "BC", "OC", "SC", "TC"], prezzo:189 },
        { trig : ["WB"], prezzo:200 },
        { trig : ["WC"], prezzo:209 },
        { trig : ["QB"], prezzo:215 },
        { trig : ["QC"], prezzo:224 }
    ],
    aumenti : {
        D : [40,31,40,31,40,31],
        L : [47,38,47,38,47,38]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2c4', '4996 Summer Mini 120', '4996_Summer_Mini_120', 'ame_20170725_35');
