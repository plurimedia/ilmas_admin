/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/parete_soffitto_sospensioni/1004_MARQUEZ_LED_F112.js;
*/

var process = require('../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB","AB","CB","OB","SB","TB","IC","AC","BC","OC","SC","TC"], prezzo:580 },
        { trig : ["ID","AD","BD","OD","SD","TD"], prezzo:580 },
        { trig : ["IE","AE", "BE", "OE", "SE", "TE"], prezzo:580 },
        { trig : ["WC","WB"], prezzo:660 },
        { trig : ["WD"], prezzo:660 },
        { trig : ["WE"], prezzo:660 },
        { trig : ["QC","QB"], prezzo:720 },
        { trig : ["QD"], prezzo:720 },
        { trig : ["QE"], prezzo:720 }
    ],
    aumenti : {
        E : [80,116,156,80,116,156,80,116,156],
        D : [240,240,348,240,240,348,240,240,348],
        L : [268,268,348,268,268,348,268,268,348]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2cc', '1004 Marquez Led', '1004_MARQUEZ_LED_F112', 'ame_20170725_13', 'F112 Led Module');

