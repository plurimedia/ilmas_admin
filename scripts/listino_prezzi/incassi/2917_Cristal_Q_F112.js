/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/2917_Cristal_Q_F112.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:132 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:132 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:132 },
        { trig : ["WB", "WC"], prezzo:152 },
        { trig : ["WD"], prezzo:152 },
        { trig : ["WE"], prezzo:152 },
        { trig : ["QB", "QC"], prezzo:167 },
        { trig : ["QD"], prezzo:167 },
        { trig : ["QE"], prezzo:167 }
    ],
    aumenti : {
        E : [20,29,39,20,29,39,20,29,39],
        D : [60,60,87,60,60,87,60,60,87],
        L : [67,67,87,67,67,87,67,67,87]
    }
}

process.execute(matrix, '5829a438ddea1004006e8b36', '2917 Cristal Q', '2917_Cristal_Q_F112', 'dani_20170723_72', 'F112 Led Module');
