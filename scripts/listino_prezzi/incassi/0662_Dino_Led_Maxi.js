/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0662_Dino_Led_Maxi.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:130 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:130 },
        { trig : ["WD"], prezzo:150 },
        { trig : ["WE"], prezzo:150 },
        { trig : ["QD"], prezzo:165 },
        { trig : ["QE"], prezzo:165 }
    ],
    aumenti : {
        E : [29,39,29,39,29,39],
        D : [60,87,60,87,60,87],
        L : [67,87,67,87,67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd254', '0662 Dino Led Maxi', '0662_Dino_Led_Maxi', 'dani_20170723_20');
