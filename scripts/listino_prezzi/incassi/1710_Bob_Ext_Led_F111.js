/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1710_Bob_Ext_Led_F111.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:427 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:427 },
        { trig : ["WB", "WC"], prezzo:487 },
        { trig : ["WD"], prezzo:487 },
        { trig : ["QB", "QC"], prezzo:532 },
        { trig : ["QD"], prezzo:532 }
    ],
    aumenti : {
        E : [60,87,60,87,60,87],
        D : [180,180,180,180,180,180],
        L : [201,201,201,201,201,201]
    }
}


process.execute(matrix, '56b8aaa90de23c95600bd264', '1710 Bob Ext', '1710_Bob_Ext_Led_F111', 'dani_20170723_52', 'F111 Led Module');
