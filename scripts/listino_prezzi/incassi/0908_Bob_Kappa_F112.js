/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0908_Bob_Kappa_F112.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:160 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:160 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:160 },
        { trig : ["WB", "WC"], prezzo:180 },
        { trig : ["WD"], prezzo:180 },
        { trig : ["WE"], prezzo:180 },
        { trig : ["QB", "QC"], prezzo:195 },
        { trig : ["QD"], prezzo:195 },
        { trig : ["QE"], prezzo:195 }
    ],
    aumenti : {
        E : [20,29,39,20,29,39,20,29,39],
        D : [60,60,87,60,60,87,60,60,87],
        L : [67,67,87,67,67,87,67,67,87]
    }
}


process.execute(matrix, '56b8aaa90de23c95600bd27c', '0908 Bob Kappa', '0908_Bob_Kappa_F112', 'dani_20170816_02', 'F112 Led Module');
