/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0786_Hammer_Q_Mini_120.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:87 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:87 },
        { trig : ["WB", "WC"], prezzo:107 },
        { trig : ["WD"], prezzo:107 },
        { trig : ["QB", "QC"], prezzo:122 },
        { trig : ["QD"], prezzo:122 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}


process.execute(matrix, '56b8aaa90de23c95600bd249', '0786 Hammer Q Mini 120', '0786_Hammer_Q_Mini_120', 'dani_20170723_27');
