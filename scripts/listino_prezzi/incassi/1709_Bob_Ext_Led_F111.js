/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1709_Bob_Ext_Led_F111.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:296 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:296 },
        { trig : ["WB", "WC"], prezzo:336 },
        { trig : ["WD"], prezzo:336 },
        { trig : ["QB", "QC"], prezzo:366 },
        { trig : ["QD"], prezzo:366 }
    ],
    aumenti : {
        E : [40,58,40,58,40,58],
        D : [120,120,120,120,120,120],
        L : [134,134,134,134,134,134]
    }
}



process.execute(matrix, '56b8aaa90de23c95600bd263', '1709 Bob Ext', '1709_Bob_Ext_Led_F111', 'dani_20170723_49', 'F111 Led Module');
