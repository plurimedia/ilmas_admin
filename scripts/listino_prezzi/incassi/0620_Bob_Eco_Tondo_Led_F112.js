/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0620_Bob_Eco_Tondo_Led_F112.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:121 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:121 },
        { trig : ["WB", "WC"], prezzo:141 },
        { trig : ["WD"], prezzo:141 },
        { trig : ["QB", "QC"], prezzo:156 },
        { trig : ["QD"], prezzo:156 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd282', '0620 Bob Eco Tondo Led', '0620_Bob_Eco_Tondo_Led_F112', 'dani_20170723_15', 'F112 Led Module');
