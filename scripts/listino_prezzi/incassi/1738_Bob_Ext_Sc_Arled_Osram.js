/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1738_Bob_Ext_Sc_Arled_Osram.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["2X", "3X"], prezzo:105 },
        { trig : ["2Y", "3Y"], prezzo:112 },
        { trig : ["1F", "2F", "3F"], prezzo:121 }
    ],
    aumenti : {
        E : [0,0,39],
        D : [0,0,87],
        L : [0,0,87]
    }
}



process.execute(matrix, '56b8aaa90de23c95600bd270', '1738 Bob Ext Sc Led', '1738_Bob_Ext_Sc_Led_Arled_Osram', 'dani_20170723_60', 'Arled Osram');
