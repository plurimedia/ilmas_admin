/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1609_Bob_Std_Sc_Led_Arled_Osram.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["2X", "3X"], prezzo:166 },
        { trig : ["2Y", "3Y"], prezzo:180 },
        { trig : ["1F", "2F", "3F"], prezzo:202 }
    ],
    aumenti : {
        E : [0,0,78],
        D : [0,0,174],
        L : [0,0,174]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd269', '1609 Bob Std Sc Led', '1609_Bob_Std_Sc_Led_Arled_Osram', 'dani_20170723_36', 'Arled Osram');
