/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0671_Astuto_Led_Midi.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IC", "AC", "BC", "OC", "SC", "TC"], prezzo:98 },
        { trig : ["IQ", "AQ", "BQ", "OQ", "SQ", "TQ"], prezzo:98 },
        { trig : ["WC"], prezzo:118 },
        { trig : ["WQ"], prezzo:118 },
        { trig : ["QC"], prezzo:133 },
        { trig : ["QQ"], prezzo:133 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd24f', '0671 Astuto Led Midi', '0671_Astuto_Led_Midi', 'dani_20170723_22');
