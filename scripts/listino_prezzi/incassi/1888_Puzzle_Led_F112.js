/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1888_Puzzle_Led_F112.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:135 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:135 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:135 },
        { trig : ["WB", "WC"], prezzo:155 },
        { trig : ["WD"], prezzo:155 },
        { trig : ["WE"], prezzo:155 },
        { trig : ["QB", "QC"], prezzo:170 },
        { trig : ["QD"], prezzo:170 },
        { trig : ["QE"], prezzo:170 }
    ],
    aumenti : {
        E : [20,29,39,20,29,39,20,29,39],
        D : [60,60,87,60,60,87,60,60,87],
        L : [67,67,87,67,67,87,67,67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd27f', '1888 Puzzle Led', '1888_Puzzle_Led_F112', 'dani_20170723_71', 'F112 Led Module');
