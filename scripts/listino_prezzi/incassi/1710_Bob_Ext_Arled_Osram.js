/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1710_Bob_Ext_Arled_Osram.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["2X", "3X"], prezzo:246 },
        { trig : ["2Y", "3Y"], prezzo:267 },
        { trig : ["1F", "2F", "3F"], prezzo:302 }
    ],
    aumenti : {
        E : [0,0,117],
        D : [0,0,261],
        L : [0,0,261]
    }
}


process.execute(matrix, '56b8aaa90de23c95600bd264', '1710 Bob Ext', '1710_Bob_Ext_Arled_Osram', 'dani_20170723_51', 'Arled Osram');