/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0787_Hammer_Q_Midi_160.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IC", "AC", "BC", "OC", "SC", "TC"], prezzo:111 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:111 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:111 },
        { trig : ["WC"], prezzo:131 },
        { trig : ["WD"], prezzo:131 },
        { trig : ["WE"], prezzo:131 },
        { trig : ["QC"], prezzo:146 },
        { trig : ["QD"], prezzo:146 },
        { trig : ["QE"], prezzo:146 }
    ],
    aumenti : {
        E : [20,29,39,20,29,39,20,29,39],
        D : [60,60,87,60,60,87,60,60,87],
        L : [67,67,87,67,67,87,67,67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd24a', '0787 Hammer Q Midi 160', '0797_Hammer_Q_Midi_160', 'dani_20170723_28');
