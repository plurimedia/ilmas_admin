/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1712_Bob_Eco_Led_F112.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:150 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:150 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:150 },
        { trig : ["WB", "WC"], prezzo:170 },
        { trig : ["WD"], prezzo:170 },
        { trig : ["WE"], prezzo:170 },
        { trig : ["QB", "QC"], prezzo:185 },
        { trig : ["QD"], prezzo:185 },
        { trig : ["QE"], prezzo:185 }
    ],
    aumenti : {
        E : [20,29,39,20,29,39,20,29,39],
        D : [60,60,87,60,60,87,60,60,87],
        L : [67,67,87,67,67,87,67,67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd279', '1712 Bob Eco Led', '1712_Bob_Eco_Led_F112', 'dani_20170723_55', 'F112 Led Module');
