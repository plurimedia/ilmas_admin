/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/2916_Cristal_F112.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:129 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:129 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:129 },
        { trig : ["WB", "WC"], prezzo:149 },
        { trig : ["WD"], prezzo:149 },
        { trig : ["WE"], prezzo:149 },
        { trig : ["QB", "QC"], prezzo:164 },
        { trig : ["QD"], prezzo:164 },
        { trig : ["QE"], prezzo:164 }
    ],
    aumenti : {
        E : [20,29,39,20,29,39,20,29,39],
        D : [60,60,87,60,60,87,60,60,87],
        L : [67,67,87,67,67,87,67,67,87]
    }
}

process.execute(matrix, '58b408ef076bbc0400698153', '2916 Cristal', '2916_Cristal_F112', 'dani_20170725_02', 'F112 Led Module');

