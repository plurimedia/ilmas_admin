/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0732_Berin_Led_Maxi.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:118 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:118 },
        { trig : ["WE"], prezzo:138 },
        { trig : ["WE"], prezzo:138 },
        { trig : ["QE"], prezzo:153 },
        { trig : ["QE"], prezzo:153 }
    ],
    aumenti : {
        E : [29,39,29,39,29,39],
        D : [60,87,60,87,60,87],
        L : [67,87,67,87,67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd258', '0732 Berin Led Maxi', '0732_Berin_Led_Maxi', 'dani_20170723_26');
