/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1608_Bob_Std_Sc_Led_F111.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["DG", "EG", "FG", "UG", "VG"], prezzo:136 },
        { trig : ["DH", "EH", "FH", "UH", "VH"], prezzo:136 },
        { trig : ["XG"], prezzo:156 },
        { trig : ["XH"], prezzo:156 }
    ],
    aumenti : {
        E : [48,58,48,58],
        D : [87,87,87,87],
        L : [87,87,87,87]
    }
}


process.execute(matrix, '56b8aaa90de23c95600bd248', '0799 Hammer EX Plus 230', '0799_Hammer_Extra_Plus_230', 'dani_20170630_01');
