/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0610_Bob_Std_Led_F111.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:417 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:417 },
        { trig : ["WB", "WC"], prezzo:477 },
        { trig : ["WD"], prezzo:477 },
        { trig : ["QB", "QC"], prezzo:522 },
        { trig : ["QD"], prezzo:522 }
    ],
    aumenti : {
        E : [60,87,60,87,60,87],
        D : [180,180,180,180,180,180],
        L : [201,201,201,201,201,201]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd25c', '0610 Bob Std Led', '0610_Bob_Std_F111', 'dani_20170723_08', 'F111 Led Module');
