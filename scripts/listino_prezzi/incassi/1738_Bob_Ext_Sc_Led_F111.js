/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1738_Bob_Ext_Sc_Led_F111.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:165 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:165 },
        { trig : ["WB", "WC"], prezzo:185 },
        { trig : ["WD"], prezzo:185 },
        { trig : ["QB", "QC"], prezzo:200 },
        { trig : ["QD"], prezzo:200 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd270', '1738 Bob Ext Sc Led', '1738_Bob_Ext_Sc_Led_F111', 'dani_20170723_61', 'F111 Led Module');
