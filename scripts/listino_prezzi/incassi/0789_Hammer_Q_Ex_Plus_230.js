/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0789_Hammer_Q_Ex_Plus_230.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["DG", "EG", "FG", "UG", "VG"], prezzo:152 },
        { trig : ["DH", "EH", "FH", "UH", "VH"], prezzo:152 },
        { trig : ["XG"], prezzo:172 },
        { trig : ["XH"], prezzo:172 }
    ],
    aumenti : {
        E : [48,58,48,58],
        D : [87,87,87,87],
        L : [87,87,87,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd24c', '0789 Hammer Q Ex Plus 230', '0789_Hammer_Q_Ex_Plus_230', 'dani_20170814_18');
