/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0660_Dino_Led_Mini.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["RB", "5B", "6B", "ZB", "7B", "8B", "RC", "5C", "6C", "ZC", "7C", "8C"], prezzo:108 },
        { trig : ["WB", "WC"], prezzo:128 },
        { trig : ["QB", "QC"], prezzo:143 }
    ],
    aumenti : {
        E : [20,20,20],
        D : [60,60,60],
        L : [67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd252', '0660_Dino_Led_Mini', '0660 Dino Led Mini', 'dani_20170723_18');
