/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0798_Hammer_Maxi_205.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IC", "AC", "BC", "OC", "SC", "TC"], prezzo:97 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:97 },
        { trig : ["IF", "AF", "BF", "OF", "SF", "TF"], prezzo:97 },
        { trig : ["WC"], prezzo:117 },
        { trig : ["WD"], prezzo:117 },
        { trig : ["WF"], prezzo:117 },
        { trig : ["QC"], prezzo:132 },
        { trig : ["QD"], prezzo:132 },
        { trig : ["QF"], prezzo:132 }
    ],
    aumenti : {
        E : [20,29,39,20,29,39,20,29,39],
        D : [60,60,87,60,60,87,60,60,87],
        L : [67,67,87,67,67,87,67,67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd245', '0798 Hammer Maxi 205', '0798_Hammer_Maxi_205', 'dani_20170723_31');
