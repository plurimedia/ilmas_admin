/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1608_Bob_Std_Sc_Led_Arled_Osram.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["2X", "3X"], prezzo:95 },
        { trig : ["2Y", "3Y"], prezzo:102 },
        { trig : ["1F", "2F", "3F"], prezzo:111 }
    ],
    aumenti : {
        E : [0,0,39],
        D : [0,0,87],
        L : [0,0,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd268', '1608 Bob Std Sc Led', '1608_Bob_Std_Sc_Led_Arled_Osram', 'dani_20170723_33', 'Arled Osram');
