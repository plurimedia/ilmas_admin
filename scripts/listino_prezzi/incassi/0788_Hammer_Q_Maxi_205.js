/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0788_Hammer_Q_Maxi_205.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IC", "AC", "BC", "OC", "SC", "TC"], prezzo:118 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:118 },
        { trig : ["IF", "AF", "BF", "OF", "SF", "TF"], prezzo:118 },
        { trig : ["WC"], prezzo:138 },
        { trig : ["WD"], prezzo:138 },
        { trig : ["WF"], prezzo:138 },
        { trig : ["QC"], prezzo:153 },
        { trig : ["QD"], prezzo:153 },
        { trig : ["QF"], prezzo:153 }
    ],
    aumenti : {
        E : [20,29,39,20,29,39,20,29,39],
        D : [60,60,87,60,60,87,60,60,87],
        L : [67,67,87,67,67,87,67,67,87]
    }
}

process.execute(matrix, '5784ca8ac98fef0300305938', '0788 Hammer Q Maxi 205', '0788_Hammer_Q_Maxi_205', 'dani_20170723_29');
