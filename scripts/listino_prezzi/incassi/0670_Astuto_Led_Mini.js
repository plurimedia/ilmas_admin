/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0670_Astuto_Led_Mini.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["RB", "5B", "6B", "ZB", "7B", "8B", "RC", "5C", "6C", "ZC", "7C", "8C"], prezzo:89 },
        { trig : ["WB", "WC"], prezzo:109 },
        { trig : ["QB", "QC"], prezzo:124 }
    ],
    aumenti : {
        E : [20,20,20],
        D : [60,60,60],
        L : [67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd24e', '0670 Astuto Led Mini', '0670_Astuto_Led_Mini', 'dani_20170723_21');
