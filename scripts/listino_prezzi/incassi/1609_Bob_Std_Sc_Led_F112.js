/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1609_Bob_Std_Sc_Led_F112.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:296 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:296 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:296 },
        { trig : ["WB", "WC"], prezzo:336 },
        { trig : ["WD"], prezzo:336 },
        { trig : ["WE"], prezzo:336 },
        { trig : ["QB", "QC"], prezzo:376 },
        { trig : ["QD"], prezzo:376 },
        { trig : ["QE"], prezzo:376 }
    ],
    aumenti : {
        E : [40,58,78,40,58,78,40,58,78],
        D : [120,120,174,120,120,174,120,120,174],
        L : [134,134,174,134,134,174,134,134,174]
    }
}


process.execute(matrix, '56b8aaa90de23c95600bd269', '1609 Bob Std Sc Led', '1609_Bob_Std_Sc_Led_F112', 'dani_20170723_38', 'F112 Led Module');

