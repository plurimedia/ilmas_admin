/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1888_Puzzle_Led_F111.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:130 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:130 },
        { trig : ["WB", "WC"], prezzo:150 },
        { trig : ["WD"], prezzo:150 },
        { trig : ["QB", "QC"], prezzo:165 },
        { trig : ["QD"], prezzo:165 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd27f', '1888 Puzzle Led', '1888_Puzzle_Led_F111', 'dani_20170723_70', 'F111 Led Module');
