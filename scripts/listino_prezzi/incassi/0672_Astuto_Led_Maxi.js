/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0672_Astuto_Led_Maxi.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:107 },
        { trig : ["IS", "AS", "BS", "OS", "SS", "TS"], prezzo:107 },
        { trig : ["WD"], prezzo:127 },
        { trig : ["WS"], prezzo:127 },
        { trig : ["QD"], prezzo:142 },
        { trig : ["QS"], prezzo:142 }
    ],
    aumenti : {
        E : [29,39,29,39,29,39],
        D : [60,87,60,87,60,87],
        L : [67,87,67,87,67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd250', '0672 Astuto Led Maxi', '0672_Astuto_Led_Maxi', 'dani_20170723_23');
