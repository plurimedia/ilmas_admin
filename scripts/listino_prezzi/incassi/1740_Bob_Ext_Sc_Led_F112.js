/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1740_Bob_Ext_Sc_Led_F112.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:442 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:442 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:442 },
        { trig : ["WB", "WC"], prezzo:502 },
        { trig : ["WD"], prezzo:502 },
        { trig : ["WE"], prezzo:502 },
        { trig : ["QB", "QC"], prezzo:547 },
        { trig : ["QD"], prezzo:547 },
        { trig : ["QE"], prezzo:547 }
    ],
    aumenti : {
        E : [60,87,117,60,87,117,60,87,117],
        D : [180,180,261,180,180,261,180,180,261],
        L : [201,201,261,201,201,261,201,201,261]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd272', '1740 Bob Ext Sc Led', '1740_Bob_Ext_Sc_Led_F112', 'dani_20170723_68', 'F112 Led Module');
