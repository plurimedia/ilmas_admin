/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0730_Berin_Led_Mini.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "OB", "SB", "TB"], prezzo:105 },
        { trig : ["WB", "WC"], prezzo:125 },
        { trig : ["QB", "QC"], prezzo:140 }
    ],
    aumenti : {
        E : [20,20,20],
        D : [60,60,60],
        L : [67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd256', '0730_Berin_Led_Mini', '0730 Berin Led Mini', 'dani_20170723_24');
