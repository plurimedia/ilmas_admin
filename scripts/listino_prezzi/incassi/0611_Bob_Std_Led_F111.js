/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0611_Bob_Std_Led_F111.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:544 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:544 },
        { trig : ["WB", "WC"], prezzo:624 },
        { trig : ["WD"], prezzo:624 },
        { trig : ["QB", "QC"], prezzo:684 },
        { trig : ["QD"], prezzo:684 }
    ],
    aumenti : {
        E : [80,116,80,116,80,116],
        D : [240,240,240,240,240,240],
        L : [268,268,268,268,268,268]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd25d', '0611 Bob Std Led', '0611_Bob_Std_F111', 'dani_20170723_11', 'F111 Led Module');
