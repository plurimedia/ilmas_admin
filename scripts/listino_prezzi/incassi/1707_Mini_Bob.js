/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1707_Mini_Bob.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["RB", "5B", "6B", "ZB", "7B", "8B"], prezzo:168 },
        { trig : ["WB", "WC"], prezzo:228 },
        { trig : ["QB", "QC"], prezzo:273 }
    ],
    aumenti : {
        E : [60,60,60],
        D : [180,180,180],
        L : [201,201,201]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd355', '1707 Mini Bob', '1707_Mini_Bob', 'dani_20170901_03');
