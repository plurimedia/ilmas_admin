/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1713_Bob_Eco_Led_F112.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:286 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:286 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:286 },
        { trig : ["WB", "WC"], prezzo:326 },
        { trig : ["WD"], prezzo:326 },
        { trig : ["WE"], prezzo:326 },
        { trig : ["QB", "QC"], prezzo:356 },
        { trig : ["QD"], prezzo:356 },
        { trig : ["QE"], prezzo:356 }
    ],
    aumenti : {
        E : [40,58,78,40,58,78,40,58,78],
        D : [120,120,174,120,120,174,120,120,174],
        L : [134,134,174,134,134,174,134,134,174]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd27a', '1713 Bob Eco Led', '1713_Bob_Eco_Led_F112', 'dani_20170723_57', 'F112 Led Module');
