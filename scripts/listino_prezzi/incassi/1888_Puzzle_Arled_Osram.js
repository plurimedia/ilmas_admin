/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1888_Puzzle_Arled_Osram.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["2X", "3X"], prezzo:72 },
        { trig : ["2Y", "3Y"], prezzo:79 },
        { trig : ["1F", "2F", "3F"], prezzo:91 }
    ],
    aumenti : {
        E : [0,0,39],
        D : [0,0,87],
        L : [0,0,87]
    }
}



process.execute(matrix, '56b8aaa90de23c95600bd280', '1888 Puzzle Arled Osram', '1888_Puzzle_Arled_Osram', 'dani_20170723_69', 'Arled Osram');
