/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1706_Mini_Bob.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["RB", "5B", "6B", "ZB", "7B", "8B"], prezzo:117 },
        { trig : ["WB", "WC"], prezzo:157 },
        { trig : ["QB", "QC"], prezzo:187 }
    ],
    aumenti : {
        E : [40,40,40],
        D : [120,120,120],
        L : [134,134,134]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd354', '1706 Mini Bob', '1706_Mini_Bob', 'dani_20170901_02');
