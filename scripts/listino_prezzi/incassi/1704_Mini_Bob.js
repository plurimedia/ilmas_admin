/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1704_Mini_Bob.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["RB", "5B", "6B", "ZB", "7B", "8B", "RC", "5C", "6C", "ZC", "7C", "8C"], prezzo:80 },
        { trig : ["WB", "WC"], prezzo:100 },
        { trig : ["QB", "QC"], prezzo:115 }
    ],
    aumenti : {
        E : [20,20,20],
        D : [60,60,60],
        L : [67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd353', '1704 Mini Bob', '1704_Mini_Bob', 'dani_20170901_01');
