/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1611_Bob_Std_Sc_Led_F112.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:564 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:564 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:564 },
        { trig : ["WB", "WC"], prezzo:644 },
        { trig : ["WD"], prezzo:644 },
        { trig : ["WE"], prezzo:644 },
        { trig : ["QB", "QC"], prezzo:704 },
        { trig : ["QD"], prezzo:704 },
        { trig : ["QE"], prezzo:704 }
    ],
    aumenti : {
        E : [80,116,156,80,116,156,80,116,156],
        D : [240,240,348,240,240,348,240,240,348],
        L : [268,268,348,268,268,348,268,268,348]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd26b', '1611 Bob Std Sc Led', '1611_Bob_Std_Sc_Led_F112', 'dani_20170723_44', 'F112 Led Module');
