/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0621_Bob_Std_Tondo_Led_F111.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:155 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:155 },
        { trig : ["WB", "WC"], prezzo:175 },
        { trig : ["WD"], prezzo:175 },
        { trig : ["QB", "QC"], prezzo:190 },
        { trig : ["QD"], prezzo:190 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd281', '0621 Bob Std Tondo Led', '0621_Bob_Std_Tondo_Led_F111', 'dani_20170723_16', 'F111 Led Module');
