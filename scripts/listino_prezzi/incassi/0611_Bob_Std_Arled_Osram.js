/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0611_Bob_Std_Arled_Osram.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["2X", "3X"], prezzo:302 },
        { trig : ["2Y", "3Y"], prezzo:330 },
        { trig : ["1F", "2F", "3F"], prezzo:377 }
    ],
    aumenti : {
        E : [0,0,156],
        D : [0,0,348],
        L : [0,0,348]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd25d', '0611 Bob Std Led', '0611_Bob_Std_Arled_Osram', 'dani_20170723_10', 'Arled Osram');
