/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/2916_Cristal_F111.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:121 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:121 },
        { trig : ["WB", "WC"], prezzo:141 },
        { trig : ["WD"], prezzo:141 },
        { trig : ["QB", "QC"], prezzo:156 },
        { trig : ["QD"], prezzo:156 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '58b408ef076bbc0400698153', '2916 Cristal', '2916_Cristal_F111', 'dani_20170725_01', 'F111 Led Module');

