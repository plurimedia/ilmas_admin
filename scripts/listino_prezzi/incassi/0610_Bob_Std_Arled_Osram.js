/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0610_Bob_Std_Arled_Osram.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["2X", "3X"], prezzo:236 },
        { trig : ["2Y", "3Y"], prezzo:257 },
        { trig : ["1F", "2F", "3F"], prezzo:292 }
    ],
    aumenti : {
        E : [0,0,117],
        D : [0,0,261],
        L : [0,0,261]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd25c', '0610 Bob Std Led', '0610_Bob_Std_Arled_Osram', 'dani_20170723_07', 'Arled Osram');
