/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1709_Bob_Ext_Arled_Osram.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["2X", "3X"], prezzo:176 },
        { trig : ["2Y", "3Y"], prezzo:190 },
        { trig : ["1F", "2F", "3F"], prezzo:212 }
    ],
    aumenti : {
        E : [0,0,78],
        D : [0,0,174],
        L : [0,0,174]
    }
}



process.execute(matrix, '56b8aaa90de23c95600bd263', '1709 Bob Ext', '1709_Bob_Ext_Arled_Osram', 'dani_20170723_48', 'Arled Osram');
