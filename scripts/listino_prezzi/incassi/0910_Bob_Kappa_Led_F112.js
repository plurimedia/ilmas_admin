/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0910_Bob_Kappa_Led_F112.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:432 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:432 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:432 },
        { trig : ["WB", "WC"], prezzo:492 },
        { trig : ["WD"], prezzo:492 },
        { trig : ["WE"], prezzo:492 },
        { trig : ["QB", "QC"], prezzo:537 },
        { trig : ["QD"], prezzo:537 },
        { trig : ["QE"], prezzo:537 }
    ],
    aumenti : {
        E : [60,87,117,60,87,117,60,87,117],
        D : [180,180,261,180,180,261,180,180,261],
        L : [201,201,261,201,201,261,201,201,261]
    }
}


process.execute(matrix, '56b8aaa90de23c95600bd27e', '0910 Bob Kappa Led', '0910_Bob_Kappa_Led_F112', 'dani_20170816_06', 'F112 Led Module');

