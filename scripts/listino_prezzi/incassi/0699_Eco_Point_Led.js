/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0699_Eco_Point_Led.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["1B", "2B", "3B"], prezzo:45},
    ],
    aumenti : {
        E : [20,20,20],
        D : [60,60,60],
        L : [67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd295', '0699 Eco Point Led', '0699 Eco Point Led ', 'ame_20170914_09', 'V2 Led Module',null, ['V1']);

