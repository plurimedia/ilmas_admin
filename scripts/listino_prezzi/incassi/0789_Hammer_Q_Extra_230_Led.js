/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0789_Hammer_Q_Extra_230_Led.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IC", "AC", "BC", "OC", "SC", "TC"], prezzo:134 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:134 },
        { trig : ["IF", "AF", "BF", "OF", "SF", "TF"], prezzo:134 },
        { trig : ["WC"], prezzo:154 },
        { trig : ["WD"], prezzo:154 },
        { trig : ["WF"], prezzo:154 },
        { trig : ["QC"], prezzo:169 },
        { trig : ["QD"], prezzo:169 },
        { trig : ["QF"], prezzo:169 }
    ],
    aumenti : {
        E : [20,29,39,20,29,39,20,29,39],
        D : [60,60,87,60,60,87,60,60,87],
        L : [67,67,87,67,67,87,67,67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd24b', '0789 Hammer Q Extra 230 Led', '0789_Hammer_Q_Extra_230_Led', 'dani_20170723_30');
