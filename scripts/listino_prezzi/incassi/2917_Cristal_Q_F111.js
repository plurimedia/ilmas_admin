/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/2917_Cristal_Q_F111.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:124 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:124 },
        { trig : ["WB", "WC"], prezzo:144 },
        { trig : ["WD"], prezzo:144 },
        { trig : ["QB", "QC"], prezzo:159 },
        { trig : ["QD"], prezzo:159 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '5829a438ddea1004006e8b36', '2917 Cristal Q', '2917_Cristal_Q_F111', 'dani_20170723_72', 'F111 Led Module');

