/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0909_Bob_Kappa_Led_F111.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:286 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:286 },
        { trig : ["WB", "WC"], prezzo:326 },
        { trig : ["WD"], prezzo:326 },
        { trig : ["QB", "QC"], prezzo:356 },
        { trig : ["QD"], prezzo:356 }
    ],
    aumenti : {
        E : [40,58,40,58,40,58],
        D : [120,120,120,120,120,120],
        L : [134,134,134,134,134,134]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd27d', '0909 Bob Kappa Led', '0909_Bob_Kappa_Led_F111', 'dani_20170816_03', 'F111 Led Module');

