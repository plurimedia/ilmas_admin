/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0731_Berin_Led_Midi.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:115 },
        { trig : ["IQ", "AQ", "BQ", "OQ", "SQ", "TQ"], prezzo:115 },
        { trig : ["WC"], prezzo:135 },
        { trig : ["WQ"], prezzo:135 },
        { trig : ["QC"], prezzo:150 },
        { trig : ["QQ"], prezzo:150 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd257', '0731 Berin Led Midi', '0671_Berin_Led_Midi', 'dani_20170723_25');
