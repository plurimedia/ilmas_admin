/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1712_Bob_Eco_Led_F111.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:145 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:145 },
        { trig : ["WB", "WC"], prezzo:165 },
        { trig : ["WD"], prezzo:165 },
        { trig : ["QB", "QC"], prezzo:180 },
        { trig : ["QD"], prezzo:180 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd279', '1712 Bob Eco Led', '1712_Bob_Eco_Led_F111', 'dani_20170723_54', 'F111 Led Module');
