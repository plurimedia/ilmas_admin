/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1714_Bob_Eco_Led_F111.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:407 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:407 },
        { trig : ["WB", "WC"], prezzo:467 },
        { trig : ["WD"], prezzo:467 },
        { trig : ["QB", "QC"], prezzo:512 },
        { trig : ["QD"], prezzo:512 }
    ],
    aumenti : {
        E : [60,87,117,60,87,117,60,87,117],
        D : [180,180,180,180,180,180,180,180,180],
        L : [201,201,201,201,201,201,201,201,201]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd27b', '1714 Bob Eco Led', '1714_Bob_Eco_Led_F111', 'dani_20170723_58', 'F111 Led Module');
