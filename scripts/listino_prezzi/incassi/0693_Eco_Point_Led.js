/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0693_Eco_Point_Led.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["1B", "2B", "3B"], prezzo:38 }
    ],
    aumenti : {
        E : [20,20,20],
        D : [60,60,60],
        L : [67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd28e', '0693 Eco Point Led', '0693 Eco Point Led ', 'ame_20170914_03', 'V2 Led Module',null , ['V1']);

