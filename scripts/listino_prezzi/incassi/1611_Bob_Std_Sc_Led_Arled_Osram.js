/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1611_Bob_Std_Sc_Led_Arled_Osram.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["2X", "3X"], prezzo:302 },
        { trig : ["2Y", "3Y"], prezzo:330 },
        { trig : ["1F", "2F", "3F"], prezzo:377 }
    ],
    aumenti : {
        E : [0,0,156],
        D : [0,0,348],
        L : [0,0,348]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd26b', '1611 Bob Std Sc Led', '1610_Bob_Std_Sc_Led_Arled_Osram', 'dani_20170723_42', 'Arled Osram');
