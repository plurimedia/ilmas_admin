/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1608_Bob_Std_Sc_Led_F111.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:155 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:155 },
        { trig : ["WB", "WC"], prezzo:175 },
        { trig : ["WD"], prezzo:175 },
        { trig : ["QB", "QC"], prezzo:195 },
        { trig : ["QD"], prezzo:195 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd268', '1608 Bob Std Sc Led', '1608_Bob_Std_Sc_Led_F111', 'dani_20170723_34', 'F111 Led Module');
