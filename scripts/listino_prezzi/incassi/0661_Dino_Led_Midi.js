/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0661_Dino_Led_Midi.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IC", "AC", "BC", "OC", "SC", "TC"], prezzo:124 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:124 },
        { trig : ["WC"], prezzo:144 },
        { trig : ["WD"], prezzo:144 },
        { trig : ["QC"], prezzo:159 },
        { trig : ["QD"], prezzo:159 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd253', '0661 Dino Led Midi', '0661_Dino_Led_Midi', 'dani_20170723_19');
