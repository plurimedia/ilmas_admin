/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0797_Hammer_Midi_160 .js;
 */


var process = require('./../generic.js');

  

var matrix = {
    prezzi : [
        { trig : ["IC", "AC", "BC", "OC", "SC", "TC"], prezzo:89 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:89 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:89 },
         { trig : ["WC"], prezzo:109 },
        { trig : ["WD"], prezzo:109 },
        { trig : ["WE"], prezzo:109 },
        { trig : ["QC"], prezzo:124 },
        { trig : ["QD"], prezzo:124 },
        { trig : ["QE"], prezzo:124 }
    ],
    aumenti : {
        E : [20,29,39,20,29,39,20,29,39],
        D : [60,60,87,60,60,87,60,60,87],
        L : [67,67,87,67,67,87,67,67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd244', '0797 Hammer Midi', '0797_Hammer_Midi', 'dani_20170723_32');
