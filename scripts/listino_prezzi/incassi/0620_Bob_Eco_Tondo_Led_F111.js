/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/0620_Bob_Eco_Tondo_Led_F111.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:116 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:116 },
        { trig : ["WB", "WC"], prezzo:136 },
        { trig : ["WD"], prezzo:136 },
        { trig : ["QB", "QC"], prezzo:151 },
        { trig : ["QD"], prezzo:151 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd282', '0620 Bob Eco Tondo Led', '0620_Bob_Eco_Tondo_Led_F111', 'dani_20170723_14', 'F111 Led Module');

