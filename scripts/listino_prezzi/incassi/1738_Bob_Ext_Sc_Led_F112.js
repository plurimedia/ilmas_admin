/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1738_Bob_Ext_Sc_Led_F112.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:170 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:170 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:170 },
        { trig : ["WB", "WC"], prezzo:190 },
        { trig : ["WD"], prezzo:190 },
        { trig : ["WE"], prezzo:190 },
        { trig : ["QB", "QC"], prezzo:205 },
        { trig : ["QD"], prezzo:205 },
        { trig : ["QE"], prezzo:205 }
    ],
    aumenti : {
        E : [20,29,39,20,29,39,20,29,39],
        D : [60,60,87,60,60,87,60,60,87],
        L : [67,67,87,67,67,87,67,67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd270', '1738 Bob Ext Sc Led', '1738_Bob_Ext_Sc_Led_F112', 'dani_20170723_62', 'F112 Led Module');
