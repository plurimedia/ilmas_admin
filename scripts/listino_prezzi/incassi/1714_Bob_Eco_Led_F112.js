/*
 LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/listino_prezzi/incassi/1714_Bob_Eco_Led_F112.js;
 */



var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:422 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:422 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:422 },
        { trig : ["WB", "WC"], prezzo:482 },
        { trig : ["WD"], prezzo:482 },
        { trig : ["WE"], prezzo:482 },
        { trig : ["QB", "QC"], prezzo:527 },
        { trig : ["QD"], prezzo:527 },
        { trig : ["QE"], prezzo:527 }
    ],
    aumenti : {
        E : [60,87,117,60,87,117,60,87,117],
        D : [180,180,261,180,180,261,180,180,261],
        L : [201,201,261,201,201,261,201,201,261]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd27b', '1714 Bob Eco Led', '1714_Bob_Eco_Led_F112', 'dani_20170723_59', 'F112 Led Module');
