/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/proiettori/_196_listino_proiettori_4.js;
 */


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    fs      = require('fs'),
    aggiornati = 0,
    trascodificaPrezzo="ame_20170720_06"
    logGlobale="";


/*
LISTINO PREZZI
*/

var primoPrezzo     = 185,
    secondoPrezzo   = 205,
    terzoPrezzo     = 220,

    primaMaggiorazione  = 60,
    secondaMaggiorazione= 75,
    terzaMaggiorazione  = 100;


console.log("************* ");
console.log("primoPrezzo ", primoPrezzo);
console.log("secondoPrezzo ", secondoPrezzo);
console.log("terzoPrezzo ", terzoPrezzo);
console.log("primaMaggiorazione + ", primaMaggiorazione);
console.log("secondaMaggiorazione + ", secondaMaggiorazione);
console.log("terzaMaggiorazione + ", terzaMaggiorazione);
console.log("************* ");

console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    var query = 
    {
        generazione: { $in: [
            "G6", 
            "D1" 
            ] },
          modello: { $in: [
            "56b8aaa90de23c95600bd23a" //1322 Viper Mega
            ] }
    }

    Prodotto.find(query).lean().sort({codice:1})
    .exec(
        function(err,prodotti){ 

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, 
            function(prodotto,cb)
            {
                if (prodotto.codice.length==8 || prodotto.codice.length==9)
                {
                    gruppo=0;

                    var p = _.clone(prodotto);

                    if (p.prezzo_old==null){
                        p.prezzo_old = _.clone(p.prezzo); 
                    }

                     var logStr = _.clone(aggiornati) + " codice " + p.codice +  " 5° e 6° car " + p.codice.substring(4,6);
     
                    if (
                        p.codice.substring(4,6)=='DF' ||
                        p.codice.substring(4,6)=='EF' ||
                        p.codice.substring(4,6)=='FF' ||
                        p.codice.substring(4,6)=='UF' ||
                        p.codice.substring(4,6)=='VF' 
                        ){ 
                            p.prezzo = primoPrezzo
                            gruppo = 1;
                            logStr = logStr + " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo
                    }
                    else if (
                        p.codice.substring(4,6)=='XF')
                    {
                        p.prezzo = secondoPrezzo
                        gruppo = 2;
                        logStr = logStr + " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo
                    }
                    else if (p.codice.substring(4,6)=='QF')
                    {
                        p.prezzo = terzoPrezzo
                        gruppo = 3;
                        logStr = logStr + " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo
                    }
                    

                    if (p.codice.length==9 && gruppo>0)
                    {
                        if (p.codice.substring(8,9)=='D')
                        {
                            p.prezzo = p.prezzo + primaMaggiorazione

                            logStr = logStr + " D secon cambio prezzo + 60  " + p.prezzo
                        }
                        else if (p.codice.substring(8,9)=='L')
                        {
                            p.prezzo = p.prezzo + secondaMaggiorazione

                            logStr = logStr + " L secon cambio prezzo + 75  " + p.prezzo
                        }
                        else if (p.codice.substring(8,9)=='P')
                        {
                            p.prezzo = p.prezzo + terzaMaggiorazione

                            logStr = logStr + " P secon cambio prezzo + 100  " + p.prezzo
                        }
                    }

                     if (gruppo>0){
                        var update = {};
                        update.$set = {};
                        update.$set.prezzo = p.prezzo;
                        update.$set.prezzo_old = p.prezzo_old;
                        update.$set.trascodificaPrezzo = trascodificaPrezzo
                        update.$set.mdate = new Date();

                        bulk.find( { _id: p._id } ).updateOne(update);

                        aggiornati ++;
                        
                        console.log(logStr); 

                        logGlobale = logGlobale + logStr + "\n"; 
                    }
                   
                }
                cb();
            },
            function(err)
            {
                if(err){
                   console.log(err)
                    throw err;
                } 
 
                if(aggiornati > 0) {
                 
                    bulk.execute(function(err, result){
                        if(err)
                            throw err;

                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                        mongoose.connection.close(); 

                        fs.writeFile(".tmp/_196_listino_proiettori_4.txt",

                             logGlobale,

                             function(err) {
                                if(err) {
                                    console.log("Log was not saved!", err);
                                    throw err;
                                }
                                else
                                    console.log("Log was saved!");
                            }
                        );

                    })
                   
                }
                else {
                    console.log('Non ci sono prodotti da aggiornare')
                    mongoose.connection.close();
                }
            }
        )
    })

 
}
);
