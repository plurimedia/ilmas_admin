/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/proiettori/1232_ten_F112.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IC", "AC", "BC","OC", "SC", "TC"], prezzo:220 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:229 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:239 },
        { trig : ["WC"], prezzo:240 },
        { trig : ["WD"], prezzo:249 },
        { trig : ["WE"], prezzo:259 },
        { trig : ["QC"], prezzo:255 },
        { trig : ["QD"], prezzo:264 },
        { trig : ["QE"], prezzo:274 }
    ],
    aumenti : {
        D : [60,60,60,60,60,60,60,60,60],
        L : [75,75,75,75,75,75,75,75,75],
        P : [100,100,100,100,100,100,100,100,100]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd241', '1232 Ten', '1232 Ten', 'dani_20170906_04','F112 Led Module');


