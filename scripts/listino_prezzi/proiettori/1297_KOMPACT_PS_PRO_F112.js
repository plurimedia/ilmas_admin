/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/proiettori/1297_KOMPACT_PS_PRO_F112.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:185 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:194 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:204 },
        { trig : ["WB", "WC"], prezzo:205 },
        { trig : ["WD"], prezzo:214 },
        { trig : ["WE"], prezzo:224 },
        { trig : ["QB", "QC"], prezzo:220 },
        { trig : ["QD"], prezzo:229 },
        { trig : ["QE"], prezzo:239 },
    ]
}

process.execute(matrix, '56b8aaa90de23c95600bd23f', '1297 KOMPACT PS PRO F111', '1297_KOMPACT_PS_PRO_F111', 'dani_20170906_04','F112 Led Module');


