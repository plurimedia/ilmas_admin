/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/proiettori/1297_KOMPACT_PS_PRO_F111.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:180 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:189 },
        { trig : ["WB", "WC"], prezzo:200 },
        { trig : ["WD"], prezzo:209 },
        { trig : ["QB", "QC"], prezzo:215 },
        { trig : ["QD"], prezzo:224 }
    ]
}

process.execute(matrix, '56b8aaa90de23c95600bd23f', '1297 KOMPACT PS PRO F111', '1297_KOMPACT_PS_PRO_F111', 'dani_20170906_04','F111 Led Module');


