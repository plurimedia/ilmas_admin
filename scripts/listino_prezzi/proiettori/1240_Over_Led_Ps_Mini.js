/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/proiettori/1240_Over_Led_Ps_Mini.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["R","S","6","Z","7","B"], prezzo:133 },
        { trig : ["W"], prezzo:153 },
        { trig : ["Q"], prezzo:168 }
    ],
    aumenti : {
        D : [60,60,60],
        L : [75,75,75],
        P : [100,100,100]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd22b', '1240 Over Led Ps Mini', '1240_Over_Led_Ps_Mini', 'dani_20170906_01', undefined, '4');
