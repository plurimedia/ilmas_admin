/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/proiettori/1321_Viper_Midi.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:138 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:147 },
        { trig : ["WB", "WC"], prezzo:158 },
        { trig : ["WD"], prezzo:167 },
        { trig : ["QB", "QC"], prezzo:173 },
        { trig : ["QD"], prezzo:182 }
    ],
    aumenti : {
        D : [60,60,60,60,60,60],
        L : [75,75,75,75,75,75],
        P : [100,100,100,100,100,100]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd238', '1321 Viper Midi', '1321_Viper_Midi', 'dani_20170907_02');


