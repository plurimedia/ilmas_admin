/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/proiettori/_210_listino_proiettori_9.js;
 */

var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    fs      = require('fs'),
    aggiornati = 0,
    logGlobale="",
    trascodificaPrezzo="ame_20170720_11";
 

/*
LISTINO PREZZI
*/

var _1Prezzo     = 99,
    _2Prezzo     = 99,
    _3Prezzo     = 99,
    _4Prezzo     = 119,
    _5Prezzo     = 119,
    _6Prezzo     = 119,
    _7Prezzo     = 134,
    _8Prezzo     = 134,
    _9Prezzo     = 134,


    _1MaggiorazioneE  = 20,
    _1MaggiorazioneD  = 60,
    _1MaggiorazioneL  = 67,

    _2MaggiorazioneE  = 29,
    _2MaggiorazioneD  = 60,
    _2MaggiorazioneL  = 67,

    _3MaggiorazioneE  = 39,
    _3MaggiorazioneD  = 60,
    _3MaggiorazioneL  = 67,

    _4MaggiorazioneE  = 20,
    _4MaggiorazioneD  = 60,
    _4MaggiorazioneL  = 67,

    _5MaggiorazioneE  = 29,
    _5MaggiorazioneD  = 60,
    _5MaggiorazioneL  = 67,

    _6MaggiorazioneE  = 39,
    _6MaggiorazioneD  = 60,
    _6MaggiorazioneL  = 67; 

    _7MaggiorazioneE  = 20,
    _7MaggiorazioneD  = 60,
    _7MaggiorazioneL  = 67; 

    _8MaggiorazioneE  = 29,
    _8MaggiorazioneD  = 60,
    _8MaggiorazioneL  = 67; 

    _9MaggiorazioneE  = 39,
    _9MaggiorazioneD  = 87,
    _9MaggiorazioneL  = 87; 
 

console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    var query = 
    {
        generazione: { $in: [
            "G6", 
            "D1" 
            ] },
          modello: { $in: [
            "56b8aaa90de23c95600bd244" //0797 Hammer Midi 160
            ] }
    }

    Prodotto.find(query).lean().sort({codice:1})
    .exec(
        function(err,prodotti){ 

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti,
            function(prodotto,cb)
            {
                if (prodotto.codice.length==8 || prodotto.codice.length==9)
                {
                    var p = _.clone(prodotto);

                    if (p.prezzo_old==null){
                        p.prezzo_old = _.clone(p.prezzo); 
                    }

                    var gruppo=0;

                    var logStr = "";
     
                    if (
                        p.codice.substring(4,6)=='IC' ||
                        p.codice.substring(4,6)=='AC' ||
                        p.codice.substring(4,6)=='BC' ||
                        p.codice.substring(4,6)=='OC' ||
                        p.codice.substring(4,6)=='SC' ||
                        p.codice.substring(4,6)=='TC' 
                        ){ 
                            p.prezzo = _1Prezzo

                            logStr = _.clone(aggiornati) + " codice " + p.codice +  " 5° e 6° car " + p.codice.substring(4,6) +
                                     " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo

                            gruppo = 1;
                    }
                    else if (
                        p.codice.substring(4,6)=='ID' ||
                        p.codice.substring(4,6)=='AD' ||
                        p.codice.substring(4,6)=='BD' ||
                        p.codice.substring(4,6)=='OD' ||
                        p.codice.substring(4,6)=='SD' ||
                        p.codice.substring(4,6)=='TD'
                    )
                    {
                        p.prezzo = _2Prezzo

                        logStr = _.clone(aggiornati) + " codice " + p.codice +  " 5° e 6° car " + p.codice.substring(4,6) +
                                     " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo

                        gruppo = 2;
                    }
                    else if (
                        p.codice.substring(4,6)=='IE' ||
                        p.codice.substring(4,6)=='AE' ||
                        p.codice.substring(4,6)=='BE' ||
                        p.codice.substring(4,6)=='OE' ||
                        p.codice.substring(4,6)=='SE' ||
                        p.codice.substring(4,6)=='TE'
                    )
                    {
                        p.prezzo = _3Prezzo

                        logStr = _.clone(aggiornati) + " codice " + p.codice +  " 5° e 6° car " + p.codice.substring(4,6) +
                                     " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo

                        gruppo = 3;
                    }
                    else if (p.codice.substring(4,6)=='WC')
                    {
                        p.prezzo = _4Prezzo

                        logStr = _.clone(aggiornati) + " codice " + p.codice +  " 5° e 6° car " + p.codice.substring(4,6) +
                                    " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo

                        gruppo = 4;
                    }
                    else if (p.codice.substring(4,6)=='WD')
                    {
                        p.prezzo = _5Prezzo

                        logStr = _.clone(aggiornati) + " codice " + p.codice +  " 5° e 6° car " + p.codice.substring(4,6) +
                                    " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo

                        gruppo = 5

                    }
                    else if (p.codice.substring(4,6)=='WE')
                    {
                        p.prezzo = _6Prezzo

                        logStr = _.clone(aggiornati) + " codice " + p.codice +  " 5° e 6° car " + p.codice.substring(4,6) +
                                    " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo

                        gruppo = 6
                    }
                     else if (p.codice.substring(4,6)=='QC')
                    {
                        p.prezzo = _7Prezzo

                        logStr = _.clone(aggiornati) + " codice " + p.codice +  " 5° e 6° car " + p.codice.substring(4,6) +
                                    " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo

                        gruppo = 7
                    }
                    else if (p.codice.substring(4,6)=='QD')
                    {
                        p.prezzo = _8Prezzo

                        logStr = _.clone(aggiornati) + " codice " + p.codice +  " 5° e 6° car " + p.codice.substring(4,6) +
                                    " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo

                        gruppo = 8
                    }
                    else if (p.codice.substring(4,6)=='QE')
                    {
                        p.prezzo = _9Prezzo

                        logStr = _.clone(aggiornati) + " codice " + p.codice +  " 5° e 6° car " + p.codice.substring(4,6) +
                                    " old_prezzo " + p.prezzo_old+  " primo cambio prezzo  " + p.prezzo

                        gruppo = 9
                    }
                    

                    if (p.codice.length==9 && gruppo > 0)
                    {
                        if (p.codice.substring(8,9)=='E')
                        {
                            if (gruppo == 1){
                                maggiorazione = _1MaggiorazioneE
                            }
                            else  if (gruppo == 2){
                                maggiorazione = _2MaggiorazioneE
                            }
                            else  if (gruppo == 3){
                                maggiorazione = _3MaggiorazioneE
                            }
                            else  if (gruppo == 4){
                                maggiorazione = _4MaggiorazioneE
                            }
                            else  if (gruppo == 5){
                                maggiorazione = _5MaggiorazioneE
                            }
                            else  if (gruppo == 6){
                                maggiorazione = _6MaggiorazioneE
                            }
                            else  if (gruppo == 7){
                                maggiorazione = _7MaggiorazioneE
                            }
                             else  if (gruppo == 8){
                                maggiorazione = _8MaggiorazioneE
                            }
                             else  if (gruppo == 9){
                                maggiorazione = _9MaggiorazioneE
                            }
                            p.prezzo = p.prezzo + maggiorazione

                            logStr = logStr + " D secon cambio prezzo +  " + maggiorazione + " " + p.prezzo
                        }
                        else if (p.codice.substring(8,9)=='D')
                        {
                            if (gruppo == 1){
                                maggiorazione = _1MaggiorazioneD
                            }
                            else  if (gruppo == 2){
                                maggiorazione = _2MaggiorazioneD
                            }
                            else  if (gruppo == 3){
                                maggiorazione = _3MaggiorazioneD
                            }
                            else  if (gruppo == 4){
                                maggiorazione = _4MaggiorazioneD
                            }
                            else  if (gruppo == 5){
                                maggiorazione = _5MaggiorazioneD
                            }
                            else  if (gruppo == 6){
                                maggiorazione = _6MaggiorazioneD
                            }
                            else  if (gruppo == 7){
                                maggiorazione = _7MaggiorazioneD
                            }
                             else  if (gruppo == 8){
                                maggiorazione = _8MaggiorazioneD
                            }
                             else  if (gruppo == 9){
                                maggiorazione = _9MaggiorazioneD
                            }
                            p.prezzo = p.prezzo + maggiorazione

                            logStr = logStr + " D secon cambio prezzo +  " + maggiorazione + " " + p.prezzo
                        }
                        else if (p.codice.substring(8,9)=='L')
                        {
                           if (gruppo == 1){
                                maggiorazione = _1MaggiorazioneL
                            }
                            else  if (gruppo == 2){
                                maggiorazione = _2MaggiorazioneL
                            }
                            else  if (gruppo == 3){
                                maggiorazione = _3MaggiorazioneL
                            }
                            else  if (gruppo == 4){
                                maggiorazione = _4MaggiorazioneL
                            }
                            else  if (gruppo == 5){
                                maggiorazione = _5MaggiorazioneL
                            }
                            else  if (gruppo == 6){
                                maggiorazione = _6MaggiorazioneL
                            }
                            else  if (gruppo == 7){
                                maggiorazione = _7MaggiorazioneL
                            }
                             else  if (gruppo == 8){
                                maggiorazione = _8MaggiorazioneL
                            }
                             else  if (gruppo == 9){
                                maggiorazione = _9MaggiorazioneL
                            }
                            p.prezzo = p.prezzo + maggiorazione

                            logStr = logStr + " L secon cambio prezzo +  " + maggiorazione + " " + p.prezzo
                        }
                    }

                    if (gruppo > 0){

                    var update = {};
                    update.$set = {};
                    update.$set.prezzo = p.prezzo;
                    update.$set.prezzo_old = p.prezzo_old;
                    update.$set.trascodificaPrezzo = trascodificaPrezzo
                    update.$set.mdate = new Date();

                    bulk.find( { _id: p._id } ).updateOne(update);

                    aggiornati ++;
                    
                    console.log(logStr); 

                    logGlobale = logGlobale + logStr + "\n"; 
                    }
                   
                }
                cb();
            },
            function(err)
            {
                if(err){
                   console.log(err)
                    throw err;
                } 
 
                if(aggiornati > 0) {
                 
                    bulk.execute(function(err, result){
                        if(err)
                            throw err;

                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                        mongoose.connection.close(); 

                        fs.writeFile(".tmp/_210_listino_proiettori_9.txt",

                             logGlobale,

                             function(err) {
                                if(err) {
                                    console.log("Log was not saved!", err);
                                    throw err;
                                }
                                else
                                    console.log("Log was saved!");
                            }
                        );

                    })
                   
                }
                else {
                    console.log('Non ci sono prodotti da aggiornare')
                    mongoose.connection.close();
                }
            }
        )
    })

 
}
);
