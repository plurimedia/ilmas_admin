/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/proiettori/0796_Hammer_Mini_120.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IB", "AB", "BB", "IC", "AC", "BC", "OB", "SB", "TB", "OC", "SC", "TC"], prezzo:70 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:70 },
        { trig : ["WB", "WC"], prezzo:90 },
        { trig : ["WD"], prezzo:90 },
        { trig : ["QB", "QC"], prezzo:105 },
        { trig : ["QD"], prezzo:105 }
    ],
    aumenti : {
        E : [20,29,20,29,20,29],
        D : [60,60,60,60,60,60],
        L : [67,67,67,67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd243', '0796 Hammer Mini 120', '0796_Hammer_Mini_120', 'ame_20170720_01');

