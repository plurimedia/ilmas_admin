/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/proiettori/1292_Over_Led_Ps2_Maxi.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:157 },
        { trig : ["IE", "AE", "BE", "OE", "SE", "TE"], prezzo:167 },
        { trig : ["WD"], prezzo:177 },
        { trig : ["WE"], prezzo:187 },
        { trig : ["QD"], prezzo:192 },
        { trig : ["QE"], prezzo:202 }
    ],
    aumenti : {
        D : [60,60,60,60,60,60],
        L : [75,75,75,75,75,75],
        P : [100,100,100,100,100,100]
    }
}
process.execute(matrix, '56b8aaa90de23c95600bd232', '1292 Over Led Ps2 Maxi', '1292_Over_Led_Ps2_Maxi', 'dani_20170906_05');


