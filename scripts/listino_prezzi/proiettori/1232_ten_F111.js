/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/proiettori/1232_ten_F111.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["IC", "AC", "BC","OC", "SC", "TC"], prezzo:215 },
        { trig : ["ID", "AD", "BD", "OD", "SD", "TD"], prezzo:224 },
        { trig : ["WC"], prezzo:235 },
        { trig : ["WD"], prezzo:244 },
        { trig : ["QC"], prezzo:250 },
        { trig : ["QD"], prezzo:259 }
    ],
    aumenti : {
        D : [60,60,60,60,60,60],
        L : [75,75,75,75,75,75],
        P : [100,100,100,100,100,100]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd241', '1232 Ten', '1232 Ten', 'dani_20170906_04','F111 Led Module');


