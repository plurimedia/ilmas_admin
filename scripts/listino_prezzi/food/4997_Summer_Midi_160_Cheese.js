/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/4997_Summer_Midi_160_Cheese.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["GD"], prezzo:243 }
    ],
    aumenti : {
        D : [31],
        L : [38]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2c5', '4997 Summer Midi 160', '4997_Summer_Midi_160_Cheese', 'dani_20170731_26');
