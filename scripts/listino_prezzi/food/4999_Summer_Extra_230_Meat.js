/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/4999_Summer_Extra_230_Meat.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["LD"], prezzo:346 },
        { trig : ["LE"], prezzo:356 }
    ],
    aumenti : {
        D : [31,48],
        L : [38,48]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2c8', '4999 Summer Extra 230', '4998_Summer_Extra_230_Meat', 'dani_20170814_01');
