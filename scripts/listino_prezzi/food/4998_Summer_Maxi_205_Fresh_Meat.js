/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/4998_Summer_Maxi_205_Fresh_Meat.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["ND"], prezzo:307 },
        { trig : ["NE"], prezzo:317 }
    ],
    aumenti : {
        D : [31,48],
        L : [38,48]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2c6', '4998 Summer Maxi 205', '4998_Summer_Maxi_205_Fresh_Meat', 'dani_20170731_29');
