/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/0798_Hammer_Maxi_205_Meat.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["LD"], prezzo:142 },
        { trig : ["LF"], prezzo:142 }
    ],
    aumenti : {
        E : [29,39],
        D : [60,87],
        L : [67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd245', '0798 Hammer Maxi 205', '0798_Hammer_Maxi_205_Meat', 'dani_20170728_07');
