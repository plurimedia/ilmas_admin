/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/1241_Over_Led_Ps_Midi_Fish.js;
 */
var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["TC"], prezzo:138 }
    ],
    aumenti : {
        D : [60],
        L : [75]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd22c', '1241 Over Led Ps Midi', '1241_Over_Led_Ps_Midi_Fish', 'dani_20170724_17');

