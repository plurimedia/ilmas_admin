/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/0797_Hammer_Midi_160_Bread.js;
 */
var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["JD"], prezzo:99 },
        { trig : ["JE"], prezzo:99 }
    ],
    aumenti : {
        E : [29,39],
        D : [60,87],
        L : [67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd244', '0797 Hammer Midi 160', '0797_Hammer_Midi_160_Bread', 'dani_20170728_06');
