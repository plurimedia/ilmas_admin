/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/1242_Over_Led_Ps_Maxi_Bread.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["JD"], prezzo:192 },
        { trig : ["JE"], prezzo:202 }
    ],
    aumenti : {
        D : [60,60],
        L : [75,75]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd22d', '1242 Over Led Ps Maxi', '1242_Over_Led_Ps_Maxi_Bread', 'dani_20170814_12');

