/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/4998_Summer_Maxi_205_Bread.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["JD"], prezzo:307 },
        { trig : ["JS"], prezzo:317 }
    ],
    aumenti : {
        D : [31,48],
        L : [38,48]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2c6', '4998 Summer Maxi 205', '4998_Summer_Maxi_205_Bread', 'dani_20170731_33');
