/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/1241_Over_Led_Ps_Midi_Bread.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["JC"], prezzo:173 }
    ],
    aumenti : {
        D : [60],
        L : [75]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd22c', '1241 Over Led Ps Midi', '1241_Over_Led_Ps_Midi_Bread', 'dani_20170724_16');

