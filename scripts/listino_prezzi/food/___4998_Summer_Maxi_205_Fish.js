/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/4998_Summer_Maxi_205_Fish.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["TD"], prezzo:272 },
        { trig : ["TS"], prezzo:282 }
    ],
    aumenti : {
        D : [31,48],
        L : [38,48]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2c6', '4998 Summer Maxi 205', '4998_Summer_Maxi_205_Fish', 'dani_20170731_30');
