/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/0797_Hammer_Midi_160_food_all.js;
 */
var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["LD","ND","JD"], prezzo:134 },
        { trig : ["LE","NE","JE"], prezzo:134},
        { trig : ["GD"], prezzo:99 },
        { trig : ["GE"], prezzo:99 }
    ],
    aumenti : {
        E : [29,39,29,39],
        D : [60,87,60,87],
        L : [67,87,67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd244', '0797 Hammer Midi 160_all_food', '0797_Hammer_Midi_160_all_food', 'dani_20170728_06');
