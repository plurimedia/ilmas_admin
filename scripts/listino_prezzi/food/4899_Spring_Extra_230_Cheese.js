/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/4899_Spring_Extra_230_Cheese.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["GD"], prezzo:274 },
        { trig : ["GE"], prezzo:284 }
    ],
    aumenti : {
        D : [31,48],
        L : [38,48]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd2a1', '4899 Spring Extra 230', '4899_Spring_Extra_230_Cheese', 'dani_20170731_20');
