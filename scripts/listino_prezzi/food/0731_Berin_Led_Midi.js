/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/0731_Berin_Led_Midi.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["NQ"], prezzo:141 },
       // { trig : ["TQ"], prezzo:141 },
       // { trig : ["SQ"], prezzo:106 },
        { trig : ["GQ"], prezzo:106 },
        { trig : ["JQ"], prezzo:141 }
    ],
    aumenti : {
        E : [29,29,29,29,29],
        D : [60,60,60,60,60],
        L : [67,67,67,67,67]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd257', '0731 Berin Led Midi', '0671_Berin_Led_Midi', 'dani_20170724_01');


