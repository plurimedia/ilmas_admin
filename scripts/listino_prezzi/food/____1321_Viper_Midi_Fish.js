/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/1321_Viper_Midi_Fish.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["TC"], prezzo:138 }
    ],
    aumenti : {
        D : [60],
        L : [75]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd238', '1321 Viper Midi', '1321_Viper_Midi_Fish', 'dani_20170724_24');

