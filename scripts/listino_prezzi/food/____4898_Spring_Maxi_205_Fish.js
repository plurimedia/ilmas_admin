/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/4898_Spring_Maxi_205_Fish.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["TD"], prezzo:237 }
    ],
    aumenti : {
        D : [31],
        L : [38]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd29f', '4898 Spring Maxi 205', '4898_Spring_Maxi_205_Fish', 'dani_20170731_12');
