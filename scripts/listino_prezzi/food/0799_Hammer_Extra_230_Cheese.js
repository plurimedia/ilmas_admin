/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/0799_Hammer_Extra_230_Cheese.js;
 */

var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["GD"], prezzo:118 },
        { trig : ["GF"], prezzo:118 }
    ],
    aumenti : {
        E : [29,39],
        D : [60,87],
        L : [67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd247', '0799 Hammer Extra 230', '0799_Hammer_Extra_230_Cheese', 'dani_20170731_08');
