/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/listino_prezzi/food/0732_Berin_Led_Maxi_Meat.js;
 */


var process = require('./../generic.js');

var matrix = {
    prezzi : [
        { trig : ["LD"], prezzo:153 },
        { trig : ["LE"], prezzo:153 }
    ],
    aumenti : {
        E : [29,39],
        D : [60,87],
        L : [67,87]
    }
}

process.execute(matrix, '56b8aaa90de23c95600bd258', '0732 Berin Led Maxi', '0732_Berin_Led_Maxi_Meat', 'dani_20170724_14');
