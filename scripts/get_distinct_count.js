/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/get_distinct_count;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    ModelloSchema = require('../server/models/modello'),
    Modello = mongoose.model('Modello'),
    aggiornati = 0,
    ObjectId = require('mongodb').ObjectID,
    fs = require('fs'),
    pathModelli = 'scripts/_modelli.json',
    modelli = JSON.parse(fs.readFileSync(pathModelli, 'utf8'));

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console
 
 var aggiornati=0;
    Modello.find() 
    .limit (25)
     .skip(440)
    .exec(
        function (err, modelli)
        { 
            async.each(modelli, 
                    function(modello,cb){ 

                       

                        Prodotto.aggregate(
                            [
                                {
                                    $match : { modello : modello._id }
                                },
                                {
                                    $group: {
                                        _id:{modello:"$modello", generazione:"$generazione"}, count:{$sum:1}
                                    } 
                                }
                            ])
                            .exec(
                                function(err,totaleProdotti){ 
                     
                                    if (err)
                                        throw err;
                                    else
                                    {


                                        

                                        var totG6 = 0;
                                        var totD1 = 0;
                                        var totG5 = 0;
                                        var totG4 = 0;
                                        var totVuoto = 0;

                                         G6 = _.filter(totaleProdotti, function(c){
                                             return  c._id.generazione==='G6'
                                         });

                                         D1 = _.filter(totaleProdotti, function(c){
                                             return  c._id.generazione==='D1'
                                         });

                                         G5 = _.filter(totaleProdotti, function(c){
                                             return  c._id.generazione==='G5'
                                         });

                                         G4 = _.filter(totaleProdotti, function(c){
                                             return  c._id.generazione==='G4'
                                         });

                                         vuoto = _.filter(totaleProdotti, function(c){
                                             return  c._id.generazione===''
                                         });


                                        if (G6.length==0)
                                            totG6 = 0;
                                        else
                                            totG6 = G6[0].count

                                        if (D1.length==0)
                                            totD1 = 0;
                                        else
                                            totD1 = D1[0].count

                                        if (G5.length==0)
                                            totG5 = 0;
                                        else
                                            totG5 = G5[0].count

                                        if (G4.length==0)
                                            totG4 = 0;
                                        else
                                            totG4 = G4[0].count

                                        if (vuoto.length==0)
                                            totVuoto = 0;
                                        else
                                            totVuoto = vuoto[0].count



                                        console.log(modello.nome, ',', totG6, ',', totD1, ',', totG5, ',', totG4, ',', totVuoto)

                                         aggiornati++
                                         cb();
                                    }
                                }
                            )

                    },
                    function(err)
                    {

                        if(err)
                            throw err; 
                        mongoose.connection.close();
                        
                    }
                )
        }   
    ) 
})