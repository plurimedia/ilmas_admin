/*
LANCIARE LA PROCEDURA
node --max-old-space-size=5000 scripts/modificacontatti_i_userd_id;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ContattoSchema = require('../server/models/contatto'),
    Contatto = mongoose.model('Contatto'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    Contatto.find() 
    .exec(function(err,contatti){

        console.log('trovati  '+contatti.length+' contatti')

        var bulk = Contatto.collection.initializeOrderedBulkOp();
        async.each(contatti, function(contatto,cb){

            if (!contatto.i_user_id || contatto.i_user_id==null || contatto.i_user_id==''){

                var userId = contatto.m_user_id;

                if (!userId || userId==null || userId==''){
                    userId = 'auth0|56b1bf38f72ee4b733b186df'; //paparelli
                }

                console.log("contatto.m_m_user_id", contatto.m_m_user_id, " userId", userId)

                var update = {};
     
                console.log("aggiornamento contatto id = ", contatto.nome);
                
                 var c = _.clone(contatto);  

                update.$set = {};
                update.$set.i_user_id = userId;
                bulk.find( { _id: contatto._id } ).updateOne(update);
                aggiornati ++;  

            }
            cb();
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0) {
             
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
               
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

        })
    })

 
     
}



);
