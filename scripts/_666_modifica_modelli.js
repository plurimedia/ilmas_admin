/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=2000 scripts/_modifica_modelli;
 */

var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ModelloSchema = require('../server/models/modello'),
    Modello = mongoose.model('Modello'),
    csv = require("fast-csv"),
    fs = require('fs'),
    ObjectId = require('mongodb').ObjectID,
    aggiornati = 0;

mongoose.connect(db);

mongoose.connection.once('open', function () {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    csvpath = "scripts/exportModelli.csv",
    modelli = [],
    stream = fs.createReadStream(csvpath);

    csv.fromStream(stream, {
        headers: true,
        delimiter: ',',
        discardUnmappedColumns: true
    })
    .on("data", function (data) {

        var record = {
            _id: data._id,
            peso: data.peso,
            volume: data.volume,
            ecolamp: data.ecolamp
        };
        modelli.push(record);

    })
    .on("end", function () {

            var bulk = Modello.collection.initializeOrderedBulkOp();
            async.each(modelli,
                function (m, cb) {

                    console.log("cerco", m._id, m.peso, m.volume, m.ecolamp);

                    var update = {};

                    update.$set = {};
                    update.$set.peso = m.peso;
                    update.$set.volume = m.volume;
                    update.$set.ecolamp = m.ecolamp;
                    bulk.find({ _id : ObjectId(m._id) }).updateOne(update);

                    /*Modello.collection.bulkWrite([{
                        updateOne: {
                            "filter": {"_id": m._id},
                            "update": {$set: {peso: m.peso, volume: m.volume, ecolamp: m.ecolamp}}
                        }
                    }]);*/
                    aggiornati ++;
                    cb(null);


                },function(err) {
                    if(err)
                        throw err;

                    if(aggiornati > 0) {

                        bulk.execute(function(err, result){
                            if(err)
                                throw err;

                            console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti', result.isOk(), result.nModified)
                            mongoose.connection.close();

                        })

                    }
                    else {
                        console.log('Non ci sono prodotti da aggiornare')
                        mongoose.connection.close();
                    }

                });
            });

});