/*
    estrae i codici senza foto
*/


var mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = 'mongodb://localhost/ilmas_admin',
    ModelloSchema = require('../server/models/modello'),
    Modello = mongoose.model('Modello'),
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    fs = require('fs'),
    csv = require("fast-csv");
   

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    Prodotto.find({foto:{$exists:false}}).lean()
    .populate('modello','nome')
    .exec(function(err,prodotti){
    
        var output = _.map(prodotti,function(p){
            
            var output = {codice: p.codice, modello:''};
            
            if(p.modello)
                output.modello = p.modello.nome;
            
            return output;
        });
        
        csv
        .writeToPath(".tmp/prodotti_senza_foto.csv", output, {
            headers: true
        })
        .on("finish", function () {
             mongoose.connection.close();
        });
       
    })
});

