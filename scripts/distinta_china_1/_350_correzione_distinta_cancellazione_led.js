/*
LANCIARE LA PROCEDURA
node --max-old-space-size=5000 scripts/_350_correzione_distinta_cancellazione_led;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    ComponenteSchema = require('../server/models/componente'),
    Prodotto = mongoose.model('Prodotto'),
    Componente = mongoose.model('Componente'), 
    aggiornati = 0;

     var pathProdotti = 'scripts/Prodotto.json';
     var prodottiAll = JSON.parse(fs.readFileSync(pathProdotti, 'utf8'));

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    Prodotto.find({distintabase:'china_1'})
   
    .lean() 
    .exec(function(err,prodotti){ 


        var bulk = Prodotto.collection.initializeOrderedBulkOp();

         
         console.log("prodotti.length", prodotti.length)

        async.each(prodotti, 

            function(prodotto,cb){  

                    var pClone = _.clone(prodotto)
                                         
                    var componenti = [];

                    var modifica = false; 

                    var daModificare = _.filter(_.clone(pClone.componenti), 
                        function(c){
                            return c._id.toString() === '5819c1acd5718e690d56a934' || c._id.toString() === '5819c18bd5718e690d56a933';
                        }
                    );

                    if (daModificare.length>0){

console.log("pClone.codice", pClone.codice)

                    async.each(pClone.componenti,
                        function (componente, callback) {

                            if (componente._id.toString()=='5819c1acd5718e690d56a934'){
                                
                                console.log("modifico con il 592ed840b48023040043146d(W)")
                                componente = {
                                    qt: 1,
                                    _id: '592ed840b48023040043146d' // 28089602851(W)
                                }
                            }
                            else if (componente._id.toString()=='5819c18bd5718e690d56a933'){
                                
                                console.log("modifico con il 592ed771b480230400431466(V)")
                                componente = {
                                    qt: 1,
                                    _id: '592ed771b480230400431466' // 28089602691(V) 
                                }
                            }
                            else{ 
                                componente = {
                                    qt: 1,
                                    _id: componente._id
                                }
                            }
                            
                            componenti.push(componente)
 

                            callback();
                        },
                        function(err){}
                    )


                    

                    

                    console.log("predispongo l'aggiornamento del codice " , pClone.codice)
                    console.log("aggiorno il codice  " , pClone.codice , " con i componenti ", componenti)
                    var update = {};
                    update.$set = {}; 
                    update.$set.componenti = componenti;
                    bulk.find( { _id: pClone._id } ).updateOne(update); 

                    aggiornati++;

                    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@" )

                    }
            

            cb();
          
           
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0) {
             
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
               
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

        })
    }) 
}



);
