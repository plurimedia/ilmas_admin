/*
LANCIARE LA PROCEDURA
node --max-old-space-size=5000 scripts/_310_distinta_base_d1;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    fs = require('fs'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    aggiornati = 0;

     var pathProdotti = 'scripts/Prodotto.json';
     var prodottiAll = JSON.parse(fs.readFileSync(pathProdotti, 'utf8'));

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    Prodotto.find({generazione:'D1'})
    .lean() 
    .exec(function(err,prodotti){ 


        var bulk = Prodotto.collection.initializeOrderedBulkOp();

         
         console.log("prodotti.length", prodotti.length)

        async.each(prodotti, 

            function(prodotto,cb){ 
           
            if (prodotto.codice){

            if  (
                    (
                    prodotto.codice.indexOf("1240")===0 || 
                    prodotto.codice.indexOf("1290")===0 ||
                    prodotto.codice.indexOf("1330")===0 || 
                    prodotto.codice.indexOf("0660")===0 || 
                    prodotto.codice.indexOf("0670")===0 
                    ) 
                    && prodotto.padre
                )
            { 

                var pClone = _.clone(prodotto);

                console.log(  " aggiornamento prodotto id = ", pClone.codice, " codicePadre ", pClone.padre);

 

                     prodottoPadre = _.clone(_.filter( prodottiAll , function(p){
                          return p.codice==pClone.padre
                    }))[0];

                      

                     console.log("prodottoPadre.codice ", prodottoPadre.codice)
                     console.log("prodottoPadre._id.$oid ", prodottoPadre._id.$oid)
                     console.log("prodottoPadre.componenti ", prodottoPadre.componenti)
                     
                                         
                    var componenti = [];

                    async.each(prodottoPadre.componenti,
                        function (componente, callback) {

                            console.log(componente._id.$oid)

                            if (componente._id.$oid=='5819bf2ad5718e690d56a921'){
                                componente = {
                                    qt: 1,
                                    _id: '5915b5ba5c28050400609f67'
                                }
                            }
                            else if (componente._id.$oid=='5819bf5fd5718e690d56a922'){
                                componente = {
                                    qt: 1,
                                    _id: '5915b60c5c28050400609f6b'
                                }
                            }
                            else if (componente._id.$oid=='5819c130d5718e690d56a930'){
                                componente = {
                                    qt: 1,
                                    _id: '5915b64d5c28050400609f6e'
                                }
                            }
                            else if (componente._id.$oid=='5819c159d5718e690d56a931'){
                                componente = {
                                    qt: 1,
                                    _id: '5915b68a5c28050400609f70'
                                }
                            }
                            else{ 
                                componente = {
                                    qt: 1,
                                    _id: componente._id.$oid
                                }
                            }
                            
                            componenti.push(componente)
 

                            callback();
                        },
                        function(err){}
                    )

                    console.log("predispongo l'aggiornamento del codice " , pClone.codice)
                    console.log("aggiorno il codice  " , pClone.codice , " con i componenti ", componenti)
                    var update = {};
                    update.$set = {}; 
                    update.$set.componenti = componenti;
                    update.$set.distintabase = "china_1";
                    bulk.find( { _id: pClone._id } ).updateOne(update); 

                    aggiornati++;

                    console.log("@@@@@@@@@@@@@@@@@@@@@@@@@@@" )

                }


            }

            cb();
          
           
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0 ) {
             
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
               
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

        })
    }) 
}



);
