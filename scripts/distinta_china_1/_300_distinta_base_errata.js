/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/distinta_china_1/_300_distinta_base_errata;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    fs      = require('fs'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    Componentechema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
    aggiornati = 0,
    logGlobale="";

    console.log(db)

    mongoose.connect(db);

    mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console
    var logStr = "";
    var contatore = 0;

        var query = {
            componenti:{ $ne: null },
            distintabase:'china_1',
            $or: [ 
                 { generazione: 'G6' }, { generazione: 'D1' },  { kit: true } , { criptato: true }
                // { kit: true } , { criptato: true }
            ],
            $where: "this.componenti.length ==  2"  ,

            

        }

        Prodotto.find(query).lean()
       .populate(
        {
            path: 'componenti._id',
            model: 'Componente'  
        })
       .populate('modello')
       .populate('categoria')
       .sort({categoria:1,modello:1})
      //.limit(1000)
        .exec(function(err,prodottiAll)
        {   

            
            async.each(prodottiAll, function(prodotto,cb){ 

           record =   prodotto.categoria.nome+','+prodotto.modello.nome + ',' + prodotto.codice+',';

            var led =  _.filter(prodotto.componenti, 
                function(comp){ 
                    if (comp  && comp._id){

                        return comp._id.tipo=='led';
                    }
                    else{
                    console.log("errore " ,record )
                    }
                }
            );

            var driver =  _.filter(prodotto.componenti, 
                function(comp){
                    if (comp && comp._id){
                    return comp._id.tipo=='driver';
                    }
                    else{
                        console.log("errore ", record)
                    }
                }
            );

            if (led.length==0){
                record =record+ "NO,";
            }
            else{
                record =record+"SI, ";
            }

            if (driver.length==0){
                record =record+"NO";
            }
            else{
                record =record+"SI";
            }

            console.log(record)

            contatore++;

logGlobale = logGlobale + record + "\r\n"
           

            cb();

        }, function(err){

            if(err)
                throw err;
 
            
             console.log(logGlobale) ;

            console.log('finito, chiudo la connessione')



            mongoose.connection.close();

            fs.writeFile(".tmp/_300_distinta_base_errata_2_1_componenti_2.csv",

                 logGlobale,

                 function(err) {
                    if(err) {
                        console.log("Log was not saved!", err);
                        throw err;
                    }
                    else
                        console.log("Log was saved!");
                }
            ); 
             
        })
    }) 
}


);
