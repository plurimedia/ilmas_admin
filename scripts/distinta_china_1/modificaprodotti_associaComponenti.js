/*
LANCIARE LA PROCEDURA
node --max-old-space-size=5000 scripts/distinta_china_1/modificaprodotti_associaComponenti;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../server/models/prodotto'),
    ComponenteSchema = require('../../server/models/componente'),
    Prodotto = mongoose.model('Prodotto'),
    Componente = mongoose.model('Componente'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() 
{ 
    console.log("Connessione aperta, parte il batch"); /// this gets printed to console

    Componente.find()
    .lean()
    .exec(
    function(err,componenti)
    {
        if(err){
            console.log(err)
            throw err;
        }
        else
        { 
            console.log('componenti trovati '+componenti.length)

            var query = {
                componenti:{ $ne: null },
                
                $or: [ 
                    { generazione: 'G6' }, { generazione: 'D1' },  { kit: true } , { criptato: true }
                ]
            }

            Prodotto.find(query)
            .lean()
            .exec(
                function(err,prodotti)
                {
                    console.log('aggiorno '+prodotti.length+' prodotti') 
                 
                    if(err){
                        console.log(err)
                        throw err;
                    }
                    else
                    {
                        var bulk = Prodotto.collection.initializeOrderedBulkOp(); 

                        async.each(prodotti, 

                            function(prodotto,cbProd){

                                if (prodotto.codice && prodotto.codice.length >=9){


                                codiceProdotto = prodotto.codice;
                                codiceCategoria = prodotto.categoria;
                                
                                componentiCorpo = _.clone(_.filter(componenti, function(c)
                                {
                                      return c.tipo === 'corpo'  &&
                                            c.codice.substring(0,4) === codiceProdotto.substring(0,4) &&
                                            c.codice.substring(6,8) === codiceProdotto.substring(6,8) 
                                }));

                                

                                if (componentiCorpo.length>0){
                                
                                //ASSOCIAZIONE LED 
                                //1 regola:
                                //il 5 carattere del prodotto === al campo lettera del componente
                                componentiLed = _.clone(_.filter(componenti, function(c)
                                {
                                    return c.tipo === 'led'  &&
                                        c.lettera === codiceProdotto.substring(4,5); 
                                }));


                                //ASSOCIAZIONE DRIVER
                                //1 regola
                                //il 6 ed il 9 carattere del codice prodotto === alle lettere di seguito.
                                //esempio: il driver 9000B02 verrà associato a tutti i prodotti che hanno  
                                //il codice dove al 6 carattere c'è una B ed al 9 carattere c'è una E
                                
                                //Se non esiste il 9 carattere non associo nulla tranne che per i proiettori
                                //dove controllo solo ed esclusivamente il 6 carattere

                                //                                      carattere codice prodotto
                                //                                                    6   /  9    
                                /*
                                x 9000B02 - DRIVER STD 350 mA                   -   B     /  E
                                x 9000B32 - DRIVER DIMM 350/500/700 mA          -   B-C-D /  D
                                x 9000B52 - DRIVER DALI 350/500/700 mA          -   B-C-D /  L
                                x 9000A03 - DRIVER STD 500 mA                   -   C     /  E
                                x 9000B05 - DRIVER STD 700 mA                   -   D     /  E
                                ​x 9000B07 - DRIVER STD 900/1050 mA              -   E-F   /  E​
                                ​x 9000B57 - DRIVER DIMM/DALI 900/1050 mA        -   E-F   /  D-L​
                                ​x 9000B10 - DRIVER STD 1200 mA                  -   G     /  E​
                                ​x 9000B60 - DRIVER DIMM/DALI 1200 mA            -   G     /  D-L
                                ​x 9000A11 - DRIVER STD 1400/1750​ mA             -   H-K   /  E
                                ​X 9000A61 - DRIVER DIMM/DALI 1400/1750 mA​       -   H-K   /  D-L
                                */



                                componentiDriver_9000B02 = _.clone(
                                    _.filter(componenti, function(c)
                                    { 

                                    //per i proiettori che non hanno la 9 cifra applichiamo solo il controllo sul 6 carattere
                                    if (codiceCategoria=='56b8aaa80de23c95600bd21a' && //Proiettori
                                        codiceProdotto.length==8)
                                    {
                                            return  c.codice === '9000B02' &&
                                                    codiceProdotto.substring(5,6) === 'B'
                                    }
                                    else
                                    {
                                            return  c.codice === '9000B02' &&
                                                    codiceProdotto.substring(5,6) === 'B' &&
                                                    codiceProdotto.substring(8,9) === 'E'
                                    }
                                }));

                                componentiDriver_9000B32 = _.clone(
                                    _.filter(componenti, function(c)
                                    { 
                                    
                                     return c.codice === '9000B32' &&
                                            (
                                                codiceProdotto.substring(5,6) === 'B' || 
                                                codiceProdotto.substring(5,6) === 'C' ||
                                                codiceProdotto.substring(5,6) === 'D'
                                            ) &&
                                            codiceProdotto.substring(8,9) === 'D' 
                                         
                                            
                                })); 


                                componentiDriver_9000B52 = _.clone(
                                    _.filter(componenti, function(c)
                                    { 
                                     return c.codice === '9000B52' &&
                                            (
                                                codiceProdotto.substring(5,6) === 'B' || 
                                                codiceProdotto.substring(5,6) === 'C' ||
                                                codiceProdotto.substring(5,6) === 'D'
                                            ) &&
                                            codiceProdotto.substring(8,9) === 'L' 
                                             
                                            
                                })); 

                                componentiDriver_9000A03 = _.clone(
                                    _.filter(componenti, function(c)
                                    {

                                    if  (codiceCategoria=='56b8aaa80de23c95600bd21a' && //Proiettori
                                        codiceProdotto.length==8)
                                    {
                                        return  c.codice === '9000A03' &&
                                                codiceProdotto.substring(5,6) === 'C'
                                    }
                                    else
                                    {
                                     return c.codice === '9000A03' &&
                                            codiceProdotto.substring(5,6) === 'C'  &&
                                            codiceProdotto.substring(8,9) === 'E'  
                                    }

                                }));


                                componentiDriver_9000B05 = _.clone(
                                    _.filter(componenti, function(c)
                                    { 
                                    if (codiceCategoria=='56b8aaa80de23c95600bd21a' && //Proiettori
                                       codiceProdotto.length==8)
                                    {
                                        return  c.codice === '9000B05' &&
                                                codiceProdotto.substring(5,6) === 'D'
                                    }
                                    else
                                    {
                                     return c.codice === '9000B05' &&
                                            codiceProdotto.substring(5,6) === 'D'  &&
                                            codiceProdotto.substring(8,9) === 'E'  
                                    }
                                }));   
 

                                 componentiDriver_9000B07 = _.clone(
                                    _.filter(componenti, function(c)
                                    { 

                                    if (codiceCategoria=='56b8aaa80de23c95600bd21a' && //Proiettori
                                         codiceProdotto.length==8)
                                    {
                                         return  c.codice === '9000B07' &&
                                        (
                                                codiceProdotto.substring(5,6) === 'E' || 
                                                codiceProdotto.substring(5,6) === 'F' 
                                        )
                                    }
                                    else
                                    {
                                      return c.codice === '9000B07' &&
                                            (
                                                codiceProdotto.substring(5,6) === 'E' || 
                                                codiceProdotto.substring(5,6) === 'F'  
                                            )
                                            &&
                                            codiceProdotto.substring(8,9) === 'E' 
                                    }
                                            
                                })); 

                                componentiDriver_9000B57 = _.clone(
                                    _.filter(componenti, function(c)
                                    { 
                                     return c.codice === '9000B57'  &&
                                            (
                                                codiceProdotto.substring(5,6) === 'E' || 
                                                codiceProdotto.substring(5,6) === 'F'  
                                            ) 
                                            &&
                                            (
                                                codiceProdotto.substring(8,9) === 'D' || 
                                                codiceProdotto.substring(8,9) === 'L'  
                                            )
                                            
                                }));

                                componentiDriver_9000B10 = _.clone(
                                    _.filter(componenti, function(c)
                                    { 
                                    if (codiceCategoria=='56b8aaa80de23c95600bd21a' && //Proiettori
                                        codiceProdotto.length==8)
                                    {
                                        return  c.codice === '9000B10' &&
                                        (
                                                codiceProdotto.substring(5,6) === 'G' 
                                         )
                                    }
                                    else
                                    {
                                     return c.codice === '9000B10' &&
                                            codiceProdotto.substring(5,6) === 'G'  &&
                                            codiceProdotto.substring(8,9) === 'E' 
                                    } 
                                })); 


                                componentiDriver_9000B60 = _.clone(
                                    _.filter(componenti, function(c)
                                    { 
                                     return c.codice === '9000B60' 
                                            &&
                                            (
                                                codiceProdotto.substring(5,6) === 'G'   
                                            ) 
                                            &&
                                            (
                                                codiceProdotto.substring(8,9) === 'D' || 
                                                codiceProdotto.substring(8,9) === 'L'  
                                            )
                                            
                                })); 


                                componentiDriver_9000A11 = _.clone(
                                    _.filter(componenti, function(c)
                                    { 
                                    if (
                                    codiceCategoria=='56b8aaa80de23c95600bd21a' && //Proiettori
                                     codiceProdotto.length==8
                                    )
                                    {
                                        return  c.codice === '9000A11' &&
                                        (
                                            codiceProdotto.substring(5,6) === 'H'   ||
                                            codiceProdotto.substring(5,6) === 'K' 
                                        ) 
                                    }
                                    else
                                    {
                                        return c.codice === '9000A11' 
                                            &&
                                            (
                                                codiceProdotto.substring(5,6) === 'H'   ||
                                                codiceProdotto.substring(5,6) === 'K' 
                                            ) 
                                            &&
                                            (
                                                codiceProdotto.substring(8,9) === 'E'  
                                            )
                                    }
                                            
                                })); 


                                 componentiDriver_9000A61 = _.clone(
                                    _.filter(componenti, function(c)
                                    { 
                                     return c.codice === '9000A61' 
                                            &&
                                            (
                                                codiceProdotto.substring(5,6) === 'H'   ||
                                                codiceProdotto.substring(5,6) === 'K' 
                                            ) 
                                            &&
                                            (
                                                codiceProdotto.substring(8,9) === 'D'  ||
                                                codiceProdotto.substring(8,9) === 'L' 
                                            )
                                            
                                }));   

                                

                                 //Raggruppo tutti i componenti in un unico array
                                componentiDaAssociare = _.union(
                                    componentiCorpo,  
                                    componentiLed,  
                                    componentiDriver_9000B02,
                                    componentiDriver_9000B32,
                                    componentiDriver_9000B52, 
                                    componentiDriver_9000A03,
                                    componentiDriver_9000B05,
                                    componentiDriver_9000B07,
                                    componentiDriver_9000B57,
                                    componentiDriver_9000B10,
                                    componentiDriver_9000B60,
                                    componentiDriver_9000A11,
                                    componentiDriver_9000A61
                                ); 
 
                                var componentiUpdate =  {}; 
                                var listComponentiUpdate =  []; 

console.log(aggiornati, prodotto.codice)
                                async.each(_.clone(componentiDaAssociare), 
                                    function(c,cb)
                                    { 

                                        console.log(c.codice, c.tipo);

                                        componentiUpdate =  {
                                            qt: 1,
                                            _id: c._id
                                        };

                                        listComponentiUpdate.push(componentiUpdate)

                                        cb();
                                    }
                                )
                                console.log("@@@@@@")

                                if (listComponentiUpdate.length>0){
                                     var update = {};  
                                    update.$set = {}; 
                                    //update.$set.mdate = new Date();
                                    update.$set.distintabase = "china_1"
                                    update.$set.componenti = listComponentiUpdate; 
                                    bulk.find( { _id: prodotto._id } ).updateOne(update);
                                    aggiornati ++; 
                                }

                                }
                                else{
                                    /*
                                    console.log(prodotto.codice, "SENZA CORPO")
                                    var update = {};  
                                    update.$set = {}; 
                                    update.$set.mdate = new Date();
                                    update.$set.componenti = []; 
                                    bulk.find( { _id: prodotto._id } ).updateOne(update);
                                    aggiornati ++; 
                                    */
                                }


                            }

                                cbProd();
                               
                            }, 
                            function(err)
                            {
                                if(err){
                                    console.log(err)
                                    throw err;
                                }

                                if(aggiornati > 0 && 1==2) 
                                {   
                                    bulk.execute(function(err, result){
                                        if(err){
                                             console.log(err)
                                            throw err;
                                        }

                                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                                        mongoose.connection.close();

                                    })
                                }
                                else {
                                    console.log('Non ci sono prodotti da aggiornare')
                                    mongoose.connection.close();
                                }
                            }
                        )
                    }
                }
            )
        }
    })
});
