/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/_320_setchina1_distinta_base;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    fs      = require('fs'),
    aggiornati = 0,
    logGlobale=""; 
 

console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    var query = 
    {
        componenti : {$exists:true},  $where: "this.componenti.length == 3" 

    }

    Prodotto.find(query).lean().sort({codice:1})
    .exec(
        function(err,prodotti){ 

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, 
            function(prodotto,cb)
            {
                 
                    var p = _.clone(prodotto);

                    var logStr = _.clone(aggiornati) + " codice " + p.codice;
       

                    var update = {};
                    update.$set = {};
                    update.$set.distintabase = "china_1";

                    bulk.find( { _id: p._id } ).updateOne(update);

                     aggiornati ++;   
                    
                    console.log(logStr); 

                    logGlobale = logGlobale + logStr + "\n"; 
                   
                

                cb();
            },
            function(err)
            {
                if(err){
                   console.log(err)
                    throw err;
                } 
 
                if(aggiornati > 0) {
                 
                    bulk.execute(function(err, result){
                        if(err)
                            throw err;

                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                        mongoose.connection.close(); 

                        fs.writeFile(".tmp/_320_setchina1_distinta_base.txt",

                             logGlobale,

                             function(err) {
                                if(err) {
                                    console.log("Log was not saved!", err);
                                    throw err;
                                }
                                else
                                    console.log("Log was saved!");
                            }
                        );

                    })
                   
                }
                else {
                    console.log('Non ci sono prodotti da aggiornare')
                    mongoose.connection.close();
                }
            }
        )
    })

 
}
);
