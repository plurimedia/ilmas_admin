/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/distinta_china_1/_360_set_codice_interno;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ComponenteSchema = require('../../server/models/componente'),
    Componente = mongoose.model('Componente'),
    fs      = require('fs'),
    aggiornati = 0,
    logGlobale=""; 
 

console.log(db)

mongoose.connect(db);

_codificaCategoria = function(componente){
    if (componente.categoria == 'lavorazione')
        return "L"
    else (componente.categoria == 'componente')
        return "C"
}

_codificaCategoria = function(componente){
    if (componente.categoria == 'attivita')
        return "A"
    else (componente.categoria == 'componente')
        return "C"
}

_codificaTipo = function(componente){
    if (componente.tipo == 'corpo')
        return "CO"
    else if (componente.tipo == 'led')
        return "LE"
    else if (componente.tipo == 'driver')
        return "DR"
    else if (componente.tipo == 'anello')
        return "AN"
    else if (componente.tipo == 'blocco_staffa')
        return "BL"
    else if (componente.tipo == 'box')
        return "BO"
    else if (componente.tipo == 'cornice')
        return "CN"
    else if (componente.tipo == 'lampada_led')
        return "LL"
    else if (componente.tipo == 'staffa_laterale')
        return "SL"   
    else if (componente.tipo == 'verniciatura')
        return "VE"
    else if (componente.tipo == 'cartone')
        return "CA"
    else if (componente.tipo == 'imballo')
        return "IM"
    else if (componente.tipo == 'manodopera')
        return "MA"
    else if (componente.tipo == 'sacchetto')
        return "SA"
}


mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

 

    Componente.find().lean()
    .exec(
        function(err,prodotti){ 

        var bulk = Componente.collection.initializeOrderedBulkOp();
        async.each(prodotti, 
            function(prodotto,cb)
            {
                var p = _.clone(prodotto);
    
                var codiceInterno = 
                _.clone(_codificaCategoria(prodotto) + 
                _codificaTipo(prodotto) + 
                ("0000000"+aggiornati).substring(("0000000"+aggiornati).length-6, ("0000000"+aggiornati).length)
                )

                var logStr = _.clone(aggiornati) + " codice " + p.codice + " " + codiceInterno ;


                var update = {};
                update.$set = {};
                update.$set.codice_interno = codiceInterno;

                bulk.find( { _id: p._id } ).updateOne(update);

                    aggiornati ++;   
                
                console.log(logStr); 

                logGlobale = logGlobale + logStr + "\n"; 

                cb();
            },
            function(err)
            {
                if(err){
                   console.log(err)
                    throw err;
                } 
 
                if(aggiornati > 0) {
                 
                    bulk.execute(function(err, result){
                        if(err)
                            throw err;

                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                        mongoose.connection.close(); 

                        fs.writeFile(".tmp/_360_set_codice_interno.txt",

                             logGlobale,

                             function(err) {
                                if(err) {
                                    console.log("Log was not saved!", err);
                                    throw err;
                                }
                                else
                                    console.log("Log was saved!");
                            }
                        );

                    })
                   
                }
                else {
                    console.log('Non ci sono prodotti da aggiornare')
                    mongoose.connection.close();
                }
            }
        )
    })

 
}
);
