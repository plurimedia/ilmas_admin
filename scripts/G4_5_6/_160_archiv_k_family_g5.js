/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/_160_archiv_k_family_g5;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    fs      = require('fs'),
    aggiornati = 0,
    logGlobale="",
    trascodifica = "20170324_03";

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    var query = 
    {
        generazione: 'G5',
          modello: { $in: [
            "573436d4c0f52e030063bb3d", // 2001 K TRACK S
            "5743f1d166923a030050afd0",// 2002 K TRACK L  
            "5847e3c50e147c0400fde026", //2003 KS/P
            "58331537e9983b040067c5ff", //2004 KL/P
            "5847e60d0e147c0400fde02a", //  2005 KS/A
            "57eb8b5d60ce4b0300262fe6", //  2025 K S/A
            "5821d5261164470300324b5c", //  2006 KL/A
            "5874aa1ad7edac040031ba6f",  //2008 KL/220
            "58734cf8043a1d040098d694", //2016 KS PLUS  
            "58247969de6dd4040049c3b4", //  2019 K L PLUS
            "583bfb098957d2040071c175", //2026 K L 1 Incasso
            "57eb8bac60ce4b0300262fe7", //2027 K L 2 Incasso
            "57eb8bbf60ce4b0300262fe8", //2028 K L 3 Incasso
            "583c40df8957d2040071c192", //2011 K1
            "58247983de6dd4040049c3b5",//2012 K2  
            "584542e550a2b304008c47a2", //2013 K3 
            ] }

    }

    Prodotto.find(query).lean()
    .populate('modello','nome')  
    .populate('categoria','nome')  
    .exec(function(err,prodotti){ 

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, 
            function(prodotto,cb)
            {
                var update = {}; 
                
                var p = _.clone(prodotto);  
                     
                p.codice = prodotto.codice+"-G5";
               
                var update = {};
                update.$set = {};
                update.$set.codice = p.codice;
                update.$set.pubblicato=false;
                update.$set.exportMetel=false;
                update.$set.keywords = _keyWords(p);
                update.$set.mdate = new Date();
                update.$set.trascodifica=trascodifica;

                bulk.find( { _id: p._id } ).updateOne(update);

                 aggiornati ++;  

                var logStr = _.clone(aggiornati) + " codice old " + prodotto.codice + " codice nuovo " + p.codice  + " " + "\n";
                
                console.log(logStr); 

                logGlobale = logGlobale + logStr + "\n"; 

                cb();
            },
            function(err)
            {
                if(err)
                    throw err;

                if(aggiornati > 0) {
                 
                    bulk.execute(function(err, result){
                        if(err)
                            throw err;

                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                        mongoose.connection.close(); 

                        fs.writeFile(".tmp/_160_archiv_k_family_g5.txt",

                             logGlobale,

                             function(err) {
                                if(err) {
                                    console.log("Log was not saved!", err);
                                    throw err;
                                }
                                else
                                    console.log("Log was saved!");
                            }
                        );

                    })
                   
                }
                else {
                    console.log('Non ci sono prodotti da aggiornare')
                    mongoose.connection.close();
                }

            }
        )
    }) 


    _keyWords = function (p) {


        var result = [p.codice,p.codice.toLowerCase()], // metti codice e codice lowercase
            _string = function (stringa) { // tutte le parole di una stringa senza il trattino, upper e lower
 
                var keywords = _.filter(stringa.split(' '),function(k){return k!=='-'});
                return [
                        keywords,
                        _.collect(keywords,function(k){return k.toLowerCase()}),
                        _.collect(keywords,function(k){return k.toUpperCase()})
                ]
            },
            _value = function (v,u){
                return [v+u,v+u.toUpperCase(),v+u.substr(0,1).toUpperCase()+u.substr(1)];
            },

            _keywordsCodice = function (codice) { 
            // metto anche il codice in pezzi (ricerca per parti di codice)
                /*
                    1241NCR9 => 1241,1241N, 1241NC, 1241NCR, 1241NCR9
                */
                 if(codice.indexOf('KIT')===-1) {
                  var c1 = codice.substr(0,3),
                    c2 = codice.substr(0,4),
                    c3 = codice.substr(0,5),
                    c4 = codice.substr(0,6);
                    c5 = codice.substr(0,7);
                    c6 = codice.substr(0,8); 

                    return _.compact([c1,c2,c3,c4,c5,c6]);
                }

                // esterno

                if(codice.indexOf('599')!==-1) {
                    return codice.split('.');
                }

                /* KIT1241L0 => KIT,KIT1241,KIT1241L */
                else {
                   var c1 = codice.substr(0,3),
                    c2 = codice.substr(0,7),
                    c3 = codice.substr(0,8),
                    c4 = codice.substr(0,9);  

                    return _.compact([c1,c2,c3,c4]);
                }

            };

        if(p.linea)
            result.push(p.linea,p.linea.toUpperCase());
        if(p.sottocategoria)
            result.push(p.sottocategoria,p.sottocategoria.toUpperCase(),p.sottocategoria.toLowerCase());
        if(p.modello)
            result.push(_string(p.modello.nome));

        if(p.categoria)
            result.push(_string(p.categoria.nome));

        if(p.k)
            result.push(_value(p.k,'k'));
        if(p.lm)
           result.push(_value(p.lm,'lm'));
        if(p.w)
           result.push(_value(p.w,'w'));
        if(p.ma)
           result.push(_value(p.ma,'ma'));
        if(p.fascio)
            result.push(p.fascio+'°');
        if(p.v)
            result.push(_value(p.v,'v'));
        if(p.hz)
            result.push(_value(p.hz,'hz'));
        if(p.irc)
            result.push(p.irc,_value(p.irc,'irc'));
        if(p.alimentatore)
            result.push(_string(p.alimentatore));
        if(p.colore)
            result.push(_string(p.colore));

        
        result.push(_keywordsCodice(p.codice.toUpperCase()))

        result.push(_keywordsCodice(p.codice.toLowerCase()))

        return _.unique(_.flatten(result));
    };
}
);
