/*
LANCIARE LA PROCEDURA
node --max-old-space-size=10000 scripts/_330_nuovi_d1_sam_10_mini_2700;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    fs      = require('fs'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    ModelloSchema = require('../server/models/modello'),
    Modello = mongoose.model('Modello'),
    aggiornati = 0,
    trascodifica="20170608_01",
    logGlobale="";

    var pathProdotti = 'scripts/ProdottoAll.json';
    var prodottiAll = JSON.parse(fs.readFileSync(pathProdotti, 'utf8'));

    console.log(db)

    mongoose.connect(db);

    mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

        var query={ 
            generazione : 'D1',
            modello: { $in: [
               "56b8aaa90de23c95600bd22b", // 1240 Over Led PS Mini  
                "56b8aaa90de23c95600bd22f",// 1290 Over Led PS2 Mini   
                "56b8aaa90de23c95600bd234", //1330 Fly Mini  
                "56b8aaa90de23c95600bd24e", //0670 Astuto Led Mini
                "56b8aaa90de23c95600bd252" //0660 Dino Led Mini
                ] }
        }
   
        Prodotto.find(query).lean()
        .populate('categoria','nome')  
        .populate('modello','nome')
        .exec(function(err,prodotti)
        {  
            var bulk = Prodotto.collection.initializeOrderedBulkOp();

            var prodotti = _.filter(prodotti, function(m){
                 return  (m.codice.substring(4,5)=='7' || m.codice.substring(4,5)=='5')
            }); 

            async.each(prodotti, function(prodotto,cb){

            var codiceNuovo =""; 
 
            if (prodotto.codice &&  prodotto.codice.substring(4,5)==='7')
            {

                var clone = _.clone(prodotto);

                codiceNuovo = clone.codice;

                codiceNuovo = codiceNuovo.substring(0,4) + 
                             "Z" + 
                             codiceNuovo.substring(5);

                clone.codice = codiceNuovo;

 
                var esisteCodice = _.filter(prodottiAll, 
                    function(prd){
                        return prd.codice === codiceNuovo;
                    }
                );

                if (esisteCodice.length==0)
                { 
                     
                    if (codiceNuovo.substring(5,6)=='B'){
                        clone.lm='1350'
                    }
                    else if (codiceNuovo.substring(5,6)=='C'){
                        clone.lm='1790'
                    }
                     
                    clone.k='2700'; 

                    clone.keywords = _keyWords(clone); 

                    if (prodotto.categoria && prodotto.categoria._id )
                        clone.categoria = prodotto.categoria._id;

                    if (prodotto.modello && prodotto.modello._id )
                        clone.modello = prodotto.modello._id;

                    clone.padre = prodotto.codice;

                    clone.trascodifica = trascodifica; // assegno un numero progressivo al set di dati creati. Se qualcosa va male cancello tutto i dati con questa chiave

                    if (clone.w && clone.w.substring(0,2)=='1x')
                        clone.w = clone.w.substring(2) 

                    clone.generazione = 'D1'; 

                    delete clone._id;

                    //delete clone.file_ldt;

                    delete clone.componenti;

                    delete clone.accessori;

                    clone.mdate = new Date();

                    bulk.insert(clone);

                    var logStr = _.clone(aggiornati) + ") codiceNuovo " +  codiceNuovo +
                                                        " codicePadre " +  prodotto.codice + 
                                                        " lm = " + clone.lm + 
                                                        " k = " + clone.k;

                    logGlobale = logGlobale + logStr + "\n";

                    console.log(logStr); 

                    aggiornati ++; 
                    
                }

            }
            else if (prodotto.codice &&  prodotto.codice.substring(4,5)==='5')
            { 

                var clone = _.clone(prodotto);

                codiceNuovo = clone.codice;

                codiceNuovo = codiceNuovo.substring(0,4) + 
                             "R" + 
                             codiceNuovo.substring(5); 

                clone.codice = codiceNuovo;

                 var esisteCodice = _.filter(prodottiAll, 
                    function(prd){
                        return prd.codice === codiceNuovo;
                    }
                );


                if (esisteCodice.length==0)
                {  

                if (codiceNuovo.substring(5,6)=='B'){
                    clone.lm='1580'
                }
                else if (codiceNuovo.substring(5,6)=='C'){
                    clone.lm='2095'
                }
                 

                clone.k='2700'; 

                clone.keywords = _keyWords(clone); 

                if (prodotto.categoria && prodotto.categoria._id )
                    clone.categoria = prodotto.categoria._id;

                if (prodotto.modello && prodotto.modello._id )
                    clone.modello = prodotto.modello._id;

                clone.padre = prodotto.codice;

                clone.trascodifica = trascodifica; // assegno un numero progressivo al set di dati creati. Se qualcosa va male cancello solo i dati con questa chiave

                if (clone.w && clone.w.substring(0,2)=='1x')
                    clone.w = clone.w.substring(2)  

                clone.generazione = 'D1';

                delete clone._id; 

                //delete clone.file_ldt;

                delete clone.componenti;

                delete clone.accessori;

                clone.mdate = new Date();

                bulk.insert(clone);

                var logStr = _.clone(aggiornati) + ") codiceNuovo " +  codiceNuovo +  
                                                   " codicePadre " +  prodotto.codice+ 
                                                        " lm = " + clone.lm + 
                                                        " k = " + clone.k;

                logGlobale = logGlobale + logStr + "\n";

                console.log(logStr); 

                aggiornati ++;
                

                }

            }

            cb();

        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0) {
             
                bulk.execute(
                    function(err, result)
                    {
                        if(err){
                             console.log("errore ", err)// throw err;
                        }
                      
                        console.log('finito, chiudo la connessione: inseriti '+aggiornati+' nuovi prodotti')
                        mongoose.connection.close();

                        fs.writeFile(".tmp/_330_nuovi_d1_sam_10_mini_2700.txt",

                             logGlobale,

                             function(err) {
                                if(err) {
                                    console.log("Log was not saved!", err);
                                    throw err;
                                }
                                else
                                    console.log("Log was saved!");
                            }
                        );
                    }
                )
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }
        })
    })
                

     _keyWords = function (p) {


        var result = [p.codice,p.codice.toLowerCase()], // metti codice e codice lowercase
            _string = function (stringa) { // tutte le parole di una stringa senza il trattino, upper e lower
 
                var keywords = _.filter(stringa.split(' '),function(k){return k!=='-'});
                return [
                        keywords,
                        _.collect(keywords,function(k){return k.toLowerCase()}),
                        _.collect(keywords,function(k){return k.toUpperCase()})
                ]
            },
            _value = function (v,u){
                return [v+u,v+u.toUpperCase(),v+u.substr(0,1).toUpperCase()+u.substr(1)];
            },

            _keywordsCodice = function (codice) { 
            // metto anche il codice in pezzi (ricerca per parti di codice)
                /*
                    1241NCR9 => 1241,1241N, 1241NC, 1241NCR, 1241NCR9
                */
                 if(codice.indexOf('KIT')===-1) {
                  var c1 = codice.substr(0,3),
                    c2 = codice.substr(0,4),
                    c3 = codice.substr(0,5),
                    c4 = codice.substr(0,6);
                    c5 = codice.substr(0,7);
                    c6 = codice.substr(0,8); 

                    return _.compact([c1,c2,c3,c4,c5,c6]);
                }

                // esterno

                if(codice.indexOf('599')!==-1) {
                    return codice.split('.');
                }

                /* KIT1241L0 => KIT,KIT1241,KIT1241L */
                else {
                   var c1 = codice.substr(0,3),
                    c2 = codice.substr(0,7),
                    c3 = codice.substr(0,8),
                    c4 = codice.substr(0,9);  

                    return _.compact([c1,c2,c3,c4]);
                }

            };

        if(p.linea)
            result.push(p.linea,p.linea.toUpperCase());
        if(p.sottocategoria)
            result.push(p.sottocategoria,p.sottocategoria.toUpperCase(),p.sottocategoria.toLowerCase());
        if(p.modello)
            result.push(_string(p.modello.nome));

        if(p.categoria)
            result.push(_string(p.categoria.nome));

        if(p.k)
            result.push(_value(p.k,'k'));
        if(p.lm)
           result.push(_value(p.lm,'lm'));
        if(p.w)
           result.push(_value(p.w,'w'));
        if(p.ma)
           result.push(_value(p.ma,'ma'));
        if(p.fascio)
            result.push(p.fascio+'°');
        if(p.v)
            result.push(_value(p.v,'v'));
        if(p.hz)
            result.push(_value(p.hz,'hz'));
        if(p.irc)
            result.push(p.irc,_value(p.irc,'irc'));
        if(p.alimentatore)
            result.push(_string(p.alimentatore));
        if(p.colore)
            result.push(_string(p.colore));

        
        result.push(_keywordsCodice(p.codice.toUpperCase()))

        result.push(_keywordsCodice(p.codice.toLowerCase()))

        return _.unique(_.flatten(result));
    };
}



);
