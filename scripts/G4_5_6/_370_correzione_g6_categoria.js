/*
LANCIARE LA PROCEDURA
node --max-old-space-size=10000 scripts/G4_5_6/_370_correzione_g6_categoria;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
     ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),
    fs      = require('fs'),
     ObjectId = require('mongodb').ObjectID,
    aggiornati = 0,
    logGlobale=""; 
 

console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    var query = 
    {
         
        categoria: { $in: [
            "56b8aaa80de23c95600bd21c" ,
            "56b8aaa80de23c95600bd21d"
            
            ] }

        

    }

    Prodotto.find(query).lean()
    //.limit(100)
    .exec(
        function(err,prodotti){ 

 

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, 
            function(p,cb)
            { 
//console.log(_.clone(aggiornati) +  " codice " + " " +  p.codice, p.divieto_controsoffitti)


                if (!p.divieto_controsoffitti || p.divieto_controsoffitti==false){

                console.log(aggiornati, p.codice, "NO")

                var _idCategoria = p.modello.categoria;

                var update = {};

                update.$set = {}; 

                update.$set.divieto_controsoffitti = true

                bulk.find( { _id: p._id } ).updateOne(update);

                aggiornati ++;  
                }

                
                

                cb();
            },
            function(err)
            {
                if(err){
                   console.log(err)
                    throw err;
                } 
 
                if(aggiornati > 0 && 1==1) {
                 
                    bulk.execute(function(err, result){
                        if(err)
                            throw err;

                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                        mongoose.connection.close(); 

                        fs.writeFile(".tmp/_370_correzione_g6.txt",

                             logGlobale,

                             function(err) {
                                if(err) {
                                    console.log("Log was not saved!", err);
                                    throw err;
                                }
                                else
                                    console.log("Log was saved!");
                            }
                        );

                    })
                   
                }
                else {
                    console.log('Non ci sono prodotti da aggiornare')
                    mongoose.connection.close();
                }
            }
        )
        
    })
 
 
}
);
