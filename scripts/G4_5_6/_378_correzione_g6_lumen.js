/*
LANCIARE LA PROCEDURA
node --max-old-space-size=10000 scripts/G4_5_6/_378_correzione_g6_lumen.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    fs      = require('fs'),
    aggiornati = 0,
    logGlobale=""; 
 

console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    var query = 
    {
        generazione : 'G6'
    }

    Prodotto.find(query).lean()
    .limit(50000)
    .skip(200000)
    .exec(
        function(err,prodotti){ 

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, 
            function(prodotto,cb)
            {
                var p = _.clone(prodotto);
                var w  = '';
                var lm = '';

                var codice_4 = p.codice.substring(0,4);

                if (codice_4=='0609'){
                    if (p.w.substring(0,2).toLowerCase()!='2x'){
                        w = '2x'+p.w;
                        lm = parseInt(p.lm) * 2;
                    }
                }
                else  if (codice_4=='0610'){
                    if (p.w.substring(0,2).toLowerCase()!='3x'){
                        w = '3x'+p.w;
                        lm = parseInt(p.lm)* 3;
                    } 
                } 
                else  if (codice_4=='0611'){
                    if (p.w.substring(0,2).toLowerCase()!='4x'){
                        w = '4x'+p.w;
                        lm = parseInt(p.lm) * 4;
                    } 
                }
                else  if (codice_4=='1709'){
                    if (p.w.substring(0,2).toLowerCase()!='2x'){
                        w = '2x'+p.w;
                        lm = parseInt(p.lm) * 2;
                    } 
                } 
                else  if (codice_4=='1710'){
                    if (p.w.substring(0,2).toLowerCase()!='3x'){
                        w = '3x'+p.w;
                        lm = parseInt(p.lm) * 3;
                    } 
                }
                else  if (codice_4=='1609'){
                    if (p.w.substring(0,2).toLowerCase()!='2x'){
                    w = '2x'+p.w;
                    lm = parseInt(p.lm) * 2;
                    } 
                }
                 else  if (codice_4=='1610'){
                    if (p.w.substring(0,2).toLowerCase()!='3x'){
                        w = '3x'+p.w;
                        lm = parseInt(p.lm) * 3;
                    } 
                }
                 else  if (codice_4=='1611'){
                    if (p.w.substring(0,2).toLowerCase()!='4x'){
                        w = '4x'+p.w;
                        lm = parseInt(p.lm) * 4;
                    } 
                }
                 else  if (codice_4=='1739'){
                    if (p.w.substring(0,2).toLowerCase()!='2x'){
                        w = '2x'+p.w;
                        lm = parseInt(p.lm) * 2;
                    } 
                }
                else  if (codice_4=='1740'){
                    if (p.w.substring(0,2).toLowerCase()!='3x'){
                        w = '3x'+p.w;
                        lm = parseInt(p.lm) * 3;
                    } 
                }
                else  if (codice_4=='1713'){
                    if (p.w.substring(0,2).toLowerCase()!='2x'){
                        w = '2x'+p.w;
                        lm = parseInt(p.lm) * 2;
                    } 
                }
                else  if (codice_4=='1714'){
                    if (p.w.substring(0,2).toLowerCase()!='3x'){
                        w = '3x'+p.w;
                        lm = parseInt(p.lm) * 3;
                    } 
                } 
                else  if (codice_4=='0909'){
                    if (p.w.substring(0,2).toLowerCase()!='2x'){
                        w = '2x'+p.w;
                        lm = parseInt(p.lm) * 2;
                    } 
                } 
                else  if (codice_4=='0910'){
                    if (p.w.substring(0,2).toLowerCase()!='3x'){
                        w = '3x'+p.w;
                        lm = parseInt(p.lm) * 3;
                    } 
                } 
                else  if (codice_4=='0606'){
                    if (p.w.substring(0,2).toLowerCase()!='2x'){
                    w = '2x'+p.w;
                    lm = parseInt(p.lm) * 2;
                    } 
                } 
                else  if (codice_4=='0607'){
                    if (p.w.substring(0,2).toLowerCase()!='3x'){
                        w = '3x'+p.w;
                        lm = parseInt(p.lm) * 3;
                    } 
                } 
                else  if (codice_4=='1606'){
                    if (p.w.substring(0,2).toLowerCase()!='2x'){
                        w = '2x'+p.w;
                        lm = parseInt(p.lm) * 2;
                    } 
                } 
                else  if (codice_4=='1607'){
                    if (p.w.substring(0,2).toLowerCase()!='3x'){
                        w = '3x'+p.w;
                        lm = parseInt(p.lm) * 3;
                    } 
                }
                else  if (codice_4=='4622'){
                    if (p.w.substring(0,2).toLowerCase()!='2x'){
                        w = '2x'+p.w;
                        lm = parseInt(p.lm) * 2;
                    } 
                }
                else  if (codice_4=='4623'){
                    if (p.w.substring(0,2).toLowerCase()!='3x'){
                        w = '3x'+p.w;
                        lm = parseInt(p.lm) * 3;
                    } 
                }
                 else  if (codice_4=='4712'){
                    if (p.w.substring(0,2).toLowerCase()!='2x'){
                        w = '2x'+p.w;
                        lm = parseInt(p.lm) * 2;
                    } 
                }
                else  if (codice_4=='4713'){
                    if (p.w.substring(0,2).toLowerCase()!='3x'){
                        w = '3x'+p.w;
                        lm = parseInt(p.lm) * 3;
                    } 
                }
                else  if (codice_4=='4714'){
                    if (p.w.substring(0,2).toLowerCase()!='4x'){
                        w = '4x'+p.w;
                        lm = parseInt(p.lm) * 4;
                    } 
                }
                else  if (codice_4=='0776'){
                    if (p.w.substring(0,2).toLowerCase()!='2x'){
                        w = '2x'+p.w;
                        lm = parseInt(p.lm) * 2;
                    } 
                }
                else  if (codice_4=='0777'){
                    if (p.w.substring(0,2).toLowerCase()!='3x'){
                        w = '3x'+p.w;
                        lm = parseInt(p.lm) * 3;
                    } 
                }
                 else  if (codice_4=='1061'){
                    if (p.w.substring(0,2).toLowerCase()!='2x'){
                        w = '2x'+p.w;
                        lm = parseInt(p.lm) * 2;
                    } 
                }
                else  if (codice_4=='1062'){
                    if (p.w.substring(0,2).toLowerCase()!='3x'){
                        w = '3x'+p.w;
                        lm = parseInt(p.lm) * 3;
                    } 
                }
                else  if (codice_4=='1063'){
                    if (p.w.substring(0,2).toLowerCase()!='4x'){
                        w = '4x'+p.w;
                        lm = parseInt(p.lm) * 4;
                    } 
                }
                else  if (codice_4=='4532'){
                    if (p.w.substring(0,2).toLowerCase()!='2x'){
                        w = '2x'+p.w;
                        lm = parseInt(p.lm)*2;
                    } 
                }
                 else  if (codice_4=='4533'){
                    if (p.w.substring(0,2).toLowerCase()!='3x'){
                        w = '3x'+p.w;
                        lm = parseInt(p.lm) *3;
                    } 
                }
                 else  if (codice_4=='1002'){
                    if (p.w.substring(0,2).toLowerCase()!='2x'){
                        w = '2x'+p.w;
                        lm = parseInt(p.lm) *2;
                    } 
                }
                 else  if (codice_4=='1003'){
                    if (p.w.substring(0,2).toLowerCase()!='3x'){
                        w = '3x'+p.w;
                        lm = parseInt(p.lm) *3;
                    } 
                }
                else  if (codice_4=='1004'){
                    if (p.w.substring(0,2).toLowerCase()!='4x'){
                        w = '4x'+p.w;
                        lm = parseInt(p.lm) *4;
                    } 
                }
                else  if (codice_4=='1006'){
                    if (p.w.substring(0,2).toLowerCase()!='6x'){
                        w = '6x'+p.w;
                        lm = parseInt(p.lm) *6;
                    } 
                }  

                if (w!=""){
                    var logStr = _.clone(aggiornati) +  " codice " + " " +  p.codice + " " +  w + " "+ lm;

                    var update = {};
                    update.$set = {};
                    update.$set.w = w;
                    update.$set.lm = lm;
                    bulk.find( { _id: p._id } ).updateOne(update);

                    aggiornati ++;   
                    
                    console.log(logStr); 

                    logGlobale = logGlobale + logStr + "\n"; 
                 }
                

                cb();
            },
            function(err)
            {
                if(err){
                   console.log(err)
                    throw err;
                } 
 
                if(aggiornati > 0 ) {
                 
                    bulk.execute(function(err, result){
                        if(err)
                            throw err;

                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                        mongoose.connection.close(); 

                        fs.writeFile(".tmp/_378_correzione_g6_lumen.txt",

                             logGlobale,

                             function(err) {
                                if(err) {
                                    console.log("Log was not saved!", err);
                                    throw err;
                                }
                                else
                                    console.log("Log was saved!");
                            }
                        );

                    })
                   
                }
                else {
                    console.log('Non ci sono prodotti da aggiornare')
                    mongoose.connection.close();
                }
            }
        )
    })
 
 
}
);
