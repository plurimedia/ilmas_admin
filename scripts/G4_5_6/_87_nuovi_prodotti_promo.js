/*
LANCIARE LA PROCEDURA
node --max-old-space-size=10000 scripts/_87_nuovi_prodotti_promo;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
     Utils = require('../server/routes/utils'),
     csv = require("fast-csv"),
    fs = require('fs'),
    aggiornati = 0;

    var pathProdotti = 'scripts/ProdottoAll.json';
    var prodottiAll = JSON.parse(fs.readFileSync(pathProdotti, 'utf8'));

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    csvpath = "scripts/csv/prodottiPromo.csv",
    prodotti = [],
    stream = fs.createReadStream(csvpath); 

     csv.fromStream(stream, {
            headers: true,
            delimiter: ',',
            discardUnmappedColumns: true
        }) 
        .on("data", function (data) {
 
            var record = {
                codicePromo : data.CodicePromo,
                codiceProdotto : data.CodiceProdotto
            }
            prodotti.push(record)

        })
        .on("end", 
            function () {

                var bulk = Prodotto.collection.initializeOrderedBulkOp();
                async.each(prodotti, 
                    function (p, cb) { 
                        var codicePromo = p.codicePromo;
                        var codiceProdotto = p.codiceProdotto;


                        var esisteCodice = _.filter(prodottiAll, 
                            function(prd){
                                return prd.codice === codicePromo;
                            }
                        );

                        if (esisteCodice.length==0) {
                      
                        Prodotto.findOne(
                            {codice:codiceProdotto}, 
                            function (err, prodotto) {
                                if (err){
                                    throw err;
                                }
                                else{

                                    if (prodotto){ 

                                        prodotto.codice = codicePromo;

                                        prodotto.keywords = Utils.keyWords(prodotto); 

                                        prodotto.modello =  prodotto.modello._id

                                        prodotto.categoria =  prodotto.categoria._id 

                                        prodotto.mdate = new Date();

                                        prodotto.padre=codiceProdotto;

                                        prodotto.trascodifica="20170613_20";

                                        var clone = _.clone(prodotto); 

                                        delete clone._id

                                        bulk.insert(clone);

                                        aggiornati ++;

                                        console.log("codicePromo inserito ", clone.codice)

                                        
                                    }
                                    else
                                    {
                                        console.log("prodotto non trovato, codice: ", codiceProdotto)
                                         
                                    }

                                    cb();
                                }
                            }
                        ).lean()
                        .populate('modello','nome')
                        .populate('categoria','nome') 
                        }
                        else{
                            console.log("codicePromo già esistente: ", codicePromo)
                            cb();
                        }
                    }, 
                    function (err) 
                    {
                        if(aggiornati > 0) {
                             bulk.execute(
                                function(err, result){
                                if(err)
                                    throw err;

                                console.log('finito, chiudo la connessione, creati ' + aggiornati + ' nuovi codici')
                                mongoose.connection.close();

                                }
                            )
                        }
                        else {
                            mongoose.connection.close();
                        }
                    }
                )
            }
        ) 
}



);
