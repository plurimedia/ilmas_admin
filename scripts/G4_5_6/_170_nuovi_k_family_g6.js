/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/_170_nuovi_k_family_g6;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
     csv = require("fast-csv");
    fs = require('fs'),
    logGlobale="",
    trascodifica = "20170324_01";
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    csvpath = "scripts/kFamily.csv",
    prodotti = [],
    stream = fs.createReadStream(csvpath); 

     csv.fromStream(stream, {
            headers: true,
            delimiter: ',',
            discardUnmappedColumns: true
        }) 
        //RECUPERO I DATI DAL FILE
        //E SETTO I DATI
        .on("data", function (data) {
 
            var record = {
                codice      : data.codice,
                modello     : data.modello,
                proprieta1  : data.proprieta1, 
                proprieta2  : data.proprieta2
                //etc
                // etc 
            }
           
            var prodotto1 = _.clone(record); //bianco
            prodotto1.codice = prodotto1.codice+"1"
            prodotto1.colore = "Bianco"
            prodotti.push(prodotto1)

            var prodotto2 = _.clone(record); //nero
            prodotto2.codice = prodotto2.codice+"2"
            prodotto1.colore = "Nero"
            prodotti.push(prodotto2)

            var prodotto3 = _.clone(record); //grigio
            prodotto3.codice = prodotto3.codice+"0"
            prodotto1.colore = "Grigio";
            prodotti.push(prodotto3)

        })
        .on("end", 
            function () {

                var bulk = Prodotto.collection.initializeOrderedBulkOp();
                async.each(prodotti, 
                    function (prodotto, cb) { 

                        var codiceProdotto = prodotto.codice;

                        console.log(codiceProdotto)
                      
                        Prodotto.findOne(
                            {codice:codiceProdotto}, 
                            function (err, esisteProdotto) {
                                if (err){
                                    throw err;
                                }
                                else{
                                    //inserisco il record in db solo se non esiste
                                    //
                                    if (!esisteProdotto){   

                                        prodotto.categoria = getNomeCategoria(prodotto.modello);
                                     
                                        prodotto.keywords = _keyWords(prodotto); 

                                        //dopo avere creato le keywords setto l'id del modello e l'id della categoria
                                        prodotto.modello =  getIdModello(prodotto.modello);

                                        prodotto.categoria =  getIdCategoria(prodotto.categoria);

                                        prodotto.mdate = new Date();

                                        prodotto.pubblicato = true;

                                        prodotto.durata = "50.000";

                                        prodotto.macadam = "3";

                                        prodotto.verniciatura = "A polvere epossidica stabilizzata UV";

                                        prodotto.tmax = "30";

                                        prodotto.alimentatore = "Driver standard";

                                        prodotto.valore_di_perdita = "0,97";

                                        prodotto.orientabile = "355";

                                        prodotto.inclinabile = "+/- 90";

                                        prodotto.rischio_fotobiologico = "RG1";

                                        prodotto.filo_incandescente = "850";

                                        prodotto.ip = "20";

                                        prodotto.alimentatore_incluso = true;

                                        prodotto.ce = true;

                                        prodotto.f = true;

                                        prodotto.divieto_controsoffitti = true;

                                        prodotto.trascodifica = trascodifica;

                                        var logStr = _.clone(aggiornati)  + "  codice" +  prodotto.codice

                                        console.log(logStr); 

                                        logGlobale = logGlobale + logStr + "\n";

                                        bulk.insert(prodotto);

                                        //aggiornati ++;

                                        cb();
                                    }
                                    else
                                    {
                                        console.log("prodotto già esistente", codiceProdotto)
                                    }
                                }
                            }
                        ).lean() 
                    }, 
                    function (err) 
                    {
                        if(aggiornati > 0) {
                             bulk.execute(
                                function(err, result){
                                if(err)
                                    throw err;

                                    console.log('finito, chiudo la connessione, creati ' + aggiornati + ' nuovi codici')
                                    mongoose.connection.close();

                                    fs.writeFile(".tmp/_170_nuovi_k_family_g6.txt",

                                     logGlobale,

                                     function(err) {
                                        if(err) {
                                            console.log("Log was not saved!", err);
                                            throw err;
                                        }
                                        else
                                            console.log("Log was saved!");
                                    }
                                );

                                }
                            )
                        }
                        else {
                            mongoose.connection.close();
                        }
                    }
                )
            }
        )

        getNomeCategoria = function(nomeModello){

            if (
                nomeModello=="2001 K TRACK S" ||
                nomeModello=="2002 K TRACK L" ||
                nomeModello=="2003 KS/P" ||
                nomeModello=="2004 KL/P" ||
                nomeModello=="2005 KS/A" ||
                nomeModello=="2006 KL/A" ||
                nomeModello=="2008 KL/220" ||
                nomeModello=="2016 KS PLUS" ||
                nomeModello=="2019 K L PLUS"
                ){
                return "Proiettori";
            }
            else if 
                (
                    nomeModello=="2026 K L 1 Incasso" ||
                    nomeModello=="2027 K L 2 Incasso" ||
                    nomeModello=="2028 K L 3 Incasso" ||
                    nomeModello=="2011 K1" ||
                    nomeModello=="2012 K2" ||
                    nomeModello=="2013 K3"
                ){
                    return "Incassi orientabili";
                }

        }

        getIdModello = function(nomeModello){
            if (nomeModello=="2001 K TRACK S"){
                return "5743f1d166923a030050afd0";
            }
            else if (nomeModello=="2003 KS/P"){
                return "5847e3c50e147c0400fde026";
            }
            else if (nomeModello=="2004 KL/P"){
                return "58331537e9983b040067c5ff";
            }
            else if (nomeModello=="2005 KS/A"){
                return "5847e60d0e147c0400fde02a";
            }
            else if (nomeModello=="2025 K S/A"){
                return "57eb8b5d60ce4b0300262fe6";
            }
            else if (nomeModello=="2006 KL/A"){
                return "5821d5261164470300324b5c";
            }
            else if (nomeModello=="2008 KL/220"){
                return "5874aa1ad7edac040031ba6f";
            }
            else if (nomeModello=="2016 KS PLUS"){
                return "58734cf8043a1d040098d694";
            }
            else if (nomeModello=="2019 K L PLUS"){
                return "58247969de6dd4040049c3b4";
            }
            else if (nomeModello=="2026 K L 1 Incasso"){
                return "583bfb098957d2040071c175";
            }
            else if (nomeModello=="2027 K L 2 Incasso"){
                return "57eb8bac60ce4b0300262fe7";
            }
            else if (nomeModello=="2028 K L 3 Incasso"){
                return "57eb8bbf60ce4b0300262fe8";
            }
            else if (nomeModello=="2011 K1"){
                return "583c40df8957d2040071c192";
            }
            else if (nomeModello=="2012 K2"){
                return "58247983de6dd4040049c3b5";
            }
            else if (nomeModello=="2013 K3"){
                return "584542e550a2b304008c47a2";
            }

        }

        getIdCategoria = function(nomeCategoria){
            if (nomeCategoria == "Proiettori"){
                return "56b8aaa80de23c95600bd21a";
            }
            else if (nomeCategoria == "Incassi orientabili"){
                return "56b8aaa80de23c95600bd21d";
            }
        }


   _keyWords = function (p) {

//console.log(p);

        var result = [p.codice,p.codice.toLowerCase()], // metti codice e codice lowercase
            _string = function (stringa) { // tutte le parole di una stringa senza il trattino, upper e lower
 
                var keywords = _.filter(stringa.split(' '),function(k){return k!=='-'});
                return [
                        keywords,
                        _.collect(keywords,function(k){return k.toLowerCase()}),
                        _.collect(keywords,function(k){return k.toUpperCase()})
                ]
            },
            _value = function (v,u){
                return [v+u,v+u.toUpperCase(),v+u.substr(0,1).toUpperCase()+u.substr(1)];
            },
            _keywordsCodice = function (codice) { // metto anche il codice in pezzi (ricerca per parti di codice)
                /*
                    1241NCR9 => 1241,1241N, 1241NC, 1241NCR, 1241NCR9
                */

                if(codice.indexOf('KIT')===-1) {
                  var c1 = codice.substr(0,3),
                    c2 = codice.substr(0,4),
                    c3 = codice.substr(0,5),
                    c4 = codice.substr(0,6);
                    c5 = codice.substr(0,7);
                    c6 = codice.substr(0,8);

                    return _.compact([c1,c2,c3,c4,c5,c6]);
                }

                // esterno

                if(codice.indexOf('599')!==-1) {
                    return p.codice.split('.');
                }

                /* KIT1241L0 => KIT,KIT1241,KIT1241L */
                else {
                   var c1 = codice.substr(0,3),
                    c2 = codice.substr(0,7),
                    c3 = codice.substr(0,8),
                    c4 = codice.substr(0,9);

                    return _.compact([c1,c2,c3,c4]);
                }

            };

        if(p.linea)
            result.push(p.linea,p.linea.toUpperCase());
        if(p.sottocategoria)
            result.push(p.sottocategoria,p.sottocategoria.toUpperCase(),p.sottocategoria.toLowerCase());
        
        if(p.modello)
            result.push(_string(p.modello));

        if(p.categoria)
            result.push(_string(p.categoria));

        if(p.k)
            result.push(_value(p.k,'k'));
        if(p.lm)
           result.push(_value(p.lm,'lm'));
        if(p.w)
           result.push(_value(p.w,'w'));
        if(p.ma)
           result.push(_value(p.ma,'ma'));
        if(p.fascio)
            result.push(p.fascio+'°');
        if(p.v)
            result.push(_value(p.v,'v'));
        if(p.hz)
            result.push(_value(p.hz,'hz'));
        if(p.irc)
            result.push(p.irc,_value(p.irc,'irc'));
        if(p.alimentatore)
            result.push(_string(p.alimentatore));
        if(p.colore)
            result.push(_string(p.colore));

         result.push(_keywordsCodice(p.codice.toUpperCase()))

        result.push(_keywordsCodice(p.codice.toLowerCase()))

        return _.unique(_.flatten(result));
    };
    
}



);
