/*
LANCIARE LA PROCEDURA
node --max-old-space-size=10000 scripts/G4_5_6/_370_correzione_g6;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    fs      = require('fs'),
    aggiornati = 0,
    logGlobale=""; 
 

console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    var query = 
    {
        generazione : 'G6' ,
        les : 'LES 19'

    }

    Prodotto.find(query).lean()
     .skip(80000)
    //.limit(80000)
    .exec(
        function(err,prodotti){ 


           //console.log(prodotti.length)

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, 
            function(prodotto,cb)
            {
                 
                    var p = _.clone(prodotto);
        
                    var w="";

                    if (p.w=='19'){
                        w = '17'
                    }
                    else if (p.w=='23'){
                        w = '21'
                    }
                    else if (p.w=='27'){
                        w = '25'
                    }
                    else if (p.w=='31'){
                        w = '29'
                    }
                    else if (p.w=='35'){
                        w = '33'
                    }

                    if (w!=""){
                        var logStr = _.clone(aggiornati) +  " codice " + " " +  p.codice + " " +  w;

                        var update = {};
                        update.$set = {};
                        update.$set.w = w;
                        bulk.find( { _id: p._id } ).updateOne(update);

                        aggiornati ++;   
                        
                        console.log(logStr); 

                        logGlobale = logGlobale + logStr + "\n"; 
                     }
                

                cb();
            },
            function(err)
            {
                if(err){
                   console.log(err)
                    throw err;
                } 
 
                if(aggiornati > 0) {
                 
                    bulk.execute(function(err, result){
                        if(err)
                            throw err;

                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                        mongoose.connection.close(); 

                        fs.writeFile(".tmp/_370_correzione_g6.txt",

                             logGlobale,

                             function(err) {
                                if(err) {
                                    console.log("Log was not saved!", err);
                                    throw err;
                                }
                                else
                                    console.log("Log was saved!");
                            }
                        );

                    })
                   
                }
                else {
                    console.log('Non ci sono prodotti da aggiornare')
                    mongoose.connection.close();
                }
            }
        )
    })
 
 
}
);
