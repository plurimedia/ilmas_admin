/*
LANCIARE LA PROCEDURA
node --max-old-space-size=10000 scripts/G4_5_6/_80_check_kit.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    Utils = require('../../server/routes/utils'),
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),
     csv = require("fast-csv"),
    fs = require('fs'),
    aggiornati = 0;

    var pathProdotti = 'scripts/ProdottoAll.json';
    var prodottiAll = JSON.parse(fs.readFileSync(pathProdotti, 'utf8'));

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    csvpath = "scripts/csv/prodottiKIT.csv",
    prodotti = [],
    stream = fs.createReadStream(csvpath); 

     csv.fromStream(stream, {
            headers: true,
            delimiter: ',',
            discardUnmappedColumns: true
        }) 
        .on("data", function (data) {
 
            var record = {
                codiceKit : data.codiceKIT
            }
            prodotti.push(record)

        })
        .on("end", 
            function () {

                var bulk = Prodotto.collection.initializeOrderedBulkOp();
                async.each(prodotti, 
                    function (p, cb) { 
                        var codiceKit = p.codiceKit;
                      
                        Prodotto.findOne(
                            {codice:codiceKit}, 
                            function (err, kit) {
                                if (err){
                                    throw err;
                                }
                                else{

                                    if (kit){

                                    var genereazione=''; 
 
                                    var update = {};
                                    update.$set = {};
                                    update.$set.kit=true;
                                    bulk.find( { _id: kit._id } ).updateOne(update); 
 
                                     console.log(aggiornati, " codiceKit modificato ", kit.codice)

                                    aggiornati++;

                                      

                                                                       
                                    }
                                    else
                                    {
                                        console.log("prodotto non trovato, codice: ", codiceKit)
                                         
                                    }

                                    cb();
                                }
                            }
                        ).lean()
                       // .populate('modello')
                       
                         
                    }, 
                    function (err) 
                    {
                        if(aggiornati > 0 ) {
                             bulk.execute(
                                function(err, result){
                                if(err)
                                    throw err;

                                console.log('finito, chiudo la connessione, creati ' + aggiornati + ' nuovi codici')
                                mongoose.connection.close();

                                }
                            )
                        }
                        else {
                            mongoose.connection.close();
                        }
                    }
                )
            }
        ) 
    
}



);
