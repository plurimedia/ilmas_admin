/*
LANCIARE LA PROCEDURA
node --max-old-space-size=10000 scripts/G4_5_6/_85_nuovi_prodotti_criptati;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
     Utils = require('../../server/routes/utils'),
     csv = require("fast-csv"),
    fs = require('fs'),
    aggiornati = 0;

    var pathProdotti = 'scripts/ProdottoAll.json';
    var prodottiAll = JSON.parse(fs.readFileSync(pathProdotti, 'utf8'));

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    csvpath = "scripts/csv/prodottiCriptati.csv",
    prodotti = [],
    stream = fs.createReadStream(csvpath); 

     csv.fromStream(stream, {
            headers: true,
            delimiter: ',',
            discardUnmappedColumns: true
        }) 
        .on("data", function (data) {
 
            var record = {
                codiceCrip : data.CodiceCrip,
                codiceProdotto : data.CodiceProdotto,
                xx : ''
            }
            prodotti.push(record)

        })
        .on("end", 
            function () {

                var bulk = Prodotto.collection.initializeOrderedBulkOp();

                async.each(prodotti, 
                    function (p, cb) { 
                        var codiceCrip = p.codiceCrip;
                        var codiceProdotto = p.codiceProdotto;


                        var esisteCodice = _.filter(prodottiAll, 
                            function(prd){
                                return prd.codice === codiceCrip;
                            }
                        );

                         if (esisteCodice.length==0) {



                        Prodotto.findOne(
                            {codice:codiceProdotto}, 
                            function (err, prodotto) {
                                if (err){
                                    throw err;
                                }
                                else{

                                    if (prodotto){

                                        prodotto.codice = codiceCrip;

                                        prodotto.keywords = Utils.keyWords(prodotto);  

                                        prodotto.modello =  prodotto.modello._id

                                        prodotto.padre=codiceProdotto;

                                        prodotto.pubblicato=false;

                                        prodotto.exportMetel=false;

                                        prodotto.trascodifica="20170626_01";

                                        prodotto.distintabase = "china_1"

                                        var clone = _.clone(prodotto); 

                                        delete clone._id

                                         bulk.insert(clone);

                                        aggiornati ++;

                                        console.log("codiceCrip inserito ", clone.codice)
                                        
                                    }
                                    else
                                    {
                                       console.log("prodotto non trovato, codice: ", codiceProdotto, prodotto)
                                         
                                    }

                                    cb();
                                }
                            }
                        ).lean()
                        .populate('modello')
                        }
                        else{
                             cb();

                          
                          
                            
                        }
                    }, 
                    function (err) 
                    {
                        if(aggiornati > 0) {
                             bulk.execute(
                                function(err, result){
                                if(err)
                                    throw err;

                                console.log('finito, chiudo la connessione, creati ' + aggiornati + ' nuovi codici')
                                mongoose.connection.close();

                                }
                            )
                        }
                        else {
                            mongoose.connection.close();
                        }
                    }
                )
            }
        ) 
}



);
