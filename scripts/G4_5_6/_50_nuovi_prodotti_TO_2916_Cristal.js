/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/_50_nuovi_prodotti_TO_2916_Cristal
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    fs      = require('fs'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    ModelloSchema = require('../server/models/modello'),
    Modello = mongoose.model('Modello'),
    aggiornati = 0,
    logGlobale="";

    console.log(db)

    mongoose.connect(db);

    mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    var nomeModelloFrom = '1888 Puzzle Led',
        nomeModelloTo   = '2916 Cristal'; //NON ESISTE CREARE
  

    Modello.findOne({nome:nomeModelloFrom})
        .exec(
         function(err,modelloFROM){  
            
        Modello.findOne({nome:nomeModelloTo})
            .exec(
             function(err,modelloTo){  
 
                // cerco tutti proddotti che hanno il modelloTo
                Prodotto.find({modello:  modelloTo._id }) 
                    .exec(
                        function(err,listaProdottiEsistenti){
 
                        Prodotto.find({modello:   modelloFROM._id }).lean()
                        .populate('categoria','nome')  
                        .populate('modello','nome') 
                        .exec(function(err,prodotti){   
 
                            var bulk = Prodotto.collection.initializeOrderedBulkOp();

                            async.each(prodotti, 

                                function(prodotto,cb){
             
                            var codiceNuovo = "";

                            if (prodotto.codice && 
                                prodotto.codice.substring(7,8)==='1')
                            {    
                                codiceNuovo = '2916' + prodotto.codice.substring(4);

                                //prima di inserire il nuovo codice
                                //controllo se esiste già
                                var codiceTemp = [];
                                codiceTemp = _.filter(
                                    listaProdottiEsistenti, function(c){
                                      return  c.codice===codiceNuovo
                                });
  
     
                                if (codiceTemp.length==0){

                                    prodotto.padre = _.clone(prodotto.codice);

                                    prodotto.codice = codiceNuovo;

                                    prodotto.modello = modelloTo.name 

                                    prodotto.keywords = _keyWords(prodotto); 

                                    prodotto.modello =  modelloTo._id;

                                    prodotto.categoria =  modelloTo.categoria

                                    prodotto.generazione = 'G4'; 

                                    prodotto.foro="153";

                                    prodotto.trascodifica = "20170320_04" // assegno un numero progressivo al set di dati creati. Se qualcosa va male cancello tutto i dati con questa chiave

                                    var logStr = _.clone(aggiornati) + ") codiceNuovo " +  codiceNuovo +
                                                        " codicePadre " +  prodotto.padre;

                                    if (prodotto.w && prodotto.w.substring(0,2)=='1x')
                                        prodotto.w = prodotto.w.substring(2)   

                                    

                                    if (prodotto.moduloled && prodotto.moduloled.indexOf("F111")==0){
                                        //modulo led F111 

                                        prodotto.foto = "wvegqa6z2mmlapd7ftuz";

                                        prodotto.disegno = "bzomxdb2gpijwgz1j6am";

                                        prodotto.fotometria = "fdhpmeziyxnvbqksqyp3";

                                        logStr = logStr + " modulo F111"


                                    }
                                    else if (prodotto.moduloled && prodotto.moduloled.indexOf("F112")==0){ 

                                        prodotto.foto = "dwu75rrqk7ehkeiy63ij";

                                        prodotto.disegno = "qlflcufj2p73cy0ohlq7";

                                        prodotto.fotometria = "uqfuq2p6cfrsm3jfesvc";

                                        logStr = logStr + " modulo F112"

                                    }
                                    else{

                                        prodotto.foto = "wvegqa6z2mmlapd7ftuz";

                                        logStr = logStr + " modulo led NON TROVATO"
                                    }



                                    var clone = _.clone(prodotto); 
                                  
                                    delete clone._id;

                                    delete clone.componenti;

                                    delete clone.accessori;

                                    clone.mdate = new Date();

                                    bulk.insert(clone);

                                    

                                    logGlobale = logGlobale + logStr + "\n";

                                    console.log(logStr); 

                                    aggiornati ++;

                                }
                                else{
                                  // console.log("codice esistente", codiceTemp.length)
                                }
                            }

                            cb();

                        }, function(err){

                            if(err)
                                throw err;

                            if(aggiornati > 0) {
                                
                                bulk.execute(
                                    function(err, result)
                                    {
                                        if(err){
                                              console.log(err);
                                        }

                                        console.log('finito, chiudo la connessione: inseriti '+aggiornati+' nuovi prodotti')
                                       
                                        mongoose.connection.close();

                                        fs.writeFile(".tmp/_50_nuovi_prodotti_TO_2916_Cristal.txt",

                                         logGlobale,

                                         function(err) {
                                            if(err) {
                                                console.log("Log was not saved!", err);
                                                throw err;
                                            }
                                            else
                                                console.log("Log was saved!");
                                        }
                                    );
                                    }
                                )
                            }
                            else {
                                console.log('Non ci sono prodotti da aggiornare')
                                mongoose.connection.close();
                            }
                        })
                    })
                })
            })
        })

     _keyWords = function (p) {


        var result = [p.codice,p.codice.toLowerCase()], // metti codice e codice lowercase
            _string = function (stringa) { // tutte le parole di una stringa senza il trattino, upper e lower
 
                var keywords = _.filter(stringa.split(' '),function(k){return k!=='-'});
                return [
                        keywords,
                        _.collect(keywords,function(k){return k.toLowerCase()}),
                        _.collect(keywords,function(k){return k.toUpperCase()})
                ]
            },
            _value = function (v,u){
                return [v+u,v+u.toUpperCase(),v+u.substr(0,1).toUpperCase()+u.substr(1)];
            },
            _keywordsCodice = function (codice) { // metto anche il codice in pezzi (ricerca per parti di codice)
                /*
                    1241NCR9 => 1241,1241N, 1241NC, 1241NCR, 1241NCR9
                */

                if(codice.indexOf('KIT')===-1) {
                  var c1 = codice.substr(0,3),
                    c2 = codice.substr(0,4),
                    c3 = codice.substr(0,5),
                    c4 = codice.substr(0,6);
                    c5 = codice.substr(0,7);
                    c6 = codice.substr(0,8);

                    return _.compact([c1,c2,c3,c4,c5,c6]);
                }

                // esterno

                if(codice.indexOf('599')!==-1) {
                    return codice.split('.');
                }

                /* KIT1241L0 => KIT,KIT1241,KIT1241L */
                else {
                   var c1 = codice.substr(0,3),
                    c2 = codice.substr(0,7),
                    c3 = codice.substr(0,8),
                    c4 = codice.substr(0,9);

                    return _.compact([c1,c2,c3,c4]);
                }

            };

        if(p.linea)
            result.push(p.linea,p.linea.toUpperCase());
        if(p.sottocategoria)
            result.push(p.sottocategoria,p.sottocategoria.toUpperCase(),p.sottocategoria.toLowerCase());
        if(p.modello)
            result.push(_string(p.modello.nome));

        if(p.categoria)
            result.push(_string(p.categoria.nome));

        if(p.k)
            result.push(_value(p.k,'k'));
        if(p.lm)
           result.push(_value(p.lm,'lm'));
        if(p.w)
           result.push(_value(p.w,'w'));
        if(p.ma)
           result.push(_value(p.ma,'ma'));
        if(p.fascio)
            result.push(p.fascio+'°');
        if(p.v)
            result.push(_value(p.v,'v'));
        if(p.hz)
            result.push(_value(p.hz,'hz'));
        if(p.irc)
            result.push(p.irc,_value(p.irc,'irc'));
        if(p.alimentatore)
            result.push(_string(p.alimentatore));
        if(p.colore)
            result.push(_string(p.colore));

        result.push(_keywordsCodice(p.codice.toUpperCase()))

        result.push(_keywordsCodice(p.codice.toLowerCase()))

        return _.unique(_.flatten(result));
    };
}



);
