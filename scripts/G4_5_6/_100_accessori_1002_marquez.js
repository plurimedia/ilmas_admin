/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/_100_accessori_1002_marquez
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    ModelloSchema = require('../server/models/modello'),
    Modello = mongoose.model('Modello'),
    aggiornati = 0;

    console.log(db)

    mongoose.connect(db);

    mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console 


    //accessori da associare
    var nomeModelloAccessorio = 'Marquez Cassetta Alimentazione';
    var nomeStartAccessorio = '1002';

    //modello a cui associare
    var nomeModelloPadre = '1002 Marquez Led';

    Modello.findOne({nome:nomeModelloAccessorio})
        .exec(
         function(err,modelloAccessorio){   

        if (modelloAccessorio){
        //cerco tutti i prodotti con questo nodello
        Prodotto.find({modello:modelloAccessorio._id})
            .exec(
             function(err,accessoriDaAssociare){ 

                console.log("1. accessoriDaAssociare ", accessoriDaAssociare.length)

                accessoriDaAssociare =  _.clone(_.filter(accessoriDaAssociare, 
                    function(acc){
                        return  acc.codice.substring(0,4)===nomeStartAccessorio  
                }));

                console.log("2. accessoriDaAssociare ", accessoriDaAssociare.length)

               

                 accessoriDaAssociare = _.clone(_.map(accessoriDaAssociare, function(obj) {
                    return obj._id;
                }));

                 console.log("3. accessoriDaAssociare ", accessoriDaAssociare.length)


                if (accessoriDaAssociare.length){
                 

                //cerco ID del modello padre
                Modello.findOne({nome:nomeModelloPadre})
                .exec(
                 function(err,modelloPadre){ 

                    console.log("modelloPadre = ", modelloPadre.nome)

                    //cerco tutti i prodotti del modello padre, a cui associare gli accessori
                    Prodotto.find(
                        {modello:modelloPadre._id }
                        )
                    .populate('modello')
                    .exec(
                    function(err,prodottiACuiAssociare){
                         

                    var bulk = Prodotto.collection.initializeOrderedBulkOp();
                  
 
console.log("prodottiACuiAssociare ", prodottiACuiAssociare.length)

                        async.each(prodottiACuiAssociare, 
                            function(prodotto,cb)
                            {

                                var p = _.clone(prodotto);

                                 console.log("***************");


                                console.log("aggiornamento prodotto id = ", p.codice);

                                var update = {};
                              
                                update.$set = {};

                                console.log("4. prima ", p.accessori.length)

                                accessoriDaAssociare = _.clone(_.union(accessoriDaAssociare, p.accessori))
                            
                                 console.log("5. ", accessoriDaAssociare.length)

                                update.$set.accessori =  accessoriDaAssociare  

                                bulk.find( { _id: p._id } ).updateOne(update); 

                                aggiornati ++; 

                                cb();

                            },
                            function(err)
                            {

                                if(err)
                                    throw err;

                                if(aggiornati > 0) {
                                 
                                    bulk.execute(function(err, result){
                                        if(err)
                                            throw err;

                                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                                       
                                        mongoose.connection.close();
                                    })
                                   
                                }
                                else {
                                    console.log('Non ci sono prodotti da aggiornare')
                                    mongoose.connection.close();
                                }

                        })



                    })

                 }
                ) 

            }
            else{
                console.log('non eistono prodotti ddel modello ' + nomeModelloAccessorio + ' da poter associare')
                mongoose.connection.close();
            }

            })
        }
        else{
            console.log('modello ' + nomeModelloAccessorio + ' inesistente')
                                mongoose.connection.close();
        }

    }
    )
    
});
