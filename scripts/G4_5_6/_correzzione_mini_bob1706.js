/*
LANCIARE LA PROCEDURA
node --max-old-space-size=20000 scripts/G4_5_6/_correzzione_mini_bob1706;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    Utils = require('../../server/routes/utils'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    Prodotto.find({trascodifica:'20170608_04'}).lean()
    .populate('modello') 
     
    .exec(function(err,prodotti){
 
        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, function(prodotto,cb){

            var update = {}; 

            
            
            if (prodotto.codice){
                var p = _.clone(prodotto); 
                 
                 

                if (p.k=='2700' ){
                    lm = "1460"

                    update.$set = {};
                    update.$set.lm = lm
                    bulk.find( { _id: prodotto._id } ).updateOne(update);
                    aggiornati ++; 

                    console.log(aggiornati, p.codice, p.k, lm)

                }
                
               
            }
            cb();
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0 && 1==2) {
             
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
               
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

        })
    })

      
}



);
