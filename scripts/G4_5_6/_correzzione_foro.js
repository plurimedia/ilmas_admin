/*
LANCIARE LA PROCEDURA
node --max-old-space-size=20000 scripts/G4_5_6/_correzzione_foro;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    Utils = require('../../server/routes/utils'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    var query = {
       // generazione: { $in: ['G6','D1'] },
        modello: { $in: [
             //  "56b8aaa90de23c95600bd274", // 1739 Bob Ext Sc Arled Osram 
             //   "56b8aaa90de23c95600bd271",// 1739 Bob Ext Sc Led

              //  "56b8aaa90de23c95600bd275", //1740 Bob Ext Sc Arled Osram   
              //  "56b8aaa90de23c95600bd272"  //1740 Bob Ext Sc Led


             //   "56b8aaa90de23c95600bd26c", //1608 Bob Std Sc Arled
             //   "56b8aaa90de23c95600bd268", //1608 Bob Std Sc Led   
             //   "585bd86567d4880400ccbd66" //1608 Bob Std Sc Toshiba   
             //  "56b8aaa90de23c95600bd2ec", //DL1608 Bob Std Sc --> non MODIFICATI

              //  "56b8aaa90de23c95600bd26d", //1609 Bob Std Sc Arled 
              //  "56b8aaa90de23c95600bd269", //1609 Bob Std Sc Led   
              //  "585bd88267d4880400ccbd67" //1609 Bob Std Sc Toshiba   
              //  "56b8aaa90de23c95600bd2ed",// DL1609 Bob Std Sc

               // "56b8aaa90de23c95600bd26e", //1610 Bob Std Sc Arled   
               //   "56b8aaa90de23c95600bd26a", //1610 Bob Std Sc Led   
               //"56b8aaa90de23c95600bd2ee", //DL1610 Bob Std Sc 

                "56b8aaa90de23c95600bd26f", //1611 Bob Std Sc Arled Osram   
                "56b8aaa90de23c95600bd26b" //1611 Bob Std Sc Led    

                ] }
    } 

    Prodotto.find(
        query
        ).lean()
    .exec(function(err,prodotti){
 
        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, function(prodotto,cb){

            var update = {};  

            
                var p = _.clone(prodotto); 
                 
                 var foro = "";

                if (p.modello=='56b8aaa90de23c95600bd274' ||
                    p.modello=='56b8aaa90de23c95600bd271')
                {
                    foro = "287x156"
                }
                else if (p.modello=='56b8aaa90de23c95600bd275' ||
                    p.modello=='56b8aaa90de23c95600bd272'
                    ){
                     foro = "430x156"
                }
                else if (p.modello=='56b8aaa90de23c95600bd26c' ||
                    p.modello=='56b8aaa90de23c95600bd268' ||
                    p.modello=='585bd86567d4880400ccbd66' ||
                    p.modello=='56b8aaa90de23c95600bd2ec' 
                    ){
                     foro = "156x156"
                }
                else if (p.modello=='56b8aaa90de23c95600bd26d' ||
                    p.modello=='56b8aaa90de23c95600bd269' ||
                    p.modello=='585bd88267d4880400ccbd67' ||
                    p.modello=='56b8aaa90de23c95600bd2ed'  
                    ){
                     foro = "287x156"
                }
                 else if (p.modello=='56b8aaa90de23c95600bd26e' ||
                    p.modello=='56b8aaa90de23c95600bd26a' ||
                    p.modello=='56b8aaa90de23c95600bd2ee'  
                    ){
                     foro = "430x156"
                }
                else if (p.modello=='56b8aaa90de23c95600bd26f' ||
                    p.modello=='56b8aaa90de23c95600bd26b' 
                    ){
                     foro = "285x285"
                } 

                    update.$set = {};
                    update.$set.foro = foro
                    bulk.find( { _id: p._id } ).updateOne(update);
                    aggiornati ++; 

                    console.log(aggiornati, p.codice, foro) 
               
             
            cb();
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0 ) {
             
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
               
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

        })
    })

      
}



);
