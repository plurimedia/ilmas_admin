/*
LANCIARE LA PROCEDURA
node --max-old-space-size=100000 scripts/codiciEan_e_KIT/importEan.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    Utils = require('../../server/routes/utils'),
    mongoose = require('mongoose'),
    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),
    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),
    ProdottoSchema = require('../../server/models/prodotto'),
    CodiceEanSchema = require('../../server/models/codiceEan'),

    Prodotto = mongoose.model('Prodotto'),
    CodiceEan = mongoose.model('CodiceEan'),

     csv = require("fast-csv"),
    fs = require('fs'),
    aggiornati = 0;

  

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    csvpath = "scripts/codiciEan_e_KIT/ean_1_1000.csv",
    codiciEanCsv = [],
    stream = fs.createReadStream(csvpath); 

        csv.fromStream(stream, {
            headers: true,
            delimiter: ',',
            discardUnmappedColumns: true
        })
        .on("data", 
            function (data) {
               
                datiCsv = _.clone(data)

                var object = {
                   codiceEan: datiCsv.GTIN,
                   codiceProdotto : datiCsv.Cod_Ilmas
                }
                codiciEanCsv.push(object)
            }
        )
        .on("end", 
            function () {

                var bulk = Prodotto.collection.initializeOrderedBulkOp();
                var bulk2 = CodiceEan.collection.initializeOrderedBulkOp();

                async.each(codiciEanCsv, 
                    function (p, cb) { 
                        var codiceProdotto = p.codiceProdotto.trim();
                        var codiceEan = p.codiceEan.trim();

                        Prodotto.findOne(
                            {codice:codiceProdotto}, 
                            function (err, result) {
                                if (err){
                                    throw err;
                                }
                                else{

                                    if (result){

                                    var update = {};
                                    update.$set = {};
                                    update.$set.codiceEan=codiceEan;
                                    update.$set.codiceEanLibero=false;
                                    bulk.find( { _id: result._id } ).updateOne(update); 
                                  //  console.log(aggiornati, "   modificato ", result.codice + " " + codiceEan)
                                  
                                  bulk2.insert({codice:codiceProdotto,codiceEan:codiceEan,mdate:new Date()});

                                        
                                    }
                                    else
                                    {
                                        console.log(aggiornati, "   non trovato ", codiceProdotto + " " + codiceEan)
                                        bulk2.insert({codice:codiceProdotto,codiceEan:null,mdate:new Date()});

                                         
                                    }
                                    aggiornati++;

                                     cb();
                                     
                                }
                            }
                        ).lean() 
                         
                    }, 
                    function (err) 
                    {
                        if(aggiornati > 0) {
                             bulk.execute(
                                function(err, result){
                                if(err)
                                    throw err;

                                    bulk2.execute(
                                        function(err, result){
                                        if(err)
                                            throw err;
        
                                        console.log('finito, chiudo la connessione, creati ' + aggiornati + ' nuovi codici')
                                        mongoose.connection.close();
        
                                        }
                                    ) 
                                

                                }
                            )
                        }
                        else {
                            mongoose.connection.close();
                        }
                    }
                )
            }
        ) 
    
}



);
