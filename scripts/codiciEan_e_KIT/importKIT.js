/*
LANCIARE LA PROCEDURA
node --max-old-space-size=100000 scripts/codiciEan_e_KIT/importKIT.js;

db.Prodotto.remove({tag:'20181015'}) 

db.Prodotto.update({kit: true},
{
    $set:{
        exportMetel:false,
        pubblicato:false
    
    }
},
    { multi: true,upsert: false}
)

*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    Utils = require('../../server/routes/utils'),
    mongoose = require('mongoose'),
    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),
    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
 
     csv = require("fast-csv"),
    fs = require('fs'),
    aggiornati = 0;
    codiceNuovo = 0;
    codiceMod = 0;
    codicePadreNonTrovato = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    csvpath = "scripts/codiciEan_e_KIT/KIT_2018.csv",
    codiciNuovi = [],
    stream = fs.createReadStream(csvpath); 

        csv.fromStream(stream, {
            headers: true,
            delimiter: ',',
            discardUnmappedColumns: true
        })
        .on("data", 
            function (data) {
               
                datiCsv = _.clone(data)

                var object = {
                   codicePadre: datiCsv.COD_PADRE,
                   codiceKit : datiCsv.KIT,
                   prezzoKit : datiCsv.PREZZO_KIT.replace('€ ','')
                }
                codiciNuovi.push(object)
            }
        )
        .on("end", 
            function () {

                var bulk = Prodotto.collection.initializeOrderedBulkOp();
                async.each(codiciNuovi, 
                    function (p, cb) {
                        var codicePadre = p.codicePadre.trim();
                        var codiceKit = p.codiceKit.trim();
                        var prezzoKit = p.prezzoKit.trim();

                        Prodotto.findOne(
                            {codice:codicePadre}, 
                            function (err, result) {
                                if (err){
                                    throw err;
                                }
                                else{

                                    
                                    if (result){ //codicePadre Esistente

                                        var prodotoPadre = _.clone(result);

                                            //Controlliamo se il nuovo KIT esiste già
                                            Prodotto.findOne(
                                                {codice:codiceKit}, 
                                                function (err, kitTrovato) {
                                                    if (err){
                                                        throw err;
                                                    }
                                                    else{
                                                     
 
                                                        if (kitTrovato){//esiste già un KIT con questo codice, facciamo update
                                                            
                                                            kitTrovato.codice=codiceKit
                                                            kitTrovato.padre=codicePadre
                                                            kitTrovato.prezzo=prezzoKit
                                                            kitTrovato.kit=true
                                                            kitTrovato.exportMetel=false
                                                            kitTrovato.pubblicato=true
                                                            kitTrovato.tag='20181015'
                                                            kitTrovato.mdate = new Date();
                                                            kitTrovato.codiceEan=null
                                                            key = Utils.keyWords(kitTrovato);

                                                             var update = {};
                                                            update.$set = {};
                                                            update.$set.codice=codiceKit
                                                            update.$set.padre=codicePadre
                                                            update.$set.prezzo=prezzoKit
                                                            update.$set.kit=true
                                                            update.$set.exportMetel=false
                                                            update.$set.pubblicato=true
                                                            update.$set.tag='20181015'
                                                            update.$set.mdate = new Date();
                                                            update.$set.keywords = key;
                                                            update.$set.codiceEan=null
                                                            update.$set.modello_cript=null
                                                            update.$set.accessori=[]
                                                           // update.$set.modello =  kit.modello._id
                                                           // update.$set.categoria =  kit.categoria._id
                                                           update.$set.componenti=[]
                                                           update.$set.distintabase=null
                                                           update.$set.codiceEanLibero=false
                                                            bulk.find( { _id: kitTrovato.codice } ).updateOne(update); 

                                                            console.log(aggiornati, "   aggiornato codice ", codiceKit, " generazione ", kitTrovato.generazione)

                                                            codiceMod++;
                                                        }
                                                        else{

                                                            nuovoKit = _.clone(prodotoPadre);
                                                             nuovoKit._id=null
                                                            nuovoKit.codice=codiceKit
                                                            nuovoKit.padre=codicePadre
                                                            nuovoKit.prezzo=prezzoKit
                                                            nuovoKit.kit=true
                                                            nuovoKit.exportMetel=false
                                                            nuovoKit.pubblicato=true
                                                            nuovoKit.tag='20181015'
                                                            nuovoKit.mdate = new Date();
                                                            nuovoKit.codiceEan=null;
                                                            key =  Utils.keyWords(nuovoKit); 
                                                            nuovoKit.keywords = key
                                                            nuovoKit.modello =  nuovoKit.modello._id
                                                            nuovoKit.categoria =  nuovoKit.categoria._id
                                                            nuovoKit.modello_cript=null
                                                            nuovoKit.accessori=[]
                                                            nuovoKit.componenti=[]
                                                            nuovoKit.distintabase=null
                                                            nuovoKit.codiceEanLibero=false
                                                            bulk.insert(nuovoKit);
                                                            console.log(aggiornati, "   inserito codice ", codiceKit , ' generazione: ' , prodotoPadre.generazione)
                                                            codiceNuovo++;
                                                        }

                                                        aggiornati++;
                                                       
                                                        cb();
                                                    }
                                                }
                                            ) 
                                            .populate('modello',{'nome':1})
                                            .populate('categoria',{'nome':1})

                                    }
                                    else
                                    {
                                        console.log(aggiornati, "   non trovato codice padre ", codicePadre )
                                        aggiornati++;
                                        codicePadreNonTrovato++

                                          cb();
                                    }

                                   // aggiornati++;

                                  //  cb();
                                     
                                }
                            }
                        ).lean() 
                        .populate('modello',{'nome':1})
                        .populate('categoria',{'nome':1})
                         
                    }, 
                    function (err) 
                    {
                        if(aggiornati > 0) {
                             bulk.execute(
                                function(err, result){
                                if(err)
                                    throw err;

                                console.log('creati ' + codiceNuovo + ' nuovi codici')
                                console.log('aggiornati ' + codiceMod + ' codici')
                                console.log('Codici non generati perchè Codice Padre Non trovato n:' + codicePadreNonTrovato + ' codici')
                                
                                mongoose.connection.close();

                                }
                            )
                        }
                        else {
                            mongoose.connection.close();
                        }
                    }
                )
            }
        ) 
    
}



);
