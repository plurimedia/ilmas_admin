/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/_eliminaProdotti_DOPPIO;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console
  
    Prodotto.find({codice: /-DOPPIO$/}).lean()
    .exec(function(err,prodotti){

        console.log('aggiorno '+prodotti.length+' prodotti')

        var bulkProdotti = Prodotto.collection.initializeOrderedBulkOp(); 

        async.each(prodotti, 
            function(prodotto,cb)
            {
            var update = {};

            console.log("aggiornamento prodotto id = ", prodotto.codice);
            
            if (prodotto.codice)
            { 
                bulk.find( { _id: prodotto._id } ).remove();
                aggiornati ++; 
            }
            cb();
            },
             function(err)
            {

            if(err)
                throw err;

            if(aggiornati > 0) {
             
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: eliminati '+aggiornati+' prodotti')
                   
                    mongoose.connection.close();
                })
               
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

        })
    }) 
      
}



);
