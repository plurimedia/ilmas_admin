//flagga automaticamente il checkbox 'visibili sul sito' di una serie di codici letti da un foglio excel nella sua prima colonna

// Elimina prodotti leggendo i codici nella prima colonna di un foglio excel

const Excel = require('exceljs');
const util = require('util');

var mongoose = require('mongoose'),
  CategoriaSchema = require('../../server/models/categoria'),
  ModelloSchema = require('../../server/models/modello'),
  ProdottoSchema = require('../../server/models/prodotto'),
  CodiceEan = require('../../server/models/codiceEan'),
  Sottocategoria = require('../../server/models/sottocategoria'),
  CreazioneDistinte = require('../../server/models/creazioneDistinte')

ObjectId = require('mongoose').Types.ObjectId;
Prodotto = mongoose.model('Prodotto'),
  CodiceEan = mongoose.model('CodiceEan'),
  Modello = mongoose.model('Modello'),
  Sottocategoria = mongoose.model('Sottocategoria'),
  Utils = require('../../server/routes/utils'),
  _ = require('underscore'),
  axios = require('axios'),
  async = require('async'),
  fs = require("fs"),
  csv = require("fast-csv"),
  numeral = require('numeral'),
  numeral_it = require('numeral/languages/it'),
  Email = require('../../server/routes/email'),
  modello_route = require('../../server/routes/modello'),

  fs = require('fs'),
  pdf = require('html-pdf'),
  CONFIG = require('../../server/config.js'),
  db = CONFIG.MONGO,
  _ = require('underscore'),
  Mustache = require('mustache'),
  classeEnergetica = require('../../server/routes/utils.js').classeEnergetica,
  formatters = require('../../server/routes/utils.js').formatters,
  headerSchedaTecnica = fs.readFileSync('./server/templates/pdf/intestazione_scheda_tecnica.html', 'utf8'), // scheda tecnica...
  footerSchedaTecnica = fs.readFileSync('./server/templates/pdf/footer_scheda_tecnica.html', 'utf8'), // footer scheda tecnica cc...
  tpl = [],
  css = fs.readFileSync('./server/templates/pdf/pdf.css', 'utf8'); // comune a tutti
tpl[0] = fs.readFileSync('./server/templates/pdf/scheda_tecnica.html', 'utf8')
tpl[1] = fs.readFileSync('./server/templates/pdf/schedaTecnicaBinario.html', 'utf8')

mongoose.connect(db);

//trova prodotto da excel
const filename = './scripts/kit_promozionali/aggiorna.xlsx';
const workbook = new Excel.Workbook();
workbook.creator = 'test';
workbook.modified = '42';

(async function () {
  await workbook.xlsx.readFile(filename);
  const worksheet = workbook.worksheets[0]; //seleziona il primo foglio di lavoro
  worksheet.eachRow(async (row, rowNumber) => {
    await Prodotto.updateOne({ codice: row.values[1] }, { pubblicato: 1 }, function (err, pr) {
      if (err) console.log(err)
      else console.log("Updated: ", row.values[1], "at line: ", rowNumber);
    });
  })

})();

