const async = require('async')
const fs = require('fs')
const MongoClient = require('mongodb').MongoClient


// ATTENZIONE: VERIFICARE IL DB A CUI CI SI CONNETTE (default: dev)
const conn = 'mongodb://ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08:1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139@37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149/ilmas_dev?authSource=admin&ssl=true&replicaSet=replset'

async function run() {

  const client = new MongoClient(conn);
  let db = await client.connect(conn);
  await db.collection('Prodotto').updateMany({ codice: { $regex: /^(K0)/ } }, { $set: { 'pubblicato': false, 'exportMetel': false } });

}

run().catch(error => console.error(error)).then(() => { console.log('Update Complete'); process.kill(process.pid, 'SIGTERM') });

