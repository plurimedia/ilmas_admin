//Crea dei "kit promozionali", appositamente flaggati.
// Legge i codici padre da un foglio excel, li clona attribuendogli il codice del kit e il prezzo speciale:
// Colonna 1: codici padre (gia' sul DB)
// Colonna 2: nuovi codici per i kit
// Colonna 3: nuovo prezzo per il kit

const Excel = require('exceljs');
const util = require('util');

var mongoose = require('mongoose'),
  CategoriaSchema = require('../../server/models/categoria'),
  ModelloSchema = require('../../server/models/modello'),
  ProdottoSchema = require('../../server/models/prodotto'),
  CodiceEan = require('../../server/models/codiceEan'),
  Sottocategoria = require('../../server/models/sottocategoria'),
  CreazioneDistinte = require('../../server/models/creazioneDistinte')

ObjectId = require('mongoose').Types.ObjectId;
Prodotto = mongoose.model('Prodotto'),
  CodiceEan = mongoose.model('CodiceEan'),
  Modello = mongoose.model('Modello'),
  Sottocategoria = mongoose.model('Sottocategoria'),
  Utils = require('../../server/routes/utils'),
  _ = require('underscore'),
  axios = require('axios'),
  async = require('async'),
  fs = require("fs"),
  csv = require("fast-csv"),
  numeral = require('numeral'),
  numeral_it = require('numeral/languages/it'),
  Email = require('../../server/routes/email'),
  modello_route = require('../../server/routes/modello'),

  fs = require('fs'),
  pdf = require('html-pdf'),
  CONFIG = require('../../server/config.js'),
  db = CONFIG.MONGO,
  _ = require('underscore'),
  Mustache = require('mustache'),
  classeEnergetica = require('../../server/routes/utils.js').classeEnergetica,
  formatters = require('../../server/routes/utils.js').formatters,
  headerSchedaTecnica = fs.readFileSync('./server/templates/pdf/intestazione_scheda_tecnica.html', 'utf8'), // scheda tecnica...
  footerSchedaTecnica = fs.readFileSync('./server/templates/pdf/footer_scheda_tecnica.html', 'utf8'), // footer scheda tecnica cc...
  tpl = [],
  css = fs.readFileSync('./server/templates/pdf/pdf.css', 'utf8'); // comune a tutti
tpl[0] = fs.readFileSync('./server/templates/pdf/scheda_tecnica.html', 'utf8')
tpl[1] = fs.readFileSync('./server/templates/pdf/schedaTecnicaBinario.html', 'utf8')

mongoose.connect(db);

save = function (newKit, newCode, newPrice) {
  var data = newKit;
  var prodotto = new Prodotto(data);

  prodotto.mdate = new Date();
  prodotto.last_user = newKit.user;
  prodotto.codice = newCode;
  prodotto.prezzo = newPrice;
  //flag di riconoscimento per archiviazione massiva
  prodotto.promozione = true;

  prodotto.save()

}

//trova prodotto dal foglio excel
const filename = './scripts/kit_promozionali/kitPluri.xlsx'; //cambiare a seconda del foglio excel da leggere
const workbook = new Excel.Workbook();
workbook.creator = 'test';
workbook.modified = '42';

(async function () {
  const workbookReader = new Excel.stream.xlsx.WorkbookReader(filename);
  for await (const worksheetReader of workbookReader) {
    for await (const row of worksheetReader) {
      let prodotto = await Prodotto.findOne({ codice: row.values[1] })
        .populate('modello', { 'nome': 1, 'peso': 1, 'volume': 1, 'ecolamp': 1 })
        .populate('modello_cript', { 'nome': 1 })
        .populate('categoria', 'nome')
        .populate('accessori')
        .exec();

      if (!prodotto) {
        console.log('prodotto non trovato: ', row.values[1]);
      } else {
        prodotto.en = 'en';

        let newKit = _.clone(prodotto)
        let kitCode;
        let kitPrice;
        if (row.values[2]) kitCode = row.values[2];
        if (row.values[3]) kitPrice = row.values[3];
        // per garantire univocita' nuovo prodotto
        delete newKit._id;

        save(newKit, kitCode, kitPrice);

      }
    }
  }
})();

console.log('Esecuzione terminata');
