/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/modificacontatti_keywords;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ContattoSchema = require('../server/models/contatto'),
    Contatto = mongoose.model('Contatto'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    Contatto.find() 
    .exec(function(err,contatti){

        console.log('aggiorno '+contatti.length+' contatti')

        var bulk = Contatto.collection.initializeOrderedBulkOp();
        async.each(contatti, function(contatto,cb){

            var update = {};
 
            console.log("aggiornamento contatto id = ", contatto.nome);
            
             var c = _.clone(contatto);  
 
            update.$set = {};
            update.$set.keywords = _keyWords(c);
            bulk.find( { _id: contatto._id } ).updateOne(update);
            aggiornati ++; 

            cb();
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0) {
             
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
               
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

        })
    })


_keyWords = function (c) {
        var result = [];
        
        result.push(c.email);
        result.push(c.email.split('@')[0])
        result.push(c.email.split('@')[1].split('.')[0])
        
        if(c.nome)
            result.push(_.map(c.nome.split(' '),function(k){return k.toLowerCase()}));
        if(c.nome)
            result.push(_.map(c.nome.split(' '),function(k){return k.toUpperCase()}));
        if(c.nome)
            result.push(_.map(c.nome.split(' '),function(k){return k}));

        if(c.azienda)
            result.push(_.map(c.azienda.split(' '),function(k){return k.toLowerCase()}));
        if(c.azienda)
            result.push(_.map(c.azienda.split(' '),function(k){return k.toUpperCase()}));
        if(c.azienda)
            result.push(_.map(c.azienda.split(' '),function(k){return k}));
 

        if(c.tags.length)
            result.push(c.tags)
         
        if(c.comune)
            result.push(_.map(c.comune.split(' '),function(k){return k.toLowerCase()}));
        if(c.comune)
            result.push(_.map(c.comune.split(' '),function(k){return k.toUpperCase()}));
        if(c.comune)
            result.push(_.map(c.comune.split(' '),function(k){return k}));

 
        if(c.nazione)
            result.push(_.map(c.nazione.split(' '),function(k){return k.toLowerCase()}));
        if(c.nazione)
            result.push(_.map(c.nazione.split(' '),function(k){return k.toUpperCase()}));
        if(c.nazione)
            result.push(_.map(c.nazione.split(' '),function(k){return k}));

            
        return _.flatten(result);
    };
     
}



);
