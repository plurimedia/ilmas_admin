const async = require('async')
const fs = require('fs')
const MongoClient = require('mongodb').MongoClient
const dir_path = '/Users/archimede/Downloads/ldts7'


// ATTENZIONE: VERIFICARE IL DB A CUI CI SI CONNETTE
const conn = 'mongodb://ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08:1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139@37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149/ilmas_prod?authSource=admin&ssl=true&replicaSet=replset'

run().catch(error => console.error(error))

async function run()
{
  let url = 'https://ilmas-admin.s3-eu-west-1.amazonaws.com/fotometrie/'

  const client = new MongoClient(conn)
  let db = await client.connect(conn)

  fs.readdir(dir_path, function (err, files) {
    if (err) return console.log('Unable to find or open the directory: ' + err)

    files.forEach(async function (file) {
      let prefix = file.split('.')[0]
      if (prefix && prefix.length===7) {
        const obj = new Object()
        obj.nome = file
        obj.url = url + file
        let upd = await db.collection('Prodotto').updateMany({ codice: new RegExp('^' + prefix) }, { $set: { "file_ldt": obj } })
        console.log(file, 'matched: '+upd.matchedCount, 'modified: '+upd.modifiedCount)
      }
    })
  })
}
