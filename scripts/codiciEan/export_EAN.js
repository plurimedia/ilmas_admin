// Ricerca codici da foglio excel (colonna 1) e associa il corrispondente EAN se esiste. I console log sono scritti per fornire un elenco in formato csv
// quando si lancia lo script scrivere i log in un file.csv.

const Excel = require('exceljs');
const util = require('util');

var mongoose = require('mongoose'),
  CategoriaSchema = require('../../server/models/categoria'),
  ModelloSchema = require('../../server/models/modello'),
  ProdottoSchema = require('../../server/models/prodotto'),
  CodiceEan = require('../../server/models/codiceEan'),
  Sottocategoria = require('../../server/models/sottocategoria'),
  CreazioneDistinte = require('../../server/models/creazioneDistinte')

ObjectId = require('mongoose').Types.ObjectId;
Prodotto = mongoose.model('Prodotto'),
  CodiceEan = mongoose.model('CodiceEan'),
  Modello = mongoose.model('Modello'),
  Sottocategoria = mongoose.model('Sottocategoria'),
  Utils = require('../../server/routes/utils'),
  _ = require('underscore'),
  axios = require('axios'),
  async = require('async'),
  fs = require("fs"),
  csv = require("fast-csv"),
  numeral = require('numeral'),
  numeral_it = require('numeral/languages/it'),
  Email = require('../../server/routes/email'),
  modello_route = require('../../server/routes/modello'),

  fs = require('fs'),
  pdf = require('html-pdf'),
  CONFIG = require('../../server/config.js'),
  db = CONFIG.MONGO,
  _ = require('underscore'),
  Mustache = require('mustache'),
  classeEnergetica = require('../../server/routes/utils.js').classeEnergetica,
  formatters = require('../../server/routes/utils.js').formatters,
  headerSchedaTecnica = fs.readFileSync('./server/templates/pdf/intestazione_scheda_tecnica.html', 'utf8'), // scheda tecnica...
  footerSchedaTecnica = fs.readFileSync('./server/templates/pdf/footer_scheda_tecnica.html', 'utf8'), // footer scheda tecnica cc...
  tpl = [],
  css = fs.readFileSync('./server/templates/pdf/pdf.css', 'utf8'); // comune a tutti
tpl[0] = fs.readFileSync('./server/templates/pdf/scheda_tecnica.html', 'utf8')
tpl[1] = fs.readFileSync('./server/templates/pdf/schedaTecnicaBinario.html', 'utf8')

mongoose.connect(db);

//trova prodotto dal foglio excel

const workbook = new Excel.Workbook();
const fileName = './scripts/codiciEan/excel_EAN.xlsx';

(async function () {

  const workbookReader = new Excel.stream.xlsx.WorkbookReader(fileName);
  for await (const worksheetReader of workbookReader) {
    for await (const row of worksheetReader) {
      let prodotto = await Prodotto.findOne({ codice: row.values[1] }).exec();

      if (!prodotto) {
        console.log(row.values[1] + ',Prodotto non trovato');
      } else {
        console.log(prodotto.codice + ',' + prodotto.codiceEan);
      }
    }
  }
  process.exit();
})();