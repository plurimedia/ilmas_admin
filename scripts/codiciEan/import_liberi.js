/*
LANCIARE LA PROCEDURA
node --max-old-space-size=20000 scripts/codiciEan/import_liberi.js;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
     _ = require('underscore'),
    csv = require("fast-csv");
    fs = require('fs'),
    async = require('async'),
    Utils = require(process.cwd() + '/server/routes/utils'),
    db = CONFIG.MONGO,
//db='mongodb://plurimedia:Canonico27!@ds057505-a0.mongolab.com:57505,ds057505-a1.mongolab.com:57505/heroku_rlzkx1m6?replicaSet=rs-ds057505'

    mongoose = require('mongoose'),
    CategoriaSchema = require('../../server/models/categoria'),
    Categoria = mongoose.model('Categoria'),
    ModelloSchema = require('../../server/models/modello'),
    Modello = mongoose.model('Modello'),
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

   aggiornati=0,
    csvpath = "./elenco_codici_ean_liberi.csv",
    csvpath = "scripts/codiciEan/elenco_codici_ean_liberi_96_fine.csv",
    
    stream = fs.createReadStream(csvpath),
    codiciEan = [] 
    strLog=""

    mongoose.connect(db);

   

mongoose.connection.once('open', function()
{
    console.log("Connessione aperta, parte il batch.");


    var bulk = Prodotto.collection.initializeOrderedBulkOp()

     csv.fromStream(stream, {
            headers: true,
            delimiter: ',',
            discardUnmappedColumns: true
        }) 
        //RECUPERO I DATI DAL FILE
        .on("data", 
            function (data) {
                codiciEan.push(_.clone(data.GTIN));
            }
        )
        .on("end", 
            function () {
                var query = {
                    generazione: { $in: ['G6', 'D1', 'D2', 'D1', 'V1', '3HE', '', null ] },
                    kit: {
                        $in: [
                            false, null
                        ]
                    },
                    criptato: {
                        $in: [
                            false, null
                        ]
                    },
                    speciale: {
                        $in: [
                            false, null
                        ]
                    },
                    codiceEan: {
                        $in: [
                            '', null
                        ]
                    },
                }

                console.log(codiciEan.length)
console.log(query)
                Prodotto.find(query)
                .lean()
                .sort({mdate:-1})
                .limit(codiciEan.length)
                .exec(
                    function(err, prodotti)
                    {
                        console.log("prodotti ", prodotti)
                        async.each(prodotti, 
                            function(prodotto, cb)
                            {
                                var pClone = _.clone(prodotto);

                                var update = {};
                                update.$set = {};

                                console.log( aggiornati, 
                                    " associazione codiciEan ", codiciEan[aggiornati] + " codice ", pClone.codice )

                                update.$set.codiceEan = codiciEan[aggiornati].trim();
                               update.$set.codiceEanLibero=true
                                bulk.find({_id: pClone._id}).updateOne(update);
                                 aggiornati++; 
                                cb();
                            },
                            function(err)
                            {
                                console.log("aggiornati ", aggiornati)
                                if (err){
                                    console.log(err)
                                    throw err;
                                }
                                if (aggiornati > 0)
                                {
                                    bulk.execute(
                                        function(result) {  

                                        mongoose.connection.close();

                                        console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                                    
                                    })
                                }
                                else
                                {
                                    console.log('Non ci sono prodotti da aggiornare')
                                    mongoose.connection.close();
                                }
                            }
                        )
                    }
                )
            }
        )
    }
)