/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/modificaDwh;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    DwhEtichettaSchema = require('../server/models/dwhEtichetta'),
    DwhEtichetta = mongoose.model('DwhEtichetta'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    DwhEtichetta.find().lean() 
    .exec(function(err,records){

        console.log('aggiorno '+records.length+' componenti')

        var bulk = DwhEtichetta .collection.initializeOrderedBulkOp();

        async.each(records, function(rec,cb){

            var update = {}; 
             
            var p = _.clone(rec);

            var dataM = p.mdate;
            

            var anno = dataM.getFullYear().toString();
            var mese = (dataM.getMonth()+1).toString();
            var giorno = dataM.getDate().toString(); 
            
            update.$set = {};
            update.$set.anno = anno;
            update.$set.mese = mese;
            update.$set.giorno = giorno;



            bulk.find( { _id: rec._id } ).updateOne(update);
            aggiornati ++; 

                 
             
            cb();
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0) {
             
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' componenti')
                    mongoose.connection.close();

                })
               
            }
            else {
                console.log('Non ci sono componenti da aggiornare')
                mongoose.connection.close();
            }

        })
    })

     
}



);
