/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/modificaprodotti_associaLinea;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
 
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); 
    
    Prodotto.find() 
    .exec(
        function(err,prodotti){       

        var bulk = LineaCommerciale.collection.initializeOrderedBulkOp();
var codici = [];

        async.each(prodotti,
         function(prodotto,cb){ 

                    var update = {};  

                    if (prodotto.codice.substr(3,1) == 'G' ||
                        prodotto.codice.substr(3,1) == 'H'  ||
                        prodotto.codice.substr(3,1) == 'J'  ||
                        prodotto.codice.substr(3,1) == 'K'  ||
                        prodotto.codice.substr(3,1) == 'L'  ||
                        prodotto.codice.substr(3,1) == 'M'  ||
                        prodotto.codice.substr(3,1) == 'N'  ||
                        prodotto.codice.substr(3,1) == 'P'  ||
                        prodotto.codice.substr(3,1) == 'S'  ||
                        prodotto.codice.substr(3,1) == 'T'  ||
                        prodotto.codice.substr(3,1) == 'U'  ||
                        prodotto.codice.substr(3,1) == 'V'){
                        
  codici.push(prodotto.codice);
}


                     
/*
                    update.$set = {};
                    update.$set.categorieAssociate = categorieAssociate;
                    update.$set.modelliAssociati = modelliAssociati;
                    update.$set.codiciAssociati = codiciAssociati;

                    bulk.find( { nome: 'Food' } ).updateOne(update);
    */
                   // aggiornati ++;

                 

            cb();
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0) {
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                console.log(codici)
                mongoose.connection.close();
            }

        })
    }).limit(10000)

      

});
