var process = require('./massive.js');
var modello = ['6321e0ea5f7cb5535afac151', '6321e10c5f7cb5535afac1a2'];
var query = { modello: { $in: modello } };
var tag = 'dani_20221027_06';

var transform1 = {
    idx: 1,
    tag: tag,
    query: query,
    preProcess: 'backupProdLocal',//'getFromProd',
    checkDuplicates: true,
    filter: [process.filters.hasCharAt,process.filters.hasCharAt],
    filterArguments: [{ chars: ['O'], pos: 5 },{ chars: ['B'], pos: 6 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p.codice = process.replaceCharAt(p.codice, 'C', 6)

                p.w = '17' // potenza
                p.ma = '500' // corrente

                switch (p.lm) {
                    case '1615':
                        p.lm = '2270'
                        break
                }

                return p
            }
        }
    ]
};

var transform2 = {
    idx: 2,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt,process.filters.hasCharAt],
    filterArguments: [{ chars: ['O'], pos: 5 },{ chars: ['B'], pos: 6 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p.codice = process.replaceCharAt(p.codice, 'Q', 6)

                p.w = '21' // potenza
                p.ma = '600' // corrente

                switch (p.lm) {
                    case '1615':
                        p.lm = '2695'
                        break
                }

                return p
            }
        }
    ]
};

var transform3 = {
    idx: 3,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['O'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p.codice = process.replaceCharAt(p.codice, 'S', 5)

                p.k = '3000'

                switch (p.lm) {
                    case '1615':
                        p.lm = '1700'
                        break
                    case '2270':
                        p.lm = '2390'
                        break
                    case '2695':
                        p.lm = '2835'
                        break
                }

                return p
            }
        }
    ]
};

var transform4 = {
    idx: 4,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['O'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p.codice = process.replaceCharAt(p.codice, 'T', 5)

                p.k = '4000'

                switch (p.lm) {
                    case '1615':
                        p.lm = '1785'
                        break
                    case '2270':
                        p.lm = '2510'
                        break
                    case '2695':
                        p.lm = '2980'
                        break
                }

                return p;
            }
        }
    ]
};

var transform5 = {
    idx: 5,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['O'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.vivid(p)

                if (p.codice.length === 8) p.prezzo += 10
                else if (p.codice.length === 9 && (process.hasCharAt(p.codice, 'D', 9) || process.hasCharAt(p.codice, 'L', 9))) p.prezzo += 10

                switch (p.lm) {
                    case '1615':
                        p.lm = '1460'
                        break
                    case '2270':
                        p.lm = '2030'
                        break
                    case '2695':
                        p.lm = '2400'
                        break
                }

                return p;
            }
        }
    ]
};

var transform6 = {
    idx: 6,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['X'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 5)
                p.k = '3500'

                switch (p.lm) {
                    case '1460':
                        p.lm = '1500'
                        break
                    case '2030':
                        p.lm = '2085'
                        break
                    case '2400':
                        p.lm = '2470'
                        break
                }

                return p;
            }
        }
    ]
};


var transform7 = {
    idx: 7,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['S'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.sunlike(p)

                if (p.codice.length === 9 && p.sottocategoria === 'Sunlike') {
                    console.log('cambio prezzo 1', p.codice)
                    p.prezzo += 30
                }/*  else if (p.codice.length === 10 && (process.hasCharAt(p.codice, 'D', 9) || process.hasCharAt(p.codice, 'L', 9))) {
                    console.log('cambio prezzo 2', p.codice)
                    p.prezzo += 30
                } */
                if (process.hasCharsAt(p.codice, '0546', 1) && 
                    process.hasCharAt(p.codice, 'S', 6) && 
                    (process.hasCharAt(p.codice, 'B', 7) || process.hasCharAt(p.codice, 'C', 7) || process.hasCharAt(p.codice, 'Q', 7)) &&
                    (process.hasCharAt(p.codice, 'D', 10) || process.hasCharAt(p.codice, 'L', 10)))
                    p.prezzo = 206

                if (process.hasCharsAt(p.codice, '0536', 1) && 
                    process.hasCharAt(p.codice, 'S', 6) && 
                    (process.hasCharAt(p.codice, 'B', 7) || process.hasCharAt(p.codice, 'C', 7) || process.hasCharAt(p.codice, 'Q', 7)) &&
                    (process.hasCharAt(p.codice, 'D', 10) || process.hasCharAt(p.codice, 'L', 10)))
                    p.prezzo = 186



                switch (p.lm) {
                    case '1700':
                        p.lm = '1200'
                        break
                    case '2390':
                        p.lm = '1700'
                        break
                    case '2835':
                        p.lm = '2100'
                        break
                }

                return p
            }
        }
    ]
};

var transform8 = {
    idx: 8,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt],
    filterArguments: [{ chars: ['1'], pos: 8 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.nero(p)

                return p
            }
        }
    ]
};

var transform9 = {
    idx: 9,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt, process.filters.hasSottoCategoria],
    filterArguments: [{ chars: ['1'], pos: 9 }, { sottocategoria: 'Sunlike' }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, '2', 9)
                p.colore = 'Nero'

                return p
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function (err) { return process.execute(transform8) })
    .then(function (err) { return process.execute(transform9) })
    .then(function (err) { console.log("finito!!"); })
    .catch(function () { console.log("fallito!!"); });


/*
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto0536.json --query='{"modello":{"$oid":"6321e0ea5f7cb5535afac151"}}'
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto0546.json --query='{"modello":{"$oid":"6321e10c5f7cb5535afac1a2"}}'

db.Prodotto.remove({modello:{$in:[ObjectId('6321e0ea5f7cb5535afac151'),ObjectId('6321e10c5f7cb5535afac1a2')]}})

mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto0536.json
mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto0546.json


Per il modello "0536 Smart/P S" - "0546 Smart/S S"

1.Duplicare i codici con 5° carattere = O:
-il 6° carattete = B --> C
-il campo "potenza" --> 17
-il campo "corrente" --> 500
-il campo "Flusso lum. nom":
-1615 --> 2270

2.Duplicare i codici con 5° carattere = O:
-il 6° carattete = B --> Q
-il campo "potenza" --> 21
-il campo "corrente" --> 600
-il campo "Flusso lum. nom":
-1615 --> 2695

3.Duplicare i codici con 5° carattere = O:
-il 5° carattere = O --> S
-il campo "Temp. di colore" --> "3000"
-il campo "Flusso lum. nom":
-1615 --> 1700
-2270 --> 2390
-2695 --> 2835

4.3.Duplicare i codici con 5° carattere = O:
-il 5° carattere = O --> T
-il campo "Temp. di colore" --> "4000"
-il campo "Flusso lum. nom":
-1615 --> 1785
-2270 --> 2510
-2695 --> 2980


3.Duplicare tutti i codici con 5° carattere = "O" :
   -5° carattere --> "X"
   -il campo "Temp. di colore" --> "3300"
   -il campo "CRI" --> "95"
   -il campo "Sottocategoria" --> "Vivid"
   -If codice lunga massima 8 caratteri --> il campo "Prezzo" = +10€
   -If codice lunga massima 9 caratteri and il 9° carattere = D or L--> il campo "Prezzo" = +10€
   -il campo "Flusso lum. nom.":
     1615 --> 1460
     2270 --> 2030
     2695 --> 2400



4.Duplicare tutti i codici con 5° carattere = "X" :
   -5° carattere --> "D"
   -il campo "Temp. di colore" --> "3500"
   -il campo "Flusso lum. nom.":
     1460 --> 1500
     2030 --> 2085
     2400 --> 2470

5.Duplicare tutti i codici con 5° carattere = "S" :
   -5° carattere --> "SS"
   -il campo "CRI" --> "97.6"
   -il campo "Sottocategoria" --> "Sunlike"
   -If codice lunga massima 9 caratteri and "Sottocategoria" = "Sunlike"--> il campo "Prezzo" = +30€
   -If codice lunga massima 10 caratteri and il 10° carattere = D or L--> il campo "Prezzo" = +30€
   -il campo "Flusso lum. nom.":
    1700 --> 1200
    2390 --> 1700
    2835 --> 2100

6.Duplicare tutti i codici:
-8° carattere = 1 --> 2
-if sottocategoria "Sunlike" and 9° carattere = 1 --> 2
-il campo "Colore" = "Nero"



N.B. --> +_€ = AGGIUNGERE



 */