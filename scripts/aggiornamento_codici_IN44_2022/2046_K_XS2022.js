var process = require('./massive.js')
var modello = '5ce65a2809bb2e0004cf724c'
var query = { modello: modello }
var tag = 'dani_20221026_02'

var transform1 = {
  idx: 1,
  tag: tag,
  query: query,
  preProcess: 'backupProdLocal',//'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['Z'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {

        p.codice = process.replaceCharAt(p.codice, '7', 5)

        switch (p.lm) {
          case '1125':
            p.lm = '1185'
            break
        }

        return p;
      }
    }
  ]
};

var transform2 = {
  idx: 2,
  tag: tag,
  query: query,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['Z'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {

        p.codice = process.replaceCharAt(p.codice, '8', 5)

        switch (p.lm) {
          case '1125':
            p.lm = '1245'
            break
        }

        return p
      }
    }
  ]
};

var transform3 = {
  idx: 3,
  tag: tag,
  query: query,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['B'], pos: 7 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'F', 7)

        p.fascio = '24'

        return p;
      }
    }
  ]
};

var transform4 = {
  idx: 4,
  tag: tag,
  query: query,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['B'], pos: 7 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'M', 7)

        p.fascio = '38'

        return p;
      }
    }
  ]
};

var transform5 = {
  idx: 5,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['B'], pos: 7 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'T', 7)
        p.fascio = '60'
        p.ottica_it = 'Riflettore in alluminio'
        p.ottica_en = 'Aluminium reflector'

        return p;
      }
    }
  ]
};

var transform6 = {
  idx: 6,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['1'], pos: 8 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.nero(p)

        return p
      }
    }
  ]
};

var transform7 = {
  idx: 7,
  tag: tag,
  query: query,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['7'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.vivid(p)

        p.prezzo = 211
        switch (p.lm) {
          case '1185':
            p.lm = '1000'
            break
        }

        return p
      }
    }
  ]
};

var transform8 = {
  idx: 8,
  tag: tag,
  query: query,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['X'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'D', 5)

        p.k = '3500'
        switch (p.lm) {
          case '1000':
            p.lm = '1015'
            break
        }

        return p
      }
    }
  ]
};

var transform9 = {
  idx: 9,
  tag: tag,
  query: query,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['7'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.sunlike(p)

        p.prezzo = 234
        p.w = '9' // potenza

        switch (p.lm) {
          case '1185':
            p.lm = '900'
            break
        }

        return p
      }
    }
  ]
};

process.execute(transform1)
  .then(function (err) { return process.execute(transform2) })
  .then(function (err) { return process.execute(transform3) })
  .then(function (err) { return process.execute(transform4) })
  .then(function (err) { return process.execute(transform5) })
  .then(function (err) { return process.execute(transform6) })
  .then(function (err) { return process.execute(transform7) })
  .then(function (err) { return process.execute(transform8) })
  .then(function (err) { return process.execute(transform9) })
  .then(function (err) { console.log("finito!!"); })
  .catch(function () { console.log("fallito!!"); });


/*
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto2046.json --query='{"modello":{"$oid":"5ce65a2809bb2e0004cf724c"}}'

db.Prodotto.remove({modello:ObjectId('5ce65a2809bb2e0004cf724c')})

mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto2046.json

Per il modello "2046 K/XS":

1.Duplicare i codici con 5° carattere = Z:
-il 5° carattere = Z --> 7
-il campo "Flusso lum. nom":
-1125 --> 1185

2.Duplicare i codici con 5° carattere = Z:
-il 5° carattere = Z --> 8
-il campo "Flusso lum. nom":
-1125 --> 1245

3.Duplicare i codici con 7° carattere = B:
-il 7° carattere = B --> F
-il campo "fascio" = 24

4.Duplicare i codici con 7° carattere = B:
-il 7° carattere = B --> M
-il campo "fascio" = 38

5.Duplicare i codici con 7° carattere = B:
-il 7° carattere = B --> T
-il campo "fascio" = 60
-il campo "Costruzione ottica (IT)" = "Riflettore in alluminio"
-il campo "Costruzione ottica (EN)" = "Aluminium reflector"

6.Duplicare tutti i codici:
-8° carattere --> 2
-il campo "Colore" = "Nero"

7.Duplicare tutti i codici con 5° carattere = "7" :
   -5° carattere --> "X"
   -il campo "Temp. di colore" --> "3300"
   -il campo "CRI" --> "95"
   -il campo "Sottocategoria" --> "Vivid"
   -il campo "Prezzo" = 211
   -il campo "Flusso lum. nom.":
     1185 --> 1000

8.Duplicare tutti i codici con 5° carattere = "X" :
   -5° carattere --> "D"
   -il campo "Temp. di colore" --> "3500"
   -il campo "Flusso lum. nom.":
     1000 --> 1015

9.Duplicare tutti i codici con 5° carattere = "7" :
   -5° carattere --> "SS"
   -il campo "CRI" --> "97.6"
   -il campo "Sottocategoria" --> "Sunlike"
   -il campo "Prezzo" = 234
   -il campo "Potenza" = 9
   -il campo "Flusso lum. nom.":
     1185 --> 900


 */