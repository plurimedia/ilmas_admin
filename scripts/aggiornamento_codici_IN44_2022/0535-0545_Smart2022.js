var process = require('./massive.js');
var modello = ['6321e0de5f7cb5535afac149', '6321e0ff5f7cb5535afac186'];
var query = { modello: { $in: modello } };
var tag = 'dani_20221028_01';

var transform1 = {
    idx: 1,
    tag: tag,
    query: query,
    preProcess: 'backupProdLocal',//'getFromProd',
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['Z'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p.codice = process.replaceCharAt(p.codice, '7', 5)

                p.k = '3000'

                switch (p.lm) {
                    case '1540':
                        p.lm = '1620'
                        break
                }

                return p
            }
        }
    ]
};

var transform2 = {
    idx: 2,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['Z'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p.codice = process.replaceCharAt(p.codice, '8', 5)

                p.k = '4000'

                switch (p.lm) {
                    case '1540':
                        p.lm = '1700'
                        break
                }

                return p
            }
        }
    ]
};

var transform3 = {
    idx: 3,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['Z'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.vivid(p)

                if (p.codice.length === 8) p.prezzo += 10
                else if (p.codice.length === 9 && (process.hasCharAt(p.codice, 'D', 9) || process.hasCharAt(p.codice, 'L', 9))) p.prezzo += 10

                switch (p.lm) {
                    case '1540':
                        p.lm = '1460'
                        break
                }

                return p
            }
        }
    ]
};

var transform4 = {
    idx: 4,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['X'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p.codice = process.replaceCharAt(p.codice, 'D', 5)

                p.k = '3500'

                switch (p.lm) {
                    case '1460':
                        p.lm = '1500'
                        break
                }

                return p;
            }
        }
    ]
};

var transform5 = {
    idx: 5,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['Z'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.sunlike(p)
                p.k = '3000'

                if (p.codice.length === 9 && p.sottocategoria === 'Sunlike') {
                    console.log('cambio prezzo 1', p.codice)
                    p.prezzo += 30
                }/*  else if (p.codice.length === 10 && (process.hasCharAt(p.codice, 'D', 9) || process.hasCharAt(p.codice, 'L', 9))) {
                    console.log('cambio prezzo 2', p.codice)
                    p.prezzo += 30
                } */
                if (process.hasCharsAt(p.codice, '0545', 1) && 
                    process.hasCharAt(p.codice, 'S', 6) && 
                    process.hasCharAt(p.codice, 'B', 7) &&
                    (process.hasCharAt(p.codice, 'D', 10) || process.hasCharAt(p.codice, 'L', 10)))
                    p.prezzo = 182

                if (process.hasCharsAt(p.codice, '0535', 1) && 
                    process.hasCharAt(p.codice, 'S', 6) && 
                    process.hasCharAt(p.codice, 'B', 7) &&
                    (process.hasCharAt(p.codice, 'D', 10) || process.hasCharAt(p.codice, 'L', 10)))
                    p.prezzo = 162

                switch (p.lm) {
                    case '1540':
                        p.lm = '1200'
                        break
                }

                return p
            }
        }
    ]
};

var transform6 = {
    idx: 6,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['1'], pos: 8 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.nero(p)

                return p
            }
        }
    ]
};

var transform7 = {
    idx: 7,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['1'], pos: 9 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.nero(p, 9)

                return p
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function (err) { console.log("finito!!"); })
    .catch(function (err) { console.log("fallito!!", err); });


/*
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto0535.json --query='{"modello":{"$oid":"6321e0de5f7cb5535afac149"}}'
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto0545.json --query='{"modello":{"$oid":"6321e0ff5f7cb5535afac186"}}'

db.Prodotto.remove({modello:{$in:[ObjectId('6321e0de5f7cb5535afac149'),ObjectId('6321e0ff5f7cb5535afac186')]}})

mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto0535.json
mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto0545.json


Per il modello "0535 Smart/P XS"-"0545 Smart/S XS":

1.Duplicare i codici con 5° carattere = Z:
-il 5° carattere = Z --> 7
-il campo "Temp. di colore" --> "3000"
-il campo "Flusso lum. nom":
-1540 --> 1620

2.Duplicare i codici con 5° carattere = Z:
-il 5° carattere = Z --> 8
-il campo "Temp. di colore" --> "4000"
-il campo "Flusso lum. nom":
-1540 --> 1700



3.Duplicare tutti i codici con 5° carattere = "Z" :
   -5° carattere --> "X"
   -il campo "Temp. di colore" --> "3300"
   -il campo "CRI" --> "95"
   -il campo "Sottocategoria" --> "Vivid"
   -If codice lunga massima 8 caratteri --> il campo "Prezzo" = +10€
   -If codice lunga massima 9 caratteri and il 9° carattere = D or L--> il campo "Prezzo" = +10€
   -il campo "Flusso lum. nom.":
     1540 --> 1460



4.Duplicare tutti i codici con 5° carattere = "X" :
   -5° carattere --> "D"
   -il campo "Temp. di colore" --> "3500"
   -il campo "Flusso lum. nom.":
     1460 --> 1500

5.Duplicare tutti i codici con 5° carattere = "Z" :
   -5° carattere --> "SS"
   -il campo "CRI" --> "97.6"
   -il campo "Sottocategoria" --> "Sunlike"
   -If codice lunga massima 9 caratteri and "Sottocategoria" = "Sunlike"--> il campo "Prezzo" = +30€
   -If codice lunga massima 10 caratteri and il 10° carattere = D or L--> il campo "Prezzo" = +30€
   -il campo "Flusso lum. nom.":
    1540 --> 1200

6.Duplicare tutti i codici:
-8° carattere = 1 --> 2
-il campo "Colore" = "Nero"



N.B. --> +_€ = AGGIUNGERE


 */