var process = require('./massive.js')
var modello = ['6357b012fbc5015ed835d90a', '6357a71ffbc5015ed83415c4']
var query = { modello: { $in: modello } }
var tag = 'dani_20221026_01'

var transform1 = {
    idx: 1,
    tag: tag,
    query: query,
    preProcess: 'backupProdLocal',//'getFromProd',
    filter: [process.filters.hasCharAt, process.filters.hasCharAt],
    filterArguments: [{ chars: ['Z'], pos: 5 }, { chars: ['B'], pos: 6 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p.codice = process.replaceCharAt(p.codice, 'C', 6)
                p.w = '18' // potenza
                p.ma = '500' // corrente

                switch (p.lm) {
                    case '1540':
                        p.lm = '2125'
                        break
                }

                return p;
            }
        }
    ]
};

var transform2 = {
    idx: 2,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['Z'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p.codice = process.replaceCharAt(p.codice, '7', 5)

                p.k = '3000' // temp colore

                switch (p.lm) {
                    case '1540':
                        p.lm = '1620'
                        break
                    case '2125':
                        p.lm = '2235'
                        break
                }

                return p
            }
        }
    ]
};

var transform3 = {
    idx: 3,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['Z'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, '8', 5)

                p.k = '4000' // temp colore

                switch (p.lm) {
                    case '1540':
                        p.lm = '1700'
                        break
                    case '2125':
                        p.lm = '2350'
                        break
                }

                return p;
            }
        }
    ]
};

var transform4 = {
    idx: 4,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['Z'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.vivid(p)
                p.w = '15'

                if (process.hasCharAt(p.codice, 'C', 6)) {
                    p.w = '15'
                    p.ma = '450'
                }

                p.prezzo += 10

                switch (p.lm) {
                    case '1540':
                        p.lm = '1460'
                        break
                    case '2125':
                        p.lm = '1850'
                        break
                }

                return p;
            }
        }
    ]
};

var transform5 = {
    idx: 5,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['X'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 5)
                p.k = '3500'

                switch (p.lm) {
                    case '1460':
                        p.lm = '1500'
                        break
                    case '1850':
                        p.lm = '1880'
                        break
                }

                return p;
            }
        }
    ]
};

var transform6 = {
    idx: 6,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['7'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.sunlike(p)

                if (process.hasCharAt(p.codice, 'C', 6)) {
                    p.w = '15'
                    p.ma = '450'
                }

                if (p.codice.indexOf('2049') === 0) {
                    p.prezzo = p.prezzo + 40
                }
                if (p.codice.indexOf('2080') === 0) {
                    p.prezzo = p.prezzo + 30
                }

                switch (p.lm) {
                    case '1620':
                        p.lm = '1200'
                        break
                    case '2235':
                        p.lm = '1500'
                        break
                }

                return p
            }
        }
    ]
};

var transform7 = {
    idx: 7,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['1'], pos: 8 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.nero(p, 8)

                return p
            }
        }
    ]
};

var transform8 = {
    idx: 8,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt, process.filters.hasSottoCategoria],
    filterArguments: [{ chars: ['1'], pos: 9 }, { sottocategoria: 'Sunlike' }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.nero(p, 9)

                return p
            }
        }
    ]
};

var transform9 = {
    idx: 9,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt, process.filters.hasCharAt],
    filterArguments: [{ chars: ['X', 'D'], pos: 5 }, { chars: ['B'], pos: 6 }],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.w = '12'

                return p
            }
        }
    ]
};

var transform10 = {
    idx: 10,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt, process.filters.hasCharAt],
    filterArguments: [{ chars: ['S'], pos: 5 }, { chars: ['C'], pos: 7 }],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.w = '15'
                p.ma = '450'

                return p
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function (err) { return process.execute(transform8) })
    .then(function (err) { return process.execute(transform9) })
    .then(function (err) { return process.execute(transform10) })
    .then(function (err) { console.log("finito!!"); })
    .catch(function () { console.log("fallito!!"); });


/*
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto2080.json --query='{"modello":{"$oid":"6357b012fbc5015ed835d90a"}}'
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto2049.json --query='{"modello":{"$oid":"6357a71ffbc5015ed83415c4"}}'

db.Prodotto.remove({modello:{$in:[ObjectId('6357b012fbc5015ed835d90a'),ObjectId('6357a71ffbc5015ed83415c4')]}})
mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto2080.json
mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto2049.json

 Per il modello "2049 Quad Maxi 48" - "2080 Quad Maxi"

1.Duplicare i codici con 5° carattere = Z:
-il 6° carattete = B --> C
-il campo "potenza" --> 18
-il campo "corrente" --> 500
-il campo "Flusso lum. nom":
1540 --> 2125


2.Duplicare i codici con 5° carattere = Z:
-il 5° carattere = Z --> 7
-il campo "Temp. di colore" --> "3000"
-il campo "Flusso lum. nom":
1540 --> 1620
2125 --> 2235

3.Duplicare i codici con 5° carattere = Z:
-il 5° carattere = Z --> 8
-il campo "Temp. di colore" --> "4000"
-il campo "Flusso lum. nom":
1540 --> 1700
2125 --> 2350

3.Duplicare tutti i codici con 5° carattere = "Z" :
   -5° carattere --> "X"
   -il campo "Temp. di colore" --> "3300"
   -il campo "CRI" --> "95"
   -il campo "potenza" --> 15
   -If 6° carattere = C --> il campo "potenza" = 15 and campo "corrente" = 450
   -il campo "Sottocategoria" --> "Vivid"
   -If codice lunga massima 9 caratteri and il 9° carattere = E --> il campo "Prezzo" = +10€
   -If codice lunga massima 9 caratteri and il 9° carattere = D or L--> il campo "Prezzo" = +10€
   -If codice lunga massima 8 caratteri --> il campo "Prezzo" = +10€
  -il campo "Prezzo" = +10€
   -il campo "Flusso lum. nom.":
     1540 --> 1460
     2125 --> 1850


4.Duplicare tutti i codici con 5° carattere = "X" :
   -5° carattere --> "D"
   -il campo "Temp. di colore" --> "3500"
   -il campo "Flusso lum. nom.":
    1460 --> 1500
    1850 --> 1880

5.Duplicare tutti i codici con 5° carattere = "7" :
   -5° carattere --> "SS"
   -il campo "CRI" --> "97.6"
   -il campo "Sottocategoria" --> "Sunlike"
  -If 6° carattere = C --> il campo "potenza" = 15 and campo "corrente" = 450
   -If primi 4 caratteri = 2049 --> il campo "Prezzo" = +40€
   -If primi 4 caratteri = 2080 --> il campo "Prezzo" = +30€
   -If codice lunga massima 10 caratteri and "Sottocategoria" = "Sunlike" and and il 10° carattere = E--> il campo        "Prezzo" = +40€
   -If codice lunga massima 10 caratteri and il 10° carattere = D or L--> il campo "Prezzo" = +40€
   -If codice lunga massima 9 caratteri -->  il campo "Prezzo" = +30€
   -If codice lunga massima 10 caratteri and il 10° carattere = D or L and i primi 4 caratteri = 2080 --> il campo     "Prezzo" = +30€
   -il campo "Flusso lum. nom.":
    1620 --> 1200
    2235 --> 1500

6.Duplicare tutti i codici:
-8° carattere = 1 --> 2
-if sottocategoria "Sunlike" and 9° carattere = 1 --> 2
-il campo "Colore" = "Nero"



N.B. --> +_€ = AGGIUNGERE



 */