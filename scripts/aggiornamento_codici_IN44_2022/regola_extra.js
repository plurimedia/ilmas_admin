var process = require('./massive.js');
var modello = ['6319d22f5f7cb5535a98b321', '6319d2395f7cb5535a98b328'];
var query = { modello: { $in: modello } };
var tag = 'dani_20221214_01';

/* var transform1 = {
  idx: 1,
  tag: tag,
  preProcess: 'backupProdLocal',//'getFromProd',
  query: query,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['2361'], pos: 1 },
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              var upd = {};
              upd.codice = p.codice.replace('2361', '2631')

              return upd;
          }
      }
  ]
};

var transform2 = {
  idx: 2,
  tag: tag,
  query: query,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['2362'], pos: 1 },
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              var upd = {};
              upd.codice = p.codice.replace('2362', '2632')

              return upd;
          }
      }
  ]
}; */

var transform1 = {
    idx: 1,
    tag: tag,
    // preProcess: 'restoreProdLocal',//'getFromProd',
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['2631'], pos: 1 },
    transform: [
        {
            action: 'genera_keywords'
        }
    ]
  };
  var transform2 = {
    idx: 2,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['2632'], pos: 1 },
    transform: [
        {
            action: 'genera_keywords'
        }
    ]
  };
  
process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { console.log("finito!!"); })
    .catch(function () { console.log("fallito!!"); });


/* var CONFIG = require(process.cwd() + '/server/config.js'),
mongoose = require('mongoose'),
_ = require('underscore'),
CategoriaSchema = require(process.cwd() + '/server/models/categoria'),
ModelloSchema = require(process.cwd() + '/server/models/modello'),
ProdottoSchema = require(process.cwd() + '/server/models/prodotto'),
Prodotto = mongoose.model('Prodotto'),
Modello = mongoose.model('Modello'),
Categoria = mongoose.model('Categoria'),
path = require('path'),
ObjectId = require('mongodb').ObjectID

const mongoOptions = {
    sslValidate: true,
    tls: true,
    tlsCAFile: path.join(__dirname, "../../ibm_mongodb_ca.pem"),
    useUnifiedTopology: true,
    useNewUrlParser: true
}

const findDoppio = (prodotto, codice) => {
    return new Promise((resolve, reject) => {
        Prodotto.find({ codice: codice }).lean().exec((err, codiceDoppio) => {
            if (codiceDoppio[0]) {
                console.log('DOPPIO ' + codiceDoppio[0].codice)
            }
            resolve()
        })
    })
}
const init = async () => {
    console.log('parto')
    try {
        Prodotto.find({ modello: ObjectId('6319d22f5f7cb5535a98b321') }).lean().exec(async (err, result) => {
            console.log(result.length)
            for (const prodotto of result) {
                // console.log(prodotto.codice)
                if (prodotto.codice.indexOf('2361') === 0) {
                    console.log('devo cercare ' + prodotto.codice.replace('2361', '2631'))
                    await findDoppio(prodotto, prodotto.codice.replace('2361', '2631'))
                }
            }
        })

        Prodotto.find({ modello: ObjectId('6319d2395f7cb5535a98b328') }).lean().exec(async (err, result) => {
            console.log(result.length)
            for (const prodotto of result) {
                // console.log(prodotto.codice)
                if (prodotto.codice.indexOf('2362') === 0) {
                    console.log('devo cercare ' + prodotto.codice.replace('2362', '2632'))
                    await findDoppio(prodotto, prodotto.codice.replace('2362', '2632'))
                }
            }
        })
    } catch (e) {
        console.error(e)
    }
}

mongoose.connect(CONFIG.MONGO, mongoOptions, function (err) {
    if (err)
        throw err;

    init()
}) */

