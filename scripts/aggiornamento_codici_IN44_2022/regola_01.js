const process = require('./massive.js');

/* const filtro = ['2001 KS Track', '2002 KL Track', '2029 KXL Track', '2025 KS/A Track', '2006 KL/A Track', '2031 KXL/A Track', '2240 Smart Track XS', '1240 Over Led Ps Mini', '1241 Over Led Ps Midi', '1242 Over Led Ps Maxi', '1244 Over Led Maxi F112', '1330 Fly Mini', '1331 Fly Midi', '1332 Fly Maxi', '1334 Fly Maxi F112', '1333 FLY MINI-A', '1335 Fly Midi/A', '1336 Fly Maxi/ A', '1321 Viper Midi', '1322 Viper Maxi', '1298 Cove Pro F112', '0830 Jet M', '1233 Sun/Track L', '0595 Smart XS', '0596 Smart S', '0597 Smart M', '0598 Smart L', '0599 SMART XL', '0796 Hammer Mini 120', '0797 Hammer Midi 160', '0798 Hammer Maxi 205', '0799 Hammer Extra 230', '0585 Smart Q XS', '0786 Hammer Q Mini 120', '0787 Hammer Q Midi 160', '0788 Hammer Q Maxi 205', '0789 Hammer Q Ex Plus 230', '2011 KS/1',
  '2026 KL/1', '0608 Bob Std Led', '1708 Bob Ext Led', '1608 Bob Std Sc Led', '1738 Bob Ext Sc Led', '0908 Bob Slim',
  '1712 Bob Eco Led', '1888 Puzzle Led', '1704 Mini Bob', '0708 Smart C/S', '0621 Bob Std Tondo', '0526 Smart A/ S',
  '0527 Smart A/ M', '0528 Smart A/L', '0670 Astuto Led Mini', '0671 Astuto Led Midi', '0672 Astuto Led Maxi', '0660 Dino Led Mini', '0661 Dino Led Midi', '0662 Dino Led Maxi', '0730 Berin Led Mini', '0731 Berin Led Midi', '0732 Berin Led Maxi', '0620 Bob Eco Tondo', '2916 Cristal', '2918 Cristal 2', '2917 Cristal Q', '2003 KS/P', '2004 KL/P',
  '2022 KS1/P', '2017 KS/Plus P', '2020 KL/Plus P', '4898 Spring Maxi 205', '4899 Spring Extra 230', '4531 Orion',
  '4621 Fold Led', '4711 Tech Led', '1060 Dial 1 Led', '0775 Nuvola Led', '4251 Magic 2 Wall S', '2018 KS/Plus S',
  '2021 KL/Plus S', '4996 Summer Mini 120', '4997 Summer Midi 160', '4998 Summer Maxi 205', '4999 Summer Extra 230',
  'F111 Led Module', 'F112 Led Module', '2150 Smart E / M', '0585 Smart Q XS', '0518 SMART B/L', '0517 Smart B/M',
  '0516 Smart B/ S', '0589 Smart Q XL', '0587 Smart Q M', '0588 Smart Q L', '0586 Smart Q S'] */

const filtro1 = ['2001 KS Track', '2002 KL Track', '2029 KXL Track', '2025 KS/A Track', '2006 KL/A Track', '2031 KXL/A Track']
const filtro2 = ['2240 Smart Track XS', '1240 Over Led Ps Mini', '1241 Over Led Ps Midi', '1242 Over Led Ps Maxi', '1244 Over Led Maxi F112']
const filtro3 = ['1330 Fly Mini', '1331 Fly Midi', '1332 Fly Maxi', '1334 Fly Maxi F112', '1333 FLY MINI-A', '1335 Fly Midi/A', '1336 Fly Maxi/ A']
const filtro4 = ['1321 Viper Midi', '1322 Viper Maxi', '1298 Cove Pro F112', '0830 Jet M', '1233 Sun/Track L', '0595 Smart XS', '0596 Smart S']
const filtro5 = ['0597 Smart M', '0598 Smart L', '0599 SMART XL', '0796 Hammer Mini 120', '0797 Hammer Midi 160', '0798 Hammer Maxi 205']
const filtro6 = ['0799 Hammer Extra 230', '0585 Smart Q XS', '0786 Hammer Q Mini 120', '0787 Hammer Q Midi 160', '0788 Hammer Q Maxi 205']
const filtro7 = ['0789 Hammer Q Ex Plus 230', '2011 KS/1','2026 KL/1', '0608 Bob Std Led', '1708 Bob Ext Led', '1608 Bob Std Sc Led', '1738 Bob Ext Sc Led']
const filtro8 = ['0908 Bob Slim','1712 Bob Eco Led', '1888 Puzzle Led', '1704 Mini Bob', '0708 Smart C/S', '0621 Bob Std Tondo', '0526 Smart A/ S']
const filtro9 = ['0527 Smart A/ M', '0528 Smart A/L', '0670 Astuto Led Mini', '0671 Astuto Led Midi', '0672 Astuto Led Maxi']
const filtro10 = ['0660 Dino Led Mini', '0661 Dino Led Midi', '0662 Dino Led Maxi', '0730 Berin Led Mini', '0731 Berin Led Midi', '0732 Berin Led Maxi']
const filtro11 = ['0620 Bob Eco Tondo', '2916 Cristal', '2918 Cristal 2', '2917 Cristal Q', '2003 KS/P', '2004 KL/P']
const filtro12 = ['2022 KS1/P', '2017 KS/Plus P', '2020 KL/Plus P', '4898 Spring Maxi 205', '4899 Spring Extra 230', '4531 Orion']
const filtro13 = ['4621 Fold Led', '4711 Tech Led', '1060 Dial 1 Led', '0775 Nuvola Led', '4251 Magic 2 Wall S', '2018 KS/Plus S']
const filtro14 = ['2021 KL/Plus S', '4996 Summer Mini 120', '4997 Summer Midi 160', '4998 Summer Maxi 205', '4999 Summer Extra 230']
const filtro15 = ['F111 Led Module', 'F112 Led Module', '2150 Smart E / M', '0585 Smart Q XS', '0518 SMART B/L', '0517 Smart B/M']
const filtro16 = ['0516 Smart B/ S', '0589 Smart Q XL', '0587 Smart Q M', '0588 Smart Q L', '0586 Smart Q S']
const filtro = filtro1

var query = { modello: { $in: [] } };
console.log(query)
var tag = 'lorenzo_2022_regola01';

const filtro_2125lm = ['2240', '2025', '2011', '2003', '2022', '2001']
const filtro_1460lm = ['1704', '0708', '0526', '0527', '0670', '0660', '2150', '0517', '0518', '0587', '0586']
const filtro_1610lm = ['0526', '0527', '0670', '0660', '2150', '2241', '1240', '1333', '0830', '1704', '0708', '0718', '0516', '0517', '0830', '0596', '0597', '0586', '0587', '1240', '1330']
const filtro_2020lm = ['1704', '0708', '0526', '0527', '0528', '0670', '2150', '0518', '0517', '0516', '0587', '0588', '0586']
const filtro_2235lm = ['2240', '2025', '2011', '2003', '2022', '2001', '0830', '0596', '0597', '0598', '1704', '0708', '0526', '0527', '0528', '0670', '2150', '0518', '0517', '0516', '0587', '0588', '0586']
const filtro_1790lm = ['2011', '2003', '2022']
const filtro_1850lm = ['2003', '2022']
const filtro_1935lm = ['1240', '1330', '1333', '1704', '0670']
const filtro_2030lm = ['0608', '1708', '1608', '1738', '0908', '1712', '1888', '1704', '0621', '0620', '2916', '2917', '4531', '4621', '4711', '1060', '0775']

//regole: [1.1, 1.2, 1.3]
var transform1 = {
  idx: 1,
  tag: tag,
  // query: query,
  modelli: filtro,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['Z', '7', '8', 'O', 'S', 'T'], pos: 5 }, //pos in base 1
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {

        var upd = {};

        switch (p.lm) {
          case '1350': upd.lm = '1540';
            break;
          case '1415': upd.lm = '1620';
            break;
          case '1495': upd.lm = '1700';
            break;
          case '1790': {
            if (filtro_1790lm.includes(p.codice.substring(0, 4))) {
              upd.lm = '2125'
            } else {
              upd.lm = '2195'
            }
          }
            break;
          case '1880': upd.lm = '2235';
            break;
          case '1990': upd.lm = '2350';
            break;
          case '2325': upd.lm = '2390';
            break;
          case '2490': upd.lm = '2510';
            break;
          case '3085': upd.lm = '3110';
            break;
          case '3480': upd.lm = '3515';
            break;
          case '3860': upd.lm = '3910';
            break;
          case '3160': upd.lm = '3270';
            break;
          case '3565': upd.lm = '3695';
            break;
          case '3955': upd.lm = '4110';
            break;
          case '3390': upd.lm = '3440';
            break;
          case '3820': upd.lm = '3890';
            break;
          case '4240': upd.lm = '4320';
            break;
          case '1460': if (filtro_1460lm.includes(p.codice.substring(0, 4))) { upd.lm = '1615' } else { upd.lm = '1540'; }
            break;
          case '1610': if (filtro_1610lm.includes(p.codice.substring(0, 4))) { upd.lm = '1785' } else { upd.lm = '1700'; }
            break;
          case '2020': if (filtro_2020lm.includes(p.codice.substring(0, 4))) { upd.lm = '2270' } else { upd.lm = '2125'; }
            break;
          case '1625': upd.lm = '1615';
            break;
          case '1665': upd.lm = '1700';
            break;
          case '2680': upd.lm = '2695';
            break;
          case '2750': upd.lm = '2835';
            break;
          case '2950': upd.lm = '2980';
            break;
          case '2375': upd.lm = '2695';
            break;
          case '2500': upd.lm = '2835';
            break;
          case '2626': upd.lm = '2980';
            break;
          case '2860': upd.lm = '3270';
            break;
          case '3000': upd.lm = '3440';
            break;
          case '2710': upd.lm = '3110';
            break;
          case '3345': upd.lm = '3910';
            break;
          case '3515': upd.lm = '4110';
            break;
          case '3695': upd.lm = '4320';
            break;
          case '4410': upd.lm = '4480';
            break;
          case '4520': upd.lm = '4715';
            break;
          case '4840': upd.lm = '4955';
            break;
          case '2125': if (filtro_2125lm.includes(p.codice.substring(0, 4))) { upd.lm = '2390' } else { upd.lm = '2235'; }
            break;
          case '1535': upd.lm = '1700';
            break;
          case '2235': if (filtro_2235lm.includes(p.codice.substring(0, 4))) { upd.lm = '2510'; } else { upd.lm = '2350'; }
            break;
          case '3035': upd.lm = '3515';
            break;
          case '3195': upd.lm = '3695';
            break;
          case '3360': upd.lm = '3890';
            break;
          case '1660': upd.lm = '1620';
            break;
          case '1745': upd.lm = '1700';
            break;
          case '1010': upd.lm = '1125';
            break;
          case '1060': upd.lm = '1185';
            break;
          case '1120': upd.lm = '1245';
            break;
          case '1580': upd.lm = '1540';
            break;
          case '2626': upd.lm = '2980';
            break;
        }

        upd.generazione = 'D3'

        /* if (p.codice.charAt(4) === 'R' || '5' || '6' || 'I' || 'A' || 'B') {

          upd.exportMetel = false;
          upd.pubblicato = false;

        } */

        return upd;
      }
    }
  ]
};

var transform2 = {
  idx: 2,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['R', '5', '6', 'I', 'A', 'B'], pos: 5 },
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              var upd = {};
              upd.pubblicato = false
              upd.exportMetel = false

              return upd;
          }
      }
  ]
};

var transform3 = {
  idx: 3,
  tag: tag,
  query: query,
  filter: [process.filters.hasCharAt,process.filters.hasCharAt],
  filterArguments: [{ chars: ['0670'], pos: 1 },{ chars: ['Y'], pos: 5 }],
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              var upd = {};
              upd.pubblicato = false
              upd.exportMetel = false
              upd.codice = p.codice + '-OLD'

              return upd;
          }
      }
  ]
};

//regola 1.4
var transform4 = {
  idx: 4,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['W', 'Y'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.vivid(p)
        p.generazione = 'D3'

        switch (p.lm) {
          case '1530':
            p.lm = '1460';
            break;
          case '1935': if (filtro_1935lm.includes(p.codice.substring(0, 4))) {
            p.lm = '2030';
            p.w = '17';
            p.ma = '500';
          } else {
            p.lm = '1850'
          } break;
          case '2140':
            p.lm = '2030';
            break;
          case '2910':
            p.lm = '2760';
            break;
          case '3280':
            p.lm = '3110';
            break;
          case '3640':
            p.lm = '3450';
            break;
          case '1450':
            p.lm = '1460';
            break;
          case '2530': if (p.codice.substring(0, 4) === '2918') p.lm = '2760'; else p.lm = '2400';
            break;
          case '1360':
            p.lm = '1460';
            break;
          case '4160':
            p.lm = '3950';
            break;
          case '965':
            p.lm = '1000';
            break;
        }

        return p;
      }
    }
  ]
};

//regola 1.5
var transform5 = {
  idx: 5,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasSottoCategoria, process.filters.hasCharAt],
  filterArguments: [{ sottocategoria: 'Vivid' }, { chars: ['X'], pos: 5 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'D', 5)
        p.k = '3500'
        p.generazione = 'D3'

        switch (p.lm) {
          case '1460':
            p.lm = '1500'
            break;
          case '1850': if (filtro_1850lm.includes(p.codice.substring(0, 4))) p.lm = '2235'; else p.lm = '1880';
            break;
          case '2030': if (filtro_2030lm.includes(p.codice.substring(0, 4))) p.lm = '2510'; else p.lm = '2085';
            break;
          case '2760':
            p.lm = '2835'
            break;
          case '3110':
            p.lm = '3195'
            break;
          case '3450':
            p.lm = '3540'
            break;
          case '2400':
            p.lm = '2470'
            break;
          case '3950':
            p.lm = '4040'
            break;
          case '1000':
            p.lm = '1015'
            break;
        }

        return p;
      }
    }
  ]
};

// regola 1.6
const codici_sunlike_univoci = []
const codici_generano_duplicati = []
var transform6 = {
  idx: 6,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['S', '7'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        const codice_old = p.codice
        p = process.sunlike(p)
        if (codici_sunlike_univoci.indexOf(p.codice) > -1) {
          codici_generano_duplicati.push(codice_old)
          p.codice = p.codice + '-AAA'
        } else {
          codici_sunlike_univoci.push(p.codice)
        }

        p.generazione = 'D3'

        switch (p.lm) {
          case '1620':
            p.lm = '1200';
            break;
          case '2235':
            p.lm = '1500';
            break;
          case '2390':
            p.lm = '1700';
            break;
          case '3270': if (process.hasCharsAt(p.codice, '0799', 1)) p.lm = '2500'; else p.lm = '2400';
            break;
          case '3695':
            p.lm = '2800';
            break;
          case '4110':
            p.lm = '3200';
            break;
          case '1700':
            p.lm = '1200';
            break;
          case '2835':
            p.lm = '2100';
            break;
          case '4715':
            p.lm = '3700';
            break;
          case '1185':
            p.lm = '900';
            break;
          case '2325':
            p.lm = '1700';
            break;


        }

        return p;
      }
    }
  ]
};

//regola 1.7
var transform7 = {
  idx: 7,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['W', 'Q'], pos: 5 },
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        p = process.archiviaConNote(p, 'IN40')

        return p
      }
    }
  ]
};


/* process.execute(transform1)
  .then(function (err) { return process.execute(transform2) })
  .then(function (err) { return process.execute(transform3) })
  .then(function (err) { return */ process.execute(transform4)/*  }) */
  .then(function (err) { return process.execute(transform5) })
  .then(function (err) { return process.execute(transform6) })
  /* .then(function (err) { return process.execute(transform7) }) */
  .then(function (err) { console.log('finito!!'); })
  .catch(function (err) { console.log('fallito!!', err); });

/*

Per i modelli:
'2001 KS Track', '2002 KL Track', '2029 KXL Track', '2025 KS/A Track', '2006 KL/A Track','2031 KXL/A Track', '2240 Smart Track XS', '1240 Over Led Ps Mini', '1241 Over Led Ps Midi', '1242 Over Led Ps Maxi', '1244 Over Led Maxi F112', '1330 Fly Mini', '1331 Fly Midi', '1332 Fly Maxi', '1334 Fly Maxi F112', '1333 FLY MINI-A', '1335 Fly Midi/A', '1336 Fly Maxi/ A', '1321 Viper Midi', '1322 Viper Maxi' , '1298 Cove Pro F112', '0830 Jet M', '1233 Sun/Track L', '0595 Smart XS', '0596 Smart S', '0597 Smart M', '0598 Smart L', '0599 SMART XL', '0796 Hammer Mini 120', '0797 Hammer Midi 160', '0798 Hammer Maxi 205', '0799 Hammer Extra 230', '0585 Smart Q XS', '0786 Hammer Q Mini 120','0787 Hammer Q Midi 160', '0788 Hammer Q Maxi 205', '0789 Hammer Q Ex Plus 230', '2011 KS/1',
'2026 KL/1', '0608 Bob Std Led', '1708 Bob Ext Led', '1608 Bob Std Sc Led', '1738 Bob Ext Sc Led', '0908 Bob Slim',
'1712 Bob Eco Led', '1888 Puzzle Led', '1704 Mini Bob', '0708 Smart C/S', '0621 Bob Std Tondo', '0526 Smart A/ S', 
'0527 Smart A/ M', '0528 Smart A/L', '0670 Astuto Led Mini', '0671 Astuto Led Midi', '0672 Astuto Led Maxi', '0660 Dino Led Mini', '0661 Dino Led Midi', '0662 Dino Led Maxi', '0730 Berin Led Mini', '0731 Berin Led Midi', '0732 Berin Led Maxi', '0620 Bob Eco Tondo', '2916 Cristal', '2918 Cristal 2', '2917 Cristal Q','2003 KS/P', '2004 KL/P',
'2022 KS1/P', '2017 KS/Plus P', '2020 KL/Plus P', '4898 Spring Maxi 205', '4899 Spring Extra 230', '4531 Orion',
'4621 Fold Led', '4711 Tech Led', '1060 Dial 1 Led','0775 Nuvola Led', '4251 Magic 2 Wall S', '2018 KS/Plus S',
'2021 KL/Plus S','4996 Summer Mini 120', '4997 Summer Midi 160', '4998 Summer Maxi 205', '4999 Summer Extra 230',
'F111 Led Module', 'F112 Led Module', '2150 Smart E / M', '0585 Smart Q XS', '0518 SMART B/L', '0517 Smart B/M',
'0516 Smart B/ S', '0589 Smart Q XL','0587 Smart Q M','0588 Smart Q L', '0586 Smart Q S'


1. Tutti i codici con 5° carattere = 'Z/7/8/O/S/T' cambio lumen del campo 'Flusso lum. nom.':

1350 --> 1540
1415 --> 1620
1495 --> 1700
1790 --> 2195
1880 --> 2235
1990 --> 2350
2325 --> 2390
2490 --> 2510
3085 --> 3110
3480 --> 3515
3860 --> 3910
3160 --> 3270
3565 --> 3695
3955 --> 4110
3390 --> 3440
3820 --> 3890
4240 --> 4320
1460 --> 1540
1610 --> 1700 
2020 --> 2125
1625 --> 1615
1665 --> 1700
2680 --> 2695
2750 --> 2835
2950 --> 2980
2375 --> 2695
2500 --> 2835
2626 --> 2980
2860 --> 3270
3000 --> 3440
2710 --> 3110 
3345 --> 3910
3515 --> 4110
3695 --> 4320
4410 --> 4480
4520 --> 4715
4840 --> 4955
2125 --> 2390
1535 --> 1700
2235 --> 2350 
3035 --> 3515
3195 --> 3695
3360 --> 3890
1660 --> 1620
1745 --> 1700
1010 --> 1125
1060 --> 1185
1120 --> 1245
1580 --> 1540
2626 --> 2980
Se campo 'Flusso lum. nom.' = 2125 and 4 primi caratteri = 2240/2025/2011/2003/2022/2001 --> il campo 'Flusso lum. nom.' --> 2235
Se campo 'Flusso lum. nom.' = 1460 and 4 primi caratteri = 1704/0708/0526/0527/0670/0660/2150/0517/0518/0587/0586--> il campo 'Flusso lum. nom.' --> 1615    
Se campo 'Flusso lum. nom.' = 1610 and 4 primi caratteri = 0526/0527/0670/0660/2150/2241/1240/1333/0830/1704/0708/0718/0516/0517/0830/0596/0597/0586/0587/1240/1330--> il campo 'Flusso lum. nom.' --> 1785 
Se campo 'Flusso lum. nom.' = 2020 and 4 primi caratteri = 1704/0708/0526/0527/0528/0670/2150/0518/0517/0516/0587/0588/0586 --> il campo 'Flusso lum. nom.' --> 2270    
Se campo 'Flusso lum. nom.' = 2235 and 4 primi caratteri = 2240/2025/2011/2003/2022/2001/0830/0596/0597/0598/1704/0708/0526/0527/0528/0670/2150/0518/0517/0516/0587/0588/0586 --> il campo 'Flusso lum. nom.' --> 2510
Se campo 'Flusso lum. nom.' = 1790 and 4 primi caratteri = 2011/2003/2022 --> il campo 'Flusso lum. nom.' --> 2125


2. Per tutti i codici modificati --> Campo 'Generazione' = 'D3' (Da creare nel menu a tendina)

3. Tutti i codici con il 5° carattere = 'R/5/6/I/A/B' --> flag metel = false e flag sito = false.

4. Duplicare tutti i codici con 5° carattere = 'W/Y' :
   -5° carattere --> 'X'
   -il campo 'Temp. di colore' --> '3300'
   -il campo 'CRI' --> '95'
   -il campo 'Sottocategoria' --> 'Vivid'
   -il campo 'Flusso lum. nom.': 
      1530 --> 1460
      1935 --> 1850
      2140 --> 2030
      2910 --> 2760
      3280 --> 3110
      3640 --> 3450
      1450 --> 1460 
      2530 --> 2400
      1360 --> 1460 
      4160 --> 3950
      965 --> 1000
      Se campo 'Flusso lum. nom.' = 1935 and 4 primi caratteri = 1240/1330/1333/1704/0670 --> campo 'Flusso                lum.nom.' --> 2030 and il campo 'potenza' --> 17 and il campo corrente --> 500
      Se campo 'Flusso lum. nom.' = 2530 and 4 primi caratteri = 2918 --> campo 'Flusso lum.nom.' --> 2760

5. Duplicare tutti i codici creati al punto 4:
   -5° carattere --> 'D'
   -il campo 'Temp. di colore' --> '3500'
   -il campo 'Flusso lum. nom.': 
      1460 --> 1500
      1850 --> 1880
      2030 --> 2085
      2760 --> 2835
      3110 --> 3195
      3450 --> 3540
      2400 --> 2470
      3950 --> 4040
      1000 --> 1015
      Se campo 'Flusso lum. nom.' = 2030 and 4 primi caratteri =       0608/1708/1608/1738/0908/1712/1888/1704/0621/0620/2916/2917/4531/4621/4711/1060/0775--> campo 'Flusso lum.            nom.' --> 2510
      Se campo 'Flusso lum. nom.' = 1850 and 4 primi caratteri = 2003/2022--> campo 'Flusso lum. nom.' --> 2235
      
      



6. Duplicare tutti i codici con 5° carattere = '7/S':
   -5° carattere '7/S' --> 'SS'
   -il campo 'CRI' --> '97,6'
   -il campo 'Sottocategoria' --> 'Sunlike'
   -il campo 'Flusso lum. nom.': 
      1620 --> 1200
      2235 --> 1500
      2390 --> 1700
      3270 --> 2400
      3695 --> 2800
      4110 --> 3200
      1700 --> 1200
      2835 --> 2100
      4715 --> 3700
      1185 --> 900
      2325 --> 1700
      Se campo 'Flusso lum. nom.' = 3270 and 4 primi caratteri = 0799 --> campo 'Flusso lum. nom.' --> 2500    




7. Tutti i codici con 5° carattere = 'W/Q':
   mettere in archivio --> 'Generazione' = 'IN40' (Da creare nel menu a tendina)
   flag metel = false e flag sito = false.





 */