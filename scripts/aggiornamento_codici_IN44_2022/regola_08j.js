var process = require('./massive.js');
var modello = ['56b8aaa90de23c95600bd2b3'];
var query = { modello : { $in: modello } };
var tag = 'dani_20221028_08';

var transform1 = {
  idx: 1,
  tag: tag,
  query: query,
  preProcess: 'backupTestLocal', // 'getFromProd',
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['D'], pos: 6 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'S', 6)
        p.luci = 3
        p.w = '28'
        p.ma = '800'

        switch (p.lm) {
          case '9330':
            p.lm = '10545';
            break;
          case '9810':
            p.lm = '11085';
            break;
          case '10320':
            p.lm = '11670';
            break;
          case '8280':
            p.lm = '9330';
            break;
          case '8505':
            p.lm = '9585';
            break;
          case '7200':
            p.lm = '8400';
            break;
        }

        return p
      }
    }
  ]
}

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .catch(function(err) { console.log("fallito!!", err); });