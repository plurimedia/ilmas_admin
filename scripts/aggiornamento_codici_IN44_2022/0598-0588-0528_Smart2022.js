var process = require('./massive.js');
var modello = ['59b00ad6d9bffb040079b531', '5b3b4829e2fa7104002f394b', '5b2b5cef694a5804009cae73'];
var query = { modello: { $in: modello } };
var tag = 'dani_20221027_04';

var transform1 = {
    idx: 1,
    tag: tag,
    query: query,
    preProcess: 'getFromProd',
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['Z', '7', '8'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p.generazione = 'D3'

                p.codice = process.ifCharAtReplaceCharAt(p.codice, 'Z', 'O', 5)
                p.codice = process.ifCharAtReplaceCharAt(p.codice, '7', 'S', 5)
                p.codice = process.ifCharAtReplaceCharAt(p.codice, '8', 'T', 5)

                /* if (process.hasCharAt(p.codice, 'Z', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'O', 5)
                }
                if (process.hasCharAt(p.codice, '7', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'S', 5)
                }
                if (process.hasCharAt(p.codice, '8', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'T', 5)
                } */

                // console.log(p.codice)
                if (process.hasCharsAt(p.codice, '0598', 1)) {
                    if (p.codice.length <= 8) p.prezzo = 117
                    if (process.hasCharAt(p.codice, 'E', 9)) p.prezzo = 139
                    if (process.hasCharAt(p.codice, 'D', 9) || process.hasCharAt(p.codice, 'L', 9)) p.prezzo = 175
                }

                if (process.hasCharsAt(p.codice, '0588', 1)) {
                    if (p.codice.length <= 8) p.prezzo = 135
                    if (process.hasCharAt(p.codice, 'E', 9)) p.prezzo = 158
                    if (process.hasCharAt(p.codice, 'D', 9) || process.hasCharAt(p.codice, 'L', 9)) p.prezzo = 194
                }

                if (process.hasCharsAt(p.codice, '0528', 1)) {
                    if (p.codice.length <= 8) p.prezzo = 125
                    if (process.hasCharAt(p.codice, 'E', 9)) p.prezzo = 147
                    if (process.hasCharAt(p.codice, 'D', 9) || process.hasCharAt(p.codice, 'L', 9)) p.prezzo = 182
                }
                // console.log(p.codice)

                switch (p.lm) {
                    case '2020':
                        p.lm = '2270'
                        break
                    case '2125':
                        p.lm = '2390'
                        break
                    case '2235':
                        p.lm = '2510'
                        break
                    case '2710':
                        p.lm = '3110'
                        break
                    case '2860':
                        p.lm = '3270'
                        break
                    case '3000':
                        p.lm = '3440'
                        break
                    case '3345':
                        p.lm = '3910'
                        break
                    case '3515':
                        p.lm = '4110'
                        break
                    case '3695':
                        p.lm = '4320'
                        break
                    case '3035':
                        p.lm = '3515'
                        break
                    case '3195':
                        p.lm = '3695'
                        break
                    case '3280':
                        p.lm = '3890'
                        break
                }

                return p;
            }
        }
    ]
};

var transform2 = {
    idx: 2,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['R', '5', '6', 'Z', '7', '8'], pos: 5 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var upd = {};
                upd.pubblicato = false
                upd.exportMetel = false

                return upd;
            }
        }
    ]
};

var transform3 = {
    idx: 3,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['W'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.vivid(p)
                p.generazione = 'D3'

                if (process.hasCharsAt(p.codice, '0598', 1)) {
                    p.prezzo -= 11
                }

                if (process.hasCharsAt(p.codice, '0588', 1) || process.hasCharsAt(p.codice, '0528', 1)) {
                    p.prezzo -= 21
                }

                switch (p.lm) {
                    case '2140': {
                        if (process.hasCharsAt(p.codice, '0528D', 1)) p.lm = '2760'; 
                        else if (process.hasCharsAt(p.codice, '0598E', 1) || process.hasCharsAt(p.codice, '0588E', 1)) p.lm = '3450'
                        else p.lm = '2030'
                        break 
                    }
                    case '2910':
                        p.lm = '2760'
                        break
                    case '3280':
                        if (p.codice.indexOf('0528') === 0) p.lm = '3110'; else p.lm = '3450'
                        break
                }

                return p;
            }
        }
    ]
};

var transform4 = {
    idx: 4,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasSottoCategoria, process.filters.hasCharAt],
    filterArguments: [{ sottocategoria: 'Vivid' }, { chars: ['X'], pos: 5 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 5)
                p.k = '3500'

                if (p.lm == '3450' && p.codice.indexOf('0528') === 0) {
                    p.lm = '3195'
                } else
                    switch (p.lm) {
                        case '2030':
                            p.lm = '2085'
                            break
                        case '2760':
                            p.lm = '2835'
                            break
                        case '3450':
                            p.lm = '3540'
                            break
                        case '3110':
                            p.lm = '3195'
                            break
                        }

                return p;
            }
        }
    ]
};

var transform5 = {
    idx: 5,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['S'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.sunlike(p)
                p.generazione = 'D3'
                p.prezzo += 33

                switch (p.lm) {
                    case '2390':
                        p.lm = '1700'
                        break
                    case '3270':
                        p.lm = '2400'
                        break
                    case '4110':
                        p.lm = '3200'
                        break
                    case '3695':
                        p.lm = '2800'
                        break
                }

                return p;
            }
        }
    ]
};

var transform6 = {
    idx: 6,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['W', 'Q'], pos: 5 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var upd = {}
                upd.generazione = 'IN40'
                upd.pubblicato = false
                upd.exportMetel = false

                return upd
            }
        }
    ]
};

var transform7 = {
    idx: 7,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['L'], pos: 9 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p = process.casambi(p, 9)
                p.prezzo += 53

                return p
            }
        }
    ]
};

var transform8 = {
    idx: 8,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['L'], pos: 10 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p = process.casambi(p, 10)
                p.prezzo += 53

                return p
            }
        }
    ]
};

var transform9 = {
    idx: 9,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['D'], pos: 9 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.alimentatore = 'Driver Dimmerabile Push N/A'
                p.dimmerabile = true

                return p
            }
        }
    ]
};

var transform10 = {
    idx: 10,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['D'], pos: 10 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.alimentatore = 'Driver Dimmerabile Push N/A'
                p.dimmerabile = true

                return p
            }
        }
    ]
};

var transform11 = {
    idx: 11,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt, process.filters.hasCharAt],
    filterArguments: [{ chars: ['1'], pos: [8, 9] }, { chars: ['0598'], pos: 1 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                if (process.hasCharAt(p.codice, '1', 8))
                    p = process.nero(p)
                else if (process.hasCharAt(p.codice, '1', 9))
                    p = process.nero(p, 9)

                return p
            }
        }
    ]
};

var transform12 = {
    idx: 12,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt, process.filters.hasCharAt],
    filterArguments: [{ chars: ['1'], pos: [8, 9] }, { chars: ['0588'], pos: 1 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                if (process.hasCharAt(p.codice, '1', 8))
                    p = process.nero(p)
                else if (process.hasCharAt(p.codice, '1', 9))
                    p = process.nero(p, 9)

                return p
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function (err) { return process.execute(transform8) })
    .then(function (err) { return process.execute(transform9) })
    .then(function (err) { return process.execute(transform10) })
    .then(function (err) { return process.execute(transform11) })
    .then(function (err) { return process.execute(transform12) })
    .then(function (err) { console.log("finito!!"); })
    .catch(function () { console.log("fallito!!"); });


/*
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto0528.json --query='{"modello":{"$oid":"5b2b5cef694a5804009cae73"}}'
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto0588.json --query='{"modello":{"$oid":"5b3b4829e2fa7104002f394b"}}'
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto0598.json --query='{"modello":{"$oid":"59b00ad6d9bffb040079b531"}}'

db.Prodotto.remove({modello:{$in:[ObjectId('5b2b5cef694a5804009cae73'),ObjectId('5b3b4829e2fa7104002f394b'),ObjectId('59b00ad6d9bffb040079b531')]}})

mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto0528.json
mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto0588.json
mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto0598.json

Per i modelli:
"0598 Smart L", "0588 Smart Q L", "0528 Smart A/L"

1. Duplicare tutti i codici con 5° carattere = "Z/7/8": 
-il campo "Flusso lum. nom.":
-2020 --> 2270
-2125 --> 2390
-2235 --> 2510
-2710 --> 3110
-2860 --> 3270
-3000 --> 3440
-3345 --> 3910
-3515 --> 4110
-3695 --> 4320
-3035 --> 3515
-3195 --> 3695
-3280 --> 3890



-il campo "Generazione" = "D3" (Da creare nel menu a tendina)
-il 5° carattere:
if = Z --> O
if = 7 --> S
if = 8 --> T

3. Tutti i codici con il 5° carattere = "R/5/6/Z/7/8" --> flag metel = false e flag sito = false.

4. Duplicare tutti i codici con 5° carattere = "W" :
   -5° carattere --> "X"
   -il campo "Temp. di colore" --> "3300"
   -il campo "CRI" --> "95"
   -il campo "Sottocategoria" --> "Vivid"
   -il campo "Flusso lum. nom.": 
   2140 --> 2030
   2910 --> 2760
   3280 --> 3450
   Se campo "Flusso Lum. Nom." = 3280 and 4 primi caratteri = 0528 --> 3110


   

5. Duplicare tutti i codici creati al punto 4:
   -5° carattere --> "D"
   -il campo "Temp. di colore" --> "3500"
   -il campo "Flusso lum. nom.": 
    2030 --> 2085
    2760 --> 2835
    3450 --> 3540
    Se campo "Flusso Lum. Nom." = 3450 and 4 primi caratteri = 0528 --> 3195



6. Duplicare tutti i codici con 5° carattere = "S":
   -5° carattere "S" --> "SS"
   -il campo "CRI" --> "97,6"
   -il campo "Sottocategoria" --> "Sunlike"
   -il campo "Flusso lum. nom.": 
     2390 --> 1700
     3270 --> 2400
     4110 --> 3200 
     3695 --> 2800
    


7. Tutti i codici con 5° carattere = "W/Q":
   mettere in archivio --> "Generazione" = "IN40" (Da creare nel menu a tendina)
   flag metel = false e flag sito = false.

8.Solo per i modelli:
"0598 Smart L", "0588 Smart Q L"


Duplicare tutti i codici con 8° carattere = "1"
-8° carattere --> "2"
-il campo "colore" --> "Nero"








 */