const process = require('./massive.js');

const filtro1 = ['2012 KS/2', '2027 KL/2']
const filtro2 = ['0609 Bob Std Led', '1709 Bob Ext Led'] // hanno girato sicuro
const filtro3 = ['1609 Bob Std Sc Led', '1739 Bob Ext Sc Led'] // hanno girato sicuro
const filtro4 = [/* '0909 Bob Slim',  */'1713 Bob Eco Led'] // hanno girato sicuro
const filtro5 = ['1706 Mini Bob', '0709 Smart C/S'] // hanno girato sicuro
const filtro6 = [/* '2023 KS2/P',  */'4532 Orion'] // hanno girato sicuro
const filtro7 = ['0776 Nuvola Led', '4712 Tech Led'] // hanno girato sicuro
const filtro8 = ['4622 Fold Led', '4252 Magic Wall S'] // hanno girato sicuro
const filtro9 = ['1002 Marquez Led', '1061 Dial 2 Led'] // hanno girato sicuro
const filtroPerTest = ['4532 Orion', '1713 Bob Eco Led']

const filtro = filtroPerTest
var query = { modello: { $in: [] } };
var tag = 'lorenzo_2022_regola02';

//regole: [2.1, 2.2, 2.3]
var transform1 = {
  idx: 1,
  tag: tag,
  // query: query,
  modelli: filtro,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['Z', '7', '8', 'O', 'S', 'T'], pos: 5 }, //pos in base 1
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {

        var upd = {};

        switch (p.lm) {
          case '2700': upd.lm = '3080';
            break;
          case '2830': upd.lm = '3240';
            break;
          case '2990': upd.lm = '3400';
            break;
          case '4650': if (process.hasCharsAt(p.codice, '4532', 1)) upd.lm = '7170'; else upd.lm = '4780';
            break;
          case '4980': if (process.hasCharsAt(p.codice, '4532', 1)) upd.lm = '7530'; else upd.lm = '5020';
            break;
          case '3250': upd.lm = '3230';
            break;
          case '3330': if (process.hasCharsAt(p.codice, '4532', 1)) upd.lm = '5100'; else upd.lm = '3400';
            break;
          case '6170': if (process.hasCharsAt(p.codice, '4532', 1)) upd.lm = '9330'; else upd.lm = '6220';
            break;
          case '6320': if (process.hasCharsAt(p.codice, '4532', 1)) upd.lm = '9810'; else upd.lm = '6540';
            break;
          case '6780': if (process.hasCharsAt(p.codice, '4532', 1)) upd.lm = '10320'; else upd.lm = '6880';
            break;
          case '6960': upd.lm = '7030';
            break;
          case '7130': upd.lm = '7390';
            break;
          case '7640': upd.lm = '7780';
            break;
          case '2930': upd.lm = '3230';
            break;
          case '3070': upd.lm = '3400';
            break;
          case '3220': upd.lm = '3570';
            break;
          case '2920': upd.lm = '3230';
            break;
          case '4040': upd.lm = '4540';
            break;
          case '4250': upd.lm = '4780';
            break;
          case '4470': upd.lm = '5020';
            break;
          case '3250': upd.lm = '4845';
            break;
          case '3570': if (process.hasCharsAt(p.codice, '1713', 1)) {upd.lm = '3570'; console.log('non cambio 3570', p.codice)} else {upd.lm = '5355'; console.log('cambio 3570', p.codice)}
            break;
          case '4540': if (process.hasCharsAt(p.codice, '1713', 1)) {upd.lm = '4540'; console.log('non cambio 4540', p.codice)}else {upd.lm = '6810'; console.log('cambio 4540', p.codice)}
            break;
          case '2020': upd.lm = '2250';
            break;
          case '2120': upd.lm = '2370';
            break;
          case '2240': upd.lm = '2490';
            break;
          case '7720': upd.lm = '7820';
            break;
          case '7910': upd.lm = '8220';
            break;
          case '8480': upd.lm = '8640';
            break;
        }

        upd.generazione = 'D3';
        
        if (process.hasCharAt(p.codice, 'C', 9)) {
          console.log('cambio prezzo a 529', p.codice)
          upd.prezzo = 529
        }

        if ((process.hasCharAt(p.codice, 'O', 5) || process.hasCharAt(p.codice, 'S', 5) || process.hasCharAt(p.codice, 'T', 5)) && 
          process.hasCharAt(p.codice, 'D', 6)) {
          console.log('cambio 2x24', p.codice)
          upd.w = '24'
          upd.luci = 2
        }

        if ((process.hasCharAt(p.codice, 'O', 5) || process.hasCharAt(p.codice, 'S', 5) || process.hasCharAt(p.codice, 'T', 5)) && 
          process.hasCharAt(p.codice, 'S', 6)) {
          upd.w = '28'
          console.log('cambio 2x28', p.codice)
          upd.luci = 2
        }

        if (process.hasCharsAt(p.codice, '4252', 1) && 
          (process.hasCharAt(p.codice, 'Z', 5) || process.hasCharAt(p.codice, '7', 5) || process.hasCharAt(p.codice, '8', 5))) {
          upd.w = '8'
          console.log('cambio 2x8', p.codice)
          upd.luci = 2
        }

        if ((process.hasCharsAt(p.codice, '2023', 1) || process.hasCharsAt(p.codice, '2012', 1) || process.hasCharsAt(p.codice, '1706', 1)) && 
          (process.hasCharAt(p.codice, 'Z', 5) || process.hasCharAt(p.codice, '7', 5) || process.hasCharAt(p.codice, '8', 5))) {
          upd.w = '12'
          console.log('cambio 2x12', p.codice)
          upd.luci = 2
        }

        if ((process.hasCharsAt(p.codice, '2027', 1) || process.hasCharsAt(p.codice, '0709', 1)) && 
          (process.hasCharAt(p.codice, 'O', 5) || process.hasCharAt(p.codice, 'S', 5) || process.hasCharAt(p.codice, 'T', 5)) &&
          process.hasCharAt(p.codice, 'B', 6)) {
          upd.w = '12'
          console.log('cambio 2x12 (2)', p.codice)
          upd.luci = 2
        }

        if ((process.hasCharsAt(p.codice, '2027', 1) || process.hasCharsAt(p.codice, '0709', 1)) && 
          (process.hasCharAt(p.codice, 'O', 5) || process.hasCharAt(p.codice, 'S', 5) || process.hasCharAt(p.codice, 'T', 5)) &&
          process.hasCharAt(p.codice, 'C', 6)) {
          upd.w = '17'
          console.log('cambio 2x17', p.codice)
          upd.luci = 2
        }

        if (process.hasCharsAt(p.codice, '2027', 1) && 
          (process.hasCharAt(p.codice, 'O', 5) || process.hasCharAt(p.codice, 'S', 5) || process.hasCharAt(p.codice, 'T', 5)) &&
          process.hasCharAt(p.codice, 'Q', 6)) {
          upd.w = '21'
          console.log('cambio 2x21', p.codice)
          upd.luci = 2
        }

        if (process.hasCharsAt(p.codice, '4532', 1) && 
          (process.hasCharAt(p.codice, 'O', 5) || process.hasCharAt(p.codice, 'S', 5) || process.hasCharAt(p.codice, 'T', 5)) &&
          process.hasCharAt(p.codice, 'E', 6)) {
          upd.w = '32'
          console.log('cambio 2x32', p.codice)
          upd.luci = 2
        }

        return upd;
      }
    }
  ]
};

var transform2 = {
  idx: 2,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['R', '5', '6', 'I', 'A', 'B'], pos: 5 },
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              var upd = {};
              upd.pubblicato = false
              upd.exportMetel = false

              return upd;
          }
      }
  ]
};

//regola 2.4
var transform3 = {
  idx: 3,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['W', 'Y'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.vivid(p)
        p.generazione = 'D3'

        if ((process.hasCharAt(p.codice, 'O', 5) || process.hasCharAt(p.codice, 'S', 5) || process.hasCharAt(p.codice, 'T', 5)) && 
          process.hasCharAt(p.codice, 'D', 6)) {
          console.log('cambio 2x24', p.codice)
          p.w = '24'
          p.luci = 2
        }

        if ((process.hasCharAt(p.codice, 'O', 5) || process.hasCharAt(p.codice, 'S', 5) || process.hasCharAt(p.codice, 'T', 5)) && 
          process.hasCharAt(p.codice, 'S', 6)) {
          console.log('cambio 2x28', p.codice)
          p.w = '28'
          p.luci = 2
        }

        if (process.hasCharsAt(p.codice, '4252', 1)) {
          p.w = '8'
          console.log('cambio 2x8', p.codice)
          p.luci = 2
        }

        if ((process.hasCharsAt(p.codice, '2023', 1) || process.hasCharsAt(p.codice, '2012', 1) || process.hasCharsAt(p.codice, '1706', 1))) {
          p.w = '12'
          console.log('cambio 2x12', p.codice)
          p.luci = 2
        }

        if ((process.hasCharsAt(p.codice, '2027', 1) || process.hasCharsAt(p.codice, '0709', 1)) && 
          process.hasCharAt(p.codice, 'B', 6)) {
          p.w = '12'
          console.log('cambio 2x12 (2)', p.codice)
          p.luci = 2
        }

        if ((process.hasCharsAt(p.codice, '2027', 1) || process.hasCharsAt(p.codice, '0709', 1)) && 
          process.hasCharAt(p.codice, 'C', 6)) {
          p.w = '17'
          console.log('cambio 2x17', p.codice)
          p.luci = 2
        }

        if (process.hasCharsAt(p.codice, '2027', 1) && 
          process.hasCharAt(p.codice, 'Q', 6)) {
          p.w = '21'
          console.log('cambio 2x21', p.codice)
          p.luci = 2
        }

        if (process.hasCharsAt(p.codice, '4532', 1) && 
          process.hasCharAt(p.codice, 'E', 6)) {
          p.w = '32'
          console.log('cambio 2x32', p.codice)
          p.luci = 2
        }

        switch (p.lm) {
          case '3060': if (process.hasCharsAt(p.codice, '4532', 1)) p.lm = '4380'; else p.lm = '2920';
            break;
          case '4280': if (process.hasCharsAt(p.codice, '4532', 1)) p.lm = '6090'; else p.lm = '4060';
            break;
          case '5820': if (process.hasCharsAt(p.codice, '4532', 1)) p.lm = '8280'; else p.lm = '5520';
            break;
          case '6560':
            p.lm = '6220';
            break;
          case '1930':
            p.lm = '2000';
            break;
          case '7280':
            p.lm = '6900';
            break;
        }

        return p;
      }
    }
  ]
};

//regola 2.5
var transform4 = {
  idx: 4,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt, process.filters.isModello],
  filterArguments: [{ chars: ['7'], pos: 5 },{ modello: '5b5f26286cbb3f0400f431ee' }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'D', 5);
        p.k = '3300';
        p.irc = '95';
        p.sottocategoria = 'Vivid';
        p.generazione = 'D3'

        switch (p.lm) {
          case '3070':
            p.lm = '2920';
            break;
          case '4250':
            p.lm = '4060';
            break;
        }

        return p;
      }
    }
  ]
};

//regola 2.6
var transform5 = {
  idx: 5,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasSottoCategoria, process.filters.hasCharAt],
  filterArguments: [{ sottocategoria: 'Vivid' }, { chars: ['X'], pos: 5 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'D', 5)
        p.k = '3500'
        p.generazione = 'D3'

        switch (p.lm) {
          case '2920': p.lm = '3000';
            break;
          case '4060': p.lm = '5020';
            break;
          case '5520': p.lm = '5670';
            break;
          case '6220':
            p.lm = '6390';
            break;
          case '4380':
            p.lm = '4500';
            break;
          case '6090':
            p.lm = '7530';
            break;
          case '8280':
            p.lm = '8505';
            break;
          case '2000':
            p.lm = '2030';
            break;
          case '6900':
            p.lm = '7080';
            break;
        }

        return p;
      }
    }
  ]
};


// regola 2.7
var transform6 = {
  idx: 6,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt],
  filterArguments: [{ chars: ['7', 'S'], pos: 5 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.sunlike(p)
        p.generazione = 'D3'

        switch (p.lm) {
          case '3240':
            p.lm = '2400';
            break;
          case '4780':
            p.lm = '3400';
            break;
          case '3400':
            p.lm = '2400';
            break;
          case '7390': 
            p.lm = '5600';
            break;
          case '6540':
            p.lm = '4800';
            break;
          case '5100':
            p.lm = '3600';
            break;
          case '7170':
            p.lm = '5100';
            break;
          case '9810':
            p.lm = '7200';
            break;
          case '2370':
            p.lm = '1800';
            break;
          case '8220':
            p.lm = '6400';
            break;
        }

        return p;
      }
    }
  ]
};

//regola 2.7
var transform7 = {
  idx: 7,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['W', 'Q'], pos: 5 },
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        p = process.archiviaConNote(p, 'IN40')

        return p
      }
    }
  ]
};

var transform8 = {
  idx: 8,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt],
  filterArguments: [{ chars: ['D'], pos: 9 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        if (process.hasCharsAt(p.codice, '0609', 1) ||
          process.hasCharsAt(p.codice, '1709', 1) ||
          process.hasCharsAt(p.codice, '1609', 1) ||
          process.hasCharsAt(p.codice, '1739', 1) ||
          process.hasCharsAt(p.codice, '0909', 1) ||
          process.hasCharsAt(p.codice, '1704', 1) ||
          process.hasCharsAt(p.codice, '1713', 1) ||
          process.hasCharsAt(p.codice, '0709', 1) ||
          process.hasCharsAt(p.codice, '1706', 1) ||
          process.hasCharsAt(p.codice, '2012', 1) ||
          process.hasCharsAt(p.codice, '2027', 1)) {

            p = process.casambi(p, 9)

            return p
        } else return null
      }
    }
  ]
};

var transform9 = {
  idx: 9,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt],
  filterArguments: [{ chars: ['D'], pos: 10 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        if (process.hasCharsAt(p.codice, '0609', 1) ||
          process.hasCharsAt(p.codice, '1709', 1) ||
          process.hasCharsAt(p.codice, '1609', 1) ||
          process.hasCharsAt(p.codice, '1739', 1) ||
          process.hasCharsAt(p.codice, '0909', 1) ||
          process.hasCharsAt(p.codice, '1704', 1) ||
          process.hasCharsAt(p.codice, '1713', 1) ||
          process.hasCharsAt(p.codice, '0709', 1) ||
          process.hasCharsAt(p.codice, '1706', 1) ||
          process.hasCharsAt(p.codice, '2012', 1) ||
          process.hasCharsAt(p.codice, '2027', 1)) {

            p = process.casambi(p, 10)

            return p
        } else return null
      }
    }
  ]
};

var transform10 = {
  idx: 10,
  tag: tag,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['D'], pos: [9, 10] },
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              p.alimentatore = 'Driver Dimmerabile Push N/A'
              p.dimmerabile = true

              return p
          }
      }
  ]
};

/* var transform11 = {
  idx: 11,
  tag: tag,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['D'], pos: 10 },
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              p.alimentatore = 'Driver Dimmerabile Push N/A'
              p.dimmerabile = true

              return p
          }
      }
  ]
}; */

/* process.execute(transform1)
  .then(function (err) { return process.execute(transform2) })
  .then(function (err) { return process.execute(transform3) })
  .then(function (err) { return */ process.execute(transform4)/*  }) */
  .then(function (err) { return process.execute(transform5) })
  .then(function (err) { return process.execute(transform6) })
  .then(function (err) { return process.execute(transform7) })
  .then(function (err) { return process.execute(transform8) })
  .then(function (err) { return process.execute(transform9) })
  .then(function (err) { return process.execute(transform10) })
  .then(function (err) { console.log('finito!!'); })
  .catch(function (err) { console.log('fallito!!', err); });

/* process.execute(transform1)
  .then(function (err) { return process.execute(transform2) })
  .then(function (err) { return process.execute(transform3) })
  .then(function (err) { return process.execute(transform4) })
  .then(function (err) { return process.execute(transform5) })
  .then(function (err) { return process.execute(transform6) })
  .then(function (err) { return process.execute(transform7) })
  .then(function (err) { return process.execute(transform8) })
  .then(function (err) { return process.execute(transform9) })
  .then(function (err) { console.log('finito!!'); })
  .catch(function (err) { console.log('fallito!!', err); }); */

/*
Per i modelli:
'2012 KS/2', '2027 KL/2', '0609 Bob Std Led', '1709 Bob Ext Led','1609 Bob Std Sc Led','1739 Bob Ext Sc Led', '0909 Bob Slim','1713 Bob Eco Led','1706 Mini Bob', '0709 Smart C/S','2023 KS2/P', '4532 Orion', '0776 Nuvola Led', '4712 Tech Led','4622 Fold Led', '4252 Magic Wall S', '1002 Marquez Led', '1061 Dial 2 Led'





1. Tutti i codici con 5° carattere = 'Z/7/8/O/S/T' cambio lumen del campo 'Flusso lum. nom.':

-2700 --> 3080
-2830 --> 3240
-2990 --> 3400
-4650 --> 4780
-4980 --> 5020
-3250 --> 3230
-3330 --> 3400
-6170 --> 6220
-6320 --> 6540
-6780 --> 6880
-6960 --> 7030
-7130 --> 7390
-7640 --> 7780
-2930 --> 3230
-3070 --> 3400
-3220 --> 3570
-2920 --> 3230
-4040 --> 4540
-4250 --> 4780
-4470 --> 5020
-3250 --> 4845
-3570 --> 5355
-4540 --> 6810
-2020 --> 2250
-2120 --> 2370
-2240 --> 2490
-7720 --> 7820
-7910 --> 8220
-8480 --> 8640
-Se campo 'Flusso Lum. Nom.' = 3330 and primi 4 caratteri = 4532 il campo 'Flusso Lum. Nom.' --> 5100
-Se campo 'Flusso Lum. Nom.' = 4650 and primi 4 caratteri = 4532 il campo 'Flusso Lum. Nom.' --> 7170
-Se campo 'Flusso Lum. Nom.' = 4980 and primi 4 caratteri = 4532 il campo 'Flusso Lum. Nom.' --> 7530
-Se campo 'Flusso Lum. Nom.' = 6170 and primi 4 caratteri = 4532 il campo 'Flusso Lum. Nom.' --> 9330
-Se campo 'Flusso Lum. Nom.' = 6320 and primi 4 caratteri = 4532 il campo 'Flusso Lum. Nom.' --> 9810
-Se campo 'Flusso Lum. Nom.' = 6780 and primi 4 caratteri = 4532 il campo 'Flusso Lum. Nom.' --> 10320




2. Per tutti i codici modificati --> Campo 'Generazione' = 'D3' (Da creare nel menu a tendina)

3. Tutti i codici con il 5° carattere = 'R/5/6/I/A/B' --> flag metel = false e flag sito = false.

4. Duplicare tutti i codici con 5° carattere = 'W/Y' :
   -5° carattere --> 'X'
   -il campo 'Temp. di colore' --> '3300'
   -il campo 'CRI' --> '95'
   -il campo 'Sottocategoria' --> 'Vivid'
   -il campo 'Flusso lum. nom.': 
     
-3060 --> 2920
-4280 --> 4060
-5820 --> 5520
-6560 --> 6220
-1930 --> 2000
-7280 --> 6900
-Se campo 'Flusso Lum. Nom.' = 3060 and primi 4 caratteri = 4532 il campo 'Flusso Lum. Nom.' --> 4380
-Se campo 'Flusso Lum. Nom.' = 4280 and primi 4 caratteri = 4532 il campo 'Flusso Lum. Nom.' --> 6090
-Se campo 'Flusso Lum. Nom.' = 5820 and primi 4 caratteri = 4532 il campo 'Flusso Lum. Nom.' --> 8280


5. Per il modello '0709 Smart C/S':
   Duplicare il codice con 5° carattere = 7
   -5° carattere --> 'X'
   -il campo 'Temp. di colore' --> '3300'
   -il campo 'CRI' --> '95'
   -il campo 'Sottocategoria' --> 'Vivid'
   -il campo 'Flusso lum. nom.': 

-3070 --> 2920
-4250 --> 4060


6. Duplicare tutti i codici creati al punto 4:
   -5° carattere --> 'D'
   -il campo 'Temp. di colore' --> '3500'
   -il campo 'Flusso lum. nom.': 
     
-2920 --> 3000
-4060 --> 5020
-5520 --> 5670
-6220 --> 6390
-4380 --> 4500
-6090 --> 7530
-8280 --> 8505
-2000 --> 2030
-6900 --> 7080




7. Duplicare tutti i codici con 5° carattere = '7/S':
   -5° carattere '7/S' --> 'SS'
   -il campo 'CRI' --> '97,6'
   -il campo 'Sottocategoria' --> 'Sunlike'
   -il campo 'Flusso lum. nom.': 
      
-3240 --> 2400
-4780 --> 3400
-3400 --> 2400
-7390 --> 5600
-6540 --> 4800
-5100 --> 3600
-7170 --> 5100
-9810 --> 7200
-2370 --> 1800
-8220 --> 6400



8. Tutti i codici con 5° carattere = 'W/Q':
   mettere in archivio --> 'Generazione' = 'IN40' (Da creare nel menu a tendina)
   flag metel = false e flag sito = false.
*/


