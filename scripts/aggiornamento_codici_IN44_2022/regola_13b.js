const process = require('./massive.js');

const filtro = ['0596 Smart S', '0586 Smart Q S', '0585 Smart Q XS', '0595 Smart XS']

var query = { modello: { $in: [] } };
var tag = 'lorenzo_2022_regola13b';

//regole: [2.1, 2.2, 2.3]
var transform1 = {
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['Z', '7', '8'], pos: 5 }, //pos in base 1
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {

        p = process.nero(p, 8);

        console.log('nella prima funzione di trasformazione ' + p.codice)
        return p;
      }
    }
  ]
};

process.execute(transform1)
  .then(function (err) { console.log('finito!!'); })
  .catch(function () { console.log('fallito!!'); });

/*
8.Solo per i modelli:
'0596 Smart S', '0586 Smart Q S', '0585 Smart Q XS', '0595 Smart XS'

Duplicare tutti i codici con 8° carattere = '1'
-8° carattere --> '2'
-il campo 'colore' --> 'Nero'

*/


