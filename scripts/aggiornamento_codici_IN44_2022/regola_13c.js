var process = require('./massive.js');
var modello = '5e2819eec38afa0004c25011';
var query = { modello: modello };
var tag = 'lorenzo_2022_regola13c';

var transform1 = {
  tag: tag,
  query: query,
  preProcess: 'getFromProd',
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['1'], pos: 8 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {

        p.codice = process.replaceCharAt(p.codice, '9016', 8)

        console.log('nella prima funzione di trasformazione ' + p.codice)
        return p
      }
    }
  ]
};

var transform2 = {
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['2'], pos: 8 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {

        p.codice = process.replaceCharAt(p.codice, '9005', 8)

        console.log('nella seconda funzione di trasformazione ' + p.codice)
        return p
      }
    }
  ]
};

var transform3 = {
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['9005'], pos: 8 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {

        p.codice = process.filters.replaceCharAt(p.codice, '9006', 8);
        p.colore = 'Grigio';

        console.log('nella terza funzione di trasformazione ' + p.codice)
        return p
      }
    }
  ]
};

var transform4 = {
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['9006'], pos: 8 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {

        p.codice = process.filters.replaceCharAt(p.codice, '3001', 8);
        p.colore = 'Rosso';

        console.log('nella quarta funzione di trasformazione ' + p.codice)
        return p
      }
    }
  ]
};

var transform5 = {
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['3001'], pos: 8 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {

        p.codice = process.filters.replaceCharAt(p.codice, '6029', 8);
        p.colore = 'Verde';

        console.log('nella quinta funzione di trasformazione ' + p.codice)
        return p
      }
    }
  ]
};

process.execute(transform1)
  .then(function (err) { return process.execute(transform2) })
  .then(function (err) { return process.execute(transform3) })
  .then(function (err) { return process.execute(transform4) })
  .then(function (err) { return process.execute(transform5) })
  .then(function (err) { console.log('finito!!'); })
  .catch(function (err) { console.log('fallito!!', err); });


/*
9.Solo per il modello '2065 K XS':
-Duplicare tutti i codici con 8° carattere = '1'
-8° carattere --> '9016'

-Duplicare tutti i codici con 8° carattere = '2'
-8° carattere --> '9005'

-Duplicare tutti i codici con 8° carattere = '9005'
-8° carattere --> '9006'
-il campo 'colore' --> 'Grigio'

-Duplicare tutti i codici con 8° carattere = '9006'
-8° carattere --> '3001'
-il campo 'colore' --> 'Rosso'

-Duplicare tutti i codici con 8° carattere = '3001'
-8° carattere --> '6029'
-il campo 'colore' --> 'Verde'

 */