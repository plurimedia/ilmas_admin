var process = require('./massive.js');
var modello = ['56b8aaa90de23c95600bd24f', '56b8aaa90de23c95600bd253'];
var query = { modello : { $in: modello } };
var tag = 'dani_20221028_05';

var transform1 = {
  idx: 1,
  tag: tag,
  query: query,
  preProcess: 'getFromProd',
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['C'], pos: 6 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'B', 6)
        p.w = '12'
        p.ma = '350'

        switch (p.lm) {
          case '2270':
            p.lm = '1615';
            break;
          case '2390':
            p.lm = '1700';
            break;
          case '2510':
            p.lm = '1785';
            break;
          case '2030':
            p.lm = '1460';
            break;
          case '2085':
            p.lm = '1500';
            break;
          case '1700':
            p.lm = '1200';
            break;
        }

        return p
      }
    }
  ]
}

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .catch(function(err) { console.log("fallito!!", err); });