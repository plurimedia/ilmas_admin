const process = require('./massive.js');

const filtro = ['0596 Smart S', '0586 Smart Q S', '0526 Smart A/ S', '2056 KL/B']

var query = { modello: { $in: [] } };
var tag = 'lorenzo_2022_regola13';

//regole: [2.1, 2.2, 2.3]
var transform1 = {
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['Z', '7', '8'], pos: 5 }, //pos in base 1
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {

        switch (p.lm) {
          case '1460': p.lm = '1615';
            break;
          case '1535': p.lm = '1700';
            break;
          case '1610': p.lm = '1785';
            break;
          case '2020': if (p.codice.substring(0, 3) === '2056') p.lm = '1615'; else p.lm = '2270';
            break;
          case '2125': p.lm = '2390';
            break;
          case '2235': p.lm = '2510';
            break;
          case '2375': p.lm = '2695';
            break;
          case '2500': p.lm = '2835';
            break;
          case '2626': p.lm = '2980';
            break;
        }

        p.generazione = 'D3';
        if (p.codice.charAt(4) === 'Z') p.codice = process.replaceCharAt(p.codice, 'O', 5)
        if (p.codice.charAt(4) === '7') p.codice = process.replaceCharAt(p.codice, 'S', 5)
        if (p.codice.charAt(4) === '8') p.codice = process.replaceCharAt(p.codice, 'T', 5)

        if (p.codice.charAt(4) === 'R' || '5' || '6' || 'I' || 'A' || 'B') {

          p.exportMetel = false;
          p.pubblicato = false;

        }

        console.log('nella prima funzione di trasformazione ' + p.codice)
        return p;
      }
    }
  ]
};

//regola 2.4
var transform2 = {
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['W'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.vivid(p)

        switch (p.lm) {
          case '1530':
            p.lm = '1460';
            break;
          case '2140':
            p.lm = '2030';
            break;
          case '2530':
            p.lm = '2400';
            break;
        }

        console.log('nella seconda funzione di trasformazione ' + p.codice)
        return p;
      }
    }
  ]
};

//regola 2.5
var transform3 = {
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasSottoCategoria, process.filters.hasCharAt],
  filterArguments: [{ sottocategoria: 'Vivid' }, { chars: ['X'], pos: 5 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'D', 5);
        p.k = 3500;

        switch (p.lm) {
          case '1460':
            p.lm = '1500';
            break;
          case '2030':
            p.lm = '2085';
            break;
          case '2400':
            p.lm = '2470';
            break;
        }

        console.log('nella terza funzione di trasformazione ' + p.codice);
        return p;

      }
    }
  ]
};

//regola 2.6
var transform4 = {
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasSottoCategoria, process.filters.hasCharAt],
  filterArguments: [{ sottocategoria: 'Vivid' }, { chars: ['X'], pos: 5 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.sunlike(p);

        switch (p.lm) {
          case '1700': p.lm = '1200';
            break;
          case '2390': p.lm = '1700';
            break;
          case '2835': p.lm = '2100';
            break;
        }

        console.log('nella seconda funzione di trasformazione ' + p.codice)
        return p;
      }
    }
  ]
};


// regola 2.6
var transform5 = {
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['W', 'Q'], pos: 5 },
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        p = process.archiviaConNote(p, 'IN40')

        console.log('nella quinta funzione di trasformazione ' + p.codice)
        return p
      }
    }
  ]
};


process.execute(transform1)
  .then(function (err) { return process.execute(transform2) })
  .then(function (err) { return process.execute(transform3) })
  .then(function (err) { return process.execute(transform4) })
  .then(function (err) { return process.execute(transform5) })
  .then(function (err) { console.log('finito!!'); })
  .catch(function () { console.log('fallito!!'); });

/*
Per i modelli:
'0596 Smart S', '0586 Smart Q S', '0526 Smart A/ S', '2056 KL/B'


1. Duplicare tutti i codici con 5° carattere = 'Z/7/8': 
-il campo 'Flusso lum. nom.':
-1460 --> 1615
-1535 --> 1700
-1610 --> 1785
-2020 --> 2270
-2125 --> 2390
-2235 --> 2510
-2375 --> 2695
-2500 --> 2835
-2626 --> 2980
-Se il campo 'Flusso Lum. Nom.' = 2020 and 4 primi caratteri = 2056 --> 1615



-il campo 'Generazione' = 'D3' (Da creare nel menu a tendina)
-il 5° carattere:
if = Z --> O
if = 7 --> S
if = 8 --> T

3. Tutti i codici con il 5° carattere = 'R/5/6/Z/7/8' --> flag metel = false e flag sito = false.

4. Duplicare tutti i codici con 5° carattere = 'W' :
   -5° carattere --> 'X'
   -il campo 'Temp. di colore' --> '3300'
   -il campo 'CRI' --> '95'
   -il campo 'Sottocategoria' --> 'Vivid'
   -il campo 'Flusso lum. nom.': 
   1530 --> 1460
   2140 --> 2030
   2530 --> 2400

   

5. Duplicare tutti i codici creati al punto 4:
   -5° carattere --> 'D'
   -il campo 'Temp. di colore' --> '3500'
   -il campo 'Flusso lum. nom.': 
    1460 --> 1500
    2030 --> 2085
    2400 --> 2470



6. Duplicare tutti i codici con 5° carattere = 'S':
   -5° carattere 'S' --> 'SS'
   -il campo 'CRI' --> '97,6'
   -il campo 'Sottocategoria' --> 'Sunlike'
   -il campo 'Flusso lum. nom.': 
     1700 --> 1200
     2390 --> 1700
     2835 --> 2100


7. Tutti i codici con 5° carattere = 'W/Q':
   mettere in archivio --> 'Generazione' = 'IN40' (Da creare nel menu a tendina)
   flag metel = false e flag sito = false.

*/


