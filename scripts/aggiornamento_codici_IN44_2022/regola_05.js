const process = require('./massive.js');

const filtro = ['1006 Marquez Led']

var query = { modello: { $in: [] } };
var tag = 'lorenzo_2022_regola05';

//regole: [5.1, 5.2, 5.3]
var transform1 = {
  idx: 1,
  tag: tag,
  // query: query,
  modelli: filtro,
  preProcess : 'backupProdLocal',//'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['Z', '7', '8', 'O', 'S', 'T'], pos: 5 }, //pos in base 1
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {

        var upd = {};

        switch (p.lm) {
          case '9750': upd.lm = '9690';
            break;
          case '9990': upd.lm = '10200';
            break;
          case '13950': upd.lm = '14340';
            break;
          case '14940': upd.lm = '15060';
            break;
          case '18510': upd.lm = '18660';
            break;
          case '18960': upd.lm = '19620';
            break;
          case '20340': upd.lm = '20640';
            break;
          case '23160': upd.lm = '23460';
            break;
          case '23730': upd.lm = '24660';
            break;
          case '25440': upd.lm = '25920';
            break;
        }

        upd.generazione = 'D3';

        /* if (p.codice.charAt(4) === 'R' || '5' || '6' || 'I' || 'A' || 'B') {

          upd.exportMetel = false;
          upd.pubblicato = false;

        } */

        // console.log('nella prima funzione di trasformazione ' + p.codice)
        return upd;
      }
    }
  ]
};

var transform2 = {
  idx: 2,
  tag: tag,
  modelli: filtro,
  filter: [process.filters.hasCharAt,process.filters.hasCharAt,process.filters.hasCharAt],
  filterArguments: [{ chars: ['O', 'S', 'T'], pos: 5 },{ chars: ['D', 'E'], pos: 6 },{ chars: ['E'], pos: 9 }],
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              var upd = {};
              upd.prezzo = 1077

              return upd;
          }
      }
  ]
};

var transform3 = {
  idx: 3,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['R', '5', '6', 'I', 'A', 'B'], pos: 5 },
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              var upd = {};
              upd.pubblicato = false
              upd.exportMetel = false

              return upd;
          }
      }
  ]
};

//regola 2.4
var transform4 = {
  idx: 4,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['W', 'Y'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.vivid(p)
        p.generazione = 'D3'

        switch (p.lm) {
          case '9180': p.lm = '8760';
            break;
          case '12840': p.lm = '12180';
            break;
          case '17460': p.lm = '16560';
            break;
          case '21840':
            p.lm = '20700';
            break;
        }

        if (process.hasCharAt(p.codice, 'X', 5) && p.codice.length <= 8) p.prezzo = 1011
        if (process.hasCharAt(p.codice, 'X', 5) && process.hasCharAt(p.codice, 'E', 9)) p.prezzo = 1144
        if (process.hasCharAt(p.codice, 'X', 5) && (process.hasCharAt(p.codice, 'D', 9) || process.hasCharAt(p.codice, 'L', 9))) p.prezzo = 1357
        // console.log('nella seconda funzione di trasformazione ' + p.codice)
        return p;
      }
    }
  ]
};

//regola 2.5

var transform5 = {
  idx: 5,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasSottoCategoria, process.filters.hasCharAt],
  filterArguments: [{ sottocategoria: 'Vivid' }, { chars: ['X'], pos: 5 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'D', 5)
        p.k = '3500'
        p.generazione = 'D3'

        switch (p.lm) {
          case '8760': p.lm = '9000';
            break;
          case '12180': p.lm = '12510';
            break;
          case '20700': p.lm = '21240';
            break;
        }

        // console.log('nella terza funzione di trasformazione ' + p.codice)
        return p;
      }
    }
  ]
};


// regola 2.6
var transform6 = {
  idx: 6,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt],
  filterArguments: [{ chars: ['7', 'S'], pos: 5 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.sunlike(p)
        p.generazione = 'D3'

        switch (p.lm) {
          case '10200':
            p.lm = '7200';
            break;
          case '14340':
            p.lm = '10200';
            break;
          case '19620':
            p.lm = '14400';
            break;
          case '24660': p.lm = '19200';
            break;
        }

        if (process.hasCharAt(p.codice, 'S', 5) && process.hasCharAt(p.codice, 'S', 6) && p.codice.length <= 9) p.prezzo = 1144
        if (process.hasCharAt(p.codice, 'S', 5) && process.hasCharAt(p.codice, 'S', 6) && process.hasCharAt(p.codice, 'E', 10)) p.prezzo = 1277
        if (process.hasCharAt(p.codice, 'S', 5) && process.hasCharAt(p.codice, 'S', 6) && (process.hasCharAt(p.codice, 'D', 10) || process.hasCharAt(p.codice, 'L', 10))) p.prezzo = 1490
        
        return p;
      }
    }
  ]
};

//regola 2.7
var transform7 = {
  idx: 7,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['W', 'Q'], pos: 5 },
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        p = process.archiviaConNote(p, 'IN40')

        // console.log('nella quinta funzione di trasformazione ' + p.codice)
        return p
      }
    }
  ]
};

var transform8 = {
  idx: 8,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['D'], pos: [9, 10] },
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              p.alimentatore = 'Driver Dimmerabile Push N/A'
              p.dimmerabile = true

              return p
          }
      }
  ]
};

var transform9 = {
  idx: 9,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['D'], pos: [6, 7] },
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        p.w = '24'
        p.luci = 6

        return p
      }
    }
  ]
};

var transform10 = {
  idx: 10,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['E'], pos: [6, 7] },
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        p.w = '32'
        p.luci = 6

        return p
      }
    }
  ]
};

process.execute(transform1)
  .then(function (err) { return process.execute(transform2) })
  .then(function (err) { return process.execute(transform3) })
  .then(function (err) { return process.execute(transform4) })
  .then(function (err) { return process.execute(transform5) })
  .then(function (err) { return process.execute(transform6) })
  .then(function (err) { return process.execute(transform7) })
  .then(function (err) { return process.execute(transform8) })
  .then(function (err) { return process.execute(transform9) })
  .then(function (err) { return process.execute(transform10) })
  .then(function (err) { console.log('finito!!'); })
  .catch(function () { console.log('fallito!!'); });

/*

*/


