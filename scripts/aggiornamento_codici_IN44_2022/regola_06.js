const process = require('./massive.js');

const filtro = ['2055 KS/B', '2056 KL/B', '2045 K/XS Omnia', '2045 K/XS 48', '2038 K/2 XS 48', '2041 KS/ Plus S 48V', '2040 KS/S 48V',
  '2035 KS/48V', '2025 KS', '2140 Smart E / S']

var query = { modello: { $in: [] } };
var tag = 'lorenzo_2022_regola06';

//regole: [6.1, 6.2]
var transform1 = {
  idx: 1,
  tag: tag,
  // query: query,
  modelli: filtro,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['R', '5', '6', 'I', 'A', 'B'], pos: 5 }, //pos in base 1
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {

        var upd = {};

        upd.exportMetel = false;
        upd.pubblicato = false;

        return upd;
      }
    }
  ]
};

var transform2 = {
  idx: 2,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['Z', '7', '8', 'O', 'S', 'T'], pos: 5 }, //pos in base 1
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {

        var upd = {};

        switch (p.lm) {
          case '1350': upd.lm = '1540';
            break;
          case '1415': upd.lm = '1620';
            break;
          case '1495': upd.lm = '1700';
            break;
          case '1790': upd.lm = '2125';
            break;
          case '1880': upd.lm = '2235';
            break;
          case '1990': upd.lm = '2350';
            break;
          case '2020': if (p.codice.substring(0, 3) === '2038') upd.lm = '2250'; else upd.lm = '2270';
            break;
          case '2125': upd.lm = '2390';
            break;
          case '2235': upd.lm = '2510';
            break;
          case '1010': upd.lm = '1125';
            break;
          case '1060': upd.lm = '1185';
            break;
          case '1120': upd.lm = '1245';
            break;
          case '2120': upd.lm = '2370';
            break;
          case '2240': upd.lm = '2490';
            break;
        }

        return upd;
      }
    }
  ]
};

//regola 2.3
var transform3 = {
  idx: 3,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt,process.filters.isModello],
  filterArguments: [{ chars: ['7', 'S'], pos: 5 },{ modello: '5fd7226fbfdf014143b00640' }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.sunlike(p)

        switch (p.lm) {
          case '1185': p.lm = '800';
            break;
        }

        return p;
      }
    }
  ]
};


process.execute(transform1)
  .then(function (err) { return process.execute(transform2) })
  .then(function (err) { return process.execute(transform3) })
  .then(function (err) { console.log('finito!!'); })
  .catch(function () { console.log('fallito!!'); });

/*
Per tutti i modelli:
'2055 KS/B', '2056 KL/B', '2045 K/XS Omnia', '2045 K/XS 48', '2038 K/2 XS 48','2041 KS/ Plus S 48V','2040 KS/S 48V',
'2035 KS/48V','2025 KS','2140 Smart E / S'










1.Tutti i codici con il 5° carattere = 'R/5/6/I/A/B' --> flag metel = false e flag sito = false.

2. Tutti i codici con 5° carattere = 'Z/7/8/O/S/T' cambio lumen del campo 'Flusso lum. nom.':
-1350 --> 1540
-1415 --> 1620
-1495 --> 1700
-1790 --> 2125
-1880 --> 2235
-1990 --> 2350
-2020 --> 2270
-2125 --> 2390
-2235 --> 2510
-1010 --> 1125
-1060 --> 1185
-1120 --> 1245
-2120 --> 2370
-2240 --> 2490


-Se campo 'Flusso Lum. Nom.' = 2020 and primi 4° caratteri = 2038 --> 2250



3.Per il modello '2045 K/XS Omnia':

 Duplicare tutti i codici con 5° carattere = '7/S':
   -5° carattere '7/S' --> 'SS'
   -il campo 'CRI' --> '97,6'
   -il campo 'Sottocategoria' --> 'Sunlike'
   -il campo 'Flusso lum. nom.': 

-1185 --> 800



*/