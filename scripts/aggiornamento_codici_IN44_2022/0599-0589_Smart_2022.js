var process = require('./massive.js');
var modello = ['5badd6e8cc10f30400a4724d', '5be1558e9746be0400ff6fe7'];
var query = { modello: { $in: modello } };
var tag = 'dani_20221027_03';

var transform1 = {
    idx: 1,
    tag: tag,
    query: query,
    preProcess: 'backupProdLocal',//'getFromProd',
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['O', 'S', 'T'], pos: 5 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                p.lm = process.elaboraCorrispondenze(`3085 --> 3110
                3085 --> 3110
                4410 --> 4480
                3160 --> 3270
                2325 --> 2390
                4520 --> 4715
                2490 --> 2510
                3390 --> 3440
                4840 --> 4955
                `, p.lm)

                p.w = process.elaboraCorrispondenze(`25 --> 24
                39 --> 37
                `, p.w)
                console.log(p.lm, p.w, p.codice)

                if ((process.hasCharAt(p.codice, 'D', 6) || process.hasCharAt(p.codice, 'F', 6) ||
                    process.hasCharAt(p.codice, 'C', 6)) && process.hasCharAt(p.codice, 'E', 9)) {
                    p.prezzo -= 10
                }
                return p
            }
        }
    ]
};

/* var transform1 = {
    idx: 1,
    tag: tag,
    query: query,
    preProcess: 'getFromProd',
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['C', 'D', 'F'], pos: 6 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                switch (p.lm) {
                    case '3085':
                        p.lm = '3110'
                        break
                    case '4410':
                        p.lm = '4480'
                        break
                    case '3160':
                        p.lm = '3270'
                        break
                    case '2325':
                        p.lm = '2390'
                        break
                    case '4520':
                        p.lm = '4715'
                        break
                    case '2490':
                        p.lm = '2510'
                        break
                    case '3390':
                        p.lm = '3440'
                        break
                    case '4840':
                        p.lm = '4955'
                        break
                }

                switch (p.w) {
                    case '25':
                        p.w = '24'
                        break
                    case '39':
                        p.w = '37'
                        break
                }

                if (process.hasCharsAt(p.codice, '0589', 1) &&
                    (process.hasCharAt(p.codice, 'D', 5) || process.hasCharAt(p.codice, 'F', 5)) &&
                    process.hasCharAt(p.codice, 'E', 9)) {
                    p.prezzo -= 20
                }
                return p
            }
        }
    ]
}; */

var transform2 = {
    idx: 2,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['W', 'Y'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p = process.vivid(p)

                p.prezzo -= 11

                p.lm = process.elaboraCorrispondenze(`2140 --> 2030
                2910 --> 2760
                4160 --> 3950
                `, p.lm)

                switch (p.w) {
                    case '25':
                        p.w = '24'
                        break
                    case '39':
                        p.w = '37'
                        break
                }

                /* if (process.hasCharAt(p.codice, 'L', 9)) {
                    p.prezzo += 53
                    p = process.casambi(p, 9)
                } */

                return p
            }
        }
    ]
};

var transform3 = {
    idx: 3,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['L'], pos: 9 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p = process.casambi(p, 9)
                p.prezzo += 53

                return p
            }
        }
    ]
};

var transform4 = {
    idx: 4,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasSottoCategoria, process.filters.hasCharAt],
    filterArguments: [{ sottocategoria: 'Vivid' }, { chars: ['X'], pos: 5 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 5)
                p.k = '3500'

                switch (p.lm) {
                    case '2030': p.lm = '2085';
                        break;
                    case '2760': p.lm = '2835';
                        break;
                    case '3950': p.lm = '4040';
                        break;
                }

                return p;
            }
        }
    ]
};

var transform5 = {
    idx: 5,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt],
    filterArguments: [{ chars: ['7', 'S'], pos: 5 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.sunlike(p)

                p.prezzo += 33

                /* switch (p.lm) {
                    case 3270:
                        p.lm = 2400;
                        break;
                    case 2390:
                        p.lm = 1700;
                        break;
                    case 4715:
                        p.lm = 3700;
                        break;
                } */
                p.lm = process.elaboraCorrispondenze(`3270 --> 2400
                    2390 --> 1700
                    4715 --> 3700`, p.lm)

                return p;
            }
        }
    ]
};

var transform6 = {
    idx: 6,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt],
    filterArguments: [{ chars: ['1'], pos: 8 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.nero(p, 8)

                return p;
            }
        }
    ]
};

var transform7 = {
    idx: 7,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt],
    filterArguments: [{ chars: ['1'], pos: 9 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.nero(p, 9)

                return p;
            }
        }
    ]
};

var transform8 = {
    idx: 8,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['D'], pos: 9 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.alimentatore = 'Driver Dimmerabile Push N/A'
                p.dimmerabile = true

                return p
            }
        }
    ]
};

var transform9 = {
    idx: 9,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['D'], pos: 10 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.alimentatore = 'Driver Dimmerabile Push N/A'
                p.dimmerabile = true

                return p
            }
        }
    ]
};

var transform10 = {
    idx: 10,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['9'], pos: 3 }, // solo il 0599
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                if ((process.hasCharAt(p.codice, 'O', 5) || process.hasCharAt(p.codice, 'S', 5) || process.hasCharAt(p.codice, 'T', 5)) && 
                    (process.hasCharAt(p.codice, 'C', 6) || process.hasCharAt(p.codice, 'D', 6) || process.hasCharAt(p.codice, 'F', 6)) && 
                    process.hasCharAt(p.codice, 'E', 9)
                ) p.prezzo = 160

                if ((process.hasCharAt(p.codice, 'X', 5) || process.hasCharAt(p.codice, 'D', 5)) && 
                    (process.hasCharAt(p.codice, 'C', 6) || process.hasCharAt(p.codice, 'D', 6) || process.hasCharAt(p.codice, 'F', 6)) && 
                    !p.alimentatore
                ) p.prezzo = 149

                if ((process.hasCharAt(p.codice, 'X', 5) || process.hasCharAt(p.codice, 'D', 5)) && 
                    (process.hasCharAt(p.codice, 'C', 6) || process.hasCharAt(p.codice, 'D', 6) || process.hasCharAt(p.codice, 'F', 6)) && 
                    process.hasCharAt(p.codice, 'E', 9)
                ) p.prezzo = 171

                if ((process.hasCharAt(p.codice, 'X', 5) || process.hasCharAt(p.codice, 'D', 5)) && 
                    (process.hasCharAt(p.codice, 'C', 6) || process.hasCharAt(p.codice, 'D', 6) || process.hasCharAt(p.codice, 'F', 6)) && 
                    (process.hasCharAt(p.codice, 'L', 9) || process.hasCharAt(p.codice, 'D', 9))
                ) p.prezzo = 207

                if ((process.hasCharAt(p.codice, 'X', 5) || process.hasCharAt(p.codice, 'D', 5)) && 
                    (process.hasCharAt(p.codice, 'C', 6) || process.hasCharAt(p.codice, 'D', 6) || process.hasCharAt(p.codice, 'F', 6)) && 
                    process.hasCharAt(p.codice, 'C', 9)
                ) p.prezzo = 260

                if ((process.hasCharAt(p.codice, 'S', 6)) && 
                    (process.hasCharAt(p.codice, 'C', 7) || process.hasCharAt(p.codice, 'D', 7) || process.hasCharAt(p.codice, 'F', 7)) && 
                    !p.alimentatore
                ) p.prezzo = 171

                if ((process.hasCharAt(p.codice, 'S', 6)) && 
                    (process.hasCharAt(p.codice, 'C', 7) || process.hasCharAt(p.codice, 'D', 7) || process.hasCharAt(p.codice, 'F', 7)) && 
                    process.hasCharAt(p.codice, 'E', 10)
                ) p.prezzo = 194

                if ((process.hasCharAt(p.codice, 'S', 6)) && 
                    (process.hasCharAt(p.codice, 'C', 7) || process.hasCharAt(p.codice, 'D', 7) || process.hasCharAt(p.codice, 'F', 7)) && 
                    (process.hasCharAt(p.codice, 'L', 10) || process.hasCharAt(p.codice, 'D', 10))
                ) p.prezzo = 229

                if ((process.hasCharAt(p.codice, 'S', 6)) && 
                    (process.hasCharAt(p.codice, 'C', 7) || process.hasCharAt(p.codice, 'D', 7) || process.hasCharAt(p.codice, 'F', 7)) && 
                    process.hasCharAt(p.codice, 'C', 10)
                ) p.prezzo = 282

                return p
            }
        }
    ]
};

var transform11 = {
    idx: 11,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['8'], pos: 3 }, // solo il 0589
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                if ((process.hasCharAt(p.codice, 'O', 5) || process.hasCharAt(p.codice, 'S', 5) || process.hasCharAt(p.codice, 'T', 5)) && 
                    (process.hasCharAt(p.codice, 'C', 6) || process.hasCharAt(p.codice, 'D', 6) || process.hasCharAt(p.codice, 'F', 6)) && 
                    process.hasCharAt(p.codice, 'E', 9)
                ) p.prezzo = 178

                if ((process.hasCharAt(p.codice, 'X', 5) || process.hasCharAt(p.codice, 'D', 5)) && 
                    (process.hasCharAt(p.codice, 'D', 6) || process.hasCharAt(p.codice, 'F', 6)) && 
                    process.hasCharAt(p.codice, 'E', 9)
                ) p.prezzo = 189

                if ((process.hasCharAt(p.codice, 'S', 6)) && 
                    (process.hasCharAt(p.codice, 'C', 7)) && 
                    process.hasCharAt(p.codice, 'E', 10)
                ) p.prezzo = 211

                if ((process.hasCharAt(p.codice, 'S', 6)) && 
                    (process.hasCharAt(p.codice, 'C', 7) || process.hasCharAt(p.codice, 'D', 7) || process.hasCharAt(p.codice, 'F', 7)) && 
                    !p.alimentatore
                ) p.prezzo = 189

                return p
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function (err) { return process.execute(transform8) })
    .then(function (err) { return process.execute(transform9) })
    .then(function (err) { return process.execute(transform10) })
    .then(function (err) { return process.execute(transform11) })
    .then(function (err) { console.log("finito!!"); })
    .catch(function () { console.log("fallito!!"); });


/*
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto0599.json --query='{"modello":{"$oid":"5badd6e8cc10f30400a4724d"}}'
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto0589.json --query='{"modello":{"$oid":"5be1558e9746be0400ff6fe7"}}'

db.Prodotto.remove({modello:{$in:[ObjectId('5badd6e8cc10f30400a4724d'),ObjectId('5be1558e9746be0400ff6fe7')]}})

mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto0599.json
mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto0589.json

Per i modelli:
"0599 SMART XL", "0589 Smart Q XL"


Duplicare tutti i codici con 8° carattere = "1"
-8° carattere --> "2"
-il campo "colore" --> "Nero"


 */