var process = require('./massive.js');
var modello = '5aa92c028525e50400b87d52';
var query = { modello: modello };
var tag = 'dani_20221021_01';

var transform1 = {
  idx: 1,
  tag: tag,
  query: query,
  preProcess : 'backupProdLocal',//'getFromProd',
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['Z', '7', '8'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {

        p.generazione = 'D3'

        if (process.hasCharAt(p.codice, 'Z', 5)) {
          p.codice = process.replaceCharAt(p.codice, 'O', 5)
        }
        if (process.hasCharAt(p.codice, '7', 5)) {
          p.codice = process.replaceCharAt(p.codice, 'S', 5)
        }
        if (process.hasCharAt(p.codice, '8', 5)) {
          p.codice = process.replaceCharAt(p.codice, 'T', 5)
        }
        console.log(p.codice)

        switch (p.lm) {
          case '2020':
            p.lm = '2270'
            break
          case '2125':
            p.lm = '2390'
            break
          case '2235':
            p.lm = '2510'
            break
          case '2710':
            p.lm = '3110'
            break
          case '2860':
            p.lm = '3270'
            break
          case '3000':
            p.lm = '3440'
            break
          case '3190':
            p.lm = '3515'
            break
          case '3355':
            p.lm = '3695'
            break
          case '3530':
            p.lm = '4105'
            break
        }

        return p;
      }
    }
  ]
};

var transform2 = {
  idx: 2,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt,process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
  filterArguments: [{ chars: ['O', 'S', 'T'], pos: 5 },{ chars: ['T'], pos: 6 },{ len: 8 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.dali(p, 9)
        p.prezzo += 67

        return p;
      }
    }
  ]
};

var transform3 = {
  idx: 3,
  tag: tag,
  query: query,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['R', '5', '6', 'Z', '7', '8'], pos: 5 },
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        var upd = {};
        upd.pubblicato = false
        upd.exportMetel = false

        return upd;
      }
    }
  ]
};

var transform4 = {
  idx: 4,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['W'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.vivid(p)
        p.generazione = 'D3'
        p.prezzo += 11

        switch (p.lm) {
          case '2140':
            p.lm = '2030'
            break
          case '2910':
            p.lm = '2760'
            break
          case '3640':
            p.lm = '3280'
            break
          case '3950':
            p.lm = '3280'
            break
        }

        return p;
      }
    }
  ]
};

var transform5 = {
  idx: 5,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt, process.filters.hasCharAt, process.filters.hasLunghezzaCodiceMax],
  filterArguments: [{ chars: ['T'], pos: 6 }, { chars: ['X'], pos: 5 }, { len: 8 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.dali(p, 9)
        p.prezzo += 67

        return p;
      }
    }
  ]
};

var transform6 = {
  idx: 6,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: [process.filters.hasSottoCategoria, process.filters.hasCharAt],
  filterArguments: [{ sottocategoria: 'Vivid' }, { chars: ['X'], pos: 5 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'D', 5)
        p.k = '3500'
        p.generazione = 'D3'

        switch (p.lm) {
          case '2030':
            p.lm = '2085'
            break
          case '2760':
            p.lm = '2835'
            break
          case '3280':
            p.lm = '3370'
            break
        }

        return p;
      }
    }
  ]
};

var transform7 = {
  idx: 7,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['S'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.sunlike(p)
        p.generazione = 'D3'
        if (p.alimentatore === 'Driver standard') p.prezzo += 11
        if (p.alimentatore === 'Driver dali') p.prezzo += 11

        switch (p.lm) {
          case '2390':
            p.lm = '1700'
            break
          case '3270':
            p.lm = '2400'
            break
          case '3695':
            p.lm = '3000'
            break
        }

        return p;
      }
    }
  ]
};

var transform8 = {
  idx: 8,
  tag: tag,
  query: query,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['W', 'Q'], pos: 5 },
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        var upd = {}
        upd.generazione = 'IN40'
        upd.pubblicato = false
        upd.exportMetel = false

        return upd
      }
    }
  ]
};

var transform9 = {
  idx: 9,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt],
  filterArguments: [{ chars: ['L'], pos: 9 }],
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        p.dimmerabile = true

        return p
      }
    }
  ]
};

var transform10 = {
  idx: 10,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt],
  filterArguments: [{ chars: ['L'], pos: 10 }],
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        p.dimmerabile = true

        return p
      }
    }
  ]
};

var transform11 = {
  idx: 11,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt, process.filters.hasCharAt],
  filterArguments: [{ chars: ['L'], pos: 9 }, { chars: ['O', 'S', 'T', 'X', 'D'], pos: 5 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.casambi(p, 9)
        p.prezzo += 22

        return p
      }
    }
  ]
};

var transform12 = {
  idx: 12,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt],
  filterArguments: [{ chars: ['L'], pos: 10 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.casambi(p, 10)
        p.prezzo += 22

        return p
      }
    }
  ]
};

process.execute(transform1)
  .then(function (err) { return process.execute(transform2) })
  .then(function (err) { return process.execute(transform3) })
  .then(function (err) { return process.execute(transform4) })
  .then(function (err) { return process.execute(transform5) })
  .then(function (err) { return process.execute(transform6) })
  .then(function (err) { return process.execute(transform7) })
  .then(function (err) { return process.execute(transform8) })
  .then(function (err) { return process.execute(transform9) })
  .then(function (err) { return process.execute(transform10) })
  .then(function (err) { return process.execute(transform11) })
  .then(function (err) { return process.execute(transform12) })
  .then(function (err) { console.log("finito!!"); })
  .catch(function () { console.log("fallito!!"); });

/*


 Per il modello "2242 Smart Track M":


1. Duplicare tutti i codici con 5° carattere = "Z/7/8": 
-il campo "Flusso lum. nom.":
-2020 --> 2270 
-2125 --> 2390 
-2235 --> 2510 
-2710 --> 3110 
-2860 --> 3270 
-3000 --> 3440 
-3190 --> 3515
-3355 --> 3695
-3530 --> 4105

-il campo "Generazione" = "D3" (Da creare nel menu a tendina)
-il 5° carattere:
if = Z --> O
if = 7 --> S
if = 8 --> T

3. Tutti i codici con il 5° carattere = "R/5/6/Z/7/8" --> flag metel = false e flag sito = false.

4. Duplicare tutti i codici con 5° carattere = "W" :
   -5° carattere --> "X"
   -il campo "Temp. di colore" --> "3300"
   -il campo "CRI" --> "95"
   -il campo "Sottocategoria" --> "Vivid"
   -il campo "Flusso lum. nom.": 
    2140 --> 2030
    2910 --> 2760
    3640 --> 3280

   

5. Duplicare tutti i codici creati al punto 4:
   -5° carattere --> "D"
   -il campo "Temp. di colore" --> "3500"
   -il campo "Flusso lum. nom.": 
     2030 --> 2085
     2760 --> 2835
     3280 --> 3370
      



6. Duplicare tutti i codici con 5° carattere = "S":
   -5° carattere "S" --> "SS"
   -il campo "CRI" --> "97,6"
   -il campo "Sottocategoria" --> "Sunlike"
   -il campo "Flusso lum. nom.": 
      2390 --> 1700
      3270 --> 2400
      3695 --> 3000



7. Tutti i codici con 5° carattere = "W/Q":
   mettere in archivio --> "Generazione" = "IN40" (Da creare nel menu a tendina)
   flag metel = false e flag sito = false.





 */