var process = require('./massive.js');
var modello = '5e2819eec38afa0004c25011';
var query = { modello: modello };
const filtro = ['2001 KS Track', '2002 KL Track', '2029 KXL Track', '2025 KS/A Track', '2006 KL/A Track', '2031 KXL/A Track', 
'2240 Smart Track XS', '1240 Over Led Ps Mini', '1241 Over Led Ps Midi', '1242 Over Led Ps Maxi', '1244 Over Led Maxi F112', 
'1330 Fly Mini', '1331 Fly Midi', '1332 Fly Maxi', '1334 Fly Maxi F112', '1333 FLY MINI-A', '1335 Fly Midi/A', '1336 Fly Maxi/ A']
var tag = 'lorenzo_2022_regola13c';

var transform1 = {
  tag: tag,
  // query: query,
  modelli: ['1335 Fly Midi/A'],
  preProcess: 'getFromProd',
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['XX'], pos: 15 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {

        p.codice = process.replaceCharAt(p.codice, '9016', 8)

        console.log('nella prima funzione di trasformazione ' + p.codice)
        return p
      }
    }
  ]
};

process.execute(transform1)
  .then(function (err) { console.log('finito!!'); })
  .catch(function (err) { console.log('fallito!!', err); });