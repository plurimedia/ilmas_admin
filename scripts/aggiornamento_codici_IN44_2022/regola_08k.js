var process = require('./massive.js');
var modello = ['5d109fab466e830004c10201'];
var query = { modello : { $in: modello } };
var tag = 'dani_20221028_09';

var transform1 = {
  idx: 1,
  tag: tag,
  query: query,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['D'], pos: 6 },
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        p = process.archiviaConNote(p, 'IN40')

        return p
      }
    }
  ]
}

var transform2 = {
  idx: 2,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['C'], pos: 6 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'B', 6)
        p.w = '12'
        p.ma = '350'

        switch (p.lm) {
          case '2270':
            p.lm = '1615';
            break;
          case '2390':
            p.lm = '1700';
            break;
          case '2510':
            p.lm = '1785';
            break;
          case '2030':
            p.lm = '1460';
            break;
          case '2085':
            p.lm = '1500';
            break;
          case '1700':
            p.lm = '1200';
            break;
        }

        return p
      }
    }
  ]
}

var transform3 = {
  idx: 3,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['C'], pos: 6 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'Q', 6)
        p.w = '21'
        p.ma = '600'

        switch (p.lm) {
          case '2270':
            p.lm = '2695';
            break;
          case '2390':
            p.lm = '2835';
            break;
          case '2510':
            p.lm = '2980';
            break;
          case '2030':
            p.lm = '2400';
            break;
          case '2085':
            p.lm = '2470';
            break;
          case '1700':
            p.lm = '2100';
            break;
        }

        return p
      }
    }
  ]
}


process.execute(transform1)
  .then(function (err) { return process.execute(transform2) })
  .then(function (err) { return process.execute(transform3) })
  .then(function(err) { console.log("finito!!"); })
    .catch(function(err) { console.log("fallito!!", err); });