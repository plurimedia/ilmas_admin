var process = require('./massive.js');
var modello = ['56b8aaa90de23c95600bd2b7', '56b8aaa90de23c95600bd2a4', '56b8aaa90de23c95600bd2b2', 
'56b8aaa90de23c95600bd2aa', '56b8aaa90de23c95600bd2bf', '56b8aaa90de23c95600bd2c0', 
'56b8aaa90de23c95600bd2b8', '56b8aaa90de23c95600bd2ab', '56b8aaa90de23c95600bd2a5', 
'56b8aaa90de23c95600bd2ac', '56b8aaa90de23c95600bd2b9'];
var query = { modello : { $in: modello } };
var tag = 'dani_20221028_07';

var transform1 = {
  idx: 1,
  tag: tag,
  query: query,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['E'], pos: 6 },
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        p = process.archiviaConNote(p, 'IN40')

        return p
      }
    }
  ]
}

var transform2 = {
  idx: 2,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['D'], pos: 6 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'S', 6)
        p.luci = 2
        p.w = '28'
        p.ma = '800'

        switch (p.lm) {
          case '6220':
            p.lm = '7030';
            break;
          case '6540':
            p.lm = '7390';
            break;
          case '6880':
            p.lm = '7780';
            break;
          case '5520':
            p.lm = '6220';
            break;
          case '5670':
            p.lm = '6390';
            break;
          case '4800':
            p.lm = '5600';
            break;
          case '9330':
            p.lm = '10545';
            break;
          case '9810':
            p.lm = '11085';
            break;
          case '10320':
            p.lm = '11670';
            break;
          case '8280':
            p.lm = '9330';
            break;
          case '8505':
            p.lm = '9585';
            break;
          case '7200':
            p.lm = '8400';
            break;
          case '12440':
            p.lm = '14060';
            break;
          case '13080':
            p.lm = '14780';
            break;
          case '13760':
            p.lm = '15560';
            break;
          case '11040':
            p.lm = '12440';
            break;
          case '11340':
            p.lm = '12780';
            break;
          case '9690':
            p.lm = '11200';
            break;
        }

        return p
      }
    }
  ]
}

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .catch(function(err) { console.log("fallito!!", err); });