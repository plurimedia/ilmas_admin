var process = require('./massive.js');
// var modello1 = ['5743f1d166923a030050afd0'] // uso solo il modello2, ho trasferito il resto sulla regola 08a, ottava trasformazione
var modello2 = [,'573436d4c0f52e030063bb3d']
var query = { modello : { $in: modello2 } };
var tag = 'dani_20221028_04';

var transform1 = {
  idx: 1,
  tag: tag,
  query: query,
  preProcess: 'getFromProd',
  checkDuplicates: true,
  filter: [process.filters.hasCharAt],
  filterArguments: [{ chars: ['D'], pos: 9 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.dali(p, 9)
        p.alimentatore_incluso = true

        return p
      }
    }
  ]
}

process.execute(transform1)
  .then(function(err) { console.log("finito!!"); })
  .catch(function(err) { console.log("fallito!!", err); });