var CONFIG = require(process.cwd() + '/server/config.js'),
    q = require('q'),
    ObjectId = require('mongodb').ObjectID,
    dbTestConn = CONFIG.getConfiguration('test').MONGO,
    dbProdConn = CONFIG.getConfiguration('production').MONGO,
    dbTest,
    dbProd,
    dbLocal,
    modello// = '5743f1d166923a030050afd0',
    //query = {modello:ObjectId(modello)}
    ;

const connect = async () => {
    /* return new Promise(function(resolve, reject){
        MongoClient.connect(conn_dbTest)
            .then(function(c) {
                console.log('connesso a test');
                dbTest = c;
                MongoClient.connect(conn_dbProd)
                    .then(function(c2) {
                        console.log('connesso a prod');
                        dbProd = c2;
                        resolve();
                    })
                    .catch(function (err) { console.log('errore connessione a prod', err); reject(err); });;
            })
            .catch(function (err) { console.log('errore connessione a test', err); reject(err); });
    }); */
    const getConnections = async () => {
        const MongoClient = require('mongodb').MongoClient
    
        const clientTest = new MongoClient(dbTestConn)
        let dbTest = await clientTest.connect(dbTestConn)
    
        const clientProd = new MongoClient(dbProdConn)
        let dbProd = await clientProd.connect(dbProdConn)
    
        const clientLocal = new MongoClient('mongodb://@localhost/ilmas_admin')
        let dbLocal = await clientProd.connect('mongodb://@localhost/ilmas_admin')
    
        return { dbTest, dbProd, dbLocal }
    }

    ({ dbTest, dbProd, dbLocal } = await getConnections())
}

const disconnect = async () => {
    if (dbProd) {
        dbProd.close(function() {
            console.log('chiudo connessione prod');
        });
    }
    if (dbTest) {
        dbTest.close(function() {
            console.log('chiudo connessione test');
        });
    }
    if (dbLocal) {
        dbLocal.close(function() {
            console.log('chiudo connessione local');
        });
    }
    /* return new Promise(function(resolve, reject) {
        if (dbTest) {
            dbTest.close(function() {
                console.log('chiudo connessione test');
                if (dbProd) {
                    dbProd.close(function() {
                        console.log('chiudo connessione prod');
                        resolve();
                    });
                }
            });
        } else {
            if (dbProd) {
                dbProd.close(function () {
                    console.log('chiudo connessione prod');
                    resolve();
                });
            }
        }
    }); */
}

function deleteProdottiTest (query) {
    /* let query = {modello:ObjectId(modello)}; */
    return new Promise(function(resolve, reject) {
        dbTest.collection('Prodotto').remove(query)
            .then(function(obj) { console.log(obj.result.n + " document(s) deleted"); resolve(); })
            .catch(function(err) { reject(err); });
    });
}

function deleteProdottiProd (query) {
    /* let query = {modello:ObjectId(modello)}; */
    return new Promise(function(resolve, reject) {
        dbProd.collection('Prodotto').remove(query)
            .then(function(obj) { console.log(obj.result.n + " document(s) deleted"); resolve(); })
            .catch(function(err) { reject(err); });
    });
}

function deleteProdottiLocal (query) {
    /* let query = {modello:ObjectId(modello)}; */
    return new Promise(function(resolve, reject) {
        dbLocal.collection('Prodotto').remove(query)
            .then(function(obj) { console.log(obj.result.n + " document(s) deleted"); resolve(); })
            .catch(function(err) { reject(err); });
    });
}

function deleteProdottiBackupTest (query) {
    return new Promise(function(resolve, reject) {
        dbTest.collection('Prodotto_bck').remove(query)
            .then(function(obj) { console.log(obj.result.n + " document(s) deleted"); resolve(); })
            .catch(function(err) { reject(err); });
    });
}

function deleteProdottiBackupProd (query) {
    return new Promise(function(resolve, reject) {
        dbProd.collection('Prodotto_bck').remove(query)
            .then(function(obj) { console.log(obj.result.n + " document(s) deleted"); resolve(); })
            .catch(function(err) { reject(err); });
    });
}

function importFromProd (query) {
    /* let query = {modello:ObjectId(modello)}; */
    return new Promise(function(resolve, reject) {
        dbProd.collection('Prodotto').find(query)
            .toArray(function(err, res) {
                if (err) reject(err);
                else {
                    console.log('risultati '+res.length, query);
                    dbTest.collection('Prodotto').insertMany(res, function(err, risultato) {
                        if (err) reject(err);

                        if (risultato && risultato.insertedCount)
                            console.log('inseriti', risultato.insertedCount);
                        else
                            console.log('no risultato.........');

                        resolve();
                    });
                }
            })
    });
}

function backupTestOnLocalTable (query) {
    /* let query = {modello:ObjectId(modello)}; */
    return new Promise(function(resolve, reject) {
        dbTest.collection('Prodotto').find(query)
            .toArray(function(err, res) {
                if (err) reject(err);
                console.log('risultati '+res.length, query);

                dbLocal.collection('Prodotto').insertMany(res, function(err, risultato) {
                    if (err) reject(err);

                    console.log('inseriti', risultato.insertedCount);
                    resolve();
                });

            });
    });
}

function backupProdOnLocalTable (query) {
    /* let query = {modello:ObjectId(modello)}; */
    return new Promise(function(resolve, reject) {
        dbProd.collection('Prodotto').find(query)
            .toArray(function(err, res) {
                if (err) reject(err);
                console.log('risultati '+res.length, query);

                dbLocal.collection('Prodotto').insertMany(res, function(err, risultato) {
                    if (err) reject(err);

                    console.log('inseriti', risultato.insertedCount);
                    resolve();
                });

            });
    });
}

function restoreTestFromLocalTable (query) {
    /* let query = {modello:ObjectId(modello)}; */
    return new Promise(function(resolve, reject) {
        dbLocal.collection('Prodotto').find(query)
            .toArray(function(err, res) {
                if (err) reject(err);
                console.log('risultati '+res.length, query);

                dbTest.collection('Prodotto').insertMany(res, function(err, risultato) {
                    if (err) reject(err);

                    console.log('inseriti', risultato.insertedCount);
                    resolve();
                });

            });
    });
}

function restoreProdFromLocalTable (query) {
    /* let query = {modello:ObjectId(modello)}; */
    return new Promise(function(resolve, reject) {
        dbLocal.collection('Prodotto').find(query)
            .toArray(function(err, res) {
                if (err) reject(err);
                console.log('risultati '+res.length, query);

                dbProd.collection('Prodotto').insertMany(res, function(err, risultato) {
                    if (err) reject(err);

                    console.log('inseriti', risultato.insertedCount);
                    resolve();
                });

            });
    });
}

//exports.go = function (q) { //q = query di estrazione e cancellazione
    //TEST
    //caso d'uso 1 : cancello in test e importo da prod
    //connect().then(deleteProdottiTest).then(importFromProd).then(disconnect);
    //caso d'uso 2 : cancello in test, importo da prod, backuppo test
    //connect().then(deleteProdottiTest).then(importFromProd).then(backupTestOnLocalTable).then(disconnect);
    //caso d'uso 3 : cancello in test, restore test
    //connect().then(deleteProdottiTest).then(restoreTestFromLocalTable).then(disconnect);

    //PROD
    //caso d'uso 4 : backup in prod
    //connect().then(backupProdOnLocalTable).then(disconnect);
    //caso d'uso 5 : restore in prod TODO forse qui ci sarebbe anche da mettere la cancellazione prima
    //connect().then(restoreProdFromLocalTable).then(disconnect);
//}

exports.caso1 = async (q) => {
    await connect()
    await deleteProdottiTest(q)
    await importFromProd(q)
    await disconnect()
}

exports.caso2 = async (q) => {
    await connect()
    await deleteProdottiTest(q)
    await importFromProd(q)
    await backupTestOnLocalTable(q)
    await disconnect()
}

exports.caso3 = async (q) => {
    await connect()
    await deleteProdottiTest(q)
    await restoreTestFromLocalTable(q)
    await disconnect()
}

exports.caso4 = async (q) => {
    await connect()
    await deleteProdottiLocal(q)
    await backupProdOnLocalTable(q)
    await disconnect()
}

// cancella i prodotti in locale e copia da locale in prod
exports.caso5 = async (q) => {
    await connect()
    await deleteProdottiProd(q)
    await restoreProdFromLocalTable(q)
    await disconnect()
}

// cancella i prodotti in locale e copia da test in locale
exports.caso6 = async (q) => {
    await connect()
    await deleteProdottiLocal(q)
    await backupTestOnLocalTable(q)
    await disconnect()
}
