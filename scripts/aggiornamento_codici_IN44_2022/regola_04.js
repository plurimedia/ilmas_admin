const process = require('./massive.js');

const filtro = ['0611 Bob Std Led', '1611 Bob Std Sc Led', '4714 Tech Led', '1063 Dial 4 Led', '1004 Marquez Led']

var query = { modello: { $in: [] } };
var tag = 'lorenzo_2022_regola04';

//regole: [3.1, 3.2, 3.3]
var transform1 = {
  idx: 1,
  tag: tag,
  // query: query,
  modelli: filtro,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['Z', '7', '8', 'O', 'S', 'T'], pos: 5 }, //pos in base 1
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {

        var upd = {};

        switch (p.lm) {
          case '6500': upd.lm = '6460';
            break;
          case '6660': upd.lm = '6800';
            break;
          case '9300': upd.lm = '9560';
            break;
          case '9960': upd.lm = '10040';
            break;
          case '12340': upd.lm = '12440';
            break;
          case '12640': upd.lm = '13080';
            break;
          case '13560': upd.lm = '13760';
            break;
          case '13920': upd.lm = '14060';
            break;
          case '14260': upd.lm = '14780';
            break;
          case '15280': upd.lm = '15560';
            break;
          case '15440': upd.lm = '15640';
            break;
          case '15820': upd.lm = '16440';
            break;
          case '16960': upd.lm = '17280';
            break;
        }

        upd.generazione = 'D3';

        /* if (p.codice.charAt(4) === 'R' || '5' || '6' || 'I' || 'A' || 'B') {

          upd.exportMetel = false;
          upd.pubblicato = false;

        } */

        return upd;
      }
    }
  ]
};

var transform2 = {
  idx: 2,
  tag: tag,
  modelli: filtro,
  filter: [process.filters.hasCharAt,process.filters.hasCharAt,process.filters.hasCharAt],
  filterArguments: [{ chars: ['O', 'S', 'T'], pos: 5 },{ chars: ['D', 'E'], pos: 6 },{ chars: ['E'], pos: 9 }],
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              var upd = {};
              upd.prezzo = p.prezzo - 10

              return upd;
          }
      }
  ]
};

var transform3 = {
  idx: 3,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['R', '5', '6', 'I', 'A', 'B'], pos: 5 },
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              var upd = {};
              upd.pubblicato = false
              upd.exportMetel = false

              return upd;
          }
      }
  ]
};

//regola 3.4
var transform4 = {
  idx: 4,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['W', 'Y'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.vivid(p)
        p.generazione = 'D3'

        switch (p.lm) {
          case '6120': p.lm = '5840';
            break;
          case '8560': p.lm = '8120';
            break;
          case '11640': p.lm = '11040';
            break;
          case '13120':
            p.lm = '12440';
            break;
          case '14560':
            p.lm = '13800';
            break;
        }

        return p;
      }
    }
  ]
};

//regola 2.5
var transform5 = {
  idx: 5,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasSottoCategoria, process.filters.hasCharAt],
  filterArguments: [{ sottocategoria: 'Vivid' }, { chars: ['X'], pos: 5 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'D', 5)
        p.generazione = 'D3'

        switch (p.lm) {
          case '5840': p.lm = '6000';
            break;
          case '8120': p.lm = '10040';
            break;
          case '11040': p.lm = '11340';
            break;
          case '12440':
            p.lm = '12780';
            break;
          case '13800':
            p.lm = '14160';
            break;
        }

        return p;
      }
    }
  ]
};


// regola 2.6
var transform6 = {
  idx: 6,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt],
  filterArguments: [{ chars: ['7', 'S'], pos: 5 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.sunlike(p)
        p.generazione = 'D3'

        switch (p.lm) {
          case '6800':
            p.lm = '4800';
            break;
          case '9560':
            p.lm = '6800';
            break;
          case '13080':
            p.lm = '9690';
            break;
          case '14780': 
            p.lm = '11200';
            break;
          case '16440':
            p.lm = '12800';
            break;
        }

        return p;
      }
    }
  ]
};

//regola 2.7
var transform7 = {
  idx: 7,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['W', 'Q'], pos: 5 },
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        p = process.archiviaConNote(p, 'IN40')

        return p
      }
    }
  ]
};

var transform8 = {
  idx: 8,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: [process.filters.hasCharAt],
  filterArguments: [{ chars: ['D'], pos: [6, 7] }],
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        const upd = {}
        upd.w = '24'
        upd.luci = 4

        return upd
      }
    }
  ]
};

var transform9 = {
  idx: 9,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: [process.filters.hasCharAt],
  filterArguments: [{ chars: ['E'], pos: [6, 7] }],
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        if (process.hasCharsAt(p.codice, '1004', 1)) {
          p.w = '32'
          p.luci = 4
        } else {
          p.w = '28'
          p.luci = 4
        }

        return p
      }
    }
  ]
};

var transform10 = {
  idx: 10,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['D'], pos: [9, 10] },
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              p.alimentatore = 'Driver Dimmerabile Push N/A'
              p.dimmerabile = true

              return p
          }
      }
  ]
};

/* var transform11 = {
  idx: 11,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['D'], pos: 10 },
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              p.alimentatore = 'Driver Dimmerabile Push N/A'
              p.dimmerabile = true

              return p
          }
      }
  ]
}; */

process.execute(transform1)
  .then(function (err) { return process.execute(transform2) })
  .then(function (err) { return process.execute(transform3) })
  .then(function (err) { return process.execute(transform4) })
  .then(function (err) { return process.execute(transform5) })
  .then(function (err) { return process.execute(transform6) })
  .then(function (err) { return process.execute(transform7) })
  .then(function (err) { return process.execute(transform8) })
  .then(function (err) { return process.execute(transform9) })
  .then(function (err) { return process.execute(transform10) })
  .then(function (err) { console.log('finito!!'); })
  .catch(function (err) { console.log('fallito!!', err); });

/*
Per i modelli:
'0611 Bob Std Led', '1611 Bob Std Sc Led', '4714 Tech Led', '1063 Dial 4 Led', '1004 Marquez Led'




1. Tutti i codici con 5° carattere = 'Z/7/8/O/S/T' cambio lumen del campo 'Flusso lum. nom.':

-6500 --> 6460
-6660 --> 6800
-9300 --> 9560
-9960 --> 10040
-12340 --> 12440
-12640 --> 13080
-13560 --> 13760
-13920 --> 14060
-14260 --> 14780
-15280 --> 15560
-15440 --> 15640
-15820 --> 16440
-16960 --> 17280





2. Per tutti i codici modificati --> Campo 'Generazione' = 'D3' (Da creare nel menu a tendina)

3. Tutti i codici con il 5° carattere = 'R/5/6/I/A/B' --> flag metel = false e flag sito = false.

4. Duplicare tutti i codici con 5° carattere = 'W/Y' :
   -5° carattere --> 'X'
   -il campo 'Temp. di colore' --> '3300'
   -il campo 'CRI' --> '95'
   -il campo 'Sottocategoria' --> 'Vivid'
   -il campo 'Flusso lum. nom.': 
     
-6120 --> 5840
-8560 --> 8120
-11640 --> 11040
-13120 --> 12440
-14560 --> 13800




5. Duplicare tutti i codici creati al punto 4:
   -5° carattere --> 'D'
   -il campo 'Temp. di colore' --> '3500'
   -il campo 'Flusso lum. nom.': 
     
-5840 --> 6000
-8120 --> 10040
-11040 --> 11340
-12440 --> 12780
-13800 --> 14160


6. Duplicare tutti i codici con 5° carattere = '7/S':
   -5° carattere '7/S' --> 'SS'
   -il campo 'CRI' --> '97,6'
   -il campo 'Sottocategoria' --> 'Sunlike'
   -il campo 'Flusso lum. nom.': 
      
-6800 --> 4800
-9560 --> 6800
-13080 --> 9690
-14780 --> 11200
-16440 --> 12800


7. Tutti i codici con 5° carattere = 'W/Q':
   mettere in archivio --> 'Generazione' = 'IN40' (Da creare nel menu a tendina)
   flag metel = false e flag sito = false.





*/


