var process = require('./massive.js');
var modello = ['59afc1c5dd826e040027a57b', '5acf2547504c990400f042b8'];
var query = { modello : { $in: modello } };
var tag = 'dani_20221028_07';

var transform1 = {
  idx: 1,
  tag: tag,
  query: query,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['S'], pos: 6 },
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        p = process.archiviaConNote(p, 'IN40')

        return p
      }
    }
  ]
}

var transform2 = {
  idx: 2,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['D'], pos: 6 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'E', 6)
        p.w = '32'
        p.ma = '900'

        switch (p.lm) {
          case '3110':
            p.lm = '3910';
            break;
          case '3270':
            p.lm = '4110';
            break;
          case '3440':
            p.lm = '4320';
            break;
          case '2760':
            p.lm = '3450';
            break;
          case '2835':
            p.lm = '3540';
            break;
          case '2400':
            p.lm = '3200';
            break;
        }

        return p
      }
    }
  ]
}

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .catch(function(err) { console.log("fallito!!", err); });