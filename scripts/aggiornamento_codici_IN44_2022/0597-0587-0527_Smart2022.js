var process = require('./massive.js');
var modello = ['59b000f3d9bffb040079b514', '5b36052a7e73a20400684c66', '5b2b567f694a5804009cae4f'];
var query = { modello: { $in: modello } };
var tag = 'dani_20221027_05';

var transform1 = {
    idx: 1,
    tag: tag,
    query: query,
    preProcess: 'getFromProd',
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['Z', '7', '8'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p.generazione = 'D3'

                // console.log(p.codice)
                if (process.hasCharAt(p.codice, 'Z', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'O', 5)
                }
                if (process.hasCharAt(p.codice, '7', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'S', 5)
                }
                if (process.hasCharAt(p.codice, '8', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'T', 5)
                }
                // console.log(p.codice)

                switch (p.lm) {
                    case '1460':
                        p.lm = '1615'
                        break
                    case '1535':
                        if (process.hasCharsAt(p.codice, '05978', 1)) p.lm = '1785'; else p.lm = '1700'
                        break
                    case '1610':
                        p.lm = '1785'
                        break
                    case '2020':
                        p.lm = '2270'
                        break
                    case '2125':
                        if (process.hasCharsAt(p.codice, '05978', 1)) p.lm = '2510'; else p.lm = '2390'
                        break
                    case '2235':
                        p.lm = '2510'
                        break
                    case '2710':
                        if (process.hasCharsAt(p.codice, '0587', 1)) p.lm = '3110'; else p.lm = '4010'
                        break
                    case '2860':
                        p.lm = '3270'
                        break
                    case '3000':
                        p.lm = '3440'
                        break
                    case '2375':
                        p.lm = '2695'
                        break
                    case '2500':
                        p.lm = '2835'
                        break
                    case '2626':
                        p.lm = '2980'
                        break
                }

                if (process.hasCharsAt(p.codice, '0527', 1) && process.hasCharAt(p.codice, 'E', 9)) p.prezzo -= 10 
                if (process.hasCharsAt(p.codice, '0527', 1) && (process.hasCharAt(p.codice, 'L', 9) || process.hasCharAt(p.codice, 'D', 9))) p.prezzo -= 1 

                return p
            }
        }
    ]
};

var transform2 = {
    idx: 2,
    tag: tag,
    query: query,
    filter: [process.filters.hasCharAt,process.filters.hasCharAt,process.filters.hasCharAt],
    filterArguments: [{ chars: ['O', 'S', 'T'], pos: 5 },{ chars: ['D'], pos: 6 },{ chars: ['E'], pos: 9 }],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var upd = {};
                upd.prezzo = p.prezzo - 10

                return upd;
            }
        }
    ]
};

var transform3 = {
    idx: 3,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['R', '5', '6', 'Z', '7', '8'], pos: 5 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var upd = {};
                upd.pubblicato = false
                upd.exportMetel = false

                return upd;
            }
        }
    ]
};

var transform4 = {
    idx: 4,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['W'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.vivid(p)
                p.generazione = 'D3'

                if (process.hasCharAt(p.codice, 'D', 6)) {
                    p.w = '24'
                }

                if (p.codice.lentgh <= 8) {
                    p.prezzo -= 11
                }
                if ((process.hasCharAt(p.codice, 'L', 9) || process.hasCharAt(p.codice, 'D', 9))) p.prezzo -= 11
                if (process.hasCharsAt(p.codice, '0587', 1) && process.hasCharAt(p.codice, 'E', 9)) p.prezzo -= 12 
                if (process.hasCharsAt(p.codice, '0597', 1) && process.hasCharAt(p.codice, 'E', 9)) p.prezzo -= 11 
                if (process.hasCharsAt(p.codice, '0527', 1) && process.hasCharAt(p.codice, 'E', 9)) p.prezzo -= 11 

                switch (p.lm) {
                    case '1530':
                        p.lm = '1460'
                        break
                    case '2140':
                        p.lm = '2030'
                        break
                    case '2910':
                        p.lm = '2760'
                        break
                    case '2530':
                        p.lm = '2400'
                        break
                }

                return p;
            }
        }
    ]
};

var transform5 = {
    idx: 5,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasSottoCategoria, process.filters.hasCharAt],
    filterArguments: [{ sottocategoria: 'Vivid' }, { chars: ['X'], pos: 5 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 5)
                p.k = '3500'
                p.generazione = 'D3'

                switch (p.lm) {
                    case '1460':
                        p.lm = '1500'
                        break
                    case '2030':
                        p.lm = '2085'
                        break
                    case '2760':
                        p.lm = '2835'
                        break
                    case '2400':
                        p.lm = '2470'
                        break
                }

                return p;
            }
        }
    ]
};

var transform6 = {
    idx: 6,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['S'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.sunlike(p)
                p.generazione = 'D3'

                if (p.codice.length <= 9) {
                    console.log('cambio prezzo max 9 caratteri', p.codice)
                    p.prezzo += 33
                }
                if (process.hasCharAt(p.codice, 'D', 10) || process.hasCharAt(p.codice, 'L', 10)) {
                    p.prezzo += 33
                }
                if (process.hasCharAt(p.codice, 'E', 10)) p.prezzo += 34

                switch (p.lm) {
                    case '1700':
                        p.lm = '1200'
                        break
                    case '2390':
                        p.lm = '1700'
                        break
                    case '3270':
                        p.lm = '2400'
                        break
                    case '2835':
                        p.lm = '2100'
                        break
                }

                return p;
            }
        }
    ]
};

var transform7 = {
    idx: 7,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['W', 'Q'], pos: 5 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var upd = {}
                upd.generazione = 'IN40'
                upd.pubblicato = false
                upd.exportMetel = false

                return upd
            }
        }
    ]
};

var transform8 = {
    idx: 8,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['D'], pos: 9 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.alimentatore = 'Driver Dimmerabile Push N/A'
                p.dimmerabile = true

                return p
            }
        }
    ]
};

var transform9 = {
    idx: 9,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['D'], pos: 10 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.alimentatore = 'Driver Dimmerabile Push N/A'
                p.dimmerabile = true

                return p
            }
        }
    ]
};

var transform10 = {
    idx: 10,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['L'], pos: 9 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p = process.casambi(p, 9)
                p.prezzo += 53

                return p
            }
        }
    ]
};

var transform11 = {
    idx: 11,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['L'], pos: 10 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p = process.casambi(p, 10)
                p.prezzo += 53

                return p
            }
        }
    ]
};

var transform12 = {
    idx: 12,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt, process.filters.hasCharAt],
    filterArguments: [{ chars: ['1'], pos: 8 }, { chars: ['0597'], pos: 1 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.nero(p)

                return p
            }
        }
    ]
};

var transform13 = {
    idx: 13,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt, process.filters.hasCharAt],
    filterArguments: [{ chars: ['1'], pos: 8 }, { chars: ['0587'], pos: 1 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.nero(p)

                return p
            }
        }
    ]
};

var transform14 = {
    idx: 14,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt, process.filters.hasCharAt],
    filterArguments: [{ chars: ['1'], pos: 9 }, { chars: ['0597'], pos: 1 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.nero(p, 9)

                return p
            }
        }
    ]
};

var transform15 = {
    idx: 15,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt, process.filters.hasCharAt],
    filterArguments: [{ chars: ['1'], pos: 9 }, { chars: ['0587'], pos: 1 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.nero(p, 9)

                return p
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function (err) { return process.execute(transform8) })
    .then(function (err) { return process.execute(transform9) })
    .then(function (err) { return process.execute(transform10) })
    .then(function (err) { return process.execute(transform11) })
    .then(function (err) { return process.execute(transform12) })
    .then(function (err) { return process.execute(transform13) })
    .then(function (err) { return process.execute(transform14) })
    .then(function (err) { return process.execute(transform15) })
    .then(function (err) { console.log("finito!!"); })
    .catch(function () { console.log("fallito!!"); });


/*
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto0597.json --query='{"modello":{"$oid":"59b000f3d9bffb040079b514"}}'
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto0587.json --query='{"modello":{"$oid":"5b36052a7e73a20400684c66"}}'
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto0527.json --query='{"modello":{"$oid":"5b2b567f694a5804009cae4f"}}'

db.Prodotto.remove({modello:{$in:[ObjectId('59b000f3d9bffb040079b514'),ObjectId('5b36052a7e73a20400684c66'),ObjectId('5b2b567f694a5804009cae4f')]}})

mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto0597.json
mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto0587.json
mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto0527.json


Per i modelli:
"0597 Smart M", "0587 Smart Q M", "0527 Smart A/ M"


1. Duplicare tutti i codici con 5° carattere = "Z/7/8": 
-il campo "Flusso lum. nom.":
-1460 --> 1615
-1535 --> 1700
-1610 --> 1785
-2020 --> 2270
-2125 --> 2390
-2235 --> 2510
-2710 --> 4010
-2860 --> 3270
-3000 --> 3440
-2375 --> 2695
-2500 --> 2835
-2626 --> 2980
-Se campo "Flusso Lum. Nom." = "2710" and 4 primi caratteri = 0587 --> 3110



-il campo "Generazione" = "D3" (Da creare nel menu a tendina)
-il 5° carattere:
if = Z --> O
if = 7 --> S
if = 8 --> T

3. Tutti i codici con il 5° carattere = "R/5/6/Z/7/8" --> flag metel = false e flag sito = false.

4. Duplicare tutti i codici con 5° carattere = "W" :
   -5° carattere --> "X"
   -il campo "Temp. di colore" --> "3300"
   -il campo "CRI" --> "95"
   -il campo "Sottocategoria" --> "Vivid"
   -il campo "Flusso lum. nom.": 
   1530 --> 1460
   2140 --> 2030
   2910 --> 2760
   2530 --> 2400

   

5. Duplicare tutti i codici creati al punto 4:
   -5° carattere --> "D"
   -il campo "Temp. di colore" --> "3500"
   -il campo "Flusso lum. nom.": 
    1460 --> 1500 
    2030 --> 2085
    2760 --> 2835
    2400 --> 2470



6. Duplicare tutti i codici con 5° carattere = "S":
   -5° carattere "S" --> "SS"
   -il campo "CRI" --> "97,6"
   -il campo "Sottocategoria" --> "Sunlike"
   -il campo "Flusso lum. nom.": 
     1700 --> 1200
     2390 --> 1700
     3270 --> 2400 
     2835 --> 2100


7. Tutti i codici con 5° carattere = "W/Q":
   mettere in archivio --> "Generazione" = "IN40" (Da creare nel menu a tendina)
   flag metel = false e flag sito = false.

8.Solo per i modelli:
"0597 Smart M", "0587 Smart Q M"

Duplicare tutti i codici con 8° carattere = "1"
-8° carattere --> "2"
-il campo "colore" --> "Nero"







 */