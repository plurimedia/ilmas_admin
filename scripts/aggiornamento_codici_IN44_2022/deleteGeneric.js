

var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    CategoriaSchema = require(process.cwd() + '/server/models/categoria'),
    ModelloSchema = require(process.cwd() + '/server/models/modello'),
    ProdottoSchema = require(process.cwd() + '/server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    Modello = mongoose.model('Modello'),
    Categoria = mongoose.model('Categoria'),
    fs = require('fs'),
    logGlobale="",
    aggiornati = 0;


exports.execute = function (prodotti) { 

mongoose.connect(db);
 

mongoose.connection.once('open', function() {

    console.log(" Start batch generic delete ");

    var bulk = Prodotto.collection.initializeOrderedBulkOp();
    async.each(prodotti, 
        function (prodotto, cb) {

//if (prodotto.codice.length==8 || prodotto.codice.length==9){
             bulk.find( { codice: prodotto.codice } ).remove() 

            console.log(aggiornati,   prodotto.codice, prodotto.codice.length)

            aggiornati ++;
//}
            cb();
             
        }, 
        function (err) 
        {
        	if(aggiornati > 0 ) {
                 bulk.execute(
                    function(err, result){
                    if(err)
                        throw err; 
                        console.log('finito, chiudo la connessione, eliminati ' + aggiornati + ' nuovi codici')
                        mongoose.connection.close();

                        fs.writeFile(".tmp/f_050.txt",

                         logGlobale,

                         function(err) {
                            if(err) {
                                console.log("Log was not saved!", err);
                                throw err;
                            }
                            else
                                console.log("Log was saved!");

                            return;
                        }
                    );

                    }
                )
            }
            else {
                mongoose.connection.close();
                console.log("connection close");
            }
        }
    )
    
}



);
}
