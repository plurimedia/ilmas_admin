const process = require('./massive.js');

const filtro1 = ['2013 KS/3', '2028 KL/3', '0610 Bob Std Led', '1710 Bob Ext Led']
const filtro2 = ['1610 Bob Std Sc Led', '1740 Bob Ext Sc Led', '0910 Bob Slim', '1714 Bob Eco Led']
const filtro3 = ['1707 Mini Bob', '0710 Smart C/S', '2024 KS3/P', '4253 Magic 2 Wall S']
const filtro4 = ['4533 Orion', '4623 Fold Led', '4713 Tech Led', '0777 Nuvola Led', '1062 Dial 3 Led', '1003 Marquez Led']

const filtro = filtro4
var tag = 'lorenzo_2022_regola03';

const filtro_4380lm = ['1707', '0710']

//regole: [3.1, 3.2, 3.3]
var transform1 = {
  idx: 1,
  tag: tag,
  // query: query,
  modelli: filtro,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['Z', '7', '8', 'O', 'S', 'T'], pos: 5 }, //pos in base 1
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {

        var upd = {};

        switch (p.lm) {
          case '4050': upd.lm = '4620';
            break;
          case '4245': upd.lm = '4860';
            break;
          case '4485': upd.lm = '5100';
            break;
          case '6975': upd.lm = '7170';
            break;
          case '7470': upd.lm = '7530';
            break;
          case '4875': upd.lm = '4845';
            break;
          case '4995': upd.lm = '5100';
            break;
          case '9255': upd.lm = '9330';
            break;
          case '9480': upd.lm = '9810';
            break;
          case '10170': upd.lm = '10320';
            break;
          case '10440': upd.lm = '10545';
            break;
          case '10695': upd.lm = '11085';
            break;
          case '11460': upd.lm = '11670';
            break;
          case '4020': upd.lm = '4845';
            break;
          case '4605': upd.lm = '5100';
            break;
          case '5640': upd.lm = '5355';
            break;
          case '4380': upd.lm = '5845';
            break;
          case '4830': upd.lm = '5355';
            break;
          case '6060': upd.lm = '6810';
            break;
          case '6375': upd.lm = '7170';
            break;
          case '6705': upd.lm = '7530';
            break;
          case '3030': upd.lm = '3275';
            break;
          case '3180': upd.lm = '3555';
            break;
          case '3360': upd.lm = '3735';
            break;
          case '11580': upd.lm = '11730';
            break;
          case '11865': upd.lm = '12330';
            break;
          case '12720': upd.lm = '12960';
            break;
        }

        upd.generazione = 'D3';

        if (process.hasCharAt(p.codice, 'O', 5) || process.hasCharAt(p.codice, 'S', 5) || process.hasCharAt(p.codice, 'T', 5)) {
          if (process.hasCharAt(p.codice, 'D', 6)) {
            upd.luci = 3
            upd.w = '24'
          }
          if (process.hasCharAt(p.codice, 'B', 6) || process.hasCharAt(p.codice, 'C', 6) || process.hasCharAt(p.codice, 'D', 6) || process.hasCharAt(p.codice, 'S', 6)) {
            if (process.hasCharAt(p.codice, 'D', 9) || process.hasCharAt(p.codice, 'L', 9)) {
              upd.prezzo = p.prezzo - 45
            }
          }
          if ((process.hasCharAt(p.codice, 'D', 6) || process.hasCharAt(p.codice, 'S', 6)) && p.codice.length <= 8) {
            upd.prezzo = p.prezzo + 270
          }
        }

        return upd;
      }
    }
  ]
};

// questa era la regola 08j
var transform2 = {
  idx: 2,
  tag: tag,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt, process.filters.hasCharAt],
  filterArguments: [{ chars: ['D'], pos: 6 }, { chars: ['0777'], pos: 1 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'S', 6)
        p.luci = 3
        p.w = '28'
        p.ma = '800'

        switch (p.lm) {
          case '9330':
            p.lm = '10545';
            break;
          case '9810':
            p.lm = '11085';
            break;
          case '10320':
            p.lm = '11670';
            break;
          case '8280':
            p.lm = '9330';
            break;
          case '8505':
            p.lm = '9585';
            break;
          case '7200':
            p.lm = '8400';
            break;
        }

        return p
      }
    }
  ]
}

var transform3 = {
  idx: 3,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['R', '5', '6', 'I', 'A', 'B'], pos: 5 },
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        var upd = {};
        upd.pubblicato = false
        upd.exportMetel = false

        return upd;
      }
    }
  ]
};

//regola 3.4
var transform4 = {
  idx: 4,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['W', 'Y'], pos: 5 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.vivid(p)
        p.generazione = 'D3'

        switch (p.lm) {
          case '4590': p.lm = '4380';
            break;
          case '6420': p.lm = '6090';
            break;
          case '8730': p.lm = '8280';
            break;
          case '9840':
            p.lm = '9330';
            break;
          case '2895':
            p.lm = '3000';
            break;
          case '10920':
            p.lm = '10350';
            break;
        }

        if (process.hasCharAt(p.codice, 'X', 5) && process.hasCharAt(p.codice, 'D', 6)) {
          p.w = '24'
          p.luci = 2
        }
        return p;
      }
    }
  ]
};

//regola 2.5
var transform5 = {
  idx: 5,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt, process.filters.isModello],
  filterArguments: [{ chars: ['7'], pos: 5 }, { modello: '5b5f27d46cbb3f0400f431fc' }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.vivid(p)
        p.generazione = 'D3'

        switch (p.lm) {
          case '4605':
            p.lm = '4380';
            break;
          case '6375':
            p.lm = '6090';
            break;
        }

        return p;
      }
    }
  ]
};

//regola 2.6
var transform6 = {
  idx: 6,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasSottoCategoria, process.filters.hasCharAt],
  filterArguments: [{ sottocategoria: 'Vivid' }, { chars: ['X'], pos: 5 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'D', 5)
        p.k = '3500'
        p.generazione = 'D3'

        switch (p.lm) {
          case '4380': if (filtro_4380lm.includes(p.codice.substring(0, 3))) p.lm = '3000'; else p.lm = '4500';
            break;
          case '6090': p.lm = '7530';
            break;
          case '8280': p.lm = '8505';
            break;
          case '9330':
            p.lm = '9585';
            break;
          case '3000':
            p.lm = '3045';
            break;
          case '10350':
            p.lm = '10620';
            break;
        }

        return p;
      }
    }
  ]
};


// regola 2.7
var transform7 = {
  idx: 7,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt],
  filterArguments: [{ chars: ['7', 'S'], pos: 5 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.sunlike(p)
        p.generazione = 'D3'

        switch (p.lm) {
          case '4860':
            p.lm = '3600';
            break;
          case '7170':
            p.lm = '5100';
            break;
          case '5100':
            p.lm = '3600';
            break;
          case '9810': 
            p.lm = '7200';
            break;
          case '11085':
            p.lm = '8400';
            break;
          case '3555':
            p.lm = '2700';
            break;
          case '6090':
            p.lm = '7530';
            break;
          case '12330':
            p.lm = '9600';
            break;
        }

        return p;
      }
    }
  ]
};

var transform8 = {
  idx: 8,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasLunghezzaCodiceMax,
  filterArguments: { len: 9 },
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              var upd = {}
              upd.alimentatore = 'Driver standard'

              return upd
          }
      }
  ]
};

var transform9 = {
  idx: 9,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['D'], pos: 9 },
  transform: [
      {
          action: 'modifica_esistente',//4623DCD9D 1003ICM2D 4533WBF1D
          replace_rule: function (p) {
              var upd = {}
              upd.alimentatore = 'Driver Dimmerabile Push N/A'
              upd.dimmerabile = true

              return upd

          }
      }
  ]
};

var transform10 = {
  idx: 10,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['D'], pos: 10 },
  transform: [
      {
          action: 'modifica_esistente',//1003SSDF0D 4533SSEB0D
          replace_rule: function (p) {
              var upd = {}
              upd.alimentatore = 'Driver Dimmerabile Push N/A'
              upd.dimmerabile = true

              return upd
          }
      }
  ]
};

var transform11 = {
  idx: 11,
  tag: tag,
  // query: query,
  modelli: filtro,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['W', 'Q'], pos: 5 },
  transform: [
      {
          action: 'modifica_esistente',
          replace_rule: function (p) {
              p = process.archiviaConNote(p, 'IN40')

              return p
          }
      }
  ]
};

console.log(this)
process.execute(transform1)
  .then(function (err) { return process.execute(transform2) })
  .then(function (err) { return process.execute(transform3) })
  .then(function (err) { return process.execute(transform4) })
  .then(function (err) { return process.execute(transform5) })
  .then(function (err) { return process.execute(transform6) })
  .then(function (err) { return process.execute(transform7) })
  .then(function (err) { return process.execute(transform8) })
  .then(function (err) { return process.execute(transform9) })
  .then(function (err) { return process.execute(transform10) })
  .then(function (err) { return process.execute(transform11) })
  .then(function (err) { console.log('finito!!'); })
  .catch(function () { console.log('fallito!!'); });

/*
Per i modelli:
'2013 KS/3', '2028 KL/3', '0610 Bob Std Led', '1710 Bob Ext Led', '1610 Bob Std Sc Led', '1740 Bob Ext Sc Led',
'0910 Bob Slim', '1714 Bob Eco Led', '1707 Mini Bob', '0710 Smart C/S', '2024 KS3/P', '4253 Magic 2 Wall S',
'4533 Orion', '4623 Fold Led', '4713 Tech Led', '0777 Nuvola Led', '1062 Dial 3 Led', '1003 Marquez Led'




1. Tutti i codici con 5° carattere = 'Z/7/8/O/S/T' cambio lumen del campo 'Flusso lum. nom.':

-4050 --> 4620
-4245 --> 4860
-4485 --> 5100
-6975 --> 7170
-7470 --> 7530
-4875 --> 4845
-4995 --> 5100
-9255 --> 9330
-9480 --> 9810
-10170 --> 10320
-10440 --> 10545
-10695 --> 11085
-11460 --> 11670
-4020 --> 4845
-4605 --> 5100
-5640 --> 5355
-4380 --> 4845
-4830 --> 5355
-6060 --> 6810
-6375 --> 7170
-6705 --> 7530
-3030 --> 3275
-3180 --> 3555
-3360 --> 3735
-11580 --> 11730
-11865 --> 12330
-12720 --> 12960







2. Per tutti i codici modificati --> Campo 'Generazione' = 'D3' (Da creare nel menu a tendina)

3. Tutti i codici con il 5° carattere = 'R/5/6/I/A/B' --> flag metel = false e flag sito = false.

4. Duplicare tutti i codici con 5° carattere = 'W/Y' :
   -5° carattere --> 'X'
   -il campo 'Temp. di colore' --> '3300'
   -il campo 'CRI' --> '95'
   -il campo 'Sottocategoria' --> 'Vivid'
   -il campo 'Flusso lum. nom.': 
     
-4590 --> 4380
-6420 --> 6090
-8730 --> 8280
-9840 --> 9330
-2895 --> 3000
-10920 --> 10350

5. Per il modello '0710 Smart C/S':
   Duplicare il codice con 5° carattere = 7
   -5° carattere --> 'X'
   -il campo 'Temp. di colore' --> '3300'
   -il campo 'CRI' --> '95'
   -il campo 'Sottocategoria' --> 'Vivid'
   -il campo 'Flusso lum. nom.': 

-4605 --> 4380
-6375 --> 6090





6. Duplicare tutti i codici creati al punto 4:
   -5° carattere --> 'D'
   -il campo 'Temp. di colore' --> '3500'
   -il campo 'Flusso lum. nom.': 
     
-4380 --> 4500
-6090 --> 7530
-8280 --> 8505
-9330 --> 9585
-3000 --> 3045
-10350 --> 10620
-Se campo 'Flusso Lum. Nom.' = 4380 and 4 primi caratteri = 1707 - 0710 --> 3000


7. Duplicare tutti i codici con 5° carattere = '7/S':
   -5° carattere '7/S' --> 'SS'
   -il campo 'CRI' --> '97,6'
   -il campo 'Sottocategoria' --> 'Sunlike'
   -il campo 'Flusso lum. nom.': 
      
-4860 --> 3600
-7170 --> 5100
-5100 --> 3600
-9810 --> 7200
-11085 --> 8400
-3555 --> 2700
-6090 --> 7530
-12330 --> 9600


8. Tutti i codici con 5° carattere = 'W/Q':
   mettere in archivio --> 'Generazione' = 'IN40' (Da creare nel menu a tendina)
   flag metel = false e flag sito = false.





*/


