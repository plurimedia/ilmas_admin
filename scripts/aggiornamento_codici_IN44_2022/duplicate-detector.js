const CONFIG = require(process.cwd() + '/server/config.js'),
  mongoose = require('mongoose'),
  path = require('path'),
  CategoriaSchema = require(process.cwd() + '/server/models/categoria'),
  ModelloSchema = require(process.cwd() + '/server/models/modello'),
  ProdottoSchema = require(process.cwd() + '/server/models/prodotto'),
  Prodotto = mongoose.model('Prodotto'),
  Modello = mongoose.model('Modello'),
  Categoria = mongoose.model('Categoria')/* ,
  p = require('./massive.js') */

const mongoOptions = {
  sslValidate: true,
  tls: true,
  tlsCAFile: path.join(__dirname, "../../ibm_mongodb_ca.pem"),
  useUnifiedTopology: true,
  useNewUrlParser: true
}

const findModelli = (filtro) => {
  return new Promise((resolve, reject) => {
      /* mongoose.connect(CONFIG.MONGO, mongoOptions, function (err) {
          if (err)
              throw err;
 */
          Modello.find({ nome: { $in: filtro } }, { codice: 1 }).lean()
          .exec(function (err, modelli) {
              /* mongoose.connection.close(); */
              if (err)
                  reject(err)
              else {
                  const ret = []
                  for (const modello of modelli) {
                      ret.push(modello._id)
                  }
                  resolve(ret)
              }
          });
      /* }) */
  })
}

const conta = (codice) => {
  return new Promise((resolve, reject) => {
    Prodotto.count({ codice }).exec((err, conto) => {
      if (err) resolve(0)
      else resolve(conto)
    })
  })
}

// const modelli = ['2001 KS Track', '2002 KL Track', '2029 KXL Track', '2025 KS/A Track', '2006 KL/A Track', '2031 KXL/A Track']
// const modelli = ['2240 Smart Track XS', '1240 Over Led Ps Mini', '1241 Over Led Ps Midi', '1242 Over Led Ps Maxi', '1244 Over Led Maxi F112', '1330 Fly Mini', '1331 Fly Midi', '1332 Fly Maxi', '1334 Fly Maxi F112', '1333 FLY MINI-A', '1335 Fly Midi/A', '1336 Fly Maxi/ A', '1321 Viper Midi', '1322 Viper Maxi', '1298 Cove Pro F112', '0830 Jet M', '1233 Sun/Track L', '0595 Smart XS', '0596 Smart S']
// const modelli = ['0597 Smart M', '0598 Smart L', '0599 SMART XL', '0796 Hammer Mini 120', '0797 Hammer Midi 160', '0798 Hammer Maxi 205', '0799 Hammer Extra 230', '0585 Smart Q XS', '0786 Hammer Q Mini 120', '0787 Hammer Q Midi 160', '0788 Hammer Q Maxi 205', '0789 Hammer Q Ex Plus 230', '2011 KS/1','2026 KL/1', '0608 Bob Std Led', '1708 Bob Ext Led', '1608 Bob Std Sc Led', '1738 Bob Ext Sc Led']
// const modelli = ['0908 Bob Slim','1712 Bob Eco Led', '1888 Puzzle Led', '1704 Mini Bob', '0708 Smart C/S', '0621 Bob Std Tondo', '0526 Smart A/ S', '0527 Smart A/ M', '0528 Smart A/L', '0670 Astuto Led Mini', '0671 Astuto Led Midi', '0672 Astuto Led Maxi', '0660 Dino Led Mini', '0661 Dino Led Midi', '0662 Dino Led Maxi', '0730 Berin Led Mini', '0731 Berin Led Midi', '0732 Berin Led Maxi']
// const modelli = ['0620 Bob Eco Tondo', '2916 Cristal', '2918 Cristal 2', '2917 Cristal Q', '2003 KS/P', '2004 KL/P', '2022 KS1/P', '2017 KS/Plus P', '2020 KL/Plus P', '4898 Spring Maxi 205', '4899 Spring Extra 230', '4531 Orion', '4621 Fold Led', '4711 Tech Led', '1060 Dial 1 Led', '0775 Nuvola Led', '4251 Magic 2 Wall S', '2018 KS/Plus S', '2021 KL/Plus S', '4996 Summer Mini 120', '4997 Summer Midi 160', '4998 Summer Maxi 205', '4999 Summer Extra 230']
const modelli = ['F111 Led Module', 'F112 Led Module', '2150 Smart E / M', '0585 Smart Q XS', '0518 SMART B/L', '0517 Smart B/M', '0516 Smart B/ S', '0589 Smart Q XL', '0587 Smart Q M', '0588 Smart Q L', '0586 Smart Q S']



// conta('2025SSCM70')
mongoose.connect(CONFIG.MONGO, mongoOptions, async function (err) {
  if (err)
    throw err;

  let m = await findModelli(modelli), query
  console.log('Modelli: ' + JSON.stringify(m))
  query = { modello: { $in: m } }
  if (m.length !== modelli.length) {
      log.log('ATTENZIONE: non ho trovato tutti i modelli richiesti')
  }

  const p = require('./massive.js')
  console.log('cerco i prodotti')
  Prodotto.find(query, { codice: 1 }).lean()
    .exec(
      async function (err, prodotti) {
        console.log('inizio ciclo')
        for (const prodotto of prodotti) {
          if (p.hasCharsAt(prodotto.codice, 'S', 5)) {
            const conto = await conta(p.replaceCharAt(prodotto.codice, '7', 5))
            if (conto > 0) console.log(prodotto.codice)
          }
          if (p.hasCharsAt(prodotto.codice, '7', 5)) {
            const conto = await conta(p.replaceCharAt(prodotto.codice, 'S', 5))
            if (conto > 0) console.log(prodotto.codice)
          }
          // console.log(prodotto)
        }
        console.log('finito')
    })
})

/*

db.Prodotto.aggregate([
    { $match: { codice: { $regex: /^.{4}[S7]/ }, modello: {
        $in: [
            ObjectId("56b8aaa90de23c95600bd282"),ObjectId("5d00f328096f0f0004dda944"),ObjectId("56b8aaa90de23c95600bd2b6"),ObjectId("5847e3c50e147c0400fde026"),ObjectId("58331537e9983b040067c5ff"),ObjectId("59b922a2d5eaa0040053d68a"),ObjectId("59b922fed5eaa0040053d68c"),ObjectId("59b9233fd5eaa0040053d68d"),ObjectId("59b92364d5eaa0040053d68e"),ObjectId("590c3904ffdcba0400f94d79"),ObjectId("58b408ef076bbc0400698153"),ObjectId("5829a438ddea1004006e8b36"),ObjectId("59b26ed2e889e40400ced241"),ObjectId("59bbd3693b58a80400ab700a"),ObjectId("56b8aaa90de23c95600bd2be"),ObjectId("56b8aaa90de23c95600bd2a3"),ObjectId("56b8aaa90de23c95600bd2a9"),ObjectId("56b8aaa90de23c95600bd29f"),ObjectId("56b8aaa90de23c95600bd2a1"),ObjectId("56b8aaa90de23c95600bd2c4"),ObjectId("56b8aaa90de23c95600bd2c5"),ObjectId("56b8aaa90de23c95600bd2c6"),ObjectId("56b8aaa90de23c95600bd2c8")
            // "5b2b85760aa32f0400e9ad59","5b2ba23f0aa32f0400e9ad79","5b2214b8cf69c604004c7f00","5afed6b91035860400702219","5b3b441fe2fa7104002f393a","5b36052a7e73a20400684c66","5b3b4829e2fa7104002f394b","5be1558e9746be0400ff6fe7","5c35d6c43fb4fc04006a0c2e","56b8aaa90de23c95600bd225","56b8aaa90de23c95600bd226"
            ]
    } }},
    {
      $group: { _id: { prima: { $substr: [ "$codice", 0, 4 ] }, dopo: { $substr: [ "$codice", 5, 100 ] } }, count:{$sum:1} }
   },
   {
       $match: { "count": 2 }
   }
])
  */