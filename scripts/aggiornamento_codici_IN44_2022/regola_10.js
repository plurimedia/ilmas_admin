const process = require('./massive.js');

const filtro = ['2025 KS/A Track', '2006 KL/A Track', '2011 KS/1', '2012 KS/2', '2013 KS/3', '2026 KL/1', '2027 KL/2', '2028 KL/3',
  '2017 KS/Plus P', '2020 KL/Plus P', '2018 KS/Plus S', '2021 KL/Plus S']

var query = { modello: { $in: [] } };
var tag = 'lorenzo_2022_regola10';

const colori = ['Blu metal', 'Oro 24K', 'Cromo', 'Bronzo base Ottone', 'Rame']

//regole: [10.1]
var transform1 = {
  tag: tag,
  // query: query,
  modelli: filtro,
  preProcess: 'getFromProd',
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        var upd = {};

        if (colori.includes(p.colore)) {
          upd.exportMetel = false;
          upd.pubblicato = false;
        }

        console.log('nella prima funzione di trasformazione ' + p.codice);
        return p;
      }
    }
  ]
};

process.execute(transform1)
  .then(function (err) { console.log('finito!!'); })
  .catch(function () { console.log('fallito!!'); });

/*
Per tutti i modelli:
'2025 KS/A Track', '2006 KL/A Track', '2011 KS/1', '2012 KS/2', '2013 KS/3', '2026 KL/1', '2027 KL/2', '2028 KL/3',
'2017 KS/Plus P', '2020 KL/Plus P', '2018 KS/Plus S', '2021 KL/Plus S'

-Per i codici con campo 'colore' = 'Blu metal' or 'Oro 24K' or 'Cromo' or 'Bronzo base Ottone' or 'Rame' --> flag 'Export per Metel' and flag 'Pubblica sul sito?' = false.

*/


