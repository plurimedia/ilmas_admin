var process = require('./massive.js');
var modello1 = ['5743f1d166923a030050afd0', '5821d5261164470300324b5c']
var modello2 = ['583bfb098957d2040071c175', '57eb8bac60ce4b0300262fe7']
var modello3 = ['57eb8bbf60ce4b0300262fe8', '58331537e9983b040067c5ff']

var query = { modello : { $in: modello1 } };
var tag = 'dani_20221028_03';

var transform1 = {
  idx: 1,
  tag: tag,
  query: query,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['D', 'S'], pos: 6 },
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        p = process.archiviaConNote(p, 'IN40')

        return p
      }
    }
  ]
}

var transform2 = {
  idx: 2,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['C'], pos: 6 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'B', 6)
        p.w = '12'
        p.ma = '350'

        switch (p.lm) {
          case '2270':
            p.lm = '1615';
            break;
          case '2390':
            p.lm = '1700';
            break;
          case '2510':
            p.lm = '1785';
            break;
          case '2030':
            p.lm = '1460';
            break;
          case '2085':
            p.lm = '1500';
            break;
          case '1700':
            p.lm = '1200';
            break;
        }

        return p
      }
    }
  ]
}

var transform3 = {
  idx: 3,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt,process.filters.hasCharAt],
  filterArguments: [{ chars: ['2027'], pos: 1 },{ chars: ['C'], pos: 6 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'B', 6)
        p.w = '2x12'
        p.ma = '350'

        switch (p.lm) {
          case '4540':
            p.lm = '3230';
            break;
          case '4780':
            p.lm = '3400';
            break;
          case '5020':
            p.lm = '3570';
            break;
          case '4060':
            p.lm = '2920';
            break;
          case '5020':
            p.lm = '3000';
            break;
          case '3400':
            p.lm = '2400';
            break;
        }

        return p
      }
    }
  ]
}

var transform4 = {
  idx: 4,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt,process.filters.hasCharAt],
  filterArguments: [{ chars: ['2028'], pos: 1 },{ chars: ['C'], pos: 6 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'B', 6)
        p.luci = 3
        p.w = '12'
        p.ma = '350'

        switch (p.lm) {
          case '6810':
            p.lm = '4845';
            break;
          case '7170':
            p.lm = '5100';
            break;
          case '7530': if (process.hasCharAt(p.codice, 'D', 5)) p.lm = '4500'; else p.lm = '5355';
            break;
          case '6090':
            p.lm = '4380';
            break;
          case '5100':
            p.lm = '3600';
            break;
        }

        return p
      }
    }
  ]
}

var transform5 = {
  idx: 5,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['C'], pos: 6 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'Q', 6)
        p.w = '21'
        p.ma = '600'

        switch (p.lm) {
          case '2270':
            p.lm = '2695';
            break;
          case '2390':
            p.lm = '2835';
            break;
          case '2510':
            p.lm = '2980';
            break;
          case '2030':
            p.lm = '2400';
            break;
          case '2085':
            p.lm = '2470';
            break;
          case '1700':
            p.lm = '2100';
            break;
        }

        return p
      }
    }
  ]
}

var transform6 = {
  idx: 6,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt,process.filters.hasCharAt],
  filterArguments: [{ chars: ['2027'], pos: 1 },{ chars: ['C'], pos: 6 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'Q', 6)
        p.luci = 2
        p.w = '21'
        p.ma = '600'

        switch (p.lm) {
          case '4540':
            p.lm = '5390';
            break;
          case '4780':
            p.lm = '5670';
            break;
          case '5020': if (process.hasCharAt(p.codice, 'D', 5)) p.lm = '4940'; else p.lm = '5960';
            break;
          case '4060':
            p.lm = '4800';
            break;
          case '5020':
            p.lm = '4200';
            break;
        }

        return p
      }
    }
  ]
}

var transform7 = {
  idx: 7,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt,process.filters.hasCharAt],
  filterArguments: [{ chars: ['2028'], pos: 1 },{ chars: ['C'], pos: 6 }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'Q', 6)
        p.luci = 3
        p.w = '21'
        p.ma = '600'

        switch (p.lm) {
          case '6810':
            p.lm = '8084';
            break;
          case '7170':
            p.lm = '8505';
            break;
          case '7530': if (process.hasCharAt(p.codice, 'D', 5)) p.lm = '7410'; else p.lm = '8940';
            break;
          case '6090':
            p.lm = '7200';
            break;
          case '5100':
            p.lm = '6300';
            break;
        }

        return p
      }
    }
  ]
}

var transform8 = {
  idx: 8,
  tag: tag,
  query: query,
  checkDuplicates: true,
  filter: [process.filters.hasCharAt, process.filters.isModello],
  filterArguments: [{ chars: ['D'], pos: 9 },{ modello: '5743f1d166923a030050afd0' }],
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p = process.dali(p, 9)
        p.alimentatore_incluso = true

        return p
      }
    }
  ]
}

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function (err) { return process.execute(transform8) })
    .then(function(err) { console.log("finito!!"); })
    .catch(function(err) { console.log("fallito!!", err); });

