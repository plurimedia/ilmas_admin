// da lanciare con extra mem alloc? node --max-old-space-size=2000 scripts/aggiornamento_codici_IN44_2022/aumento_prezzi.js;

const process = require('./massive.js');
const csv = require("fast-csv");
const fs = require('fs');
const csvpath = "scripts/aggiornamento_codici_IN44_2022/vecchio_listino.csv";
const stream = fs.createReadStream(csvpath);

const filtro1X = ["2001 KS Track", "2002 KL Track", "2029 KXL Track", "2025 KS/A Track", "2006 KL/A Track", "2031 KXL/A Track", "2240 Smart Track XS", "1240 Over Led Ps Mini", "1241 Over Led Ps Midi", "1242 Over Led Ps Maxi", "1244 Over Led Maxi F112", "1330 Fly Mini", "1331 Fly Midi", "1332 Fly Maxi", "1334 Fly Maxi F112", "1333 FLY MINI-A", "1335 Fly Midi/A", "1336 Fly Maxi/ A", "1321 Viper Midi", "1322 Viper Maxi", "1298 Cove Pro F112", "0830 Jet M", "1233 Sun/Track L", "0595 Smart XS", "0596 Smart S", "0597 Smart M", "0598 Smart L", "0599 SMART XL", "0796 Hammer Mini 120", "0797 Hammer Midi 160", "0798 Hammer Maxi 205", "0799 Hammer Extra 230", "0585 Smart Q XS", "0786 Hammer Q Mini 120", "0787 Hammer Q Midi160", "0788 Hammer Q Maxi 205", "0789 Hammer Q Ex Plus 230", "2011 KS/1", "2026 KL/1", "0608 Bob Std Led", "1708 Bob Ext Led", "1608 Bob Std Sc Led", "1738 Bob Ext Sc Led", "0908 Bob Slim", "1712 Bob Eco Led", "1888 Puzzle Led", "1704 Mini Bob", "0708 Smart C/S", "0621 Bob Std Tondo", "0526 Smart A/ S", "0527 Smart A/ M", "0528 Smart A/L", "0670 Astuto Led Mini", "0671 Astuto Led Midi", "0672 Astuto Led Maxi", "0660 Dino Led Mini", "0661 Dino Led Midi", "0662 Dino Led Maxi", "0730 Berin Led Mini", "0731 Berin Led Midi", "0732 Berin Led Maxi", "0620 Bob Eco Tondo", "2916 Cristal", "2918 Cristal 2", "2917 Cristal Q", "2003 KS/P", "2004 KL/P", "2022 KS1/P", "2017 KS/Plus P", "2020 KL/Plus P", "4898 Spring Maxi 205", "4899 Spring Extra 230", "4531 Orion", "4621 Fold Led", "4711 Tech Led", "1060 Dial 1 Led", "0775 Nuvola Led", "4251 Magic 2 Wall S", "2018 KS/Plus S", "2021 KL/Plus S", "4996 Summer Mini 120", "4997 Summer Midi 160", "4998 Summer Maxi 205", "4999 Summer Extra 230", "F111 Led Module", "F112 Led Module", "2150 Smart E / M", "0585 Smart Q XS", "0518 SMART B/L", "0517 Smart B/M", "0516 Smart B/ S", "0589 Smart Q XL", "0587 Smart Q M", "0588 Smart Q L", "0586 Smart Q S"];
const filtro2X = ["2012 KS/2", "2027 KL/2", "0609 Bob Std Led", "1709 Bob Ext Led", "1609 Bob Std Sc Led", "1739 Bob Ext Sc Led", "0909 Bob Slim", "1713 Bob Eco Led", "1706 Mini Bob", "0709 Smart C/S", "2023 KS2/P", "4532 Orion", "0776 Nuvola Led", "4712 Tech Led", "4622 Fold Led", "4252 Magic Wall S", "1002 Marquez Led", "1061 Dial 2 Led"];
const filtro3X = ["2013 KS/3", "2028 KL/3", "0610 Bob Std Led", "1710 Bob Ext Led", "1610 Bob Std Sc Led", "1740 Bob Ext Sc Led", "0910 Bob Slim", "1714 Bob Eco Led", "1707 Mini Bob", "0710 Smart C/S", "2024 KS3/P", "4253 Magic 2 Wall S", "4533 Orion", "4623 Fold Led", "4713 Tech Led", "0777 Nuvola Led", "1062 Dial 3 Led", "1003 Marquez Led"];
const filtro4X = ["0611 Bob Std Led", "1611 Bob Std Sc Led", "4714 Tech Led", "1063 Dial 4 Led", "1004 Marquez Led"];
const filtro6X = ["1006 Marquez Led"];

const modelli1X = await db.collection('Modello').find({ nome: { $in: filtro1X } }, { codice: 1 }).toArray();
const modelli2X = await db.collection('Modello').find({ nome: { $in: filtro2X } }, { codice: 1 }).toArray();
const modelli3X = await db.collection('Modello').find({ nome: { $in: filtro3X } }, { codice: 1 }).toArray();
const modelli4X = await db.collection('Modello').find({ nome: { $in: filtro4X } }, { codice: 1 }).toArray();
const modelli6X = await db.collection('Modello').find({ nome: { $in: filtro6X } }, { codice: 1 }).toArray();

const query1X = { modello: { $in: modelli1X } };
const query2X = { modello: { $in: modelli2X } };
const query3X = { modello: { $in: modelli3X } };
const query4X = { modello: { $in: modelli4X } };
const query6X = { modello: { $in: modelli6X } };

const tag = 'lorenzo_2022_regola_aumentoPrezzi';

// regola prezzi 1X
var transform1 = {
  tag: tag,
  query: query1X,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ["7", "S"], pos: 5 }, //pos in base 1
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        var upd = {};

        csv.fromStream(stream, {
          headers: true,
          delimiter: ',',
          discardUnmappedColumns: true
        })
          //RECUPERO I DATI DAL FILE
          .on("data", function (data) {

            if (data.codice === p.codice) {
              if (p.sottocategoria === 'Vivid') upd.prezzo = (data.prezzo + 10) * 1.1;
              if (p.sottocategoria === 'Sunlike') upd.prezzo = (data.prezzo + 30) * 1.1;

            }
          })

        return upd
      }
    }
  ]
};
//regola prezzi 2X
var transform2 = {
  tag: tag,
  query: query2X,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ["7", "S"], pos: 5 }, //pos in base 1
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        var upd = {};

        csv.fromStream(stream, {
          headers: true,
          delimiter: ',',
          discardUnmappedColumns: true
        })
          //RECUPERO I DATI DAL FILE
          .on("data", function (data) {

            if (data.codice === p.codice) {
              if (p.sottocategoria === 'Vivid') upd.prezzo = (data.prezzo + 20) * 1.1;
              if (p.sottocategoria === 'Sunlike') upd.prezzo = (data.prezzo + 60) * 1.1;

            }
          })

        return upd
      }
    }
  ]
};
//regola prezzi 3X
var transform3 = {
  tag: tag,
  query: query3X,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ["7", "S"], pos: 5 }, //pos in base 1
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        var upd = {};

        csv.fromStream(stream, {
          headers: true,
          delimiter: ',',
          discardUnmappedColumns: true
        })
          //RECUPERO I DATI DAL FILE
          .on("data", function (data) {

            if (data.codice === p.codice) {
              if (p.sottocategoria === 'Vivid') upd.prezzo = (data.prezzo + 30) * 1.1;
              if (p.sottocategoria === 'Sunlike') upd.prezzo = (data.prezzo + 90) * 1.1;

            }
          })

        return upd
      }
    }
  ]
};
//regola prezzi 4X
var transform4 = {
  tag: tag,
  query: query4X,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ["7", "S"], pos: 5 }, //pos in base 1
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        var upd = {};

        csv.fromStream(stream, {
          headers: true,
          delimiter: ',',
          discardUnmappedColumns: true
        })
          //RECUPERO I DATI DAL FILE
          .on("data", function (data) {

            if (data.codice === p.codice) {
              if (p.sottocategoria === 'Vivid') upd.prezzo = (data.prezzo + 40) * 1.1;
              if (p.sottocategoria === 'Sunlike') upd.prezzo = (data.prezzo + 120) * 1.1;

            }
          })

        return upd
      }
    }
  ]
};
//regola prezzi 6X
var transform5 = {
  tag: tag,
  query: query6X,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ["7", "S"], pos: 5 }, //pos in base 1
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        var upd = {};

        csv.fromStream(stream, {
          headers: true,
          delimiter: ',',
          discardUnmappedColumns: true
        })
          //RECUPERO I DATI DAL FILE
          .on("data", function (data) {

            if (data.codice === p.codice) {
              if (p.sottocategoria === 'Vivid') upd.prezzo = (data.prezzo + 60) * 1.1;
              if (p.sottocategoria === 'Sunlike') upd.prezzo = (data.prezzo + 180) * 1.1;
            }
          })

        return upd
      }
    }
  ]
};

process.execute(transform1)
  .then(function (err) { return process.execute(transform2) })
  .then(function (err) { return process.execute(transform3) })
  .then(function (err) { return process.execute(transform4) })
  .then(function (err) { return process.execute(transform5) })
  .then(function (err) { console.log("finito!!"); })
  .catch(function () { console.log("fallito!!"); });

/*
vedi regole 1X, 2X, 3X, 4X, 6X
*/


