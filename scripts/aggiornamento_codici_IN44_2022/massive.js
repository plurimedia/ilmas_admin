var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    q = require('q'),
    db = CONFIG.MONGO,
    dbTestConn = CONFIG.getConfiguration('test').MONGO,
    dbProdConn = CONFIG.getConfiguration('production').MONGO,
    CategoriaSchema = require(process.cwd() + '/server/models/categoria'),
    ModelloSchema = require(process.cwd() + '/server/models/modello'),
    ProdottoSchema = require(process.cwd() + '/server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    Modello = mongoose.model('Modello'),
    Categoria = mongoose.model('Categoria'),
    Utils = require('../../server/routes/utils'),
    /* fs      = require('fs'), */
    path = require('path'),
    dest = path.join(process.cwd(),'dump'),
    backup = require('mongodb-backup'),
    restore = require('mongodb-restore'),
    bckrst = require('./backup-restore'),
    ObjectId = require('mongodb').ObjectID,
    aggiornati = 0,
    g_tag = "",
    g_executedBackup = false;

const mongoOptions = {
    sslValidate: true,
    tls: true,
    tlsCAFile: path.join(__dirname, "../../ibm_mongodb_ca.pem"),
    useUnifiedTopology: true,
    useNewUrlParser: true
}

function _filter (p, t) {
    var trovato = false;

    if (!t.filter) return true;

    if (_.isArray(t.filter)) {

        //console.log('[massive.js] filtri multipli');

        for (var idx = 0; idx < t.filter.length; idx++) {

            var filt = t.filter[idx]
            if (t.filter[idx] === exports.filters.hasCharAt || 
                t.filter[idx] === exports.filters.hasSottoCategoria ||
                t.filter[idx] === exports.filters.isModello) return true // shortcut, ora ho messo questi filtri direttamente nelle query
            trovato = filt(p, t.filterArguments[idx]);
            //console.log('[DEBUG] '+ idx + ' ' + trovato + ' ' + p.codice);
            if (!trovato) {
                //console.log('[massive.js] trovato globale '+ trovato);
                return trovato
            }

        }
    } else {
        if (t.filter === exports.filters.hasCharAt || 
            t.filter === exports.filters.hasSottoCategoria ||
            t.filter === exports.filters.isModello) return true // shortcut, ora ho messo questi filtri direttamente nelle query

        if (t.filter(p, t.filterArguments)) {
            trovato = true;
        }
    }

    //console.log('[massive.js] trovato globale '+ trovato);
    return trovato;
}

function bck (que) {
    //console.log('connessione', CONFIG.MONGO);
    //console.log('destinazione', dest);
    //console.log('query', que);
    var deferred = q.defer();

    backup({
        uri : CONFIG.MONGO,
        root : dest,
        tar: 'dump'+g_tag+'.tar',
        query : que,
        //query : { modello: ObjectId('56b8aaa90de23c95600bd2cd'),generazione: 'G6',k: '3000',kit: { '$in': [ false, null ] },criptato: { '$in': [ false, null ] } },
        collections : ['Prodotto'],
        callback : function (err) {
            if (err) {
                console.log('errore backup', err);
                deferred.reject(err);
            }
            else
                deferred.resolve();
        }
    });

    return deferred.promise;
}

function rst (tag) {
    var deferred = q.defer();

    restore({
        uri : CONFIG.MONGO,
        root : dest,
        tar: 'dump'+g_tag+'.tar',
        callback : function (err) {
            if (err) {
                console.log('errore restore', err);
                deferred.reject(err);
            }
            else
                deferred.resolve();
        }
    });

    return deferred.promise;
}

exports.keywords = function (que) { //que è la query
    var deferred = q.defer();

    mongoose.connect(db, mongoOptions);

    mongoose.connection.once('open', function() {

            console.log("Connessione aperta, parte il batch."); /// this gets printed to console

            Prodotto.find(que).lean()
                .populate('modello')

                .exec(function(err,prodotti){

                    // console.log('trovati ' + prodotti.length + ' prodotti')

                    var bulk = Prodotto.collection.initializeOrderedBulkOp();
                    async.each(prodotti, function(prodotto,cb){

                        var update = {};

                       // console.log(aggiornati, "  - aggiornamento prodotto id = " , prodotto.codice);

                        if (prodotto.codice){
                            var p = _.clone(prodotto);


                            update.$set = {};
                            update.$set.keywords =   Utils.keyWords(p);
                            //update.$set.pubblicato=false;
                            bulk.find( { _id: prodotto._id } ).updateOne(update);
                            aggiornati ++;

                        }
                        cb();
                    }, function(err){

                        if(err)
                            deferred.reject(err);

                        if(aggiornati > 0) {

                            bulk.execute(function(err, result){
                                if(err)
                                    deferred.reject(err)

                                console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                                mongoose.connection.close();
                                deferred.resolve();

                            })

                        }
                        else {
                            console.log('Non ci sono prodotti da aggiornare')
                            mongoose.connection.close();
                            deferred.resolve();
                        }

                        numeroTrasformazioni++

                    })
                })


        }



    );

    return deferred.promise;
}

function backupProcess (t) {
    var deferred = q.defer();

    if (process.argv && process.argv[2] && t.query.modello && !process.executedBackup) {
        if (process.argv[2] == 'bck1') {
            console.log('lancia backup caso 1', t.query.modello);
            process.executedBackup = true;
            return bckrst.caso1(t.query.modello);
        }
        if (process.argv[2] == 'bck2') {
            console.log('lancia backup caso 2', t.query.modello);
            process.executedBackup = true;
            return bckrst.caso2(t.query.modello);
        }
        if (process.argv[2] == 'bck3') {
            console.log('lancia backup caso 3', t.query.modello);
            process.executedBackup = true;
            return bckrst.caso3(t.query.modello);
        }
        if (process.argv[2] == 'bck4') {
            console.log('lancia backup caso 4', t.query.modello);
            process.executedBackup = true;
            return bckrst.caso4(t.query.modello);
        }
        if (process.argv[2] == 'bck5') {
            console.log('lancia backup caso 5', t.query.modello);
            process.executedBackup = true;
            return bckrst.caso5(t.query.modello);
        }
    } else {
        return deferred.promise.resolve();
        deferred.resolve();
    }
}

const getConnections = async () => {
    const MongoClient = require('mongodb').MongoClient

    const clientTest = new MongoClient(dbTestConn)
    let dbTest = await clientTest.connect(dbTestConn, mongoOptions)

    const clientProd = new MongoClient(dbProdConn)
    let dbProd = await clientProd.connect(dbProdConn, mongoOptions)

    const clientLocal = new MongoClient('mongodb://@localhost/ilmas_admin')
    let dbLocal = await clientProd.connect('mongodb://@localhost/ilmas_admin')

    return { dbTest, dbProd, dbLocal }
}

/*
var transform1 = {
    tag : tag per riconoscere i prodotti creati con questa trasformazione,
    query: query di estrazione prodotti, alternativo a modelli,
    modelli: array di nomi di modelli, alternativo a query,
    test: se eseguire l'operazione sul db o dare solo i log informativi',
    checkDuplicates: se true quando trova duplicati, li lascia e non fa operazioni su quel prodotto,
    filter:filtro per filtrare i prodotti, ce ne sono in fondo a questo file. accetta anche array,
    filterArguments:argomenti per il filtro, vedere le funzioni in fondo al file se filter è un array, anche questo deve esserlo
    transform: [
        {
            action: per ora puo valere nuovo_inserimento e modifica_esistente (insert o update),
            replace_rule: funzione di trasformazione. Deve ritornare il prodotto in caso di inserimento, l'oggetto update in caso di update
        }
    ],
    preProcess: azione codificata dentro massive che viene eseguita prima delle trasformazioni. Es : getFromProd
};

azioni di preprocess : 
- getFromProd : da usare SOLO in ambiente di test. Cancella i prodotti interessati dalla query su test e li reinserisce da prod
*/

exports.execute_____testbackup = function (t) {
    var deferred = q.defer();

    backupProcess(t)
        .then(function() { exports.execute_(t); })
        .then(function() { deferred.resolve(); });

    return deferred.promise;


}

const objFindOne = async (obj, query, params) => {
    return new Promise((resolve, reject) => {
        obj.findOne(query, params).lean()
            .exec((err,result) => {
                if (err) reject(err)
                else resolve(result)
            })
    })
}

const findModelli = (filtro) => {
    return new Promise((resolve, reject) => {
        mongoose.connect(CONFIG.MONGO, mongoOptions, function (err) {
            if (err)
                throw err;

            Modello.find({ nome: { $in: filtro } }, { codice: 1 }).lean()
            .exec(function (err, modelli) {
                mongoose.connection.close();
                if (err)
                    reject(err)
                else {
                    const ret = []
                    for (const modello of modelli) {
                        ret.push(modello._id)
                    }
                    resolve(ret)
                }
            });
        })
    })
}

exports.execute = async function (t) {
    g_tag = t.tag;
    var log = {
        info : function (tx) {
            if (t.logLevel && t.logLevel > 0)
                console.log('[INFO] '+tx);
        },
        debug : function (tx) {
            if (t.logLevel && t.logLevel > 1)
                console.log('[DEBUG] '+tx);
        },
        log : function (tx) {
            console.log(tx);
        }
    };

    var deferred = q.defer();
    if (!t.query && t.modelli) {
        let m = await findModelli(t.modelli)
        console.log('Modelli: ' + JSON.stringify(m))
        t.query = { modello: { $in: m } }
        if (m.length !== t.modelli.length) {
            log.log('ATTENZIONE: non ho trovato tutti i modelli richiesti')
        }
    }

    var query = _.extend(t.query, {
            kit: {$in: [false, null]}
        },
        {
            speciale: {$in: [false, null]}
        },
        {
            criptato: {$in: [false, null]}
        }/* ,
        {
            generazione: {$nin: ['IN39', 'IN40', 'G4', 'G5']}
        } */
    )

    aggiornati = 0;
    var log_duplicates = "";

    let dbTest, dbProd, dbLocal, myQuery
    myQuery = {...query}
    if (myQuery.modello) {
        if (myQuery.modello.$in) {
            myIn = []
            for (let modello of myQuery.modello.$in) {
                myIn.push(ObjectId(modello))
            }
            myQuery.modello.$in = myIn
        } else {
            myQuery.modello = ObjectId(myQuery.modello)
        }
    }

    const addCharFilterToQuery = (filterArguments) => {
        let singleCharacters = true // di solito questa funzione viene chiamata o con un array di singoli caratteri, o con una stringa intera. In caso di stringa intera bisogna agire diversamente nella regexp
        for (const ch of filterArguments.chars) {
            if (ch.length > 1) singleCharacters = false
        }
        const searchChar = filterArguments.chars.join('')
        let orCondition
        if (_.isArray(filterArguments.pos)) {
            // in questo caso sono quasi certo di usare sempre la modalita singleCharacter, ma in futuro potrebbe non essere così
            orCondition = { $or: [] }
            for (const pos of filterArguments.pos) {
                orCondition.$or.push({
                    codice:  { $regex: new RegExp(`^.{${(pos - 1)}}[${searchChar}]`) }
                })
            }
        } else {
            // qui è possibile che io usi la modalità stringa (es : cercare i prodotti che iniziano con 2004)
            const searchString = singleCharacters ? `[${searchChar}]` : `${searchChar}`
            orCondition = {
                codice: { $regex: new RegExp(`^.{${(filterArguments.pos - 1)}}${searchString}`) }
            }
        }
        return orCondition
    }

    const addSottocategoriaFilterToQuery = (filterArguments) => {
        return { sottocategoria: { $regex: new RegExp(`^${filterArguments.sottocategoria}$`, 'i') } }
    }

    const addModelloFilterToQuery = (filterArguments) => {
        return { modello: filterArguments.modello }
    }

    if (t.filter && _.isArray(t.filter)) {
        query.$and = []
        for (const [idx, filtro] of Object.entries(t.filter)) {
            if (t.filter[idx] === exports.filters.hasCharAt) {
                query.$and.push(addCharFilterToQuery(t.filterArguments[idx]))
            }
            if (t.filter[idx] === exports.filters.hasSottoCategoria) {
                query.$and.push(addSottocategoriaFilterToQuery(t.filterArguments[idx]))
            }
            if (t.filter[idx] === exports.filters.isModello) {
                query.$and.push(addModelloFilterToQuery(t.filterArguments[idx]))
            }
        }
    } else {
        if (t.filter === exports.filters.hasCharAt) {
            Object.assign(query, addCharFilterToQuery(t.filterArguments))
        }
        if (t.filter === exports.filters.hasSottoCategoria) {
            Object.assign(query, addSottocategoriaFilterToQuery(t.filterArguments))
        }
        if (t.filter === exports.filters.isModello) {
            Object.assign(query, addModelloFilterToQuery(t.filterArguments))
        }
}

    var findProdotti = function () {
        console.log('Inizio processo ' + t.idx)

        var Fdeferred = q.defer();

        log.log("Tento di connettermi - DB: " + CONFIG.MONGO); /// this gets printed to console
        mongoose.connect(CONFIG.MONGO, mongoOptions, function (err) {
            if (err)
                throw err;
    
      
        /* mongoose.connect(db);

        

        mongoose.connection.once('open', function () { */

            log.log("Connessione aperta, parte il batch " + JSON.stringify(query)); /// this gets printed to console
            log.log('---------------------------------------------------------------------------------------')

            var text_categoria, text_modello;

            Prodotto.find(query).lean().limit(t.limit||0)
                .exec(
                function (err, prodotti) {
                    console.log('Trovati ' + prodotti.length + ' prodotti')
                    var bulk = Prodotto.collection.initializeOrderedBulkOp();
                    
                    async.each(prodotti,
                        function (prodotto, cb) {

                            var findModello = function () {
                                return new Promise(function (resolve, reject) {
                                    if (_filter(prodotto, t)) {
                                        Modello.findOne({_id: prodotto.modello}).lean()
                                            .exec(function (err, modello) {
                                                if (err)
                                                    log.log(err);
                                                else {
                                                    if (modello.nome) {
                                                        log.debug('modello trovato ' + modello.nome);
                                                        text_modello = modello.nome;
                                                    }
                                                    else
                                                        log.log('modello senza nome??'+modello._id);
    
                                                }
    
                                                resolve();
                                            });
                                    } else resolve();
                                })

                            }

                            var findCategoria = function () {
                                return new Promise(function(resolve, reject) {
                                    if (_filter(prodotto, t)) {
                                        Categoria.findOne({_id: prodotto.categoria}).lean()
                                            .exec(function (err, categoria) {
                                                if (err)
                                                    log.log(err);
                                                else {
                                                    if (categoria)
                                                        if (categoria.nome) {
                                                            log.debug('categoria trovata ' + categoria.nome);
                                                            text_categoria = categoria.nome;
                                                        }
                                                        else
                                                            log.log('categoria senza nome??'+categoria._id);
                                                    else
                                                        log.log('categoria non trovata?? '+prodotto.categoria);
                                                }
    
                                                resolve();
    
                                            });
                                    } else resolve();
                                })
                            }

                            var azione = async () => {

                                if (_filter(prodotto, t)) {

                                    var clone = _.clone(prodotto);

                                    for (const a of t.transform) {
                                        if (a.action === 'nuovo_inserimento') {
                                            const old = clone.codice;
                                            clone = a.replace_rule(clone);

                                            // se la funzione torna null o false non fa nulla
                                            if (clone) {

                                                delete clone._id;

                                                clone.mdate = new Date();

                                                clone.insertTag = t.tag;

                                                //pre keywords
                                                clone.modello = text_modello;
                                                clone.categoria = text_categoria;

                                                clone.keywords = Utils.keyWords(clone);

                                                //post keywords
                                                clone.modello = prodotto.modello;
                                                clone.categoria = prodotto.categoria;

                                                delete clone.last_user;

                                                if (t.checkDuplicates) {
                                                    let codiceTrovato
                                                    try {
                                                        codiceTrovato = await objFindOne(Prodotto, {codice:clone.codice}, {codice:1, generazione:1, criptato:1, speciale:1})

                                                        if (!codiceTrovato){

                                                            bulk.insert(clone);
                                                            log.info('accodato ' + old + ' ' + clone.codice);
                                                            aggiornati++;

                                                        } else {
                                                            // log.log('\'' + clone.codice + '\',')
                                                            if (codiceTrovato.criptato || codiceTrovato.speciale)
                                                                log.log('codice '+codiceTrovato.codice+' trovato, prodotto speciale o criptato, per cui saltata elaborazione.');
                                                            else {
                                                                bulk.find({codice:clone.codice}).update({ $set: clone });
                                                                log.log("codice " + clone.codice + ", generazione " + codiceTrovato.generazione + " esistente, sovrascrivo");
                                                                //log.log('\'' + clone.codice + '\',')
                                                                log_duplicates = log_duplicates + 'codice esistente : da ' + old + " a " + clone.codice + "\n";
                                                                aggiornati++;
                                                            }
                                                        }
                                                        log.log(`Eseguita la funzione di trasformazione numero ${t.idx} ${old} -> ${clone.codice} (${clone.generazione})`)
                                                    } catch (err) {
                                                        log.log("errore recupero codice esistente ", old, clone.codice, err);
                                                    }
                                                    
                                                } else {

                                                    bulk.insert(clone);
                                                    log.info('accodato ', old, clone.codice);

                                                    aggiornati++;
                                                    log.log('Eseguita la funzione di trasformazione numero ' + t.idx + ' ' + old.codice + ' -> ' + clone.codice)

                                                }
                                            }
                                        } else if (a.action === 'modifica_esistente') {
 
                                            var update = {};
                                            update.$set = a.replace_rule(clone);
                                           
                                            update.$set.mdate = new Date();
                                            update.$set.updateTag = t.tag;

                                            //pre keywords
                                            clone.modello = text_modello;
                                            clone.categoria = text_categoria;

                                            update.$set.keywords = Utils.keyWords(clone);

                                            //post keywords TODO : non so se servono piu
                                            clone.modello = prodotto.modello;
                                            clone.categoria = prodotto.categoria;
                                            // console.log('in modifica esistente', aggiornati, prodotto.codice, clone._id.toString());

                                            bulk.find({_id: clone._id}).updateOne(update)
                                            log.log('Eseguita la funzione di trasformazione numero ' + t.idx + ' ' + clone.codice)

                                            aggiornati++;
                                        } else if (a.action === 'genera_keywords') {
                                            var update = {}
                                            update.$set = clone
                                           
                                            update.$set.mdate = new Date()
                                            update.$set.updateTag = t.tag

                                            //pre keywords
                                            clone.modello = text_modello;
                                            clone.categoria = text_categoria;

                                            update.$set.keywords = Utils.keyWords(clone)

                                            //post keywords TODO : non so se servono piu
                                            clone.modello = prodotto.modello;
                                            clone.categoria = prodotto.categoria;
                                            // console.log('in modifica esistente', aggiornati, prodotto.codice, clone._id.toString());

                                            bulk.find({_id: clone._id}).updateOne(update)
                                            log.log('Riscrittura keywords numero ' + t.idx + ' ' + clone.codice)

                                            aggiornati++;                                            
                                        }
                                    }

                                }
                            }

                            var pre_azione = function () {
                                t.pre_replace(prodotto);
                            }

                            if (t.pre_replace) {
                                pre_azione();
                                findModello()
                                    .then(findCategoria)
                                    .then(azione)
                                    .then(function() { cb(); });
                            } else {
                                findModello()
                                    .then(findCategoria)
                                    .then(azione)
                                    .then(function() 
                                    { 
                                        //console.log("eccomi  ")
                                        cb(); 
                                    }
                                );
                            }
                        },
                        function (err) {

                          
                            if (err) {
                                console.log(err)
                                throw err;
                                Fdeferred.reject();
                            }

                            if (log_duplicates != "") {
                                /*
                                fs.appendFile(".tmp/" + text_modello + "_duplicates.txt",

                                    log_duplicates,

                                    function (err) {
                                        if (err) {
                                            log.log("Log was not saved!", err);
                                            throw err;
                                        }
                                        else
                                            log.debug("Log was saved!");
                                    }
                                );
                                */
                            }

                            if (aggiornati > 0 && !t.test) {

                                bulk.execute(function (err, result) {
                                    if (err) {
                                        log.log("Errore nella execute")
                                        throw err;
                                    }

                                    log.log('finito, chiudo la connessione: inseriti/aggiornati ' + aggiornati + ' prodotti per ' + JSON.stringify(query))
                                    log.log('------------------------------------------------------------------------------------------------')
                                    mongoose.connection.close();
                                    Fdeferred.resolve();

                                })

                            }
                            else {
                                log.log('Non ci sono prodotti da aggiornare')
                                mongoose.connection.close();
                                Fdeferred.resolve();
                            }
                        }
                    )
                });
        });

        return Fdeferred.promise;
    }

    function handleProcess(p) { // 2022 : process:'getFromProd' // { process:'backup|restore|execute' }
        console.log('handling process', p);
        var Hdeferred = q.defer();

        if (p == 'backup') {
            bck(t.query)
                .then(function(err) { if (err) Hdeferred.reject(); else Hdeferred.resolve(); });
        } else if (p == 'restore') {
            rst(t.query)
                .then(function(err) { if (err) Hdeferred.reject(); else Hdeferred.resolve(); });
        } else if (p === 'localBackupTest') {
            console.log('processo di backup in locale dei prodotti da test')
            dbLocal.collection('Prodotto').remove(myQuery).then((result) => {
                console.log('cancellati prodotti in locale', result.result.n)
                // esporto i prodotti da test (in un file? in memoria?)
                dbTest.collection('Prodotto').find(myQuery).toArray((err, prodottiDaTest) => {
                    // importo i prodotti in locale
                    dbLocal.collection('Prodotto').insertMany(prodottiDaTest).then(() => {
                        console.log('importati i prodotti in locale')
                        Hdeferred.resolve()
                    })
                })
            })
        } else if (p === 'localBackupRestore') {
            console.log('processo di recupero prodotti da prod')
            dbLocal.collection('Prodotto').remove(myQuery).then((result) => {
                console.log('cancellati prodotti in locale', result.result.n)
                // esporto i prodotti da test (in un file? in memoria?)
                dbTest.collection('Prodotto').find(myQuery).toArray((err, prodottiDaTest) => {
                    // importo i prodotti in locale
                    dbLocal.collection('Prodotto').insertMany(prodottiDaTest).then(() => {
                        console.log('importati i prodotti in locale')
                        Hdeferred.resolve()
                    })
                })
            })
        } else if (p === 'getFromProd') {
            // cancello con la query i prodotti in test
            console.log('processo di recupero prodotti da prod')
            bckrst.caso1(myQuery).then(() => {
                Hdeferred.resolve()
            })
        } else if (p === 'backupProdLocal') {
            // cancello con la query i prodotti in test
            console.log('processo di backup di prod in locale')
            bckrst.caso4(myQuery).then(() => {
                Hdeferred.resolve()
            })
        } else if (p === 'restoreProdLocal') {
            // cancello con la query i prodotti in test
            console.log('processo di restore di prod da locale')
            bckrst.caso5(myQuery).then(() => {
                Hdeferred.resolve()
            })
        } else if (p === 'backupTestLocal') {
            console.log('processo di backup di test in locale')
            bckrst.caso6(myQuery).then(() => {
                Hdeferred.resolve()
            })
        }//implementare execute

        return Hdeferred.promise;
    }

    if (t.preProcess && t.postProcess)
        handleProcess(t.preProcess)
            .then(function (err) { return findProdotti(); })
            .then(function (err) { return handleProcess(t.postProcess); })
            .then(function(err) { deferred.resolve(); })
            .fail(function() { deferred.reject(); });
    else if (t.preProcess && !t.postProcess)
        handleProcess(t.preProcess)
            .then(function (err) { return findProdotti(); })
            .then(function(err) { deferred.resolve(); })
            .fail(function() { deferred.reject(); });
    else if (!t.preProcess && t.postProcess)
        findProdotti()
            .then(function (err) { return handleProcess(t.postProcess); })
            .then(function(err) { deferred.resolve(); })
            .fail(function() { deferred.reject(); });
    else
        findProdotti()
            .then(function(err) { deferred.resolve(); })
            .fail(function() { deferred.reject(); });


    return deferred.promise;

}

exports.elaboraCorrispondenze = (c, v) => { // stringa matrice, valore
/*
`3085 --> 3110
3160 --> 3270
3390 --> 3440
3480 --> 3515
3565 --> 3695
3820 --> 3890
3860 --> 3910
3955 --> 4110
4240 --> 4320
`
*/
    const arr = c.split('\n')
    if (!arr.length || arr.length === 0) {
        console.log('Errore di interpretazione di una corrispondenza')
        return null
    }
    for (const el of arr) {
        const b = el.split('-->')
        if (b[0].trim() == v) {
            return b[1].trim()
        }
    }
    console.log('Corrispondenza matrice non trovata per ' + v)
    return v
}

exports.filters = {};

exports.filters.hasCharAt = function (p, pars) { //prodotto, oggetto parametro {chars:[], pos:1} o {chars:[], pos:[1, 2]}
    // i chars sono in OR e anche le posizioni
    if (_.isArray(pars.pos)) {
        for (const posizione of pars.pos) {
            if (exports.filters.hasCharAt(p, { chars: pars.chars, pos: posizione })) return true
        }
        return false
    }
    var trovato = false;
    // console.log('[DEBUG FUNZIONE] '+ p.codice);

    _.each(pars.chars, function (el) {
        //console.log('[DEBUG FUNZIONE] '+ p.codice + " " + el + " " + p.codice.indexOf(el) + " " + (pars.pos-1));
        if (el.length > 1) {
            if (p.codice.indexOf(el) === pars.pos - 1)//voglio in indice 1
                trovato = true;
        } else {
            if(p.codice.substring(pars.pos - 1, pars.pos) === el)
                trovato = true;
        }
    });

    return trovato;
}

exports.filters.hasNotCharAt = function (p, pars) { //prodotto, oggetto parametro {chars:[], pos:1} posizione in base 1
    return ! exports.filters.hasCharAt(p, pars);
}

exports.filters.hasSottoCategoria = function (p, pars) { //prodotto, oggetto parametro {sottocategoria:''} CASE INSENSITIVE!!!!!!
    if (!p.sottocategoria) return false;

    if (p.sottocategoria.toLowerCase() === pars.sottocategoria.toLowerCase())
        return true;


    return false;
}

exports.filters.hasNotSottoCategoria = function (p, pars) { //prodotto, oggetto parametro {sottocategoria:''} CASE INSENSITIVE!!!!!!
    //console.log(p.sottocategoria, pars.sottocategoria);
    if (!p.sottocategoria) return true;

    if (p.sottocategoria.toLowerCase() != pars.sottocategoria.toLowerCase())
        return true;


    return false;
}

exports.filters.hasLunghezzaCodiceMax = function (p, pars) { //prodotto, oggetto parametro {len:8}
    if (p.codice.length <= pars.len)
        return true;


    return false;
}

exports.filters.isGenerazione = function (p, pars) { // prodotto, oggetto parametro {gen:['D1','G6']}
    if (_.indexOf(pars.gen, p.generazione) > -1)
        return true;

    return false;
}

exports.filters.isModello = function (p, pars) { // prodotto, id mongo del modello {modello:'5b5f26286cbb3f0400f431ee'}
    if (p.modello.toString() === pars.modello)
        return true
    
    return false
}

exports.replaceCharAt = function (s, c, p) { //stringa carattere posizione
    return s.substring(0, p-1) + c + s.substring(p,100);//in base 1
}

exports.ifCharAtReplaceCharAt = function (s, c1, c2, p) { //stringa carattere da cercare, carattere da sostituire, posizione
    if (exports.hasCharAt(s, c1, p))
        return exports.replaceCharAt(s, c2, p);

    return s
}

exports.hasCharAt = function (s, c, p) { //stringa carattere posizione
    if (_.isArray(c)) {
        let trovato = false
        for (const lettera of c) {
            trovato = s.charAt(p-1) == lettera
            if (trovato) return true
        }
        return trovato
    }
    return s.charAt(p-1) == c;//in base 1
}

exports.hasCharsAt = function (s, c, p) { //stringa caratteri posizione
    return s.indexOf(c) == p-1;//in base 1
}


exports.dimmerabile = function (p) {
   // p.codice = exports.replaceCharAt(p.codice, 'D', 9);
    p.codice = p.codice + "D"
    p.alimentatore = 'Driver dimmerabile';
    p.dimmerabile = true;
    return p;
}

exports.dali = function (p, pos) { // prodotto, posizione della lettera da cambiare. Se non passo pos funziona come prima (retrocompatibilità)
    if (!pos) {
        p.codice = p.codice + "L"
    } else {
        p.codice = exports.replaceCharAt(p.codice, 'L', pos);
    }
    p.alimentatore = 'Driver dali';
    p.dimmerabile = true;
    return p;
}

exports.push_memory = function (p) {
    p.codice = exports.replaceCharAt(p.codice, 'P', 9);
    p.alimentatore = 'Driver dimmerabile Push Memory';
    return p;
}

exports.casambi = function (p, pos) { // prodotto, posizione della lettera da cambiare
    p.codice = exports.replaceCharAt(p.codice, 'C', pos);
    p.dimmerabile = true
    p.alimentatore = 'Driver casambi';
    return p;
}

exports.sunlike = function (p) {
    p.codice = exports.replaceCharAt(p.codice, 'SS', 5)
    p.irc = '97,6'
    p.sottocategoria = 'Sunlike'
    return p;
}

exports.vivid = function(p) {
    p.codice = exports.replaceCharAt(p.codice, 'X', 5)
    p.k = '3300'
    p.irc = '95'
    p.sottocategoria = 'Vivid'
    return p
}

exports.grigio = function (p) {
    p.codice = exports.replaceCharAt(p.codice, '0', 8);
    p.colore = 'Grigio';
    return p;
}

exports.nero = function (p, pos) { // prodotto, posizione della lettera da cambiare. Se non passo pos funziona come prima (retrocompatibilità)
    if (!pos) {
        p.codice = exports.replaceCharAt(p.codice, '2', 8);
    } else {
        p.codice = exports.replaceCharAt(p.codice, '2', pos);
    }
    p.colore = 'Nero';
    return p;
}

exports.bianco = function (p) {
    p.codice = exports.replaceCharAt(p.codice, '9', 8);
    p.colore = 'Tutto bianco';
    return p;
}


exports.archiviaConNote = function (p, gen) {
    p.codice = p.codice+"-"+gen;
    p.note = p.generazione
    p.generazione = gen
    p.exportMetel = false
    p.pubblicato = false
    return p
}

//exports.disArchivia = function (p, gen) {
exports.creaNuovoProdotto = function (p, gen) {
    p.padre = p.codice
    p.codice = p.codice.replace('-IN39', '');
    p.generazione = gen
    p.exportMetel = true
    p.pubblicato = true
    p.codiceEan = null
    p.modello_cript = null
    p.accessori=null
    p.padre=null
    p.last_user=null
    return p
}

exports.modificaProdotto = function (p) {
    return p
}


exports.aumentaPrezzo = function (p, delta) {
    p.prezzo = p.prezzo+delta;
    return p
}