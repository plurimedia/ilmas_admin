const process = require('./massive.js');

const filtro = ['2001 KS Track', '2025 KS/A Track', '2011 KS/1', '2012 KS/2', '2018 KS/Plus S', '2003 KS/P', '2017 KS/Plus P',
  '2022 KS1/P', '2023 KS2/P', '2013 KS/3', '2024 KS3/P', '4251 Magic 2 Wall S', '4252 Magic Wall S', '4253 Magic 2 Wall S']

var query = { modello: { $in: [] } };
var tag = 'lorenzo_2022_regola07';

//regole: [7.1]

var transform1 = {
  idx: 1,
  tag: tag,
  // query: query,
  modelli: filtro,
  checkDuplicates: true,
  preProcess: 'getFromProd',
  filter: process.filters.hasCharAt,
  filterArguments: { chars: ['F'], pos: 7 },
  transform: [
    {
      action: 'nuovo_inserimento',
      replace_rule: function (p) {
        p.codice = process.replaceCharAt(p.codice, 'B', 7);
        p.fascio = 12;

        return p;
      }
    }
  ]
};

process.execute(transform1)
  .then(function (err) { console.log('finito!!'); })
  .catch(function () { console.log('fallito!!'); });

/*
'2001 KS Track', '2025 KS/A Track', '2011 KS/1', '2012 KS/2', '2018 KS/Plus S', '2003 KS/P', '2017 KS/Plus P',
'2022 KS1/P', '2023 KS2/P', '2013 KS/3', '2024 KS3/P', '4251 Magic 2 Wall S', '4252 Magic Wall S', '4253 Magic 2 Wall S'

*/


