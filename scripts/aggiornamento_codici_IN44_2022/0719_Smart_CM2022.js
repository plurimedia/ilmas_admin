var process = require('./massive.js');
var modello = '5b5f2a616cbb3f0400f4320b';
var query = { modello: modello };
var tag = 'dani_20221027_02';

var transform1 = {
    idx: 1,
    tag: tag,
    query: query,
    preProcess: 'backupProdLocal',//'getFromProd',
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['Z', '7', '8'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p.generazione = 'D3'

                // console.log(p.codice)
                if (process.hasCharAt(p.codice, 'Z', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'O', 5)
                }
                if (process.hasCharAt(p.codice, '7', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'S', 5)
                }
                if (process.hasCharAt(p.codice, '8', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'T', 5)
                }
                // console.log(p.codice)

                switch (p.lm) {
                    case '2920':
                        p.lm = '3230'
                        break
                    case '3070':
                        p.lm = '3400'
                        break
                    case '3220':
                        p.lm = '3570'
                        break
                    case '4040':
                        p.lm = '4540'
                        break
                    case '4250':
                        p.lm = '4780'
                        break
                    case '5420':
                        p.lm = '6220'
                        break
                    case '5720':
                        p.lm = '6540'
                        break
                    case '6000':
                        p.lm = '6880'
                        break
                }

                return p
            }
        }
    ]
};

var transform2 = {
    idx: 2,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['R', '5', '6', 'Z', '7', '8'], pos: 5 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var upd = {};
                upd.pubblicato = false
                upd.exportMetel = false

                return upd;
            }
        }
    ]
};

var transform3 = {
    idx: 3,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['W'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.vivid(p)
                p.generazione = 'D3'
                p.prezzo -= 22

                switch (p.lm) {
                    case '3060':
                        p.lm = '2920'
                        break
                    case '4280':
                        p.lm = '4060'
                        break
                }

                return p;
            }
        }
    ]
};

var transform4 = {
    idx: 4,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['C'], pos: 6 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 6)
                p.w = '24' // potenza
                p.luci = 2
                p.ma = '700' // corrente
                p.generazione = 'D3'

                switch (p.lm) {
                    case '4060':
                        p.lm = '5520'
                        break
                }

                return p;
            }
        }
    ]
};

var transform5 = {
    idx: 5,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasSottoCategoria, process.filters.hasCharAt],
    filterArguments: [{ sottocategoria: 'Vivid' }, { chars: ['X'], pos: 5 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 5)
                p.k = '3500'
                p.generazione = 'D3'

                switch (p.lm) {
                    case '2920':
                        p.lm = '3000'
                        break
                    case '4060':
                        p.lm = '5020'
                        break
                    case '5520':
                        p.lm = '5670'
                        break
                }

                return p;
            }
        }
    ]
};

var transform6 = {
    idx: 6,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['S'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.sunlike(p)
                p.generazione = 'D3'
                p.prezzo += 67

                switch (p.lm) {
                    case '3400':
                        p.lm = '2400'
                        break
                    case '4780':
                        p.lm = '3400'
                        break
                    case '6540':
                        p.lm = '4800'
                        break
                }

                return p;
            }
        }
    ]
};

var transform7 = {
    idx: 7,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['W', 'Q'], pos: 5 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var upd = {}
                upd.generazione = 'IN40'
                upd.pubblicato = false
                upd.exportMetel = false

                return upd
            }
        }
    ]
};

var transform8 = {
    idx: 8,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt, process.filters.hasCharAt],
    filterArguments: [{ chars: ['L'], pos: 9 }, { chars: ['O', 'S', 'T'], pos: 5 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.casambi(p, 9)
                p.prezzo += 18

                return p;
            }
        }
    ]
};

var transform9 = {
    idx: 9,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt, process.filters.hasCharAt],
    filterArguments: [{ chars: ['L'], pos: 9 }, { chars: ['X', 'D'], pos: 5 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.casambi(p, 9)
                p.prezzo += 106

                return p;
            }
        }
    ]
};

var transform10 = {
    idx: 10,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt],
    filterArguments: [{ chars: ['L'], pos: 10 }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.casambi(p, 10)
                p.prezzo += 107

                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function (err) { return process.execute(transform8) })
    .then(function (err) { return process.execute(transform9) })
    .then(function (err) { return process.execute(transform10) })
    .then(function (err) { console.log("finito!!"); })
    .catch(function () { console.log("fallito!!"); });


/*
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto0719.json --query='{"modello":{"$oid":"5b5f2a616cbb3f0400f4320b"}}'

db.Prodotto.remove({modello:ObjectId('5b5f2a616cbb3f0400f4320b')})

mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto0719.json

Per il modello "0719 Smart C/M":


1. Duplicare tutti i codici con 5° carattere = "Z/7/8": 
-il campo "Flusso lum. nom.":
-2920 --> 3230
-3070 --> 3400
-3220 --> 3570
-4040 --> 4540
-4250 --> 4780
-5420 --> 6220
-5720 --> 6540
-6000 --> 6880




-il campo "Generazione" = "D3" (Da creare nel menu a tendina)
-il 5° carattere:
if = Z --> O
if = 7 --> S
if = 8 --> T

3. Tutti i codici con il 5° carattere = "R/5/6/Z/7/8" --> flag metel = false e flag sito = false.

4. Duplicare tutti i codici con 5° carattere = "W" :
   -5° carattere --> "X"
   -il campo "Temp. di colore" --> "3300"
   -il campo "CRI" --> "95"
   -il campo "Sottocategoria" --> "Vivid"
   -il campo "Flusso lum. nom.": 
    3060 --> 2920
    4280 --> 4060

    
5. Duplicare il codice con 6° carattere = C
   -6° carattere = C --> D
   -il campo "potenza" --> 2x24
   -il campo "corrente" --> 700
   -il campo "Flusso lum. nom.":
    4060 --> 5520

   

5. Duplicare tutti i codici creati al punto 4:
   -5° carattere --> "D"
   -il campo "Temp. di colore" --> "3500"
   -il campo "Flusso lum. nom.": 
    2920 --> 3000
    4060 --> 5020
    5520 --> 5670



6. Duplicare tutti i codici con 5° carattere = "S":
   -5° carattere "S" --> "SS"
   -il campo "CRI" --> "97,6"
   -il campo "Sottocategoria" --> "Sunlike"
   -il campo "Flusso lum. nom.": 
      3400 --> 2400
      4780 --> 3400
      6540 --> 4800

7. Tutti i codici con 5° carattere = "W/Q":
   mettere in archivio --> "Generazione" = "IN40" (Da creare nel menu a tendina)
   flag metel = false e flag sito = false.







 */