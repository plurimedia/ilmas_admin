const process = require('./massive.js');

const filtro = ['2016 KS/Plus', '1290 Over Led Ps2 Mini', '1291 Over Led Ps2 Midi', '1292 Over Led Ps2 Maxi', '1294 Over Led Ps2 Maxi F112', '2005 D KS/A Track ADATTATORE DRIVER', '2019 KL/Plus', '1322 Viper Mega', '1255 Five', '1256 Seven', '1257 Five', '1295 Kompact Ar', '1297 Kompact Ps Pro', '1296 Kompact Pro', '1222 Six', '1232 Ten', '1272 Form Led',
  '1416 Ca Track', '0799 Hammer Ex Plus 230', '0789 Hammer Q Ex Plus 230', '0931 Triec 120', '0933 Triec 160',
  '0937 Triec 230', '2660 Bat S', '2770 Bat M', '2880 Bat L', '2039 CONCAVE', '2030 KXL/P', '2009 KS/220 P',
  '2014 KL/220 P', '4999 Summer Extra Plus 230', '4416 Ca', '2010 KS/220 S', '2015 KL PLUS/220 S', '1155 Bell Mono',
  '1165 Bell Bi', '1167 Bell Bi', 'ZEUS', 'F050 Led Module', 'V0 Led Module', 'V5 Emergency Led Module']

var query = { modello: { $in: [] } };
var tag = 'lorenzo_2022_regola11';

//regola 11.1
var transform1 = {
  tag: tag,
  // query: query,
  modelli: filtro,
  preProcess: 'getFromProd',
  transform: [
    {
      action: 'modifica_esistente',
      replace_rule: function (p) {
        p = process.archiviaConNote(p, 'IN40')

        console.log('nella prima funzione di trasformazione ' + p.codice)
        return p
      }
    }
  ]
};


process.execute(transform1)
  .then(function (err) { console.log('finito!!'); })
  .catch(function () { console.log('fallito!!'); });

/*
modelli
'2016 KS/Plus', '1290 Over Led Ps2 Mini','1291 Over Led Ps2 Midi', '1292 Over Led Ps2 Maxi', '1294 Over Led Ps2 Maxi F112', '2005 D KS/A Track ADATTATORE DRIVER','2019 KL/Plus', '1322 Viper Mega','1255 Five','1256 Seven', '1257 Five','1295 Kompact Ar', '1297 Kompact Ps Pro','1296 Kompact Pro', '1222 Six', '1232 Ten', '1272 Form Led', 
'1416 Ca Track', '0799 Hammer Ex Plus 230', '0789 Hammer Q Ex Plus 230', '0931 Triec 120', '0933 Triec 160',
'0937 Triec 230','2660 Bat S', '2770 Bat M', '2880 Bat L', '2039 CONCAVE','2030 KXL/P', '2009 KS/220 P',
'2014 KL/220 P', '4999 Summer Extra Plus 230', '4416 Ca', '2010 KS/220 S', '2015 KL PLUS/220 S', '1155 Bell Mono'
'1157 Bell Mono','1165 Bell Bi', '1167 Bell Bi', 'ZEUS', 'F050 Led Module', 'V0 Led Module', 'V5 Emergency Led Module'

mettere in archivio con IN40
*/


