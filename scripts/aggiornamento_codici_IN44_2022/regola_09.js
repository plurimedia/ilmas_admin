var process = require('./massive.js');
var modello = ['59b9208ed5eaa0040053d67c'];
var query = { modello: { $in: modello } };
var tag = 'dani_20221028_02';

var transform1 = {
    idx: 1,
    tag: tag,
    query: query,
    preProcess: 'backupProdLocal',//'getFromProd',
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['F'], pos: 6 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                p = process.archiviaConNote(p, 'IN40')

                return p
            }
        }
    ]
};

var transform2 = {
    idx: 2,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['O', 'S', 'T'], pos: 5 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                p.lm = process.elaboraCorrispondenze(`3085 --> 3110
                3160 --> 3270
                3390 --> 3440
                3480 --> 3515
                3565 --> 3695
                3820 --> 3890
                3860 --> 3910
                3955 --> 4110
                4240 --> 4320
                `, p.lm)
                /* switch (p.lm) {
                    case 3085:
                        p.lm = 3110;
                        break;
                    case 3160:
                        p.lm = 3270;
                        break;
                    case 3390:
                        p.lm = 3440;
                        break;
                    case 3480:
                        p.lm = 3515;
                        break;
                    case 3565:
                        p.lm = 3695;
                        break;
                    case 3820:
                        p.lm = 3890;
                        break;
                    case 3860:
                        p.lm = 3910;
                        break;
                    case 3955:
                        p.lm = 4110;
                        break;
                    case 4240:
                        p.lm = 4320;
                        break;
                } */

                /* switch (p.w) {
                    case 25:
                        p.w = 24;
                        break;
                    case 29:
                        p.w = 28;
                        break;
                    case 33:
                        p.w = 32;
                        break;
                } */
                p.w = process.elaboraCorrispondenze(`25 --> 24
                29 --> 28
                33 --> 32
                `, p.w)

                if (p.alimentatore === 'Driver dimmerabile Push Memory') p.prezzo += 75

                return p
            }
        }
    ]
};

var transform3 = {
    idx: 3,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['W', 'Y'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.vivid(p)

                /* switch (p.lm) {
                    case '2910':
                        p.lm = '2760'
                        break
                    case '3280':
                        p.lm = '3110'
                        break
                    case '3640':
                        p.lm = '3450'
                        break
                } */
                p.lm = process.elaboraCorrispondenze(`2910 --> 2760
                3280 --> 3110
                3640 --> 3450
                `, p.lm)

                p.w = process.elaboraCorrispondenze(`25 --> 24
                29 --> 28
                33 --> 32
                `, p.w)

                /* switch (p.w) {
                    case 25:
                        p.w = 24;
                        break;
                    case 29:
                        p.w = 28;
                        break;
                    case 33:
                        p.w = 32;
                        break;
                } */

                if (p.alimentatore === 'Driver dimmerabile Push Memory') p.prezzo += 64
                else p.prezzo -= 11

                return p;
            }
        }
    ]
};

var transform4 = {
    idx: 4,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.hasCharAt, process.filters.hasSottoCategoria],
    filterArguments: [{ chars: ['X'], pos: 5 }, { sottocategoria: 'Vivid' }],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p.codice = process.replaceCharAt(p.codice, 'D', 5)
                p.k = '3500'

                switch (p.lm) {
                    case '2760':
                        p.lm = '2835'
                        break
                    case '3110':
                        p.lm = '3195'
                        break
                    case '3450':
                        p.lm = '3540'
                        break
                }

                return p
            }
        }
    ]
};

var transform5 = {
    idx: 5,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['7', 'S'], pos: 5 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.sunlike(p)

                if (process.hasCharAt(p.codice, 'C', 6)) {
                    p.w = '15'
                    p.ma = '450'
                }

                p.prezzo += 33

                switch (p.lm) {
                    case '3270':
                        p.lm = '2400'
                        break
                    case '3695':
                        p.lm = '2800'
                        break
                    case '4110':
                        p.lm = '3200'
                        break
                }

                return p
            }
        }
    ]
};

var transform6 = {
    idx: 6,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['D'], pos: 9 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p, 9)
                p.alimentatore_incluso = true

                return p
            }
        }
    ]
};

var transform7 = {
    idx: 7,
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['D'], pos: 10 },
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p, 10)
                p.alimentatore_incluso = true

                return p
            }
        }
    ]
};

var transform8 = {
    idx: 8,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['D'], pos: 9 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.alimentatore = 'Driver Dimmerabile Push N/A'
                p.dimmerabile = true

                return p
            }
        }
    ]
};

var transform9 = {
    idx: 9,
    tag: tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments: { chars: ['D'], pos: 10 },
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.alimentatore = 'Driver Dimmerabile Push N/A'
                p.dimmerabile = true

                return p
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function (err) { return process.execute(transform8) })
    .then(function (err) { return process.execute(transform9) })
    .then(function (err) { console.log("finito!!"); })
    .catch(function () { console.log("fallito!!"); });


/*
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto2029.json --query='{"modello":{"$oid":"59b9208ed5eaa0040053d67c"}}'

db.Prodotto.remove({modello:{$in:[ObjectId('59b9208ed5eaa0040053d67c')]}})

mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto2029.json


Per il modello:
"2029 KXL Track"



1.Mettere in archivio i codici con 6° carattere = F.

2.Duplicare i codici con 9° carattere = D:
-il 9° carattere D --> L
-il campo "alimentatore" = Driver Dali
-flag "Dimmerabile" and "Alimentatore inc." = true 





 */