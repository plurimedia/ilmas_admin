var process = require('./massive.js');
var modello = '5a65b145e23c1c0400bd7e9f';
var query = { modello : modello };
var tag = 'dani_20221121_01';

var transform1 = {
    idx: 1,
    tag : tag,
    query: query,
    // preProcess : 'backupProdLocal',//'getFromProd',
    filter: [process.filters.hasCharAt,process.filters.hasCharAt,process.filters.hasCharAt],
    filterArguments : [{chars:['S'], pos:6},{chars:['B','C','D'], pos:7},{chars:['C'], pos:10}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                p.prezzo = 303

                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .catch(function() { console.log("fallito!!"); });


/*
mongoexport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_prod" --collection "Prodotto" --out=Prodotto2241.json --query='{"modello":{"$oid":"5a65b145e23c1c0400bd7e9f"}}'

db.Prodotto.remove({modello:ObjectId('5a65b145e23c1c0400bd7e9f')})

mongoimport -u "ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08" -p "1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139" --authenticationDatabase "admin" --host "37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149" --ssl --sslCAFile "./ibm_mongodb_ca.pem" -d "ilmas_test" --collection "Prodotto" --file=Prodotto2241.json

db.Prodotto.remove({ codice:{$in:['2241SQE2','2241SQN1L','2241TQN1','2241SQE1L','2241TQN1L']}}) // codice duplicato

Per il modello "2241 Smart Track S":


1. Duplicare tutti i codici con 5° carattere = "Z/7/8": 
-il campo "Flusso lum. nom.":
-1460 --> 1615
-1535 --> 1700
-1610 --> 1785
-2020 --> 2270
-2125 --> 2390
-2235 --> 2510
-2375 --> 2695
-2500 --> 2835
-2626 --> 2980

-il campo "Generazione" = "D3" (Da creare nel menu a tendina)
-il 5° carattere:
if = Z --> O
if = 7 --> S
if = 8 --> T

3. Tutti i codici con il 5° carattere = "R/5/6/Z/7/8" --> flag metel = false e flag sito = false.

4. Duplicare tutti i codici con 5° carattere = "W" :
   -5° carattere --> "X"
   -il campo "Temp. di colore" --> "3300"
   -il campo "CRI" --> "95"
   -il campo "Sottocategoria" --> "Vivid"
   -il campo "Flusso lum. nom.": 
    1530 --> 1460
    2140 --> 2030
    2530 --> 2400
   

5. Duplicare tutti i codici creati al punto 4:
   -5° carattere --> "D"
   -il campo "Temp. di colore" --> "3500"
   -il campo "Flusso lum. nom.": 
      1460 --> 1500
      2030 --> 2085
      2400 --> 2470
      



6. Duplicare tutti i codici con 5° carattere = "S":
   -5° carattere "S" --> "SS"
   -il campo "CRI" --> "97,6"
   -il campo "Sottocategoria" --> "Sunlike"
   -il campo "Flusso lum. nom.": 
      2390 --> 1700
      1700 --> 1200
      2835 --> 2100
     



7. Tutti i codici con 5° carattere = "W/Q":
   mettere in archivio --> "Generazione" = "IN40" (Da creare nel menu a tendina)
   flag metel = false e flag sito = false.





 */