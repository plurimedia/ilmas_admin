// sistema il json dei comuni e delle nazioni e li mette nel db
// arrivano da da https://github.com/dakk/Italia.json e https://raw.githubusercontent.com/umpirsky/country-list/master/country/cldr/it/country.json

var fs = require('fs'),
    _ = require('underscore'),
    async = require('async'),
    mongoose = require('mongoose'),
    Comune = require('../server/models/comune'),
    Nazione = require('../server/models/nazione'),
    comuni = JSON.parse(fs.readFileSync('scripts/comuni.json')),
    nazioni = JSON.parse(fs.readFileSync('scripts/nazioni.json')),
    regioni = {
    	'VEN': 'Veneto',
    	'LOM': 'Lombardia',
    	'TOS': 'Toscana',
    	'SAR': 'Sardegna',
    	'ABR': 'Abruzzo',
    	'BAS': 'Basilicata',
    	'SIC': 'Sicilia',
    	'PUG': 'Puglia',
    	'PIE': 'Piemonte',
    	'LAZ': 'Lazio',
    	'CAM': 'Campania',
    	'MAR': 'Marche',
    	'CAL': 'Calabria',
    	'MOL': 'Molise',
    	'UMB': 'Umbria',
    	'FVG': 'Friuli Venezia Giulia',
    	'TAA': 'Trentino Alto Adige',
    	'LIG': 'Liguria',
    	'EMR': 'Emilia Romagna',
    	'VDA': 'Val D\'Aosta'
    }
    _regione = function (r) {
    	return regioni[r];
    },
    _cap = function (c) {
        if(c.indexOf('xx')!==-1) // citta con + cap
            return c.slice(0,-2)+'00';
        if(c.indexOf('x')!==-1) // citta con + cap
            return c.slice(0,-1)+'0';
        else
            return c;
    },
    resultComuni = [],
    resultNazioni = [];

    comuni.forEach(function (c)
    {
    	var c = {
    		nome: c.Comune,
    		provincia: c.Provincia,
    		cap: _cap(c.CAP),
    		regione: _regione(c.Regione)
    	};

    	resultComuni.push(c);
    })

    _.each(nazioni, function (v,k){
        if(k!=='ZZ')
            resultNazioni.push({
                codice: k,
                nome: v
            })
    })

    /* li metto nel db */ 
    mongoose.connect('mongodb://@localhost/ilmas_admin');
    mongoose.connection.on('open', function() {

        var Comune = require('mongoose').model('Comune'),
            Nazione = require('mongoose').model('Nazione');

        async.series([
            function (callback){
                /* comuni */
                Comune.remove({}, function (err){
                    if (err)
                        throw err;

                    async.each(resultComuni,
                      function (comune, callback) {

                          var c = new Comune(comune);
                          c.save(function (err, result) {
                            if(err) {
                              throw err;
                            }
                            else
                                callback();
                          });
                      },
                      function (err) {
                        if(err)
                            console.log(err)
                        else {
                            console.log('inseriti i comuni nel db')
                            callback(null);
                        }
                            
                      }
                    );
                })
            },
            function (callback){
                Nazione.remove({}, function (err){
                    if (err)
                        throw err;

                    async.each(resultNazioni,
                      function (nazione, callback) {
                          var n = new Nazione(nazione);
                          n.save(function (err, result) {
                            if(err) {
                              throw err;
                            }
                            else
                                callback();
                          });
                      },
                      function (err) {

                            if(err)
                                console.log(err)
                            else {
                                console.log('inserite le nazioni nel db');
                                callback(null);
                            }
                                
                        }
                    );
                })
            }
        ],
        // optional callback
        function (err, results){
            mongoose.connection.close();
        });

 
    });

