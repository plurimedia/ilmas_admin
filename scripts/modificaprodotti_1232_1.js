/*
    reindicizza il db prodotti con nuove caratteristiche di ricerca
    lanciare con: 
    node --max-old-space-size=2000 scripts/modificaprodotti_1232_1.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    Utils = require('../server/routes/utils'),
    aggiornati = 0;

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console
    console.log(db)


    var query =
            { 
                modello: {
                    $in: [
                         '56b8aaa90de23c95600bd241' 
                    ]
                },
                generazione:{
                    $in: [
                         'G6'
                    ]
                },
                moduloled:'F111 Led Module'
            }

    console.log(query);

    Prodotto.find(query).lean()
    .exec(function(err,prodotti){
        
        console.log('aggiorno '+prodotti.length+' prodotti')
        
        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, 

            function(prodotto,cb){
            
            var cloneP = _.clone(prodotto); 

            if ( cloneP.codice.substring(5,6)=='B'){

                var update = {};
                update.$set = {}; 
                
                update.$set.exportMetel=false;
                update.$set.pubblicato=false;

                update.$set.mdate = new Date();

                console.log(aggiornati, cloneP.codice);

                bulk.find( { _id: cloneP._id } ).updateOne(update);
                aggiornati ++;
            }
           

            cb();
        }, function(err){

            if(err)
                throw err;
            
            if(aggiornati > 0 ) {

                bulk.execute(function(err, result){
                    if(err)
                        throw err;
                    
                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();                
                        
                })                
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();    
            }

        })
    })
});