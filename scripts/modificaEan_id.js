/*
LANCIARE LA PROCEDURA
node --max-old-space-size=20000 scripts/modificaEan_id;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ModelloSchema = require('../server/models/modello'),
    CategoriaSchema = require('../server/models/categoria'),
    CodiceEanSchema = require('../server/models/codiceEan'),
    ProdottoSchema = require('../server/models/prodotto'),

    Modello = mongoose.model('Modello'),
    Categoria = mongoose.model('Categoria'),
    CodiceEan = mongoose.model('CodiceEan'),
    Prodotto = mongoose.model('Prodotto'),
    

    Utils = require('../server/routes/utils'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    CodiceEan.find({idCodice:null}).lean().skip(15000)
    .exec(function(err,codiciEan){

        console.log('aggiorno '+codiciEan.length+' codiciEan')

        var bulk = CodiceEan.collection.initializeOrderedBulkOp();
        async.each(codiciEan, 

            function(codiceEan,cb){
                
                var codiceProdotto = codiceEan.codice.trim()+'-IN39'
                console.log("cerco prodotto codice ",codiceProdotto)

                Prodotto.findOne(
                    {codice:codiceProdotto}, 
                    //{ keywords: { '$all': codiceEan.prodotto }}
                    function (err, result) {
                        if (err){
                            throw err;
                        }
                        else{
                            if (result){
                                var p = _.clone(result);

                                var update = {}; 
                                update.$set = {};
                                update.$set.idCodice =  p._id;

                            
                                console.log(codiceProdotto, "trovato")

                                bulk.find( { _id: codiceEan._id } ).updateOne(update);
                                aggiornati ++; 
                                
                            }
                            else{
                                console.log(codiceProdotto, "NON trovato")
                                
                            }
                            cb();
                        }
                  
                }).lean() //fine select Prodotto
            
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0) {
             
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
               
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

        })
    })

      
}



);
