var ComuneSchema = require(process.env.PWD + '/server/models/comune'),
AttivitaSchema = require(process.env.PWD + '/server/models/attivita'),
CategoriaSchema = require(process.env.PWD + '/server/models/categoria'),
SottocategoriaSchema = require(process.env.PWD + '/server/models/sottocategoria'),
ContattoSchema = require(process.env.PWD + '/server/models/contatto'),
ImmagineSchema = require(process.env.PWD + '/server/models/immagine'),
ModelloSchema = require(process.env.PWD + '/server/models/modello'),
NazioneSchema = require(process.env.PWD + '/server/models/nazione'),
OffertaSchema = require(process.env.PWD + '/server/models/offerta'),
ProdottoSchema = require(process.env.PWD + '/server/models/prodotto'),
CodiceEanSchema = require(process.env.PWD + '/server/models/codiceEan'),
SettingsSchema = require(process.env.PWD + '/server/models/settings'),
ComponenteSchema = require(process.env.PWD + '/server/models/componente'),
LineaCommercialeSchema = require(process.env.PWD + '/server/models/lineaCommerciale'),
LineaCommercialeInfoProdottoSchema = require(process.env.PWD + '/server/models/lineaCommercialeInfoProdotto'),
DwhEtichettaSchema = require(process.env.PWD + '/server/models/dwhEtichetta'),
MailingListSchema = require(process.env.PWD + '/server/models/mailingList'),
DwhLogSchema = require(process.env.PWD + '/server/models/dwhlog'),
DwhRicercaSchema = require(process.env.PWD + '/server/models/dwhRicerca'),
DwhSchedaTecnica = require(process.env.PWD + '/server/models/dwhSchedaTecnica'),
DwhSchedaDettaglio = require(process.env.PWD + '/server/models/dwhSchedaDettaglio'),
DashboardSchema = require(process.env.PWD + '/server/models/dashboard'),
ApiLog = require(process.env.PWD + '/server/models/apiLog'),
ConfigTipologieAssociazioniDSSchema = require(process.env.PWD + '/server/models/configTipologieAssociazioniDS'),

md = require(process.env.PWD + '/server/routes/modello');


//console.log(process.argv[2], process.argv[3] ? JSON.parse(process.argv[3]) : undefined)
console.log('inizio processo su file esterno', process.argv[1], JSON.parse(process.argv[3]));
md.creaDistintaBase(JSON.parse(process.argv[2]), JSON.parse(process.argv[3]))
    .then(md.inviaDistintaKyte)
    .then(function(err, result) {
        if (err)
            console.error(err);
            //res.status(500).send(err);

        console.log('dopo creazione distinta');
        //res.status(200).send('distinta creata correttamente');
    })

