/*
LANCIARE LA PROCEDURA
node --max-old-space-size=100000 scripts/insertCodiceFornitore.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
mongoose = require('mongoose'),
_ = require('underscore'),
async = require('async'),
db = CONFIG.MONGO,
ProdottoSchema = require('../server/models/prodotto'),
Prodotto = mongoose.model('Prodotto'),
csv = require("fast-csv"),
fs = require('fs'),
aggiornati = 0,
count=0;


console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

console.log("Connessione aperta, parte il batch."); /// this gets printed to console

prodotti = [
	{
		"ARTICOLO": "5019510A0000L",
		"CodArtClienteFornitore": "126183001V"
	},
	{
		"ARTICOLO": "5019510A0000T",
		"CodArtClienteFornitore": "126183001V"
	},
	{
		"ARTICOLO": "9605A021",
		"CodArtClienteFornitore": "9000-2/W-ST"
	},
	{
		"ARTICOLO": "28022176184",
		"CodArtClienteFornitore": "22176184"
	},
	{
		"ARTICOLO": "28028000015",
		"CodArtClienteFornitore": "28000015"
	},
	{
		"ARTICOLO": "28028000701",
		"CodArtClienteFornitore": "28000701"
	},
	{
		"ARTICOLO": "28028001122",
		"CodArtClienteFornitore": "28001122"
	},
	{
		"ARTICOLO": "28028001248",
		"CodArtClienteFornitore": "28001248"
	},
	{
		"ARTICOLO": "28087500346",
		"CodArtClienteFornitore": "87500346"
	},
	{
		"ARTICOLO": "28089602199",
		"CodArtClienteFornitore": "89602199"
	},
	{
		"ARTICOLO": "28089602277",
		"CodArtClienteFornitore": "89602277"
	},
	{
		"ARTICOLO": "28089602280",
		"CodArtClienteFornitore": "89602280"
	},
	{
		"ARTICOLO": "28089602400",
		"CodArtClienteFornitore": "89602400"
	},
	{
		"ARTICOLO": "28089602815",
		"CodArtClienteFornitore": "89602815"
	},
	{
		"ARTICOLO": "9000M60",
		"CodArtClienteFornitore": "28000412"
	},
	{
		"ARTICOLO": "19652",
		"CodArtClienteFornitore": "19652"
	},
	{
		"ARTICOLO": "19663",
		"CodArtClienteFornitore": "19663"
	},
	{
		"ARTICOLO": "710.03.00.30",
		"CodArtClienteFornitore": "710.03.00.30"
	},
	{
		"ARTICOLO": "62140-LBC",
		"CodArtClienteFornitore": "62140-LBC"
	},
	{
		"ARTICOLO": "62141-LBC",
		"CodArtClienteFornitore": "62141-LBC"
	},
	{
		"ARTICOLO": "71100",
		"CodArtClienteFornitore": "71100"
	},
	{
		"ARTICOLO": "E1210-LBC",
		"CodArtClienteFornitore": "E1210-LBC"
	},
	{
		"ARTICOLO": "E8110",
		"CodArtClienteFornitore": "E8110"
	},
	{
		"ARTICOLO": "E8160",
		"CodArtClienteFornitore": "E8160"
	},
	{
		"ARTICOLO": "0908XXX",
		"CodArtClienteFornitore": "B.173.A.52"
	},
	{
		"ARTICOLO": "0910BOX.00",
		"CodArtClienteFornitore": "B.173.A.72"
	},
	{
		"ARTICOLO": "2023FONDO",
		"CodArtClienteFornitore": "2023"
	},
	{
		"ARTICOLO": "2024STAFFA",
		"CodArtClienteFornitore": "2024"
	},
	{
		"ARTICOLO": "2026BOX.00",
		"CodArtClienteFornitore": "2041"
	},
	{
		"ARTICOLO": "2027BOX.00",
		"CodArtClienteFornitore": "ILMAS2042"
	},
	{
		"ARTICOLO": "2027FONDO",
		"CodArtClienteFornitore": "2027"
	},
	{
		"ARTICOLO": "7101",
		"CodArtClienteFornitore": "7101"
	},
	{
		"ARTICOLO": "7103",
		"CodArtClienteFornitore": "7103"
	},
	{
		"ARTICOLO": "7201",
		"CodArtClienteFornitore": "7201"
	},
	{
		"ARTICOLO": "7203",
		"CodArtClienteFornitore": "7203"
	},
	{
		"ARTICOLO": "B.127.C.05",
		"CodArtClienteFornitore": "B.127.C.05"
	},
	{
		"ARTICOLO": "B.152.C.01",
		"CodArtClienteFornitore": "B.152.C.01"
	},
	{
		"ARTICOLO": "B.152.C.03",
		"CodArtClienteFornitore": "B.152.C.03"
	},
	{
		"ARTICOLO": "B.253.A.72",
		"CodArtClienteFornitore": "B.253.A.72"
	},
	{
		"ARTICOLO": "B.284.A.22",
		"CodArtClienteFornitore": "B.284.A.22"
	},
	{
		"ARTICOLO": "B.333.C.12",
		"CodArtClienteFornitore": "B.333.C.12"
	},
	{
		"ARTICOLO": "B.333.C.13",
		"CodArtClienteFornitore": "B.333.C.13"
	},
	{
		"ARTICOLO": "B.333.C.22",
		"CodArtClienteFornitore": "B.333.C.22"
	},
	{
		"ARTICOLO": "B.333.C.22.05",
		"CodArtClienteFornitore": "B.333.C.22.05"
	},
	{
		"ARTICOLO": "B.333.C.22.1000",
		"CodArtClienteFornitore": "B.333.C.22.1000"
	},
	{
		"ARTICOLO": "B.333.C.22.2000",
		"CodArtClienteFornitore": "B.333.C.22.2000"
	},
	{
		"ARTICOLO": "B.346.A.11",
		"CodArtClienteFornitore": "B.346.A.11"
	},
	{
		"ARTICOLO": "B.346.A.21",
		"CodArtClienteFornitore": "B.346.A.21"
	},
	{
		"ARTICOLO": "B.346.A.31",
		"CodArtClienteFornitore": "B.346.A.31"
	},
	{
		"ARTICOLO": "B.346.C.11.1",
		"CodArtClienteFornitore": "B.346.C.11.1"
	},
	{
		"ARTICOLO": "B.346.C.12.1",
		"CodArtClienteFornitore": "B.346.C.12.1"
	},
	{
		"ARTICOLO": "B.346.C.12.2",
		"CodArtClienteFornitore": "B.346.C.12.2"
	},
	{
		"ARTICOLO": "B.346.C.13.1",
		"CodArtClienteFornitore": "B.346.C.13.1"
	},
	{
		"ARTICOLO": "B.346.C.13.2",
		"CodArtClienteFornitore": "B.346.C.13.2"
	},
	{
		"ARTICOLO": "B.346.C.13.3",
		"CodArtClienteFornitore": "B.346.C.13.3"
	},
	{
		"ARTICOLO": "CUT.C.000",
		"CodArtClienteFornitore": "CUT.C.000"
	},
	{
		"ARTICOLO": "ILMAS2023",
		"CodArtClienteFornitore": "ILMAS2023"
	},
	{
		"ARTICOLO": "ILMAS2024",
		"CodArtClienteFornitore": "ILMAS2024"
	},
	{
		"ARTICOLO": "ILMAS2041",
		"CodArtClienteFornitore": "ILMAS2041"
	},
	{
		"ARTICOLO": "ILMAS2042",
		"CodArtClienteFornitore": "ILMAS2042"
	},
	{
		"ARTICOLO": "ILMAS2043",
		"CodArtClienteFornitore": "ILMAS2043"
	},
	{
		"ARTICOLO": "ILS007-01",
		"CodArtClienteFornitore": "ILS007-01"
	},
	{
		"ARTICOLO": "ILS007-02",
		"CodArtClienteFornitore": "ILS007-02"
	},
	{
		"ARTICOLO": "KOR0609.00",
		"CodArtClienteFornitore": "B.173.A.62"
	},
	{
		"ARTICOLO": "008MATRIX1.00",
		"CodArtClienteFornitore": "008MATRIX1.00"
	},
	{
		"ARTICOLO": "008MATRIX300",
		"CodArtClienteFornitore": "008MATRIX3.00"
	},
	{
		"ARTICOLO": "1709BOX.00",
		"CodArtClienteFornitore": "DL-609/BAL.00"
	},
	{
		"ARTICOLO": "1710BOX.00",
		"CodArtClienteFornitore": "1710BOX.00"
	},
	{
		"ARTICOLO": "DL-608/BAL.00",
		"CodArtClienteFornitore": "DL-608/BOX.A"
	},
	{
		"ARTICOLO": "126119040",
		"CodArtClienteFornitore": "122356"
	},
	{
		"ARTICOLO": "126123010",
		"CodArtClienteFornitore": "123010"
	},
	{
		"ARTICOLO": "126127471",
		"CodArtClienteFornitore": "127471"
	},
	{
		"ARTICOLO": "126128087830/S",
		"CodArtClienteFornitore": "128087830/S"
	},
	{
		"ARTICOLO": "126128149830/S",
		"CodArtClienteFornitore": "128149830/S"
	},
	{
		"ARTICOLO": "126137988TC",
		"CodArtClienteFornitore": "137988"
	},
	{
		"ARTICOLO": "126183202M",
		"CodArtClienteFornitore": "183202M"
	},
	{
		"ARTICOLO": "12619920506",
		"CodArtClienteFornitore": "19920506"
	},
	{
		"ARTICOLO": "126489822258",
		"CodArtClienteFornitore": "489822258"
	},
	{
		"ARTICOLO": "126MB/218/2",
		"CodArtClienteFornitore": "137998TC"
	},
	{
		"ARTICOLO": "400126119618PR",
		"CodArtClienteFornitore": "119618PR"
	},
	{
		"ARTICOLO": "400126119772",
		"CodArtClienteFornitore": "119772"
	},
	{
		"ARTICOLO": "400126119773",
		"CodArtClienteFornitore": "119773"
	},
	{
		"ARTICOLO": "400126122099",
		"CodArtClienteFornitore": "122099"
	},
	{
		"ARTICOLO": "400126122200",
		"CodArtClienteFornitore": "122200"
	},
	{
		"ARTICOLO": "400126122201",
		"CodArtClienteFornitore": "122201"
	},
	{
		"ARTICOLO": "400126122201BI",
		"CodArtClienteFornitore": "122201BI"
	},
	{
		"ARTICOLO": "400126122204",
		"CodArtClienteFornitore": "122204"
	},
	{
		"ARTICOLO": "400126122208",
		"CodArtClienteFornitore": "122208"
	},
	{
		"ARTICOLO": "400126122218",
		"CodArtClienteFornitore": "122218"
	},
	{
		"ARTICOLO": "400126122219",
		"CodArtClienteFornitore": "122219"
	},
	{
		"ARTICOLO": "400126122220",
		"CodArtClienteFornitore": "122220"
	},
	{
		"ARTICOLO": "400126122300",
		"CodArtClienteFornitore": "122300"
	},
	{
		"ARTICOLO": "400126122320",
		"CodArtClienteFornitore": "122320"
	},
	{
		"ARTICOLO": "400126122322",
		"CodArtClienteFornitore": "122322"
	},
	{
		"ARTICOLO": "400126122350",
		"CodArtClienteFornitore": "122350"
	},
	{
		"ARTICOLO": "400126122354",
		"CodArtClienteFornitore": "122354"
	},
	{
		"ARTICOLO": "400126122360",
		"CodArtClienteFornitore": "122360"
	},
	{
		"ARTICOLO": "400126122400",
		"CodArtClienteFornitore": "123400"
	},
	{
		"ARTICOLO": "400126122403",
		"CodArtClienteFornitore": "122403"
	},
	{
		"ARTICOLO": "400126122409",
		"CodArtClienteFornitore": "122409"
	},
	{
		"ARTICOLO": "400126122413",
		"CodArtClienteFornitore": "122413"
	},
	{
		"ARTICOLO": "400126122417",
		"CodArtClienteFornitore": "122417"
	},
	{
		"ARTICOLO": "400126122420",
		"CodArtClienteFornitore": "122420"
	},
	{
		"ARTICOLO": "400126122424",
		"CodArtClienteFornitore": "122424"
	},
	{
		"ARTICOLO": "400126122440IP",
		"CodArtClienteFornitore": "122440IP"
	},
	{
		"ARTICOLO": "400126122479",
		"CodArtClienteFornitore": "122479"
	},
	{
		"ARTICOLO": "400126122511",
		"CodArtClienteFornitore": "122511"
	},
	{
		"ARTICOLO": "400126122598",
		"CodArtClienteFornitore": "122598"
	},
	{
		"ARTICOLO": "400126122633",
		"CodArtClienteFornitore": "122633"
	},
	{
		"ARTICOLO": "400126122635",
		"CodArtClienteFornitore": "122635"
	},
	{
		"ARTICOLO": "400126122652",
		"CodArtClienteFornitore": "122652"
	},
	{
		"ARTICOLO": "400126122656",
		"CodArtClienteFornitore": "122656"
	},
	{
		"ARTICOLO": "400126122672",
		"CodArtClienteFornitore": "122672"
	},
	{
		"ARTICOLO": "400126122750",
		"CodArtClienteFornitore": "122750"
	},
	{
		"ARTICOLO": "400126122754",
		"CodArtClienteFornitore": "122754"
	},
	{
		"ARTICOLO": "400126122756",
		"CodArtClienteFornitore": "122756"
	},
	{
		"ARTICOLO": "400126122758",
		"CodArtClienteFornitore": "122758"
	},
	{
		"ARTICOLO": "400126122764",
		"CodArtClienteFornitore": "122764"
	},
	{
		"ARTICOLO": "400126127030",
		"CodArtClienteFornitore": "127030"
	},
	{
		"ARTICOLO": "400126127032",
		"CodArtClienteFornitore": "127032"
	},
	{
		"ARTICOLO": "400126127046",
		"CodArtClienteFornitore": "127046"
	},
	{
		"ARTICOLO": "400126127074",
		"CodArtClienteFornitore": "127074"
	},
	{
		"ARTICOLO": "400126127310",
		"CodArtClienteFornitore": "127310"
	},
	{
		"ARTICOLO": "400126127314",
		"CodArtClienteFornitore": "127314"
	},
	{
		"ARTICOLO": "400126127471DI",
		"CodArtClienteFornitore": "127471DI"
	},
	{
		"ARTICOLO": "400126127472DI",
		"CodArtClienteFornitore": "127472DI"
	},
	{
		"ARTICOLO": "400126127473DI",
		"CodArtClienteFornitore": "127473DI"
	},
	{
		"ARTICOLO": "400126127474",
		"CodArtClienteFornitore": "127474"
	},
	{
		"ARTICOLO": "400126127475",
		"CodArtClienteFornitore": "127475"
	},
	{
		"ARTICOLO": "400126127476",
		"CodArtClienteFornitore": "127476"
	},
	{
		"ARTICOLO": "400126127477",
		"CodArtClienteFornitore": "127477"
	},
	{
		"ARTICOLO": "400126127478",
		"CodArtClienteFornitore": "127478"
	},
	{
		"ARTICOLO": "400126127479",
		"CodArtClienteFornitore": "127479"
	},
	{
		"ARTICOLO": "400126127484",
		"CodArtClienteFornitore": "127484"
	},
	{
		"ARTICOLO": "400126127486",
		"CodArtClienteFornitore": "127486"
	},
	{
		"ARTICOLO": "400126127534",
		"CodArtClienteFornitore": "127534"
	},
	{
		"ARTICOLO": "400126127535",
		"CodArtClienteFornitore": "127535"
	},
	{
		"ARTICOLO": "400126137944/16H",
		"CodArtClienteFornitore": "137944/16H"
	},
	{
		"ARTICOLO": "400126137988TC",
		"CodArtClienteFornitore": "137988TC"
	},
	{
		"ARTICOLO": "400126137999/49",
		"CodArtClienteFornitore": "137999/49"
	},
	{
		"ARTICOLO": "400126180294",
		"CodArtClienteFornitore": "180294"
	},
	{
		"ARTICOLO": "400126180426",
		"CodArtClienteFornitore": "180426"
	},
	{
		"ARTICOLO": "400126180427",
		"CodArtClienteFornitore": "180427"
	},
	{
		"ARTICOLO": "400126183202M",
		"CodArtClienteFornitore": "183202M"
	},
	{
		"ARTICOLO": "400126183246",
		"CodArtClienteFornitore": "183246"
	},
	{
		"ARTICOLO": "400126425720016",
		"CodArtClienteFornitore": "425720016"
	},
	{
		"ARTICOLO": "400126485720518",
		"CodArtClienteFornitore": "485720518"
	},
	{
		"ARTICOLO": "400128087/830S",
		"CodArtClienteFornitore": "128087/830S"
	},
	{
		"ARTICOLO": "400128416/940/35",
		"CodArtClienteFornitore": "128416/940/35"
	},
	{
		"ARTICOLO": "400137968/142",
		"CodArtClienteFornitore": "137968/142"
	},
	{
		"ARTICOLO": "400137969IP24MM",
		"CodArtClienteFornitore": "137969IP24MM"
	},
	{
		"ARTICOLO": "468780047",
		"CodArtClienteFornitore": "468780047"
	},
	{
		"ARTICOLO": "468780049",
		"CodArtClienteFornitore": "468780049"
	},
	{
		"ARTICOLO": "485720518",
		"CodArtClienteFornitore": "485720518"
	},
	{
		"ARTICOLO": "488787422",
		"CodArtClienteFornitore": "488787422"
	},
	{
		"ARTICOLO": "488787423",
		"CodArtClienteFornitore": "488787423"
	},
	{
		"ARTICOLO": "498780048",
		"CodArtClienteFornitore": "498780048"
	},
	{
		"ARTICOLO": "5019530A0242E",
		"CodArtClienteFornitore": "137962/242"
	},
	{
		"ARTICOLO": "5019530A02E6E",
		"CodArtClienteFornitore": "137968/226"
	},
	{
		"ARTICOLO": "900048070",
		"CodArtClienteFornitore": "122672"
	},
	{
		"ARTICOLO": "900048150",
		"CodArtClienteFornitore": "122764"
	},
	{
		"ARTICOLO": "9000B91",
		"CodArtClienteFornitore": "123010"
	},
	{
		"ARTICOLO": "9000B93",
		"CodArtClienteFornitore": "123010/3"
	},
	{
		"ARTICOLO": "9000B95",
		"CodArtClienteFornitore": "485720513"
	},
	{
		"ARTICOLO": "9000Q070",
		"CodArtClienteFornitore": "122479"
	},
	{
		"ARTICOLO": "9000Q150",
		"CodArtClienteFornitore": "126511"
	},
	{
		"ARTICOLO": "SPHWHAHDNG2VYZAVD2",
		"CodArtClienteFornitore": "SPHWHAHDNG2VYZAVD2"
	},
	{
		"ARTICOLO": "WHAHDND25YZT3D1",
		"CodArtClienteFornitore": "WHAHDND25YZT3D1"
	},
	{
		"ARTICOLO": "WHAHDND25YZV3D1",
		"CodArtClienteFornitore": "WHAHDND25YZV3D1"
	},
	{
		"ARTICOLO": "WHAHDND25YZV3D2",
		"CodArtClienteFornitore": "WHAHDND25YZV3D2"
	},
	{
		"ARTICOLO": "WHAHDND25YZW3D1",
		"CodArtClienteFornitore": "WHAHDND25YZW3D1"
	},
	{
		"ARTICOLO": "WHAHDND27YZT3D1",
		"CodArtClienteFornitore": "WHAHDND27YZT3D1"
	},
	{
		"ARTICOLO": "WHAHDND27YZV3D1",
		"CodArtClienteFornitore": "WHAHDND27YZV3D1"
	},
	{
		"ARTICOLO": "WHAHDNG25YZP3D1",
		"CodArtClienteFornitore": "WHAHDNG25YZP3D1"
	},
	{
		"ARTICOLO": "WHAHDNG25YZT3D1",
		"CodArtClienteFornitore": "WHAHDNG25YZT3D1"
	},
	{
		"ARTICOLO": "WHAHDNG25YZV3D1",
		"CodArtClienteFornitore": "WHAHDNG25YZV3D1"
	},
	{
		"ARTICOLO": "WHAHDNG25YZW3D1",
		"CodArtClienteFornitore": "WHAHDNG25YZW3D1"
	},
	{
		"ARTICOLO": "WHAHDNG27YZT3D1",
		"CodArtClienteFornitore": "WHAHDNG27YZT3D1"
	},
	{
		"ARTICOLO": "WHAHDNG27YZV3D1",
		"CodArtClienteFornitore": "WHAHDNG27YZV3D1"
	},
	{
		"ARTICOLO": "WHAHDNK25YZT3D1",
		"CodArtClienteFornitore": "WHAHDNK25YZT3D1"
	},
	{
		"ARTICOLO": "B.342.C.04",
		"CodArtClienteFornitore": "B.342.C.04"
	},
	{
		"ARTICOLO": "CUT.C.10",
		"CodArtClienteFornitore": "CUT.C.10"
	},
	{
		"ARTICOLO": "JAM",
		"CodArtClienteFornitore": "C1-56 - CUT.C.10.2"
	},
	{
		"ARTICOLO": "10130089005",
		"CodArtClienteFornitore": "10130089005"
	},
	{
		"ARTICOLO": "1014010",
		"CodArtClienteFornitore": "1014010"
	},
	{
		"ARTICOLO": "10140109005",
		"CodArtClienteFornitore": "10140109005"
	},
	{
		"ARTICOLO": "08502DT1",
		"CodArtClienteFornitore": "559249.01"
	},
	{
		"ARTICOLO": "08552DE1",
		"CodArtClienteFornitore": "561800"
	},
	{
		"ARTICOLO": "08552DN1",
		"CodArtClienteFornitore": "561802"
	},
	{
		"ARTICOLO": "08553DE1",
		"CodArtClienteFornitore": "561804"
	},
	{
		"ARTICOLO": "100106.01",
		"CodArtClienteFornitore": "100106.01"
	},
	{
		"ARTICOLO": "101REATT/9",
		"CodArtClienteFornitore": "501644"
	},
	{
		"ARTICOLO": "186449.82",
		"CodArtClienteFornitore": "186449.82"
	},
	{
		"ARTICOLO": "188700.92",
		"CodArtClienteFornitore": "188700.92"
	},
	{
		"ARTICOLO": "270186432",
		"CodArtClienteFornitore": "186432"
	},
	{
		"ARTICOLO": "270186625",
		"CodArtClienteFornitore": "186625"
	},
	{
		"ARTICOLO": "270186626",
		"CodArtClienteFornitore": "186626"
	},
	{
		"ARTICOLO": "270186627",
		"CodArtClienteFornitore": "186627"
	},
	{
		"ARTICOLO": "500V2",
		"CodArtClienteFornitore": "561793.01"
	},
	{
		"ARTICOLO": "50108501LX131",
		"CodArtClienteFornitore": "559249.01"
	},
	{
		"ARTICOLO": "50108501LX135",
		"CodArtClienteFornitore": "559248.01"
	},
	{
		"ARTICOLO": "50108501LX141",
		"CodArtClienteFornitore": "559251.01"
	},
	{
		"ARTICOLO": "50108501LX145",
		"CodArtClienteFornitore": "559250.01"
	},
	{
		"ARTICOLO": "527800.01",
		"CodArtClienteFornitore": "527800.01"
	},
	{
		"ARTICOLO": "532434",
		"CodArtClienteFornitore": "532434"
	},
	{
		"ARTICOLO": "558419",
		"CodArtClienteFornitore": "558419"
	},
	{
		"ARTICOLO": "561143.01",
		"CodArtClienteFornitore": "561143.01"
	},
	{
		"ARTICOLO": "561144.01",
		"CodArtClienteFornitore": "561144.01"
	},
	{
		"ARTICOLO": "561880",
		"CodArtClienteFornitore": "561880"
	},
	{
		"ARTICOLO": "561882",
		"CodArtClienteFornitore": "561882"
	},
	{
		"ARTICOLO": "563858",
		"CodArtClienteFornitore": "563858"
	},
	{
		"ARTICOLO": "563859",
		"CodArtClienteFornitore": "563859"
	},
	{
		"ARTICOLO": "563866.01",
		"CodArtClienteFornitore": "563866.01"
	},
	{
		"ARTICOLO": "564049.02",
		"CodArtClienteFornitore": "564049.02"
	},
	{
		"ARTICOLO": "564053.02",
		"CodArtClienteFornitore": "564053.02"
	},
	{
		"ARTICOLO": "9000H060",
		"CodArtClienteFornitore": "186625"
	},
	{
		"ARTICOLO": "9000H75",
		"CodArtClienteFornitore": "186432"
	},
	{
		"ARTICOLO": "GE08504K",
		"CodArtClienteFornitore": "563866.01"
	},
	{
		"ARTICOLO": "V5011B92300",
		"CodArtClienteFornitore": "566643.01"
	},
	{
		"ARTICOLO": "V5012A92",
		"CodArtClienteFornitore": "561639.01"
	},
	{
		"ARTICOLO": "V5012B92",
		"CodArtClienteFornitore": "561640.01"
	},
	{
		"ARTICOLO": "V5012E92",
		"CodArtClienteFornitore": "561641.01"
	},
	{
		"ARTICOLO": "V5013A92",
		"CodArtClienteFornitore": "561642.01"
	},
	{
		"ARTICOLO": "V5013B92",
		"CodArtClienteFornitore": "561643.01"
	},
	{
		"ARTICOLO": "V5021B2",
		"CodArtClienteFornitore": "561793 (ex 500V2)"
	},
	{
		"ARTICOLO": "V5022A2",
		"CodArtClienteFornitore": "562581.01"
	},
	{
		"ARTICOLO": "V5022B2",
		"CodArtClienteFornitore": "561793"
	},
	{
		"ARTICOLO": "V5022E2",
		"CodArtClienteFornitore": "562582 (ex 500GV)"
	},
	{
		"ARTICOLO": "V5023B2",
		"CodArtClienteFornitore": "562584.01"
	},
	{
		"ARTICOLO": "V8012A2",
		"CodArtClienteFornitore": "563013.01"
	},
	{
		"ARTICOLO": "V8012B2",
		"CodArtClienteFornitore": "563014"
	},
	{
		"ARTICOLO": "V8012B92",
		"CodArtClienteFornitore": "565711.01"
	},
	{
		"ARTICOLO": "50130001344P1",
		"CodArtClienteFornitore": "9310351842"
	},
	{
		"ARTICOLO": "241M095GFT3",
		"CodArtClienteFornitore": "BM M095GFT3"
	},
	{
		"ARTICOLO": "92GF2",
		"CodArtClienteFornitore": "92GF2"
	},
	{
		"ARTICOLO": "94CS2",
		"CodArtClienteFornitore": "94CS2"
	},
	{
		"ARTICOLO": "991",
		"CodArtClienteFornitore": "991"
	},
	{
		"ARTICOLO": "BM00130",
		"CodArtClienteFornitore": "BM00130"
	},
	{
		"ARTICOLO": "BM00140",
		"CodArtClienteFornitore": "BM00140"
	},
	{
		"ARTICOLO": "BM2507",
		"CodArtClienteFornitore": "BM2507"
	},
	{
		"ARTICOLO": "BM2696",
		"CodArtClienteFornitore": "BM 2696"
	},
	{
		"ARTICOLO": "BM4007",
		"CodArtClienteFornitore": "BM 4007"
	},
	{
		"ARTICOLO": "BM90160",
		"CodArtClienteFornitore": "BM90160"
	},
	{
		"ARTICOLO": "BMM096",
		"CodArtClienteFornitore": "BMM096"
	},
	{
		"ARTICOLO": "01UMN78380",
		"CodArtClienteFornitore": "01UMN78380"
	},
	{
		"ARTICOLO": "5010778104000",
		"CodArtClienteFornitore": "1020-75"
	},
	{
		"ARTICOLO": "5010778104001",
		"CodArtClienteFornitore": "1020-11"
	},
	{
		"ARTICOLO": "5010778104002",
		"CodArtClienteFornitore": "1020-10"
	},
	{
		"ARTICOLO": "5010779104000",
		"CodArtClienteFornitore": "1021-75"
	},
	{
		"ARTICOLO": "5010779104001",
		"CodArtClienteFornitore": "1021-11"
	},
	{
		"ARTICOLO": "5010779104002",
		"CodArtClienteFornitore": "1021-10"
	},
	{
		"ARTICOLO": "5019600A01021",
		"CodArtClienteFornitore": "7511-20-31"
	},
	{
		"ARTICOLO": "5019600A03012",
		"CodArtClienteFornitore": "7511-30-30"
	},
	{
		"ARTICOLO": "5019600A16001",
		"CodArtClienteFornitore": "7658-10-W31"
	},
	{
		"ARTICOLO": "5019600A16002",
		"CodArtClienteFornitore": "7658-10-W30"
	},
	{
		"ARTICOLO": "7626-00-00",
		"CodArtClienteFornitore": "7626-00-00"
	},
	{
		"ARTICOLO": "94-01010-30",
		"CodArtClienteFornitore": "94-01010-30"
	},
	{
		"ARTICOLO": "94-01010-31",
		"CodArtClienteFornitore": "94-01010-31"
	},
	{
		"ARTICOLO": "9600A010",
		"CodArtClienteFornitore": "7511-10-42"
	},
	{
		"ARTICOLO": "9600A011",
		"CodArtClienteFornitore": "7511-10-31"
	},
	{
		"ARTICOLO": "9600A012",
		"CodArtClienteFornitore": "7511-10-30"
	},
	{
		"ARTICOLO": "9600A017",
		"CodArtClienteFornitore": "7511-10-42"
	},
	{
		"ARTICOLO": "9600A020",
		"CodArtClienteFornitore": "7511-20-42"
	},
	{
		"ARTICOLO": "9600A021",
		"CodArtClienteFornitore": "7511-20-31"
	},
	{
		"ARTICOLO": "9600A022",
		"CodArtClienteFornitore": "7511-20-30"
	},
	{
		"ARTICOLO": "9600A027",
		"CodArtClienteFornitore": "7511-20-42"
	},
	{
		"ARTICOLO": "9600A030",
		"CodArtClienteFornitore": "7511-30-42"
	},
	{
		"ARTICOLO": "9600A031",
		"CodArtClienteFornitore": "7511-30-31"
	},
	{
		"ARTICOLO": "9600A032",
		"CodArtClienteFornitore": "7511-30-30"
	},
	{
		"ARTICOLO": "9600A037",
		"CodArtClienteFornitore": "7511-30-42"
	},
	{
		"ARTICOLO": "9600A110",
		"CodArtClienteFornitore": "7601-00-W20"
	},
	{
		"ARTICOLO": "9600A111",
		"CodArtClienteFornitore": "7601-00-W31"
	},
	{
		"ARTICOLO": "9600A112",
		"CodArtClienteFornitore": "7601-00-W30"
	},
	{
		"ARTICOLO": "9600A120",
		"CodArtClienteFornitore": "7652-10-W20"
	},
	{
		"ARTICOLO": "9600A121",
		"CodArtClienteFornitore": "7652-10-W31"
	},
	{
		"ARTICOLO": "9600A122",
		"CodArtClienteFornitore": "7652-10-W30"
	},
	{
		"ARTICOLO": "9600A130",
		"CodArtClienteFornitore": "7659-00-W20"
	},
	{
		"ARTICOLO": "9600A131",
		"CodArtClienteFornitore": "7659-00-W31"
	},
	{
		"ARTICOLO": "9600A132",
		"CodArtClienteFornitore": "7659-00-W30"
	},
	{
		"ARTICOLO": "9600A141",
		"CodArtClienteFornitore": "7604-10-W31"
	},
	{
		"ARTICOLO": "9600A142",
		"CodArtClienteFornitore": "7604-10-W30"
	},
	{
		"ARTICOLO": "9600A150",
		"CodArtClienteFornitore": "7652-11-W20"
	},
	{
		"ARTICOLO": "9600A151",
		"CodArtClienteFornitore": "7652-11-W31"
	},
	{
		"ARTICOLO": "9600A152",
		"CodArtClienteFornitore": "7652-11-W30"
	},
	{
		"ARTICOLO": "9600A160",
		"CodArtClienteFornitore": "7658-10-W20"
	},
	{
		"ARTICOLO": "9600A161",
		"CodArtClienteFornitore": "7658-10-W31"
	},
	{
		"ARTICOLO": "9600A162",
		"CodArtClienteFornitore": "7658-10-W30"
	},
	{
		"ARTICOLO": "9600A170",
		"CodArtClienteFornitore": "7625-00-52"
	},
	{
		"ARTICOLO": "9600A171",
		"CodArtClienteFornitore": "7625-00-W31"
	},
	{
		"ARTICOLO": "9600A172",
		"CodArtClienteFornitore": "7625-00-30"
	},
	{
		"ARTICOLO": "9600A210",
		"CodArtClienteFornitore": "7607-00-00"
	},
	{
		"ARTICOLO": "9600A211",
		"CodArtClienteFornitore": "7607-00-00"
	},
	{
		"ARTICOLO": "9600A217",
		"CodArtClienteFornitore": "7607-00-00"
	},
	{
		"ARTICOLO": "9600A221",
		"CodArtClienteFornitore": "7606-00-31"
	},
	{
		"ARTICOLO": "9600A222",
		"CodArtClienteFornitore": "7606-00-30"
	},
	{
		"ARTICOLO": "9600A230",
		"CodArtClienteFornitore": "7655-10-W20"
	},
	{
		"ARTICOLO": "9600A231",
		"CodArtClienteFornitore": "7655-10-W31"
	},
	{
		"ARTICOLO": "9600A232",
		"CodArtClienteFornitore": "7655-10-W30"
	},
	{
		"ARTICOLO": "9600A240",
		"CodArtClienteFornitore": "7656-10-W20"
	},
	{
		"ARTICOLO": "9600A241",
		"CodArtClienteFornitore": "7656-10-W31"
	},
	{
		"ARTICOLO": "9600A242",
		"CodArtClienteFornitore": "7656-10-W30"
	},
	{
		"ARTICOLO": "9600A250",
		"CodArtClienteFornitore": "7657-10-W20"
	},
	{
		"ARTICOLO": "9600A251",
		"CodArtClienteFornitore": "7657-10-W31"
	},
	{
		"ARTICOLO": "9600A252",
		"CodArtClienteFornitore": "7657-10-W30"
	},
	{
		"ARTICOLO": "9600A260",
		"CodArtClienteFornitore": "7653-10-W20"
	},
	{
		"ARTICOLO": "9600A261",
		"CodArtClienteFornitore": "7653-10-W31"
	},
	{
		"ARTICOLO": "9600A262",
		"CodArtClienteFornitore": "7653-10-W30"
	},
	{
		"ARTICOLO": "9600A270",
		"CodArtClienteFornitore": "7655-11-W20"
	},
	{
		"ARTICOLO": "9600A271",
		"CodArtClienteFornitore": "7655-11-W31"
	},
	{
		"ARTICOLO": "9600A272",
		"CodArtClienteFornitore": "7655-11-W30"
	},
	{
		"ARTICOLO": "9600A281",
		"CodArtClienteFornitore": "7624-00-W31"
	},
	{
		"ARTICOLO": "9600A282",
		"CodArtClienteFornitore": "7624-00-30"
	},
	{
		"ARTICOLO": "9600A301",
		"CodArtClienteFornitore": "7654-10-W31"
	},
	{
		"ARTICOLO": "9600A302",
		"CodArtClienteFornitore": "7654-10-W30"
	},
	{
		"ARTICOLO": "9600A310",
		"CodArtClienteFornitore": "7656-11-W20"
	},
	{
		"ARTICOLO": "9600A311",
		"CodArtClienteFornitore": "7656-11-W31"
	},
	{
		"ARTICOLO": "9600A312",
		"CodArtClienteFornitore": "7656-11-W30"
	},
	{
		"ARTICOLO": "9610A022",
		"CodArtClienteFornitore": "7512-20-30"
	},
	{
		"ARTICOLO": "9610A032",
		"CodArtClienteFornitore": "7512-30-30"
	},
	{
		"ARTICOLO": "9610A121",
		"CodArtClienteFornitore": "7662-10-W31"
	},
	{
		"ARTICOLO": "9610A152",
		"CodArtClienteFornitore": "7662-11-W30"
	},
	{
		"ARTICOLO": "214650",
		"CodArtClienteFornitore": "214650"
	},
	{
		"ARTICOLO": "214650AZZ",
		"CodArtClienteFornitore": "214650"
	},
	{
		"ARTICOLO": "238110",
		"CodArtClienteFornitore": "238110"
	},
	{
		"ARTICOLO": "238200",
		"CodArtClienteFornitore": "238200"
	},
	{
		"ARTICOLO": "297092",
		"CodArtClienteFornitore": "297092"
	},
	{
		"ARTICOLO": "427300",
		"CodArtClienteFornitore": "427300"
	},
	{
		"ARTICOLO": "427303",
		"CodArtClienteFornitore": "427303"
	},
	{
		"ARTICOLO": "622890",
		"CodArtClienteFornitore": "622890"
	},
	{
		"ARTICOLO": "622890BLU",
		"CodArtClienteFornitore": "622890"
	},
	{
		"ARTICOLO": "_x000D_"
	},
	{
		"ARTICOLO": "28089602786",
		"CodArtClienteFornitore": "89602786"
	},
	{
		"ARTICOLO": "28022176000",
		"CodArtClienteFornitore": "22176000"
	},
	{
		"ARTICOLO": "28022176183",
		"CodArtClienteFornitore": "22176183"
	},
	{
		"ARTICOLO": "28022176185",
		"CodArtClienteFornitore": "22176185"
	},
	{
		"ARTICOLO": "28022176186",
		"CodArtClienteFornitore": "22186186"
	},
	{
		"ARTICOLO": "28022185153",
		"CodArtClienteFornitore": "22185153"
	},
	{
		"ARTICOLO": "28022185154",
		"CodArtClienteFornitore": "22185154"
	},
	{
		"ARTICOLO": "28022185156",
		"CodArtClienteFornitore": "22185156"
	},
	{
		"ARTICOLO": "28022185209",
		"CodArtClienteFornitore": "22185209"
	},
	{
		"ARTICOLO": "28022185211",
		"CodArtClienteFornitore": "22185211"
	},
	{
		"ARTICOLO": "28022185227",
		"CodArtClienteFornitore": "22185227"
	},
	{
		"ARTICOLO": "28028000125",
		"CodArtClienteFornitore": "28000125"
	},
	{
		"ARTICOLO": "28028000411",
		"CodArtClienteFornitore": "28028000411"
	},
	{
		"ARTICOLO": "28028000412",
		"CodArtClienteFornitore": "28000412"
	},
	{
		"ARTICOLO": "28028000413",
		"CodArtClienteFornitore": "28000413"
	},
	{
		"ARTICOLO": "28028000414",
		"CodArtClienteFornitore": "28000414"
	},
	{
		"ARTICOLO": "28028000675",
		"CodArtClienteFornitore": "28000675"
	},
	{
		"ARTICOLO": "28028000694",
		"CodArtClienteFornitore": "28000694"
	},
	{
		"ARTICOLO": "28028000695",
		"CodArtClienteFornitore": "28000695"
	},
	{
		"ARTICOLO": "28028000696",
		"CodArtClienteFornitore": "28000696"
	},
	{
		"ARTICOLO": "28028000700",
		"CodArtClienteFornitore": "28000700"
	},
	{
		"ARTICOLO": "28028000702",
		"CodArtClienteFornitore": "28000702"
	},
	{
		"ARTICOLO": "28028000730",
		"CodArtClienteFornitore": "28000730"
	},
	{
		"ARTICOLO": "28028000731",
		"CodArtClienteFornitore": "28000731"
	},
	{
		"ARTICOLO": "28028000947",
		"CodArtClienteFornitore": "28000947"
	},
	{
		"ARTICOLO": "28028001044",
		"CodArtClienteFornitore": "28001044"
	},
	{
		"ARTICOLO": "28028001045",
		"CodArtClienteFornitore": "28001045"
	},
	{
		"ARTICOLO": "28028001114",
		"CodArtClienteFornitore": "28001114"
	},
	{
		"ARTICOLO": "28028001116",
		"CodArtClienteFornitore": "28001116"
	},
	{
		"ARTICOLO": "28028001118",
		"CodArtClienteFornitore": "28001118"
	},
	{
		"ARTICOLO": "28028001120",
		"CodArtClienteFornitore": "28001120"
	},
	{
		"ARTICOLO": "28028001253",
		"CodArtClienteFornitore": "28001253"
	},
	{
		"ARTICOLO": "28028001582",
		"CodArtClienteFornitore": "28001582"
	},
	{
		"ARTICOLO": "28028001583",
		"CodArtClienteFornitore": "28001583"
	},
	{
		"ARTICOLO": "28028001585",
		"CodArtClienteFornitore": "28001585"
	},
	{
		"ARTICOLO": "28028001666",
		"CodArtClienteFornitore": "28001666"
	},
	{
		"ARTICOLO": "28028001771",
		"CodArtClienteFornitore": "28001771"
	},
	{
		"ARTICOLO": "28087500188",
		"CodArtClienteFornitore": "87500188"
	},
	{
		"ARTICOLO": "28087500189",
		"CodArtClienteFornitore": "87500189"
	},
	{
		"ARTICOLO": "28087500196",
		"CodArtClienteFornitore": "87500196"
	},
	{
		"ARTICOLO": "28087500197",
		"CodArtClienteFornitore": "87500197"
	},
	{
		"ARTICOLO": "28087500210",
		"CodArtClienteFornitore": "87500210"
	},
	{
		"ARTICOLO": "28087500246",
		"CodArtClienteFornitore": "87500246"
	},
	{
		"ARTICOLO": "28087500344",
		"CodArtClienteFornitore": "87500344"
	},
	{
		"ARTICOLO": "28087500553",
		"CodArtClienteFornitore": "87500553"
	},
	{
		"ARTICOLO": "28087500625",
		"CodArtClienteFornitore": "87500625"
	},
	{
		"ARTICOLO": "28089600876",
		"CodArtClienteFornitore": "89600876"
	},
	{
		"ARTICOLO": "28089602198",
		"CodArtClienteFornitore": "89602198"
	},
	{
		"ARTICOLO": "28089602216",
		"CodArtClienteFornitore": "89602216"
	},
	{
		"ARTICOLO": "28089602217",
		"CodArtClienteFornitore": "89602217"
	},
	{
		"ARTICOLO": "28089602218",
		"CodArtClienteFornitore": "89602218"
	},
	{
		"ARTICOLO": "28089602219",
		"CodArtClienteFornitore": "89602219"
	},
	{
		"ARTICOLO": "28089602220",
		"CodArtClienteFornitore": "89602220"
	},
	{
		"ARTICOLO": "28089602221",
		"CodArtClienteFornitore": "89602221"
	},
	{
		"ARTICOLO": "28089602222",
		"CodArtClienteFornitore": "89602222"
	},
	{
		"ARTICOLO": "28089602223",
		"CodArtClienteFornitore": "89602223"
	},
	{
		"ARTICOLO": "28089602232",
		"CodArtClienteFornitore": "89602232"
	},
	{
		"ARTICOLO": "28089602236",
		"CodArtClienteFornitore": "89602236"
	},
	{
		"ARTICOLO": "28089602262",
		"CodArtClienteFornitore": "89602262"
	},
	{
		"ARTICOLO": "28089602276",
		"CodArtClienteFornitore": "89602276"
	},
	{
		"ARTICOLO": "28089602282",
		"CodArtClienteFornitore": "89602282"
	},
	{
		"ARTICOLO": "28089602283",
		"CodArtClienteFornitore": "89602283"
	},
	{
		"ARTICOLO": "28089602284",
		"CodArtClienteFornitore": "89602284"
	},
	{
		"ARTICOLO": "28089602285",
		"CodArtClienteFornitore": "89602285"
	},
	{
		"ARTICOLO": "28089602286",
		"CodArtClienteFornitore": "89602286"
	},
	{
		"ARTICOLO": "28089602287",
		"CodArtClienteFornitore": "89602287"
	},
	{
		"ARTICOLO": "28089602291",
		"CodArtClienteFornitore": "89602291"
	},
	{
		"ARTICOLO": "28089602292",
		"CodArtClienteFornitore": "89602292"
	},
	{
		"ARTICOLO": "28089602640",
		"CodArtClienteFornitore": "89602640"
	},
	{
		"ARTICOLO": "28089602655",
		"CodArtClienteFornitore": "89602655"
	},
	{
		"ARTICOLO": "28089602656",
		"CodArtClienteFornitore": "89602656"
	},
	{
		"ARTICOLO": "28089602657",
		"CodArtClienteFornitore": "89602657"
	},
	{
		"ARTICOLO": "28089602658",
		"CodArtClienteFornitore": "89602658"
	},
	{
		"ARTICOLO": "28089602659",
		"CodArtClienteFornitore": "89602659"
	},
	{
		"ARTICOLO": "28089602660",
		"CodArtClienteFornitore": "89602660"
	},
	{
		"ARTICOLO": "28089602661",
		"CodArtClienteFornitore": "89602661"
	},
	{
		"ARTICOLO": "28089602662",
		"CodArtClienteFornitore": "89602662"
	},
	{
		"ARTICOLO": "28089602663",
		"CodArtClienteFornitore": "89602663"
	},
	{
		"ARTICOLO": "28089602664",
		"CodArtClienteFornitore": "89602664"
	},
	{
		"ARTICOLO": "28089602666",
		"CodArtClienteFornitore": "89602666"
	},
	{
		"ARTICOLO": "28089602667",
		"CodArtClienteFornitore": "89602667"
	},
	{
		"ARTICOLO": "28089602668",
		"CodArtClienteFornitore": "89602668"
	},
	{
		"ARTICOLO": "28089602669",
		"CodArtClienteFornitore": "89602669"
	},
	{
		"ARTICOLO": "28089602670",
		"CodArtClienteFornitore": "89602670"
	},
	{
		"ARTICOLO": "28089602685",
		"CodArtClienteFornitore": "89602685"
	},
	{
		"ARTICOLO": "28089602687",
		"CodArtClienteFornitore": "89602687"
	},
	{
		"ARTICOLO": "28089602688",
		"CodArtClienteFornitore": "89602688"
	},
	{
		"ARTICOLO": "28089602689",
		"CodArtClienteFornitore": "89602689"
	},
	{
		"ARTICOLO": "28089602691",
		"CodArtClienteFornitore": "89602691"
	},
	{
		"ARTICOLO": "28089602692",
		"CodArtClienteFornitore": "89602692"
	},
	{
		"ARTICOLO": "28089602694",
		"CodArtClienteFornitore": "89602694"
	},
	{
		"ARTICOLO": "28089602695",
		"CodArtClienteFornitore": "89602695"
	},
	{
		"ARTICOLO": "28089602697",
		"CodArtClienteFornitore": "89602697"
	},
	{
		"ARTICOLO": "28089602724",
		"CodArtClienteFornitore": "89602724"
	},
	{
		"ARTICOLO": "28089602725",
		"CodArtClienteFornitore": "89602725"
	},
	{
		"ARTICOLO": "28089602726",
		"CodArtClienteFornitore": "89602726"
	},
	{
		"ARTICOLO": "28089602727",
		"CodArtClienteFornitore": "89602727"
	},
	{
		"ARTICOLO": "28089602784",
		"CodArtClienteFornitore": "89602784"
	},
	{
		"ARTICOLO": "28089602786",
		"CodArtClienteFornitore": "89602786"
	},
	{
		"ARTICOLO": "28089602788",
		"CodArtClienteFornitore": "89602788"
	},
	{
		"ARTICOLO": "28089602794",
		"CodArtClienteFornitore": "89602794"
	},
	{
		"ARTICOLO": "28089602796",
		"CodArtClienteFornitore": "89602796"
	},
	{
		"ARTICOLO": "28089602851",
		"CodArtClienteFornitore": "89602851"
	},
	{
		"ARTICOLO": "28089602880",
		"CodArtClienteFornitore": "89602880"
	},
	{
		"ARTICOLO": "28089602881",
		"CodArtClienteFornitore": "89602881"
	},
	{
		"ARTICOLO": "28089602914",
		"CodArtClienteFornitore": "89602914"
	},
	{
		"ARTICOLO": "28089602966",
		"CodArtClienteFornitore": "89602966"
	},
	{
		"ARTICOLO": "280LES1584-G5",
		"CodArtClienteFornitore": "89602199"
	},
	{
		"ARTICOLO": "280LES19-G4",
		"CodArtClienteFornitore": "89602297"
	},
	{
		"ARTICOLO": "280LES19-G5GOLD+",
		"CodArtClienteFornitore": "89602288"
	},
	{
		"ARTICOLO": "280LES1982-G5",
		"CodArtClienteFornitore": "89602262"
	},
	{
		"ARTICOLO": "280LES1983-G5",
		"CodArtClienteFornitore": "89602220"
	},
	{
		"ARTICOLO": "280LES1984-G5",
		"CodArtClienteFornitore": "89602221"
	},
	{
		"ARTICOLO": "280LES1993-G5",
		"CodArtClienteFornitore": "89602222"
	},
	{
		"ARTICOLO": "280LES1994",
		"CodArtClienteFornitore": "89601933"
	},
	{
		"ARTICOLO": "280LES1994-G5",
		"CodArtClienteFornitore": "89602223"
	},
	{
		"ARTICOLO": "280LES2383-G5",
		"CodArtClienteFornitore": "89602232"
	},
	{
		"ARTICOLO": "280LES2384-G5",
		"CodArtClienteFornitore": "89602233"
	},
	{
		"ARTICOLO": "280LES2393-G5",
		"CodArtClienteFornitore": "89602234"
	},
	{
		"ARTICOLO": "280LES2394-G5",
		"CodArtClienteFornitore": "89602235"
	},
	{
		"ARTICOLO": "89602219",
		"CodArtClienteFornitore": "89602219"
	},
	{
		"ARTICOLO": "9000M035",
		"CodArtClienteFornitore": "28028000411"
	},
	{
		"ARTICOLO": "9000M100",
		"CodArtClienteFornitore": "28000413"
	},
	{
		"ARTICOLO": "V504011",
		"CodArtClienteFornitore": "89800538"
	},
	{
		"ARTICOLO": "V504031",
		"CodArtClienteFornitore": "89800540"
	},
	{
		"ARTICOLO": "NT20N MM200",
		"CodArtClienteFornitore": "NT20N MM200"
	},
	{
		"ARTICOLO": "NT20NMM200",
		"CodArtClienteFornitore": "NT20N MM200"
	},
	{
		"ARTICOLO": "126ETPARR105",
		"CodArtClienteFornitore": "4008321111579"
	},
	{
		"ARTICOLO": "126ETPARR70",
		"CodArtClienteFornitore": "4008321111593"
	},
	{
		"ARTICOLO": "4008321049629",
		"CodArtClienteFornitore": "4008321049629"
	},
	{
		"ARTICOLO": "4008321204219",
		"CodArtClienteFornitore": "4008321204219"
	},
	{
		"ARTICOLO": "4008321352071",
		"CodArtClienteFornitore": "4008321352071"
	},
	{
		"ARTICOLO": "4008321528285",
		"CodArtClienteFornitore": "4008321528285"
	},
	{
		"ARTICOLO": "4008321955869",
		"CodArtClienteFornitore": "4008321955869"
	},
	{
		"ARTICOLO": "4008321981721",
		"CodArtClienteFornitore": "4008321981721"
	},
	{
		"ARTICOLO": "400PTI150S",
		"CodArtClienteFornitore": "4008321188090"
	},
	{
		"ARTICOLO": "400PTI20/S",
		"CodArtClienteFornitore": "4008321353290"
	},
	{
		"ARTICOLO": "400PTI35S",
		"CodArtClienteFornitore": "4008321073112"
	},
	{
		"ARTICOLO": "400PTI70S",
		"CodArtClienteFornitore": "4008321049629"
	},
	{
		"ARTICOLO": "400QTPDL2X55N",
		"CodArtClienteFornitore": "4008321390172"
	},
	{
		"ARTICOLO": "4050300008110",
		"CodArtClienteFornitore": "4050300008110"
	},
	{
		"ARTICOLO": "4050300010595",
		"CodArtClienteFornitore": "4050300010595"
	},
	{
		"ARTICOLO": "4050300010625",
		"CodArtClienteFornitore": "4050300010625"
	},
	{
		"ARTICOLO": "4050300017594",
		"CodArtClienteFornitore": "4050300017594"
	},
	{
		"ARTICOLO": "4050300024400",
		"CodArtClienteFornitore": "4050300024400"
	},
	{
		"ARTICOLO": "4050300025681",
		"CodArtClienteFornitore": "4050300025681"
	},
	{
		"ARTICOLO": "4050300025698",
		"CodArtClienteFornitore": "4050300025698"
	},
	{
		"ARTICOLO": "4050300299037",
		"CodArtClienteFornitore": "4050300299037"
	},
	{
		"ARTICOLO": "4050300351551",
		"CodArtClienteFornitore": "4050300351551"
	},
	{
		"ARTICOLO": "4050300389059",
		"CodArtClienteFornitore": "4050300389059"
	},
	{
		"ARTICOLO": "4050300446165",
		"CodArtClienteFornitore": "4050300446165"
	},
	{
		"ARTICOLO": "4050300591384",
		"CodArtClienteFornitore": "4050300591384"
	},
	{
		"ARTICOLO": "4050300591643",
		"CodArtClienteFornitore": "4050300591643"
	},
	{
		"ARTICOLO": "4052899188662",
		"CodArtClienteFornitore": "4052899188662"
	},
	{
		"ARTICOLO": "4052899253438",
		"CodArtClienteFornitore": "4052899253438"
	},
	{
		"ARTICOLO": "4052899259027",
		"CodArtClienteFornitore": "4052899259027"
	},
	{
		"ARTICOLO": "4052899259041",
		"CodArtClienteFornitore": "4052899259041"
	},
	{
		"ARTICOLO": "4052899323858",
		"CodArtClienteFornitore": "4052899323858"
	},
	{
		"ARTICOLO": "4052899435650",
		"CodArtClienteFornitore": "4052899435650"
	},
	{
		"ARTICOLO": "4052899947122",
		"CodArtClienteFornitore": "4052899947122"
	},
	{
		"ARTICOLO": "4052899947139",
		"CodArtClienteFornitore": "4052899947139"
	},
	{
		"ARTICOLO": "4052899952775",
		"CodArtClienteFornitore": "4052899952775"
	},
	{
		"ARTICOLO": "5001100010100",
		"CodArtClienteFornitore": "4050300272672"
	},
	{
		"ARTICOLO": "5001100036100",
		"CodArtClienteFornitore": "4050300272795"
	},
	{
		"ARTICOLO": "5001200006100",
		"CodArtClienteFornitore": "4050300011776"
	},
	{
		"ARTICOLO": "5001200024100",
		"CodArtClienteFornitore": "4050300011783"
	},
	{
		"ARTICOLO": "5001200040100",
		"CodArtClienteFornitore": "4050300011790"
	},
	{
		"ARTICOLO": "5001300024100",
		"CodArtClienteFornitore": "4050300358628"
	},
	{
		"ARTICOLO": "500448302P002",
		"CodArtClienteFornitore": "4050300025698"
	},
	{
		"ARTICOLO": "500448402P002",
		"CodArtClienteFornitore": "4050300010625"
	},
	{
		"ARTICOLO": "500458302P002",
		"CodArtClienteFornitore": "4050300025704"
	},
	{
		"ARTICOLO": "500458402P002",
		"CodArtClienteFornitore": "4050300012056"
	},
	{
		"ARTICOLO": "500468302P002",
		"CodArtClienteFornitore": "4050300025711"
	},
	{
		"ARTICOLO": "500468402P002",
		"CodArtClienteFornitore": "4050300012049"
	},
	{
		"ARTICOLO": "500508304P002",
		"CodArtClienteFornitore": "4050300327235"
	},
	{
		"ARTICOLO": "500508404P002",
		"CodArtClienteFornitore": "4050300020303"
	},
	{
		"ARTICOLO": "500528304P100",
		"CodArtClienteFornitore": "4050300425641"
	},
	{
		"ARTICOLO": "500528404P100",
		"CodArtClienteFornitore": "4050300425627"
	},
	{
		"ARTICOLO": "500DF36840",
		"CodArtClienteFornitore": "4050300299037"
	},
	{
		"ARTICOLO": "500DINTSENS15827",
		"CodArtClienteFornitore": "4008321987082"
	},
	{
		"ARTICOLO": "500HALOTT350",
		"CodArtClienteFornitore": "4008321204219"
	},
	{
		"ARTICOLO": "500HQIT2000DIN",
		"CodArtClienteFornitore": "924058817161"
	},
	{
		"ARTICOLO": "500Q58404P002",
		"CodArtClienteFornitore": "4050300017617"
	},
	{
		"ARTICOLO": "500Q68404P002",
		"CodArtClienteFornitore": "4050300348568"
	},
	{
		"ARTICOLO": "500R783000000",
		"CodArtClienteFornitore": "4008321678300"
	},
	{
		"ARTICOLO": "500R884000000",
		"CodArtClienteFornitore": "4008321678386"
	},
	{
		"ARTICOLO": "500S9930001EL",
		"CodArtClienteFornitore": "4008321681812"
	},
	{
		"ARTICOLO": "500W500040100",
		"CodArtClienteFornitore": "4008321909237"
	},
	{
		"ARTICOLO": "500X400024100",
		"CodArtClienteFornitore": "4050300011769"
	},
	{
		"ARTICOLO": "500X400040100",
		"CodArtClienteFornitore": "4008321909213"
	},
	{
		"ARTICOLO": "500X6930001EL",
		"CodArtClienteFornitore": "4008321681850"
	},
	{
		"ARTICOLO": "9000E09",
		"CodArtClienteFornitore": "4052899947122"
	},
	{
		"ARTICOLO": "95001M0",
		"CodArtClienteFornitore": "4052899324015"
	},
	{
		"ARTICOLO": "95001N0",
		"CodArtClienteFornitore": "4052899324077"
	},
	{
		"ARTICOLO": "95002A0",
		"CodArtClienteFornitore": "4052899165533"
	},
	{
		"ARTICOLO": "95002D0",
		"CodArtClienteFornitore": "4052899165571"
	},
	{
		"ARTICOLO": "95002E0",
		"CodArtClienteFornitore": "4052899165656"
	},
	{
		"ARTICOLO": "95002H0",
		"CodArtClienteFornitore": "4052899324039"
	},
	{
		"ARTICOLO": "95002K0",
		"CodArtClienteFornitore": "4052899324091"
	},
	{
		"ARTICOLO": "95003B0",
		"CodArtClienteFornitore": "4052899165632"
	},
	{
		"ARTICOLO": "95003D0",
		"CodArtClienteFornitore": "4052899165595"
	},
	{
		"ARTICOLO": "95003E0",
		"CodArtClienteFornitore": "4052899165670"
	},
	{
		"ARTICOLO": "95003L0",
		"CodArtClienteFornitore": "4052899324121"
	},
	{
		"ARTICOLO": "95502XF",
		"CodArtClienteFornitore": "4052899276604"
	},
	{
		"ARTICOLO": "95502XN",
		"CodArtClienteFornitore": "4052899276642"
	},
	{
		"ARTICOLO": "B02583169",
		"CodArtClienteFornitore": "B02583169"
	},
	{
		"ARTICOLO": "B02583170",
		"CodArtClienteFornitore": "B02583170"
	},
	{
		"ARTICOLO": "A13117EN1T",
		"CodArtClienteFornitore": "A13117EN1T"
	},
	{
		"ARTICOLO": "A1357EN1T",
		"CodArtClienteFornitore": "A1357EN1T"
	},
	{
		"ARTICOLO": "A1387EN1T",
		"CodArtClienteFornitore": "A1387EN1T"
	},
	{
		"ARTICOLO": "A23R08MINP48",
		"CodArtClienteFornitore": "A23R08MINP48"
	},
	{
		"ARTICOLO": "A4A11-J20",
		"CodArtClienteFornitore": "A4A11-J20"
	},
	{
		"ARTICOLO": "A4A8-320",
		"CodArtClienteFornitore": "A4A8-320"
	},
	{
		"ARTICOLO": "A4E16NRGBBWLCT",
		"CodArtClienteFornitore": "A4E16NRGBBWLCT"
	},
	{
		"ARTICOLO": "A4R3IWL20",
		"CodArtClienteFornitore": "A4R3IWL20"
	},
	{
		"ARTICOLO": "A4S16IRGBLPT",
		"CodArtClienteFornitore": "A4S16IRGBLPT"
	},
	{
		"ARTICOLO": "ACFC70",
		"CodArtClienteFornitore": "ACFC70"
	},
	{
		"ARTICOLO": "M109ND",
		"CodArtClienteFornitore": "M109ND"
	},
	{
		"ARTICOLO": "M140BOG",
		"CodArtClienteFornitore": "M140BOG"
	},
	{
		"ARTICOLO": "M141BOQ",
		"CodArtClienteFornitore": "M141BOQ"
	},
	{
		"ARTICOLO": "MMTRNL08",
		"CodArtClienteFornitore": "MMTRNL08"
	},
	{
		"ARTICOLO": "SH31N40G35J10G2",
		"CodArtClienteFornitore": "SH31N40G35J10G2"
	},
	{
		"ARTICOLO": "EMMU 12",
		"CodArtClienteFornitore": "EMMU 12"
	},
	{
		"ARTICOLO": "EVPS 12",
		"CodArtClienteFornitore": "EVPS 12"
	},
	{
		"ARTICOLO": "3AMELP030V0240LS",
		"CodArtClienteFornitore": "3AMELP030V0240LS"
	},
	{
		"ARTICOLO": "3AML440MA04T2A01",
		"CodArtClienteFornitore": "3AML440MA04T2A01"
	},
	{
		"ARTICOLO": "3SLCFA2WW1512E00",
		"CodArtClienteFornitore": "3SLCFA2WW1512E00"
	},
	{
		"ARTICOLO": "4CTM1865279036TW",
		"CodArtClienteFornitore": "4CTM1865279036TW"
	},
	{
		"ARTICOLO": "L308MC10C1A00",
		"CodArtClienteFornitore": "L308MC10C1A00"
	},
	{
		"ARTICOLO": "L408MA10C1A00",
		"CodArtClienteFornitore": "L408MA10C1A00"
	},
	{
		"ARTICOLO": "0786000U",
		"CodArtClienteFornitore": "SCQ-83"
	},
	{
		"ARTICOLO": "0796000U",
		"CodArtClienteFornitore": "SC-HAMMER-86"
	},
	{
		"ARTICOLO": "0797000U",
		"CodArtClienteFornitore": "SC-HAMMER-122"
	},
	{
		"ARTICOLO": "0798000U",
		"CodArtClienteFornitore": "SC-HAMMER-162"
	},
	{
		"ARTICOLO": "0799000U",
		"CodArtClienteFornitore": "SC-184"
	},
	{
		"ARTICOLO": "D76-TR",
		"CodArtClienteFornitore": "D76-TR"
	},
	{
		"ARTICOLO": "SC-HAMMER-162",
		"CodArtClienteFornitore": "SC-HAMMER-162"
	},
	{
		"ARTICOLO": "SC-HAMMER-86",
		"CodArtClienteFornitore": "SC-HAMMER-86"
	},
	{
		"ARTICOLO": "9000MPL3",
		"CodArtClienteFornitore": "MPLI031"
	},
	{
		"ARTICOLO": "9000PBOX",
		"CodArtClienteFornitore": "A40PBOX1500B"
	},
	{
		"ARTICOLO": "9000PDL",
		"CodArtClienteFornitore": "A40PDL433RGBB"
	},
	{
		"ARTICOLO": "9000PIR",
		"CodArtClienteFornitore": "A40PIRSW0010"
	},
	{
		"ARTICOLO": "9000QTT",
		"CodArtClienteFornitore": "A40QTT100000"
	},
	{
		"ARTICOLO": "9000QTT1CT",
		"CodArtClienteFornitore": "A400QTT1CT000"
	},
	{
		"ARTICOLO": "9000RC43",
		"CodArtClienteFornitore": "A40RC433RGBN"
	},
	{
		"ARTICOLO": "9000RGBX35",
		"CodArtClienteFornitore": "A40RGBOX350B"
	},
	{
		"ARTICOLO": "9000RGBX45",
		"CodArtClienteFornitore": "A40RGBOX450B"
	},
	{
		"ARTICOLO": "9000RGBX55",
		"CodArtClienteFornitore": "A40RGBOX550B"
	},
	{
		"ARTICOLO": "9000RPDL",
		"CodArtClienteFornitore": "A40PWMREP00B"
	},
	{
		"ARTICOLO": "9000WALL",
		"CodArtClienteFornitore": "A40WALLRGBON"
	},
	{
		"ARTICOLO": "9000WIFI",
		"CodArtClienteFornitore": "A40WIFIRGBON"
	},
	{
		"ARTICOLO": "A40IR2R00000",
		"CodArtClienteFornitore": "A40IR2R00000"
	},
	{
		"ARTICOLO": "A40IR3T00000",
		"CodArtClienteFornitore": "A40IR3T00000"
	},
	{
		"ARTICOLO": "A40KRGBOX350B",
		"CodArtClienteFornitore": "A40KRGBOX350B"
	},
	{
		"ARTICOLO": "A40KRGBOX35B",
		"CodArtClienteFornitore": "A40KRGBOX35B"
	},
	{
		"ARTICOLO": "A40PDL43RGBB",
		"CodArtClienteFornitore": "A40PDL43RGBB"
	},
	{
		"ARTICOLO": "A40PLK12000B",
		"CodArtClienteFornitore": "A40PLK12000B"
	},
	{
		"ARTICOLO": "A40RGBOX450B",
		"CodArtClienteFornitore": "A40RGBOX450B"
	},
	{
		"ARTICOLO": "A40TRIOCPW00",
		"CodArtClienteFornitore": "A40TRIOCPW00"
	},
	{
		"ARTICOLO": "A40TRIOCPWOO",
		"CodArtClienteFornitore": "A40TRIOCPW00"
	},
	{
		"ARTICOLO": "A40TZ1650N00",
		"CodArtClienteFornitore": "A40TZ1650N00"
	},
	{
		"ARTICOLO": "A41ST12600RG",
		"CodArtClienteFornitore": "A41ST12600RG"
	},
	{
		"ARTICOLO": "A70PIR5B000B",
		"CodArtClienteFornitore": "A70PIR5B000B"
	},
	{
		"ARTICOLO": "RES1K",
		"CodArtClienteFornitore": "RESISTENZA1K"
	},
	{
		"ARTICOLO": "ST060RGB",
		"CodArtClienteFornitore": "A41ST24600RG"
	},
	{
		"ARTICOLO": "ST120RGBA",
		"CodArtClienteFornitore": "A41524120RAO"
	},
	{
		"ARTICOLO": "ST120RGBW",
		"CodArtClienteFornitore": "A41S24120RW0"
	},
	{
		"ARTICOLO": "ST21120E27",
		"CodArtClienteFornitore": "ST21120E27"
	},
	{
		"ARTICOLO": "ST2460",
		"CodArtClienteFornitore": "ST2460"
	},
	{
		"ARTICOLO": "ST2460CW",
		"CodArtClienteFornitore": "ST2460CW"
	},
	{
		"ARTICOLO": "ST60IP68RGB",
		"CodArtClienteFornitore": "A416T24601SR"
	},
	{
		"ARTICOLO": "ST60RGB",
		"CodArtClienteFornitore": "A41STR24600RG"
	},
	{
		"ARTICOLO": "STIP60RGB",
		"CodArtClienteFornitore": "A41ST2460ASR"
	},
	{
		"ARTICOLO": "STIP65RGB",
		"CodArtClienteFornitore": "A41ST2460ASR"
	},
	{
		"ARTICOLO": "ZCB056000000",
		"CodArtClienteFornitore": "ZCB056000000"
	},
	{
		"ARTICOLO": "ZCB069000000",
		"CodArtClienteFornitore": "ZCB069000000"
	},
	{
		"ARTICOLO": "0604BOX.00",
		"CodArtClienteFornitore": "DL604/BOX.00"
	},
	{
		"ARTICOLO": "0608BOX.00",
		"CodArtClienteFornitore": "DL608/BOX.00"
	},
	{
		"ARTICOLO": "0609BOX.00",
		"CodArtClienteFornitore": "DL609/BOX.00"
	},
	{
		"ARTICOLO": "0609COR.00",
		"CodArtClienteFornitore": "DL609/C.00 "
	},
	{
		"ARTICOLO": "0611BOX.00",
		"CodArtClienteFornitore": "0611BOX.00"
	},
	{
		"ARTICOLO": "0611COR.00",
		"CodArtClienteFornitore": "0611COR.00"
	},
	{
		"ARTICOLO": "0672207X",
		"CodArtClienteFornitore": "0672207X"
	},
	{
		"ARTICOLO": "0732205X",
		"CodArtClienteFornitore": "0732205X"
	},
	{
		"ARTICOLO": "078900W0",
		"CodArtClienteFornitore": "078900W0"
	},
	{
		"ARTICOLO": "0798270X",
		"CodArtClienteFornitore": "0798270X"
	},
	{
		"ARTICOLO": "1255CAS+STAF.00",
		"CodArtClienteFornitore": "B.165.A.85.2"
	},
	{
		"ARTICOLO": "1256CAS+STAF.00",
		"CodArtClienteFornitore": "B.165.A.85.1"
	},
	{
		"ARTICOLO": "2666255X",
		"CodArtClienteFornitore": "2666255X"
	},
	{
		"ARTICOLO": "4621/1.00",
		"CodArtClienteFornitore": "4621/1.00"
	},
	{
		"ARTICOLO": "4712SPECX",
		"CodArtClienteFornitore": "4712SPECX"
	},
	{
		"ARTICOLO": "4714/1.00",
		"CodArtClienteFornitore": "4714/1.00"
	},
	{
		"ARTICOLO": "4714/2.00",
		"CodArtClienteFornitore": "4714/2.00"
	},
	{
		"ARTICOLO": "4714/3.00",
		"CodArtClienteFornitore": "4714/3.00"
	},
	{
		"ARTICOLO": "5014712/1.00",
		"CodArtClienteFornitore": "ILM-SL0063"
	},
	{
		"ARTICOLO": "5014712/2.00",
		"CodArtClienteFornitore": "ILM-SL0061"
	},
	{
		"ARTICOLO": "5014712/3.00",
		"CodArtClienteFornitore": "ILM-SL0062"
	},
	{
		"ARTICOLO": "B.238.A.11",
		"CodArtClienteFornitore": "B.238.A.11"
	},
	{
		"ARTICOLO": "B.238.C.11",
		"CodArtClienteFornitore": "B.238.C.11"
	},
	{
		"ARTICOLO": "COPRIBASE1004",
		"CodArtClienteFornitore": "COPRIBASE1004"
	},
	{
		"ARTICOLO": "COR-CART0608",
		"CodArtClienteFornitore": "COR-CART0608"
	},
	{
		"ARTICOLO": "COR-CART0609",
		"CodArtClienteFornitore": "COR-CART0609"
	},
	{
		"ARTICOLO": "COR-CART0610",
		"CodArtClienteFornitore": "COR-CART0610"
	},
	{
		"ARTICOLO": "COR-CART0611",
		"CodArtClienteFornitore": "COR-CART0611"
	},
	{
		"ARTICOLO": "DIALCOP.00",
		"CodArtClienteFornitore": "DIALCOP.00"
	},
	{
		"ARTICOLO": "DL-609/C.00",
		"CodArtClienteFornitore": "DL-609/C.00"
	},
	{
		"ARTICOLO": "DPE0609SPEC",
		"CodArtClienteFornitore": "DPE0609SPEC"
	},
	{
		"ARTICOLO": "ILM-AS0234",
		"CodArtClienteFornitore": "ILM-AS0234"
	},
	{
		"ARTICOLO": "ILM-SL0169",
		"CodArtClienteFornitore": "ILM-SL0169"
	},
	{
		"ARTICOLO": "ILM-SL0170",
		"CodArtClienteFornitore": "ILM-SL0170"
	},
	{
		"ARTICOLO": "ILM-SL0221",
		"CodArtClienteFornitore": "ILM-SL0221"
	},
	{
		"ARTICOLO": "ILM-SL0236",
		"CodArtClienteFornitore": "ILM-SL0236"
	},
	{
		"ARTICOLO": "ILMSL0242",
		"CodArtClienteFornitore": "ILMSL0242"
	},
	{
		"ARTICOLO": "ILMSL0243",
		"CodArtClienteFornitore": "ILMSL0243"
	},
	{
		"ARTICOLO": "PZZ0610BOX.00",
		"CodArtClienteFornitore": "PZZ0610BOX.00"
	},
	{
		"ARTICOLO": "PZZ0610COR.00",
		"CodArtClienteFornitore": "PZZ0610COR.00"
	},
	{
		"ARTICOLO": "STAF-BOBSTD1",
		"CodArtClienteFornitore": "STAF-BOBSTD1"
	},
	{
		"ARTICOLO": "STAF-BOBSTD2",
		"CodArtClienteFornitore": "STAF-BOBSTD2"
	},
	{
		"ARTICOLO": "STAF_HAMM",
		"CodArtClienteFornitore": "STAF_HAMM"
	},
	{
		"ARTICOLO": "10033",
		"CodArtClienteFornitore": "10033"
	},
	{
		"ARTICOLO": "1804/2000",
		"CodArtClienteFornitore": "1804/2000"
	},
	{
		"ARTICOLO": "1804/5000",
		"CodArtClienteFornitore": "1804/5000"
	},
	{
		"ARTICOLO": "5021 1 118",
		"CodArtClienteFornitore": "5021 1 118"
	},
	{
		"ARTICOLO": "5036 8 1 16",
		"CodArtClienteFornitore": "5036/8/1/16"
	},
	{
		"ARTICOLO": "5590/34/280",
		"CodArtClienteFornitore": "5590/34/280"
	},
	{
		"ARTICOLO": "5905A////011",
		"CodArtClienteFornitore": "5905A////011"
	},
	{
		"ARTICOLO": "5915A////011",
		"CodArtClienteFornitore": "5915A////011"
	},
	{
		"ARTICOLO": "5922 18 26",
		"CodArtClienteFornitore": "5922 18 26"
	},
	{
		"ARTICOLO": "9605A357",
		"CodArtClienteFornitore": "ST-006"
	},
	{
		"ARTICOLO": "9605A367",
		"CodArtClienteFornitore": "804/C20"
	},
	{
		"ARTICOLO": "9605A397",
		"CodArtClienteFornitore": "1804/2000"
	},
	{
		"ARTICOLO": "9625A737",
		"CodArtClienteFornitore": "STR/BER"
	},
	{
		"ARTICOLO": "CA2000/86",
		"CodArtClienteFornitore": "CA2000/86"
	},
	{
		"ARTICOLO": "FUNECM60",
		"CodArtClienteFornitore": "FUNECM60"
	},
	{
		"ARTICOLO": "M55501100",
		"CodArtClienteFornitore": "M55501100"
	},
	{
		"ARTICOLO": "ST006",
		"CodArtClienteFornitore": "ST006"
	},
	{
		"ARTICOLO": "STR/BER",
		"CodArtClienteFornitore": "STR/BER"
	},
	{
		"ARTICOLO": "T11002161",
		"CodArtClienteFornitore": "T11.00.21-61"
	},
	{
		"ARTICOLO": "500H4",
		"CodArtClienteFornitore": "LDRC0927MU1EUD2"
	},
	{
		"ARTICOLO": "0002*A",
		"CodArtClienteFornitore": "0002*A"
	},
	{
		"ARTICOLO": "0002*CAG",
		"CodArtClienteFornitore": "0002*CAG"
	},
	{
		"ARTICOLO": "0002*D",
		"CodArtClienteFornitore": "0002*D"
	},
	{
		"ARTICOLO": "0002*S2",
		"CodArtClienteFornitore": "0002*S2"
	},
	{
		"ARTICOLO": "0002*S3",
		"CodArtClienteFornitore": "0002*S3"
	},
	{
		"ARTICOLO": "0002*S4",
		"CodArtClienteFornitore": "0002*S4"
	},
	{
		"ARTICOLO": "0002*S5",
		"CodArtClienteFornitore": "0002*S5"
	},
	{
		"ARTICOLO": "0002*S6",
		"CodArtClienteFornitore": "0002*S6"
	},
	{
		"ARTICOLO": "0002*T",
		"CodArtClienteFornitore": "0002*T"
	},
	{
		"ARTICOLO": "14634",
		"CodArtClienteFornitore": "14634"
	},
	{
		"ARTICOLO": "14638",
		"CodArtClienteFornitore": "14638"
	},
	{
		"ARTICOLO": "ACQDI5LT",
		"CodArtClienteFornitore": "ACQDI5LT"
	},
	{
		"ARTICOLO": "CARTA35KG",
		"CodArtClienteFornitore": "CARTA35KG"
	},
	{
		"ARTICOLO": "EST23MTR2.4",
		"CodArtClienteFornitore": "EST23MTR2.4"
	},
	{
		"ARTICOLO": "FEAUTOBIANC",
		"CodArtClienteFornitore": "FEAUTOBIANC"
	},
	{
		"ARTICOLO": "ILMA0002*N",
		"CodArtClienteFornitore": "IL"
	},
	{
		"ARTICOLO": "ILMA0002*N1",
		"CodArtClienteFornitore": "ILMA0002*N1"
	},
	{
		"ARTICOLO": "ILMA0002*T",
		"CodArtClienteFornitore": "ILMA0002*T"
	},
	{
		"ARTICOLO": "MANU200B",
		"CodArtClienteFornitore": "MANU200B"
	},
	{
		"ARTICOLO": "MINI10",
		"CodArtClienteFornitore": "MINI10"
	},
	{
		"ARTICOLO": "MINI10150",
		"CodArtClienteFornitore": "MINI10150"
	},
	{
		"ARTICOLO": "N50PPLFRAG",
		"CodArtClienteFornitore": "N50PPLFRAG"
	},
	{
		"ARTICOLO": "NAS50132PPLT",
		"CodArtClienteFornitore": "NAS50132PPLT"
	},
	{
		"ARTICOLO": "PAT110G",
		"CodArtClienteFornitore": "PAT110G"
	},
	{
		"ARTICOLO": "PATT110T",
		"CodArtClienteFornitore": "PATT110T"
	},
	{
		"ARTICOLO": "PL125-2THD",
		"CodArtClienteFornitore": "PL125-2THD"
	},
	{
		"ARTICOLO": "PR/U/15-25",
		"CodArtClienteFornitore": "PR/U/15-25"
	},
	{
		"ARTICOLO": "PR/U/20-35-80",
		"CodArtClienteFornitore": "PR/U/20-35-80"
	},
	{
		"ARTICOLO": "1594004",
		"CodArtClienteFornitore": "1594004"
	},
	{
		"ARTICOLO": "1594007",
		"CodArtClienteFornitore": "1594007"
	},
	{
		"ARTICOLO": "1594013",
		"CodArtClienteFornitore": "1594013"
	},
	{
		"ARTICOLO": "1594026",
		"CodArtClienteFornitore": "1594026"
	},
	{
		"ARTICOLO": "1594030",
		"CodArtClienteFornitore": "1594030"
	},
	{
		"ARTICOLO": "1594036",
		"CodArtClienteFornitore": "1594036"
	},
	{
		"ARTICOLO": "1594037",
		"CodArtClienteFornitore": "1594037"
	},
	{
		"ARTICOLO": "909.118-LAR75-17",
		"CodArtClienteFornitore": "909.118-LAR75-17"
	},
	{
		"ARTICOLO": "909.FASC.14°",
		"CodArtClienteFornitore": "909.FASC.14°"
	},
	{
		"ARTICOLO": "909.FASC.26°",
		"CodArtClienteFornitore": "909.LAR102.36°"
	},
	{
		"ARTICOLO": "909.FASC.40°",
		"CodArtClienteFornitore": "909.FASC.40°"
	},
	{
		"ARTICOLO": "909.LAR102.36°",
		"CodArtClienteFornitore": "909.LAR102.36°"
	},
	{
		"ARTICOLO": "909.LAR102.ORO",
		"CodArtClienteFornitore": "909.LAR102.ORO"
	},
	{
		"ARTICOLO": "909.LAR117.24°",
		"CodArtClienteFornitore": "909.LAR117.24°"
	},
	{
		"ARTICOLO": "909.LAR126.60°",
		"CodArtClienteFornitore": "909.LAR126.60°"
	},
	{
		"ARTICOLO": "909.LAR127.60°",
		"CodArtClienteFornitore": "909.LAR127.60°"
	},
	{
		"ARTICOLO": "909.LAR128",
		"CodArtClienteFornitore": "909.LAR128"
	},
	{
		"ARTICOLO": "909.LAR129",
		"CodArtClienteFornitore": "909.LAR129"
	},
	{
		"ARTICOLO": "909.LAR130QUADRO",
		"CodArtClienteFornitore": "909.LAR130QUADRO"
	},
	{
		"ARTICOLO": "DL-F111RIFL17",
		"CodArtClienteFornitore": "909.FASC.14°D=80H=50"
	},
	{
		"ARTICOLO": "DL-F111RIFL26",
		"CodArtClienteFornitore": "909.FASC.26°D=80H=50"
	},
	{
		"ARTICOLO": "DL-F111RIFL40",
		"CodArtClienteFornitore": "909.FASC.40°D=80H=50"
	},
	{
		"ARTICOLO": "VLAR128",
		"CodArtClienteFornitore": "909.LAR128.H12"
	},
	{
		"ARTICOLO": "VLAR129",
		"CodArtClienteFornitore": "909.LAR129.H12"
	},
	{
		"ARTICOLO": "ILMAS1000",
		"CodArtClienteFornitore": "ILMAS1000"
	},
	{
		"ARTICOLO": "ILMAS121",
		"CodArtClienteFornitore": "ILMAS121"
	},
	{
		"ARTICOLO": "ILMAS132",
		"CodArtClienteFornitore": "ILMAS132"
	},
	{
		"ARTICOLO": "ILMAS133",
		"CodArtClienteFornitore": "ILMAS133"
	},
	{
		"ARTICOLO": "ILMAS134",
		"CodArtClienteFornitore": "SNODO ADAT SCOMP.01"
	},
	{
		"ARTICOLO": "ILMAS139",
		"CodArtClienteFornitore": "B.332.C.98"
	},
	{
		"ARTICOLO": "ILMAS140",
		"CodArtClienteFornitore": "ILMAS140"
	},
	{
		"ARTICOLO": "ILMAS141",
		"CodArtClienteFornitore": "ILMAS141"
	},
	{
		"ARTICOLO": "A53E310N",
		"CodArtClienteFornitore": "A53E310N"
	},
	{
		"ARTICOLO": "APF150-48S",
		"CodArtClienteFornitore": "APF150-48S"
	},
	{
		"ARTICOLO": "D-SHE-B-60-SE",
		"CodArtClienteFornitore": "D-SHE-B-60-SE"
	},
	{
		"ARTICOLO": "D-SHEB250-SE",
		"CodArtClienteFornitore": "D-SHEB250-SE"
	},
	{
		"ARTICOLO": "HZK218B",
		"CodArtClienteFornitore": "HZK218B"
	},
	{
		"ARTICOLO": "LL01CRAQH38L02",
		"CodArtClienteFornitore": "LL01CRAQH38L02"
	},
	{
		"ARTICOLO": "LL01CRDF25LM1T",
		"CodArtClienteFornitore": "LL01CRDF25LM1T"
	},
	{
		"ARTICOLO": "LL01CTAZW12L02",
		"CodArtClienteFornitore": "LL01CTAZW12L02"
	},
	{
		"ARTICOLO": "LL01CTAZW24L02",
		"CodArtClienteFornitore": "LL01CTAZW24L02"
	},
	{
		"ARTICOLO": "LL01CTAZW38L02",
		"CodArtClienteFornitore": "LL01CTAZW38L02"
	},
	{
		"ARTICOLO": "LL01EDABY24L02",
		"CodArtClienteFornitore": "LL01EDABY24L02"
	},
	{
		"ARTICOLO": "LL01EDASA12L6",
		"CodArtClienteFornitore": "LL01EDASA12L6"
	},
	{
		"ARTICOLO": "LL01EDASA12L6P",
		"CodArtClienteFornitore": "LL01EDASA12L6P"
	},
	{
		"ARTICOLO": "LL01EDASA24L6",
		"CodArtClienteFornitore": "LL01EDASA24L6"
	},
	{
		"ARTICOLO": "LL01EDASA24L6P",
		"CodArtClienteFornitore": "LL01EDASA24L6P"
	},
	{
		"ARTICOLO": "LL01EDASA38L6",
		"CodArtClienteFornitore": "LL01EDASA38L6"
	},
	{
		"ARTICOLO": "LL01EDASA38L6P",
		"CodArtClienteFornitore": "LL01EDASA38L6P"
	},
	{
		"ARTICOLO": "LL07CRCCN20R2P",
		"CodArtClienteFornitore": "LL07CRCCN20R2P"
	},
	{
		"ARTICOLO": "LL07CRCCN35R2P",
		"CodArtClienteFornitore": "LL07CRCCN35R2P"
	},
	{
		"ARTICOLO": "LL07CRTV20H1",
		"CodArtClienteFornitore": "LL07CRTV20H1"
	},
	{
		"ARTICOLO": "LL07CRTV20H2",
		"CodArtClienteFornitore": "LL07CRTV20H2"
	},
	{
		"ARTICOLO": "LL25BK",
		"CodArtClienteFornitore": "LL01CRDF25LM1T"
	},
	{
		"ARTICOLO": "LL25WH",
		"CodArtClienteFornitore": "LL01CRDF25LM2T"
	},
	{
		"ARTICOLO": "LL40WH",
		"CodArtClienteFornitore": "LL01CRDF40LM2T"
	},
	{
		"ARTICOLO": "LL60BK",
		"CodArtClienteFornitore": "LL01CRDF60LM1T"
	},
	{
		"ARTICOLO": "LL60WH",
		"CodArtClienteFornitore": "LL01CRDF60LM2T"
	},
	{
		"ARTICOLO": "B.119.C.85.20",
		"CodArtClienteFornitore": "TB51699"
	},
	{
		"ARTICOLO": "B.342.C.01",
		"CodArtClienteFornitore": "TB51949"
	},
	{
		"ARTICOLO": "CUT.C.60.1000",
		"CodArtClienteFornitore": "TB51700"
	},
	{
		"ARTICOLO": "TB47029",
		"CodArtClienteFornitore": "CUT.C.02"
	},
	{
		"ARTICOLO": "TB47030",
		"CodArtClienteFornitore": "CUT.C.03"
	},
	{
		"ARTICOLO": "TB47036",
		"CodArtClienteFornitore": "CUT.C.09"
	},
	{
		"ARTICOLO": "TB47038",
		"CodArtClienteFornitore": "CUT.C.12"
	},
	{
		"ARTICOLO": "TB47353",
		"CodArtClienteFornitore": "CUT.C.20"
	},
	{
		"ARTICOLO": "TB47356",
		"CodArtClienteFornitore": "CUT.C.24"
	},
	{
		"ARTICOLO": "TB47408",
		"CodArtClienteFornitore": "CUT.C.25"
	},
	{
		"ARTICOLO": "TB48586",
		"CodArtClienteFornitore": "CUT.C.33"
	},
	{
		"ARTICOLO": "TB48588",
		"CodArtClienteFornitore": "CUT.C.41"
	},
	{
		"ARTICOLO": "TB48589",
		"CodArtClienteFornitore": "CUT.C.42"
	},
	{
		"ARTICOLO": "TB48644",
		"CodArtClienteFornitore": "CUT.C.38"
	},
	{
		"ARTICOLO": "TB48653",
		"CodArtClienteFornitore": "CUT.C.47"
	},
	{
		"ARTICOLO": "TB48665",
		"CodArtClienteFornitore": "CUT.C.46"
	},
	{
		"ARTICOLO": "TB48695",
		"CodArtClienteFornitore": "CUT.C.31"
	},
	{
		"ARTICOLO": "TB48720",
		"CodArtClienteFornitore": "CUT.C.43"
	},
	{
		"ARTICOLO": "4898CORPO",
		"CodArtClienteFornitore": "B154.C.30"
	},
	{
		"ARTICOLO": "4998CORPO0",
		"CodArtClienteFornitore": "B.152.C.30"
	},
	{
		"ARTICOLO": "4999CORPO",
		"CodArtClienteFornitore": "B.152.C.40"
	},
	{
		"ARTICOLO": "7102",
		"CodArtClienteFornitore": "7102"
	},
	{
		"ARTICOLO": "7202",
		"CodArtClienteFornitore": "7202"
	},
	{
		"ARTICOLO": "ROSONEK",
		"CodArtClienteFornitore": "ROSONEK"
	},
	{
		"ARTICOLO": "CUT.C.21.2000",
		"CodArtClienteFornitore": "CUT.C.21.2000"
	},
	{
		"ARTICOLO": "010181123X6",
		"CodArtClienteFornitore": "010181123X6"
	},
	{
		"ARTICOLO": "13PE47",
		"CodArtClienteFornitore": "13PE47"
	},
	{
		"ARTICOLO": "30PE73",
		"CodArtClienteFornitore": "30PE73"
	},
	{
		"ARTICOLO": "30PE73LPCSOPM",
		"CodArtClienteFornitore": "30PE73LPCSOPM"
	},
	{
		"ARTICOLO": "06024BOX.00",
		"CodArtClienteFornitore": "06024BOX.00"
	},
	{
		"ARTICOLO": "34-60x60SOFF",
		"CodArtClienteFornitore": "34-60x60SOFF"
	},
	{
		"ARTICOLO": "34-60X60SOFF1",
		"CodArtClienteFornitore": "34-60X60SOFF"
	},
	{
		"ARTICOLO": "34-60X60TOSH",
		"CodArtClienteFornitore": "34-60X60TOSHIBA"
	},
	{
		"ARTICOLO": "34-9625STAFFA",
		"CodArtClienteFornitore": "34-9625STAFFA"
	},
	{
		"ARTICOLO": "34-DL3110STAFFA",
		"CodArtClienteFornitore": "34-DL3110STAFFA"
	},
	{
		"ARTICOLO": "34-STAFFA-IGROX",
		"CodArtClienteFornitore": "34-STAFFA-IGROX"
	},
	{
		"ARTICOLO": "34-STAFFAT-IGROX",
		"CodArtClienteFornitore": "34-STAFFAT-IGROX"
	},
	{
		"ARTICOLO": "34-TESTATA1-IGROX",
		"CodArtClienteFornitore": "34-TESTATA1-IGROX"
	},
	{
		"ARTICOLO": "B.328.C.04",
		"CodArtClienteFornitore": "B.328.C.04"
	},
	{
		"ARTICOLO": "JAKIT02L100",
		"CodArtClienteFornitore": "JAKIT02L100"
	},
	{
		"ARTICOLO": "11.610",
		"CodArtClienteFornitore": "11.610"
	},
	{
		"ARTICOLO": "0208",
		"CodArtClienteFornitore": "0208"
	},
	{
		"ARTICOLO": "0324",
		"CodArtClienteFornitore": "0324"
	},
	{
		"ARTICOLO": "0325",
		"CodArtClienteFornitore": "0325"
	},
	{
		"ARTICOLO": "0326",
		"CodArtClienteFornitore": "0326"
	},
	{
		"ARTICOLO": "0345",
		"CodArtClienteFornitore": "0345"
	},
	{
		"ARTICOLO": "0395",
		"CodArtClienteFornitore": "0395"
	},
	{
		"ARTICOLO": "0469",
		"CodArtClienteFornitore": "0469"
	},
	{
		"ARTICOLO": "0503",
		"CodArtClienteFornitore": "0503"
	},
	{
		"ARTICOLO": "0654",
		"CodArtClienteFornitore": "0654"
	},
	{
		"ARTICOLO": "GDCS8PM1.14-UNUO-W4-",
		"CodArtClienteFornitore": "GDCS8PM1.14-UNUO-W4-"
	},
	{
		"ARTICOLO": "GHCS8PM1.24-4T2U-1",
		"CodArtClienteFornitore": "GHCS8PM1.24-4T2U-1"
	},
	{
		"ARTICOLO": "GWCS8PM1.PM-LRLT-A23",
		"CodArtClienteFornitore": "GWCS8PM1.PM-LRLT-A23"
	},
	{
		"ARTICOLO": "GWCS8PM1.PM-LRLT-A33",
		"CodArtClienteFornitore": "GWCS8PM1.PM-LRLT-A33"
	},
	{
		"ARTICOLO": "GWCSSRM1",
		"CodArtClienteFornitore": "GWCSSRM1"
	},
	{
		"ARTICOLO": "LTCP7P-KYKZ-26",
		"CodArtClienteFornitore": "LTCP7P-KYKZ-26"
	},
	{
		"ARTICOLO": "TAPPO101",
		"CodArtClienteFornitore": "TAPPO101"
	},
	{
		"ARTICOLO": "TUBO100",
		"CodArtClienteFornitore": "TUBO100"
	},
	{
		"ARTICOLO": "4652LL5X2H",
		"CodArtClienteFornitore": "4652LL5X2H"
	},
	{
		"ARTICOLO": "RPT01",
		"CodArtClienteFornitore": "RPT01"
	},
	{
		"ARTICOLO": "5036 8 1 26",
		"CodArtClienteFornitore": "5036/8/1/26"
	},
	{
		"ARTICOLO": "9000/BP-D-W",
		"CodArtClienteFornitore": "9000/BP-D-W"
	},
	{
		"ARTICOLO": "9519-166/B",
		"CodArtClienteFornitore": "9519-166/B"
	},
	{
		"ARTICOLO": "9519-166/W",
		"CodArtClienteFornitore": "9519-166/W"
	},
	{
		"ARTICOLO": "9605A011",
		"CodArtClienteFornitore": "9000-1/W-ST"
	},
	{
		"ARTICOLO": "9605A012",
		"CodArtClienteFornitore": "9000-1/B-ST"
	},
	{
		"ARTICOLO": "9605A017",
		"CodArtClienteFornitore": "9000-1/G-ST"
	},
	{
		"ARTICOLO": "9605A017-RAL7037",
		"CodArtClienteFornitore": "9000-1/G1-ST"
	},
	{
		"ARTICOLO": "9605A021",
		"CodArtClienteFornitore": "9000-2/W-ST"
	},
	{
		"ARTICOLO": "9605A022",
		"CodArtClienteFornitore": "9000-2/B-ST"
	},
	{
		"ARTICOLO": "9605A027",
		"CodArtClienteFornitore": "9000-2/G-ST"
	},
	{
		"ARTICOLO": "9605A027-RAL7037",
		"CodArtClienteFornitore": "9000-2/G1-ST"
	},
	{
		"ARTICOLO": "9605A031",
		"CodArtClienteFornitore": "9000-3/W-ST"
	},
	{
		"ARTICOLO": "9605A032",
		"CodArtClienteFornitore": "9000-3/B-ST"
	},
	{
		"ARTICOLO": "9605A037",
		"CodArtClienteFornitore": "9000-3/G-ST"
	},
	{
		"ARTICOLO": "9605A037-RAL7037",
		"CodArtClienteFornitore": "9000-3/G1-ST"
	},
	{
		"ARTICOLO": "9605A110",
		"CodArtClienteFornitore": "9009-4/G"
	},
	{
		"ARTICOLO": "9605A111",
		"CodArtClienteFornitore": "9009-4/W"
	},
	{
		"ARTICOLO": "9605A112",
		"CodArtClienteFornitore": "9009-4/B"
	},
	{
		"ARTICOLO": "9605A120",
		"CodArtClienteFornitore": "9002/G"
	},
	{
		"ARTICOLO": "9605A121",
		"CodArtClienteFornitore": "9002/W"
	},
	{
		"ARTICOLO": "9605A122",
		"CodArtClienteFornitore": "9002/B"
	},
	{
		"ARTICOLO": "9605A130",
		"CodArtClienteFornitore": "9004/G"
	},
	{
		"ARTICOLO": "9605A131",
		"CodArtClienteFornitore": "9004/W"
	},
	{
		"ARTICOLO": "9605A132",
		"CodArtClienteFornitore": "9004/B"
	},
	{
		"ARTICOLO": "9605A140",
		"CodArtClienteFornitore": "9003/G"
	},
	{
		"ARTICOLO": "9605A141",
		"CodArtClienteFornitore": "9003/W"
	},
	{
		"ARTICOLO": "9605A142",
		"CodArtClienteFornitore": "9003/B"
	},
	{
		"ARTICOLO": "9605A150",
		"CodArtClienteFornitore": "9001/G"
	},
	{
		"ARTICOLO": "9605A151",
		"CodArtClienteFornitore": "9001/W"
	},
	{
		"ARTICOLO": "9605A152",
		"CodArtClienteFornitore": "9001/B"
	},
	{
		"ARTICOLO": "9605A161",
		"CodArtClienteFornitore": "9018/W"
	},
	{
		"ARTICOLO": "9605A162",
		"CodArtClienteFornitore": "9018/B"
	},
	{
		"ARTICOLO": "9605A170",
		"CodArtClienteFornitore": "S-9000/M-G"
	},
	{
		"ARTICOLO": "9605A171",
		"CodArtClienteFornitore": "S-9000/M-W"
	},
	{
		"ARTICOLO": "9605A172",
		"CodArtClienteFornitore": "S-9000/M-B"
	},
	{
		"ARTICOLO": "9605A217",
		"CodArtClienteFornitore": "S-9000/111"
	},
	{
		"ARTICOLO": "9605A230",
		"CodArtClienteFornitore": "9011/G"
	},
	{
		"ARTICOLO": "9605A231",
		"CodArtClienteFornitore": "9011/W"
	},
	{
		"ARTICOLO": "9605A232",
		"CodArtClienteFornitore": "9011/B"
	},
	{
		"ARTICOLO": "9605A240",
		"CodArtClienteFornitore": "9013/G"
	},
	{
		"ARTICOLO": "9605A241",
		"CodArtClienteFornitore": "9013/W"
	},
	{
		"ARTICOLO": "9605A242",
		"CodArtClienteFornitore": "9013/B"
	},
	{
		"ARTICOLO": "9605A251",
		"CodArtClienteFornitore": "9017/W"
	},
	{
		"ARTICOLO": "9605A252",
		"CodArtClienteFornitore": "9017/B"
	},
	{
		"ARTICOLO": "9605A260",
		"CodArtClienteFornitore": "9010/G"
	},
	{
		"ARTICOLO": "9605A261",
		"CodArtClienteFornitore": "9010/W"
	},
	{
		"ARTICOLO": "9605A262",
		"CodArtClienteFornitore": "9010/2"
	},
	{
		"ARTICOLO": "9605A270",
		"CodArtClienteFornitore": "9012/G"
	},
	{
		"ARTICOLO": "9605A271",
		"CodArtClienteFornitore": "9012/W"
	},
	{
		"ARTICOLO": "9605A272",
		"CodArtClienteFornitore": "9012/B"
	},
	{
		"ARTICOLO": "9605A280",
		"CodArtClienteFornitore": "S-9000/GP-G"
	},
	{
		"ARTICOLO": "9605A281",
		"CodArtClienteFornitore": "S-9000/GP-W"
	},
	{
		"ARTICOLO": "9605A282",
		"CodArtClienteFornitore": "S-9000/GP-B"
	},
	{
		"ARTICOLO": "9605A310",
		"CodArtClienteFornitore": "9014/G"
	},
	{
		"ARTICOLO": "9605A311",
		"CodArtClienteFornitore": "9014/W"
	},
	{
		"ARTICOLO": "9605A312",
		"CodArtClienteFornitore": "9014/B"
	},
	{
		"ARTICOLO": "9605A320",
		"CodArtClienteFornitore": "9015/G"
	},
	{
		"ARTICOLO": "9605A321",
		"CodArtClienteFornitore": "9015/W"
	},
	{
		"ARTICOLO": "9605A322",
		"CodArtClienteFornitore": "9015/B"
	},
	{
		"ARTICOLO": "9605A330",
		"CodArtClienteFornitore": "9016/G"
	},
	{
		"ARTICOLO": "9605A331",
		"CodArtClienteFornitore": "9016/W"
	},
	{
		"ARTICOLO": "9605A332",
		"CodArtClienteFornitore": "9016/B"
	},
	{
		"ARTICOLO": "9605A720",
		"CodArtClienteFornitore": "9009/G"
	},
	{
		"ARTICOLO": "9605A721",
		"CodArtClienteFornitore": "9009/W"
	},
	{
		"ARTICOLO": "9605A722",
		"CodArtClienteFornitore": "9009/B"
	},
	{
		"ARTICOLO": "9605A810",
		"CodArtClienteFornitore": "9209-4/G"
	},
	{
		"ARTICOLO": "9605A811",
		"CodArtClienteFornitore": "9209-4/W"
	},
	{
		"ARTICOLO": "9605A812",
		"CodArtClienteFornitore": "9209-4/B"
	},
	{
		"ARTICOLO": "9605A820",
		"CodArtClienteFornitore": "9209/G"
	},
	{
		"ARTICOLO": "9605A821",
		"CodArtClienteFornitore": "9209/W"
	},
	{
		"ARTICOLO": "9605A822",
		"CodArtClienteFornitore": "9209/B"
	},
	{
		"ARTICOLO": "9605A997",
		"CodArtClienteFornitore": "S-9000/T"
	},
	{
		"ARTICOLO": "9615A012",
		"CodArtClienteFornitore": "9000-1/B-R"
	},
	{
		"ARTICOLO": "9615A021",
		"CodArtClienteFornitore": "9000-2/W-R"
	},
	{
		"ARTICOLO": "9615A022",
		"CodArtClienteFornitore": "9000-2/B-R"
	},
	{
		"ARTICOLO": "9615A031",
		"CodArtClienteFornitore": "9000-3/W-R"
	},
	{
		"ARTICOLO": "9615A032",
		"CodArtClienteFornitore": "9000-3/B-R"
	},
	{
		"ARTICOLO": "9615A037",
		"CodArtClienteFornitore": "9000-3/G-R"
	},
	{
		"ARTICOLO": "9615A120",
		"CodArtClienteFornitore": "9002-R/G"
	},
	{
		"ARTICOLO": "9615A121",
		"CodArtClienteFornitore": "9002-R/W"
	},
	{
		"ARTICOLO": "9615A122",
		"CodArtClienteFornitore": "9002-R/B"
	},
	{
		"ARTICOLO": "9615A150",
		"CodArtClienteFornitore": "9001-R/G"
	},
	{
		"ARTICOLO": "9615A151",
		"CodArtClienteFornitore": "9001-R/W"
	},
	{
		"ARTICOLO": "9615A152",
		"CodArtClienteFornitore": "9001-R/B"
	},
	{
		"ARTICOLO": "9615A231",
		"CodArtClienteFornitore": "9011-R/W"
	},
	{
		"ARTICOLO": "9615A240",
		"CodArtClienteFornitore": "9015/G"
	},
	{
		"ARTICOLO": "9615A241",
		"CodArtClienteFornitore": "9015/W"
	},
	{
		"ARTICOLO": "9615A242",
		"CodArtClienteFornitore": "9015/B"
	},
	{
		"ARTICOLO": "9615A260",
		"CodArtClienteFornitore": "9010-R/G"
	},
	{
		"ARTICOLO": "9615A261",
		"CodArtClienteFornitore": "9010-R/W"
	},
	{
		"ARTICOLO": "9615A262",
		"CodArtClienteFornitore": "9010-R/B"
	},
	{
		"ARTICOLO": "9615A271",
		"CodArtClienteFornitore": "9012-R/W"
	},
	{
		"ARTICOLO": "9625A011",
		"CodArtClienteFornitore": "9000-1/W-H"
	},
	{
		"ARTICOLO": "9625A012",
		"CodArtClienteFornitore": "9000-1/B-H"
	},
	{
		"ARTICOLO": "9625A017",
		"CodArtClienteFornitore": "9000-1/G-H"
	},
	{
		"ARTICOLO": "9625A021",
		"CodArtClienteFornitore": "9000-2/W-H"
	},
	{
		"ARTICOLO": "9625A022",
		"CodArtClienteFornitore": "9000-2/B-H"
	},
	{
		"ARTICOLO": "9625A027",
		"CodArtClienteFornitore": "9000-2/G-H"
	},
	{
		"ARTICOLO": "9625A031",
		"CodArtClienteFornitore": "9000-3/W-H"
	},
	{
		"ARTICOLO": "9625A032",
		"CodArtClienteFornitore": "9000-3/B-H"
	},
	{
		"ARTICOLO": "9625A037",
		"CodArtClienteFornitore": "9000-3/G-H"
	},
	{
		"ARTICOLO": "9625A121",
		"CodArtClienteFornitore": "9002/W-H"
	},
	{
		"ARTICOLO": "9625A122",
		"CodArtClienteFornitore": "9002/B-H"
	},
	{
		"ARTICOLO": "9625A131",
		"CodArtClienteFornitore": "9004/W-H"
	},
	{
		"ARTICOLO": "9625A132",
		"CodArtClienteFornitore": "9004/B-H"
	},
	{
		"ARTICOLO": "9625A141",
		"CodArtClienteFornitore": "9010/W-H"
	},
	{
		"ARTICOLO": "9625A142",
		"CodArtClienteFornitore": "9010/B-H"
	},
	{
		"ARTICOLO": "9625A150",
		"CodArtClienteFornitore": "90001/G-H"
	},
	{
		"ARTICOLO": "9625A151",
		"CodArtClienteFornitore": "9001/W-H"
	},
	{
		"ARTICOLO": "9625A152",
		"CodArtClienteFornitore": "9001/B-H"
	},
	{
		"ARTICOLO": "9625A220",
		"CodArtClienteFornitore": "9000/BP-D-G"
	},
	{
		"ARTICOLO": "9625A221",
		"CodArtClienteFornitore": "9000/BP-D-W"
	},
	{
		"ARTICOLO": "9625A222",
		"CodArtClienteFornitore": "9000/BP-D-B"
	},
	{
		"ARTICOLO": "9625A231",
		"CodArtClienteFornitore": "9011/W-H"
	},
	{
		"ARTICOLO": "9625A232",
		"CodArtClienteFornitore": "9011/B-H"
	},
	{
		"ARTICOLO": "9625A241",
		"CodArtClienteFornitore": "9013/W-H"
	},
	{
		"ARTICOLO": "9625A242",
		"CodArtClienteFornitore": "9013/B-H"
	},
	{
		"ARTICOLO": "9625A251",
		"CodArtClienteFornitore": "9017/W-H"
	},
	{
		"ARTICOLO": "9625A252",
		"CodArtClienteFornitore": "9017/B-H"
	},
	{
		"ARTICOLO": "9625A262",
		"CodArtClienteFornitore": "9010/B-H"
	},
	{
		"ARTICOLO": "9625A271",
		"CodArtClienteFornitore": "9012/W-H"
	},
	{
		"ARTICOLO": "9625A272",
		"CodArtClienteFornitore": "9012/B-H"
	},
	{
		"ARTICOLO": "9625A311",
		"CodArtClienteFornitore": "9014/W-H"
	},
	{
		"ARTICOLO": "9625A312",
		"CodArtClienteFornitore": "9014/B-H"
	},
	{
		"ARTICOLO": "9625A321",
		"CodArtClienteFornitore": "9015/W-H"
	},
	{
		"ARTICOLO": "9625A331",
		"CodArtClienteFornitore": "9016/W-H"
	},
	{
		"ARTICOLO": "9625A332",
		"CodArtClienteFornitore": "9016/B-H"
	},
	{
		"ARTICOLO": "9625A557",
		"CodArtClienteFornitore": "S-9000/122-SR-H"
	},
	{
		"ARTICOLO": "9625A737",
		"CodArtClienteFornitore": "S-9000/112-LA-H"
	},
	{
		"ARTICOLO": "9625A807",
		"CodArtClienteFornitore": "S-9000/112-LA-H"
	},
	{
		"ARTICOLO": "9625A817",
		"CodArtClienteFornitore": "S-9000/112-SV-H"
	},
	{
		"ARTICOLO": "9625A840",
		"CodArtClienteFornitore": "S-9000/5-G-H"
	},
	{
		"ARTICOLO": "9625A841",
		"CodArtClienteFornitore": "S-9000/5-W-H"
	},
	{
		"ARTICOLO": "9630A012",
		"CodArtClienteFornitore": "9500-1/B-ST1"
	},
	{
		"ARTICOLO": "9630A021",
		"CodArtClienteFornitore": "9500-2/W-ST1"
	},
	{
		"ARTICOLO": "9630A022",
		"CodArtClienteFornitore": "9500-2/B-ST1"
	},
	{
		"ARTICOLO": "9630A031",
		"CodArtClienteFornitore": "9500-3/W-ST1"
	},
	{
		"ARTICOLO": "9630A032",
		"CodArtClienteFornitore": "9500-3/B-ST1"
	},
	{
		"ARTICOLO": "9630A121",
		"CodArtClienteFornitore": "9501/W"
	},
	{
		"ARTICOLO": "9630A122",
		"CodArtClienteFornitore": "9501/B"
	},
	{
		"ARTICOLO": "9630A131",
		"CodArtClienteFornitore": "9504/ST1-W"
	},
	{
		"ARTICOLO": "9630A132",
		"CodArtClienteFornitore": "9504/ST1-B"
	},
	{
		"ARTICOLO": "9640A012",
		"CodArtClienteFornitore": "9500-1/B-ST3"
	},
	{
		"ARTICOLO": "9640A021",
		"CodArtClienteFornitore": "9500-2/W-ST3"
	},
	{
		"ARTICOLO": "9640A022",
		"CodArtClienteFornitore": "9500-2/B-ST3"
	},
	{
		"ARTICOLO": "9645A012",
		"CodArtClienteFornitore": "9500-1/B-ST4"
	},
	{
		"ARTICOLO": "9909-4-DRI/B",
		"CodArtClienteFornitore": "9909-4-DRI/B"
	},
	{
		"ARTICOLO": "9909-4-DRI/G",
		"CodArtClienteFornitore": "9909-4-DRI/G"
	},
	{
		"ARTICOLO": "9909-4-DRI/W",
		"CodArtClienteFornitore": "9909-4-DRI/W"
	},
	{
		"ARTICOLO": "9909-DRI/B",
		"CodArtClienteFornitore": "9909-DRI/B"
	},
	{
		"ARTICOLO": "9909-DRI/W",
		"CodArtClienteFornitore": "9909-DRI/W"
	},
	{
		"ARTICOLO": "ARC9605A241",
		"CodArtClienteFornitore": "9013/W"
	},
	{
		"ARTICOLO": "ARC9605A242",
		"CodArtClienteFornitore": "9013/B"
	},
	{
		"ARTICOLO": "ARC9605A311",
		"CodArtClienteFornitore": "9014/W"
	},
	{
		"ARTICOLO": "ARC9605A312",
		"CodArtClienteFornitore": "9014/B"
	},
	{
		"ARTICOLO": "ARC9605A321",
		"CodArtClienteFornitore": "9015/W"
	},
	{
		"ARTICOLO": "ARC9605A331",
		"CodArtClienteFornitore": "9016/W"
	},
	{
		"ARTICOLO": "ARC9605A332",
		"CodArtClienteFornitore": "9016/B"
	},
	{
		"ARTICOLO": "EPC11175S",
		"CodArtClienteFornitore": "EPC11175S"
	},
	{
		"ARTICOLO": "PT0561414-10-12773",
		"CodArtClienteFornitore": "PT056114-10-12773"
	},
	{
		"ARTICOLO": "S-9000/112-LA",
		"CodArtClienteFornitore": "S-9000/112-LA"
	},
	{
		"ARTICOLO": "S-9000/112-LA-H",
		"CodArtClienteFornitore": "S-9000/112-LA-H"
	},
	{
		"ARTICOLO": "S-9000/112-SV",
		"CodArtClienteFornitore": "S-9000/112-SV"
	},
	{
		"ARTICOLO": "S-9009/M10",
		"CodArtClienteFornitore": "S-9009/M10"
	},
	{
		"ARTICOLO": "S-9009/M13",
		"CodArtClienteFornitore": "S-9009/M13"
	},
	{
		"ARTICOLO": "S-9011-R/6-B",
		"CodArtClienteFornitore": "S-9011-R/6-B"
	},
	{
		"ARTICOLO": "S-9013-R/6-B ",
		"CodArtClienteFornitore": "S-9013-R/6-B "
	},
	{
		"ARTICOLO": "S-9519/M8",
		"CodArtClienteFornitore": "S-9519/M8"
	},
	{
		"ARTICOLO": "S-9909-DRI/11-B",
		"CodArtClienteFornitore": "S-9909-DRI/11-B"
	},
	{
		"ARTICOLO": "S-9909/M10",
		"CodArtClienteFornitore": "S-9909/M10"
	},
	{
		"ARTICOLO": "SPF052025-01-12774",
		"CodArtClienteFornitore": "PF052025-01-12774-"
	},
	{
		"ARTICOLO": "SPF052090-01-12774",
		"CodArtClienteFornitore": "PF052090-01-12774"
	},
	{
		"ARTICOLO": "SPF052090-11-12774",
		"CodArtClienteFornitore": "PF052090-11-12774"
	},
	{
		"ARTICOLO": "SPF080080-11-12789",
		"CodArtClienteFornitore": "PF080080-11-12789"
	},
	{
		"ARTICOLO": "SPF080085-01-12789",
		"CodArtClienteFornitore": "PF080085-01-12789"
	},
	{
		"ARTICOLO": "SPT056070-X0-12773",
		"CodArtClienteFornitore": "PT056070-X0-12773-"
	},
	{
		"ARTICOLO": "SPT056114-10-12773",
		"CodArtClienteFornitore": "PT056114-10-12773"
	},
	{
		"ARTICOLO": "SPT056114-11-12773",
		"CodArtClienteFornitore": "PT056114-11-12773"
	},
	{
		"ARTICOLO": "SPT056135-10-12773",
		"CodArtClienteFornitore": "PT056135-10-12773"
	},
	{
		"ARTICOLO": "SPT056135-11-12773",
		"CodArtClienteFornitore": "PT056135-11-12773"
	},
	{
		"ARTICOLO": "SPT056135-11-127773",
		"CodArtClienteFornitore": "PT056135-11-12773"
	},
	{
		"ARTICOLO": "spt056245-10-12773",
		"CodArtClienteFornitore": "PT056245-10-12773"
	},
	{
		"ARTICOLO": "SPT085120-10-12788",
		"CodArtClienteFornitore": "PT085120-10-12788"
	},
	{
		"ARTICOLO": "SPT085135-10-12788",
		"CodArtClienteFornitore": "PT085135-10-12788"
	},
	{
		"ARTICOLO": "2/001.0",
		"CodArtClienteFornitore": "1091.1"
	},
	{
		"ARTICOLO": "2/001.1",
		"CodArtClienteFornitore": "2/001.1"
	},
	{
		"ARTICOLO": "2/001.1RAL9010",
		"CodArtClienteFornitore": "2/001.1RAL9010"
	},
	{
		"ARTICOLO": "2/001.1RAL9016",
		"CodArtClienteFornitore": "2/001.1RAL9016"
	},
	{
		"ARTICOLO": "2/001.2",
		"CodArtClienteFornitore": "1091.1"
	},
	{
		"ARTICOLO": "2/002.0",
		"CodArtClienteFornitore": "1090.1"
	},
	{
		"ARTICOLO": "2/002.1",
		"CodArtClienteFornitore": "1090.1"
	},
	{
		"ARTICOLO": "2/002.1RAL9010",
		"CodArtClienteFornitore": "2/002.1RAL9010"
	},
	{
		"ARTICOLO": "2/002.1RAL9016",
		"CodArtClienteFornitore": "2/002.1RAL9016"
	},
	{
		"ARTICOLO": "2/002.2",
		"CodArtClienteFornitore": "1090.1"
	},
	{
		"ARTICOLO": "2/003.0",
		"CodArtClienteFornitore": "1191.1"
	},
	{
		"ARTICOLO": "2/003.1",
		"CodArtClienteFornitore": "1191.1"
	},
	{
		"ARTICOLO": "2/003.1RAL9010",
		"CodArtClienteFornitore": "2/003.1RAL9010"
	},
	{
		"ARTICOLO": "2/003.1RAL9016",
		"CodArtClienteFornitore": "2/003.1RAL9016"
	},
	{
		"ARTICOLO": "2/003.2",
		"CodArtClienteFornitore": "1191.1"
	},
	{
		"ARTICOLO": "2/004.0",
		"CodArtClienteFornitore": "1190.1"
	},
	{
		"ARTICOLO": "2/004.1",
		"CodArtClienteFornitore": "1190.1"
	},
	{
		"ARTICOLO": "2/004.1RAL9010",
		"CodArtClienteFornitore": "2/004.1RAL9010"
	},
	{
		"ARTICOLO": "2/004.1RAL9016",
		"CodArtClienteFornitore": "2/004.1RAL9016"
	},
	{
		"ARTICOLO": "2/004.2",
		"CodArtClienteFornitore": "1190.1"
	},
	{
		"ARTICOLO": "00401760553A",
		"CodArtClienteFornitore": "00401760553A"
	},
	{
		"ARTICOLO": "00601760433A",
		"CodArtClienteFornitore": "00601760433A"
	},
	{
		"ARTICOLO": "00801760146A",
		"CodArtClienteFornitore": "00801760146A"
	},
	{
		"ARTICOLO": "01001315201A",
		"CodArtClienteFornitore": "01001315201A"
	},
	{
		"ARTICOLO": "01001315203A",
		"CodArtClienteFornitore": "01001315203A"
	},
	{
		"ARTICOLO": "01001315204A",
		"CodArtClienteFornitore": "01001315204A"
	},
	{
		"ARTICOLO": "01001315205A",
		"CodArtClienteFornitore": "01001315205A"
	},
	{
		"ARTICOLO": "01001760481AR",
		"CodArtClienteFornitore": "01001760481A"
	},
	{
		"ARTICOLO": "01001760571AR",
		"CodArtClienteFornitore": "01001760571A"
	},
	{
		"ARTICOLO": "01401760443A",
		"CodArtClienteFornitore": "01401760443A"
	},
	{
		"ARTICOLO": "02001760978A",
		"CodArtClienteFornitore": "02001760978A"
	},
	{
		"ARTICOLO": "02101760981A",
		"CodArtClienteFornitore": "02101760981A"
	},
	{
		"ARTICOLO": "101ANELEST.00",
		"CodArtClienteFornitore": "IL.010"
	},
	{
		"ARTICOLO": "101ANELEST.11",
		"CodArtClienteFornitore": "IL.010G"
	},
	{
		"ARTICOLO": "101ANELINT.00",
		"CodArtClienteFornitore": "IL.011"
	},
	{
		"ARTICOLO": "101ANELINT.11",
		"CodArtClienteFornitore": "IL.011G"
	},
	{
		"ARTICOLO": "DL-F111BASE.00",
		"CodArtClienteFornitore": "IL.001"
	},
	{
		"ARTICOLO": "DL-F111BASE.10",
		"CodArtClienteFornitore": "IL.001B"
	},
	{
		"ARTICOLO": "DL-F111BASE.11",
		"CodArtClienteFornitore": "IL.001G"
	},
	{
		"ARTICOLO": "DL-F111BASE.20",
		"CodArtClienteFornitore": "IL.001N"
	},
	{
		"ARTICOLO": "DL-F111RING.00",
		"CodArtClienteFornitore": "IL.002"
	},
	{
		"ARTICOLO": "DL-F111RING.10",
		"CodArtClienteFornitore": "IL.002B"
	},
	{
		"ARTICOLO": "DL-F111RING.11",
		"CodArtClienteFornitore": "IL.002G"
	},
	{
		"ARTICOLO": "DL-F111RING.20",
		"CodArtClienteFornitore": "IL.002N"
	},
	{
		"ARTICOLO": "DL-F112BASE.00",
		"CodArtClienteFornitore": "IL.004"
	},
	{
		"ARTICOLO": "DL-F112BASE.10",
		"CodArtClienteFornitore": "IL.004B"
	},
	{
		"ARTICOLO": "DL-F112BASE.11",
		"CodArtClienteFornitore": "IL.004G"
	},
	{
		"ARTICOLO": "DL-F112BASE.20",
		"CodArtClienteFornitore": "IL.004N"
	},
	{
		"ARTICOLO": "DL-F112RING.00",
		"CodArtClienteFornitore": "IL.003"
	},
	{
		"ARTICOLO": "DL-F112RING.10",
		"CodArtClienteFornitore": "IL.003B"
	},
	{
		"ARTICOLO": "DL-F112RING.11",
		"CodArtClienteFornitore": "IL.003G"
	},
	{
		"ARTICOLO": "DL-F112RING.20",
		"CodArtClienteFornitore": "IL.003N"
	},
	{
		"ARTICOLO": "IL.001",
		"CodArtClienteFornitore": "IL.001"
	},
	{
		"ARTICOLO": "IL.002",
		"CodArtClienteFornitore": "IL.002"
	},
	{
		"ARTICOLO": "IL.003",
		"CodArtClienteFornitore": "IL.003"
	},
	{
		"ARTICOLO": "IL.004",
		"CodArtClienteFornitore": "IL.004"
	},
	{
		"ARTICOLO": "IL.005",
		"CodArtClienteFornitore": "IL.005"
	},
	{
		"ARTICOLO": "IL.006",
		"CodArtClienteFornitore": "IL.006"
	},
	{
		"ARTICOLO": "IL.007",
		"CodArtClienteFornitore": "IL.007"
	},
	{
		"ARTICOLO": "IL.008",
		"CodArtClienteFornitore": "IL.008"
	},
	{
		"ARTICOLO": "IL.010",
		"CodArtClienteFornitore": "IL.010"
	},
	{
		"ARTICOLO": "IL.011",
		"CodArtClienteFornitore": "IL.011"
	},
	{
		"ARTICOLO": "IL.015",
		"CodArtClienteFornitore": "IL.015"
	},
	{
		"ARTICOLO": "IL.016",
		"CodArtClienteFornitore": "IL.016"
	},
	{
		"ARTICOLO": "IL.017",
		"CodArtClienteFornitore": "IL.017"
	},
	{
		"ARTICOLO": "IL.018",
		"CodArtClienteFornitore": "IL.018"
	},
	{
		"ARTICOLO": "IL.019",
		"CodArtClienteFornitore": "IL.019"
	},
	{
		"ARTICOLO": "IL.020",
		"CodArtClienteFornitore": "IL.020"
	},
	{
		"ARTICOLO": "IL.025",
		"CodArtClienteFornitore": "IL.025"
	},
	{
		"ARTICOLO": "IL.026",
		"CodArtClienteFornitore": "IL.026"
	},
	{
		"ARTICOLO": "IL.029",
		"CodArtClienteFornitore": "IL.029"
	},
	{
		"ARTICOLO": "IL.030",
		"CodArtClienteFornitore": "IL.030"
	},
	{
		"ARTICOLO": "IL.031",
		"CodArtClienteFornitore": "IL.031"
	},
	{
		"ARTICOLO": "IL.032",
		"CodArtClienteFornitore": "IL.032"
	},
	{
		"ARTICOLO": "IL.033",
		"CodArtClienteFornitore": "IL.033"
	},
	{
		"ARTICOLO": "CUT.C.26.1.C2.B",
		"CodArtClienteFornitore": "CUT.C.26.1.C2.B"
	},
	{
		"ARTICOLO": "CUT.C.26.1.C2.W",
		"CodArtClienteFornitore": "CUT.C.26.1.C2.W"
	},
	{
		"ARTICOLO": "CUT.C.26.1.C3.B",
		"CodArtClienteFornitore": "CUT.C.26.1.C3.B"
	},
	{
		"ARTICOLO": "CUT.C.26.1.C3.W",
		"CodArtClienteFornitore": "CUT.C.26.1.C3.W"
	},
	{
		"ARTICOLO": "CUT.C.26.2",
		"CodArtClienteFornitore": "CUT.C.26.2"
	},
	{
		"ARTICOLO": "CUT.C.26.3",
		"CodArtClienteFornitore": "CUT.C.26.3"
	},
	{
		"ARTICOLO": "LDREU002A30NA0",
		"CodArtClienteFornitore": "LDREU002A30NA0"
	},
	{
		"ARTICOLO": "LDRU002A30NA0",
		"CodArtClienteFornitore": "LDRU002A30NA0"
	},
	{
		"ARTICOLO": "_x000D_"
	},
	{
		"ARTICOLO": "929001419106",
		"CodArtClienteFornitore": "929001419106"
	},
	{
		"ARTICOLO": "40086459011",
		"CodArtClienteFornitore": "9137006532"
	},
	{
		"ARTICOLO": "400CERT105",
		"CodArtClienteFornitore": "913700633991"
	},
	{
		"ARTICOLO": "400HIDCPV150I",
		"CodArtClienteFornitore": "913700624466"
	},
	{
		"ARTICOLO": "441HIDCPV150I",
		"CodArtClienteFornitore": "913700624466"
	},
	{
		"ARTICOLO": "50014740ES",
		"CodArtClienteFornitore": "924058817161"
	},
	{
		"ARTICOLO": "500458402P000",
		"CodArtClienteFornitore": "927905784040"
	},
	{
		"ARTICOLO": "500468302P000",
		"CodArtClienteFornitore": "927906183040"
	},
	{
		"ARTICOLO": "500468402P000",
		"CodArtClienteFornitore": "927906184040"
	},
	{
		"ARTICOLO": "500488402P000",
		"CodArtClienteFornitore": "927914384071"
	},
	{
		"ARTICOLO": "500498402P000",
		"CodArtClienteFornitore": "927914584071"
	},
	{
		"ARTICOLO": "5007983000000",
		"CodArtClienteFornitore": "928082205125"
	},
	{
		"ARTICOLO": "5007994200000",
		"CodArtClienteFornitore": "928084705133"
	},
	{
		"ARTICOLO": "5008083000000",
		"CodArtClienteFornitore": "928083605133"
	},
	{
		"ARTICOLO": "5008094200000",
		"CodArtClienteFornitore": "928084805133"
	},
	{
		"ARTICOLO": "5008283000000",
		"CodArtClienteFornitore": "928183405125"
	},
	{
		"ARTICOLO": "5008383000000",
		"CodArtClienteFornitore": "928085205125"
	},
	{
		"ARTICOLO": "5008394200000",
		"CodArtClienteFornitore": "928093805125"
	},
	{
		"ARTICOLO": "5008483000000",
		"CodArtClienteFornitore": "928086505131"
	},
	{
		"ARTICOLO": "5008494200000",
		"CodArtClienteFornitore": "928094605175"
	},
	{
		"ARTICOLO": "5008683000000",
		"CodArtClienteFornitore": "928082305176"
	},
	{
		"ARTICOLO": "5008694200000",
		"CodArtClienteFornitore": "928084505131"
	},
	{
		"ARTICOLO": "5008783000000",
		"CodArtClienteFornitore": "928083705176"
	},
	{
		"ARTICOLO": "5008794200000",
		"CodArtClienteFornitore": "928084605131"
	},
	{
		"ARTICOLO": "5008983030000",
		"CodArtClienteFornitore": "928108200630"
	},
	{
		"ARTICOLO": "5009083040000",
		"CodArtClienteFornitore": "928109500630"
	},
	{
		"ARTICOLO": "5009094240000",
		"CodArtClienteFornitore": "928053100630"
	},
	{
		"ARTICOLO": "5009683000000",
		"CodArtClienteFornitore": "928087905130"
	},
	{
		"ARTICOLO": "50098930000EL",
		"CodArtClienteFornitore": "928189105131"
	},
	{
		"ARTICOLO": "50099930000EL",
		"CodArtClienteFornitore": "928189505131"
	},
	{
		"ARTICOLO": "500D193025000",
		"CodArtClienteFornitore": "928194705330"
	},
	{
		"ARTICOLO": "500E193000000",
		"CodArtClienteFornitore": "928191705131"
	},
	{
		"ARTICOLO": "500E393000000",
		"CodArtClienteFornitore": "928094705125"
	},
	{
		"ARTICOLO": "500ELG41WW",
		"CodArtClienteFornitore": "8718696422281"
	},
	{
		"ARTICOLO": "500G427024000",
		"CodArtClienteFornitore": "929001123802"
	},
	{
		"ARTICOLO": "500G692725000",
		"CodArtClienteFornitore": "929001138602"
	},
	{
		"ARTICOLO": "500G692740000",
		"CodArtClienteFornitore": "929001138702"
	},
	{
		"ARTICOLO": "500G693024000",
		"CodArtClienteFornitore": "929001138802"
	},
	{
		"ARTICOLO": "500G693040000",
		"CodArtClienteFornitore": "929001138902"
	},
	{
		"ARTICOLO": "500G694040000",
		"CodArtClienteFornitore": "929001139102"
	},
	{
		"ARTICOLO": "500HPI250BU",
		"CodArtClienteFornitore": "928076709891"
	},
	{
		"ARTICOLO": "500HPI400B",
		"CodArtClienteFornitore": "928074709891"
	},
	{
		"ARTICOLO": "500HPI400BU",
		"CodArtClienteFornitore": "928074309891"
	},
	{
		"ARTICOLO": "500HPI400BUIND",
		"CodArtClienteFornitore": "928074309891"
	},
	{
		"ARTICOLO": "500HPIT250",
		"CodArtClienteFornitore": "928481300098"
	},
	{
		"ARTICOLO": "500HPIT400",
		"CodArtClienteFornitore": "8711500179906"
	},
	{
		"ARTICOLO": "500HPIT400IND",
		"CodArtClienteFornitore": "928481600096"
	},
	{
		"ARTICOLO": "500M02704000N",
		"CodArtClienteFornitore": "929001145602"
	},
	{
		"ARTICOLO": "500M03004000N",
		"CodArtClienteFornitore": "929001145702"
	},
	{
		"ARTICOLO": "500M183010000",
		"CodArtClienteFornitore": "928191005331"
	},
	{
		"ARTICOLO": "500M327025000",
		"CodArtClienteFornitore": "929001139202"
	},
	{
		"ARTICOLO": "500M327040000",
		"CodArtClienteFornitore": "929001139302"
	},
	{
		"ARTICOLO": "500M393040000",
		"CodArtClienteFornitore": "929001139502"
	},
	{
		"ARTICOLO": "500M427024000",
		"CodArtClienteFornitore": "929001152802"
	},
	{
		"ARTICOLO": "500M427036000",
		"CodArtClienteFornitore": "929001153102"
	},
	{
		"ARTICOLO": "500M427060000",
		"CodArtClienteFornitore": "929001153402"
	},
	{
		"ARTICOLO": "500M430024000",
		"CodArtClienteFornitore": "929001152902"
	},
	{
		"ARTICOLO": "500M430036000",
		"CodArtClienteFornitore": "929001241402"
	},
	{
		"ARTICOLO": "500M430060000",
		"CodArtClienteFornitore": "929001153502"
	},
	{
		"ARTICOLO": "500M440024000",
		"CodArtClienteFornitore": "929001153002"
	},
	{
		"ARTICOLO": "500M440036000",
		"CodArtClienteFornitore": "929001153302"
	},
	{
		"ARTICOLO": "500M527024000",
		"CodArtClienteFornitore": "929001149802"
	},
	{
		"ARTICOLO": "500M527036000",
		"CodArtClienteFornitore": "8-50W 827 MR16 36D"
	},
	{
		"ARTICOLO": "500M530024000",
		"CodArtClienteFornitore": "929001149902"
	},
	{
		"ARTICOLO": "500M530036000",
		"CodArtClienteFornitore": "929001150202"
	},
	{
		"ARTICOLO": "500M727025000",
		"CodArtClienteFornitore": "929001198702"
	},
	{
		"ARTICOLO": "500M740025000",
		"CodArtClienteFornitore": "929001198802"
	},
	{
		"ARTICOLO": "500M827025000",
		"CodArtClienteFornitore": "929001142802"
	},
	{
		"ARTICOLO": "500Q68274P000",
		"CodArtClienteFornitore": "927914782771"
	},
	{
		"ARTICOLO": "500Q68304P000",
		"CodArtClienteFornitore": "927914783071"
	},
	{
		"ARTICOLO": "500Q68404P000",
		"CodArtClienteFornitore": "927914784071"
	},
	{
		"ARTICOLO": "500Q88304P000",
		"CodArtClienteFornitore": "927914483071"
	},
	{
		"ARTICOLO": "500R327024000",
		"CodArtClienteFornitore": "929001169802"
	},
	{
		"ARTICOLO": "500R327040000",
		"CodArtClienteFornitore": "929001170002"
	},
	{
		"ARTICOLO": "500R330024000",
		"CodArtClienteFornitore": "929001169902"
	},
	{
		"ARTICOLO": "500R330040000",
		"CodArtClienteFornitore": "929001170102"
	},
	{
		"ARTICOLO": "500R427024000",
		"CodArtClienteFornitore": "929000261002"
	},
	{
		"ARTICOLO": "500R427040000",
		"CodArtClienteFornitore": "929000261202"
	},
	{
		"ARTICOLO": "500R492740000",
		"CodArtClienteFornitore": "929001170402"
	},
	{
		"ARTICOLO": "500R493024000",
		"CodArtClienteFornitore": "929001170302"
	},
	{
		"ARTICOLO": "500R493040000",
		"CodArtClienteFornitore": "929001170502"
	},
	{
		"ARTICOLO": "500SDWT100",
		"CodArtClienteFornitore": "928154109278"
	},
	{
		"ARTICOLO": "500SDWTG100",
		"CodArtClienteFornitore": "928158905131"
	},
	{
		"ARTICOLO": "500SON70",
		"CodArtClienteFornitore": "928150108828"
	},
	{
		"ARTICOLO": "500SONTPLUS100",
		"CodArtClienteFornitore": "928151709230"
	},
	{
		"ARTICOLO": "500SONTPLUS150",
		"CodArtClienteFornitore": "928150909230"
	},
	{
		"ARTICOLO": "500SONTPLUS250",
		"CodArtClienteFornitore": "8711500179876"
	},
	{
		"ARTICOLO": "500W093000000",
		"CodArtClienteFornitore": "928064705131"
	},
	{
		"ARTICOLO": "500X0930000EL",
		"CodArtClienteFornitore": "928185205131"
	},
	{
		"ARTICOLO": "500X1942000EL",
		"CodArtClienteFornitore": "928193705131"
	},
	{
		"ARTICOLO": "500X293010000",
		"CodArtClienteFornitore": "8718291689188"
	},
	{
		"ARTICOLO": "500X293024000",
		"CodArtClienteFornitore": "928195405330"
	},
	{
		"ARTICOLO": "500X293040000",
		"CodArtClienteFornitore": "928195505330"
	},
	{
		"ARTICOLO": "500X383024000",
		"CodArtClienteFornitore": "928195705330"
	},
	{
		"ARTICOLO": "500X393024000",
		"CodArtClienteFornitore": "928195705330"
	},
	{
		"ARTICOLO": "500X393040000",
		"CodArtClienteFornitore": "928195805330"
	},
	{
		"ARTICOLO": "500X9",
		"CodArtClienteFornitore": "928191805131"
	},
	{
		"ARTICOLO": "5019510A020EX",
		"CodArtClienteFornitore": "913700624466"
	},
	{
		"ARTICOLO": "501CERTALINE",
		"CodArtClienteFornitore": "913700633991"
	},
	{
		"ARTICOLO": "913700624466",
		"CodArtClienteFornitore": "913700624466"
	},
	{
		"ARTICOLO": "913700652966",
		"CodArtClienteFornitore": "913700652966"
	},
	{
		"ARTICOLO": "913700653266",
		"CodArtClienteFornitore": "913700653266"
	},
	{
		"ARTICOLO": "913700685566",
		"CodArtClienteFornitore": "913700685566"
	},
	{
		"ARTICOLO": "913700696166",
		"CodArtClienteFornitore": "913700696166"
	},
	{
		"ARTICOLO": "913712009566",
		"CodArtClienteFornitore": "913712009566"
	},
	{
		"ARTICOLO": "913712009666",
		"CodArtClienteFornitore": "913712009666"
	},
	{
		"ARTICOLO": "927914383071",
		"CodArtClienteFornitore": "927914383071"
	},
	{
		"ARTICOLO": "928481600096",
		"CodArtClienteFornitore": "928481600096"
	},
	{
		"ARTICOLO": "928482600096",
		"CodArtClienteFornitore": "928482600096"
	},
	{
		"ARTICOLO": "929001243702",
		"CodArtClienteFornitore": "929001243702"
	},
	{
		"ARTICOLO": "929001243802",
		"CodArtClienteFornitore": "929001243802"
	},
	{
		"ARTICOLO": "929001412680",
		"CodArtClienteFornitore": "929001412680"
	},
	{
		"ARTICOLO": "929001419106",
		"CodArtClienteFornitore": "929001419106"
	},
	{
		"ARTICOLO": "929001461680",
		"CodArtClienteFornitore": "929001461680"
	},
	{
		"ARTICOLO": "HFMRED124S",
		"CodArtClienteFornitore": "931700420666"
	},
	{
		"ARTICOLO": "PHIHID-PVC150",
		"CodArtClienteFornitore": "913700624466"
	},
	{
		"ARTICOLO": "DLB1248-1CV-DALI",
		"CodArtClienteFornitore": "DLB1248-1CV-DALI"
	},
	{
		"ARTICOLO": "DLC1248",
		"CodArtClienteFornitore": "DLC1248-1CV-BTN"
	},
	{
		"ARTICOLO": "DLC1248-1CV-110",
		"CodArtClienteFornitore": "DLC1248-1CV-110"
	},
	{
		"ARTICOLO": "09.45.060",
		"CodArtClienteFornitore": "09.45.060"
	},
	{
		"ARTICOLO": "ILMAS001K",
		"CodArtClienteFornitore": "ILMAS001"
	},
	{
		"ARTICOLO": "ILMAS003",
		"CodArtClienteFornitore": "ILMAS003"
	},
	{
		"ARTICOLO": "ILMAS005",
		"CodArtClienteFornitore": "ILMAS005"
	},
	{
		"ARTICOLO": "ILMAS006",
		"CodArtClienteFornitore": "ILMAS006"
	},
	{
		"ARTICOLO": "ILMAS009",
		"CodArtClienteFornitore": "ILMAS009"
	},
	{
		"ARTICOLO": "ILMAS011",
		"CodArtClienteFornitore": "ILMAS011"
	},
	{
		"ARTICOLO": "ILMAS012",
		"CodArtClienteFornitore": "ILMAS012"
	},
	{
		"ARTICOLO": "ILMAS013",
		"CodArtClienteFornitore": "ILMAS013"
	},
	{
		"ARTICOLO": "ILMAS015",
		"CodArtClienteFornitore": "ILMAS015"
	},
	{
		"ARTICOLO": "ILMAS016",
		"CodArtClienteFornitore": "ILMAS016"
	},
	{
		"ARTICOLO": "ILMAS017",
		"CodArtClienteFornitore": "ILMAS017"
	},
	{
		"ARTICOLO": "ILMAS020",
		"CodArtClienteFornitore": "ILMAS020"
	},
	{
		"ARTICOLO": "ILMAS021",
		"CodArtClienteFornitore": "ILMAS021"
	},
	{
		"ARTICOLO": "ILMAS022",
		"CodArtClienteFornitore": "ILMAS022"
	},
	{
		"ARTICOLO": "ELC040650600",
		"CodArtClienteFornitore": "ELC040650600"
	},
	{
		"ARTICOLO": "ELC040655300",
		"CodArtClienteFornitore": "ELC040655300"
	},
	{
		"ARTICOLO": "A2213480-1",
		"CodArtClienteFornitore": "2213480-1"
	},
	{
		"ARTICOLO": "AL2C5-30801211E1900",
		"CodArtClienteFornitore": "L2C5-30801211E1900"
	},
	{
		"ARTICOLO": "AL2C5-RM001211E1900",
		"CodArtClienteFornitore": "L2C5-RM001211E1900"
	},
	{
		"ARTICOLO": "19PG24L3",
		"CodArtClienteFornitore": "19PG24L3"
	},
	{
		"ARTICOLO": "19PG24L3C8",
		"CodArtClienteFornitore": "19PG24L3C8"
	},
	{
		"ARTICOLO": "19PG30L4",
		"CodArtClienteFornitore": "19PG30L4"
	},
	{
		"ARTICOLO": "CNAC221070CT1021",
		"CodArtClienteFornitore": "CNAC221070CT1021"
	},
	{
		"ARTICOLO": "ITDCUSB060CT2132",
		"CodArtClienteFornitore": "ITDCUSB060CT2132"
	},
	{
		"ARTICOLO": "ITWSBAT000WL8B81",
		"CodArtClienteFornitore": "ITWSBAT000WL8B81"
	},
	{
		"ARTICOLO": "PSL500CCS7223C3E",
		"CodArtClienteFornitore": "PSL500CCS7223C3E"
	},
	{
		"ARTICOLO": "PSL500CCSN228C3E",
		"CodArtClienteFornitore": "PSL500CCSN228C3E"
	},
	{
		"ARTICOLO": "680.16.01.30",
		"CodArtClienteFornitore": "680.16.01.30"
	},
	{
		"ARTICOLO": "CAFR01",
		"CodArtClienteFornitore": "CAFR01"
	},
	{
		"ARTICOLO": "2213130-1",
		"CodArtClienteFornitore": "2213130-1"
	},
	{
		"ARTICOLO": "L2C5-57701208E1500",
		"CodArtClienteFornitore": "L2C5-57701208E1500"
	},
	{
		"ARTICOLO": "1130950401",
		"CodArtClienteFornitore": "1130950401"
	},
	{
		"ARTICOLO": "2411021000",
		"CodArtClienteFornitore": "2411021000"
	},
	{
		"ARTICOLO": "2414123049",
		"CodArtClienteFornitore": "2414123049"
	},
	{
		"ARTICOLO": "ILMAS100",
		"CodArtClienteFornitore": "B.346.C.01"
	},
	{
		"ARTICOLO": "ILMAS101",
		"CodArtClienteFornitore": "B.346.C.02"
	},
	{
		"ARTICOLO": "ILMAS124",
		"CodArtClienteFornitore": "1005"
	},
	{
		"ARTICOLO": "ILMAS125",
		"CodArtClienteFornitore": "1006"
	},
	{
		"ARTICOLO": "ILMAS142",
		"CodArtClienteFornitore": "ILMAS142"
	},
	{
		"ARTICOLO": "8793433",
		"CodArtClienteFornitore": "8793433"
	},
	{
		"ARTICOLO": "448180",
		"CodArtClienteFornitore": "448180"
	},
	{
		"ARTICOLO": "599B-02",
		"CodArtClienteFornitore": "599B-02"
	},
	{
		"ARTICOLO": "FX951-62",
		"CodArtClienteFornitore": "FX951-62"
	},
	{
		"ARTICOLO": "T15-BLL",
		"CodArtClienteFornitore": "T15-BLL"
	},
	{
		"ARTICOLO": "T15-IL",
		"CodArtClienteFornitore": "T15-IL"
	},
	{
		"ARTICOLO": "ET60-24VF1",
		"CodArtClienteFornitore": "ET60-24VF1"
	},
	{
		"ARTICOLO": "SNP-30-12VF-1",
		"CodArtClienteFornitore": "SNP-30-12VF-1"
	},
	{
		"ARTICOLO": "0600000U",
		"CodArtClienteFornitore": "0600000U"
	},
	{
		"ARTICOLO": "1060SABB",
		"CodArtClienteFornitore": "1060"
	},
	{
		"ARTICOLO": "1060TRASP",
		"CodArtClienteFornitore": "1060"
	},
	{
		"ARTICOLO": "1160SABB",
		"CodArtClienteFornitore": "1160"
	},
	{
		"ARTICOLO": "1160TRASP",
		"CodArtClienteFornitore": "1160"
	},
	{
		"ARTICOLO": "FL0058-WW",
		"CodArtClienteFornitore": "FL0058-WW"
	},
	{
		"ARTICOLO": "FL0058I-WW",
		"CodArtClienteFornitore": "FL0058I-WW"
	},
	{
		"ARTICOLO": "002.025.8020.00",
		"CodArtClienteFornitore": "002.025.8020.00"
	},
	{
		"ARTICOLO": "003.025.2000.05",
		"CodArtClienteFornitore": "003.025.2000.05"
	},
	{
		"ARTICOLO": "005.024.2001.23",
		"CodArtClienteFornitore": "005.024.2001.23"
	},
	{
		"ARTICOLO": "032.024.0001.00",
		"CodArtClienteFornitore": "032.024.0001.00"
	},
	{
		"ARTICOLO": "P75EPDM",
		"CodArtClienteFornitore": "P 75 EPDM"
	},
	{
		"ARTICOLO": "SEMALA3",
		"CodArtClienteFornitore": "SEMALA3"
	},
	{
		"ARTICOLO": "H4L6302002000",
		"CodArtClienteFornitore": "H4L6302002000"
	},
	{
		"ARTICOLO": "H4L6303003000",
		"CodArtClienteFornitore": "H4L6303003000"
	},
	{
		"ARTICOLO": "5993040106000",
		"CodArtClienteFornitore": "5934-0-2-262-04 E27"
	},
	{
		"ARTICOLO": "59930401860M2",
		"CodArtClienteFornitore": "5934-6-3-752-01"
	},
	{
		"ARTICOLO": "5993102206000",
		"CodArtClienteFornitore": "5002-0-2-265-34"
	},
	{
		"ARTICOLO": "5993103208000",
		"CodArtClienteFornitore": "5003-0-2-266-34"
	},
	{
		"ARTICOLO": "5993103208002",
		"CodArtClienteFornitore": "5003-0-0-266-31"
	},
	{
		"ARTICOLO": "5993105106000",
		"CodArtClienteFornitore": "5005-0-2-262-04"
	},
	{
		"ARTICOLO": "5993106108000",
		"CodArtClienteFornitore": "5006-0-2-263-04"
	},
	{
		"ARTICOLO": "5993120206000",
		"CodArtClienteFornitore": "5101-0-2-265-34"
	},
	{
		"ARTICOLO": "5993121146000",
		"CodArtClienteFornitore": "5102-7-3-424-34"
	},
	{
		"ARTICOLO": "5993200104000",
		"CodArtClienteFornitore": "4001-0-2-221-04"
	},
	{
		"ARTICOLO": "5993200105000",
		"CodArtClienteFornitore": "4001-0-2-261-04"
	},
	{
		"ARTICOLO": "5993830141000",
		"CodArtClienteFornitore": "6061-0-3-412-34"
	},
	{
		"ARTICOLO": "5993830141001",
		"CodArtClienteFornitore": "6061-0-3-412-36"
	},
	{
		"ARTICOLO": "5993900102000",
		"CodArtClienteFornitore": "7009-0-2-493-34"
	},
	{
		"ARTICOLO": "5993900153000",
		"CodArtClienteFornitore": "7009-0-2-493-04"
	},
	{
		"ARTICOLO": "599390015300E",
		"CodArtClienteFornitore": "7009-0-2-493-RAL7043"
	},
	{
		"ARTICOLO": "5993901254000",
		"CodArtClienteFornitore": "7010-0-2-496-04"
	},
	{
		"ARTICOLO": "5993921153000",
		"CodArtClienteFornitore": "7013-0-2-493-04"
	},
	{
		"ARTICOLO": "599392115300E",
		"CodArtClienteFornitore": "7013-0-2-493-RAL7043"
	},
	{
		"ARTICOLO": "5993922254000",
		"CodArtClienteFornitore": "7014-0-2-496-04"
	},
	{
		"ARTICOLO": "5993931153000",
		"CodArtClienteFornitore": "7015-0-2-493-04"
	},
	{
		"ARTICOLO": "599393115300E",
		"CodArtClienteFornitore": "7015-0-2-493-RAL7043"
	},
	{
		"ARTICOLO": "5993990146000",
		"CodArtClienteFornitore": "7028-0-3-424-04"
	},
	{
		"ARTICOLO": "5993990146001",
		"CodArtClienteFornitore": "7028-0-3-424-06"
	},
	{
		"ARTICOLO": "5994076153000",
		"CodArtClienteFornitore": "7153-0-2-493-34"
	},
	{
		"ARTICOLO": "5995311111SR5",
		"CodArtClienteFornitore": "7406-0-2-281-92"
	},
	{
		"ARTICOLO": "DL-608/1T.00",
		"CodArtClienteFornitore": "0340-0-8-000-51-303"
	},
	{
		"ARTICOLO": "DL-608/1T.10",
		"CodArtClienteFornitore": "0340-0-8-000-56-303"
	},
	{
		"ARTICOLO": "DL-608/2LI.00",
		"CodArtClienteFornitore": "30411412"
	},
	{
		"ARTICOLO": "DL-608/4.T0",
		"CodArtClienteFornitore": "SUPPORT RING"
	},
	{
		"ARTICOLO": "0530XXJ1",
		"CodArtClienteFornitore": "TR530-W"
	},
	{
		"ARTICOLO": "0530XXJ2",
		"CodArtClienteFornitore": "TR530-B"
	},
	{
		"ARTICOLO": "0530XXN1",
		"CodArtClienteFornitore": "TR530-B"
	},
	{
		"ARTICOLO": "0530XXN2",
		"CodArtClienteFornitore": "TR530-W"
	},
	{
		"ARTICOLO": "0596XXX1",
		"CodArtClienteFornitore": "VP1042-W"
	},
	{
		"ARTICOLO": "0597XXX1",
		"CodArtClienteFornitore": "VP1242-W"
	},
	{
		"ARTICOLO": "0660XXE0",
		"CodArtClienteFornitore": "COL1440-S"
	},
	{
		"ARTICOLO": "0660XXE1",
		"CodArtClienteFornitore": "COL1440-W"
	},
	{
		"ARTICOLO": "0660XXE2",
		"CodArtClienteFornitore": "COL1040-B"
	},
	{
		"ARTICOLO": "0660XXE9",
		"CodArtClienteFornitore": "COL1440-WW"
	},
	{
		"ARTICOLO": "0660XXN0",
		"CodArtClienteFornitore": "COL1441-S"
	},
	{
		"ARTICOLO": "0660XXN1",
		"CodArtClienteFornitore": "COL1441-W"
	},
	{
		"ARTICOLO": "0660XXN2",
		"CodArtClienteFornitore": "COL1441-B"
	},
	{
		"ARTICOLO": "0660XXN9",
		"CodArtClienteFornitore": "COL1441-WW"
	},
	{
		"ARTICOLO": "0660XXT0",
		"CodArtClienteFornitore": "COL1442-S"
	},
	{
		"ARTICOLO": "0660XXT1",
		"CodArtClienteFornitore": "COL1442-W"
	},
	{
		"ARTICOLO": "0660XXT2",
		"CodArtClienteFornitore": "COL1442-B"
	},
	{
		"ARTICOLO": "0660XXT9",
		"CodArtClienteFornitore": "COL1442-WW"
	},
	{
		"ARTICOLO": "0661XXG0",
		"CodArtClienteFornitore": "COL1640-S"
	},
	{
		"ARTICOLO": "0661XXG1",
		"CodArtClienteFornitore": "COL1640-W"
	},
	{
		"ARTICOLO": "0661XXG2",
		"CodArtClienteFornitore": "COL1640-B"
	},
	{
		"ARTICOLO": "0661XXG9",
		"CodArtClienteFornitore": "COL1640-WW"
	},
	{
		"ARTICOLO": "0661XXJ0",
		"CodArtClienteFornitore": "COL1641-S"
	},
	{
		"ARTICOLO": "0661XXJ1",
		"CodArtClienteFornitore": "COL1641-W"
	},
	{
		"ARTICOLO": "0661XXJ2",
		"CodArtClienteFornitore": "COL1641-B"
	},
	{
		"ARTICOLO": "0661XXJ9",
		"CodArtClienteFornitore": "COL1641-WW"
	},
	{
		"ARTICOLO": "0661XXR0",
		"CodArtClienteFornitore": "COL1642-S"
	},
	{
		"ARTICOLO": "0661XXR1",
		"CodArtClienteFornitore": "COL1642-W"
	},
	{
		"ARTICOLO": "0661XXR2",
		"CodArtClienteFornitore": "COL1642-B"
	},
	{
		"ARTICOLO": "0661XXR9",
		"CodArtClienteFornitore": "COL1642-WW"
	},
	{
		"ARTICOLO": "0662XXG0",
		"CodArtClienteFornitore": "COL1940-S"
	},
	{
		"ARTICOLO": "0662XXG1",
		"CodArtClienteFornitore": "COL1940-W"
	},
	{
		"ARTICOLO": "0662XXG2",
		"CodArtClienteFornitore": "COL1940-B"
	},
	{
		"ARTICOLO": "0662XXG9",
		"CodArtClienteFornitore": "COL1940-WW"
	},
	{
		"ARTICOLO": "0662XXJ0",
		"CodArtClienteFornitore": "COL1941-S"
	},
	{
		"ARTICOLO": "0662XXJ1",
		"CodArtClienteFornitore": "COL1941-W"
	},
	{
		"ARTICOLO": "0662XXJ2",
		"CodArtClienteFornitore": "COL1941-B"
	},
	{
		"ARTICOLO": "0662XXJ9",
		"CodArtClienteFornitore": "COL1941-WW"
	},
	{
		"ARTICOLO": "0662XXR0",
		"CodArtClienteFornitore": "COL1942-S"
	},
	{
		"ARTICOLO": "0662XXR1",
		"CodArtClienteFornitore": "COL1942-W"
	},
	{
		"ARTICOLO": "0662XXR2",
		"CodArtClienteFornitore": "COL1942-B"
	},
	{
		"ARTICOLO": "0662XXR9",
		"CodArtClienteFornitore": "COL1942-WW"
	},
	{
		"ARTICOLO": "0670XXE0",
		"CodArtClienteFornitore": "NPL1240-S"
	},
	{
		"ARTICOLO": "0670XXE1",
		"CodArtClienteFornitore": "NPL1240-W"
	},
	{
		"ARTICOLO": "0670XXE2",
		"CodArtClienteFornitore": "NPL1240-B"
	},
	{
		"ARTICOLO": "0670XXE9",
		"CodArtClienteFornitore": "NPL1240-WW"
	},
	{
		"ARTICOLO": "0670XXN0",
		"CodArtClienteFornitore": "NPL1241-S"
	},
	{
		"ARTICOLO": "0670XXN1",
		"CodArtClienteFornitore": "NPL1241-W"
	},
	{
		"ARTICOLO": "0670XXN2",
		"CodArtClienteFornitore": "NPL1241-B"
	},
	{
		"ARTICOLO": "0670XXN9",
		"CodArtClienteFornitore": "NPL1241-WW"
	},
	{
		"ARTICOLO": "0670XXT0",
		"CodArtClienteFornitore": "NPL1242-S"
	},
	{
		"ARTICOLO": "0670XXT1",
		"CodArtClienteFornitore": "NPL1242-W"
	},
	{
		"ARTICOLO": "0670XXT2",
		"CodArtClienteFornitore": "NPL1242-B"
	},
	{
		"ARTICOLO": "0670XXT9",
		"CodArtClienteFornitore": "NPL1242-WW"
	},
	{
		"ARTICOLO": "0671XXG0",
		"CodArtClienteFornitore": "NPL1440-S"
	},
	{
		"ARTICOLO": "0671XXG1",
		"CodArtClienteFornitore": "NPL1440-W"
	},
	{
		"ARTICOLO": "0671XXG2",
		"CodArtClienteFornitore": "NPL1440-B"
	},
	{
		"ARTICOLO": "0671XXG9",
		"CodArtClienteFornitore": "NPL1440-WW"
	},
	{
		"ARTICOLO": "0671XXJ0",
		"CodArtClienteFornitore": "NPL1441-S"
	},
	{
		"ARTICOLO": "0671XXJ1",
		"CodArtClienteFornitore": "NPL1441-W"
	},
	{
		"ARTICOLO": "0671XXJ2",
		"CodArtClienteFornitore": "NPL1441-B"
	},
	{
		"ARTICOLO": "0671XXJ9",
		"CodArtClienteFornitore": "NPL1441-WW"
	},
	{
		"ARTICOLO": "0671XXR0",
		"CodArtClienteFornitore": "NPL1442-S"
	},
	{
		"ARTICOLO": "0671XXR1",
		"CodArtClienteFornitore": "NPL1442-W"
	},
	{
		"ARTICOLO": "0671XXR2",
		"CodArtClienteFornitore": "NPL1442-B"
	},
	{
		"ARTICOLO": "0671XXR9",
		"CodArtClienteFornitore": "NPL1442-WW"
	},
	{
		"ARTICOLO": "0672XXG0",
		"CodArtClienteFornitore": "NPL1640-S"
	},
	{
		"ARTICOLO": "0672XXG1",
		"CodArtClienteFornitore": "NPL1640-W"
	},
	{
		"ARTICOLO": "0672XXG2",
		"CodArtClienteFornitore": "NPL1640-B"
	},
	{
		"ARTICOLO": "0672XXG9",
		"CodArtClienteFornitore": "NPL1640-WW"
	},
	{
		"ARTICOLO": "0672XXJ0",
		"CodArtClienteFornitore": "NPL1641-S"
	},
	{
		"ARTICOLO": "0672XXJ1",
		"CodArtClienteFornitore": "NPL1641-W"
	},
	{
		"ARTICOLO": "0672XXJ2",
		"CodArtClienteFornitore": "NPL1641-B"
	},
	{
		"ARTICOLO": "0672XXJ9",
		"CodArtClienteFornitore": "NPL1641-WW"
	},
	{
		"ARTICOLO": "0672XXR0",
		"CodArtClienteFornitore": "NPL1642-S"
	},
	{
		"ARTICOLO": "0672XXR1",
		"CodArtClienteFornitore": "NPL1642-W"
	},
	{
		"ARTICOLO": "0672XXR2",
		"CodArtClienteFornitore": "NPL1642-B"
	},
	{
		"ARTICOLO": "0672XXR9",
		"CodArtClienteFornitore": "NPL1642-WW"
	},
	{
		"ARTICOLO": "0730XXG0",
		"CodArtClienteFornitore": "LDS7240-S"
	},
	{
		"ARTICOLO": "0730XXG1",
		"CodArtClienteFornitore": "LDS7240-W"
	},
	{
		"ARTICOLO": "0730XXG2",
		"CodArtClienteFornitore": "LDS7240-B"
	},
	{
		"ARTICOLO": "0730XXJ0",
		"CodArtClienteFornitore": "LDS7241-S"
	},
	{
		"ARTICOLO": "0730XXJ1",
		"CodArtClienteFornitore": "LDS7241-W"
	},
	{
		"ARTICOLO": "0730XXJ2",
		"CodArtClienteFornitore": "LDS7241-B"
	},
	{
		"ARTICOLO": "0730XXR0",
		"CodArtClienteFornitore": "LDS7242-S"
	},
	{
		"ARTICOLO": "0730XXR1",
		"CodArtClienteFornitore": "LDS7242-W"
	},
	{
		"ARTICOLO": "0730XXR2",
		"CodArtClienteFornitore": "LDS7242-B"
	},
	{
		"ARTICOLO": "0731XXG0",
		"CodArtClienteFornitore": "LDS7340-S"
	},
	{
		"ARTICOLO": "0731XXG1",
		"CodArtClienteFornitore": "LDS7340-W"
	},
	{
		"ARTICOLO": "0731XXG2",
		"CodArtClienteFornitore": "LDS7340-B"
	},
	{
		"ARTICOLO": "0731XXJ0",
		"CodArtClienteFornitore": "LDS7341-S"
	},
	{
		"ARTICOLO": "0731XXJ1",
		"CodArtClienteFornitore": "LDS7341-W"
	},
	{
		"ARTICOLO": "0731XXJ2",
		"CodArtClienteFornitore": "LDS7341-B"
	},
	{
		"ARTICOLO": "0731XXR0",
		"CodArtClienteFornitore": "LDS7342-S"
	},
	{
		"ARTICOLO": "0731XXR1",
		"CodArtClienteFornitore": "LDS7342-W"
	},
	{
		"ARTICOLO": "0731XXR2",
		"CodArtClienteFornitore": "LDS7342-B"
	},
	{
		"ARTICOLO": "0732XXD0",
		"CodArtClienteFornitore": "LDS7440-S"
	},
	{
		"ARTICOLO": "0732XXD1",
		"CodArtClienteFornitore": "LDS7440-W"
	},
	{
		"ARTICOLO": "0732XXD2",
		"CodArtClienteFornitore": "LDS7440-B"
	},
	{
		"ARTICOLO": "0732XXH0",
		"CodArtClienteFornitore": "LDS7441-S"
	},
	{
		"ARTICOLO": "0732XXH1",
		"CodArtClienteFornitore": "LDS7442-W"
	},
	{
		"ARTICOLO": "0732XXH2",
		"CodArtClienteFornitore": "LDS7441-B"
	},
	{
		"ARTICOLO": "0732XXP0",
		"CodArtClienteFornitore": "LDS7442-S"
	},
	{
		"ARTICOLO": "0732XXP1",
		"CodArtClienteFornitore": "LDS7442-W"
	},
	{
		"ARTICOLO": "0732XXP2",
		"CodArtClienteFornitore": "LDS7442-B"
	},
	{
		"ARTICOLO": "0786XXR0",
		"CodArtClienteFornitore": "SLHAM1224-S"
	},
	{
		"ARTICOLO": "0786XXR1",
		"CodArtClienteFornitore": "SLHAM1242-W"
	},
	{
		"ARTICOLO": "0786XXR1S",
		"CodArtClienteFornitore": "SLHAM1242-W-S"
	},
	{
		"ARTICOLO": "0786XXR2",
		"CodArtClienteFornitore": "SLHAM1242-B"
	},
	{
		"ARTICOLO": "0787XXR0",
		"CodArtClienteFornitore": "SLHAM1672-S"
	},
	{
		"ARTICOLO": "0787XXR1",
		"CodArtClienteFornitore": "SLHAM1672-W"
	},
	{
		"ARTICOLO": "0787XXR1S",
		"CodArtClienteFornitore": "SLHAM1672-W-S"
	},
	{
		"ARTICOLO": "0787XXR2",
		"CodArtClienteFornitore": "SLHAM1672-B"
	},
	{
		"ARTICOLO": "0787XXRS",
		"CodArtClienteFornitore": "SLHAM1672-S"
	},
	{
		"ARTICOLO": "0787XXRW",
		"CodArtClienteFornitore": "SLHAM1672-RAL9010"
	},
	{
		"ARTICOLO": "0788XXR1",
		"CodArtClienteFornitore": "SLHAM2072-W"
	},
	{
		"ARTICOLO": "0788XXR1S",
		"CodArtClienteFornitore": "SLHAM2072-W-S"
	},
	{
		"ARTICOLO": "0788XXRS",
		"CodArtClienteFornitore": "SLHAM2072-RAL7037"
	},
	{
		"ARTICOLO": "0788XXRW",
		"CodArtClienteFornitore": "SLHAM2072-RAL9010"
	},
	{
		"ARTICOLO": "0788XXT0",
		"CodArtClienteFornitore": "SLHAM2072-S"
	},
	{
		"ARTICOLO": "0788XXT1",
		"CodArtClienteFornitore": "SLHAM2072-W"
	},
	{
		"ARTICOLO": "0788XXT2",
		"CodArtClienteFornitore": "SLHAM2072-B"
	},
	{
		"ARTICOLO": "0788XXTW",
		"CodArtClienteFornitore": "SLHAM2072-RAL9010"
	},
	{
		"ARTICOLO": "0789XXT1",
		"CodArtClienteFornitore": "SLHAM2372-W"
	},
	{
		"ARTICOLO": "0796XXC0",
		"CodArtClienteFornitore": "LHAM1270-S"
	},
	{
		"ARTICOLO": "0796XXC1",
		"CodArtClienteFornitore": "LHAM1270-W"
	},
	{
		"ARTICOLO": "0796XXC2",
		"CodArtClienteFornitore": "LHAM1270-B"
	},
	{
		"ARTICOLO": "0796XXK0",
		"CodArtClienteFornitore": "LHAM1271-S"
	},
	{
		"ARTICOLO": "0796XXK1",
		"CodArtClienteFornitore": "LHAM1271-W"
	},
	{
		"ARTICOLO": "0796XXK2",
		"CodArtClienteFornitore": "LHAM1271-B"
	},
	{
		"ARTICOLO": "0796XXR0",
		"CodArtClienteFornitore": "LHAM1272-S"
	},
	{
		"ARTICOLO": "0796XXR1",
		"CodArtClienteFornitore": "LHAM1272-W"
	},
	{
		"ARTICOLO": "0796XXR1S",
		"CodArtClienteFornitore": "LHAM1272-W-S"
	},
	{
		"ARTICOLO": "0796XXR2",
		"CodArtClienteFornitore": "LHAM1272-B"
	},
	{
		"ARTICOLO": "0797XXC0",
		"CodArtClienteFornitore": "LHAM1670-S"
	},
	{
		"ARTICOLO": "0797XXC1",
		"CodArtClienteFornitore": "LHAM1670-W"
	},
	{
		"ARTICOLO": "0797XXC2",
		"CodArtClienteFornitore": "LHAM1670-B"
	},
	{
		"ARTICOLO": "0797XXK0",
		"CodArtClienteFornitore": "LHAM1671-S"
	},
	{
		"ARTICOLO": "0797XXK1",
		"CodArtClienteFornitore": "LHAM1671-W"
	},
	{
		"ARTICOLO": "0797XXK2",
		"CodArtClienteFornitore": "LHAM1671-B"
	},
	{
		"ARTICOLO": "0797XXR0",
		"CodArtClienteFornitore": "LHAM1672-S"
	},
	{
		"ARTICOLO": "0797XXR1",
		"CodArtClienteFornitore": "LHAM1672-W"
	},
	{
		"ARTICOLO": "0797XXR1S",
		"CodArtClienteFornitore": "LHAM1672-W-S"
	},
	{
		"ARTICOLO": "0797XXR2",
		"CodArtClienteFornitore": "LHAM-1672-B"
	},
	{
		"ARTICOLO": "0797XXR2S",
		"CodArtClienteFornitore": "LHAM1672-B-S"
	},
	{
		"ARTICOLO": "0798XXE0",
		"CodArtClienteFornitore": "LHAM2070-S"
	},
	{
		"ARTICOLO": "0798XXE1",
		"CodArtClienteFornitore": "LHAM2070-W"
	},
	{
		"ARTICOLO": "0798XXE2",
		"CodArtClienteFornitore": "LHAM2070-B"
	},
	{
		"ARTICOLO": "0798XXN0",
		"CodArtClienteFornitore": "LHAM2071-S"
	},
	{
		"ARTICOLO": "0798XXN1",
		"CodArtClienteFornitore": "LHAM2071-W"
	},
	{
		"ARTICOLO": "0798XXN2",
		"CodArtClienteFornitore": "LHAM2071-B"
	},
	{
		"ARTICOLO": "0798XXT0",
		"CodArtClienteFornitore": "LHAM2072-S"
	},
	{
		"ARTICOLO": "0798XXT1",
		"CodArtClienteFornitore": "LHAM2072-W"
	},
	{
		"ARTICOLO": "0798XXT1S",
		"CodArtClienteFornitore": "LHAM2072-W-S"
	},
	{
		"ARTICOLO": "0798XXT2",
		"CodArtClienteFornitore": "LHAM2072-B"
	},
	{
		"ARTICOLO": "0799XXE0",
		"CodArtClienteFornitore": "LHAM2370-S"
	},
	{
		"ARTICOLO": "0799XXE1",
		"CodArtClienteFornitore": "LHAM2370-W"
	},
	{
		"ARTICOLO": "0799XXE2",
		"CodArtClienteFornitore": "LHAM2370-B"
	},
	{
		"ARTICOLO": "0799XXN0",
		"CodArtClienteFornitore": "LHAM2371-S"
	},
	{
		"ARTICOLO": "0799XXN1",
		"CodArtClienteFornitore": "LHAM2371-W"
	},
	{
		"ARTICOLO": "0799XXN2",
		"CodArtClienteFornitore": "LHAM2371-B"
	},
	{
		"ARTICOLO": "0799XXT0",
		"CodArtClienteFornitore": "LHAM2373-S"
	},
	{
		"ARTICOLO": "0799XXT1",
		"CodArtClienteFornitore": "LHAM2373-W"
	},
	{
		"ARTICOLO": "0799XXT1S",
		"CodArtClienteFornitore": "LHAM2373-W-S"
	},
	{
		"ARTICOLO": "0799XXT2",
		"CodArtClienteFornitore": "LHAM2372-B"
	},
	{
		"ARTICOLO": "0830XXJ1",
		"CodArtClienteFornitore": "TR830-W"
	},
	{
		"ARTICOLO": "0830XXJ2",
		"CodArtClienteFornitore": "TR830-B"
	},
	{
		"ARTICOLO": "0900XXJ1",
		"CodArtClienteFornitore": "SH900-W"
	},
	{
		"ARTICOLO": "0900XXJ2",
		"CodArtClienteFornitore": "SH900-B"
	},
	{
		"ARTICOLO": "1230XXJ1",
		"CodArtClienteFornitore": "SH1230-W"
	},
	{
		"ARTICOLO": "1230XXJ2",
		"CodArtClienteFornitore": "SH1230-B"
	},
	{
		"ARTICOLO": "1233XXJ0",
		"CodArtClienteFornitore": "SHE1230-S"
	},
	{
		"ARTICOLO": "1233XXJ1",
		"CodArtClienteFornitore": "SHE1230-W"
	},
	{
		"ARTICOLO": "1233XXJ2",
		"CodArtClienteFornitore": "SHE1230-B"
	},
	{
		"ARTICOLO": "1240XXE0",
		"CodArtClienteFornitore": "SCOL-840-S"
	},
	{
		"ARTICOLO": "1240XXE1",
		"CodArtClienteFornitore": "SCOL-840-W"
	},
	{
		"ARTICOLO": "1240XXE2",
		"CodArtClienteFornitore": "SCOL-840-B"
	},
	{
		"ARTICOLO": "1240XXE9",
		"CodArtClienteFornitore": "SCOL-840-WW"
	},
	{
		"ARTICOLO": "1240XXN0",
		"CodArtClienteFornitore": "SCOL841-S"
	},
	{
		"ARTICOLO": "1240XXN1",
		"CodArtClienteFornitore": "SCOL-841-W"
	},
	{
		"ARTICOLO": "1240XXN2",
		"CodArtClienteFornitore": "SCOL-841-B"
	},
	{
		"ARTICOLO": "1240XXN9",
		"CodArtClienteFornitore": "SCOL841-WW"
	},
	{
		"ARTICOLO": "1240XXT0",
		"CodArtClienteFornitore": "SCOL842-S"
	},
	{
		"ARTICOLO": "1240XXT1",
		"CodArtClienteFornitore": "SCOL842-W"
	},
	{
		"ARTICOLO": "1240XXT2",
		"CodArtClienteFornitore": "SCOL842-B"
	},
	{
		"ARTICOLO": "1240XXT9",
		"CodArtClienteFornitore": "SCOL842-WW"
	},
	{
		"ARTICOLO": "1241XXG0",
		"CodArtClienteFornitore": "SCOL1040-S"
	},
	{
		"ARTICOLO": "1241XXG1",
		"CodArtClienteFornitore": "SCOL1040-W"
	},
	{
		"ARTICOLO": "1241XXG2",
		"CodArtClienteFornitore": "SCOL1040-B"
	},
	{
		"ARTICOLO": "1241XXG9",
		"CodArtClienteFornitore": "SCOL1040-WW"
	},
	{
		"ARTICOLO": "1241XXJ0",
		"CodArtClienteFornitore": "SCOL1041-S"
	},
	{
		"ARTICOLO": "1241XXJ1",
		"CodArtClienteFornitore": "SCOL1041-W"
	},
	{
		"ARTICOLO": "1241XXJ2",
		"CodArtClienteFornitore": "SCOL1041-B"
	},
	{
		"ARTICOLO": "1241XXJ9",
		"CodArtClienteFornitore": "SCOL1041-WW"
	},
	{
		"ARTICOLO": "1241XXR0",
		"CodArtClienteFornitore": "SCOL1042-S"
	},
	{
		"ARTICOLO": "1241XXR1",
		"CodArtClienteFornitore": "SCOL1042-W"
	},
	{
		"ARTICOLO": "1241XXR2",
		"CodArtClienteFornitore": "SCOL1042-B"
	},
	{
		"ARTICOLO": "1241XXR9",
		"CodArtClienteFornitore": "SCOL1042-WW"
	},
	{
		"ARTICOLO": "124200Y1",
		"CodArtClienteFornitore": "CM888-W"
	},
	{
		"ARTICOLO": "124200Y2",
		"CodArtClienteFornitore": "CM888-B"
	},
	{
		"ARTICOLO": "1242XXG0",
		"CodArtClienteFornitore": "SCOL1240-S"
	},
	{
		"ARTICOLO": "1242XXG1",
		"CodArtClienteFornitore": "SCOL1240-W"
	},
	{
		"ARTICOLO": "1242XXG2",
		"CodArtClienteFornitore": "SCOL1240-B"
	},
	{
		"ARTICOLO": "1242XXG9",
		"CodArtClienteFornitore": "SCOL1240-WW"
	},
	{
		"ARTICOLO": "1242XXJ0",
		"CodArtClienteFornitore": "SCOL1241-S"
	},
	{
		"ARTICOLO": "1242XXJ1",
		"CodArtClienteFornitore": "SCOL1241-W"
	},
	{
		"ARTICOLO": "1242XXJ2",
		"CodArtClienteFornitore": "SCOL1241-B"
	},
	{
		"ARTICOLO": "1242XXJ9",
		"CodArtClienteFornitore": "SCOL1241-WW"
	},
	{
		"ARTICOLO": "1242XXJS",
		"CodArtClienteFornitore": "SCOL1241-S"
	},
	{
		"ARTICOLO": "1242XXJW",
		"CodArtClienteFornitore": "SCOL1241-R9010"
	},
	{
		"ARTICOLO": "1242XXR0",
		"CodArtClienteFornitore": "SCOL1242-S"
	},
	{
		"ARTICOLO": "1242XXR1",
		"CodArtClienteFornitore": "SCOL1242-W"
	},
	{
		"ARTICOLO": "1242XXR2",
		"CodArtClienteFornitore": "SCOL1242-B"
	},
	{
		"ARTICOLO": "1242XXR7037",
		"CodArtClienteFornitore": "SCOL-1242-S"
	},
	{
		"ARTICOLO": "1242XXR9",
		"CodArtClienteFornitore": "SCOL1242-WW"
	},
	{
		"ARTICOLO": "1242XXRS",
		"CodArtClienteFornitore": "SCOL-1242-S"
	},
	{
		"ARTICOLO": "1242XXRW",
		"CodArtClienteFornitore": "SCOL1242-RAL9010"
	},
	{
		"ARTICOLO": "1291XXG0",
		"CodArtClienteFornitore": "HSCOL-1040-S"
	},
	{
		"ARTICOLO": "1291XXG1",
		"CodArtClienteFornitore": "HSCOL-1040-W"
	},
	{
		"ARTICOLO": "1291XXG2",
		"CodArtClienteFornitore": "HSCOL-1040-B"
	},
	{
		"ARTICOLO": "1291XXG9",
		"CodArtClienteFornitore": "HSCOL-1040-WW"
	},
	{
		"ARTICOLO": "1291XXJ0",
		"CodArtClienteFornitore": "HSCOL-1041-S"
	},
	{
		"ARTICOLO": "1291XXJ1",
		"CodArtClienteFornitore": "HSCOL-1041-W"
	},
	{
		"ARTICOLO": "1291XXJ2",
		"CodArtClienteFornitore": "HSCOL-1041-B"
	},
	{
		"ARTICOLO": "1291XXJ9",
		"CodArtClienteFornitore": "HSCOL-1041-WW"
	},
	{
		"ARTICOLO": "1291XXR0",
		"CodArtClienteFornitore": "HSCOL-1042-S"
	},
	{
		"ARTICOLO": "1291XXR1",
		"CodArtClienteFornitore": "HSCOL-1042-W"
	},
	{
		"ARTICOLO": "1291XXR2",
		"CodArtClienteFornitore": "HSCOL-1042-B"
	},
	{
		"ARTICOLO": "1291XXR9",
		"CodArtClienteFornitore": "HSCOL-1042-WW"
	},
	{
		"ARTICOLO": "1292XXG0",
		"CodArtClienteFornitore": "HSCOL-1240-S"
	},
	{
		"ARTICOLO": "1292XXG1",
		"CodArtClienteFornitore": "HSCOL-1240-W"
	},
	{
		"ARTICOLO": "1292XXG2",
		"CodArtClienteFornitore": "HSCOL-1240-B"
	},
	{
		"ARTICOLO": "1292XXG9",
		"CodArtClienteFornitore": "HSCOL-1240-WW"
	},
	{
		"ARTICOLO": "1292XXJ0",
		"CodArtClienteFornitore": "HSCOL-1241-S"
	},
	{
		"ARTICOLO": "1292XXJ1",
		"CodArtClienteFornitore": "HSCOL-1241-W"
	},
	{
		"ARTICOLO": "1292XXJ2",
		"CodArtClienteFornitore": "HSCOL-1241-B"
	},
	{
		"ARTICOLO": "1292XXJ9",
		"CodArtClienteFornitore": "HSCOL-1241-WW"
	},
	{
		"ARTICOLO": "1292XXR0",
		"CodArtClienteFornitore": "HSCOL-1242-S"
	},
	{
		"ARTICOLO": "1292XXR1",
		"CodArtClienteFornitore": "HSCOL-1242-W"
	},
	{
		"ARTICOLO": "1292XXR2",
		"CodArtClienteFornitore": "HSCOL-1242-B"
	},
	{
		"ARTICOLO": "1292XXR9",
		"CodArtClienteFornitore": "HSCOL-1242-WW"
	},
	{
		"ARTICOLO": "1321XXJ2",
		"CodArtClienteFornitore": "BASI-1041-B"
	},
	{
		"ARTICOLO": "1321XXR1",
		"CodArtClienteFornitore": "BASI-1042-W"
	},
	{
		"ARTICOLO": "1321XXR2",
		"CodArtClienteFornitore": "BASI-1042-B"
	},
	{
		"ARTICOLO": "1330XXN0",
		"CodArtClienteFornitore": "TCA-841-S"
	},
	{
		"ARTICOLO": "1330XXT2",
		"CodArtClienteFornitore": "TCA842-B"
	},
	{
		"ARTICOLO": "1331XXG0",
		"CodArtClienteFornitore": "TCA-840-S"
	},
	{
		"ARTICOLO": "1331XXG1",
		"CodArtClienteFornitore": "TCA-840-W"
	},
	{
		"ARTICOLO": "1331XXG2",
		"CodArtClienteFornitore": "TCA-1040-B"
	},
	{
		"ARTICOLO": "1331XXG9",
		"CodArtClienteFornitore": "TCA-840-WW"
	},
	{
		"ARTICOLO": "1331XXJ0",
		"CodArtClienteFornitore": "TCA-1041-S"
	},
	{
		"ARTICOLO": "1331XXJ1",
		"CodArtClienteFornitore": "TCA-1041-W"
	},
	{
		"ARTICOLO": "1331XXJ2",
		"CodArtClienteFornitore": "TCA-1041-B"
	},
	{
		"ARTICOLO": "1331XXJ9",
		"CodArtClienteFornitore": "TCA-1041-WW"
	},
	{
		"ARTICOLO": "1331XXR0",
		"CodArtClienteFornitore": "TCA-1042-S"
	},
	{
		"ARTICOLO": "1331XXR1",
		"CodArtClienteFornitore": "TCA-1042-W"
	},
	{
		"ARTICOLO": "1331XXR2",
		"CodArtClienteFornitore": "TCA-1042-B"
	},
	{
		"ARTICOLO": "1331XXR9",
		"CodArtClienteFornitore": "TCA-1042-WW"
	},
	{
		"ARTICOLO": "1332XXG0",
		"CodArtClienteFornitore": "TCA-1240-S"
	},
	{
		"ARTICOLO": "1332XXG1",
		"CodArtClienteFornitore": "TCA-1240-W"
	},
	{
		"ARTICOLO": "1332XXG2",
		"CodArtClienteFornitore": "TCA-1240-B"
	},
	{
		"ARTICOLO": "1332XXJ0",
		"CodArtClienteFornitore": "TCA-1241-S"
	},
	{
		"ARTICOLO": "1332XXJ1",
		"CodArtClienteFornitore": "TCA-1241-W"
	},
	{
		"ARTICOLO": "1332XXJ2",
		"CodArtClienteFornitore": "TCA-1241-B"
	},
	{
		"ARTICOLO": "1332XXR0",
		"CodArtClienteFornitore": "TCA-1242-S"
	},
	{
		"ARTICOLO": "1332XXR1",
		"CodArtClienteFornitore": "TCA-1242-W"
	},
	{
		"ARTICOLO": "1332XXR2",
		"CodArtClienteFornitore": "TCA-1242-B"
	},
	{
		"ARTICOLO": "1630XXJ1",
		"CodArtClienteFornitore": "SH1630-W"
	},
	{
		"ARTICOLO": "1630XXJ2",
		"CodArtClienteFornitore": "SH1630-B"
	},
	{
		"ARTICOLO": "1704XXE0",
		"CodArtClienteFornitore": "1LDP840-S"
	},
	{
		"ARTICOLO": "1704XXE1",
		"CodArtClienteFornitore": "1LDP840-W"
	},
	{
		"ARTICOLO": "1704XXE2",
		"CodArtClienteFornitore": "1LDP840-B"
	},
	{
		"ARTICOLO": "1704XXN0",
		"CodArtClienteFornitore": "1LDP841-S"
	},
	{
		"ARTICOLO": "1704XXN1",
		"CodArtClienteFornitore": "1LDP841-W"
	},
	{
		"ARTICOLO": "1704XXN2",
		"CodArtClienteFornitore": "1LDP841-B"
	},
	{
		"ARTICOLO": "1704XXR0",
		"CodArtClienteFornitore": "1LDP842-S"
	},
	{
		"ARTICOLO": "1704XXR1",
		"CodArtClienteFornitore": "1LDP842-W"
	},
	{
		"ARTICOLO": "1704XXR2",
		"CodArtClienteFornitore": "1LDP842-B"
	},
	{
		"ARTICOLO": "1704XXT0",
		"CodArtClienteFornitore": "1LDP842-S"
	},
	{
		"ARTICOLO": "1704XXT1",
		"CodArtClienteFornitore": "1LDP842-W"
	},
	{
		"ARTICOLO": "1704XXT2",
		"CodArtClienteFornitore": "1LDP842-B"
	},
	{
		"ARTICOLO": "1706XXE0",
		"CodArtClienteFornitore": "2LDP840-S"
	},
	{
		"ARTICOLO": "1706XXE1",
		"CodArtClienteFornitore": "2LDP840-W"
	},
	{
		"ARTICOLO": "1706XXE2",
		"CodArtClienteFornitore": "2LDP840-B"
	},
	{
		"ARTICOLO": "1706XXN0",
		"CodArtClienteFornitore": "2LDP841-S"
	},
	{
		"ARTICOLO": "1706XXN1",
		"CodArtClienteFornitore": "2LDP841-W"
	},
	{
		"ARTICOLO": "1706XXN2",
		"CodArtClienteFornitore": "2LDP841-B"
	},
	{
		"ARTICOLO": "1706XXR0",
		"CodArtClienteFornitore": "2LDP842-S"
	},
	{
		"ARTICOLO": "1706XXR1",
		"CodArtClienteFornitore": "2LDP842-W"
	},
	{
		"ARTICOLO": "1706XXR2",
		"CodArtClienteFornitore": "2LDP842-B"
	},
	{
		"ARTICOLO": "1706XXT0",
		"CodArtClienteFornitore": "2LDP842-S"
	},
	{
		"ARTICOLO": "1706XXT1",
		"CodArtClienteFornitore": "2LDP842-W"
	},
	{
		"ARTICOLO": "1706XXT2",
		"CodArtClienteFornitore": "2LDP842-B"
	},
	{
		"ARTICOLO": "1707XXE0",
		"CodArtClienteFornitore": "3LDP840-S"
	},
	{
		"ARTICOLO": "1707XXE1",
		"CodArtClienteFornitore": "3LDP840-W"
	},
	{
		"ARTICOLO": "1707XXE2",
		"CodArtClienteFornitore": "3LDP840-B"
	},
	{
		"ARTICOLO": "1707XXN0",
		"CodArtClienteFornitore": "3LDP841-S"
	},
	{
		"ARTICOLO": "1707XXN1",
		"CodArtClienteFornitore": "3LDP841-W"
	},
	{
		"ARTICOLO": "1707XXN2",
		"CodArtClienteFornitore": "3LDP841-B"
	},
	{
		"ARTICOLO": "1707XXR0",
		"CodArtClienteFornitore": "3LDP842-S"
	},
	{
		"ARTICOLO": "1707XXR2",
		"CodArtClienteFornitore": "3LDP842-B"
	},
	{
		"ARTICOLO": "1707XXT0",
		"CodArtClienteFornitore": "3LDP842-S"
	},
	{
		"ARTICOLO": "1707XXT1",
		"CodArtClienteFornitore": "3LDP842-W"
	},
	{
		"ARTICOLO": "1707XXT2",
		"CodArtClienteFornitore": "3LDP842-B"
	},
	{
		"ARTICOLO": "2660XXX1",
		"CodArtClienteFornitore": "025-660-W"
	},
	{
		"ARTICOLO": "2770XXX1",
		"CodArtClienteFornitore": "02-770-W"
	},
	{
		"ARTICOLO": "2880XXX1",
		"CodArtClienteFornitore": "02-880-W"
	},
	{
		"ARTICOLO": "5010874MLED4K1",
		"CodArtClienteFornitore": "MINIR9101-W"
	},
	{
		"ARTICOLO": "5010875MLED3K1",
		"CodArtClienteFornitore": "MINIS9101-S"
	},
	{
		"ARTICOLO": "5010875MLED4K1",
		"CodArtClienteFornitore": "MINIS9101-W"
	},
	{
		"ARTICOLO": "5010876MLED4K1",
		"CodArtClienteFornitore": "MINIS9000-W"
	},
	{
		"ARTICOLO": "50112421X4001",
		"CodArtClienteFornitore": "SCON1220-W"
	},
	{
		"ARTICOLO": "50112421X4002",
		"CodArtClienteFornitore": "SCON-1220-B"
	},
	{
		"ARTICOLO": "5011242AFLOOD1",
		"CodArtClienteFornitore": "IO2 1252-WW"
	},
	{
		"ARTICOLO": "50112721X4001",
		"CodArtClienteFornitore": "TCH1250-W"
	},
	{
		"ARTICOLO": "50112721X4002",
		"CodArtClienteFornitore": "TCH1250-B"
	},
	{
		"ARTICOLO": "5019600A11000",
		"CodArtClienteFornitore": "PC-204-S"
	},
	{
		"ARTICOLO": "9331XXJW",
		"CodArtClienteFornitore": "TCA-1041-W-SP"
	},
	{
		"ARTICOLO": "9331XXR0",
		"CodArtClienteFornitore": "TCA-1042-S-SP"
	},
	{
		"ARTICOLO": "9331XXR2",
		"CodArtClienteFornitore": "TCA-1042-B-SP"
	},
	{
		"ARTICOLO": "MOON1200",
		"CodArtClienteFornitore": "ROV1210-W"
	},
	{
		"ARTICOLO": "MOON600",
		"CodArtClienteFornitore": "ROV610-W"
	},
	{
		"ARTICOLO": "MOON900",
		"CodArtClienteFornitore": "ROV910-W"
	},
	{
		"ARTICOLO": "P230XXJ1",
		"CodArtClienteFornitore": "SH1230 CEILING MOUNT"
	},
	{
		"ARTICOLO": "P630XXJ1",
		"CodArtClienteFornitore": "SH1630 CEILING MOUNT"
	},
	{
		"ARTICOLO": "RING1200",
		"CodArtClienteFornitore": "RSD1200-W"
	},
	{
		"ARTICOLO": "RING600",
		"CodArtClienteFornitore": "RSD600-W"
	},
	{
		"ARTICOLO": "RING900",
		"CodArtClienteFornitore": "RSD900-W"
	},
	{
		"ARTICOLO": "5010694111002",
		"CodArtClienteFornitore": "BS3297XL-A(B)-I6"
	},
	{
		"ARTICOLO": "5010695111002",
		"CodArtClienteFornitore": "BS3297FXL-A(B)-I6"
	},
	{
		"ARTICOLO": "RB-2203000120",
		"CodArtClienteFornitore": "RB3528WW120G200D"
	},
	{
		"ARTICOLO": "RB-2204000120",
		"CodArtClienteFornitore": "RB3528W120G200D"
	},
	{
		"ARTICOLO": "RB-HVCLIP",
		"CodArtClienteFornitore": "RB-HV-CLIP"
	},
	{
		"ARTICOLO": "RB-HVCN2",
		"CodArtClienteFornitore": "RB-HV-CON2P"
	},
	{
		"ARTICOLO": "RB-HVPC",
		"CodArtClienteFornitore": "RB-HV-PC50"
	},
	{
		"ARTICOLO": "SL3000",
		"CodArtClienteFornitore": "RB2835WW196G24 5mt"
	},
	{
		"ARTICOLO": "SL3000M30",
		"CodArtClienteFornitore": "RB2835WW196G24 30 mt"
	},
	{
		"ARTICOLO": "0693XXX1",
		"CodArtClienteFornitore": "BS3198L(W)-I6"
	},
	{
		"ARTICOLO": "0694XXX1",
		"CodArtClienteFornitore": "BS3297XL-A(W)-I6"
	},
	{
		"ARTICOLO": "0695XXX1",
		"CodArtClienteFornitore": "BS3297FXL-A"
	},
	{
		"ARTICOLO": "1712XXX1",
		"CodArtClienteFornitore": "XM2478-1(QR111)"
	},
	{
		"ARTICOLO": "1713XXX1",
		"CodArtClienteFornitore": "1713XXX1"
	},
	{
		"ARTICOLO": "1714XXX1",
		"CodArtClienteFornitore": "XM2478-3"
	},
	{
		"ARTICOLO": "1808XXX0",
		"CodArtClienteFornitore": "XM-2478-1"
	},
	{
		"ARTICOLO": "1808XXX1",
		"CodArtClienteFornitore": "XM-2478-1"
	},
	{
		"ARTICOLO": "1809XXX0",
		"CodArtClienteFornitore": "XM-2478-2"
	},
	{
		"ARTICOLO": "1809XXX1",
		"CodArtClienteFornitore": "XM2478-2(QR111)"
	},
	{
		"ARTICOLO": "26662CZ0",
		"CodArtClienteFornitore": "LXM2666M(SG)-3000K"
	},
	{
		"ARTICOLO": "26662CZ1",
		"CodArtClienteFornitore": ""
	},
	{
		"ARTICOLO": "26663CZ0",
		"CodArtClienteFornitore": "LXM2666M(SG)-4000K"
	},
	{
		"ARTICOLO": "26663CZ1",
		"CodArtClienteFornitore": ""
	},
	{
		"ARTICOLO": "2916XXX1",
		"CodArtClienteFornitore": "XM2916(W)"
	},
	{
		"ARTICOLO": "2917XXX1",
		"CodArtClienteFornitore": "XM2917-1(W)"
	},
	{
		"ARTICOLO": "3209XXX0",
		"CodArtClienteFornitore": "BS3209W(SG)"
	},
	{
		"ARTICOLO": "3209XXX1",
		"CodArtClienteFornitore": "BS3209W(W)"
	},
	{
		"ARTICOLO": "3209XXX2",
		"CodArtClienteFornitore": "BS3209W(B)"
	},
	{
		"ARTICOLO": "3210XXX0",
		"CodArtClienteFornitore": "BS3209WF(SG)"
	},
	{
		"ARTICOLO": "3210XXX1",
		"CodArtClienteFornitore": "BS3209WF(W)"
	},
	{
		"ARTICOLO": "3210XXX2",
		"CodArtClienteFornitore": "BS3209WF(B)"
	},
	{
		"ARTICOLO": "3265XXX0",
		"CodArtClienteFornitore": "BS3265CA(SG)"
	},
	{
		"ARTICOLO": "3265XXX1",
		"CodArtClienteFornitore": "BS3265CA(W)"
	},
	{
		"ARTICOLO": "3265XXX2",
		"CodArtClienteFornitore": "3265CA(B)"
	},
	{
		"ARTICOLO": "3266XXX0",
		"CodArtClienteFornitore": "BS3265CF(SG)"
	},
	{
		"ARTICOLO": "3266XXX1",
		"CodArtClienteFornitore": "BS3265CF(W)"
	},
	{
		"ARTICOLO": "3266XXX2",
		"CodArtClienteFornitore": "BS3265CF(B)"
	},
	{
		"ARTICOLO": "5010620113001",
		"CodArtClienteFornitore": "XM2489-Q(W)I6"
	},
	{
		"ARTICOLO": "5010692111001",
		"CodArtClienteFornitore": "BS3198FL(W)-I6"
	},
	{
		"ARTICOLO": "5011888113000",
		"CodArtClienteFornitore": "XM2488-Q(SG)-I6"
	},
	{
		"ARTICOLO": "5011888L00000",
		"CodArtClienteFornitore": "XM2488-L-I6"
	},
	{
		"ARTICOLO": "5011888M00000",
		"CodArtClienteFornitore": "XM2488-B-I6"
	},
	{
		"ARTICOLO": "5011888S00000",
		"CodArtClienteFornitore": "XM2488(SG)-EMPTY-I6"
	},
	{
		"ARTICOLO": "501B835ST-GU10",
		"CodArtClienteFornitore": "DISTANCE BAR NO.7-I6"
	},
	{
		"ARTICOLO": "B835XXX2",
		"CodArtClienteFornitore": "BS3215(B)I6"
	},
	{
		"ARTICOLO": "01201760032A",
		"CodArtClienteFornitore": "01201760032A"
	}
]
 

            var bulk = Prodotto.collection.initializeOrderedBulkOp();

            async.each(prodotti, 
                function (p, cb) { 

                    var codiceP = p.ARTICOLO,
                        codicePFornitore = p.CodArtClienteFornitore;
                  
                    Prodotto.findOne(
                        {codice:codiceP}, 
                        function (err, prod) {
                            if (err){
                                throw err;
                            }
                            else{

                                count++;

                                if (prod){

                                    var update = {};
                                    update.$set = {};
                                    update.$set.codice_prodotto_fornitore=codicePFornitore;
                                    update.$set.mdate = new Date();

                                    bulk.find( { codice: codiceP } ).updateOne(update); 

                                    console.log(count, " Codice modificato: ", codiceP, " codice fornitore ", codicePFornitore)

                                    aggiornati++;
                                    
                                }
                                else
                                {
                                    console.log(count, "Codice NON TROVATO in back_End: ", codiceP)
                                     
                                }

                                 cb();
                                 
                            }
                        }
                    ).lean() 
                     
                }, 
                function (err) 
                { 

                    if(aggiornati > 0 ) {
                         bulk.execute(
                            function(err, result){
                            if(err)
                                throw err;

                            console.log('finito, chiudo la connessione, creati ' + aggiornati + ' nuovi codici')
                            mongoose.connection.close();

                            }
                        )
                    }
                    else {
                        mongoose.connection.close();
                    }
                }
            )
        })