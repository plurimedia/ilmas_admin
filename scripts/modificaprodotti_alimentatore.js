/*
ISTRUZIONI

ESPORTARE I DATI DAL DB DI PRODUZIONE
    mongodump -h ds057505-a0.mongolab.com:57505 -d heroku_rlzkx1m6 -c Prodotto -u plurimedia -p Canonico27! -o  /Users/Amedeo/backups/ilmas_admin
    mongodump -h ds057505-a0.mongolab.com:57505 -d heroku_rlzkx1m6 -c Modello -u plurimedia -p Canonico27! -o  /Users/Amedeo/backups/ilmas_admin
    mongodump -h ds057505-a0.mongolab.com:57505 -d heroku_rlzkx1m6 -c Categoria -u plurimedia -p Canonico27! -o  /Users/Amedeo/backups/ilmas_admin


mongo ds057505-a1.mongolab.com:57505/heroku_rlzkx1m6 -u plurimedia -p Canonico27!


CANCELLARE TUTTI I DATI IN LOCALE
    db.Categoria.remove({})
    db.Modello.remove({})
    db.Prodotto.remove({})

     > db.Categoria.remove({})
    WriteResult({ "nRemoved" : 17 })
    > db.Modello.remove({})
    WriteResult({ "nRemoved" : 358 })
    > db.Prodotto.remove({})
    WriteResult({ "nRemoved" : 139786 })



IMPORTARE I DATI DAI FILE CREATI

mongorestore -h localhost:27017 -d ilmas_admin -c Prodotto  /Users/Amedeo/backups/ilmas_admin_produzione/heroku_rlzkx1m6/Prodotto.bson
mongorestore -h localhost:27017 -d ilmas_admin -c Modello  /Users/Amedeo/backups/ilmas_admin_produzione/heroku_rlzkx1m6/Modello.bson
mongorestore -h localhost:27017 -d ilmas_admin -c Categoria  /Users/Amedeo/backups/ilmas_admin_produzione/heroku_rlzkx1m6/Categoria.bson


LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/modificaprodotti_alimentatore;


RICREARE IL FILE
    mongodump -h localhost:27017 -d ilmas_admin -c Prodotto    -o  /Users/Amedeo/backups/ilmas_admin_locale2
    mongodump -h localhost:27017 -d ilmas_admin -c Modello     -o  /Users/Amedeo/backups/ilmas_admin_locale2
    mongodump -h localhost:27017 -d ilmas_admin -c Categoria   -o  /Users/Amedeo/backups/ilmas_admin_locale2


CANCELLARE TUTTI I DATI IN PRODUZIONE
    db.Categoria.remove({})
    db.Modello.remove({})
    db.Prodotto.remove({})

IMPORTARE I DATI CREATI IN LOCALE SUL DB DI PRODUZIONE

    mongorestore -h ds057505-a0.mongolab.com:57505 -d heroku_rlzkx1m6 -c Categoria -u plurimedia -p Canonico27! --batchSize=1000 /Users/Amedeo/backups/ilmas_admin_locale2/ilmas_admin/Categoria.bson
    mongorestore -h ds057505-a0.mongolab.com:57505 -d heroku_rlzkx1m6 -c Modello -u plurimedia -p Canonico27! --batchSize=1000 /Users/Amedeo/backups/ilmas_admin_locale2/ilmas_admin/Modello.bson
    mongorestore -h ds057505-a0.mongolab.com:57505 -d heroku_rlzkx1m6 -c Prodotto -u plurimedia -p Canonico27! --batchSize=1000 /Users/Amedeo/backups/ilmas_admin_locale2/ilmas_admin/Prodotto.bson


*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ModelloSchema = require('../server/models/modello'),
    Modello = mongoose.model('Modello'),
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    Prodotto.find({}).lean()
    .populate('modello','nome')
    .exec(function(err,prodotti){

        console.log('aggiorno '+prodotti.length+' prodotti')

        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, function(prodotto,cb){

            var update = {};

            //Forzare alimentatore incluso in tutti questi modelli
            //over spring summer orion fold tek bell dial1 nuvola star led

            //tek ??

            if( (prodotto.modello) && (isModelloToRefactor(prodotto.modello.nome)==true) )
            {
                if ( (!prodotto.alimentatore_incluso) || prodotto.alimentatore_incluso==false)
                {
                    console.log("aggiornamento prodotto id = ", prodotto._id);

                    update.$set = {};
                    update.$set.alimentatore_incluso = true;
                    bulk.find( { _id: prodotto._id } ).updateOne(update);
                    aggiornati ++;
                }
            }

            cb();
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0) {
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                    mongoose.connection.close();

                })
            }
            else {
                console.log('Non ci sono prodotti da aggiornare')
                mongoose.connection.close();
            }

        })
    })

    function isModelloToRefactor(modello){
        if (
            modello.match(/OVER/gi)     ||
            modello.match(/Spring/gi)   ||
            modello.match(/summer/gi)   ||
            modello.match(/orion/gi)    ||
            modello.match(/fold/gi)     ||
            modello.match(/tek/gi)      ||
            modello.match(/bell/gi)     ||
            modello.match(/dial 1/gi)   ||
            modello.match(/nuvola/gi)   ||
            modello.match(/star led/gi)
           ){
             console.log("modello To REFACTOR:", modello);
            return true;
        }
        else{
            return false;
        }
    }
}



);
