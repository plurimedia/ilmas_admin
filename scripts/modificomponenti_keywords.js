/*
LANCIARE LA PROCEDURA
node --max-old-space-size=20000 scripts/modificomponenti_keywords;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ComponenteSchema = require('../server/models/componente'),
    Componente = mongoose.model('Componente'),
    aggiornati = 0;

    console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    Componente.find().lean() 
    .exec(function(err,componenti){

        console.log('aggiorno '+componenti.length+' componenti')

        var bulk = Componente.collection.initializeOrderedBulkOp();
        async.each(componenti, function(componente,cb){

            var update = {}; 
             
            if (componente.codice){
            var p = _.clone(componente);

                if (p.nome && p.codice !== '' && p.codice !== null){
               
                update.$set = {};
                update.$set.keywords = _keyWords(p);
                bulk.find( { _id: componente._id } ).updateOne(update);
                aggiornati ++; 

                }
            }
            cb();
        }, function(err){

            if(err)
                throw err;

            if(aggiornati > 0) {
             
                bulk.execute(function(err, result){
                    if(err)
                        throw err;

                    console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' componenti')
                    mongoose.connection.close();

                })
               
            }
            else {
                console.log('Non ci sono componenti da aggiornare')
                mongoose.connection.close();
            }

        })
    })

     _keyWords = function (p) {

         var result = [p.codice,p.codice.toLowerCase()], // metti codice e codice lowercase
            _string = function (stringa) { // tutte le parole di una stringa senza il trattino, upper e lower
 
                var keywords = _.filter(stringa.split(' '),function(k){return k!=='-'});
                return [
                        keywords,
                        _.collect(keywords,function(k){return k.toLowerCase()}),
                        _.collect(keywords,function(k){return k.toUpperCase()})
                ]
            },
            _value = function (v,u){
                return [v+u,v+u.toUpperCase(),v+u.substr(0,1).toUpperCase()+u.substr(1)];
            },
           _keywordsCodice = function (codice) { // metto anche il codice in pezzi (ricerca per parti di codice)
            var tmp = [];
            if (codice.length>3){ 
                for (i=3;i<=codice.length;i++){
                    tmp.push(codice.substring(0,i)) 
                } 
            }
            else{
                tmp.push(codice)
            }
            
            return _.compact(tmp);
        };
 
        if (p.nome)
        result.push(_string(p.nome));

     
        result.push(_keywordsCodice(p.codice.toUpperCase()))

        result.push(_keywordsCodice(p.codice.toLowerCase()))

        result.push(p.tipo.toLowerCase())

        result.push(p.tipo.toUpperCase())

        return _.unique(_.flatten(result));
    };
}



);
