/*
LANCIARE LA PROCEDURA
node --max-old-space-size=5000 scripts/componenti_china1
*/

var MongoClient = require('mongodb').MongoClient, aggiornati = 0,
    CONFIG = require('../server/config.js'),
    ObjectID = require('mongodb').ObjectID,
    async = require('async');

async function connect()
{
    try {
        let dbc= await MongoClient.connect(CONFIG.MONGO);
        return dbc;
    } catch (e) {
        console.log("Connect error", e);
        throw err;
    }
}

async function getProdotti(db)
{
    try {
        let a = await db.collection("Prodotto").find({ 'distintabase': 'china_1' }, { componenti: 1 }).toArray();
        return a;
    } catch(e) {
        console.log('Errore getProdotti', e);
    }
}

async function process()
{
   let db= await connect();

   let p= await getProdotti(db);

   var bulk= db.collection("Prodotto").initializeUnorderedBulkOp();

   for (let prodotto of p)
   {
       var modifica= false, update;
console.log('elaborazione prodotto', prodotto._id);

       if (prodotto.componenti && prodotto.componenti.length)
       {
           for (let c of prodotto.componenti)
           {
               //if (!c.tipoAssociazione)
               //{
console.log('elaborazione componente', c._id);
                   var comp= await db.collection("Componente").findOne({ '_id': ObjectID(c._id) }, { tipo: 1 });
                   if (comp && comp.tipo)
                   {
                       modifica= true;
                       if (comp.tipo==='led')
                          c.tipoAssociazione= 'Led';
                       else if (comp.tipo==='driver')
                          c.tipoAssociazione= 'Driver';
                       else
                          c.tipoAssociazione= 'Corpo';
                   }
               //}
           };

           if (modifica)
           {
               bulk.find({ _id: prodotto._id }).updateOne({ $set: { componenti: prodotto.componenti } });
               aggiornati++;
           }
       }
   };

   if (aggiornati > 0)
   {
       var r= await bulk.execute();
       console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti');
       db.close();
   }
   return;
}

process();
