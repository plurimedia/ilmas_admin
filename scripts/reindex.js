/*
    reindicizza il db prodotti con nuove caratteristiche di ricerca
    lanciare con: node --max-old-space-size=2000 scripts/reindex;
*/


var mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = 'mongodb://localhost/ilmas_admin',
    ModelloSchema = require('../server/models/modello'),
    Modello = mongoose.model('Modello'),
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    /* crea le parole chiave per la ricerca */
    _searchKeywords = function (p) {

        var result = [p.codice,p.codice.toLowerCase()], // metti codice e codice lowercase
            _string = function (s) { // tutte le parole di una stringa senza il trattino, upper e lower
                var keywords = _.filter(s.split(' '),function(k){return k!=='-'});
                return [
                        keywords, 
                        _.collect(keywords,function(k){return k.toLowerCase()}),
                        _.collect(keywords,function(k){return k.toUpperCase()})
                ]
            },
            _value = function (v,u){
                return [v+u,v+u.toUpperCase(),v+u.substr(0,1).toUpperCase()+u.substr(1)];
            },
            _keywordsCodice = function (codice) { // metto anche il codice in pezzi (ricerca per parti di codice)
                /*
                    1241NCR9 => 1241,1241N, 1241NC, 1241NCR, 1241NCR9
                */

                if(codice.indexOf('KIT')===-1) {
                  var c1 = p.codice.substr(0,3),
                    c2 = p.codice.substr(0,4),
                    c3 = p.codice.substr(0,5),
                    c4 = p.codice.substr(0,6);
                    c5 = p.codice.substr(0,7);
                    c6 = p.codice.substr(0,8);

                    return _.compact([c1,c2,c3,c4,c5,c6]);                
                }

                // esterno

                if(codice.indexOf('599')!==-1) {
                    return p.codice.split('.');
                }

                /* KIT1241L0 => KIT,KIT1241,KIT1241L */
                else {
                   var c1 = p.codice.substr(0,3),
                    c2 = p.codice.substr(0,7),
                    c3 = p.codice.substr(0,8),
                    c4 = p.codice.substr(0,9);

                    return _.compact([c1,c2,c3,c4]);                     
                }

            };
        
        if(p.linea)
            result.push(p.linea,p.linea.toUpperCase());
        if(p.sottocategoria)
            result.push(p.sottocategoria,p.sottocategoria.toUpperCase(),p.sottocategoria.toLowerCase());
        if(p.modello)
            result.push(_string(p.modello.nome));
        if(p.k)
            result.push(_value(p.k,'k'));
        if(p.lm)
           result.push(_value(p.lm,'lm'));
        if(p.w)
           result.push(_value(p.w,'w'));
        if(p.ma)
           result.push(_value(p.ma,'ma'));
        if(p.fascio)
            result.push(p.fascio.indexOf('°')!==-1 ? p.fascio : p.fascio+'°')
        if(p.v)
            result.push(_value(p.v,'v'));
        if(p.hz)
            result.push(_value(p.hz,'hz'));
        if(p.irc)
            result.push(p.irc,_value(p.irc,'irc'));
        if(p.alimentatore)
            result.push(_string(p.alimentatore));
        if(p.colore)
            result.push(_string(p.colore));

        result.push(_keywordsCodice(p.codice))
        
        
        return _.unique(_.flatten(result));
    };

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console

    Prodotto.find({}).lean()
    .populate('modello','nome')
    .exec(function(err,prodotti){
        
        console.log('aggiorno '+prodotti.length+' prodotti')
        
        var bulk = Prodotto.collection.initializeOrderedBulkOp();
        async.each(prodotti, function(prodotto,cb){
            
            var update = { $set: { keywords: _searchKeywords(prodotto)}};
            bulk.find( { _id: prodotto._id } ).updateOne(update);
            cb();
        }, function(err){
            if(err)
                throw err;
            
            bulk.execute(function(err, result){
                if(err)
                    throw err;
                
                console.log('finito, chiudo la connessione')
                mongoose.connection.close();                
                    
            })
        })
    })
});

//mongoose.connection.close();