/*
LANCIARE LA PROCEDURA
node scripts/elencoEan;
*/

let CONFIG = require(process.cwd() + '/server/config.js'),
    async = require('async'),
    conn = CONFIG.MONGO;

const excel = require('exceljs')
const MongoClient = require('mongodb').MongoClient
const ObjectId = require('mongodb').ObjectID
const options = { sort: { codice: 1 }, projection: { _id: 0, codice: 1, codiceEan: 1 } };
console.log(conn)
run().catch(error => console.error(error))

let client, db, modelli;
let filtro = [
'1240 Over Led Ps Mini',
'1241 Over Led Ps Midi',
'1242 Over Led Ps Maxi',
'1320 Viper Mini',
'1321 Viper Midi',
'1322 Viper Maxi',
'2341 C/TRACK M',
'2342 C/TRACK L',
'1090 J-TRACK XS',
'0595 Smart XS',
'0596 Smart S',
'0597 Smart M',
'0598 Smart L',
'0599 SMART XL',
'0796 Hammer Mini 120',
'0797 Hammer Midi 160',
'0798 Hammer Maxi 205',
'0799 Hammer Extra 230',
'0585 Smart Q XS',
'0586 Smart Q S',
'0587 Smart Q M',
'0588 Smart Q L',
'0589 Smart Q XL',
'0786 Hammer Q Mini 120',
'0787 Hammer Q Midi 160',
'0788 Hammer Q Maxi 205',
'0789 Hammer Q Extra 230',
'0328 Moon /SC 280',
'0338 Moon /SC 380',
'0360 Moon /SC 600',
'0228 Moon 280',
'0238 Moon 380',
'0260 Moon 600',
'4028 Moon 280',
'4038 Moon/P 380',
'4060 Moon 600',
'4090 Moon 900',
'4099 Moon 1200',
'0670 Astuto Led Mini',
'0671 Astuto Led Midi',
'0672 Astuto Led Maxi',
'0526 Smart A/ S',
'0527 Smart A/ M',
'0528 Smart A/L',
'0660 Dino Led Mini',
'0661 Dino Led Midi',
'0662 Dino Led Maxi',
'0730 Berin Led Mini',
'0731 Berin Led Midi',
'0732 Berin Led Maxi',
'0516 Smart B/ S',
'0517 Smart B/M',
'0518 SMART B/L',
'0900 Sun S',
'1230 Sun M',
'1233 Sun/Track L',
'1630 Sun L',
'4830 Sun/P M',
'4850 Sun/P L',
'1106 Ring 600',
'1109 Ring 900',
'1112 Ring 1200',
'1160 Square 600',
'1190 Square 900'
];

async function asyncForEach(array, callback)
{
   for (let index = 0; index < array.length; index++)
   {
      console.log('array[index]', array[index])
      await callback(array[index], index, array);
   }
}

let elabora = async function()
{
   let ris=[];

   for (const m of modelli)
   {
      let prodotti = await db.collection('Prodotto').find({ codiceEan: { $exists: true, $ne: null }, modello: ObjectId(m._id) }, options).toArray()
      for (const p of prodotti)
      {
         ris.push({ modello: m.nome, prodotto: p.codice, codiceEan: p.codiceEan })
         if (ris.length < 5)
            console.log(ris[ris.length-1]);
      }
   }

   return ris;
}

async function run()
{
   let risultati=[];
   client = await MongoClient.connect(conn, { useUnifiedTopology: true });
   db = await client.db();
   modelli = await db.collection('Modello').find({ nome: { $in: filtro } }, { nome: 1 }).toArray()

   risultati = await elabora()

   let workbook = new excel.Workbook();
   let worksheet = workbook.addWorksheet("Codici Ean");

   worksheet.columns = [
     { header: "Modello", key: "modello", width: 30 },
     { header: "Prodotto", key: "prodotto", width: 20 },
     { header: "Codice Ean", key: "codiceEan", width: 20 },
   ];

   console.log('worksheet.addRows', risultati.length)
   worksheet.addRows(risultati);

   await workbook.xlsx.writeFile('codici_ean.xlsx');
   console.log('fine')
}
