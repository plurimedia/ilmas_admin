/*
 LANCIARE LA PROCEDURA
 node --max-old-space-size=10000 scripts/_668_correzione_modelloLed;
 */


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    fs      = require('fs'),
    aggiornati = 0,
    parsati = 0,
    logGlobale="";

var transform = {
    "AR111": "AR111 Led Module",
    "F111 Led Module": undefined,
    "F112 Led Module": undefined,
    "F050 Led Module": undefined,
    "V1 Led Module": undefined,
    "V2 Led Module": undefined,
    "Arled Osram": undefined,
    "V1 Plus Led Module": undefined,
    "" : undefined,
    "V1 PLUS Led Module": "V1 Plus Led Module",
    "F050 Led Module ": "F050 Led Module",
    "F111": "F111 Led Module",
    "ARLED": "Arled Osram",
    "OSRAM": "Arled Osram",
    "DILED": "Diled Osram",
    "V2": "V2 Led Module",
    "F050": "F050 Led Module",
    "F112": "F112 Led Module",
    "V1 PLUS": "V1 Plus Led Module"
}


console.log(db)

mongoose.connect(db);

mongoose.connection.once('open', function() {

        console.log("Connessione aperta, parte il batch."); /// this gets printed to console

        var query = {"moduloled":{$exists:true}}

        Prodotto.find(query).lean()
            //.skip(30000)
            //.limit(10000)
            .exec(
            function(err,prodotti){


                console.log(prodotti.length);

                var bulk = Prodotto.collection.initializeOrderedBulkOp();
                async.each(prodotti,
                    function(prodotto,cb)
                    {
                        parsati++;

                        var p = _.clone(prodotto);

                        if (p.moduloled)
                            console.log(p.moduloled, "**"+transform[p.moduloled]+"**");

                        if (p.moduloled && transform[p.moduloled]) {
                            var l = transform[p.moduloled];
                            console.log(p.moduloled, l);

                            //p.moduloled = l;

                            var logStr = _.clone(aggiornati) +  " codice " + " " +  p.codice + " " +  l;

                            var update = {};
                            update.$set = {};
                            update.$set.moduloled = l;
                            bulk.find( { _id: p._id } ).updateOne(update);

                            aggiornati ++;

                            console.log(logStr);

                            logGlobale = logGlobale + logStr + "\n";
                        }

                        cb();
                    },
                    function(err)
                    {
                        if(err){
                            console.log(err)
                            throw err;
                        }

                        if(aggiornati > 0) {

                            bulk.execute(function(err, result){
                                if(err)
                                    throw err;

                                console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                                mongoose.connection.close();

                                fs.writeFile(".tmp/_668_correzione_modelloLed.txt",

                                    logGlobale,

                                    function(err) {
                                        if(err) {
                                            console.log("Log was not saved!", err);
                                            throw err;
                                        }
                                        else
                                            console.log("Log was saved!");
                                    }
                                );

                            })
                            console.log('finito, chiudo la connessione: parsati '+parsati+' aggiornati '+aggiornati+' prodotti')

                        }
                        else {
                            console.log('Non ci sono prodotti da aggiornare parsati '+parsati)
                            mongoose.connection.close();
                        }
                    }
                )
            })


    }
);
