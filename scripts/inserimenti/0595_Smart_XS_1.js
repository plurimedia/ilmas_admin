var process = require('./massive.js');
var modello = '59afed5ad9bffb040079b4d1';
var query = { modello : modello };
var tag = 'dani_20170914_04';

var transform1 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["W"], pos:5},
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'Y', 5);

                p.lm = '1360';
                p.les = 'LES10';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

 A partire dai codici del modello con 5° carattere =W modificare i seguenti campi:
 codice che diventa all’quinto  carattere = Y
 flusso lum. Nom.  =1360
 LES: LES10
 */