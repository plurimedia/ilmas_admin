var process = require('./massive.js');
var modello = '59b00ad6d9bffb040079b531';
var query = { modello : modello };
var tag = 'dani_20180702_08';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.ugr = '<19';
                p.ottica_it = 'Riflettore in policarbonato metallizzato';
                p.ottica_en = 'Metallized polycarbonate reflector';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["G"], pos:7},
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'E', 7);
                p.fascio = '20';

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:7},
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'N', 7);
                p.fascio = '40';

                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["L"], pos:7},
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'S', 7);
                p.fascio = '55';

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                switch (p.prezzo) {
                    case 165 :
                        p.prezzo = 157;
                        break;
                    case 192 :
                        p.prezzo = 157;
                        break;
                    case 185 :
                        p.prezzo = 177;
                        break;
                    case 212 :
                        p.prezzo = 177;
                        break;
                    case 200 :
                        p.prezzo = 192;
                        break;
                    case 227 :
                        p.prezzo = 192;
                        break;
                    case 172 :
                        p.prezzo = 157;
                        break;
                    case 192 :
                        p.prezzo = 177;
                        break;
                    case 207 :
                        p.prezzo = 192;
                        break;
                    case 144 :
                        p.prezzo = 134;
                        break;
                    case 164 :
                        p.prezzo = 154;
                        break;
                    case 179 :
                        p.prezzo = 169;
                        break;
                }

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

 A partire dai codici del modello modificare i seguenti campi:
 Costruzione ottica (IT): Riflettore in policarbonato metallizzato
 Costruzione ottica (EN):  Metallized polycarbonate reflector
 UGR: <19
 2. A partire da tutti i codici del modello con 7° carattere = G modificare i  record per i seguenti campi:
 7° carattere G  E
 Fascio: 20
 3. A partire da tutti i codici del modello con 7° carattere = R modificare i  record per i seguenti campi:
 7° carattere R  T
 Fascio: 60
 4. A partire da tutti i codici del modello modificare il campo prezzo con la seguente regola:
 165157
 192  157
 185177
 212177
 200192
 227192
 172 157
 192177
 207192

 */