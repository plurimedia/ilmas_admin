var process = require('./massive.js');
var modello = '59aff297d9bffb040079b4e4';
var query = { modello : modello };
var tag = 'dani_20180702_06';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.ugr = '<19';
                p.ottica_it = 'Riflettore in policarbonato metallizzato';
                p.ottica_en = 'Metallized polycarbonate reflector';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["K"], pos:7},
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'S', 7);
                p.fascio = '55';

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                switch (p.prezzo) {
                    case 145 :
                        p.prezzo = 137;
                        break;
                    case 165 :
                        p.prezzo = 157;
                        break;
                    case 180 :
                        p.prezzo = 172;
                        break;
                    case 152 :
                        p.prezzo = 137;
                        break;
                    case 172 :
                        p.prezzo = 157;
                        break;
                    case 187 :
                        p.prezzo = 172;
                        break;
                }

                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

 A partire dai codici del modello modificare i seguenti campi:
 Costruzione ottica (IT): Riflettore in policarbonato metallizzato
 Costruzione ottica (EN):  Metallized polycarbonate reflector
 UGR: <19
 2. A partire da tutti i codici del modello con 7° carattere = G modificare i  record per i seguenti campi:
 7° carattere G  E
 Fascio: 20
 3. A partire da tutti i codici del modello con 7° carattere = R modificare i  record per i seguenti campi:
 7° carattere R  T
 Fascio: 60
 4. A partire da tutti i codici del modello modificare il campo prezzo con la seguente regola:
 145137
 165157
 180172
 152 137
 172157
 187172
 */