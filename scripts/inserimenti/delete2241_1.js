/* 
    lanciare con: 
	node --max-old-space-size=2000 scripts/inserimenti/delete1006_1.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    CategoriaSchema = require(process.cwd() + '/server/models/categoria'),
    ModelloSchema = require(process.cwd() + '/server/models/modello'),
    ProdottoSchema = require(process.cwd() + '/server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    ObjectId = require('mongodb').ObjectID,
	aggiornati = 0;

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console
    console.log(db)

    var query =
            { 
	            modello:ObjectId('5a65b145e23c1c0400bd7e9f')
            }

    console.log("query", query);

    Prodotto.find(query).lean()
    .exec(

    	function(err,prodotti){
        
            console.log('lunghezza '+prodotti.length+' prodotti')
        
  		    mongoose.connection.close();

  		    console.log('connection close') 

    		//cancelliamo tutti i record tranne quelli elencati
    		prodotti = _.filter(prodotti, function(p){
    			return (p.codice != '2241RBE1' &&
                        p.codice != '2241RBE1L' &&
                        p.codice != '22415BE1' &&
                        p.codice != '22415BE1L' &&
                        p.codice != '22416BE1' &&
                        p.codice != '22416BE1L' &&
                        p.codice != '2241ZBE1' &&
                        p.codice != '2241ZBE1L' &&
                        p.codice != '22417BE1' &&
                        p.codice != '22417BE1L' &&
                        p.codice != '22418BE1' &&
                        p.codice != '22418BE1L' &&
                        p.codice != '2241WBE1' &&
                        p.codice != '2241WBE1L' &&
                        p.codice != '2241QBE1' &&
                        p.codice != '2241QBE1L')

    		})
  		 
  		    var process = require('../../scripts/inserimenti/deleteGeneric.js');
            process.execute(prodotti);
        
        }
    )
});


