var process = require('./massive.js'),
    ObjectId = require('mongodb').ObjectID;
var modello = '59aff297d9bffb040079b4e4'; //codice del modello 0596 da cui partiamo
var query = { modello : modello };
var tag = 'dani_20180703_06';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.modello = ObjectId('5b3b441fe2fa7104002f393a');
                //non so perche ma non va : lanciare dopo
                //db.Prodotto.update({"codice": /0586.*/},{$set:{ modello:ObjectId('5b3b441fe2fa7104002f393a') }},{multi:true})
                p.codice = process.replaceCharAt(p.codice, '8', 3);

                switch (p.prezzo) {
                    case 85 :
                        p.prezzo = 92;
                        break;
                    case 114 :
                        p.prezzo = 121;
                        break;
                    case 125 :
                        p.prezzo = 132;
                        break;
                    case 134 :
                        p.prezzo = 141;
                        break;
                    case 120 :
                        p.prezzo = 127;
                        break;
                    case 140 :
                        p.prezzo = 147;
                        break;
                    case 149 :
                        p.prezzo = 156;
                        break;
                    case 137 :
                        p.prezzo = 144;
                        break;
                    case 157 :
                        p.prezzo = 164;
                        break;
                    case 172 :
                        p.prezzo = 179;
                        break;
                    case 105 :
                        if (p.codice.length <= 9) {
                            p.prezzo = 127;
                            break;
                        }
                }

                p.foro = '95X95';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

 1. A partire dai codici del modello 0596, duplicare i report modificando i seguenti campi:
 MODELLO: 0586 Smart Q S
 codice che diventa al terzo  carattere = 8
 prezzo:
 85  92
 105 AND MAX  LUNGH 9 CARATTERI112
 114121
 105 AND LUNGH 8 CARATTERI112
 125132
 134141
 120127
 140147
 149156
 137144
 157164
 172179

 Foro di incasso: 95X95
 */