var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    q = require('q'),
    db = CONFIG.MONGO,
    CategoriaSchema = require(process.cwd() + '/server/models/categoria'),
    ModelloSchema = require(process.cwd() + '/server/models/modello'),
    ProdottoSchema = require(process.cwd() + '/server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    Modello = mongoose.model('Modello'),
    Categoria = mongoose.model('Categoria'),
    Utils = require('../../server/routes/utils'),
    fs      = require('fs'),
    path = require('path'),
    dest = path.join(process.cwd(),'dump'),
    backup = require('mongodb-backup'),
    restore = require('mongodb-restore'),
    bckrst = require('./backup-restore'),
    ObjectId = require('mongodb').ObjectID,
    aggiornati = 0,
    g_tag = "",
    g_executedBackup = false;

function _filter (p, t) {
    var trovato = false;

    if (!t.filter) return true;

    if (_.isArray(t.filter)) {

        //console.log('[massive.js] filtri multipli');

        for (var idx = 0; idx < t.filter.length; idx++) {

            var filt = t.filter[idx];
            trovato = filt(p, t.filterArguments[idx]);
            //console.log('[DEBUG] '+ idx + ' ' + trovato + ' ' + p.codice);
            if (!trovato) {
                //console.log('[massive.js] trovato globale '+ trovato);
                return trovato;
            }

        }
    } else {

        if (t.filter(p, t.filterArguments)) {
            trovato = true;
        }
    }

    //console.log('[massive.js] trovato globale '+ trovato);
    return trovato;
}

function bck (que) {
    //console.log('connessione', CONFIG.MONGO);
    //console.log('destinazione', dest);
    //console.log('query', que);
    var deferred = q.defer();

    backup({
        uri : CONFIG.MONGO,
        root : dest,
        tar: 'dump'+g_tag+'.tar',
        query : que,
        //query : { modello: ObjectId('56b8aaa90de23c95600bd2cd'),generazione: 'G6',k: '3000',kit: { '$in': [ false, null ] },criptato: { '$in': [ false, null ] } },
        collections : ['Prodotto'],
        callback : function (err) {
            if (err) {
                console.log('errore backup', err);
                deferred.reject(err);
            }
            else
                deferred.resolve();
        }
    });

    return deferred.promise;
}

function rst (tag) {
    var deferred = q.defer();

    restore({
        uri : CONFIG.MONGO,
        root : dest,
        tar: 'dump'+g_tag+'.tar',
        callback : function (err) {
            if (err) {
                console.log('errore restore', err);
                deferred.reject(err);
            }
            else
                deferred.resolve();
        }
    });

    return deferred.promise;
}

exports.keywords = function (que) { //que è la query
    var deferred = q.defer();

    mongoose.connect(db);

    mongoose.connection.once('open', function() {

            console.log("Connessione aperta, parte il batch."); /// this gets printed to console

            Prodotto.find(que).lean()
                .populate('modello')

                .exec(function(err,prodotti){

                    //console.log('aggiorno '+prodotti.length+' prodotti')

                    var bulk = Prodotto.collection.initializeOrderedBulkOp();
                    async.each(prodotti, function(prodotto,cb){

                        var update = {};

                        console.log(aggiornati, "  - aggiornamento prodotto id = " , prodotto.codice);

                        if (prodotto.codice){
                            var p = _.clone(prodotto);


                            update.$set = {};
                            update.$set.keywords =   Utils.keyWords(p);
                            //update.$set.pubblicato=false;
                            bulk.find( { _id: prodotto._id } ).updateOne(update);
                            aggiornati ++;

                        }
                        cb();
                    }, function(err){

                        if(err)
                            deferred.reject(err);

                        if(aggiornati > 0) {

                            bulk.execute(function(err, result){
                                if(err)
                                    deferred.reject(err)

                                console.log('finito, chiudo la connessione: aggiornati '+aggiornati+' prodotti')
                                mongoose.connection.close();
                                deferred.resolve();

                            })

                        }
                        else {
                            console.log('Non ci sono prodotti da aggiornare')
                            mongoose.connection.close();
                            deferred.resolve();
                        }

                    })
                })


        }



    );

    return deferred.promise;
}

function backupProcess (t) {
    var deferred = q.defer();

    if (process.argv && process.argv[2] && t.query.modello && !process.executedBackup) {
        if (process.argv[2] == 'bck1') {
            console.log('lancia backup caso 1', t.query.modello);
            process.executedBackup = true;
            return bckrst.caso1(t.query.modello);
        }
        if (process.argv[2] == 'bck2') {
            console.log('lancia backup caso 2', t.query.modello);
            process.executedBackup = true;
            return bckrst.caso2(t.query.modello);
        }
        if (process.argv[2] == 'bck3') {
            console.log('lancia backup caso 3', t.query.modello);
            process.executedBackup = true;
            return bckrst.caso3(t.query.modello);
        }
        if (process.argv[2] == 'bck4') {
            console.log('lancia backup caso 4', t.query.modello);
            process.executedBackup = true;
            return bckrst.caso4(t.query.modello);
        }
        if (process.argv[2] == 'bck5') {
            console.log('lancia backup caso 5', t.query.modello);
            process.executedBackup = true;
            return bckrst.caso5(t.query.modello);
        }
    } else {
        return deferred.promise.resolve();
        deferred.resolve();
    }
}

/*
 var transform1 = {
 tag : tag per riconoscere i prodotti creati con questa trasformazione,
 query: query di estrazione prodotti,
 test: se eseguire l'operazione sul db o dare solo i log informativi',
 checkDuplicates: se true quando trova duplicati, li lascia e non fa operazioni su quel prodotto,
 filter:filtro per filtrare i prodotti, ce ne sono in fondo a questo file. accetta anche array,
 filterArguments:argomenti per il filtro, vedere le funzioni in fondo al file se filter è un array, anche questo deve esserlo
 transform: [
 {
 action: per ora puo valere nuovo_inserimento e modifica_esistente (insert o update),
 replace_rule: funzione di trasformazione. Deve ritornare il prodotto in caso di inserimento, l'oggetto update in caso di update
 }
 }
 ]
 };
*/

exports.execute_____testbackup = function (t) {
    var deferred = q.defer();

    backupProcess(t)
        .then(function() { exports.execute_(t); })
        .then(function() { deferred.resolve(); });

    return deferred.promise;


}

exports.execute = async function (t) {
    g_tag = t.tag;
    var log = {
        info : function (tx) {
            if (t.logLevel && t.logLevel > 0)
                console.log('[INFO] '+tx);
        },
        debug : function (tx) {
            if (t.logLevel && t.logLevel > 1)
                console.log('[DEBUG] '+tx);
        },
        log : function (tx) {
            console.log(tx);
        }
    };

    //var deferred = q.defer();

    var query = _.extend(t.query, {
            kit: {$in: [false, null]}
        },
        {
            speciale: {$in: [false, null]}
        },
        {
            criptato: {$in: [false, null]}
        }
    );

    aggiornati = 0;
    var log_duplicates = "";

    var findProdotti = async function () {
        console.log('processo vero e proprio');

        await mongoose.connect(db);

        log.log("tento di connettermi - DB: " + CONFIG.MONGO); /// this gets printed to console

        log.log("Connessione aperta, parte il batch " + JSON.stringify(query)); /// this gets printed to console

        var text_categoria, text_modello;

        try {
            let prodotti = await
            Prodotto.find(query)
                .lean()
                .limit(t.limit || 0)
                .exec();

            console.log(prodotti.length);
            return;
        } catch (err) {
            console.log('errore qui', err);
        }

    }

    /*function handleProcess(p) { // { process:'backup|restore|execute' }
        console.log('handling process', p);
        var Hdeferred = q.defer();

        if (p == 'backup') {
            bck(t.query)
                .then(function(err) { if (err) Hdeferred.reject(); else Hdeferred.resolve(); });
        } else if (p == 'restore') {
            rst(t.query)
                .then(function(err) { if (err) Hdeferred.reject(); else Hdeferred.resolve(); });
        } //implementare execute

        return Hdeferred.promise;
    }

    if (t.preProcess && t.postProcess)
        handleProcess(t.preProcess)
            .then(function (err) { return findProdotti(); })
            .then(function (err) { return handleProcess(t.postProcess); })
            .then(function(err) { deferred.resolve(); })
            .fail(function() { deferred.reject(); });
    else if (t.preProcess && !t.postProcess)
        handleProcess(t.preProcess)
            .then(function (err) { return findProdotti(); })
            .then(function(err) { deferred.resolve(); })
            .fail(function() { deferred.reject(); });
    else if (!t.preProcess && t.postProcess)
        findProdotti()
            .then(function (err) { return handleProcess(t.postProcess); })
            .then(function(err) { deferred.resolve(); })
            .fail(function() { deferred.reject(); });
    else
        findProdotti()
            .then(function(err) { deferred.resolve(); })
            .fail(function() { deferred.reject(); });


    return deferred.promise;*/

    try {
        await findProdotti();
    } catch(err) {
        console.log('errore li', err);
    }

    return;

}

exports.filters = {};

exports.filters.hasCharAt = function (p, pars) { //prodotto, oggetto parametro {chars:[], pos:1}
    var trovato = false;
    //console.log('[DEBUG FUNZIONE] '+ p.codice);
    _.each(pars.chars, function (el) {
        //console.log('[DEBUG FUNZIONE] '+ p.codice + " " + el + " " + p.codice.indexOf(el) + " " + (pars.pos-1));
        if (el.length > 1) {
            if (p.codice.indexOf(el) === pars.pos - 1)//voglio in indice 1
                trovato = true;
        } else {
            if(p.codice.substring(pars.pos - 1, pars.pos) === el)
                trovato = true;
        }
    });

    return trovato;
}

exports.filters.hasNotCharAt = function (p, pars) { //prodotto, oggetto parametro {chars:[], pos:1} posizione in base 1
    return ! exports.filters.hasCharAt(p, pars);
}

exports.filters.hasSottoCategoria = function (p, pars) { //prodotto, oggetto parametro {sottocategoria:''} CASE INSENSITIVE!!!!!!
    if (!p.sottocategoria) return false;

    if (p.sottocategoria.toLowerCase() === pars.sottocategoria.toLowerCase())
        return true;


    return false;
}

exports.filters.hasNotSottoCategoria = function (p, pars) { //prodotto, oggetto parametro {sottocategoria:''} CASE INSENSITIVE!!!!!!
    //console.log(p.sottocategoria, pars.sottocategoria);
    if (!p.sottocategoria) return true;

    if (p.sottocategoria.toLowerCase() != pars.sottocategoria.toLowerCase())
        return true;


    return false;
}

exports.filters.hasLunghezzaCodiceMax = function (p, pars) { //prodotto, oggetto parametro {len:8}
    if (p.codice.length <= pars.len)
        return true;


    return false;
}

exports.filters.isGenerazione = function (p, pars) { // prodotto, oggetto parametro {gen:['D1','G6']}
    if (_.indexOf(pars.gen, p.generazione) > -1)
        return true;

    return false;
}

exports.replaceCharAt = function (s, c, p) { //stringa carattere posizione
    return s.substring(0, p-1) + c + s.substring(p,20);//in base 1
}

exports.ifCharAtReplaceCharAt = function (s, c1, c2, p) { //stringa carattere da cercare, carattere da sostituire, posizione
    if (exports.hasCharAt(s, c1, p))
        return exports.replaceCharAt(s, c2, p);
}

exports.hasCharAt = function (s, c, p) { //stringa carattere posizione
    return s.charAt(p-1) == c;//in base 1
}

exports.hasCharsAt = function (s, c, p) { //stringa caratteri posizione
    return s.indexOf(c) == p-1;//in base 1
}


exports.dimmerabile = function (p) {
   // p.codice = exports.replaceCharAt(p.codice, 'D', 9);
    p.codice = p.codice + "D"
    p.alimentatore = 'Driver dimmerabile';
    p.dimmerabile = true;
    return p;
}

exports.dali = function (p) {
    //p.codice = exports.replaceCharAt(p.codice, 'L', 9);
    p.codice = p.codice + "L"
    p.alimentatore = 'Driver dali';
    p.dimmerabile = true;
    return p;
}

exports.push_memory = function (p) {
    p.codice = exports.replaceCharAt(p.codice, 'P', 9);
    p.alimentatore = 'Driver dimmerabile Push Memory';
    return p;
}

exports.grigio = function (p) {
    p.codice = exports.replaceCharAt(p.codice, '0', 8);
    p.colore = 'Grigio';
    return p;
}

exports.nero = function (p) {
    p.codice = exports.replaceCharAt(p.codice, '2', 8);
    p.colore = 'Nero';
    return p;
}

exports.bianco = function (p) {
    p.codice = exports.replaceCharAt(p.codice, '9', 8);
    p.colore = 'Tutto bianco';
    return p;
}


exports.archiviaConNote = function (p, gen) {
    p.codice = p.codice+"-"+gen;
    p.note = p.generazione
    p.generazione = gen
    p.exportMetel = false
    p.pubblicato = false
    return p
}

//exports.disArchivia = function (p, gen) {
exports.creaNuovoProdotto = function (p, gen) {
    p.codice = p.codice.replace('-IN39', '');
    p.generazione = gen
    p.exportMetel = true
    p.pubblicato = true
    return p
}

 

exports.aumentaPrezzo = function (p, delta) {
    p.prezzo = p.prezzo+delta;
    return p
}