var process = require('./massive.js');
var modello = '5aa92c028525e50400b87d52';
var query = { modello : modello };
var tag = 'dani_20180702_03';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                //p.codice = process.replaceCharAt(p.codice, '2', 8);
                //p.colore = 'Nero';
                p = process.nero(p);

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["C"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 6);

                p.w = 25; //potenza
                p.ma = 700; //corrente

                switch (p.lm) {
                    case '2360' :
                        if (process.hasCharAt(p.codice, 'R', 5)) {
                            p.lm = '3170';
                            break;
                        }
                    case '2480' :
                        if (process.hasCharAt(p.codice, '5', 5)) {
                            p.lm = '3330';
                            break;
                        }
                    case '2600' :
                        if (process.hasCharAt(p.codice, '6', 5)) {
                            p.lm = '3500';
                            break;
                        }
                    case '2020':
                        if (process.hasCharAt(p.codice, 'Z', 5)) {
                            p.lm = '2710';
                            break;
                        }
                    case '2125' :
                        if (process.hasCharAt(p.codice, '7', 5)) {
                            p.lm = '2860';
                            break;
                        }
                    case '2235' :
                        if (process.hasCharAt(p.codice, '8', 5)) {
                            p.lm = '3000';
                            break;
                        }
                    case '2140' :
                        if (process.hasCharAt(p.codice, 'W', 5)) {
                            p.lm = '2910';
                            break;
                        }
                    case '2020' :
                        if (process.hasCharAt(p.codice, 'Q', 5)) {
                            p.lm = '2750';
                            break;
                        }
                }

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:["D"], pos:6},{len:8}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'T', 6);
                p.w = 31; //potenza
                p.ma = 850; //corrente

                switch (p.lm) {
                    case '3170' :
                        p.lm = '3730';
                        break;
                    case '3330' :
                        p.lm = '3915';
                        break;
                    case '3500':
                        p.lm = '4115';
                        break;
                    case '2710' :
                        p.lm = '3190';
                        break;
                    case '2860' :
                        p.lm = '3355';
                        break;
                    case '3000' :
                        p.lm = '3530';
                        break;
                    case '2910' :
                        p.lm = '3950';
                        break;
                    case '2750' :
                        p.lm = '3440';
                        break;
                }

                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'N', 7);
                p.fascio = 40;

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["N"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'T', 7);
                p.fascio = 60;

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*


 1. Cancellare tutti i codici del modello ad eccezione di questi 16 codici:
 2242RCE1
 2242RCE1L
 22425CE1
 22425CE1L
 22426CE1
 22426CE1L
 2242ZCE1
 2242ZCE1L
 22427CE1
 22427CE1L
 22428CE1
 22428CE1L
 2242WCE1
 2242WCE1L
 2242QCE1
 2242QCE1L

 2. A partire dai codici rimasti dal PUNTO1 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 codice che diventa all’ottavo carattere = 2
 colore = Nero
 3. A partire da tutti i codici creati sia nel punto 1 che nel  punto 2  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 6° carattere C  D
 Potenza: 25
 Corrente: 700
 Fl. Lum. Nom:
 2360  3170
 2480  3330
 2600  3500
 2020   2710
 2125  2860
 2235  3000
 2140  2910
 2020  2750

 4. A partire da tutti i codici creati nel punto 3 CON LUNGHEZZA MAX 8 CARATTERI creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 6° carattere D T
 Potenza: 31
 Corrente: 850
 Fl. Lum. Nom:
 31703730
 33303915
 35004115
 27103190
 28603355
 30003530
 29103950
 27503440

 5. A partire da tutti i codici creati sia nel punto 1 che nel  punto 2 che nel punto 3 che nel punto 4  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 7° carattere E  N
 Fascio: 40
 5 . A partire da tutti i codici creati nel punto 5 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 7° carattere N  T
 Fascio: 60
 */