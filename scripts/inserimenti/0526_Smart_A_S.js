var process = require('./massive.js');
var modello = '5b2b4fe1694a5804009cae35';
var query = { modello : modello };
var tag = 'dani_20180702_09';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'N', 7);
                p.fascio = '40';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["N"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'S', 7);
                p.fascio = '55';

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["B"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'C', 6);

                p.w = 17; //potenza
                p.ma = 500; //corrente

                switch (p.lm) {
                    case '1705' :
                        p.lm = '2360';
                        break;
                    case '1790' :
                        p.lm = '2480';
                        break;
                    case '1880' :
                        p.lm = '2600';
                        break;
                    case '1460' :
                        p.lm = '2020';
                        break;
                    case '1535' :
                        p.lm = '2125';
                        break;
                    case '1610' :
                        p.lm = '2235';
                        break;
                    case '1530' :
                        p.lm = '2140';
                        break;
                    case '1450' :
                        p.lm = '2020';
                        break;
                }
                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    //filter: process.filters.hasNotCharAt,
    //filterArguments : {chars:["C"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice += 'E';
                p.alimentatore = 'Driver standard';
                p.alimentatore_incluso = true;

                if (process.hasCharAt(p.codice, 'R', 5) ||
                    process.hasCharAt(p.codice, '5', 5) ||
                    process.hasCharAt(p.codice, '6', 5) ||
                    process.hasCharAt(p.codice, 'Z', 5) ||
                    process.hasCharAt(p.codice, '7', 5) ||
                    process.hasCharAt(p.codice, '8', 5)
                )
                    p.prezzo = 113;

                if (process.hasCharAt(p.codice, 'W', 5))
                    p.prezzo = 133;

                if (process.hasCharAt(p.codice, 'Q', 5))
                    p.prezzo = 148;

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["E"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);

                switch (p.prezzo) {
                    case 113 :
                        p.prezzo = 145;
                        break;
                    case 133 :
                        p.prezzo = 165;
                        break;
                    case 148 :
                        p.prezzo = 180;
                        break;
                }

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform6 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);

                p.dimmerabile = true;

                console.log('nella sesta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};
process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

 1. Cancellare tutti i codici del modello ad eccezione di questi 8 codici:
 0526RBE1
 05265BE1
 05266BE1
 0526ZBE1
 05267BE1
 05268BE1
 0526WBE1
 0526QBE1
 2. A partire dai codici rimasti dal PUNTO1 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 codice che diventa all’settimo carattere = N
 fascio = 40
 3.A partire da tutti i codici creati sia nel punto 2  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 codice che diventa all’settimo carattere  N  T
 fascio = 60
 4.A partire da tutti i codici creati sia nel punto 2 che nel punto 3 e quelli rimasti nel punto 1 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 6° carattere B  C
 Potenza: 17
 Corrente: 500
 Fl. Lum. Nom:
 17052360
 17902480
 18802600
 14602020
 15352125
 16102235
 15302140
 1450 2020

 5. A partire da tutti i codici creati sia nel punto 1 che nel  punto 2 che nel punto 3  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =E
 Alimentatore: Driver standard
 Flag Alimentatore incluso =TRUE
 Prezzo:
 5° carattere =(R AND 5 AND 6 AND Z AND 7 AND 8): 113
 5° carattere = W   :  133
 5° carattere = qqqqqq  Q   :  148

 6. A partire da tutti i codici creati sia nel punto 5 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =       ED
 Alimentatore: Driver dimmerabile
 Flag Dimm.=TRUE
 Prezzo:
 113  145
 133  165
 148  180

 7. A partire da tutti i codici creati sia nel punto 6 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =       DL
 Alimentatore: Driver Dali
 Flag Dimm.=TRUE

 */