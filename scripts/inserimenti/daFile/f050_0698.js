/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/inserimenti/daFile/f050_0698.js;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
     _ = require('underscore'),
    csv = require("fast-csv");
    fs = require('fs'),
    aggiornati = 0, 
    csvpath = "scripts/inserimenti/daFile/F050/1_F050_0698.csv",
    prodotti = [],
    stream = fs.createReadStream(csvpath); 


     csv.fromStream(stream, {
            headers: true,
            delimiter: ',',
            discardUnmappedColumns: true
        }) 
        //RECUPERO I DATI DAL FILE
        .on("data", function (data) {
             
             
                var record = {
                    codice      : data.codice,
                    lm          : data.Fl_Lum_Nom,
                    generazione :data.Generazione,
                    macadam:data.MacAdam,
                    lunghezza:data.Lungh,
                    prezzo:parseFloat(data.Prezzo),
                    w : data.potenza,
                    colore:data.COLORE,
                    verniciatura: data.Vern,
                    corpo_it : data.Costr_Corpo_IT,
                    linea:data.Linea.toLowerCase(),
                    k:data.Temp_Col,
                    ma:data.corrente,
                    moduloled:data.MOD_LED,
                    corpo_en:data.Costr_Corpo_EN,
                    destinazione:data.dest_Uso.toLowerCase(),
                    fascio:data.FASCIO,
                    ottica_it:data.Costr_Ottica_IT,

                    modello_id:"56b8aaa90de23c95600bd294",
                    categoria_id:"56b8aaa80de23c95600bd21c",

                    modello: "0698 Eco Point Led",
                    categoria:"Incassi fissi",

                    v:data.Tensione,
                    tmax:data.Temp_Amb_Max,
                    ottica_en:data.costr_Ottica_EN,
                    hz:data.Freq,
                    alimentatore:data.alimentatore,
                    irc:data.in_resa_crom,
                    sottocategoria:data.sottocat,
                    dimmerabile : Boolean(data.Dimm),
                    descrizione_applicazione : data.Descr_Appl,
                    rischio_fotobiologico : data.Risch_Fotob,
                    filo_incandescente : data.Filo_inc,
                    durata : data.durata,
                    orientabile : data.orien,
                    classe_isolamento : data.Cl_Isolamento,
                    ip : data.Grado_IP,
                    vita_utile_desc : data.Dec_Fl,
                    inclinabile : data.Incl,
                    foro : data.foro,
                    satinato : Boolean(data.sat),
                    alimentatore_incluso : Boolean(data.alim_Incl),
                    divieto_controsoffitti : Boolean(data.div_Inst_contr_Is),
                 ce : true,
                    f : true,
                    pubblicato : true,
                    exportMetel :true 
                }

                
               
                var prodotto = _.clone(record);
                prodotti.push(prodotto)
             
        })
        .on("end", 
            function () {

                 //console.log("prodotti ", prodotti);

                  var process = require('../../../scripts/inserimenti/daFile/insertGeneric.js');
                  process.execute(prodotti);


            }
        ) 