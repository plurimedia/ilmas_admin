/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/inserimenti/daFile/kfamily_1.js;
*/

var categorie = [{
    "_id": {
        "$oid": "56b8aaa80de23c95600bd219"
    },
    "nome": "Moduli Led"
},
{
    "_id": {
        "$oid": "56b8aaa80de23c95600bd21a"
    },
    "nome": "Proiettori"
},
{
    "_id": {
        "$oid": "56b8aaa80de23c95600bd21c"
    },
    "nome": "Incassi fissi"
},
{
    "_id": {
        "$oid": "56b8aaa80de23c95600bd21d"
    },
    "nome": "Incassi orientabili"
},
{
    "_id": {
        "$oid": "56b8aaa80de23c95600bd21e"
    },
    "nome": "Plafone/Parete"
},
{
    "_id": {
        "$oid": "56b8aaa80de23c95600bd220"
    },
    "nome": "Sospensioni"
},
{
    "_id": {
        "$oid": "56b8aaa80de23c95600bd224"
    },
    "nome": "Profili"
},
{
    "_id": {
        "$oid": "56b8ab022a376026658f22e3"
    },
    "nome": "Lampada Led"
},
{
    "_id": {
        "$oid": "56b8ab022a376026658f22e4"
    },
    "nome": "Apparecchio"
},
{
    "_id": {
        "$oid": "570b5473eff59523af4fcd2b"
    },
    "nome": "Accessori"
},
{
    "_id": {
        "$oid": "570b550deff59523af4fcd2c"
    },
    "nome": "Binari"
},
{
    "_id": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    },
    "nome": "Sorgenti non led"
}]


var modelli = 
[{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd225"
    },
    "nome": "F111 Led Module",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd219"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd226"
    },
    "nome": "F112 Led Module",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd219"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd227"
    },
    "nome": "F050 Led Module",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd219"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd228"
    },
    "nome": "V1 Plus Led Module",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd219"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd229"
    },
    "nome": "V1 Led Module",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd219"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd22a"
    },
    "nome": "V2 Led Module",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd219"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd22b"
    },
    "nome": "1240 Over Led Ps Mini",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd22c"
    },
    "nome": "1241 Over Led Ps Midi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd22d"
    },
    "nome": "1242 Over Led Ps Maxi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd22e"
    },
    "nome": "1242 Over Led Ps Mega",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd22f"
    },
    "nome": "1290 Over Led Ps2 Mini",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd230"
    },
    "nome": "1291 Over Led Ps2 Midi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd232"
    },
    "nome": "1292 Over Led Ps2 Maxi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd233"
    },
    "nome": "1292 Over Led Ps2 Mega",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd234"
    },
    "nome": "1330 Fly Mini",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd235"
    },
    "nome": "1331 Fly Midi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd236"
    },
    "nome": "1332 Fly Maxi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd237"
    },
    "nome": "1332 Fly Mega",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd238"
    },
    "nome": "1321 Viper Midi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd239"
    },
    "nome": "1322 Viper Maxi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd23a"
    },
    "nome": "1322 Viper Mega",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd23b"
    },
    "nome": "1272 Form Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd23c"
    },
    "nome": "1255 Five",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd23d"
    },
    "nome": "1295 Kompact Ar",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd23e"
    },
    "nome": "1296 Kompact  Pro",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd23f"
    },
    "nome": "1297 Kompact Ps Pro",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd240"
    },
    "nome": "1222 Six",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd241"
    },
    "nome": "1232 Ten",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd242"
    },
    "nome": "1256 Seven",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd243"
    },
    "nome": "0796 Hammer Mini 120",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd244"
    },
    "nome": "0797 Hammer Midi 160",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd245"
    },
    "nome": "0798 Hammer Maxi 205",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd246"
    },
    "nome": "0798 Hammer Mega 205",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd247"
    },
    "nome": "0799 Hammer Extra 230",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd248"
    },
    "nome": "0799 Hammer EX Plus 230",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd249"
    },
    "nome": "0786 Hammer Q Mini 120",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd24a"
    },
    "nome": "0787 Hammer Q Midi 160",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd24b"
    },
    "nome": "0789 Hammer Q Extra 230",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd24c"
    },
    "nome": "0789 Hammer Q Ex Plus 230",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd24d"
    },
    "nome": "2666 Tiger Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd24e"
    },
    "nome": "0670 Astuto Led Mini",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd24f"
    },
    "nome": "0671 Astuto Led Midi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd250"
    },
    "nome": "0672 Astuto Led Maxi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd251"
    },
    "nome": "0672 Astuto Led Mega",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd252"
    },
    "nome": "0660 Dino Led Mini",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd253"
    },
    "nome": "0661 Dino Led Midi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd254"
    },
    "nome": "0662 Dino Led Maxi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd255"
    },
    "nome": "0662 Dino Led Mega",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd256"
    },
    "nome": "0730 Berin Led Mini",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd257"
    },
    "nome": "0731 Berin Led Midi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd258"
    },
    "nome": "0732 Berin Led Maxi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd259"
    },
    "nome": "0732 Berin Led Mega",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd25a"
    },
    "nome": "0608 Bob Std Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd25b"
    },
    "nome": "0609 Bob Std Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd25c"
    },
    "nome": "0610 Bob Std Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd25d"
    },
    "nome": "0611 Bob Std Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd25e"
    },
    "nome": "0608 Bob Std Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd25f"
    },
    "nome": "0609 Bob Std Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd260"
    },
    "nome": "0610 Bob Std Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd261"
    },
    "nome": "0611 Bob Std Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd262"
    },
    "nome": "1708 Bob Ext Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd263"
    },
    "nome": "1709 Bob Ext Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd264"
    },
    "nome": "1710 Bob Ext Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd265"
    },
    "nome": "1708 Bob Ext Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd266"
    },
    "nome": "1709 Bob Ext Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd267"
    },
    "nome": "1710 Bob Ext Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd268"
    },
    "nome": "1608 Bob Std Sc Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd269"
    },
    "nome": "1609 Bob Std Sc Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd26a"
    },
    "nome": "1610 Bob Std Sc Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd26b"
    },
    "nome": "1611 Bob Std Sc Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd26c"
    },
    "nome": "1608 Bob Std Sc Arled",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd26d"
    },
    "nome": "1609 Bob Std Sc Arled",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd26e"
    },
    "nome": "1610 Bob Std Sc Arled",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd26f"
    },
    "nome": "1611 Bob Std Sc Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd270"
    },
    "nome": "1738 Bob Ext Sc Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd271"
    },
    "nome": "1739 Bob Ext Sc Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd272"
    },
    "nome": "1740 Bob Ext Sc Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd273"
    },
    "nome": "1738 Bob Ext Sc Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd274"
    },
    "nome": "1739 Bob Ext Sc Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd275"
    },
    "nome": "1740 Bob Ext Sc Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd276"
    },
    "nome": "1808 Bob Eco Arled",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd277"
    },
    "nome": "1809 Bob Eco Arled",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd278"
    },
    "nome": "1810 Bob Eco Arled",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd279"
    },
    "nome": "1712 Bob Eco Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd27a"
    },
    "nome": "1713 Bob Eco Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd27b"
    },
    "nome": "1714 Bob Eco Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd27c"
    },
    "nome": "0908 Bob Slim",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd27d"
    },
    "nome": "0909 Bob Slim",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd27e"
    },
    "nome": "0910 Bob Slim",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd27f"
    },
    "nome": "1888 Puzzle Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd280"
    },
    "nome": "1888 Puzzle Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd281"
    },
    "nome": "0621 Bob Std Tondo",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd282"
    },
    "nome": "0620 Bob Eco Tondo",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd283"
    },
    "nome": "0621 Bob Std Tondo Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd284"
    },
    "nome": "0620 Bob Eco Tondo Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd285"
    },
    "nome": "0604 Lisetta Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd286"
    },
    "nome": "1604 Lisetta Sc Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd287"
    },
    "nome": "0606 Lisetta Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd288"
    },
    "nome": "1606 Lisetta Sc Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd289"
    },
    "nome": "0607 Lisetta Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd28a"
    },
    "nome": "1607 Lisetta Sc Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd28b"
    },
    "nome": "0445 Star Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd28c"
    },
    "nome": "0444 Star Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd28d"
    },
    "nome": "0446 Star Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd28e"
    },
    "nome": "0693 Eco Point Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd28f"
    },
    "nome": "0692 Eco Point Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd290"
    },
    "nome": "0694 Eco Point Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd291"
    },
    "nome": "0695 Eco Point Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd292"
    },
    "nome": "0696 Eco Point Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd293"
    },
    "nome": "0697 Eco Point Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd294"
    },
    "nome": "0698 Eco Point Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd295"
    },
    "nome": "0699 Eco Point Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd296"
    },
    "nome": "B835 Eco Point Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd297"
    },
    "nome": "0860 Focus",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd298"
    },
    "nome": "0861 Point Plus",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd299"
    },
    "nome": "0862 Point Plus",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd29a"
    },
    "nome": "0850 Flat Point",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd29b"
    },
    "nome": "0873 Point Led IP 65",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd29c"
    },
    "nome": "0876 Point Led IP 65",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd29d"
    },
    "nome": "0874 Point Led IP 65",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd29e"
    },
    "nome": "0875 Point Led IP 65",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd29f"
    },
    "nome": "4898 Spring Maxi 205",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2a0"
    },
    "nome": "4898 Spring Mega 205",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2a1"
    },
    "nome": "4899 Spring Extra 230",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2a2"
    },
    "nome": "4899 Spring Extra Plus 230",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2a3"
    },
    "nome": "4621 Fold Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2a4"
    },
    "nome": "4622 Fold Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2a5"
    },
    "nome": "4623 Fold Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2a6"
    },
    "nome": "4621 Fold Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2a7"
    },
    "nome": "4622 Fold Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2a8"
    },
    "nome": "4623 Fold Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2a9"
    },
    "nome": "4711 Tech Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2aa"
    },
    "nome": "4712 Tech Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2ab"
    },
    "nome": "4713 Tech Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2ac"
    },
    "nome": "4714 Tech Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2ad"
    },
    "nome": "4711 Tech Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2ae"
    },
    "nome": "4712 Tech Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2af"
    },
    "nome": "4713 Tech Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2b0"
    },
    "nome": "4714 Tech Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2b1"
    },
    "nome": "0775 Nuvola Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2b2"
    },
    "nome": "0776 Nuvola Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2b3"
    },
    "nome": "0777 Nuvola Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2b4"
    },
    "nome": "0778 Nuvolino",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2b5"
    },
    "nome": "0779 Nuvolino",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2b6"
    },
    "nome": "1060 Dial 1 Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2b7"
    },
    "nome": "1061 Dial 2 Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2b8"
    },
    "nome": "1062 Dial 3 Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2b9"
    },
    "nome": "1063 Dial 4 Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2ba"
    },
    "nome": "1060 Dial Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2bb"
    },
    "nome": "1061 Dial Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2bc"
    },
    "nome": "1062 Dial Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2bd"
    },
    "nome": "1063 Dial Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2be"
    },
    "nome": "4531 Orion",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2bf"
    },
    "nome": "4532 Orion",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2c0"
    },
    "nome": "4533 Orion",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2c1"
    },
    "nome": "4531 Orion Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2c2"
    },
    "nome": "4532 Orion Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2c3"
    },
    "nome": "4533 Orion Arled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2c4"
    },
    "nome": "4996 Summer Mini 120",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2c5"
    },
    "nome": "4997 Summer Midi 160",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2c6"
    },
    "nome": "4998 Summer Maxi 205",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2c7"
    },
    "nome": "4998 Summer Mega 205",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2c8"
    },
    "nome": "4999 Summer Extra 230",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2c9"
    },
    "nome": "4999 Summer Extra Plus 230",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2ca"
    },
    "nome": "1002 Marquez Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2cb"
    },
    "nome": "1003 Marquez Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2cc"
    },
    "nome": "1004 Marquez Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2cd"
    },
    "nome": "1006 Marquez Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2ce"
    },
    "nome": "1002 Marquez - Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2cf"
    },
    "nome": "1003 Marquez Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2d0"
    },
    "nome": "1004 Marquez Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2d1"
    },
    "nome": "1006 Marquez Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2d2"
    },
    "nome": "1155 Bell Mono",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2d3"
    },
    "nome": "1157 Bell Mono",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2d4"
    },
    "nome": "1165 Bell Bi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2d5"
    },
    "nome": "1167 Bell Bi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2d6"
    },
    "nome": "DL1240 Over",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2d7"
    },
    "nome": "DL1241 Over",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2d8"
    },
    "nome": "DL1242 Over",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2d9"
    },
    "nome": "DL1270 Form",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2da"
    },
    "nome": "DL1271 Form",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2db"
    },
    "nome": "DL1272 Form",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2dc"
    },
    "nome": "DL1273 Form",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2dd"
    },
    "nome": "DL1281 Quadro",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2de"
    },
    "nome": "DL0608 Bob Std",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2df"
    },
    "nome": "DL0609 Bob Std",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2e0"
    },
    "nome": "DL0610 Bob Std",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2e1"
    },
    "nome": "DL0611 Bob Std",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2e2"
    },
    "nome": "DL6b08 Bob Std Whi",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2e3"
    },
    "nome": "DL6b09 Bob Std Whi",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2e4"
    },
    "nome": "DL6b10 Bob Std Whi",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2e5"
    },
    "nome": "DL6b11 Bob Std Whi",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2e6"
    },
    "nome": "DL1708 Bob Ext",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2e7"
    },
    "nome": "DL1709 Bob Ext",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2e8"
    },
    "nome": "DL1710 Bob Ext",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2e9"
    },
    "nome": "DL17b8 Bob Ext Whi",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2ea"
    },
    "nome": "DL17b9 Bob Ext Whi",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2eb"
    },
    "nome": "DL17b0 Bob Ext Whi",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2ec"
    },
    "nome": "DL1608 Bob Std Sc",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2ed"
    },
    "nome": "DL1609 Bob Std Sc",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2ee"
    },
    "nome": "DL1610 Bob Std Sc",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2ef"
    },
    "nome": "DL1808 Bob Eco Tondo",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2f0"
    },
    "nome": "DL1809 Bob Eco Tondo",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2f1"
    },
    "nome": "DL1810 Bob Eco Tondo",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2f2"
    },
    "nome": "DL1712 Bob Eco Tondo",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2f3"
    },
    "nome": "DL1713 Bob Eco Tondo 0620",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2f4"
    },
    "nome": "DL1714 Bob Eco Tondo",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2f5"
    },
    "nome": "DL1888 Puzzle Led",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2f6"
    },
    "nome": "DL0621 Bob Std",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2f7"
    },
    "nome": "DL0620 Bob Eco Tondo",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2f8"
    },
    "nome": "DL0604 Lisetta",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2f9"
    },
    "nome": "DL0606 Lisetta",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2fa"
    },
    "nome": "DL0607 Lisetta",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2fb"
    },
    "nome": "DL0603 Lisetta",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2fc"
    },
    "nome": "DL61b9 Sc Lisetta",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2fd"
    },
    "nome": "DL61b0 Sc Lisetta",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2fe"
    },
    "nome": "DL0600 Cloud",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd2ff"
    },
    "nome": "DL0605 Cloud",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd300"
    },
    "nome": "DL0680 Cloud",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd301"
    },
    "nome": "DL0681 Cloud",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd302"
    },
    "nome": "DL0682 Cloud",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd303"
    },
    "nome": "DL0683 Cloud",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd304"
    },
    "nome": "DL0684 Cloud",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd305"
    },
    "nome": "DL0690 Cloud",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd306"
    },
    "nome": "DL691b Cloud",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd307"
    },
    "nome": "DL0691 Cloud",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd308"
    },
    "nome": "DL0650 Remy",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd309"
    },
    "nome": "DL0651 Remy",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd30a"
    },
    "nome": "DL0652 Remy",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd30b"
    },
    "nome": "DL0653 Lagun",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd30c"
    },
    "nome": "DL0654 Lagun",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd30d"
    },
    "nome": "DL0655 Lagun",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd30e"
    },
    "nome": "DL0656 Lagun",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd30f"
    },
    "nome": "DL0658 Luna",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd310"
    },
    "nome": "DL0670 Astuto",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd311"
    },
    "nome": "DL0671 Astuto",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd312"
    },
    "nome": "DL0672 Astuto",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd313"
    },
    "nome": "DL0660 Dino",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd314"
    },
    "nome": "DL0661 Dino",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd315"
    },
    "nome": "DL0662 Dino",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd316"
    },
    "nome": "DL0649 Venus",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd317"
    },
    "nome": "DL0750 Drk",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd318"
    },
    "nome": "DL0751 Drk",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd319"
    },
    "nome": "DL0752 Drk",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd31a"
    },
    "nome": "DL0753 Drk",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd31b"
    },
    "nome": "DL0754 Drk",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd31c"
    },
    "nome": "DL2666 Tiger",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd31d"
    },
    "nome": "DL0780 Sqr",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd31e"
    },
    "nome": "DL0781 Sqr",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd31f"
    },
    "nome": "DL0790 Sqr",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd320"
    },
    "nome": "DL0791 Sqr",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd321"
    },
    "nome": "DL0730 Berin",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd322"
    },
    "nome": "DL0731 Berin",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd323"
    },
    "nome": "DL0732 Berin",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd324"
    },
    "nome": "DL0734 Berin",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd325"
    },
    "nome": "DL0694 Eco Point 1a",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd326"
    },
    "nome": "DL0695 Eco Point 1b",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd327"
    },
    "nome": "DL0693 Eco Point 2a",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd328"
    },
    "nome": "DL0692 Eco Point 2b",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd329"
    },
    "nome": "DL0696 Eco Point",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd32a"
    },
    "nome": "DL0697 Eco Point",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd32b"
    },
    "nome": "DL0698 Eco Point",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd32c"
    },
    "nome": "DL0699 Eco Point",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd32d"
    },
    "nome": "DLB835 Eco Point 5a",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd32e"
    },
    "nome": "DL0811 Point",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd32f"
    },
    "nome": "DL0812 Point",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd330"
    },
    "nome": "DL0813 Point",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd331"
    },
    "nome": "DL0814 Point",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd332"
    },
    "nome": "DL0818 Point",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd333"
    },
    "nome": "DL0822 Point",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd334"
    },
    "nome": "DL0823 Point",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd335"
    },
    "nome": "DL0824 Point",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd336"
    },
    "nome": "DL0830 Point",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd337"
    },
    "nome": "DL0838 Point",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd338"
    },
    "nome": "DL1060 Dial 1",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd339"
    },
    "nome": "DL1061 Dial 2",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd33a"
    },
    "nome": "DL1062 Dial 3",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd33b"
    },
    "nome": "DL1063 Dial 4",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd33c"
    },
    "nome": "DL4531 Orion",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd33d"
    },
    "nome": "DL4532 Orion",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd33e"
    },
    "nome": "DL4533 Orion",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd33f"
    },
    "nome": "DL4621 Fold",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd340"
    },
    "nome": "DL4622 Fold",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd341"
    },
    "nome": "DL4623 Fold",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd342"
    },
    "nome": "DL4711 Tech",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd343"
    },
    "nome": "DL4712 Tech",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd344"
    },
    "nome": "DL4713 Tech",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd345"
    },
    "nome": "DL4714 Tech",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd346"
    },
    "nome": "DL0775 Nuvola",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd347"
    },
    "nome": "DL0776 Nuvola",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd348"
    },
    "nome": "DL0777 Nuvola",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd349"
    },
    "nome": "DL0778 Nuvolino",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd34a"
    },
    "nome": "DL0779 Nuvolino",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd34b"
    },
    "nome": "DL1000 Marquez",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd34c"
    },
    "nome": "DL3000 Reglette",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd34d"
    },
    "nome": "DL3001 Reglette",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd34e"
    },
    "nome": "Lampade Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd219"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd350"
    },
    "nome": "0931 Triec 120",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd351"
    },
    "nome": "0933 Triec 160",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd352"
    },
    "nome": "0937 Triec 230",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd353"
    },
    "nome": "1704 Mini Bob",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd354"
    },
    "nome": "1706 Mini Bob",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd355"
    },
    "nome": "1707 Mini Bob",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd356"
    },
    "nome": "2667 Tiger Led Q",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd357"
    },
    "nome": "B835 Eco Point Diled Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd358"
    },
    "nome": "1241 Over Dali Ps Midi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd359"
    },
    "nome": "1242 Over Dali Ps Maxi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd35a"
    },
    "nome": "1242 Over Dali Ps Mega",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd35b"
    },
    "nome": "1292 Over Dali Ps2 Maxi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd35c"
    },
    "nome": "1292 Over Dali Ps2 Mega",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd35d"
    },
    "nome": "1321 Viper Dali Midi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd35e"
    },
    "nome": "1322 Viper Dali Maxi",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd35f"
    },
    "nome": "1322 Viper Dali Mega",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd360"
    },
    "nome": "Strip",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd219"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd361"
    },
    "nome": "Profile",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd219"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd362"
    },
    "nome": "Flex Tridonic",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd219"
    }
},
{
    "_id": {
        "$oid": "56b8aaa90de23c95600bd363"
    },
    "nome": "Kit Jc",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "56b8ab022a376026658f22e5"
    },
    "nome": "Goccia A67",
    "categoria": {
        "$oid": "56b8ab022a376026658f22e3"
    }
},
{
    "_id": {
        "$oid": "56b8ab022a376026658f22e6"
    },
    "nome": "Goccia A60",
    "categoria": {
        "$oid": "56b8ab022a376026658f22e3"
    }
},
{
    "_id": {
        "$oid": "56b8ab022a376026658f22e7"
    },
    "nome": "Golf E27",
    "categoria": {
        "$oid": "56b8ab022a376026658f22e3"
    }
},
{
    "_id": {
        "$oid": "56b8ab022a376026658f22e8"
    },
    "nome": "Golf E14",
    "categoria": {
        "$oid": "56b8ab022a376026658f22e3"
    }
},
{
    "_id": {
        "$oid": "56b8ab022a376026658f22e9"
    },
    "nome": "Candela E14",
    "categoria": {
        "$oid": "56b8ab022a376026658f22e3"
    }
},
{
    "_id": {
        "$oid": "56b8ab022a376026658f22ea"
    },
    "nome": "Gu10",
    "categoria": {
        "$oid": "56b8ab022a376026658f22e3"
    }
},
{
    "_id": {
        "$oid": "56b8ab022a376026658f22eb"
    },
    "nome": "Gu5.3",
    "categoria": {
        "$oid": "56b8ab022a376026658f22e3"
    }
},
{
    "_id": {
        "$oid": "56b8ab022a376026658f22ec"
    },
    "nome": "Tubi",
    "categoria": {
        "$oid": "56b8ab022a376026658f22e3"
    }
},
{
    "_id": {
        "$oid": "56b8ab022a376026658f22ed"
    },
    "nome": "Led Panel",
    "categoria": {
        "$oid": "56b8ab022a376026658f22e4"
    }
},
{
    "_id": {
        "$oid": "570cc3b81fb26a0300b0c1f0"
    },
    "nome": "0672 Astuto Led Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "570cdd4b1fb26a0300b0c1f8"
    },
    "nome": "0672 Astuto Led AR111",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "570ce73d1fb26a0300b0c209"
    },
    "nome": "0662 Dino Led AR111",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "570d2f691fb26a0300b0c227"
    },
    "nome": "0620 Bob Eco Tondo AR111",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "570d31d71fb26a0300b0c22c"
    },
    "nome": "0620 Bob Eco Tondo Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "570d4e61f393520300ad8fc7"
    },
    "nome": "1888 Puzzle Led Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "570d51d0f393520300ad8fcf"
    },
    "nome": "1808 Bob Eco Led Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "570d51e4f393520300ad8fd0"
    },
    "nome": "1809 Bob Eco Led Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "570e00c9f393520300ad8fed"
    },
    "nome": "1810 Bob Eco Led Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "570e00dcf393520300ad8fee"
    },
    "nome": "1808 Bob Eco Led AR111",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "570e00f6f393520300ad8fef"
    },
    "nome": "1809 Bob Eco Led AR111",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "570e010af393520300ad8ff0"
    },
    "nome": "1810 Bob Eco Led AR111",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "570f7c2fdc2639030018740a"
    },
    "nome": "0649 Venus Toshiba GU10",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "5710b91720a93203003757dc"
    },
    "nome": "B835 Eco Point Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "5710b92b20a93203003757dd"
    },
    "nome": "B835 Eco Point Toshiba GU10",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "571162455f09a60300e387ab"
    },
    "nome": "AR111",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd219"
    }
},
{
    "_id": {
        "$oid": "5711f38e5f09a60300e387b0"
    },
    "nome": "9600 Binario elettrificato / Track",
    "categoria": {
        "$oid": "570b550deff59523af4fcd2c"
    }
},
{
    "_id": {
        "$oid": "5711f3f85f09a60300e387b1"
    },
    "nome": "9605 Binario Dali",
    "categoria": {
        "$oid": "570b550deff59523af4fcd2c"
    }
},
{
    "_id": {
        "$oid": "5711f4135f09a60300e387b2"
    },
    "nome": "9615 Binario Dali incasso",
    "categoria": {
        "$oid": "570b550deff59523af4fcd2c"
    }
},
{
    "_id": {
        "$oid": "5711f4485f09a60300e387b5"
    },
    "nome": "9615 Binario Dali",
    "categoria": {
        "$oid": "570b550deff59523af4fcd2c"
    }
},
{
    "_id": {
        "$oid": "5718b2ec86ecb603002d84ff"
    },
    "nome": "DL3120 Danny",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "57333a9d77c68d03002e9c7d"
    },
    "nome": "Alette",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "573436d4c0f52e030063bb3d"
    },
    "nome": "2001 KS Track",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "5743f1d166923a030050afd0"
    },
    "nome": "2002 KL Track",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "5770ccdd4e67d30300065189"
    },
    "nome": "4855 Star Led Plafone",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "5784ca8ac98fef0300305938"
    },
    "nome": "0788 Hammer Q Maxi 205",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "57c5311409d58a030000bb73"
    },
    "nome": "0788 Hammer Q Mega 205",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "57c932d4bd08520300abacf2"
    },
    "nome": "DL1808 Bob Eco",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "57c95a6a325cad0300b6a625"
    },
    "nome": "DL1809 Bob Eco",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "57c95a86325cad0300b6a626"
    },
    "nome": "DL1810 Bob Eco",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "57c95aaf325cad0300b6a628"
    },
    "nome": "DL1712 Bob Eco",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "57c95ac6325cad0300b6a629"
    },
    "nome": "DL1713 Bob Eco",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "57c95afe325cad0300b6a62b"
    },
    "nome": "DL1714 Bob Eco",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "57c95b15325cad0300b6a62c"
    },
    "nome": "DL0620 Bob Eco",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "57cd7bcf7648790300f38739"
    },
    "nome": "Accesorio staffe (2 P.z) per installazione a sistema",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57cd7bed7648790300f3873a"
    },
    "nome": "Accessorio montaggio per cartongesso",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57cd7c1b7648790300f3873c"
    },
    "nome": "Anello di compensazione",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57cd7c377648790300f3873d"
    },
    "nome": "Base di fissaggio a soffitto senza driver",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57cd7c567648790300f3873e"
    },
    "nome": "Basetta Alluminio",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57cd7c6f7648790300f3873f"
    },
    "nome": "Basetta Plastica",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57cd7c8a7648790300f38740"
    },
    "nome": "Cornice di compensazione",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57cd7ca17648790300f38741"
    },
    "nome": "Kit gruppo di emergenza Sa",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57cd7cb97648790300f38742"
    },
    "nome": "Kit sospensione cavo acciaio 3 Pz",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57cd7cd17648790300f38743"
    },
    "nome": "Ottica alluminio alta efficienza Ottica 17°",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57cd7ce87648790300f38744"
    },
    "nome": "Ottica alluminio alta efficienza Ottica 25°",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57cd7cfa7648790300f38745"
    },
    "nome": "Ottica alluminio alta efficienza Ottica 33°",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57cd7d0d7648790300f38746"
    },
    "nome": "Vetro sabbiato",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57cd8b3e7648790300f38747"
    },
    "nome": "Marquez Cassetta Alimentazione",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57d2ca7b816282030004991e"
    },
    "nome": "DL3901 Yama",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "57e4d90c3e3a3b030076c98a"
    },
    "nome": "Accessori Binari",
    "categoria": {
        "$oid": "570b5473eff59523af4fcd2b"
    }
},
{
    "_id": {
        "$oid": "57eb8b5d60ce4b0300262fe6"
    },
    "nome": "2025 KS/A Track",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "57eb8bac60ce4b0300262fe7"
    },
    "nome": "2027 KL/2",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "57eb8bbf60ce4b0300262fe8"
    },
    "nome": "2028 KL/3",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "57f51b9b3a304f03004a3774"
    },
    "nome": "3209 Artik R",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "58132feca2f78a03006fe544"
    },
    "nome": "1257 Five",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "5813730d4f8ebc0300f82859"
    },
    "nome": "3266 Artik Q",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "5821d5261164470300324b5c"
    },
    "nome": "2006 KL/A Track",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "5821f09d1164470300324b63"
    },
    "nome": "DL4030 Cream",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "5821f2781164470300324b65"
    },
    "nome": "DL4076 Display",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "5821f3401164470300324b67"
    },
    "nome": "DL3710 Pilot Inox",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "5821f4911164470300324b69"
    },
    "nome": "DL5040 Pentagle",
    "categoria": {
        "$oid": "57e0eb8eb0b11603005a1bf7"
    }
},
{
    "_id": {
        "$oid": "58247969de6dd4040049c3b4"
    },
    "nome": "2019 KL/Plus",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "58247983de6dd4040049c3b5"
    },
    "nome": "2012 KS/2",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "582968d4e423cf0400a8e4e4"
    },
    "nome": "0000 Stagna",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "5829a438ddea1004006e8b36"
    },
    "nome": "2917 Cristal Q",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "582de4725c365604000296e2"
    },
    "nome": "1001 Marquez Osram",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "5832fdbb104d0c04007da7ea"
    },
    "nome": "Disco",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "58331537e9983b040067c5ff"
    },
    "nome": "2004 KL/P",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "583bfb098957d2040071c175"
    },
    "nome": "2026 KL/1",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "583c40df8957d2040071c192"
    },
    "nome": "2011 KS/1",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "584542e550a2b304008c47a2"
    },
    "nome": "2013 KS/3",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "5847e3c50e147c0400fde026"
    },
    "nome": "2003 KS/P",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "5847e60d0e147c0400fde02a"
    },
    "nome": "2005 KS/A Track ADATTATORE DRIVER",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "5850255f0d89af040012e2a6"
    },
    "nome": "DL3050 DIVE",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "585025770d89af040012e2a9"
    },
    "nome": "DL3090 RUBIK",
    "categoria": {
        "$oid": "56b8ab022a376026658f22e4"
    }
},
{
    "_id": {
        "$oid": "58539e0316a72e0400c22cfe"
    },
    "nome": "PIADINERIA",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "5853b60816a72e0400c22d07"
    },
    "nome": "TRACK SPOTLIGHT",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "585bc1cf67d4880400ccbd57"
    },
    "nome": "0608 Bob Std Toshiba",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "585bc23767d4880400ccbd5c"
    },
    "nome": "0609 Bob Std Toshiba",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "585bd83e67d4880400ccbd65"
    },
    "nome": "0611 Bob Std Toshiba",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "585bd86567d4880400ccbd66"
    },
    "nome": "1608 Bob Std Sc Toshiba",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "585bd88267d4880400ccbd67"
    },
    "nome": "1609 Bob Std Sc Toshiba",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "58734cf8043a1d040098d694"
    },
    "nome": "2016 K/Plus",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "5874aa1ad7edac040031ba6f"
    },
    "nome": "2008 KL/220 Track",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "5887383286d21104003f2ac1"
    },
    "nome": "1154 MINI BELL",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "5889ce4e73206104009f0adf"
    },
    "nome": "PROIETTORE BINARIO",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "5889dd0673206104009f0af6"
    },
    "nome": "PROIETTORE",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "588b25024ef16604003fc3fb"
    },
    "nome": "INCASSO",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "58ab117677b72004004fc110"
    },
    "nome": "PARALUME",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "58ad5568c21d5604001181dc"
    },
    "nome": "SOSPENSIONE",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "58adbd8732512904001137e6"
    },
    "nome": "DL1811 Bob Eco",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "58b408ef076bbc0400698153"
    },
    "nome": "2916 Cristal",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "58d296a2ee2bbb04005a94cd"
    },
    "nome": "3210 Artik RQ",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "590c3904ffdcba0400f94d79"
    },
    "nome": "2022 KS1/P",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "5915c8b05c28050400609fd4"
    },
    "nome": "INCASSO ORIENTABILE",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "59354c7427945b040011c536"
    },
    "nome": "2007 KS/220 Track",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "593fdbd4180224040037f24b"
    },
    "nome": "1416 Ca Track",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "593fdd68180224040037f24c"
    },
    "nome": "4416 Ca",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "5943da6f09aa320400807a6e"
    },
    "nome": "2045 KXS BASETTA",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "594b8a0f21ef9f04004e4170"
    },
    "nome": "1335 Fly Midi/A",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "594bdcf5409e830400edaadb"
    },
    "nome": "2015 KL/220",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "594ceb98409e830400edab5e"
    },
    "nome": "4622 Fold Toshiba AR111",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "5954b14124365c0400892f53"
    },
    "nome": "2045 MACIG 48 KXS",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "595a489fd98eef0400d66ee0"
    },
    "nome": "1112 Ring 1200",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "595cdda2fe948d04007d8a75"
    },
    "nome": "1244 Over Led Maxi F112",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "59ae4d9b144c280400213f34"
    },
    "nome": "V0 Led Module",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd219"
    }
},
{
    "_id": {
        "$oid": "59ae5856144c280400213f57"
    },
    "nome": "V5 Emergency Led Module",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd219"
    }
},
{
    "_id": {
        "$oid": "59ae5ae3144c280400213f61"
    },
    "nome": "0530 Jet S",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "59ae6797144c280400213f92"
    },
    "nome": "0830 Jet M",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "59ae704a6ce53e0400a8f7f9"
    },
    "nome": "1233 Sun/Track L",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "59aea69fdd826e040027a418"
    },
    "nome": "TOSHIBA AR 111",
    "categoria": {
        "$oid": "56b8ab022a376026658f22e3"
    }
},
{
    "_id": {
        "$oid": "59aeac1ddd826e040027a42f"
    },
    "nome": "1294 Over Led Ps2 Maxi F112",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "59aeaee9dd826e040027a43d"
    },
    "nome": "1334 Fly Maxi F112",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "59aec583dd826e040027a477"
    },
    "nome": "1320 Viper Mini",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "59afc1c5dd826e040027a57b"
    },
    "nome": "1298 Cove Pro F112",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "59afc980d9bffb040079b45f"
    },
    "nome": "1299 Cove",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "59afed5ad9bffb040079b4d1"
    },
    "nome": "0595 Smart XS",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "59aff297d9bffb040079b4e4"
    },
    "nome": "0596 Smart S",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "59b000f3d9bffb040079b514"
    },
    "nome": "0597 Smart M",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "59b00ad6d9bffb040079b531"
    },
    "nome": "0598 Smart L",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "59b013fbd9bffb040079b550"
    },
    "nome": "2660 Bat S",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "59b0185fd9bffb040079b55e"
    },
    "nome": "2770 Bat M",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "59b01a41d9bffb040079b570"
    },
    "nome": "2880 Bat L",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "59b117e9d9bffb040079b665"
    },
    "nome": "0855 Flat Led",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "59b1216bd9bffb040079b69b"
    },
    "nome": "0814 Pin",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "59b1232de889e40400ced0c0"
    },
    "nome": "Led Panel 60x60",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21c"
    }
},
{
    "_id": {
        "$oid": "59b138ece889e40400ced0ed"
    },
    "nome": "0900 Sun S",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "59b140a3e889e40400ced105"
    },
    "nome": "1230 Sun M",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "59b14365e889e40400ced116"
    },
    "nome": "1630 Sun L",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "59b153c2e889e40400ced130"
    },
    "nome": "1106 Ring 600",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "59b15503e889e40400ced134"
    },
    "nome": "1109 Ring 900",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "59b26ed2e889e40400ced241"
    },
    "nome": "2918 Cristal 2",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "59b28fda5d5c9e040071fd3e"
    },
    "nome": "3265 Artik",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21d"
    }
},
{
    "_id": {
        "$oid": "59b7ef7b22960404003a2157"
    },
    "nome": "4060 Moon 600",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "59b7f0d222960404003a215f"
    },
    "nome": "4090 Moon 900",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "59b7f1ba22960404003a2169"
    },
    "nome": "4099 Moon 1200",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "59b9208ed5eaa0040053d67c"
    },
    "nome": "2029 KXL Track",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "59b920f4d5eaa0040053d680"
    },
    "nome": "2030 KXL/P",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "59b92161d5eaa0040053d681"
    },
    "nome": "2031 KXL/A Track",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21a"
    }
},
{
    "_id": {
        "$oid": "59b921b6d5eaa0040053d685"
    },
    "nome": "2009 KS/220 P",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "59b92208d5eaa0040053d686"
    },
    "nome": "2010 KS/220",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "59b92245d5eaa0040053d687"
    },
    "nome": "2014 KL/220 P",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "59b922a2d5eaa0040053d68a"
    },
    "nome": "2017 KS/Plus P",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "59b922fed5eaa0040053d68c"
    },
    "nome": "2018 KS/Plus",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "59b9233fd5eaa0040053d68d"
    },
    "nome": "2020 KL/Plus P",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "59b92364d5eaa0040053d68e"
    },
    "nome": "2021 KL/Plus",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd220"
    }
},
{
    "_id": {
        "$oid": "59b923dcd5eaa0040053d690"
    },
    "nome": "2023 KS2/P",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
},
{
    "_id": {
        "$oid": "59b923f8d5eaa0040053d691"
    },
    "nome": "2024 KS3/P",
    "categoria": {
        "$oid": "56b8aaa80de23c95600bd21e"
    }
}]



_managePrezzo = function(data){
    var prezzo = data.prezzo1

    if (prezzo == ''){
        prezzo = data.prezzo2
    }
    if (prezzo == ''){
        prezzo = data.prezzo3
    }
    if (prezzo == ''){
        prezzo = data.prezzo4
    }
    prezzo = prezzo.replace("€", "")

    prezzo = parseFloat(prezzo);

    return prezzo;
}

_manageAdattatore  = function(data){
    if (Boolean(data.ADATTORE_UNIVERSALE_A_3_FASI))
        return "Adattatore universale a 3 fasi"
    else
        return ""
}



_manageCategoriaModello = function(modelloNome){
    
    console.log("modello trovato nel file", modelloNome)


    modello = _.filter(modelli, function (m) {
        return m.nome.indexOf(modelloNome.substring(0, 4))!=-1
    })[0];

    categoria = _.filter(categorie, function (c) {

        return c._id.$oid === modello.categoria.$oid
    })[0]; 

}
 

_manageSottocategoria = function(data){

    
    var sottocategoria = data.sottocat

    if (sottocategoria != ''){

        if (sottocategoria == 'ADV'){
            return 'Advanced';
        }
        else  if (sottocategoria == 'FASH'){
            return 'Fashion';
        }
        else return 'Art'
    } 
}



var CONFIG = require(process.cwd() + '/server/config.js'),
     _ = require('underscore'),
    csv = require("fast-csv");
    fs = require('fs'),
    aggiornati = 0, 
    csvpath = "scripts/inserimenti/daFile/K1.csv",
    prodotti = [],
    stream = fs.createReadStream(csvpath); 

    var modello = {};
    var categoria = {};

     csv.fromStream(stream, {
            headers: true,
            delimiter: ',',
            discardUnmappedColumns: true
        }) 
        //RECUPERO I DATI DAL FILE
        .on("data", function (data) {  

                _manageCategoriaModello(data.codice); 
 
             
                var record = {
                    codice      : data.codice,
                    lm          : data.Fl_Lum_Nom,
                    generazione :data.Generazione,
                    macadam:data.MacAdam,
                    lunghezza:data.Lungh,
                    prezzo:_managePrezzo(data),
                    w : data.potenza,
                    colore:data.COLORE,
                    verniciatura: data.Vern,
                    corpo_it : data.Costr_Corpo_IT,
                    linea:"ilmas",
                    k:data.Temp_Col,
                    ma:data.corrente,
                    moduloled:data.MOD_LED,
                    corpo_en:data.Costr_Corpo_EN,
                    destinazione:"indoor",
                    fascio:data.FASCIO,
                    ottica_it:data.Costr_Ottica_IT, 

                    modello: modello.nome,
                    categoria:categoria.nome,

                    modello_id:modello._id.$oid,
                    categoria_id:categoria._id.$oid,

                    v:data.Tensione,
                    tmax:data.Temp_Amb_Max,
                    ottica_en:data.costr_Ottica_EN,
                    hz:data.Freq,
                    alimentatore:data.alimentatore,
                    irc:data.in_resa_crom,
                    sottocategoria:_manageSottocategoria(data),
                    dimmerabile : Boolean(data.Dimm),
                    descrizione_applicazione : data.Descr_Appl,
                    rischio_fotobiologico : data.Risch_Fotob,
                    filo_incandescente : data.Filo_inc,
                    durata : data.durata,
                    orientabile : data.orien,
                    classe_isolamento : data.Cl_Isolamento,
                    ip : data.Grado_IP,
                    vita_utile_desc : data.Dec_Fl,
                    inclinabile : data.Incl,
                    foro : data.foro,
                    satinato : Boolean(data.sat),
                    alimentatore_incluso : Boolean(data.alim_Incl),
                    divieto_controsoffitti : Boolean(data.div_Inst_contr_Is),
                    ce : true,
                    f : true,
                    pubblicato : true,
                    exportMetel : true ,
                    les : data.LES,
                    adattatore_it: _manageAdattatore(data)
                }

                
               
                var prodotto = _.clone(record);
                prodotti.push(prodotto)
             
        })
        .on("end", 
            function () {
                 var process = require('../../../scripts/inserimenti/daFile/updateGeneric.js');
                 process.execute(prodotti);
            }
        ) 


