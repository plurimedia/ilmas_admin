
var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    Utils = require('../../../server/routes/utils'),
    ProdottoSchema = require('../../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),

    ModelloSchema = require('../../../server/models/modello'),
    Modello = mongoose.model('Modello'),

     csv = require("fast-csv");
    fs = require('fs'),
    ObjectId = require('mongodb').ObjectID,
    logGlobale="",
    trascodifica = "20170324_01";
    aggiornati = 0;


exports.execute = function (prodotti) { 
    console.log(db)

mongoose.connect(db);
 

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch.");

    var bulk = Prodotto.collection.initializeOrderedBulkOp();
    async.each(prodotti, 
        function (prodotto, cb) {



            Modello.findOne({_id:prodotto.modello_id}).lean()
            .exec(

                function(err,modelloTrovato){

                    if (modelloTrovato){

                        console.log(" cancello : ", prodotto.codice)

                        bulk.find( { codice: prodotto.codice } ).remove(); //elimina il record prima di inserirlo

                    	var newProdotto = _.clone(prodotto)

                     	newProdotto.mdate = new Date();

                     	newProdotto.keywords = Utils.keyWords(prodotto); 

                     	newProdotto.modello =  modelloTrovato._id

                        newProdotto.categoria =  modelloTrovato.categoria
                 
                        delete newProdotto.modello_id

                        delete newProdotto.categoria_id

                     	bulk.insert(newProdotto);
            				
                        console.log("inserisco ",  newProdotto.codice);
            			
                    }
                    else{
                        console.log(err)
                    }

                    aggiornati ++;

                    cb();
                }
            )
             
        }, 
        function (err) 
        {
        	if(aggiornati > 0) {
                 bulk.execute(
                    function(err, result){
                    if(err)
                        throw err;

                    	console.log(logGlobale)

                        console.log('finito, chiudo la connessione, creati ' + aggiornati + ' nuovi codici')
                        mongoose.connection.close();

                        fs.writeFile(".tmp/f_050.txt",

                         logGlobale,

                         function(err) {
                            if(err) {
                                console.log("Log was not saved!", err);
                                throw err;
                            }
                            else
                                console.log("Log was saved!");
                        }
                    );

                    }
                )
            }
            else {
                mongoose.connection.close();
                console.log("connection close");
            }
        }
    )
    
}



);
}
