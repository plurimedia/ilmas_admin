/*
LANCIARE LA PROCEDURA
node --max-old-space-size=2000 scripts/inserimenti/daFile/viPlus.js;
*/

var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    Utils = require('../../../server/routes/utils'),
    ProdottoSchema = require('../../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
     ModelloSchema = require('../../../server/models/modello'),
    Modello = mongoose.model('Modello'),
     csv = require("fast-csv");
    fs = require('fs'),
    ObjectId = require('mongodb').ObjectID,
    logGlobale="",
    trascodifica = "20170324_01";
    aggiornati = 0;


exports.execute = function (prodotti) { 
    console.log(db)

mongoose.connect(db);
 

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch.");

    var bulk = Prodotto.collection.initializeOrderedBulkOp();
    async.each(prodotti, 
        function (prodotto, cb) {

            var cloneP = _.clone(prodotto);

            Modello.findOne({_id:cloneP.modello_id}).lean()
            .exec(

                function(err,modelloTrovato){

                    if (modelloTrovato){

                        var modelloP = _.clone(modelloTrovato);  

                        console.log(aggiornati, prodotto.codice, modelloP._id, modelloP.nome);
      

                        var update = {};
                        update.$set = {}; 
                        update.$set.prezzo=prodotto.prezzo; 

                        update.$set.modello=modelloP._id
                        update.$set.categoria=modelloP.categoria

                        bulk.find( { codice: cloneP.codice } ).updateOne(update);

                    

                    }
                    else{
                        console.log(err)
                    }

                    aggiornati ++;

                    cb();
                }
            )

        	
             
        }, 
        function (err) 
        {
        	if(aggiornati > 0) {
                 bulk.execute(
                    function(err, result){
                    if(err)
                        throw err; 
                        console.log('finito, chiudo la connessione, creati ' + aggiornati + ' nuovi codici')
                        mongoose.connection.close();

                        fs.writeFile(".tmp/f_050.txt",

                         logGlobale,

                         function(err) {
                            if(err) {
                                console.log("Log was not saved!", err);
                                throw err;
                            }
                            else
                                console.log("Log was saved!");
                        }
                    );

                    }
                )
            }
            else {
                mongoose.connection.close();
                console.log("connection close");
            }
        }
    )
    
}



);
}
