var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd269';
var query = {modello:modello };
var tag = 'dani_20171004_05';

var transform1 = {
    tag : tag,
    query: query,
    preProcess : 'backup',
    //postProcess : 'restore',
    filter: [process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:['1F','2F','3F'], pos:5},{len:15}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var upd = {};

                upd.moduloled = 'Arled Osram';
                upd.sottocategoria = 'Arled Osram';
                upd.durata = '30.000';
                upd.prezzo = 280;
                upd.w = "2x38";
                upd.alimentatore_incluso = true;
                upd.alimentatore = 'Driver standard';

                if (process.filters.hasCharAt(p, {chars:["1F"], pos:5})) {
                    upd.k = '2700';
                    upd.lm = '6000';
                }
                else if (process.filters.hasCharAt(p, {chars:["2F"], pos:5})) {
                    upd.k = '3000';
                    upd.lm = '6600';
                }
                else if (process.filters.hasCharAt(p, {chars:["3F"], pos:5})) {
                    upd.k = '4000';
                    upd.lm = '7200';
                }

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return upd;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:['1F','2F','3F'], pos:5},{len:15}],
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);
                p.prezzo = 376;

                p.insertRule = 2;

                console.log('nella seconda funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: {modello:modello, insertRule:2, insertTag:tag },
    filter: [process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:['1F','2F','3F'], pos:5},{len:15}],
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);
                p.prezzo = 376;

                delete p.insertRule;

                console.log('nella terza funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

