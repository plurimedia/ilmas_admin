var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd234';
var query = {modello:modello, generazione: {$in: ['G6', 'D1']} };
var tag = 'dani_20170922_02';

var transform1 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:['D'], pos:9},
    checkDuplicates:true,
    test:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.push_memory(p);

                switch (p.prezzo) {
                    case 193 :
                        p.prezzo = 233;
                        break;
                    case 213 :
                        p.prezzo = 253;
                        break;
                    case 228 :
                        p.prezzo = 268;
                        break;
                }

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

