var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    q = require('q'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    Modello = mongoose.model('Modello'),
    Categoria = mongoose.model('Categoria'),
    Utils = require('../../server/routes/utils'),
    aggiornati = 0;


function _filter (p, t) {
    var trovato = false;

    if (!t.filter) return true;

    if (_.isArray(t.filter)) {

        //console.log('[massive.js] filtri multipli');

        for (var idx = 0; idx < t.filter.length; idx++) {

            var filt = t.filter[idx];
            trovato = filt(p, t.filterArguments[idx]);
            console.log('[DEBUG] '+ idx + ' ' + trovato + ' ' + p.codice);
            if (!trovato) {
                //console.log('[massive.js] trovato globale '+ trovato);
                return trovato;
            }

        }
    } else {

        if (t.filter(p, t.filterArguments)) {
            trovato = true;
        }
    }

    //console.log('[massive.js] trovato globale '+ trovato);
    return trovato;
}

/*
var transform1 = {
    tag : tag per riconoscere i prodotti creati con questa trasformazione,
    query: query di estrazione prodotti,
    test: se eseguire l'operazione sul db o dare solo i log informativi',
    checkDuplicates: se true quando trova duplicati, li lascia e non fa operazioni su quel prodotto,
    filter:filtro per filtrare i prodotti, ce ne sono in fondo a questo file. accetta anche array,
    filterArguments:argomenti per il filtro, vedere le funzioni in fondo al file se filter è un array, anche questo deve esserlo
    transform: [
        {
            action: per ora puo valere nuovo_inserimento e modifica_esistente (insert o update),
            replace_rule: funzione di trasformazione. Deve ritornare il prodotto in caso di inserimento, l'oggetto update in caso di update
            }
        }
    ]
};
*/




exports.execute = function (t) {
    var deferred = q.defer();

    var query = _.extend(t.query, {
                            kit: {$in: [false, null]}
                        },
                        {
                            criptato: {$in: [false, null]}
                        }
                    );
    aggiornati = 0;

    var findProdotti = function () {
        console.log("[massive.js] tento di connettermi"); /// this gets printed to console
        mongoose.connect(db);

        mongoose.connection.once('open', function () {

            console.log("[massive.js] Connessione aperta, parte il batch " + JSON.stringify(query)); /// this gets printed to console

            var last_categoria, last_modello, text_categoria, text_modello;

            Prodotto.find(query).sort({_id:1})//.lean()
                .exec(
                function (err, prodotti) {

                    var bulk = Prodotto.collection.initializeOrderedBulkOp();
                    var cb = function(t){console.log(t);};
                    var i = 0;
                    async.each(prodotti,
                        function (prodotto, cb) {
                            i++;
                            var findModello = function () {
                                var Mdeferred = q.defer();

                                if (_filter(prodotto, t)) {
                                    Modello.findOne({_id: prodotto.modello}).lean()
                                        .exec(function (err, modello) {
                                            if (err)
                                                console.log(err);
                                            else {
                                                //console.log('[massive.js] modello trovato ' + modello.nome);
                                                text_modello = modello.nome;

                                            }

                                            Mdeferred.resolve();
                                        });
                                }else Mdeferred.resolve();

                                return Mdeferred.promise;

                            }

                            var findCategoria = function () {
                                var Cdeferred = q.defer();

                                if (_filter(prodotto, t)) {
                                    Categoria.findOne({_id: prodotto.categoria}).lean()
                                        .exec(function (err, categoria) {
                                            if (err)
                                                console.log(err);
                                            else {
                                                //console.log('[massive.js] categoria trovata ' + categoria.nome);
                                                text_categoria = categoria.nome;
                                            }

                                            Cdeferred.resolve();

                                        });
                                }else Cdeferred.resolve();

                                return Cdeferred.promise;

                            }

                            var azione = function () {
                                var Adeferred = q.defer();

                                if (_filter(prodotto, t)) {

                                    var clone = _.clone(prodotto);

                                    t.transform.forEach(function (a) {
                                        if (a.action === 'nuovo_inserimento') {
                                            var old = clone.codice;
                                            clone = a.replace_rule(clone);
                                            console.log('dopo replace rule');

                                            //incollati qui da fuori
                                            delete clone._id;

                                            clone.mdate = new Date();

                                            clone.insertTag = t.tag;

                                            //pre keywords
                                            clone.modello = text_modello;
                                            clone.categoria = text_categoria;

                                            clone.keywords = Utils.keyWords(clone);

                                            //post keywords
                                            clone.modello = prodotto.modello;
                                            clone.categoria = prodotto.categoria;

                                            delete clone.last_user;
                                            console.log('dopo delete');

                                            /*if (t.checkDuplicates)
                                                Prodotto.findOne({codice:clone.codice}, {codice:1}).lean()
                                                    .exec(function(err,codiceTrovato){
                                                        if (err) console.log("[massive.js] errore recupero codice esistente ", old, clone.codice, err)

                                                        if (!codiceTrovato){

                                                            bulk.insert(clone);
                                                            aggiornati++;
                                                            Adeferred.resolve();

                                                        }
                                                        else{
                                                            console.log("[massive.js] codice esistente ", old, clone.codice)
                                                            aggiornati++;
                                                            Adeferred.resolve();
                                                        }

                                                    }


                                                );
                                            else {*/
                                            console.log('prima insert');
                                                //bulk.insert(clone);
                                            console.log('dopo insert');
                                                aggiornati++;
                                            console.log('risolvo Adeferred');
                                                Adeferred.resolve();
                                            //}

                                        } else if (a.action === 'modifica_esistente') {

                                            var update = {};
                                            update.$set = a.replace_rule(clone);
                                            console.log(JSON.stringify(update.$set));
                                            update.$set.mdate = new Date();
                                            update.$set.updateTag = t.tag;
                                            //console.log('[massive.js] nella modifica ' + JSON.stringify(update.$set));

                                            bulk.find({_id: clone._id}).updateOne(update);
                                            aggiornati++;
                                            Adeferred.resolve();

                                        }
                                    })

                                } else {
                                    Adeferred.resolve();
                                }

                                return Adeferred.promise;

                            }

                            var pre_azione = function () {
                                t.pre_replace(prodotto);
                            }

                            if (t.pre_replace) {
                                pre_azione();
                                findModello()
                                    .then(findCategoria)
                                    .then(azione)
                                    .then(function() { cb('cb 1'); });
                            } else {
                                findModello()
                                    .then(findCategoria)
                                    .then(azione)
                                    .then(function() { cb('cb 2 ' + i); });
                            }
                            console.log('fondo del loop');


                        },
                        function (err) {
                            console.log('inizio funzione finale');

                            if (err) {
                                console.log(err)
                                throw err;
                                deferred.reject();
                            }

                            if (aggiornati > 0 && !t.test) {

                                bulk.execute(function (err, result) {
                                    if (err) {
                                        console.log("[massive.js] Errore nella execute")
                                        throw err;
                                    }

                                    console.log('[massive.js] finito, chiudo la connessione: inseriti ' + aggiornati + ' prodotti per ' + JSON.stringify(query))
                                    mongoose.connection.close();
                                    deferred.resolve();

                                })

                            }
                            else {
                                console.log('[massive.js] Non ci sono prodotti da aggiornare')
                                mongoose.connection.close();
                                deferred.resolve();
                            }
                        }
                    )
                });
        });
    }

    findProdotti();

    return deferred.promise;

}

exports.filters = {};

exports.filters.hasCharAt = function (p, pars) { //prodotto, oggetto parametro {chars:[], pos:1}
    var trovato = false;
    //console.log('[DEBUG FUNZIONE] '+ p.codice);
    _.each(pars.chars, function (el) {
        //console.log('[DEBUG FUNZIONE] '+ p.codice + " " + el + " " + p.codice.indexOf(el) + " " + (pars.pos-1));
        if (el.length > 1) {
            if (p.codice.indexOf(el) === pars.pos - 1)//voglio in indice 1
                trovato = true;
        } else {
            if(p.codice.substring(pars.pos - 1, pars.pos) === el)
                trovato = true;
        }
    });

    return trovato;
}

exports.filters.hasSottoCategoria = function (p, pars) { //prodotto, oggetto parametro {sottocategoria:''} CASE INSENSITIVE!!!!!!
    if (!p.sottocategoria) return false;

    if (p.sottocategoria.toLowerCase() === pars.sottocategoria.toLowerCase())
        return true;


    return false;
}

exports.filters.hasLunghezzaCodiceMax = function (p, pars) { //prodotto, oggetto parametro {len:8}
    if (p.codice.length <= pars.len)
        return true;


    return false;
}

exports.replaceCharAt = function (s, c, p) {
    return s.substring(0, p-1) + c + s.substring(p,20);//replace del settimo carattere
}

exports.dimmerabile = function (p) {
    p.codice = exports.replaceCharAt(p.codice, 'D', 9);
    p.alimentatore = 'Driver dimmerabile';
    p.dimmerabile = true;
    return p;
}

exports.dali = function (p) {
    p.codice = exports.replaceCharAt(p.codice, 'L', 9);
    p.alimentatore = 'Driver dali';
    return p;
}

exports.grigio = function (p) {
    p.codice = exports.replaceCharAt(p.codice, '0', 8);
    p.colore = 'Grigio';
    return p;
}

exports.nero = function (p) {
    p.codice = exports.replaceCharAt(p.codice, '2', 8);
    p.colore = 'Nero';
    return p;
}
