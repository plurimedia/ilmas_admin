var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd353';
var query = {modello:modello, generazione: { $in: ['G6', 'D1']} };
var tag = 'dani_20170922_13';

var transform1 = {
    tag : tag,
    query: query,
    filter: process.filters.hasLunghezzaCodiceMax,
    filterArguments : {len:9},
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'C', 6)
                p.w = 47;
                p.ma = 1200;

                switch (p.lm) {
                    case '1705' :
                        p.lm = '2360';
                        break;
                    case '1790' :
                        p.lm = '2480';
                        break;
                    case '1880' :
                        p.lm = '2600';
                        break;
                    case '1460' :
                        p.lm = '2020';
                        break;
                    case '1535' :
                        p.lm = '2125';
                        break;
                    case '1610' :
                        p.lm = '2235';
                        break;
                    case '1530' :
                        p.lm = '1935';
                        break;
                    case '1450' :
                        p.lm = '1835';
                        break;
                }

                if (p.generazione === 'G6') {
                    p.ma = 450;
                    p.w = 15;
                } else if (p.generazione === 'D1') {
                    p.ma = 500;
                    p.w = 17;
                }

                p.distintabase = 'china_1';

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

