var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd2c6';
var query = {modello:modello, generazione: 'G6' };
var tag = 'dani_20170922_23';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:['E'], pos:6},{len:9}],
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'S', 6)
                p.w = 29;
                p.ma = 800;

                switch (p.lm) {
                    case '4550' :
                        p.lm = '4100';
                        break;
                    case '4660' :
                        p.lm = '4210';
                        break;
                    case '4870' :
                        p.lm = '4390';
                        break;
                    case '3860' :
                        p.lm = '3480';
                        break;
                    case '3955' :
                        p.lm = '3565';
                        break;
                    case '4240' :
                        p.lm = '3820';
                        break;
                    case '3640' :
                        p.lm = '3280';
                        break;
                    case '3440' :
                        p.lm = '3100';
                        break;
                    case '2450' :
                        p.lm = '2210';
                        break;
                    case '2490' :
                        p.lm = '2245';
                        break;
                    case '3715' :
                        p.lm = '3350';
                        break;
                    case '1600' :
                        p.lm = '2350';
                        break;
                }

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

