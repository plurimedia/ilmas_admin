var process = require('./massive.js');
var modello = '59b26ed2e889e40400ced241';
var query = { $and: [{modello:modello},{$or: [{moduloled: { $exists: false }},{ moduloled: ''}]} ]};
var tag = 'dani_20170914_06';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'J', 7);

                p.fascio = '33';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["G"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'R', 7);

                p.fascio = '50';

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = p.codice + 'E';

                if (p.sottocategoria.toLowerCase() != 'fashion') {
                    switch (p.prezzo) {
                        case 121 :
                            p.prezzo = 141;
                            break;
                        case 156 :
                            p.prezzo = 176;
                            break;
                    }
                    ;
                }else {
                    p.prezzo = 161;
                }

                p.alimentatore = 'Driver standard';
                p.alimentatore_incluso = true;

                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["E"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 9);

                switch (p.prezzo) {
                    case 141 :
                        p.prezzo = 181;
                        break;
                    case 161 :
                        p.prezzo = 201;
                        break;
                    case 176 :
                        p.prezzo = 216;
                        break;
                };

                p.alimentatore = 'Driver dimmerabile';
                p.dimmerabile = true;

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'L', 9);

                switch (p.prezzo) {
                    case 181 :
                        p.prezzo = 188;
                        break;
                    case 201 :
                        p.prezzo = 208;
                        break;
                    case 216 :
                        p.prezzo = 223;
                        break;
                };

                p.alimentatore = 'Driver dali';

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform6 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'C', 6);

                switch (p.lm) {
                    case '1920' :
                        p.lm = '2675';
                        break;
                    case '1970' :
                        p.lm = '2745';
                        break;
                    case '2050' :
                        p.lm = '2860';
                        break;
                    case '1625' :
                        p.lm = '2270';
                        break;
                    case '1665' :
                        p.lm = '2325';
                        break;
                    case '1785' :
                        p.lm = '2490';
                        break;
                    case '1530' :
                        p.lm = '2140';
                        break;
                    case '1450' :
                        p.lm = '2020';
                        break;
                }

                p.w = 17;
                p.ma = 500;

                console.log('nella sesta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform7 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["B"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                var old = p.codice;
                p.codice = process.replaceCharAt(p.codice, 'D', 6);

                if (p.sottocategoria.toLowerCase() != 'fashion' && p.sottocategoria.toLowerCase() != 'art') {
                    switch (p.lm) {
                        case '2360' :
                            p.lm = '3170';
                            break;
                        case '3170' :
                            p.lm = '3330';
                            break;
                        case '2600' :
                            p.lm = '3500';
                            break;
                        case '2020':
                            p.lm = '2710';
                            break;
                        case '2125' :
                            p.lm = '2860';
                            break;
                        case '2235' :
                            p.lm = '3000';
                            break;
                        case '2140' :
                            p.lm = '2910';
                            break;
                    }
                }else if (p.sottocategoria.toLowerCase() == 'fashion'){
                    switch (p.lm) {
                        case '2020' :
                            p.lm = '2750';
                            break;
                    }
                }else if (p.sottocategoria.toLowerCase() == 'art'){
                    switch (p.lm) {
                        case '2020' :
                            p.lm = '2750';
                            break;
                    }
                }

                switch (p.prezzo) {
                    case 125 :
                        p.prezzo = 134;
                        break;
                    case 145 :
                        p.prezzo = 154;
                        break;
                    case 160 :
                        p.prezzo = 169;
                        break;
                };

                p.w = 24;
                p.ma = 700;

                console.log('nella settima funzione di trasformazione '+ old + " " + p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

