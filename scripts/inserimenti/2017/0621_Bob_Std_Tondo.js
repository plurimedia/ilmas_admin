var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd281';
var query = {modello:modello, generazione: 'G6'};
var tag = 'dani_20170922_14';

var transform1 = {
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["1"], pos:8},
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.bianco(p);

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

