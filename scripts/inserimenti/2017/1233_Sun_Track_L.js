var process = require('./massive.js');
var modello = '59ae704a6ce53e0400a8f7f9';
var query = { modello : modello };
var tag = 'dani_20170911_01';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, '2', 8);
                p.colore = 'Nero';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["1"], pos:8},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, '0', 8);
                p.colore = 'Grigio';

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["E"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'L', 7);
                p.fascio = '35';

                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["E"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'R', 7);
                p.fascio = '50';

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = p.codice + 'D';
                switch (p.prezzo) {
                    case 157 :
                        p.prezzo = 217;
                        break;
                    case 177 :
                        p.prezzo = 237;
                        break;
                    case 192 :
                        p.prezzo = 252;
                        break;
                };
                p.alimentatore = 'Driver dimmerabile';
                p.dimmerabile = true;

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform6 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'L', 9);
                switch (p.prezzo) {
                    case 217 :
                        p.prezzo = 232;
                        break;
                    case 237 :
                        p.prezzo = 252;
                        break;
                    case 252 :
                        p.prezzo = 267;
                        break;
                };
                p.alimentatore = 'Driver dali';

                console.log('nella sesta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*


 1. Colore nero: parti da tutti i codici con i primi 4 caratteri 1233, li duplichi:
 8° carattere 1  2
 Colore: Nero
 2. Colore grigio : parti da tutti i codici con i primi 4 caratteri 1233 e 8° carattere 1, li duplichi:
 8° carattere 1  0
 Colore: Grigio
 3. Ottiche 35°: parti da tutti i codici con i primi 4 caratteri 1233, li duplichi:
 7° carattere E  L
 Fascio: 35
 4. Ottiche 50°: parti da tutti i codici con i primi 4 caratteri 1233 e 7° carattere E, li duplichi:
 7° carattere E  R
 Fascio: 50
 5. Versione Dimmerabile: parti da tutti i codici con i primi 4 caratteri 1233, li duplichi:
 Aggiungo il 9° carattere : D
 Prezzo: 157217
 177237
 192252
 Alimentatore: Driver dimmerabile
 Dimmerabile: X (flag)
 6. Versione Dali: parti da tutti i codici con i primi 4 caratteri 1233 e 9° carattere D, li duplichi:
 9° carattere : DL
 Prezzo: 217232
 237252
 252267 ( prezzo sottocategoria ART e 9° carattere L)
 Alimentatore: Driver dali
 */