/* 
    lanciare con: 
	node --max-old-space-size=2000 scripts/inserimenti/delete1332.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    ObjectId = require('mongodb').ObjectID,
	aggiornati = 0;

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console
    console.log(db)

    var query =
            { 
	             modello:ObjectId('56b8aaa90de23c95600bd236'),
	    		     generazione: {$in: ['G6']},
                       kit :{"$in":[false,null]},
                criptato :{"$in":[false,null]},
             }

    console.log("query", query);

    Prodotto.find(query).lean()
    .exec(

    	function(err,prodotti){
        
        console.log('lunghezza '+prodotti.length+' prodotti')
        
  		  mongoose.connection.close();

  		  console.log('connection close') 

    		//cancelliamo  solo i record di colore grigio i DIM e DALI
            prodotti = _.filter(prodotti, function(p){
              
                return (p.codice.substring(7,8) == '0'  ||
                p.codice.substring(8,9) == 'D' || p.codice.substring(8,9) == 'L'   || p.codice.substring(8,9) == 'P'
                &&
               ( p.codice.length==8 || p.codice.length==9))

            })
  		 
  		  var process = require('../../scripts/inserimenti/deleteGeneric.js');
        process.execute(prodotti);
        
    })
});


