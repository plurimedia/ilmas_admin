var process = require('./massive.js');
var ObjectId = require('mongodb').ObjectID;
var modello = '59b9208ed5eaa0040053d67c';
var query = {modello:ObjectId(modello) };
var tag = 'dani_20171010_01';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);
                switch (p.prezzo) {
                    case 196 :
                        p.prezzo = 256;
                        break;
                    case 216 :
                        p.prezzo = 276;
                        break;
                    case 231 :
                        p.prezzo = 291;
                        break;
                }

                p.insertRule = 1;

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: {modello:modello, insertRule:1, insertTag:tag },
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.push_memory(p);
                switch (p.prezzo) {
                    case 256 :
                        p.prezzo = 296;
                        break;
                    case 276 :
                        p.prezzo = 316;
                        break;
                    case 291 :
                        p.prezzo = 331;
                        break;
                }

                delete p.insertRule;

                console.log('nella seconda funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

