var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd252';
var query = {modello:modello, generazione: 'G4'};
var tag = 'dani_20170918_04';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasSottoCategoria,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{sottocategoria:'fashion'},{len:11}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                var old = p.codice;
                p.codice = p.codice.replace('-G4', '');

                p.generazione = 'G6';

                switch (p.lm) {
                    case '1250' :
                        p.lm = '1530';
                        break;
                    case '1800' :
                        p.lm = '1935';
                        break;
                }
                p.les = 'LES 19';
                switch (p.ma) {
                    case '350' :
                        p.ma = '350';
                        break;
                    case '500' :
                        p.ma = '450';
                        break;
                }
                switch (p.w.toLowerCase()) {
                    case '1x12' :
                        p.w = '12';
                        break;
                    case '1x17' :
                        p.w = '15';
                        break;
                }
                p.prezzo = 128;

                console.log('nella prima funzione di trasformazione ' + old + " " + p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: {modello:modello, generazione: 'G6', insertTag: {$exists:true}},//quelli del punto 1
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'E', 9);
                p.alimentatore = 'Driver standard';
                p.alimentatore_incluso = true;
                p.prezzo = 148;

                p.insertRule = 2;

                console.log('nella seconda funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: {modello:modello, generazione: 'G6', insertTag: {$exists:true} , insertRule:2},//quelli del punto 2
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);
                p.prezzo = 188;

                p.insertRule = 3;

                console.log('nella terza funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: {modello:modello, generazione: 'G6', insertRule:3},//quelli del punto 3
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);
                p.prezzo = 195;

                console.log('nella quarta funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: {modello:modello, generazione: 'G6'},
    filter: process.filters.hasSottoCategoria,
    filterArguments : {sottocategoria:'ART'},
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var upd = {};
                upd.pre_update = {ma: p.ma, w: p.w, lm: p.lm };
                upd.ma = 450;
                upd.w = 15;
                upd.lm = '1835';

                console.log('nella quinta funzione di trasformazione ' + p.codice);
                return upd;
            }
        }
    ]
};
process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

