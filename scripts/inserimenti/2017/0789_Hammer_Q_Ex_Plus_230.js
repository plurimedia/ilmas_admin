var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd24c';
var query = {modello:modello, generazione: 'G6' };
var tag = 'dani_20170922_12';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasCharAt, process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:['F'], pos:6}, {len:9}],
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'G', 6)
                p.w = 47;
                p.ma = 1200;

                switch (p.lm) {
                    case '5595' :
                        p.lm = '6290';
                        break;
                    case '5830' :
                        p.lm = '6560';
                        break;
                    case '4625' :
                        p.lm = '5200';
                        break;
                    case '4740' :
                        p.lm = '5330';
                        break;
                    case '5080' :
                        p.lm = '5710';
                        break;
                    case '4360' :
                        p.lm = '4900';
                        break;
                }

                //console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasCharAt, process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:['UH'], pos:5}, {len:9}],
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'F', 5)
                p.k = 2700;
                p.lm = 5945;

                console.log('nella seconda funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

