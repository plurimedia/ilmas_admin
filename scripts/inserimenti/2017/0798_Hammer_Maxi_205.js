var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd245';
var query = {modello:modello, generazione: 'G6' };
var tag = 'dani_20170922_06';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasCharAt, process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:['D'], pos:6}, {len:9}],
//    test:true,
//    logLevel:2,
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'F', 6)
                p.w = 33;
                p.ma = 900;

                switch (p.lm) {
                    case '3640' :
                        p.lm = '5200';
                        break;
                    case '3735' :
                        p.lm = '5340';
                        break;
                    case '3890' :
                        p.lm = '5560';
                        break;
                    case '3085' :
                        p.lm = '4410';
                        break;
                    case '3160' :
                        p.lm = '4520';
                        break;
                    case '3390' :
                        p.lm = '4840';
                        break;
                    case '2910' :
                        p.lm = '4160';
                        break;
                    case '2750' :
                        p.lm = '3930';
                        break;
                    case '1960' :
                        p.lm = '2800';
                        break;
                    case '1990' :
                        p.lm = '2850';
                        break;
                    case '2970' :
                        p.lm = '4250';
                        break;
                    case '2090' :
                        p.lm = '2980';
                        break;
                }

                switch (p.prezzo) {
                    case 126 :
                        p.prezzo = 136;
                        break;
                    case 157 :
                        p.prezzo = 184;
                        break;
                    case 164 :
                        p.prezzo = 184;
                        break;
                    case 146 :
                        p.prezzo = 156;
                        break;
                    case 177 :
                        p.prezzo = 204;
                        break;
                    case 184 :
                        p.prezzo = 204;
                        break;
                    case 161 :
                        p.prezzo = 171;
                        break;
                    case 192 :
                        p.prezzo = 219;
                        break;
                    case 199 :
                        p.prezzo = 219;
                        break;
                };

                //console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

