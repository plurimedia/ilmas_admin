var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd241';
var query = {modello:modello, generazione: 'G6', moduloled:'F111 Led Module'};
var tag = 'dani_20170920_08';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);

                switch (p.prezzo) {
                    case 215 :
                        p.prezzo = 275;
                        break;
                    case 224 :
                        p.prezzo = 284;
                        break;
                    case 235 :
                        p.prezzo = 295;
                        break;
                    case 244 :
                        p.prezzo = 304;
                        break;
                    case 250 :
                        p.prezzo = 310;
                        break;
                    case 259 :
                        p.prezzo = 319;
                        break;
                }

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    //filter: process.filters.hasLunghezzaCodiceMax,
    //filterArguments : {len:8},
    query: {modello:modello, generazione: 'G6', insertTag: tag},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);

                switch (p.prezzo) {
                    case 275 :
                        p.prezzo = 290;
                        break;
                    case 284 :
                        p.prezzo = 299;
                        break;
                    case 295 :
                        p.prezzo = 310;
                        break;
                    case 304 :
                        p.prezzo = 319;
                        break;
                    case 310 :
                        p.prezzo = 325;
                        break;
                    case 319 :
                        p.prezzo = 334;
                        break;
                }

                console.log('nella seconda funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

