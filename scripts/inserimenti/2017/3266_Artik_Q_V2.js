var process = require('./massive.js');
var modello = '5813730d4f8ebc0300f82859';
var query = {modello:modello, moduloled:'V2 Led Module'};
var tag = 'dani_20170915_01';

var transform1 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["1BE", "2BE", "3BE", "1BN",  "2BN", "3BN", "1BU", "2BU", "3BU"], pos:5},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = p.codice + 'E';

                p.prezzo = 54;
                p.alimentatore = 'Driver standard';
                p.alimentatore_incluso = true;

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: {modello:modello, moduloled:'V2 Led Module'},
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["E"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 9);

                p.prezzo = 94;
                p.alimentatore = 'Driver dimmerabile';
                p.dimmerabile = true;

                console.log('nella seconda funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: {modello:modello, moduloled:'V2 Led Module'},
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'L', 9);

                p.prezzo = 101;
                p.alimentatore = 'Driver dali';

                console.log('nella terza funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

