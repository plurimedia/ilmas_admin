var process = require('./massive.js');
var modello = '5784ca8ac98fef0300305938';
var query = {modello:modello, generazione: 'G6' };
var tag = 'dani_20170922_10';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasCharAt, process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:['D'], pos:6}, {len:9}],
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'F', 6)
                p.w = 39;
                p.ma = 1050;

                switch (p.lm) {
                    case '3640' :
                        p.lm = '5200';
                        break;
                    case '3735' :
                        p.lm = '5340';
                        break;
                    case '3890' :
                        p.lm = '5560';
                        break;
                    case '3085' :
                        p.lm = '4410';
                        break;
                    case '3160' :
                        p.lm = '4520';
                        break;
                    case '3390' :
                        p.lm = '4840';
                        break;
                    case '2910' :
                        p.lm = '4160';
                        break;
                    case '2750' :
                        p.lm = '3930';
                        break;
                }

                switch (p.prezzo) {
                    case 147 :
                        p.prezzo = 157;
                        break;
                    case 178 :
                        p.prezzo = 205;
                        break;
                    case 185 :
                        p.prezzo = 205;
                        break;
                    case 167 :
                        p.prezzo = 177;
                        break;
                    case 198 :
                        p.prezzo = 225;
                        break;
                    case 205 :
                        p.prezzo = 225;
                        break;
                    case 182 :
                        p.prezzo = 192;
                        break;
                    case 213 :
                        p.prezzo = 240;
                        break;
                    case 220 :
                        p.prezzo = 240;
                        break;
                };

                //console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

