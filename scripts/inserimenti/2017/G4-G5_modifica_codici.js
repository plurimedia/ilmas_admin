var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd234';
var query = {modello:modello, generazione: 'G4'};
var tag = 'dani_20170929_01';

var transform1 = {
    tag : tag,
    query: {generazione: 'G4', codice:{$not: /G4$/}},
    filter: [process.filters.hasNotCharAt,process.filters.hasNotCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:["-G4"], pos:8},{chars:["-G4"], pos:9},{len:9}],
    //test:true,
    checkDuplicates:true,
    logLevel:1,
    limit:5000,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var old = p.codice;

                var upd = {};
                upd.codice = p.codice + '-G4';

                //console.log('nella prima funzione di trasformazione ',old,p.codice);
                return upd;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: {generazione: 'G5', codice:{$not: /G5$/}},
    filter: [process.filters.hasNotCharAt,process.filters.hasNotCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:["-G5"], pos:8},{chars:["KIT"], pos:1},{len:9}],
    //test:true,
    checkDuplicates:true,
    logLevel:1,
    limit:5000,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var old = p.codice;

                var upd = {};
                upd.codice = p.codice + '-G5';

                //console.log('nella prima funzione di trasformazione ',old,p.codice);
                return upd;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

