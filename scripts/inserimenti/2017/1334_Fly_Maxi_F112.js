var process = require('./massive.js');
var modello = '59aeaee9dd826e040027a43d';
var query = { modello : modello };
var tag = 'dani_20170913_02';

var transform1 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["1"], pos:8},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, '2', 8);
                p.colore = 'Nero';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["1"], pos:8},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, '0', 8);
                p.colore = 'Grigio';

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["C"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 6);

                switch (p.lm) {
                    case '2675' :
                        p.lm = '3640';
                        break;
                    case '2745' :
                        p.lm = '3735';
                        break;
                    case '2860' :
                        p.lm = '3890';
                        break;
                    case '2270' :
                        p.lm = '3085';
                        break;
                    case '2325' :
                        p.lm = '3160';
                        break;
                    case '2490' :
                        p.lm = '3390';
                        break;
                    case '2140' :
                        p.lm = '2910';
                        break;
                    case '2020' :
                        p.lm = '2750';
                        break;
                }

                switch (p.prezzo) {
                    case 180 :
                        p.prezzo = 188;
                        break;
                    case 200 :
                        p.prezzo = 208;
                        break;
                    case 215 :
                        p.prezzo = 223;
                        break;
                };
                p.w = 25;
                p.ma = 700;

                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = p.codice + 'D';
                switch (p.prezzo) {
                    case 180 :
                        p.prezzo = 240;
                        break;
                    case 200 :
                        p.prezzo = 260;
                        break;
                    case 215 :
                        p.prezzo = 275;
                        break;
                    case 188 :
                        p.prezzo = 248;
                        break;
                    case 208 :
                        p.prezzo = 268;
                        break;
                    case 223 :
                        p.prezzo = 283;
                        break;
                };
                p.alimentatore = 'Driver dimmerabile';
                p.dimmerabile = true;

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'L', 9);
                switch (p.prezzo) {
                    case 240 :
                        p.prezzo = 255;
                        break;
                    case 248 :
                        p.prezzo = 263;
                        break;
                    case 260 :
                        p.prezzo = 275;
                        break;
                    case 268 :
                        p.prezzo = 283;
                        break;
                    case 275 :
                        p.prezzo = 290;
                        break;
                    case 283 :
                        p.prezzo = 298;
                        break;
                };
                p.alimentatore = 'Driver dali';

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*


 Colore nero: parti da tutti i codici con i primi 4 caratteri 1244, li duplichi:
 8° carattere 9  2
 Colore: Nero
 Colore grigio: parti da tutti i codici con i primi 4 caratteri 1244 e 8° carattere 9, li duplichi:
 8° carattere 9  0
 Colore: Grigio
 700 mA: parti da tutti i codici con i primi 4 caratteri 1244, li duplichi:
 6° carattere C  D
 Potenza: 25
 Corrente: 700
 Fl. Lum. Nom:  2675  3640
 2745  3735
 2860  3890
 2270 3085
 2325 3160
 2490 3390
 2140  2910
 20202750
 Prezzo: 180 188
 200208
 215223
 Versione Dimmerabile: parti da tutti i codici con i primi 4 caratteri 1244, li duplichi:
 Aggiungo il 9° carattere : D
 Prezzo: 180240
 200260
 215275
 188248
 208268
 223283
 Alimentatore: Driver dimmerabile
 Dimmerabile: X (flag)
 Versione Dali: parti da tutti i codici con i primi 4 caratteri 1244 e 9° carattere D, li duplichi:
 9° carattere : DL
 Prezzo: 240255
 248263
 260275
 268283
 275290
 283298
 Alimentatore: Driver dali
 */