var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd22c';
var query = {modello:modello, generazione: 'G6'};
var tag = 'dani_20170921_01';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasNotSottoCategoria,process.filters.hasCharAt],
    filterArguments : [{sottocategoria:'Excite fruit & vegetables'},{chars:['1241'], pos:1}],
    checkDuplicates: true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);

                switch (p.prezzo) {
                    case 138 :
                        p.prezzo = 198;
                        break;
                    case 147 :
                        p.prezzo = 207;
                        break;
                    case 158 :
                        p.prezzo = 218;
                        break;
                    case 167 :
                        p.prezzo = 227;
                        break;
                    case 173 :
                        p.prezzo = 233;
                        break;
                    case 182 :
                        p.prezzo = 242;
                        break;
                }

                p.insertRule = 1;

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: {modello:modello, generazione: 'G6', insertRule: 1},
    checkDuplicates: true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);

                switch (p.prezzo) {
                    case 198 :
                        p.prezzo = 213;
                        break;
                    case 207 :
                        p.prezzo = 222;
                        break;
                    case 218 :
                        p.prezzo = 233;
                        break;
                    case 227 :
                        p.prezzo = 242;
                        break;
                    case 233 :
                        p.prezzo = 248;
                        break;
                    case 242 :
                        p.prezzo = 257;
                        break;
                }

                delete p.insertRule;

                console.log('nella seconda funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: {modello:modello, generazione: 'G6', insertRule: 1},
    checkDuplicates: true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.push_memory(p);

                switch (p.prezzo) {
                    case 198 :
                        p.prezzo = 238;
                        break;
                    case 207 :
                        p.prezzo = 247;
                        break;
                    case 218 :
                        p.prezzo = 258;
                        break;
                    case 227 :
                        p.prezzo = 267;
                        break;
                    case 233 :
                        p.prezzo = 273;
                        break;
                    case 242 :
                        p.prezzo = 282;
                        break;
                }

                delete p.insertRule;

                console.log('nella terza funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

