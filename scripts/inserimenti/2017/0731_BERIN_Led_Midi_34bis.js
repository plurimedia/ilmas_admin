var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd257';
var query = {modello:modello, generazione: 'G6' };
var tag = 'dani_20170922_17';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasCharAt,process.filters.hasCharAt],
    filterArguments : [{chars:['C'], pos:6},{chars:['L','N','G','J'], pos:5}],
    //filter: process.filters.hasCharAt,
    //filterArguments : {chars:['C'], pos:6},
    checkDuplicates:true,
    //test:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var old = p.codice;
                p.codice = process.replaceCharAt(p.codice, 'Q', 6)
                p.w = 21;
                p.ma = 600;

                switch (p.lm) {
                    case '1440' :
                        p.lm = '1700';
                        break;
                    case '1470' :
                        p.lm = '1730';
                        break;
                    case '2185' :
                        p.lm = '2585';
                        break;
                    case '1530' :
                        p.lm = '1810';
                        break;
                }

                console.log('nella prima funzione di trasformazione ',old,p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

