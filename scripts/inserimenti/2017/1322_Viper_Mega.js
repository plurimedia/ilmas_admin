var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd23a';
var query = {modello:modello, generazione: 'G6'};
var tag = 'dani_20170920_06';

var transform1 = {
    tag : tag,
    query: query,
    filter: process.filters.hasNotSottoCategoria,
    filterArguments : {sottocategoria:'Excite fruit & vegetables'},
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);

                switch (p.prezzo) {
                    case 185 :
                        p.prezzo = 245;
                        break;
                    case 205 :
                        p.prezzo = 265;
                        break;
                }

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: {modello:modello, generazione: 'G6', insertTag: { $exists:true}},
    checkDuplicates:true,
//    filter: process.filters.hasLunghezzaCodiceMax,
//    filterArguments : {len:8},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);

                switch (p.prezzo) {
                    case 245 :
                        p.prezzo = 260;
                        break;
                    case 265 :
                        p.prezzo = 280;
                        break;
                }

                console.log('nella seconda funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    checkDuplicates:true,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["1"], pos:8},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.grigio(p);

                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    checkDuplicates:true,
    filter: [process.filters.hasCharAt, process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:["UF"], pos:5},{len:9}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'F', 5);
                p.k = '2700';
                p.lm = '4625';

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

