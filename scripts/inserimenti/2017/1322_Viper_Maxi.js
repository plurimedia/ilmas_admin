var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd239';
var query = {modello:modello, generazione: 'G6'};
var tag = 'dani_20170920_05';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasNotSottoCategoria, process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{sottocategoria:'Excite fruit & vegetables'},{len:8}],
    checkDuplicates: true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);

                switch (p.prezzo) {
                    case 157 :
                        p.prezzo = 217;
                        break;
                    case 167 :
                        p.prezzo = 227;
                        break;
                    case 177 :
                        p.prezzo = 237;
                        break;
                    case 187 :
                        p.prezzo = 247;
                        break;
                    case 192 :
                        p.prezzo = 252;
                        break;
                    case 202 :
                        p.prezzo = 262;
                        break;
                }

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: {modello:modello, generazione: 'G6', insertTag: { $exists:true}},
    //filter: process.filters.hasLunghezzaCodiceMax,
    //filterArguments :{len:8},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);

                switch (p.prezzo) {
                    case 217 :
                        p.prezzo = 232;
                        break;
                    case 227 :
                        p.prezzo = 242;
                        break;
                    case 237 :
                        p.prezzo = 252;
                        break;
                    case 247 :
                        p.prezzo = 262;
                        break;
                    case 252 :
                        p.prezzo = 267;
                        break;
                    case 262 :
                        p.prezzo = 277;
                        break;
                }

                console.log('nella seconda funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasCharAt, process.filters.hasNotSottoCategoria],
    filterArguments : [{chars:["1"], pos:8}, {sottocategoria:'Excite fruit & vegetables'}],
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                var old = p.codice;
                p = process.grigio(p);

                console.log('nella terza funzione di trasformazione ',old,p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

