var process = require('./massive.js');
var modello = '59b00ad6d9bffb040079b531';
var query = { modello : modello };
var tag = 'dani_20170914_04';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'G', 7);

                p.fascio = '25';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'L', 7);

                p.fascio = '35';

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = p.codice + 'E';
                if (p.sottocategoria.toLowerCase() != 'fashion') {
                    switch (p.prezzo) {
                        case 105 :
                            p.prezzo = 125;
                            break;
                        case 140 :
                            p.prezzo = 160;
                            break;
                    }
                    ;
                }else {
                    p.prezzo = 145;
                }

                p.alimentatore = 'Driver standard';
                p.alimentatore_incluso = true;

                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["E"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 9);

                switch (p.prezzo) {
                    case 125 :
                        p.prezzo = 165;
                        break;
                    case 145 :
                        p.prezzo = 185;
                        break;
                    case 160 :
                        p.prezzo = 200;
                        break;
                };

                p.alimentatore = 'Driver dimmerabile';
                p.dimmerabile = true;

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'L', 9);
                switch (p.prezzo) {
                    case 165 :
                        p.prezzo = 172;
                        break;
                    case 185 :
                        p.prezzo = 192;
                        break;
                    case 200 :
                        p.prezzo = 207;
                        break;
                };
                p.alimentatore = 'Driver dali';

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform6 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 6);

                if (p.sottocategoria.toLowerCase() != 'fashion' && p.sottocategoria.toLowerCase() != 'art') {
                    switch (p.lm) {
                        case '2360' :
                            p.lm = '3170';
                            break;
                        case '3170' :
                            p.lm = '3330';
                            break;
                        case '2600' :
                            p.lm = '3500';
                            break;
                        case '2020':
                            p.lm = '2710';
                            break;
                        case '2125' :
                            p.lm = '2860';
                            break;
                        case '2235' :
                            p.lm = '3000';
                            break;
                        case '2140' :
                            p.lm = '2910';
                            break;
                    }
                }else if (p.sottocategoria.toLowerCase() == 'fashion'){
                    switch (p.lm) {
                        case '2020' :
                            p.lm = '2750';
                            break;
                    }
                }else if (p.sottocategoria.toLowerCase() == 'art'){
                    switch (p.lm) {
                        case '2020' :
                            p.lm = '2750';
                            break;
                    }
                }

                switch (p.prezzo) {
                    case 125 :
                        p.prezzo = 134;
                        break;
                    case 145 :
                        p.prezzo = 154;
                        break;
                    case 160 :
                        p.prezzo = 169;
                        break;
                };

                p.w = 24;
                p.ma = 700;

                console.log('nella sesta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform7 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["C"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'E', 6);

                if (p.sottocategoria.toLowerCase() != 'fashion' && p.sottocategoria.toLowerCase() != 'art') {
                    switch (p.lm) {
                        case '2360' :
                            p.lm = '3905';
                            break;
                        case '2480' :
                            p.lm = '4105';
                            break;
                        case '2600' :
                            p.lm = '4310';
                            break;
                        case '2020' :
                            p.lm = '3345';
                            break;
                        case '2125' :
                            p.lm = '3515';
                            break;
                        case '2235' :
                            p.lm = '3695';
                            break;
                        case '2140' :
                            p.lm = '3640';
                            break;
                    }
                }else if (p.sottocategoria.toLowerCase() == 'fashion'){
                    switch (p.lm) {
                        case '2020' :
                            p.lm = '3440';
                            break;
                    }
                }else if (p.sottocategoria.toLowerCase() == 'art'){
                    switch (p.lm) {
                        case '2020' :
                            p.lm = '3440';
                            break;
                    }
                }


                if (p.sottocategoria.toLowerCase() == 'Fashion' && p.codice.substring(8,9) == 'L') {
                    p.prezzo = 212;
                }else {
                    switch (p.prezzo) {
                        case 125 :
                            p.prezzo = 144;
                            break;
                        case 145 :
                            p.prezzo = 164;
                            break;
                        case 160 :
                            p.prezzo = 179;
                            break;
                        case 165 :
                            p.prezzo = 192;
                            break;
                        case 185 :
                            p.prezzo = 212;
                            break;
                        case 200 :
                            p.prezzo = 227;
                            break;
                        case 172 :
                            p.prezzo = 192;
                            break;
                        case 207 :
                            p.prezzo = 227;
                            break;
                    }
                    ;
                }

                p.w = 32;
                p.ma = 900;

                console.log('nella settima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*


 1. Colore nero: parti da tutti i codici con i primi 4 caratteri 1335, li duplichi:
 8° carattere 1  2
 Colore: Nero
 2. Colore grigio: parti da tutti i codici con i primi 4 caratteri 1335 e 8° carattere 1, li duplichi:
 8° carattere 1  0
 Colore: Grigio
 3. 500 mA: parti da tutti i codici con i primi 4 caratteri 1335, li duplichi:
 6° carattere B  C
 Potenza: 17
 Corrente: 500
 Fl. Lum. Nom:  19202675
 19702745
 20502860
 16252270
 16652325
 17852490
 15302140
 14502020
 4. 700 mA: parti da tutti i codici con i primi 4 caratteri 1335 e 6° carattere C, li duplichi:
 6° carattere C  D
 Potenza: 25
 Corrente: 700
 Fl. Lum. Nom:  2675  3640
 2745  3735
 2860  3890
 2270 3085
 2325 3160
 2490 3390
 2140  2910
 20202750
 5. Ottica 33°: parti da tutti i codici con i primi 4 caratteri 1335, li duplichi:
 7° carattere G  J
 Fascio: 33
 6. Ottica 50°: parti da tutti i codici con i primi 4 caratteri 1335 e 7° carattere G, li duplichi:
 7° carattere G  R
 Fascio: 50
 */