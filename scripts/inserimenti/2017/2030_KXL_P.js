var process = require('./massive.js');
var ObjectId = require('mongodb').ObjectID;
var modello = '59b920f4d5eaa0040053d680';
var query = {modello:ObjectId(modello) };
var tag = 'dani_20171010_02';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);
                switch (p.prezzo) {
                    case 236 :
                        p.prezzo = 296;
                        break;
                    case 256 :
                        p.prezzo = 316;
                        break;
                    case 271 :
                        p.prezzo = 331;
                        break;
                }

                p.insertRule = 1;

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

