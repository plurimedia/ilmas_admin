var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd2d3';
var query = {modello:modello, generazione: 'G6' };
var tag = 'dani_20170922_26';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var upd = {};
                upd.codice = process.replaceCharAt(p.codice, 'E', 6);
                upd.w = '3x33';
                upd.ma = 900;

                p.lm_old = p.lm;

                switch (p.lm) {
                    case '4670' :
                        upd.lm = '14010';
                        break;
                    case '4870' :
                        upd.lm = '14610';
                        break;
                    case '3955' :
                        upd.lm = '11865';
                        break;
                    case '4240' :
                        upd.lm = '12720';
                        break;
                    case '3640' :
                        upd.lm = '10920';
                        break;
                }

                p.prezzo_old = p.prezzo;

                switch (p.prezzo) {
                    case 1050 :
                        upd.prezzo = 960;
                        break;
                    case 1060 :
                        upd.prezzo = 960;
                        break;
                    case 1300 :
                        upd.prezzo = 1104;
                        break;
                    case 1230 :
                        upd.prezzo = 1104;
                        break;
                    case 1200 :
                        upd.prezzo = 1104;
                        break;
                    case 1210 :
                        upd.prezzo = 1065;
                        break;
                    case 1400 :
                        upd.prezzo = 1209;
                        break;
                    case 1460 :
                        upd.prezzo = 1209;
                        break;
                };

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return upd;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

