var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd22b';
var query = {modello:modello, generazione: { $in: ['G6','D1']}};
var tag = 'dani_20170925_01';

var transform1 = {
    tag : tag,
    query: query,
    filter: process.filters.hasNotSottoCategoria,
    filterArguments : {sottocategoria:'Fashion'},
    //test:true,
    checkDuplicates: true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                var old = p.codice;
                p = process.dimmerabile(p);

                switch (p.prezzo) {
                    case 133 :
                        p.prezzo = 193;
                        break;
                    case 153 :
                        p.prezzo = 213;
                        break;
                    case 168 :
                        p.prezzo = 228;
                        break;
                }

                p.insertRule = 1;

                console.log('nella prima funzione di trasformazione ',old, p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: {modello:modello, insertRule: 1},
    checkDuplicates: true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);

                switch (p.prezzo) {
                    case 193 :
                        p.prezzo = 208;
                        break;
                    case 213 :
                        p.prezzo = 228;
                        break;
                    case 228 :
                        p.prezzo = 243;
                        break;
                }

                delete p.insertRule;

                console.log('nella seconda funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: {modello:modello, insertRule: 1},
    checkDuplicates: true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.push_memory(p);

                switch (p.prezzo) {
                    case 193 :
                        p.prezzo = 233;
                        break;
                    case 213 :
                        p.prezzo = 253;
                        break;
                    case 228 :
                        p.prezzo = 268;
                        break;
                }

                delete p.insertRule;

                console.log('nella terza funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

