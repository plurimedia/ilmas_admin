var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd2d5';
var query = {modello:modello, generazione: 'G6'};
var tag = 'dani_20170922_28';

var transform1 = {
    tag : tag,
    query: query,
    filter: process.filters.hasNotCharAt,
    filterArguments : {chars:['O','L','I'], pos:5},
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var old = p.codice;

                var upd = {};
                upd.codice = process.replaceCharAt(p.codice, 'E', 6);
                upd.w = '1x33+3x33';
                upd.ma = '900+900';

                switch (p.lm.replace(" + ", "+").replace("\n+\n", "+")) {
                    case '4670' :
                        upd.lm = '4670 + 14010';
                        break;
                    case '4870' :
                        upd.lm = '4870 + 14610';
                        break;
                    case '3955' :
                        upd.lm = '3955 + 11865';
                        break;
                    case '4240' :
                        upd.lm = '4240 + 12720';
                        break;
                    case '3640' :
                        upd.lm = '3640 + 10920';
                        break;
                }

                switch (p.prezzo) {
                    case 1360 :
                        upd.prezzo = 1200;
                        break;
                    case 1580 :
                        upd.prezzo = 1365;
                        break;
                    case 1660 :
                        upd.prezzo = 1365;
                        break;
                    case 1350 :
                        upd.prezzo = 1200;
                        break;
                    case 1550 :
                        upd.prezzo = 1365;
                        break;
                    case 1590 :
                        upd.prezzo = 1365;
                        break;
                    case 1570 :
                        upd.prezzo = 1340;
                        break;
                    case 1800 :
                        upd.prezzo = 1505;
                        break;
                    case 1880 :
                        upd.prezzo = 1505;
                        break;
                };

                console.log('nella prima funzione di trasformazione',old, p.codice);
                return upd;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

