var process = require('./massive.js');
var modello = '59ae6797144c280400213f92';
var query = { modello : modello };
var tag = 'dani_20170910_01';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                //p.codice = p.codice.substring(0, 7) + '2' + p.codice.substring(8,20);//replace dell'ottavo carattere
                p.codice = process.replaceCharAt(p.codice, '2', 8);
                p.colore = 'Nero';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                //p.codice = p.codice.substring(0, 5) + 'C' + p.codice.substring(6,20);//sesto carattere
                p.codice = process.replaceCharAt(p.codice, 'C', 6);
                p.w = '17';
                p.ma = 500;
                switch (p.lm) {
                    case '1705' :
                        p.lm = '2360';
                        break;
                    case '1790' :
                        p.lm = '2480';
                        break;
                    case '1880' :
                        p.lm = '2600';
                        break;
                    case '1460' :
                        p.lm = '2020';
                        break;
                    case '1535' :
                        p.lm = '2125';
                        break;
                    case '1610' :
                        p.lm = '2235';
                        break;
                    case '1530' :
                        p.lm = '2140';
                        break;
                    case '1450' :
                        p.lm = '2020';
                        break;
                }

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["G"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                //p.codice = p.codice.substring(0, 6) + 'J' + p.codice.substring(7,20);//replace del settimo carattere
                p.codice = process.replaceCharAt(p.codice, 'J', 7);
                p.fascio = '33';

                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["G"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                //p.codice = p.codice.substring(0, 6) + 'R' + p.codice.substring(7,20);//replace del settimo carattere
                p.codice = process.replaceCharAt(p.codice, 'R', 7);
                p.fascio = '50';

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = p.codice + 'D';
                switch (p.prezzo) {
                    case 157 :
                        p.prezzo = 197;
                        break;
                    case 177 :
                        p.prezzo = 217;
                        break;
                    case 192 :
                        p.prezzo = 232;
                        break;
                };
                p.alimentatore = 'Driver dimmerabile';
                p.dimmerabile = true;

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform6 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                //p.codice = p.codice.substring(0, 6) + 'L' + p.codice.substring(7,20);//replace del settimo carattere
                p.codice = process.replaceCharAt(p.codice, 'L', 9);
                switch (p.prezzo) {
                    case 197 :
                        p.prezzo = 204;
                        break;
                    case 217 :
                        p.prezzo = 224;
                        break;
                    case 232 :
                        p.prezzo = 239;
                        break;
                };
                p.alimentatore = 'Driver dali';

                console.log('nella sesta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*


 1) a partire da tutti i codicii relativi al modello sopra indicato
 creare un nuovo codice con le stesse ed identiche caratteristiche del padre tranne che per
 codice, che diventa all'ottavo carattere 2
 colore = nero

 2) a partire da tutti i codici relativi al modello sopra indicato (compresi quelli creati al punto 1):

 creare un nuovo con le stesse ed identiche caratteristiche del padre, tranne che per
 codice, che diventa al quinto  carattere C
 potenza = 17
 Corrente = 500
 Fl. Lum. Nom:
 se 1705 --> 2360
 se 1790 --> 2480
 se 1880 --> 2600
 se 1460 --> 2020
 se 1535 --> 2125
 se 1610 --> 2235
 se 1530 --> 2140
 se 1450 --> 2020

 3) a partire da tutti i codici relativi al modello sopra indicato (compresi quelli creati al punto 1 e 2):

 creare un nuovo codice con le stesse ed identiche caratteristiche del padre tranne che per
 codice, che diventa al settimo carattere  carattere J
 fascio = 33

 4) a partire da tutti i codici relativi al modello sopra indicato (compresi quelli creati al punto 1 e 2 e 3) che hanno al settimo carattere lalettera G:
 creare un nuovo codice con le stesse ed identiche caratteristiche del padre tranne che per
 codice, che diventa al settimo carattere  carattere R
 fascio = 50

 5) a partire da tutti i codici relativi al modello sopra indicato (compresi quelli creati al punto 1 e 2 e 3 e 4) che hanno al settimo carattere lalettera G:

 creare un nuovo codice con le stesse ed identiche caratteristiche del padre tranne che per
 codice, al quale va aggiunto il nono carattere D
 prezzi
 se prezzo == 157, diventa 197
 se prezzo == 177, diventa 217
 se prezzo == 192, diventa 232
 alimentatore = Driver dimmerabile
 dimmerabile = true


 6) ersione Dali: parti da tutti i codici con i primi 4 caratteri 0830 e 9° carattere D, li duplichi:
 9° carattere : DL
 Prezzo: 197204
 217224
 232239
 Alimentatore: Driver dali


 */