var process = require('./massive.js');
var ObjectId = require('mongodb').ObjectID;
var modello = '59b28fda5d5c9e040071fd3e';
var query = {modello:ObjectId(modello), moduloled:'V2 Led Module'};
var tag = 'dani_20170914_07';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                var old = p.codice;
                p.codice = process.replaceCharAt(p.codice, '2', 5);

                p.k = '3000';
                p.lm = 600;

                console.log('nella prima funzione di trasformazione '+ old + " " + p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["1"], pos:5},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                var old = p.codice;
                p.codice = process.replaceCharAt(p.codice, '3', 5);

                p.k = '4000';
                p.lm = 650;

                console.log('nella seconda funzione di trasformazione '+ old + " " + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

