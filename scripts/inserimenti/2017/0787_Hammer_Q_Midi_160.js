var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd24a';
var query = {modello:modello, generazione: 'G6' };
var tag = 'dani_20170922_09';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:['D'], pos:6},{len:9}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'E', 6)
                p.w = 47;
                p.ma = 1200;

                switch (p.lm) {
                    case '3640' :
                        p.lm = '4550';
                        break;
                    case '3735' :
                        p.lm = '4670';
                        break;
                    case '3890' :
                        p.lm = '4870';
                        break;
                    case '3085' :
                        p.lm = '3860';
                        break;
                    case '3160' :
                        p.lm = '3955';
                        break;
                    case '3390' :
                        p.lm = '4240';
                        break;
                    case '2910' :
                        p.lm = '3660';
                        break;
                    case '2750' :
                        p.lm = '3440';
                        break;
                }

                switch (p.prezzo) {
                    case 140 :
                        p.prezzo = 150;
                        break;
                    case 171 :
                        p.prezzo = 198;
                        break;
                    case 178 :
                        p.prezzo = 198;
                        break;
                    case 160 :
                        p.prezzo = 170;
                        break;
                    case 191 :
                        p.prezzo = 218;
                        break;
                    case 198 :
                        p.prezzo = 218;
                        break;
                    case 175 :
                        p.prezzo = 185;
                        break;
                    case 206 :
                        p.prezzo = 233;
                        break;
                    case 213 :
                        p.prezzo = 233;
                        break;
                };

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

