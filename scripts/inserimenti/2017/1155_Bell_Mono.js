var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd2d2';
var query = {modello:modello, generazione: 'G4'};
var tag = 'dani_20170922_25';

var transform1 = {
    tag : tag,
    query: query,
    filter: process.filters.hasLunghezzaCodiceMax,
    filterArguments : {len:9},
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                var upd = {};
                upd.codice = p.codice+'-G4';
                upd.pubblicato = false;
                upd.exportMetel = false;

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return upd;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:['AF','BF','AG','BG','SF','TF','WF'], pos:5},
    //test:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                var old = p.codice;
                p.codice = p.codice.replace('-G4', '');
                p.pubblicato = true;
                p.exportMetel = true;
                p.generazione = 'G6';

                //console.log(p.lm, p.lm == '4950', codice);
                switch (p.lm) {
                    case '5150' :
                        p.lm = '5560';
                        break;
                    case '4950' :
                        p.lm = '5340';
                        break;
                    case '5160' :
                        p.lm = '5560';
                        break;
                    case '4200' :
                        p.lm = '4520';
                        break;
                    case '4400' :
                        p.lm = '4840';
                        break;
                    case '3450' :
                        p.lm = '4160';
                        break;
                    case '5600' :
                        p.lm = '6290';
                        break;
                    case '5800' :
                        p.lm = '6560';
                        break;
                }

                //console.log(p.w.toLowerCase(), p.w.toLowerCase() == '1x38', p.w.toLowerCase() == '1x43');
                switch (p.w.toLowerCase()) {
                    case '1x38' :
                        p.w = '39';
                        break;
                    case '1x43' :
                        p.w = '47';
                        break;
                }

                if (p.prezzo === 700 && process.filters.hasCharAt(p, {chars:["AF"], pos:5}) && process.filters.hasCharAt(p, {chars:["D"], pos:9}))
                    p.prezzo = 698;
                else if (p.prezzo === 700 && process.filters.hasCharAt(p, {chars:["WF"], pos:5}) && process.filters.hasLunghezzaCodiceMax(p, {len:8}))
                    p.prezzo = 685;
                else if (p.prezzo === 780)
                    p.prezzo = 733;
                else if (p.prezzo === 800)
                    p.prezzo = 733;
                else if (p.prezzo === 700 && process.filters.hasCharAt(p, {chars:["AG"], pos:5}) && process.filters.hasLunghezzaCodiceMax(p, {len:8}))
                    p.prezzo = 690;
                else if (p.prezzo === 750)
                    p.prezzo = 738;
                else if (p.prezzo === 770)
                    p.prezzo = 738;
                else if (p.prezzo === 700 && process.filters.hasCharAt(p, {chars:["BG"], pos:5}) && process.filters.hasLunghezzaCodiceMax(p, {len:8}))
                    p.prezzo = 690;

                console.log('nella seconda funzione di trasformazione ' + old + " " + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

