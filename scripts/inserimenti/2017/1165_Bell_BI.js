var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd2d4';
var query = {modello:modello, generazione: 'G4'};
var tag = 'dani_20170922_27';

var transform1 = {
    tag : tag,
    query: query,
    filter: process.filters.hasLunghezzaCodiceMax,
    filterArguments : {len:9},
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                var upd = {};
                upd.codice = p.codice+'-G4';
                upd.pubblicato = false;
                upd.exportMetel = false;

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return upd;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:['AF','BF','AG','BG','SG','TG','WG'], pos:5},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                var old = p.codice;
                p.codice = p.codice.replace('-G4', '');
                p.pubblicato = true;
                p.exportMetel = true;
                p.generazione = 'G6';

                p.lm_old = p.lm;
                switch (p.lm.replace(" + ", "+").replace("\n+\n", "+")) {
                    case '4600+4950' :
                        p.lm = '4670 + 5340';
                        break;
                    case '4700+5150' :
                        p.lm = '4870 + 5560';
                        break;
                    case '4600+5600' :
                        p.lm = '4670 + 6290';
                        break;
                    case '4700+5800' :
                        p.lm = '4870 + 6570';
                        break;
                    case '3900+4700' :
                        p.lm = '3955 + 5330';
                        break;
                    case '4000+4900' :
                        p.lm = '4240 + 5710';
                        break;
                    case '3200+3900' :
                        p.lm = '3640 + 4900';
                        break;
                }

                p.w_old = p.w;
                switch (p.w.toLowerCase().replace("\n+\n", "+")) {
                    case '1x34+1x38' :
                        p.w = '1x33+1x39';
                        break;
                    case '1x34+1x43' :
                        p.w = '1x33+1x47';
                        break;
                }

                p.ma_old = p.ma;
                switch (p.ma.replace(" + ", "+").replace("\n+\n", "+").replace("\n","")) {
                    case '9001050' :
                        p.ma = '900+1050';
                        break;
                    case '9001200' :
                        p.ma = '900+1200';
                        break;
                }

                if (p.prezzo === 950)
                    p.prezzo = 850;
                else if (p.prezzo === 1050 && process.filters.hasCharAt(p, {chars:["AF","BF"], pos:5}) && process.filters.hasCharAt(p, {chars:["D"], pos:9}))
                    p.prezzo = 946;
                else if (p.prezzo === 1080)
                    p.prezzo = 946;
                else if (p.prezzo === 1050 && process.filters.hasCharAt(p, {chars:["AG","BG","SG","TG"], pos:5}) && process.filters.hasLunghezzaCodiceMax(p, {len:8}))
                    p.prezzo = 950;

                else if (p.prezzo === 1170)
                    p.prezzo = 1037;
                else if (p.prezzo === 1200)
                    p.prezzo = 1037;
                else if (p.prezzo === 1160)
                    p.prezzo = 1020;
                else if (p.prezzo === 1320)
                    p.prezzo = 1107;
                else if (p.prezzo === 1360)
                    p.prezzo = 1107;

                console.log('nella seconda funzione di trasformazione ' + old + " " + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

