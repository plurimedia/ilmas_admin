var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd241';
var query = {modello:modello, generazione: 'G6', moduloled:'F112 Led Module'};
var tag = 'dani_20170920_09';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);

                switch (p.prezzo) {
                    case 220 :
                        p.prezzo = 280;
                        break;
                    case 229 :
                        p.prezzo = 289;
                        break;
                    case 239 :
                        p.prezzo = 299;
                        break;
                    case 240 :
                        p.prezzo = 300;
                        break;
                    case 249 :
                        p.prezzo = 309;
                        break;
                    case 259 :
                        p.prezzo = 319;
                        break;
                    case 255 :
                        p.prezzo = 315;
                        break;
                    case 264 :
                        p.prezzo = 324;
                        break;
                    case 274 :
                        p.prezzo = 334;
                        break;
                }

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    //filter: process.filters.hasLunghezzaCodiceMax,
    //filterArguments : {len:8},
    query: {modello:modello, generazione: 'G6', insertTag: tag},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);

                switch (p.prezzo) {
                    case 280 :
                        p.prezzo = 295;
                        break;
                    case 289 :
                        p.prezzo = 304;
                        break;
                    case 299 :
                        p.prezzo = 314;
                        break;
                    case 300 :
                        p.prezzo = 315;
                        break;
                    case 309 :
                        p.prezzo = 324;
                        break;
                    case 319 :
                        p.prezzo = 334;
                        break;
                    case 315 :
                        p.prezzo = 330;
                        break;
                    case 324 :
                        p.prezzo = 339;
                        break;
                    case 334 :
                        p.prezzo = 349;
                        break;
                }


                console.log('nella seconda funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

