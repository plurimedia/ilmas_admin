var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd22f';
var query = {modello:modello, generazione: 'G4'};
var tag = 'dani_20170918_03';

var transform1 = {
    tag : tag,
    query: query,
    filter: process.filters.hasSottoCategoria,
    filterArguments : {sottocategoria:'fashion'},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                var old = p.codice;
                p.codice = p.codice.replace('-G4', '');

                p.generazione = 'G6';

                switch (p.lm) {
                    case '1250' :
                        p.lm = '1530';
                        break;
                    case '1800' :
                        p.lm = '1935';
                        break;
                }
                p.les = 'LES 19';
                switch (p.ma) {
                    case '350' :
                        p.ma = '350';
                        break;
                    case '500' :
                        p.ma = '450';
                        break;
                }
                switch (p.w.toLowerCase()) {
                    case '1x12' :
                        p.w = '12';
                        break;
                    case '1x17' :
                        p.w = '15';
                        break;
                }
                p.prezzo = 153;

                console.log('nella prima funzione di trasformazione ' + old + " " + p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: {modello:modello, generazione: 'G6', insertTag: {$exists:true}},//quelli del punto 1
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);
                p.prezzo = 213;

                p.insertRule = 2;

                console.log('nella seconda funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: {modello:modello, generazione: 'G6', insertRule:2},//quelli del punto 2
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);
                p.prezzo = 228;

                console.log('nella terza funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: {modello:modello, generazione: 'G6'},
    filter: process.filters.hasNotSottoCategoria,
    filterArguments : {sottocategoria:'fashion'},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);

                switch (p.prezzo) {
                    case 133 :
                        p.prezzo = 193;
                        break;
                    case 168 :
                        p.prezzo = 228;
                        break;
                }

                p.insertRule = 4;

                console.log('nella quarta funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: {modello:modello, generazione: 'G6', insertRule:4},//quelli del punto 2
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);
                switch (p.prezzo) {
                    case 193 :
                        p.prezzo = 208;
                        break;
                    case 228 :
                        p.prezzo = 243;
                        break;
                }

                console.log('nella quinta funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};


var transform6 = {
    tag : tag,
    query: {modello:modello, generazione: 'G6'},
    filter: process.filters.hasSottoCategoria,
    filterArguments : {sottocategoria:'ART'},
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                var upd = {};
                upd.pre_update = {ma: p.ma, w: p.w, lm: p.lm };
                upd.ma = 450;
                upd.w = 15;
                upd.lm = '1835';

                console.log('nella sesta funzione di trasformazione ' + p.codice);
                return upd;
            }
        }
    ]
};
process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

