var process = require('./massive.js');
var modello = '59b14365e889e40400ced116';
var query = { modello : modello };
var tag = 'dani_20170914_05';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, '2', 8);
                p.colore = 'Nero';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'L', 7);

                p.fascio = '25';

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["E"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'R', 7);

                p.fascio = '50';

                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = p.codice + 'E';

                p.prezzo = 70;

                p.alimentatore = 'Driver standard';
                p.alimentatore_incluso = true;

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["E"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 9);

                p.prezzo = 110;

                p.alimentatore = 'Driver dimmerabile';
                p.dimmerabile = true;

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform6 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'L', 9);

                p.prezzo = 117;

                p.alimentatore = 'Driver dali';

                console.log('nella sesta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform7 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'C', 6);

                switch (p.lm) {
                    case '1580' :
                        p.lm = '2095';
                        break;
                    case '1660' :
                        p.lm = '2210';
                        break;
                    case '1745' :
                        p.lm = '2315';
                        break;
                    case '1350' :
                        p.lm = '1790';
                        break;
                    case '1415' :
                        p.lm = '1880';
                        break;
                    case '1610' :
                        p.lm = '1990';
                        break;
                }

                p.w = 18;
                p.ma = 500;

                console.log('nella settima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*


 1. Colore nero: parti da tutti i codici con i primi 4 caratteri 1335, li duplichi:
 8° carattere 1  2
 Colore: Nero
 2. Colore grigio: parti da tutti i codici con i primi 4 caratteri 1335 e 8° carattere 1, li duplichi:
 8° carattere 1  0
 Colore: Grigio
 3. 500 mA: parti da tutti i codici con i primi 4 caratteri 1335, li duplichi:
 6° carattere B  C
 Potenza: 17
 Corrente: 500
 Fl. Lum. Nom:  19202675
 19702745
 20502860
 16252270
 16652325
 17852490
 15302140
 14502020
 4. 700 mA: parti da tutti i codici con i primi 4 caratteri 1335 e 6° carattere C, li duplichi:
 6° carattere C  D
 Potenza: 25
 Corrente: 700
 Fl. Lum. Nom:  2675  3640
 2745  3735
 2860  3890
 2270 3085
 2325 3160
 2490 3390
 2140  2910
 20202750
 5. Ottica 33°: parti da tutti i codici con i primi 4 caratteri 1335, li duplichi:
 7° carattere G  J
 Fascio: 33
 6. Ottica 50°: parti da tutti i codici con i primi 4 caratteri 1335 e 7° carattere G, li duplichi:
 7° carattere G  R
 Fascio: 50
 */