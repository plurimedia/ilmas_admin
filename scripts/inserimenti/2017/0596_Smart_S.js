var process = require('./massive.js');
var modello = '59aff297d9bffb040079b4e4';
var query = { modello : modello };
var tag = 'dani_20170914_02';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'K', 7);

                p.fascio = '30';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["E"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'N', 7);

                p.fascio = '40';

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = p.codice + 'E';
                switch (p.prezzo) {
                    case 85 :
                        p.prezzo = 105;
                        break;
                    case 105 :
                        p.prezzo = 125;
                        break;
                    case 120 :
                        p.prezzo = 140;
                        break;
                };

                p.alimentatore = 'Driver standard';
                p.alimentatore_incluso = true;

                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["E"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 9);

                switch (p.prezzo) {
                    case 105 :
                        p.prezzo = 145;
                        break;
                    case 125 :
                        p.prezzo = 165;
                        break;
                    case 140 :
                        p.prezzo = 180;
                        break;
                };

                p.alimentatore = 'Driver dimmerabile';
                p.dimmerabile = true;

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'L', 9);
                switch (p.prezzo) {
                    case 145 :
                        p.prezzo = 152;
                        break;
                    case 165 :
                        p.prezzo = 172;
                        break;
                    case 180 :
                        p.prezzo = 187;
                        break;
                };
                p.alimentatore = 'Driver dali';

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform6 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'C', 6);

                switch (p.lm) {
                    case '1705' :
                        p.lm = '2360';
                        break;
                    case '1790' :
                        p.lm = '2480';
                        break;
                    case '1880' :
                        p.lm = '2600';
                        break;
                    case '1460' :
                        p.lm = '2020';
                        break;
                    case '1535' :
                        p.lm = '2125';
                        break;
                    case '1610' :
                        p.lm = '2235';
                        break;
                    case '1530' :
                        p.lm = '2140';
                        break;
                    case '1450' :
                        p.lm = '2020';
                        break;
                }

                p.w = 17;
                p.ma = 500;

                console.log('nella sesta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform7 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["B"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'Q', 6);

                switch (p.lm) {
                    case '1705' :
                        p.lm = '2775';
                        break;
                    case '1790' :
                        p.lm = '2915';
                        break;
                    case '1880' :
                        p.lm = '3060';
                        break;
                    case '1460' :
                        p.lm = '2375';
                        break;
                    case '1535' :
                        p.lm = '2500';
                        break;
                    case '1610' :
                        p.lm = '2626';
                        break;
                    case '1530' :
                        p.lm = '2530';
                        break;
                    case '1450' :
                        p.lm = '2390';
                        break;
                }

                if (p.sottocategoria.toLowerCase() == 'Fashion' && p.codice.length == 8) {
                    switch (p.prezzo) {
                        case 125 :
                            p.prezzo = 134;
                            break;
                        case 140 :
                            p.prezzo = 149;
                            break;
                    }
                    ;
                }else {
                    switch (p.prezzo) {
                        case 105 :
                            p.prezzo = 114;
                            break;
                        case 125 :
                            p.prezzo = 134;
                            break;
                        case 140 :
                            p.prezzo = 149;
                            break;
                    }
                    ;
                }

                p.w = 21;
                p.ma = 600;

                console.log('nella settima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*


 1. Colore nero: parti da tutti i codici con i primi 4 caratteri 1335, li duplichi:
 8° carattere 1  2
 Colore: Nero
 2. Colore grigio: parti da tutti i codici con i primi 4 caratteri 1335 e 8° carattere 1, li duplichi:
 8° carattere 1  0
 Colore: Grigio
 3. 500 mA: parti da tutti i codici con i primi 4 caratteri 1335, li duplichi:
 6° carattere B  C
 Potenza: 17
 Corrente: 500
 Fl. Lum. Nom:  19202675
 19702745
 20502860
 16252270
 16652325
 17852490
 15302140
 14502020
 4. 700 mA: parti da tutti i codici con i primi 4 caratteri 1335 e 6° carattere C, li duplichi:
 6° carattere C  D
 Potenza: 25
 Corrente: 700
 Fl. Lum. Nom:  2675  3640
 2745  3735
 2860  3890
 2270 3085
 2325 3160
 2490 3390
 2140  2910
 20202750
 5. Ottica 33°: parti da tutti i codici con i primi 4 caratteri 1335, li duplichi:
 7° carattere G  J
 Fascio: 33
 6. Ottica 50°: parti da tutti i codici con i primi 4 caratteri 1335 e 7° carattere G, li duplichi:
 7° carattere G  R
 Fascio: 50
 */