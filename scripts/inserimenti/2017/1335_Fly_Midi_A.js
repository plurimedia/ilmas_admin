var process = require('./massive.js');
var modello = '594b8a0f21ef9f04004e4170';
var query = { modello : modello };
var tag = 'dani_20170913_03';

var transform1 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["1"], pos:8},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, '2', 8);
                p.colore = 'Nero';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["1"], pos:8},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, '0', 8);
                p.colore = 'Grigio';

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["B"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'C', 6);

                switch (p.lm) {
                    case '1920' :
                        p.lm = '2675';
                        break;
                    case '1970' :
                        p.lm = '2745';
                        break;
                    case '2050' :
                        p.lm = '2860';
                        break;
                    case '1625' :
                        p.lm = '2270';
                        break;
                    case '1665' :
                        p.lm = '2325';
                        break;
                    case '1785' :
                        p.lm = '2490';
                        break;
                    case '1530' :
                        p.lm = '2140';
                        break;
                    case '1450' :
                        p.lm = '2020';
                        break;
                }

                p.w = 17;
                p.ma = 500;

                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["C"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 6);

                switch (p.lm) {
                    case '2675' :
                        p.lm = '3640';
                        break;
                    case '2745' :
                        p.lm = '3735';
                        break;
                    case '2860' :
                        p.lm = '3890';
                        break;
                    case '2270' :
                        p.lm = '3085';
                        break;
                    case '2325' :
                        p.lm = '3160';
                        break;
                    case '2490' :
                        p.lm = '3390';
                        break;
                    case '2140' :
                        p.lm = '2910';
                        break;
                    case '2020' :
                        p.lm = '2750';
                        break;
                }

                p.w = 25;
                p.ma = 700;

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["G"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'J', 7);

                p.fascio = '33';

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform6 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["G"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'R', 7);

                p.fascio = '50';

                console.log('nella sesta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*


 1. Colore nero: parti da tutti i codici con i primi 4 caratteri 1335, li duplichi:
 8° carattere 1  2
 Colore: Nero
 2. Colore grigio: parti da tutti i codici con i primi 4 caratteri 1335 e 8° carattere 1, li duplichi:
 8° carattere 1  0
 Colore: Grigio
 3. 500 mA: parti da tutti i codici con i primi 4 caratteri 1335, li duplichi:
 6° carattere B  C
 Potenza: 17
 Corrente: 500
 Fl. Lum. Nom:  19202675 
 19702745 
 20502860 
 16252270 
 16652325 
 17852490 
 15302140 
 14502020
 4. 700 mA: parti da tutti i codici con i primi 4 caratteri 1335 e 6° carattere C, li duplichi:
 6° carattere C  D
 Potenza: 25
 Corrente: 700
 Fl. Lum. Nom:  2675  3640
 2745  3735
 2860  3890
 2270 3085
 2325 3160
 2490 3390
 2140  2910
 20202750
 5. Ottica 33°: parti da tutti i codici con i primi 4 caratteri 1335, li duplichi:
 7° carattere G  J
 Fascio: 33
 6. Ottica 50°: parti da tutti i codici con i primi 4 caratteri 1335 e 7° carattere G, li duplichi:
 7° carattere G  R
 Fascio: 50
 */