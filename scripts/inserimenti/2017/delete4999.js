/* 
    lanciare con: 
	node --max-old-space-size=2000 scripts/inserimenti/delete4999.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    ProdottoSchema = require('../../server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    ObjectId = require('mongodb').ObjectID,
	aggiornati = 0;

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console
    console.log(db)

    var query =
            { 
	            modello:{$in: ['56b8aaa90de23c95600bd2c8']},
	    		generazione: {$in: ['G6']}
            }

    console.log("query", query);

    Prodotto.find(query).lean()
    .exec(

    	function(err,prodotti){
        
            console.log('lunghezza '+prodotti.length+' prodotti')
        
  		    mongoose.connection.close();

  		    console.log('connection close') 

    		//cancelliamo  solo i record con il non sesto del codice == S
    		prodotti = _.filter(prodotti, function(p){
    			return p.codice.substring(5,6) == 'E'
    		})
  		 
  		    var process = require('../../scripts/inserimenti/deleteGeneric.js');
            process.execute(prodotti);
        
        }
    )
});


