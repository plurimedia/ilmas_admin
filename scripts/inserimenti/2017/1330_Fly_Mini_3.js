var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd234';
var query = {modello:modello, generazione: 'D1'};
var tag = 'dani_20170928_01';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);

                p.prezzo = 193;

                p.insertRule = 1;

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: {modello:modello, generazione: 'D1', insertTag: {$exists:true}, insertRule:1},//quelli del punto 1
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);

                p.prezzo = 208;
                delete p.insertRule;

                console.log('nella seconda funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: {modello:modello, generazione: 'D1', insertTag: {$exists:true}, insertRule:1},//quelli del punto 1
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                var old = p.codice;
                p = process.push_memory(p);

                p.prezzo = 233;
                delete p.insertRule;

                console.log('nella terza funzione di trasformazione ',old,p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: {modello:modello, generazione: 'D1'},//, insertTag: {$exists:true}},//quelli del punto 1,2,3
    checkDuplicates:true,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:['1'], pos:8},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                var old = p.codice;
                p = process.grigio(p);
                delete p.insertRule;

                console.log('nella quarta funzione di trasformazione ',old,p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

