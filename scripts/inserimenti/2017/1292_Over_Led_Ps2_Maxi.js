var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd232';
var query = {modello:modello, generazione: 'G6'};
var tag = 'dani_20170920_01';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates:true,
    filter: process.filters.hasLunghezzaCodiceMax,
    filterArguments : {len:8},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                var old = p.codice;
                p = process.dimmerabile(p);

                switch (p.prezzo) {
                    case 157 :
                        p.prezzo = 217;
                        break;
                    case 167 :
                        p.prezzo = 227;
                        break;
                    case 177 :
                        p.prezzo = 237;
                        break;
                    case 187 :
                        p.prezzo = 247;
                        break;
                    case 192 :
                        p.prezzo = 252;
                        break;
                    case 202 :
                        p.prezzo = 262;
                        break;
                }

                console.log('nella prima funzione di trasformazione ',old,p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

