var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd2c8';
var query = {modello:modello, generazione: 'G6' };
var tag = 'dani_20170922_24';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:['D'], pos:6},{len:9}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'E', 6)
                p.w = 33;
                p.ma = 900;

                switch (p.lm) {
                    case '3640' :
                        p.lm = '4550';
                        break;
                    case '3735' :
                        p.lm = '4670';
                        break;
                    case '3890' :
                        p.lm = '4870';
                        break;
                    case '3085' :
                        p.lm = '3860';
                        break;
                    case '3160' :
                        p.lm = '3955';
                        break;
                    case '3390' :
                        p.lm = '4240';
                        break;
                    case '2910' :
                        p.lm = '3660';
                        break;
                    case '2750' :
                        p.lm = '3440';
                        break;
                    case '1960' :
                        p.lm = '2450';
                        break;
                    case '1990' :
                        p.lm = '2490';
                        break;
                    case '2970' :
                        p.lm = '3725';
                        break;
                    case '2090' :
                        p.lm = '2600';
                        break;
                }

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

