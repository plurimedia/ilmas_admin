var process = require('./massive.js');
var modello = '56b8aaa90de23c95600bd257';
var query = {modello:modello, generazione: 'G6' };
var tag = 'dani_20170922_17';

var transform1 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:['D'], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'Q', 6)
                p.w = 21;
                p.ma = 600;

                switch (p.lm) {
                    case '3640' :
                        p.lm = '3165';
                        break;
                    case '3735' :
                        p.lm = '3245';
                        break;

                    case '3980' :
                        p.lm = '3385';
                        break;

                    case '3085' :
                        p.lm = '2680';
                        break;
                    case '3160' :
                        p.lm = '2750';
                        break;
                    case '3390' :
                        p.lm = '2950';
                        break;
                    case '2910' :
                        p.lm = '2530';
                        break;
                    case '2750' :
                        p.lm = '2390';
                        break;
                    case '1960' :
                        p.lm = '1700';
                        break;
                    case '1990' :
                        p.lm = '1730';
                        break;
                    case '2970' :
                        p.lm = '2585';
                        break;
                    case '2090' :
                        p.lm = '1810';
                        break;
                }

                console.log('nella prima funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

