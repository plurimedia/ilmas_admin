var process = require('./massive.js');
var modello = '5b2b85760aa32f0400e9ad59';
var query = { modello : modello };
var tag = 'dani_20180703_02';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'N', 7);
                p.fascio = '40';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["N"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'S', 7);
                p.fascio = '55';

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["B"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'C', 6);

                p.w = 17; //potenza
                p.ma = 500; //corrente

                switch (p.lm) {
                    case '1705' :
                        p.lm = '2360';
                        break;
                    case '1790' :
                        p.lm = '2480';
                        break;
                    case '1880' :
                        p.lm = '2600';
                        break;
                    case '1460' :
                        p.lm = '2020';
                        break;
                    case '1535' :
                        p.lm = '2125';
                        break;
                    case '1610' :
                        p.lm = '2235';
                        break;
                    case '1530' :
                        p.lm = '2140';
                        break;
                    case '1450' :
                        p.lm = '2020';
                        break;
                }
                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice += 'E';
                p.alimentatore = 'Driver standard';
                p.alimentatore_incluso = true;

                if (process.hasCharAt(p.codice, 'R', 5) ||
                    process.hasCharAt(p.codice, '5', 5) ||
                    process.hasCharAt(p.codice, '6', 5) ||
                    process.hasCharAt(p.codice, 'Z', 5) ||
                    process.hasCharAt(p.codice, '7', 5) ||
                    process.hasCharAt(p.codice, '8', 5)
                )
                    p.prezzo = 130;

                if (process.hasCharAt(p.codice, 'W', 5))
                    p.prezzo = 150;

                if (process.hasCharAt(p.codice, 'Q', 5))
                    p.prezzo = 165;

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["E"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);

                switch (p.prezzo) {
                    case 130 :
                        p.prezzo = 162;
                        break;
                    case 150 :
                        p.prezzo = 182;
                        break;
                    case 165 :
                        p.prezzo = 197;
                        break;
                }

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform6 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);
                p.dimmerabile = true;

                console.log('nella sesta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

 1. Cancellare tutti i codici del modello ad eccezione di questi 8 codici:
 0516RBE1
 05165BE1
 05166BE1
 0516ZBE1
 05167BE1
 05168BE1
 0516WBE1
 0516QBE1
 2. A partire dai codici rimasti dal PUNTO1 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 codice che diventa all’settimo carattere = N
 fascio = 40
 3.A partire da tutti i codici creati sia nel punto 2  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 codice che diventa all’settimo carattere  N  T
 fascio = 60
 4.A partire da tutti i codici creati sia nel punto 2 che nel punto 3 e quelli rimasti nel punto 1 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 6° carattere B  C
 Potenza: 17
 Corrente: 500
 Fl. Lum. Nom:
 17052360
 17902480
 18802600
 14602020
 15352125
 16102235
 15302140
 1450 2020

 5. A partire da tutti i codici creati sia nel punto 1 che nel  punto 2 che nel punto 3  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =E
 Alimentatore: Driver standard
 Flag Alimentatore incluso =TRUE
 Prezzo:
 5° carattere =(R AND 5 AND 6 AND Z AND 7 AND 8): 130
 5° carattere = W   :  150
 5° carattere = qqqqqq  Q   :  165

 6. A partire da tutti i codici creati sia nel punto 5 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =       ED
 Alimentatore: Driver dimmerabile
 Flag Dimm.=TRUE
 Prezzo:
 130 162
 150182
 165197

 7. A partire da tutti i codici creati sia nel punto 6 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =       DL
 Alimentatore: Driver Dali
 Flag Dimm.=TRUE

 */