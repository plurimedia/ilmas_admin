var process = require('./massive.js');
var modello = '5b2214b8cf69c604004c7f00';
var query = { modello : modello };
var tag = 'dani_20180703_04';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'N', 7);
                p.fascio = '40';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["N"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'S', 7);
                p.fascio = '55';

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["C"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 6);

                p.w = 24; //potenza
                p.ma = 700; //corrente

                switch (p.lm) {
                    case '2360' :
                        if (process.hasCharAt(p.codice, 'R', 5)) {
                            p.lm = '3170';
                            break;
                        }
                    case '2480' :
                        if (process.hasCharAt(p.codice, '5', 5)) {
                            p.lm = '3330';
                            break;
                        }
                    case '2600' :
                        if (process.hasCharAt(p.codice, '6', 5)) {
                            p.lm = '3500';
                            break;
                        }
                    case '2020' :
                        if (process.hasCharAt(p.codice, 'Z', 5)) {
                            p.lm = '2710';
                            break;
                        }
                    case '2125' :
                        if (process.hasCharAt(p.codice, '7', 5)) {
                            p.lm = '2860';
                            break;
                        }
                    case '2235' :
                        if (process.hasCharAt(p.codice, '8', 5)) {
                            p.lm = '3000';
                            break;
                        }
                    case '2140' :
                        if (process.hasCharAt(p.codice, 'W', 5)) {
                            p.lm = '2910';
                            break;
                        }
                    case '2020' :
                        if (process.hasCharAt(p.codice, 'Q', 5)) {
                            p.lm = '2750';
                            break;
                        }
                }
                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'E', 6);

                p.w = 32; //potenza
                p.ma = 800; //corrente

                switch (p.lm) {
                    case '3170' :
                        if (process.hasCharAt(p.codice, 'R', 5)) {
                            p.lm = '3905';
                            break;
                        }
                    case '3330' :
                        if (process.hasCharAt(p.codice, '5', 5)) {
                            p.lm = '4105';
                            break;
                        }
                    case '3500' :
                        if (process.hasCharAt(p.codice, '6', 5)) {
                            p.lm = '4310';
                            break;
                        }
                    case '2710' :
                        if (process.hasCharAt(p.codice, 'Z', 5)) {
                            p.lm = '3345';
                            break;
                        }
                    case '2860' :
                        if (process.hasCharAt(p.codice, '7', 5)) {
                            p.lm = '3515';
                            break;
                        }
                    case '3000' :
                        if (process.hasCharAt(p.codice, '8', 5)) {
                            p.lm = '3695';
                            break;
                        }
                    case '2910' :
                        if (process.hasCharAt(p.codice, 'W', 5)) {
                            p.lm = '3640';
                            break;
                        }
                    case '2750' :
                        if (process.hasCharAt(p.codice, 'Q', 5)) {
                            p.lm = '3440';
                            break;
                        }
                }

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice += 'E';
                p.alimentatore = 'Driver standard';
                p.alimentatore_incluso = true;

                if ((process.hasCharAt(p.codice, 'R', 5) ||
                    process.hasCharAt(p.codice, '5', 5) ||
                    process.hasCharAt(p.codice, '6', 5) ||
                    process.hasCharAt(p.codice, 'Z', 5) ||
                    process.hasCharAt(p.codice, '7', 5) ||
                    process.hasCharAt(p.codice, '8', 5)) &&
                    process.hasCharAt(p.codice, 'C', 6)
                )
                    p.prezzo = 144;

                if ((process.hasCharAt(p.codice, 'R', 5) ||
                    process.hasCharAt(p.codice, '5', 5) ||
                    process.hasCharAt(p.codice, '6', 5) ||
                    process.hasCharAt(p.codice, 'Z', 5) ||
                    process.hasCharAt(p.codice, '7', 5) ||
                    process.hasCharAt(p.codice, '8', 5)) &&
                    process.hasCharAt(p.codice, 'D', 6)
                )
                    p.prezzo = 153;

                if ((process.hasCharAt(p.codice, 'R', 5) ||
                    process.hasCharAt(p.codice, '5', 5) ||
                    process.hasCharAt(p.codice, '6', 5) ||
                    process.hasCharAt(p.codice, 'Z', 5) ||
                    process.hasCharAt(p.codice, '7', 5) ||
                    process.hasCharAt(p.codice, '8', 5)) &&
                    process.hasCharAt(p.codice, 'E', 6)
                )
                    p.prezzo = 153;

                if (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'C', 6))
                    p.prezzo = 164;

                if (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'D', 6))
                    p.prezzo = 173;

                if (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'E', 6))
                    p.prezzo = 173;

                if (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'C', 6))
                    p.prezzo = 179;

                if (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'D', 6))
                    p.prezzo = 188;

                if (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'E', 6))
                    p.prezzo = 188;

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform6 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["E"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);

                switch (p.prezzo) {
                    case 144 :
                        p.prezzo = 176;
                        break;
                    case 153 :
                        p.prezzo = 176;
                        break;
                    case 163 :
                        p.prezzo = 176;
                        break;
                    case 164 :
                        p.prezzo = 196;
                        break;
                    case 173 :
                        p.prezzo = 196;
                        break;
                    case 183 :
                        p.prezzo = 196;
                        break;
                    case 179 :
                        p.prezzo = 211;
                        break;
                    case 188 :
                        p.prezzo = 211;
                        break;
                    case 198 :
                        p.prezzo = 211;
                        break;
                }

                console.log('nella sesta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform7 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);
                p.dimmerabile = true;

                console.log('nella settima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*


 1. Cancellare tutti i codici del modello ad eccezione di questi 8 codici:
 0518RCE1
 05185CE1
 05186CE1
 0518ZCE1
 05187CE1
 05188CE1
 0518WCE1
 0518QCE1
 2. A partire dai codici rimasti dal PUNTO1 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 codice che diventa all’settimo carattere = N
 fascio = 40
 3.A partire da tutti i codici creati sia nel punto 2  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 codice che diventa all’settimo carattere  N  T
 fascio = 60
 4.A partire da tutti i codici creati sia nel punto 2 che nel punto 3 e quelli rimasti nel punto 1 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 6° carattere C  D
 Potenza: 24
 Corrente: 700
 Fl. Lum. Nom:
 2360 3170
 24803330
 2600 3500
 20202710
 21252860
 22353000
 21402910
 20202750
 5.A partire da tutti i codici creati sia nel punto  4 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 6° carattere D  S
 Potenza: 28
 Corrente: 800
 Fl. Lum. Nom:
 31703545
 33303730
 35003915
 27103035
 28603195
 30003360
 29103280
 27503100


 6. A partire da tutti i codici creati sia nei punti 1-2-3-4-5  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =E
 Alimentatore: Driver standard
 Flag Alimentatore incluso =TRUE
 Prezzo:
 5° carattere AND 6° carattere =(RC AND 5C AND 6C AND ZC AND 7C AND 8C): 144
 5° carattere AND 6° carattere =(RD AND 5D AND 6D AND ZD AND 7D AND 8D): 153
 5° carattere AND 6° carattere =(RS AND 5S AND 6S AND ZS AND 7S AND 8S): 163
 5° carattere AND 6° carattere = WC   :  164
 5° carattere AND 6° carattere = WD   :  173
 5° carattere AND 6° carattere = WQ:  183
 5° carattere AND 6° carattere = QC   :  179
 5° carattere AND 6° carattere = QD   :  188
 5° carattere AND 6° carattere = QS:  198


 7. A partire da tutti i codici creati sia nel punto 6 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =       ED
 Alimentatore: Driver dimmerabile
 Flag Dimm.=TRUE
 Prezzo:
 144 176
 153176
 163176
 164196
 173196
 183196
 179211
 188211
 198211

 8. A partire da tutti i codici creati sia nel punto 7 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =       DL
 Alimentatore: Driver Dali
 Flag Dimm.=TRUE

 */