var process = require('./massive.js');
var modello = '59afed5ad9bffb040079b4d1';
var query = { modello : modello };
var tag = 'dani_20170914_05';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                delete p.adattatore_it
                delete p.adattatore_en
                p.ugr = '<19';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                switch (p.prezzo) {
                    case 120 :
                        p.prezzo = 112;
                        break;
                    case 127 :
                        p.prezzo = 112;
                        break;
                    case 140 :
                        p.prezzo = 132;
                        break;
                    case 147 :
                        p.prezzo = 132;
                        break;
                }

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
 Di tutti i codici del modello rimasti modificare  i seguenti  campi:
 Adattatore (IT): “vuoto”
 Adattatore (EN): “vuoto”
 UGR: <19
 4. A partire da tutti i codici del modello modificare il campo prezzo con la seguente regola:
 120  112
 127112
 140132
 147132
 */