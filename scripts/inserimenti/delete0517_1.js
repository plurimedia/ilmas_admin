/* 
    lanciare con: 
	node --max-old-space-size=2000 scripts/inserimenti/delete1006_1.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    CategoriaSchema = require(process.cwd() + '/server/models/categoria'),
    ModelloSchema = require(process.cwd() + '/server/models/modello'),
    ProdottoSchema = require(process.cwd() + '/server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    ObjectId = require('mongodb').ObjectID,
	aggiornati = 0;

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console
    console.log(db)

    var query =
            { 
	            modello:ObjectId('5b2ba23f0aa32f0400e9ad79')
            }

    console.log("query", query);

    Prodotto.find(query).lean()
    .exec(

    	function(err,prodotti){
        
            console.log('lunghezza '+prodotti.length+' prodotti')
        
  		    mongoose.connection.close();

  		    console.log('connection close') 

    		//cancelliamo tutti i record tranne quelli elencati
    		prodotti = _.filter(prodotti, function(p){
    			return (p.codice != '0517RBE1' &&
                        p.codice != '05175BE1' &&
                        p.codice != '05176BE1' &&
                        p.codice != '0517ZBE1' &&
                        p.codice != '05177BE1' &&
                        p.codice != '05178BE1' &&
                        p.codice != '0517WBE1' &&
                        p.codice != '0517QBE1')

    		})
  		 
  		    var process = require('../../scripts/inserimenti/deleteGeneric.js');
            process.execute(prodotti);
        
        }
    )
});


