var process = require('./massive.js'),
    ObjectId = require('mongodb').ObjectID;
var modello = '59afed5ad9bffb040079b4d1'; //codice del modello 0595 da cui partiamo
var query = { modello : modello };
var tag = 'dani_20180703_05';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.modello = ObjectId('5afed6b91035860400702219');
                //non so perche ma non va : lanciare dopo
                //db.Prodotto.update({"codice": /0585.*/},{$set:{ modello:ObjectId('5afed6b91035860400702219') }},{multi:true})
                p.codice = process.replaceCharAt(p.codice, '8', 3);

                switch (p.prezzo) {
                    case 60 :
                        p.prezzo = 65;
                        break;
                    case 80 :
                        p.prezzo = 85;
                        break;
                    case 100 :
                        p.prezzo = 105;
                        break;
                    case 112 :
                        p.prezzo = 117;
                        break;
                    case 132 :
                        p.prezzo = 137;
                        break;
                }

                p.foro = '70X70';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

 1. A partire dai codici del modello 0595, duplicare i report modificando i seguenti campi:
 MODELLO: 0585 Smart Q XS
 codice che diventa al terzo  carattere = 8
 prezzo:
 60  65
 80 85
 100105
 112117
 132137
 Foro di incasso: 70X70
 */