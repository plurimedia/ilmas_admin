var _ = require('underscore'),
    async = require('async'),
    q = require('q'),
    ObjectId = require('mongodb').ObjectID,
    conn_db_test = 'mongodb://plurimedia:Canonico27!@ds139590-a0.mlab.com:39590,ds139590-a1.mlab.com:39590/heroku_fpbvn56p?replicaSet=rs-ds139590',
    conn_db_prod = 'mongodb://plurimedia:Canonico27!@ds057505-a0.mongolab.com:57505,ds057505-a1.mongolab.com:57505/heroku_rlzkx1m6?replicaSet=rs-ds057505',
    MongoClient = require('mongodb').MongoClient,
    db_test,
    db_prod,
    modello// = '5743f1d166923a030050afd0',
    //query = {modello:ObjectId(modello)}
    ;

function connect () {
    return new Promise(function(resolve, reject){
        MongoClient.connect(conn_db_test)
            .then(function(c) {
                console.log('connesso a test');
                db_test = c;
                MongoClient.connect(conn_db_prod)
                    .then(function(c2) {
                        console.log('connesso a prod');
                        db_prod = c2;
                        resolve();
                    })
                    .catch(function (err) { console.log('errore connessione a prod', err); reject(err); });;
            })
            .catch(function (err) { console.log('errore connessione a test', err); reject(err); });
    });
}

function disconnect () {
    return new Promise(function(resolve, reject) {
        if (db_test) {
            db_test.close(function() {
                console.log('chiudo connessione test');
                if (db_prod) {
                    db_prod.close(function() {
                        console.log('chiudo connessione prod');
                        resolve();
                    });
                }
            });
        } else {
            if (db_prod) {
                db_prod.close(function () {
                    console.log('chiudo connessione prod');
                    resolve();
                });
            }
        }
    });
}

function deleteProdottiTest () {
    let query = {modello:ObjectId(modello)};
    return new Promise(function(resolve, reject) {
        db_test.collection('Prodotto').remove(query)
            .then(function(obj) { console.log(obj.result.n + " document(s) deleted"); resolve(); })
            .catch(function(err) { reject(err); });
    });
}

function deleteProdottiProd () {
    let query = {modello:ObjectId(modello)};
    return new Promise(function(resolve, reject) {
        db_prod.collection('Prodotto').remove(query)
            .then(function(obj) { console.log(obj.result.n + " document(s) deleted"); resolve(); })
            .catch(function(err) { reject(err); });
    });
}

function deleteProdottiBackupTest () {
    return new Promise(function(resolve, reject) {
        db_test.collection('Prodotto_bck').remove(query)
            .then(function(obj) { console.log(obj.result.n + " document(s) deleted"); resolve(); })
            .catch(function(err) { reject(err); });
    });
}

function deleteProdottiBackupProd () {
    return new Promise(function(resolve, reject) {
        db_prod.collection('Prodotto_bck').remove(query)
            .then(function(obj) { console.log(obj.result.n + " document(s) deleted"); resolve(); })
            .catch(function(err) { reject(err); });
    });
}

function importFromProd () {
    let query = {modello:ObjectId(modello)};
    return new Promise(function(resolve, reject) {
        /*db_prod.collection('Prodotto').find(query).count(function(e, count) {
            if (e) reject(e);

            let limit = 100;
            let numero_giri = Math.round(count / limit);
            console.log('documenti', count, 'numero giri', numero_giri);
            let i = 0, coll = [];
            while (i <= numero_giri) {
                let skip = limit * i;
                coll.push(skip);
                i++;
            }
            async.eachSeries(coll, function(skip, cb) {
                db_prod.collection('Prodotto').find(query).skip(skip).limit(limit)
                    .toArray(function(err, res) {
                        if (err) reject(err);

                        console.log(skip, limit, 'risultati ' + res.length, query);
                        db_test.collection('Prodotto').insertMany(res, function (err, risultato) {
                            if (err) reject(err);

                            if (risultato)
                                console.log('inseriti', risultato.insertedCount);
                            else
                                console.log('no risultato.........');

                            cb();

                        });
                    });
            });
            //resolve();
        })*/
        db_prod.collection('Prodotto').find(query)
            .toArray(function(err, res) {
                if (err) reject(err);
                console.log('risultati '+res.length, query);
                db_test.collection('Prodotto').insertMany(res, function(err, risultato) {
                    if (err) reject(err);

                    if (risultato)
                        console.log('inseriti', risultato.insertedCount);
                    else
                        console.log('no risultato.........');

                    resolve();
                });
                /*async.each(res, function(p,cb){

                    delete p._id;

                    db_test.collection('Prodotto').insertOne(p, function(err, risultato) {
                        if (err) reject(err);

                        console.log('inserito uno', p.codice, risultato);
                    });

                    cb();
                }, function(err){
                    if (err) reject(err);

                    resolve();
                });*/
            })
    });
}

function backupTestOnLocalTable () {
    let query = {modello:ObjectId(modello)};
    return new Promise(function(resolve, reject) {
        db_test.collection('Prodotto').find(query)
            .toArray(function(err, res) {
                if (err) reject(err);
                console.log('risultati '+res.length, query);

                db_test.collection('Prodotto_bck').insertMany(res, function(err, risultato) {
                    if (err) reject(err);

                    console.log('inseriti', risultato.insertedCount);
                    resolve();
                });

            });
    });
}

function backupProdOnLocalTable () {
    let query = {modello:ObjectId(modello)};
    return new Promise(function(resolve, reject) {
        db_prod.collection('Prodotto').find(query)
            .toArray(function(err, res) {
                if (err) reject(err);
                console.log('risultati '+res.length, query);

                db_prod.collection('Prodotto_bck').insertMany(res, function(err, risultato) {
                    if (err) reject(err);

                    console.log('inseriti', risultato.insertedCount);
                    resolve();
                });

            });
    });
}

function restoreTestFromLocalTable () {
    let query = {modello:ObjectId(modello)};
    return new Promise(function(resolve, reject) {
        db_test.collection('Prodotto_bck').find(query)
            .toArray(function(err, res) {
                if (err) reject(err);
                console.log('risultati '+res.length, query);

                db_test.collection('Prodotto').insertMany(res, function(err, risultato) {
                    if (err) reject(err);

                    console.log('inseriti', risultato.insertedCount);
                    resolve();
                });

            });
    });
}

function restoreProdFromLocalTable () {
    let query = {modello:ObjectId(modello)};
    return new Promise(function(resolve, reject) {
        db_prod.collection('Prodotto_bck').find(query)
            .toArray(function(err, res) {
                if (err) reject(err);
                console.log('risultati '+res.length, query);

                db_prod.collection('Prodotto').insertMany(res, function(err, risultato) {
                    if (err) reject(err);

                    console.log('inseriti', risultato.insertedCount);
                    resolve();
                });

            });
    });
}

//exports.go = function (q) { //q = query di estrazione e cancellazione
    //TEST
    //caso d'uso 1 : cancello in test e importo da prod
    //connect().then(deleteProdottiTest).then(importFromProd).then(disconnect);
    //caso d'uso 2 : cancello in test, importo da prod, backuppo test
    //connect().then(deleteProdottiTest).then(importFromProd).then(backupTestOnLocalTable).then(disconnect);
    //caso d'uso 3 : cancello in test, restore test
    //connect().then(deleteProdottiTest).then(restoreTestFromLocalTable).then(disconnect);

    //PROD
    //caso d'uso 4 : backup in prod
    //connect().then(backupProdOnLocalTable).then(disconnect);
    //caso d'uso 5 : restore in prod TODO forse qui ci sarebbe anche da mettere la cancellazione prima
    //connect().then(restoreProdFromLocalTable).then(disconnect);
//}

exports.caso1 = function (m) {
    var deferred = q.defer();
    //console.log(m);
    modello = m;
    connect().then(deleteProdottiTest).then(importFromProd).then(disconnect).then(function() { deferred.resolve() });

    return deferred.promise;
}

exports.caso2 = function (m) {
    var deferred = q.defer();
    //console.log(m);
    modello = m;
    connect().then(deleteProdottiTest).then(importFromProd).then(backupTestOnLocalTable).then(disconnect).then(function() { deferred.resolve() });

    return deferred.promise;
}

exports.caso3 = function (m) {
    var deferred = q.defer();
    //console.log(m);
    modello = m;
    connect().then(deleteProdottiTest).then(restoreTestFromLocalTable).then(disconnect).then(function() { deferred.resolve() });

    return deferred.promise;
}

exports.caso4 = function (m) {
    var deferred = q.defer();
    //console.log(m);
    modello = m;
    connect().then(backupProdOnLocalTable).then(disconnect).then(function() { deferred.resolve() });

    return deferred.promise;
}

exports.caso5 = function (m) {
    var deferred = q.defer();
    //console.log(m);
    modello = m;
    connect().then(deleteProdottiProd).then(restoreProdFromLocalTable).then(disconnect).then(function() { deferred.resolve() });

    return deferred.promise;
}
