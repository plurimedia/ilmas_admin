var process = require('./massive.js');
var modello = '5b2b5cef694a5804009cae73';
var query = { modello : modello };
var tag = 'dani_20180703_01';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'N', 7);
                p.fascio = '40';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["N"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'S', 7);
                p.fascio = '55';

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["C"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 6);

                p.w = 24; //potenza
                p.ma = 700; //corrente

                switch (p.lm) {
                    case '2360' :
                        if (process.hasCharAt(p.codice, 'R', 5)) {
                            p.lm = '3170';
                            break;
                        }
                    case '2480' :
                        if (process.hasCharAt(p.codice, '5', 5)) {
                            p.lm = '3330';
                            break;
                        }
                    case '2600' :
                        if (process.hasCharAt(p.codice, '6', 5)) {
                            p.lm = '3500';
                            break;
                        }
                    case '2020' :
                        if (process.hasCharAt(p.codice, 'Z', 5)) {
                            p.lm = '2710';
                            break;
                        }
                    case '2125' :
                        if (process.hasCharAt(p.codice, '7', 5)) {
                            p.lm = '2860';
                            break;
                        }
                    case '2235' :
                        if (process.hasCharAt(p.codice, '8', 5)) {
                            p.lm = '3000';
                            break;
                        }
                    case '2140' :
                        if (process.hasCharAt(p.codice, 'W', 5)) {
                            p.lm = '2910';
                            break;
                        }
                    case '2020' :
                        if (process.hasCharAt(p.codice, 'Q', 5)) {
                            p.lm = '2750';
                            break;
                        }
                }
                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'S', 6);

                p.w = 28; //potenza
                p.ma = 800; //corrente

                switch (p.lm) {
                    case '3170' :
                        if (process.hasCharAt(p.codice, 'R', 5)) {
                            p.lm = '3545';
                            break;
                        }
                    case '3330' :
                        if (process.hasCharAt(p.codice, '5', 5)) {
                            p.lm = '3730';
                            break;
                        }
                    case '3500' :
                        if (process.hasCharAt(p.codice, '6', 5)) {
                            p.lm = '3915';
                            break;
                        }
                    case '2710' :
                        if (process.hasCharAt(p.codice, 'Z', 5)) {
                            p.lm = '3035';
                            break;
                        }
                    case '2860' :
                        if (process.hasCharAt(p.codice, '7', 5)) {
                            p.lm = '3195';
                            break;
                        }
                    case '3000' :
                        if (process.hasCharAt(p.codice, '8', 5)) {
                            p.lm = '3360';
                            break;
                        }
                    case '2910' :
                        if (process.hasCharAt(p.codice, 'W', 5)) {
                            p.lm = '3280';
                            break;
                        }
                    case '2750' :
                        if (process.hasCharAt(p.codice, 'Q', 5)) {
                            p.lm = '3100';
                            break;
                        }
                }

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice += 'E';
                p.alimentatore = 'Driver standard';
                p.alimentatore_incluso = true;

                if ((process.hasCharAt(p.codice, 'R', 5) ||
                    process.hasCharAt(p.codice, '5', 5) ||
                    process.hasCharAt(p.codice, '6', 5) ||
                    process.hasCharAt(p.codice, 'Z', 5) ||
                    process.hasCharAt(p.codice, '7', 5) ||
                    process.hasCharAt(p.codice, '8', 5)) &&
                    process.hasCharAt(p.codice, 'C', 6)
                )
                    p.prezzo = 132;

                if ((process.hasCharAt(p.codice, 'R', 5) ||
                    process.hasCharAt(p.codice, '5', 5) ||
                    process.hasCharAt(p.codice, '6', 5) ||
                    process.hasCharAt(p.codice, 'Z', 5) ||
                    process.hasCharAt(p.codice, '7', 5) ||
                    process.hasCharAt(p.codice, '8', 5)) &&
                    process.hasCharAt(p.codice, 'D', 6)
                )
                    p.prezzo = 141;

                if ((process.hasCharAt(p.codice, 'R', 5) ||
                    process.hasCharAt(p.codice, '5', 5) ||
                    process.hasCharAt(p.codice, '6', 5) ||
                    process.hasCharAt(p.codice, 'Z', 5) ||
                    process.hasCharAt(p.codice, '7', 5) ||
                    process.hasCharAt(p.codice, '8', 5)) &&
                    process.hasCharAt(p.codice, 'S', 6)
                )
                    p.prezzo = 141;

                if (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'C', 6))
                    p.prezzo = 152;

                if (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'D', 6))
                    p.prezzo = 161;

                if (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'S', 6))
                    p.prezzo = 161;

                if (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'C', 6))
                    p.prezzo = 167;

                if (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'D', 6))
                    p.prezzo = 176;

                if (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'S', 6))
                    p.prezzo = 176;

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform6 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["E"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);

                switch (p.prezzo) {
                    case 132 :
                        p.prezzo = 164;
                        break;
                    case 141 :
                        p.prezzo = 164;
                        break;
                    case 151 :
                        p.prezzo = 164;
                        break;
                    case 152 :
                        p.prezzo = 184;
                        break;
                    case 161 :
                        p.prezzo = 184;
                        break;
                    case 171 :
                        p.prezzo = 184;
                        break;
                    case 167 :
                        p.prezzo = 199;
                        break;
                    case 176 :
                        p.prezzo = 199;
                        break;
                    case 186 :
                        p.prezzo = 199;
                        break;
                }

                console.log('nella sesta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform7 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);
                p.dimmerabile = true;

                console.log('nella settima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

 1. Cancellare tutti i codici del modello ad eccezione di questi 8 codici:
 0528RBE1
 05285BE1
 05286BE1
 0528ZBE1
 05287BE1
 05288BE1
 0528WBE1
 0528QBE1
 2. A partire dai codici rimasti dal PUNTO1 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 codice che diventa all’settimo carattere = N
 fascio = 40
 3.A partire da tutti i codici creati sia nel punto 2  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 codice che diventa all’settimo carattere  N  T
 fascio = 60
 4.A partire da tutti i codici creati sia nel punto 2 che nel punto 3 e quelli rimasti nel punto 1 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 6° carattere C  D
 Potenza: 24
 Corrente: 700
 Fl. Lum. Nom:
 2360 3170
 24803330
 2600 3500
 20202710
 21252860
 22353000
 21402910
 20202750
 5.A partire da tutti i codici creati sia nel punto  4 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 6° carattere D  S
 Potenza: 28
 Corrente: 800
 Fl. Lum. Nom:
 31703545
 33303730
 35003915
 27103035
 28603195
 30003360
 29103280
 27503100


 6. A partire da tutti i codici creati sia nei punti 1-2-3-4-5  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =E
 Alimentatore: Driver standard
 Flag Alimentatore incluso =TRUE
 Prezzo:
 5° carattere AND 6° carattere =(RC AND 5C AND 6C AND ZC AND 7C AND 8C): 132
 5° carattere AND 6° carattere =(RD AND 5D AND 6D AND ZD AND 7D AND 8D): 141
 5° carattere AND 6° carattere =(RS AND 5S AND 6S AND ZS AND 7S AND 8S): 151
 5° carattere AND 6° carattere = WC   :  152
 5° carattere AND 6° carattere = WD   :  161
 5° carattere AND 6° carattere = WQ:  171
 5° carattere AND 6° carattere = QC   :  167
 5° carattere AND 6° carattere = QD   :  176
 5° carattere AND 6° carattere = QS:  186

 7. A partire da tutti i codici creati sia nel punto 6 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =       ED
 Alimentatore: Driver dimmerabile
 Flag Dimm.=TRUE
 Prezzo:
 132  164
 141  164
 151  164
 152184
 161  184
 171  184
 167  199
 176  199
 186  199

 8. A partire da tutti i codici creati sia nel punto 7 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =       DL
 Alimentatore: Driver Dali
 Flag Dimm.=TRUE

 */