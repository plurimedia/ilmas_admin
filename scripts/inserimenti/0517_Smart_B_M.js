var process = require('./massive.js');
var modello = '5b2ba23f0aa32f0400e9ad79';
var query = { modello : modello };
var tag = 'dani_20180703_03';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'N', 7);
                p.fascio = '40';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["N"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'S', 7);
                p.fascio = '55';

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["B"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'C', 6);

                p.w = 17; //potenza
                p.ma = 500; //corrente

                switch (p.lm) {
                    case '1705' :
                        p.lm = '2360';
                        break;
                    case '1790' :
                        p.lm = '2480';
                        break;
                    case '1880' :
                        p.lm = '2600';
                        break;
                    case '1460' :
                        p.lm = '2020';
                        break;
                    case '1535' :
                        p.lm = '2125';
                        break;
                    case '1610' :
                        p.lm = '2235';
                        break;
                    case '1530' :
                        p.lm = '2140';
                        break;
                    case '1450' :
                        p.lm = '2020';
                        break;
                }
                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["C"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'Q', 6);

                p.w = 21; //potenza
                p.ma = 600; //corrente

                switch (p.lm) {
                    case '2360' :
                        if (process.hasCharAt(p.codice, 'R', 5)) {
                            p.lm = '2775';
                            break;
                        }
                    case '2480' :
                        if (process.hasCharAt(p.codice, '5', 5)) {
                            p.lm = '2915';
                            break;
                        }
                    case '2600' :
                        if (process.hasCharAt(p.codice, '6', 5)) {
                            p.lm = '3060';
                            break;
                        }
                    case '2020':
                        if (process.hasCharAt(p.codice, 'Z', 5)) {
                            p.lm = '2375';
                            break;
                        }
                    case '2125' :
                        if (process.hasCharAt(p.codice, '7', 5)) {
                            p.lm = '2500';
                            break;
                        }
                    case '2235' :
                        if (process.hasCharAt(p.codice, '8', 5)) {
                            p.lm = '2626';
                            break;
                        }
                    case '2140' :
                        if (process.hasCharAt(p.codice, 'W', 5)) {
                            p.lm = '2530';
                            break;
                        }
                    case '2020' :
                        if (process.hasCharAt(p.codice, 'Q', 5)) {
                            p.lm = '2390';
                            break;
                        }
                }

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice += 'E';
                p.alimentatore = 'Driver standard';
                p.alimentatore_incluso = true;

                if ((process.hasCharAt(p.codice, 'R', 5) && process.hasCharAt(p.codice, 'B', 6)) ||
                    (process.hasCharAt(p.codice, 'R', 5) && process.hasCharAt(p.codice, 'C', 6)) ||
                    (process.hasCharAt(p.codice, '5', 5) && process.hasCharAt(p.codice, 'B', 6)) ||
                    (process.hasCharAt(p.codice, '5', 5) && process.hasCharAt(p.codice, 'C', 6)) ||
                    (process.hasCharAt(p.codice, '6', 5) && process.hasCharAt(p.codice, 'B', 6)) ||
                    (process.hasCharAt(p.codice, '6', 5) && process.hasCharAt(p.codice, 'C', 6)) ||
                    (process.hasCharAt(p.codice, 'Z', 5) && process.hasCharAt(p.codice, 'B', 6)) ||
                    (process.hasCharAt(p.codice, 'Z', 5) && process.hasCharAt(p.codice, 'C', 6)) ||
                    (process.hasCharAt(p.codice, '7', 5) && process.hasCharAt(p.codice, 'B', 6)) ||
                    (process.hasCharAt(p.codice, '7', 5) && process.hasCharAt(p.codice, 'C', 6)) ||
                    (process.hasCharAt(p.codice, '8', 5) && process.hasCharAt(p.codice, 'B', 6)) ||
                    (process.hasCharAt(p.codice, '8', 5) && process.hasCharAt(p.codice, 'C', 6))
                )
                    p.prezzo = 140;

                if ((process.hasCharAt(p.codice, 'R', 5) ||
                    process.hasCharAt(p.codice, '5', 5) ||
                    process.hasCharAt(p.codice, '6', 5) ||
                    process.hasCharAt(p.codice, 'Z', 5) ||
                    process.hasCharAt(p.codice, '7', 5) ||
                    process.hasCharAt(p.codice, '8', 5)) &&
                    process.hasCharAt(p.codice, 'Q', 6)
                )
                    p.prezzo = 149;

                if (process.hasCharAt(p.codice, 'W', 5) && (process.hasCharAt(p.codice, 'B', 6) || process.hasCharAt(p.codice, 'C', 6)))
                    p.prezzo = 160;

                if (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'Q', 6))
                    p.prezzo = 169;

                if (process.hasCharAt(p.codice, 'Q', 5) && (process.hasCharAt(p.codice, 'B', 6) || process.hasCharAt(p.codice, 'C', 6)))
                    p.prezzo = 175;

                if (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'Q', 6))
                    p.prezzo = 184;

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform6 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["E"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dimmerabile(p);

                switch (p.prezzo) {
                    case 140 :
                        p.prezzo = 172;
                        break;
                    case 149 :
                        p.prezzo = 172;
                        break;
                    case 160 :
                        p.prezzo = 192;
                        break;
                    case 169 :
                        p.prezzo = 192;
                        break;
                    case 175 :
                        p.prezzo = 207;
                        break;
                    case 184 :
                        p.prezzo = 207;
                        break;
                }

                console.log('nella sesta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform7 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["D"], pos:9},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);
                p.dimmerabile = true;

                console.log('nella settima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

 1. Cancellare tutti i codici del modello ad eccezione di questi 8 codici:
 0517RBE1
 05175BE1
 05176BE1
 0517ZBE1
 05177BE1
 05178BE1
 0517WBE1
 0517QBE1
 2. A partire dai codici rimasti dal PUNTO1 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 codice che diventa all’settimo carattere = N
 fascio = 40
 3.A partire da tutti i codici creati sia nel punto 2  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 codice che diventa all’settimo carattere  N  T
 fascio = 60
 4.A partire da tutti i codici creati sia nel punto 2 che nel punto 3 e quelli rimasti nel punto 1 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 6° carattere B  C
 Potenza: 17
 Corrente: 500
 Fl. Lum. Nom:
 17052360
 17902480
 18802600
 14602020
 15352125
 16102235
 15302140
 1450 2020
 5.A partire da tutti i codici creati sia nel punto  4 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 6° carattere C  Q
 Potenza: 21
 Corrente: 600
 Fl. Lum. Nom:
 2360 2775
 2480 2915
 2600 3060
 2020 2375
 2125 2500
 2235  2626
 2140 2530
 2020 2390


 6. A partire da tutti i codici creati sia nei punti 1-2-3-4-5  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =E
 Alimentatore: Driver standard
 Flag Alimentatore incluso =TRUE
 Prezzo:
 5° carattere AND 6° carattere =(RB AND RC AND 5B AND 5C AND 6B AND 6C AND ZB AND ZC AND 7B AND 7C AND 8B AND 8C): 140
 5° carattere AND 6° carattere =(RQ AND 5Q AND 6Q AND ZQ AND 7Q AND 8Q): 149
 5° carattere AND 6° carattere = WB AND WC   :  160
 5° carattere AND 6° carattere = WQ:  169
 5° carattere AND 6° carattere = qqqqqq  QB AND QC   :  175
 5° carattere AND 6° carattere = qqqqqq  QQ   :  184


 7. A partire da tutti i codici creati sia nel punto 6 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =       ED
 Alimentatore: Driver dimmerabile
 Flag Dimm.=TRUE
 Prezzo:
 140 172
 149  172
 160  192
 169 192
 175 207
 184 207

 8. A partire da tutti i codici creati sia nel punto 7 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 Aggiungere 9° carattere al codice =       DL
 Alimentatore: Driver Dali
 Flag Dimm.=TRUE

 */