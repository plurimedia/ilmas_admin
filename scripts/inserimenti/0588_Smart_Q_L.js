var process = require('./massive.js'),
    ObjectId = require('mongodb').ObjectID;
var modello = '59b00ad6d9bffb040079b531'; //codice del modello 0598 da cui partiamo
var query = { modello : modello };
var tag = 'dani_20180703_08';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.modello = ObjectId('5b3b4829e2fa7104002f394b');
                //non so perche ma non va : lanciare dopo
                //db.Prodotto.update({"codice": /0588.*/},{$set:{ modello:ObjectId('5b3b4829e2fa7104002f394b') }},{multi:true})
                p.codice = process.replaceCharAt(p.codice, '8', 3);

                switch (p.prezzo) {
                    case 105 :
                        p.prezzo = 116;
                        break;
                    case 125 :
                        p.prezzo = 136;
                        break;
                    case 140 :
                        p.prezzo = 151;
                        break;
                    case 125 :
                        p.prezzo = 142;
                        break;
                    case 134 :
                        p.prezzo = 151;
                        break;
                    case 144 :
                        p.prezzo = 161;
                        break;
                    case 157 :
                        p.prezzo = 174;
                        break;
                    case 177 :
                        p.prezzo = 194;
                        break;
                    case 192 :
                        p.prezzo = 209;
                        break;
                }

                p.foro = '142X142';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

 1. A partire dai codici del modello 0598, duplicare i report modificando i seguenti campi:
 MODELLO: 0588 Smart Q M
 codice che diventa al terzo  carattere = 8
 prezzo:
 105  116
 125136
 140151
 125142
 134151
 144161
 157174
 177194
 192209

 Foro di incasso: 142X142
 */