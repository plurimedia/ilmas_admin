var process = require('./massive.js'),
    ObjectId = require('mongodb').ObjectID;
var modello = '59b000f3d9bffb040079b514'; //codice del modello 0597 da cui partiamo
var query = { modello : modello };
var tag = 'dani_20180703_07';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.modello = ObjectId('5b36052a7e73a20400684c66');
                //non so perche ma non va : lanciare dopo
                //db.Prodotto.update({"codice": /0587.*/},{$set:{ modello:ObjectId('5b36052a7e73a20400684c66') }},{multi:true})
                p.codice = process.replaceCharAt(p.codice, '8', 3);

                switch (p.prezzo) {
                    case 95 :
                        p.prezzo = 116;
                        break;
                    case 115 :
                        p.prezzo = 136;
                        break;
                    case 130 :
                        p.prezzo = 151;
                        break;
                    case 115 :
                        p.prezzo = 136;
                        break;
                    case 124 :
                        p.prezzo = 145;
                        break;
                    case 135 :
                        p.prezzo = 156;
                        break;
                    case 144 :
                        p.prezzo = 165;
                        break;
                    case 150 :
                        p.prezzo = 171;
                        break;
                    case 159 :
                        p.prezzo = 180;
                        break;
                    case 147 :
                        p.prezzo = 168;
                        break;
                    case 167 :
                        p.prezzo = 188;
                        break;
                    case 182 :
                        p.prezzo = 203;
                        break;
                }

                p.foro = '115X115';

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

 1. A partire dai codici del modello 0597, duplicare i report modificando i seguenti campi:
 MODELLO: 0587 Smart Q M
 codice che diventa al terzo  carattere = 8
 prezzo:
 95  116
 115 136
 130151
 115 136
 124145
 135156
 144165
 150171
 159180
 147168
 167188
 182203

 Foro di incasso: 115X115
 */