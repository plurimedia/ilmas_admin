var process = require('./massive.js');
var modello = '5a65b145e23c1c0400bd7e9f';
var query = { modello : modello };
var tag = 'dani_20180702_02';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                //p.codice = process.replaceCharAt(p.codice, '2', 8);
                //p.colore = 'Nero';
                p = process.nero(p);

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["B"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'C', 6);

                p.w = 17; //potenza
                p.ma = 500; //corrente

                switch (p.lm) {
                    case '1705' :
                        p.lm = '2360';
                        break;
                    case '1790' :
                        p.lm = '2480';
                        break;
                    case '1880' :
                        p.lm = '2600';
                        break;
                    case '1460' :
                        p.lm = '2020';
                        break;
                    case '1535' :
                        p.lm = '2125';
                        break;
                    case '1610' :
                        p.lm = '2235';
                        break;
                    case '1530' :
                        p.lm = '2140';
                        break;
                    case '1450' :
                        p.lm = '2020';
                        break;
                }

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["C"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 6);
                p.w = 25; //potenza
                p.ma = 700; //corrente

                switch (p.lm) {
                    case '2360' :
                        if (process.hasCharAt(p.codice, 'R', 5)) {
                            p.lm = '3170';
                            break;
                        }
                    case '2480' :
                        if (process.hasCharAt(p.codice, '5', 5)) {
                            p.lm = '3330';
                            break;
                        }
                    case '2600' :
                        if (process.hasCharAt(p.codice, '6', 5)) {
                            p.lm = '3500';
                            break;
                        }
                    case '2020':
                        if (process.hasCharAt(p.codice, 'Z', 5)) {
                            p.lm = '2710';
                            break;
                        }
                    case '2125' :
                        if (process.hasCharAt(p.codice, '7', 5)) {
                            p.lm = '2860';
                            break;
                        }
                    case '2235' :
                        if (process.hasCharAt(p.codice, '8', 5)) {
                            p.lm = '3000';
                            break;
                        }
                    case '2140' :
                        if (process.hasCharAt(p.codice, 'W', 5)) {
                            p.lm = '2910';
                            break;
                        }
                    case '2020' :
                        if (process.hasCharAt(p.codice, 'Q', 5)) {
                            p.lm = '2750';
                            break;
                        }
                }

                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'N', 7);
                p.fascio = 40;

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform5 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["N"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'T', 7);
                p.fascio = 60;

                console.log('nella quinta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*


 1. Cancellare tutti i codici del modello ad eccezione di questi 16 codici:
 2241RBE1
 2241RBE1L
 22415BE1
 22415BE1L
 22416BE1
 22416BE1L
 2241ZBE1
 2241ZBE1L
 22417BE1
 22417BE1L
 22418BE1
 22418BE1L
 2241WBE1
 2241WBE1L
 2241QBE1
 2241QBE1L

 2. A partire dai codici rimasti dal PUNTO1 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 codice che diventa all’ottavo carattere = 2
 colore = Nero
 3. A partire da tutti i codici creati sia nel punto 1 che nel  punto 2  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 6° carattere B  C
 Potenza: 17
 Corrente: 500
 Fl. Lum. Nom:
 1705  2360
 1790  2480
 1880  2600
 1460  2020
 1535  2125
 1610  2235
 1530  2140
 1450  2020

 4. A partire da tutti i codici creati nel punto 3 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 6° carattere C D
 Potenza: 25
 Corrente: 700
 Fl. Lum. Nom:
 2360  3170
 2480  3330
 2600  3500
 2020   2710
 2125  2860
 2235  3000
 2140  2910
 2020  2750

 5. A partire da tutti i codici creati sia nel punto 1 che nel  punto 2 che nel punto 3 che nel punto 4  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 7° carattere E  N
 Fascio: 40
 5 . A partire da tutti i codici creati nel punto 5 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 7° carattere N  T
 Fascio: 60
 */