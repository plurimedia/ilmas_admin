var process = require('./massive.js');
var modello = '5a3a8072652d870400a24c8c';
var query = { modello : modello };
var tag = 'dani_20180702_01';

var transform1 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                //p.codice = process.replaceCharAt(p.codice, '2', 8);
                //p.colore = 'Nero';
                p = process.nero(p);

                console.log('nella prima funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["B"], pos:6},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'C', 6);

                switch (p.les) {
                    case 'SAM 10' :
                        p.w = 18;
                        break;
                    case 'LES 10' :
                        p.w = 19;
                        break;
                };

                p.ma = 500; //corrente

                switch (p.lm) {
                    case '1580' :
                        p.lm = '2095';
                        break;
                    case '1660' :
                        p.lm = '2210';
                        break;
                    case '1745' :
                        p.lm = '2315';
                        break;
                    case '1350' :
                        p.lm = '1790';
                        break;
                    case '1415' :
                        p.lm = '1880';
                        break;
                    case '1495' :
                        p.lm = '1990';
                        break;
                    case '1360' :
                        p.lm = '1820';
                        break;
                }

                console.log('nella seconda funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'N', 7);
                p.fascio = 40;

                console.log('nella terza funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:["N"], pos:7},
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'R', 7);
                p.fascio = 50;

                console.log('nella quarta funzione di trasformazione '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*


 1. Cancellare tutti i codici del modello ad eccezione di questi 14 codici:
 2240RBG1
 2240RBG1L
 22405BG1
 22405BG1L
 22406BG1
 22406BG1L
 2240ZBG1
 2240ZBG1L
 22407BG1
 22407BG1L
 22408BG1
 22408BG1L
 2240YBG1
 2240YBG1L
 2. A partire dai codici rimasti dal PUNTO1 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 codice che diventa all’ottavo carattere = 2
 colore = Nero
 3. A partire da tutti i codici creati sia nel punto 1 che nel  punto 2  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 6° carattere B  C
 Potenza:
 se LES = SAM10    potenza :18
 se LES=LES10    potenza : 19
 Corrente: 500
 Fl. Lum. Nom:
 1580  2095
 1660  2210
 1745  2315
 1350  1790
 1415  1880
 1495   1990
 1360  1820

 4. A partire da tutti i codici creati sia nel punto 1 che nel  punto 2 che nel punto 3  creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 7° carattere G  N
 Fascio: 40
 5 . A partire da tutti i codici creati nel punto 4 creare un nuovo record con le stesse ed identiche caratteristiche del codice padre tranne che per i seguenti campi:
 7° carattere N  R
 Fascio: 50
 */