/* 
    lanciare con: 
	node --max-old-space-size=2000 scripts/inserimenti/delete1006_1.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    CategoriaSchema = require(process.cwd() + '/server/models/categoria'),
    ModelloSchema = require(process.cwd() + '/server/models/modello'),
    ProdottoSchema = require(process.cwd() + '/server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    ObjectId = require('mongodb').ObjectID,
	aggiornati = 0;

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console
    console.log(db)

    var query =
            { 
	            modello:ObjectId('5aa92c028525e50400b87d52')
            }

    console.log("query", query);

    Prodotto.find(query).lean()
    .exec(

    	function(err,prodotti){
        
            console.log('lunghezza '+prodotti.length+' prodotti')
        
  		    mongoose.connection.close();

  		    console.log('connection close') 

    		//cancelliamo tutti i record tranne quelli elencati
    		prodotti = _.filter(prodotti, function(p){
    			return (p.codice != '2242RCE1' &&
                        p.codice != '2242RCE1L' &&
                        p.codice != '22425CE1' &&
                        p.codice != '22425CE1L' &&
                        p.codice != '22426CE1' &&
                        p.codice != '22426CE1L' &&
                        p.codice != '2242ZCE1' &&
                        p.codice != '2242ZCE1L' &&
                        p.codice != '22427CE1' &&
                        p.codice != '22427CE1L' &&
                        p.codice != '22428CE1' &&
                        p.codice != '22428CE1L' &&
                        p.codice != '2242WCE1' &&
                        p.codice != '2242WCE1L' &&
                        p.codice != '2242QCE1' &&
                        p.codice != '2242QCE1L')

    		})
  		 
  		    var process = require('../../scripts/inserimenti/deleteGeneric.js');
            process.execute(prodotti);
        
        }
    )
});


