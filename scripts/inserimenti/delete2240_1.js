/* 
    lanciare con: 
	node --max-old-space-size=2000 scripts/inserimenti/delete1006_1.js;
*/


var CONFIG = require(process.cwd() + '/server/config.js'),
    mongoose = require('mongoose'),
    _ = require('underscore'),
    async = require('async'),
    db = CONFIG.MONGO,
    CategoriaSchema = require(process.cwd() + '/server/models/categoria'),
    ModelloSchema = require(process.cwd() + '/server/models/modello'),
    ProdottoSchema = require(process.cwd() + '/server/models/prodotto'),
    Prodotto = mongoose.model('Prodotto'),
    ObjectId = require('mongodb').ObjectID,
	aggiornati = 0;

mongoose.connect(db);

mongoose.connection.once('open', function() {

    console.log("Connessione aperta, parte il batch."); /// this gets printed to console
    console.log(db)

    var query =
            { 
	            modello:ObjectId('5a3a8072652d870400a24c8c')
            }

    console.log("query", query);

    Prodotto.find(query).lean()
    .exec(

    	function(err,prodotti){
        
            console.log('lunghezza '+prodotti.length+' prodotti')
        
  		    mongoose.connection.close();

  		    console.log('connection close') 

    		//cancelliamo tutti i record tranne quelli elencati
    		prodotti = _.filter(prodotti, function(p){
    			return (p.codice != '2240RBG1' &&
                        p.codice != '2240RBG1L' &&
                        p.codice != '22405BG1' &&
                        p.codice != '22405BG1L' &&
                        p.codice != '22406BG1' &&
                        p.codice != '22406BG1L' &&
                        p.codice != '2240ZBG1' &&
                        p.codice != '2240ZBG1L' &&
                        p.codice != '22407BG1' &&
                        p.codice != '22407BG1L' &&
                        p.codice != '22408BG1' &&
                        p.codice != '22408BG1L' &&
                        p.codice != '2240YBG1' &&
                        p.codice != '2240YBG1L')

    		})
  		 
  		    var process = require('../../scripts/inserimenti/deleteGeneric.js');
            process.execute(prodotti);
        
        }
    )
});


