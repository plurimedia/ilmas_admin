
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 594b8a0f21ef9f04004e4170

node --max-old-space-size=20000 scripts/inserimenti/settembre/24_1335_FLY_MIDI_A.js 
*/

var process = require('./../massive.js');
var modello = '594b8a0f21ef9f04004e4170';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_24';
ObjectId = require('mongodb').ObjectID;


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE','']}],
    transform: [
        {
           // action: 'nuovo_inserimento',
            action: 'modifica_esistente',
            replace_rule: function (p) { 

                if (p.prezzo==171)
                    p.prezzo=138
                else if (p.prezzo==191)
                    p.prezzo=158
                else if (p.prezzo==206)
                    p.prezzo=173

                console.log('24.1 MODIFICA PREZZO:  '+ p.codice);
                return p;
            }
        }
    ]
};
  

var transform2 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione, process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE','']}, {chars:["D"], pos:6}],
    checkDuplicates:true,
     transform: [
        {
           action: 'nuovo_inserimento',
            //action: 'modifica_esistente',
            replace_rule: function (p) { 
                p = process.creaNuovoProdotto(p,'G6')
                p.codice = process.replaceCharAt(p.codice, 'Q', 6)
                p.ma = 600
                p.w = 21 

                switch (p.lm) {
                    case '3640' :
                        p.lm = '3165';
                        break; 
                    case '3735' :
                        p.lm = '3245';
                        break; 
                    case '3890' :
                            p.lm = '3385'; 
                        break; 
                    case '3085' :
                        p.lm = '2680';
                        break;  
                    case '3160' :
                        p.lm = '2750';
                        break;  
                    case '3390' :
                        p.lm = '2950';
                        break;
                    case '2910' :
                        p.lm = '2530';
                        break;
                    case '2750' :
                        p.lm = '2390';
                        break;
                }

                console.log('24.2 DUPLICARE TUTTI I CODICI DEL...:  '+ p.codice);
                return p;
            }
        }
    ]
};


var transform3 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione, process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE']}, {chars:["D"], pos:6}],
     transform: [
        {
           //action: 'nuovo_inserimento',
            action: 'modifica_esistente',
            replace_rule: function (p) { 

               p = process.archiviaConNote(p,'IN39')

                console.log('24.3 METTERE IN ARCHIVIO TUTTI I CODICI...:  '+ p.codice);
                return p;
            }
        }
    ]
};



var transform4 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE','']}],
    checkDuplicates:true,
     transform: [
        {
            action: 'nuovo_inserimento',
            //action: 'modifica_esistente',
            replace_rule: function (p) { 

                p = process.dali(p);

                if (p.prezzo==138){
                    p.prezzo=198
                }
                else if (p.prezzo==158){
                    p.prezzo=218
                }
                else if (p.prezzo==173){
                    p.prezzo=233
                }

                console.log('24.4 METTERE IN ARCHIVIO TUTTI I CODICI...:  '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 24: CAMBIO CODICI, VALORI E PREZZI 1335 FLY  MIDI/A 
24.1-> MODIFICA PREZZO 
         Prezzo:
 171138
191158
206173

24.2  --> DUPLICARE TUTTI I CODICI DEL MODELLO ESCLUSI FLAG CRIPTATI=TRUE
CON 6°CARATTERE D E MODIFICARE COSì I RECORD:
6°CARATTERE: Q
CORRENTE: 600
POTENZA: 21
FL. LUM. NOM:
36403165
37353245
38903385
30852680
31602750
33902950
29102530
27502390

24.3 METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO CON 6° CARATTERE=D

24.4 DUPLICARE TUTTI I CODICI RIMASTI DEL MODELLO, ESCLUDENDO FLAG CRIPTATI=TRUE,  MODIFICANDO I RECORD SEGUENTI:
9°CARATTERE: L
DRIVER: Driver Dali
FLAG DIMMERABILE: TRUE
PREZZO:
 138198
158218
173233
*/
