
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 57eb8bac60ce4b0300262fe7

node --max-old-space-size=20000 scripts/inserimenti/settembre/50_2027_KL_2.js 
*/

var process = require('./../massive.js');
var modello = '57eb8bac60ce4b0300262fe7';
var query = { modello : modello,  criptato: { $ne: true } } ;
var tag = 'dani_20180903_05';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],

    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) { 
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.isGenerazione,
    filterArguments : {gen:['IN39']},
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.creaNuovoProdotto(p, 'G6');

                p.codice = process.ifCharAtReplaceCharAt(p.codice, 'R', 'I', 5);
                p.codice = process.ifCharAtReplaceCharAt(p.codice, '5', 'A', 5);
                p.codice = process.ifCharAtReplaceCharAt(p.codice, '6', 'B', 5);
                p.codice = process.ifCharAtReplaceCharAt(p.codice, 'Z', 'O', 5);
                p.codice = process.ifCharAtReplaceCharAt(p.codice, '7', 'S', 5);
                p.codice = process.ifCharAtReplaceCharAt(p.codice, '8', 'T', 5);

                switch (p.lm) {
                    case '4720' :
                        p.lm = '5350';
                        break;
                    case '4960' :
                        p.lm = '5490';
                        break;
                    case '5200' :
                        p.lm = '5720';
                        break;
                    case '4040' :
                        p.lm = '4540';
                        break;
                    case '4250' :
                        p.lm = '4650';
                        break;
                    case '4470' :
                        p.lm = '4980';
                        break;
                }

                p.les = 'LES 19';

                switch (p.prezzo) {
                    case 450 :
                        p.prezzo = 394;
                        break;
                    case 480 :
                        p.prezzo = 394;
                        break;
                    case 490 :
                        p.prezzo = 394;
                        break;
                    case 520 :
                        if(process.hasCharAt(p.codice, 'W', 5)) {
                            p.prezzo = 434;
                        } else {
                            if(process.hasCharAt(p.codice, 'Q', 5)) {
                                p.prezzo = 464;
                            }
                        }
                        break;
                    case 550 :
                        p.prezzo = 464;
                        break;
                    case 780 :
                        p.prezzo = 724;
                        break;
                    case 810 :
                        p.prezzo = 724;
                        break;
                    case 860 :
                        p.prezzo = 764;
                        break;
                    case 890 :
                        p.prezzo = 764;
                        break; 
                    case 920 :
                        p.prezzo = 794;
                        break;
                    case 950 :
                        p.prezzo = 794;
                        break;
                }

                console.log('modifica prezzo '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

 PUNTO 50: CAMBIO PREZZI E CODICI  2027 KL/2
 5.1  --> METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO ESCLUSI FLAG CRIPTATI=TRUE
 5.2--> DUPLICARE TUTTI I CODICI DEL MODELLO IN ARCHIVIO (ESCLUSO FLAG CRIPTATI=TRUE) E MODIFICARE RECORD:
 5° CARATTERE:
 "R" --> "I"
 "5" --> "A"
 "6" --> "B"
 "Z" --> "O"
 "7" --> "S"
 "8" --> "T"

 FL. LUM. NOM:
 47205350
 49605490
 52005720
 40404540
 42504650
 44704980

 LES:
 SAM15-->LES19

 GENARAZIONE:
 D1-->G6


 Prezzo:
 450394
 480394
 490434
 520 AND 5°CARATTERE “W”434
 520 AND 5°CARATTERE “Q”464
 550464
 780724
 810724
 860764
 890764
 920794
 950794
*/
