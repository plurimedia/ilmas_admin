
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 59aeaee9dd826e040027a43d

node --max-old-space-size=20000 scripts/inserimenti/settembre/22_1334_FLY_MAXI_F112.js 
*/

var process = require('./../massive.js');
var modello = '59aeaee9dd826e040027a43d';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_22';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE','']}],
     transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                switch (p.prezzo) {
                    case 240 :
                        p.prezzo = 212;
                        break; 
                    case 248 :
                        p.prezzo = 212;
                        break; 
                    case 260 :
                           p.prezzo = 232; 
                        break; 
                    case 268 :
                        p.prezzo = 232;
                        break; 
                    case 275 :
                        if( (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'D', 6) ) ){
                            p.prezzo = 247;
                        }
                        if( (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'D', 6) ) ){
                            p.prezzo = 232;
                        }
                        break; 
                    case 283 :
                        if( (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'E', 6) ) ){
                            p.prezzo = 247;
                        }
                        if( (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'E', 6) ) ){
                            p.prezzo = 232;
                        }
                        break; 
                    case 255 :
                        p.prezzo = 212;
                        break; 
                    case 263 :
                        p.prezzo = 212;
                        break;  
                     
                    case 290 :
                        p.prezzo = 247;
                        break;
                    case 298 :
                        p.prezzo = 247;
                        break;
                } 

                console.log('22.1 MODIFICA PREZZO:  '+ p.codice);
                return p;
            }
        }
    ]
}; 
  
 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 22: MODIFICA PREZZI 1334 FLY MAXI F112 (USARE REGOLA PUNTO 12)
22.1 MODIFICA PREZZO:
240212
248212
260232
268232
275 AND “QD”247
283 AND “QE”247
 255212
263212
275 AND “WD”232
283 AND “WE”232
290247
298247
*/
