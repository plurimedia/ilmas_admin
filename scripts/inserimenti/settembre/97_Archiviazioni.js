
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 59b28fda5d5c9e040071fd3e
 5813730d4f8ebc0300f82859
 57f51b9b3a304f03004a3774
 58d296a2ee2bbb04005a94cd
 56b8aaa90de23c95600bd296
 56b8aaa90de23c95600bd290
 56b8aaa90de23c95600bd291
 56b8aaa90de23c95600bd292
 56b8aaa90de23c95600bd293
 56b8aaa90de23c95600bd294
 56b8aaa90de23c95600bd295

 node --max-old-space-size=20000 scripts/inserimenti/settembre/83_Archiviazioni.js;
 */


var process = require('./../massive.js');
var modello = '59b28fda5d5c9e040071fd3e'; // 3265
var query = {modello:modello };
var tag = 'dani_20180905_14';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['ZD','7D','8D','ZB','7B','8B','1B','2B','3B'], pos:5}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

query = {modello:'5813730d4f8ebc0300f82859'}; // 3266
var transform2 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['ZD','7D','8D','ZB','7B','8B','1B','2B','3B'], pos:5}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

query = {modello:'57f51b9b3a304f03004a3774'}; // 3209
var transform3 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['ZD','7D','8D','ZB','7B','8B','1B','2B','3B'], pos:5}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

query = {modello:'58d296a2ee2bbb04005a94cd'}; // 3210
var transform4 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['ZD','7D','8D','ZB','7B','8B','1B','2B','3B'], pos:5}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

query = {modello:'56b8aaa90de23c95600bd296'}; // B835
var transform5 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['ZD','7D','8D','ZB','7B','8B','1B','2B','3B'], pos:5}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

query = {modello:'56b8aaa90de23c95600bd290'}; // 0694
var transform6 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['ZD','7D','8D','ZB','7B','8B','1B','2B','3B'], pos:5}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

query = {modello:'56b8aaa90de23c95600bd291'}; // 0695
var transform7 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['ZD','7D','8D','ZB','7B','8B','1B','2B','3B'], pos:5}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

query = {modello:'56b8aaa90de23c95600bd292'}; // 0696
var transform8 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['ZD','7D','8D','ZB','7B','8B','1B','2B','3B'], pos:5}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

query = {modello:'56b8aaa90de23c95600bd293'}; // 0697
var transform9 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['ZD','7D','8D','ZB','7B','8B','1B','2B','3B'], pos:5}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};


query = {modello:'56b8aaa90de23c95600bd294'}; // 0698
var transform10 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['ZD','7D','8D','ZB','7B','8B','1B','2B','3B'], pos:5}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

query = {modello:'56b8aaa90de23c95600bd295'}; // 0699
var transform11 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['ZD','7D','8D','ZB','7B','8B','1B','2B','3B'], pos:5}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function (err) { return process.execute(transform6) })
    .then(function (err) { return process.execute(transform7) })
    .then(function (err) { return process.execute(transform8) })
    .then(function (err) { return process.execute(transform9) })
    .then(function (err) { return process.execute(transform10) })
    .then(function (err) { return process.execute(transform11) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

