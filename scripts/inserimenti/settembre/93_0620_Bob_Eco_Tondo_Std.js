/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd282

 node --max-old-space-size=20000 scripts/inserimenti/settembre/93_0620_Bob_Eco_Tondo_Std.js;
 */

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd282';
var query = {modello:modello };
var tag = 'dani_20180906_05';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                if (p.prezzo == 121)
                    p.prezzo = 116;
                else if (p.prezzo == 141)
                    p.prezzo = 136;
                else if (p.prezzo == 156)
                    p.prezzo = 151;
                else if (process.hasCharAt(p.codice, 'E', 9)) {
                    if (process.hasCharsAt(p.codice, 'IB', 5) ||
                        process.hasCharsAt(p.codice, 'IC', 5) ||
                        process.hasCharsAt(p.codice, 'AB', 5) ||
                        process.hasCharsAt(p.codice, 'AC', 5) ||
                        process.hasCharsAt(p.codice, 'BB', 5) ||
                        process.hasCharsAt(p.codice, 'BC', 5) ||
                        process.hasCharsAt(p.codice, 'OB', 5) ||
                        process.hasCharsAt(p.codice, 'OC', 5) ||
                        process.hasCharsAt(p.codice, 'SB', 5) ||
                        process.hasCharsAt(p.codice, 'SC', 5) ||
                        process.hasCharsAt(p.codice, 'TB', 5) ||
                        process.hasCharsAt(p.codice, 'TC', 5)) {
                        p.prezzo = 136;
                    } else if (process.hasCharsAt(p.codice, 'ID', 5) ||
                        process.hasCharsAt(p.codice, 'IE', 5) ||
                        process.hasCharsAt(p.codice, 'AD', 5) ||
                        process.hasCharsAt(p.codice, 'AE', 5) ||
                        process.hasCharsAt(p.codice, 'BD', 5) ||
                        process.hasCharsAt(p.codice, 'BE', 5) ||
                        process.hasCharsAt(p.codice, 'OD', 5) ||
                        process.hasCharsAt(p.codice, 'OE', 5) ||
                        process.hasCharsAt(p.codice, 'SD', 5) ||
                        process.hasCharsAt(p.codice, 'SE', 5) ||
                        process.hasCharsAt(p.codice, 'TD', 5) ||
                        process.hasCharsAt(p.codice, 'TE', 5)) {
                        p.prezzo = 145;
                    } else  if (process.hasCharsAt(p.codice, 'WB', 5) ||
                        process.hasCharsAt(p.codice, 'WC', 5)) {
                        p.prezzo = 156;
                    } else  if (process.hasCharsAt(p.codice, 'WD', 5) ||
                        process.hasCharsAt(p.codice, 'WE', 5)) {
                        p.prezzo = 165;
                    } else if (process.hasCharsAt(p.codice, 'QB', 5) ||
                        process.hasCharsAt(p.codice, 'QC', 5)) {
                        p.prezzo = 171;
                    } else if (process.hasCharsAt(p.codice, 'QD', 5) ||
                        process.hasCharsAt(p.codice, 'WE', 5)) {
                        p.prezzo = 180;
                    }
                } else if (process.hasCharAt(p.codice, 'L', 9) || process.hasCharAt(p.codice, 'D', 9)) {
                    if (process.hasCharAt(p.codice, 'I', 5) ||
                        process.hasCharAt(p.codice, 'A', 5) ||
                        process.hasCharAt(p.codice, 'B', 5) ||
                        process.hasCharAt(p.codice, 'O', 5) ||
                        process.hasCharAt(p.codice, 'S', 5) ||
                        process.hasCharAt(p.codice, 'T', 5)) {
                        p.prezzo = 168;
                    } else if (process.hasCharAt(p.codice, 'W', 5)) {
                        p.prezzo = 188;
                    } else if (process.hasCharAt(p.codice, 'Q', 5)) {
                        p.prezzo = 203;
                    }
                }

                console.log('trasf 1'+ p.codice);
                return p;
            }
        }
    ]
};

var transform2_1 = {
    tag: tag,
    query: query,
    filter: [process.filters.isGenerazione, process.filters.hasCharAt],
    filterArguments: [{gen: ['D1', 'D2', 'V1', 'G6', '3HE', '']}, {chars: ['1F'], pos: 5}],
    checkDuplicates: true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.process.ifCharAtReplaceCharAt(p.codice, '2', '5', 5);
                p.process.ifCharAtReplaceCharAt(p.codice, '3', '6', 5);

                switch (p.lm) {
                    case '3300' :
                        p.lm = '4300';
                        break;
                    case '3600' :
                        p.lm = '4500';
                        break;
                }

                switch (p.prezzo) {
                    case 121 :
                        p.prezzo = 100;
                        break;
                    case 169 :
                        p.prezzo = 132;
                        break;
                }
                p.sottocategoria = 'Arled Cob Osram';
                p.moduloled = p.sottocategoria;

                console.log('trasf 2.1 ' + p.codice);
                return p;
            }
        }
    ]
};

var transform2_2 = {
    tag: tag,
    query: query,
    filter: [process.filters.isGenerazione, process.filters.hasCharAt],
    filterArguments: [{gen: ['D1', 'D2', 'V1', 'G6', '3HE', '']}, {chars: ['2F'], pos: 5}],
    checkDuplicates: true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.process.ifCharAtReplaceCharAt(p.codice, '2', '5', 5);
                p.process.ifCharAtReplaceCharAt(p.codice, '3', '6', 5);

                switch (p.lm) {
                    case '3300' :
                        p.lm = '4300';
                        break;
                    case '3600' :
                        p.lm = '4500';
                        break;
                }

                switch (p.prezzo) {
                    case 121 :
                        p.prezzo = 100;
                        break;
                    case 169 :
                        p.prezzo = 132;
                        break;
                }
                p.sottocategoria = 'Arled Cob Osram';
                p.moduloled = p.sottocategoria;

                console.log('trasf 2.2  ' + p.codice);
                return p;
            }
        }
    ]
};

var transform2_3 = {
    tag: tag,
    query: query,
    filter: [process.filters.isGenerazione, process.filters.hasCharAt],
    filterArguments: [{gen: ['D1', 'D2', 'V1', 'G6', '3HE', '']}, {chars: ['3F'], pos: 5}],
    checkDuplicates: true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.process.ifCharAtReplaceCharAt(p.codice, '2', '5', 5);
                p.process.ifCharAtReplaceCharAt(p.codice, '3', '6', 5);

                switch (p.lm) {
                    case '3300' :
                        p.lm = '4300';
                        break;
                    case '3600' :
                        p.lm = '4500';
                        break;
                }

                switch (p.prezzo) {
                    case 121 :
                        p.prezzo = 100;
                        break;
                    case 169 :
                        p.prezzo = 132;
                        break;
                }
                p.sottocategoria = 'Arled Cob Osram';
                p.moduloled = p.sottocategoria;

                console.log('trasf 2.3 ' + p.codice);
                return p;
            }
        }
    ]
};

var transform3_1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['1F'],pos:5}],
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('trasf 3.1  '+ p.codice);
                return p;
            }
        }
    ]
};


var transform3_2 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['2F'],pos:5}],
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('trasf 3.2 '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3_3 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['3F'],pos:5}],
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('trasf 3.3 '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4_1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['2X'],pos:5},{len:8}],
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                delete p.note;
                delete p.ma;
                p.v = '230';
                console.log('trasf 4.1 '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4_2 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['3X'],pos:5},{len:8}],
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                delete p.note;
                delete p.ma;
                p.v = '230';
                console.log('trasf 4.2 '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4_3 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['2Y'],pos:5},{len:8}],
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                delete p.note;
                delete p.ma;
                p.v = '230';
                console.log('trasf 4.3 '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4_4 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['3Y'],pos:5},{len:8}],
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                delete p.note;
                delete p.ma;
                p.v = '230';
                console.log('trasf 4.4'+ p.codice);
                return p;
            }
        }
    ]
};

var transform4_5 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['7W'],pos:5},{len:8}],
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                delete p.note;
                delete p.ma;
                p.v = '230';
                console.log('trasf 4.5 '+ p.codice);
                return p;
            }
        }
    ]
};

var transform4_6 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['8W'],pos:5},{len:8}],
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                delete p.note;
                delete p.ma;
                p.v = '230';
                console.log('trasf 4.6'+ p.codice);
                return p;
            }
        }
    ]
};


var transform5 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.foro = '160';
                console.log('trasf 5'+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2_1) })
    .then(function (err) { return process.execute(transform2_2) })
    .then(function (err) { return process.execute(transform2_3) })

    .then(function (err) { return process.execute(transform3_1) })
    .then(function (err) { return process.execute(transform3_2) })
    .then(function (err) { return process.execute(transform3_3) })

    .then(function (err) { return process.execute(transform4_1) })
    .then(function (err) { return process.execute(transform4_2) })
    .then(function (err) { return process.execute(transform4_3) })
    .then(function (err) { return process.execute(transform4_4) })
    .then(function (err) { return process.execute(transform4_5) })
    .then(function (err) { return process.execute(transform4_6) })

    .then(function (err) { return process.execute(transform5) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

