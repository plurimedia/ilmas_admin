
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 59b92161d5eaa0040053d681

node --max-old-space-size=20000 scripts/inserimenti/settembre/6_2031_KXL_A_TRACK.js 
*/

var process = require('./../massive.js');
var modello = '59b92161d5eaa0040053d681';
var query = { modello : modello,   criptato: { $ne: true }} ;
var tag = 'ame_20180829_06';


 
var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates:true,
    filter: [ process.filters.hasNotCharAt,process.filters.hasNotCharAt,process.filters.isGenerazione],
    filterArguments : [{chars:["Q"], pos:5},{chars:["T"], pos:5},{gen:['D1','D2','V1','G6', '3HE']}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p = process.creaNuovoProdotto(p,'G6')
                
                p = process.dali(p); 
                p = process.aumentaPrezzo(p,60)

                console.log('6,2--> DUPLICARE TUTT.....  '+ p.codice);
                return p;
            }
        }
    ]
}; 

process.execute(transform1)
     .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 6: CREAZIONE CODICI 2031 KXL/A TRACK
6.1--> DUPLICARE TUTTI I CODICI DEL MODELLO, 
ESCLUSO FLAG CRIPTATI=TRUE AND CODICI CON 5°CARATTERE= S OR T, E MODIFICARE RECORD:
AGGIUNGERE A FINE CODICE LETTERA L
DRIVER: Driver Dali
FLAG DIMMERABILE= TRUE
PREZZO:  
AGGIUNGERE + 60€  A TUTTI I PREZZI        
*/
