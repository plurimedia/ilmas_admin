
/*
node --max-old-space-size=10000 scripts/inserimenti/settembre/bck.js 573436d4c0f52e030063bb3d

node --max-old-space-size=10000 scripts/inserimenti/settembre/1_2001_KS_TRACK.js 
*/

var process = require('./../massive.js');
var modello = '573436d4c0f52e030063bb3d';
var query = { modello : modello,  criptato: { $ne: true } } ;
var tag = 'ame_20180829_01';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE']}],

    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) { 
                if ( !(process.hasCharAt(p.codice, 'Q', 6))) {
                    p = process.archiviaConNote(p, 'IN39');
                    console.log('trasf 1 '+ p.codice);
                }
                return p;
            }
        }
    ]
};

query = { modello : modello} ;
var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.isGenerazione,
    filterArguments : {gen:['D1','D2','V1','G6', '3HE']},
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p.accessori=null;
                switch (p.prezzo) {
                    case 221 :
                        p.prezzo = 193;
                        break;
                    case 261 :
                        p.prezzo = 193;
                        break;
                    case 241 :
                        p.prezzo = '213';
                        break;
                    case 281 :
                        p.prezzo = 213;
                        break;
                    case 256 :
                        p.prezzo = 228;
                        break;
                    case 296 :
                        p.prezzo = 228;
                        break;
                    case 249 :
                        p.prezzo = 221;
                        break;
                    case 274 :
                        p.prezzo = 246;
                        break;
                    case 293 :
                        p.prezzo = 265;
                        break; 
                    case 289 :
                        p.prezzo = 221;
                        break;
                    case 314 :
                        p.prezzo = 246;
                        break;
                    case 333 :
                        p.prezzo = 265;
                        break; 

                }

                console.log('trasf 2'+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

PUNTO 1: CAMBIO PREZZI 2001 KS/TRACK

1.1 METTERE IN ARCHIVIO TUTTI I CODICI, ESCLUSO FLAG CRIPTATI=TRUE, 6°CARATTERE= Q

1.2 MODIFICA PREZZO DEI CODICI RIMASTI DAL PUNTO 1.1:
221-->193
261-->193
241-->213
281-->213
256-->228
296-->228
249-->221
274-->246
293-->265
289-->221
314-->246
333-->265
*/
