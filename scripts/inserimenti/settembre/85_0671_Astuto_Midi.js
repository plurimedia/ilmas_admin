
/*
      node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd24f

node --max-old-space-size=20000 scripts/inserimenti/settembre/85_0671_Astuto_Midi.js
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd24f';
var query = { modello : modello};
var tag = 'dani_20180905_16';


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) { 
                 
                switch (p.prezzo) {
                    case 158 :
                        p.prezzo = 150;
                        break; 
                    case 165 :
                        p.prezzo = 150;
                        break;
                    case 178 :
                        p.prezzo = 170;
                        break;
                    case 185 :
                        p.prezzo = 170;
                        break;
                    case 193 :
                        p.prezzo = 185;
                        break;
                    case 200 :
                        p.prezzo = 185;
                        break;
                }

                console.log('modifica prezzo prodotto  '+ p.codice);
                return p;
            }
        }
    ]
};

 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*PUNTO 85: CAMBIO PREZZI 0671 ASTUTO MIDI
85.1 MODIFICA PREZZO  :
158150
165150
178170
185170
193185
200185*/