
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd249

node --max-old-space-size=20000 scripts/inserimenti/settembre/40_0786_HAMMER_Q_MINI_120.js
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd249';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_38';
ObjectId = require('mongodb').ObjectID;


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
           // action: 'nuovo_inserimento',
            action: 'modifica_esistente',
            replace_rule: function (p) {  

                switch (p.prezzo) {
                    case 147 :
                        p.prezzo = 139;
                        break; 
                    case 154 :
                        p.prezzo = 139;
                        break; 
                    case 174 :
                           p.prezzo = 159; 
                        break; 
                    case 182 :
                        p.prezzo = 174; 
                     break;  
                    case 189 :
                     p.prezzo = 174; 
                     break;  
             
                }
                console.log('archivia:  '+ p.codice);
                return p;
            }
        }
    ]
};

 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 40: CAMBIO PREZZI 0786 HAMMER  Q MINI 120
6.1 MODIFICA PREZZO :
147139
154139
167159
174159
182174
189174
*/
