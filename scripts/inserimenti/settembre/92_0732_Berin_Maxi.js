
/*
      node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd258

node --max-old-space-size=20000 scripts/inserimenti/settembre/92_0732_Berin_Maxi.js
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd258';
var query = { modello : modello};
var tag = 'dani_20180906_04';


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                switch (p.prezzo) {
                    case 157 :
                        p.prezzo = 147;
                        break; 
                    case 178 :
                        p.prezzo = 170;
                        break;
                    case 205 :
                        p.prezzo = 170;
                        break;
                    case 185 :
                        p.prezzo = 170;
                        break;
                    case 177 :
                        p.prezzo = 167;
                        break;
                    case 198 :
                        p.prezzo = 190;
                        break;
                    case 225 :
                        p.prezzo = 190;
                        break;
                    case 205 :
                        p.prezzo = 190;
                        break;
                    case 192 :
                        p.prezzo = 182;
                        break;
                    case 213 :
                        p.prezzo = 205;
                        break;
                    case 240 :
                        p.prezzo = 205;
                        break;
                    case 220 :
                        p.prezzo = 205;
                        break;
                }

                console.log('modifica prezzo prodotto  '+ p.codice);
                return p;
            }
        }
    ]
};

 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

