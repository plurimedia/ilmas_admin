
/*
   node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 59afc1c5dd826e040027a57b

node --max-old-space-size=20000 scripts/inserimenti/settembre/32_1298_COVE_PRO_F112.js 
*/

var process = require('./../massive.js');
var modello = '59afc1c5dd826e040027a57b';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_29';
ObjectId = require('mongodb').ObjectID;


var transform1 = {
    tag : tag,
    query: query,
 
    filter: [process.filters.isGenerazione,],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],

    transform: [
        {
           // action: 'nuovo_inserimento',
            action: 'modifica_esistente',
            replace_rule: function (p) { 
                
                switch (p.prezzo) {
                    case 220 :
                        p.prezzo = 210;
                        break; 
                    case 240 :
                        p.prezzo = 220;
                        break; 
                    case 265 :
                           p.prezzo = 245; 
                        break; 
                    
                }

                console.log('32.1 MODIFICA PREZZO:  '+ p.codice);
                return p;
            }
        }
    ]
};


var transform2= {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione, process.filters.hasNotCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}, {chars:["T"], pos:6}],
    checkDuplicates:true,
    
    transform: [
        {
            action: 'nuovo_inserimento',
           // action: 'modifica_esistente',
            replace_rule: function (p) { 
                p = process.creaNuovoProdotto(p,'G6')
                p = process.dali(p)

                if (p.prezzo==220)
                        p.prezzo==270
                else if (p.prezzo==230)
                    p.prezzo==290
                else if (p.prezzo==245)
                    p.prezzo==305 
                    
                console.log('32.1 MODIFICA PREZZO:  '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 32: MODIFICA PREZZI  E CREAZIONE CODICI 1298 COVE PRO F112
32.1 MODIFICA PREZZI:
220210
240220
265245
32.2  DUPLICARE TUTTI I CODICI DEL MODELLO, ESCLUSI CRIPTATI=TRUE AND 6°CARATTERE=T,  MODIFICANDO I SEGUENTI RECORD:
9° CARATTERE= L
DRIVER:Driver Dali
FLAG DIMMERABILE: TRUE
PREZZI:
220270
220290
245305
*/
