
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd23a

node --max-old-space-size=2000 scripts/inserimenti/settembre/28_1322_VIPER_MEGA.js 
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd23a';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_28';
ObjectId = require('mongodb').ObjectID;


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE','']}],
    transform: [
        {
           // action: 'nuovo_inserimento',
            action: 'modifica_esistente',
            replace_rule: function (p) { 
                
 

                switch (p.prezzo) {
                    case 245 :
                        p.prezzo = 217;
                        break; 
                    case 260 :
                        p.prezzo = 217;
                        break; 
                    case 265 :
                           p.prezzo = 237; 
                        break; 
                    case 280 :
                        p.prezzo = 237;
                        break;  
                }

                console.log('28.1 MODIFICA PREZZO:  '+ p.codice);
                return p;
            }
        }
    ]
};
   

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 28: MODIFICA PREZZI 1322 VIPER MEGA  
28.1 MODIFICA PREZZO:
245217
260217
265237
280237
*/
