
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 58247983de6dd4040049c3b5

node --max-old-space-size=20000 scripts/inserimenti/settembre/47_2012_KS_2.js 
*/

var process = require('./../massive.js');
var modello = '58247983de6dd4040049c3b5';
var query = { modello : modello} ;
var tag = 'dani_20180903_02';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {  

                switch (p.prezzo) {
                    case 420 :
                        p.prezzo = 364;
                        break;
                    case 450 :
                        p.prezzo = 364;
                        break;
                    case 460 :
                        p.prezzo = 404;
                        break;
                    case 490 :
                        if(process.hasCharAt(p.codice, 'W', 5)) {
                            p.prezzo = 404;
                       } else {
                            if(process.hasCharAt(p.codice, 'Q', 5)) {
                                p.prezzo = 434;
                            }
                        }
                        break;
                    case 520 :
                        p.prezzo = 434;
                        break;
                    case 720 :
                        p.prezzo = 664;
                        break;
                    case 750 :
                        p.prezzo = 664;
                        break;
                    case 800 :
                        p.prezzo = 704;
                        break;
                    case 830 :
                        p.prezzo = 704;
                        break;
                    case 860 :
                        p.prezzo = 734;
                        break;
                    case 890 :
                        p.prezzo = 734;
                        break;
                    case 680 :
                        p.prezzo = 640;
                        break;
                    case 740 :
                        p.prezzo = 670;
                        break;
                }

                console.log('modifica prezzo :   '+ p.codice);
                return p;
            }
        }
    ]
}; 
 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
 PUNTO 47: CAMBIO PREZZI 2012 KS/2
 2.1 MODIFICA PREZZO  :
 420 364
 450364
 460404
 490 AND 5°CARATTERE “W”404
 490 AND 5°CARATTERE “Q”434
 520434
 720664
 750664
 800704
 830704
 860734
 890734
 680640
 740670
*/
