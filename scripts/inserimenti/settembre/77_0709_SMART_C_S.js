
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 5b5f21406cbb3f0400f431e2

 node --max-old-space-size=20000 scripts/inserimenti/settembre/77_0709_SMART_C_S.js;
 */

var process = require('./../massive.js');
var modello = '5b5f21406cbb3f0400f431e2';
var query = {modello:modello };
var tag = 'ame_20180904_77';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.modello = ObjectId('5b5f26286cbb3f0400f431ee');
                p.codice = process.replaceCharAt(p.codice, '9', 4);

                switch (p.prezzo) {
                    case 90 :
                        p.prezzo = 170;
                        break;
                    case 70 :
                        p.prezzo = 130;
                        break;
                    case 122 :
                        p.prezzo = 234;
                        break;
                }

                switch (p.w) {
                    case '12' :
                        p.w = '2x12';
                        break;
                    case '17' :
                        p.w = '2x17';
                        break;
                }

                switch (p.lm) {
                    case '1705' :
                        p.lm = '3410';
                        break;
                    case '1790' :
                        p.lm = '3580';
                        break;
                    case '1880' :
                        p.lm = '3760';
                        break;
                    case '2360' :
                        p.lm = '4720';
                        break;
                    case '2480' :
                        p.lm = '4960';
                        break;
                    case '2600' :
                        p.lm = '5200';
                        break;
                    case '1460' :
                        p.lm = '2920';
                        break;
                    case '1535' :
                        p.lm = '3070';
                        break;
                    case '1610' :
                        p.lm = '3220';
                        break;
                    case '2020' :
                        p.lm = '4040';
                        break;
                    case '2125' :
                        p.lm = '4250';
                        break;
                    case '2235' :
                        p.lm = '4470';
                        break;
                }

                console.log('cambio prezzo prodotto '+ p.codice);
                return p;
            }
        }
    ]
};
 
process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });


/*
PUNTO77: CODICI  0709 SMART C/S
77.1 DUPLICARE TUTTI I CODICI DEL MODELLO : 0708 SMART C/S, ESCLUSI CRIPTATI=, 
 MODIFICANDO I SEGUENTI RECORD:
MODELLO: 0709 SMART C/S
CODICE: 4° CARATTERE= 9
PREZZI:
90170
70130
122234
POTENZA:
12      2 X 12
17      2 X 17
FL. LUM. NOM.:
17053410
17903580
18803760
23604720
24804960
26005200
14602920
15353070
16103220
20204040
21254250
22354470
*/