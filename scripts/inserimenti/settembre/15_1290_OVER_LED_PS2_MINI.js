
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd23b

node --max-old-space-size=20000 scripts/inserimenti/settembre/15_1290_OVER_LED_PS2_MINI.js
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd22f';
var query = { modello : modello} ;
var tag = 'ame_20180829_15';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [ {gen:['D1','D2','V1','G6', '3HE','']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {  

                switch (p.prezzo) {
                    case 193 :
                        p.prezzo = 165;
                        break; 
                    case 213 :
                        p.prezzo = 185;
                        break; 
                    case 228 :
                        if( (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'B', 6) ) || 
                            (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'C', 6) ) ){
                            p.prezzo = 200;
                       }
                        break; 
                    case 208 :
                        p.prezzo = 165;
                        break; 
                    case 228 :
                        if( (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'B', 6) ) || 
                            (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'C', 6) ) ){
                            p.prezzo = 185;
                        }
                        break; 
                    case 243 :
                        p.prezzo = 200;
                        break; 
                }               

                console.log('13.1- METTERE IL ARCHIVIO CODICI DEL ....:   '+ p.codice);
                return p;
            }
        }
    ]
}; 
 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 15: MODIFICA PREZZI 1290 OVER LED PS2 MINI   (USARE REGOLA PUNTO 9)
9.1 MODIFICA PREZZO:
193165
213185
228 AND “QB” OR “QC”200
208165
228 AND “WB” OR “WC”185
243200
*/
