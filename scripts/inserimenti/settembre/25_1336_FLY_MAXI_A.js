
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd236

node --max-old-space-size=20000 scripts/inserimenti/settembre/25_1336_FLY_MAXI_A.js
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd236';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_25';
ObjectId = require('mongodb').ObjectID;

//25.1 DUPLICARE TUTTI I CODICI DEL MODELLO 1332 FLY MAXI
 //AND LUNGH. MAX 8 CARATTERI 
 //AND 6°CARATTERE = D, 
 //ESCLUDEDO FLAG CRIPTATI=TRUE , 
 
var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione, process.filters.hasLunghezzaCodiceMax,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE','']},{len:8}, {chars:["D"], pos:6}],
    checkDuplicates:true,
    transform: [
        {
           action: 'nuovo_inserimento',
           // action: 'modifica_esistente',
            replace_rule: function (p) { 
                p = process.creaNuovoProdotto(p,'G6')
                p.modello = ObjectId('5acf2547504c990400f042b8');
                p.codice = process.replaceCharAt(p.codice, '6', 4)
                p.tag = "ame_20180829_25_2"

                console.log('24.1 MODIFICA PREZZO:  '+ p.codice);
                return p;
            }
        }
    ]
};

//25.2 DUPLICARE I CODICI CREATI NEL PUNTO 25.1 MODIFICANDO I SEGUENTI RECORD:
query = {tag:"ame_20180829_25_2"}
var transform2 = {
    tag : tag,
    query: query,
    checkDuplicates:true,
    transform: [
        {
           action: 'nuovo_inserimento',
           // action: 'modifica_esistente',
            replace_rule: function (p) { 
                p.tag = ""
                p = process.creaNuovoProdotto(p,'G6')
               p = process.dali(p);

               if (p.prezzo==157)
                    p.prezzo=217
                if (p.prezzo==177)
                    p.prezzo=237
                if (p.prezzo==192)
                    p.prezzo=252    
                console.log('25.2 DUPLICARE I CODICI CREATI NEL PUNTO ...:  '+ p.codice);
                return p;
            }
        }
    ]
};

//25.3 DUPLICARE I CODICI CREATI NEL PUNTO 25.1 MODIFICANDO I SEGUENTI RECORD:
query = {tag:"ame_20180829_25_2"}
var transform3 = {
    tag : tag,
    query: query,
    transform: [
        {
           action: 'nuovo_inserimento',
           // action: 'modifica_esistente',
            replace_rule: function (p) { 
                p.tag = ""
                
                p = process.creaNuovoProdotto(p,'G6')
                p.codice = process.replaceCharAt(p.codice, 'T', 6)
                p.ma="850"
                p.w="31"
 

               if (p.prezzo==3640)
                    p.prezzo=4550
                if (p.prezzo==3735)
                    p.prezzo=4650
                if (p.prezzo==3890)
                    p.prezzo=4850
                if (p.prezzo==3085)
                    p.prezzo=3850
                if (p.prezzo==3160)
                    p.prezzo=3960
                if (p.prezzo==3390)
                    p.prezzo=4240
                if (p.prezzo==2910)
                    p.prezzo=3640
                if (p.prezzo==2750)
                    p.prezzo=3440

                console.log('25.3 DUPLICARE I CODICI CREATI NEL PUNTO  ...:  '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
25.1 DUPLICARE TUTTI I CODICI DEL MODELLO 1332 FLY MAXI
 AND LUNGH. MAX 8 CARATTERI 
 AND 6°CARATTERE = D, 
 ESCLUDEDO FLAG CRIPTATI=TRUE , 
MODIFICANDO I SEGUENTI RECORD:
 
MODELLO: 1336 FLY MAXI/A
4°CARATTERE: 6


25.2 DUPLICARE I CODICI CREATI NEL PUNTO 25.1 MODIFICANDO I SEGUENTI RECORD:
9°CARATTERE:L
DRIVER:Driver Dali
FLAG DIMMERABILE:TRUE

PREZZO: 
157217
177237
192252

25.3 DUPLICARE I CODICI CREATI NEL PUNTO 25.1 MODIFICANDO I SEGUENTI RECORD:
6°CARATTERE: T
CORRENTE:850
POTENZA :31
FL.LUM.NOM.:
36404550
37354650
38904850
30853850
31603960
33904240
29103640
27503440
*/
