
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd254

 node --max-old-space-size=20000 scripts/inserimenti/settembre/89_0662_Dino_Maxi.js;
 */

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd254';
var query = {modello:modello };
var tag = 'dani_20180906_01';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['E'], pos:6}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'S', 6);
                p.ma = '800';
                p.w = '29';

                switch (p.lm) {
                    case '4550' :
                        p.lm = '4100';
                        break;
                    case '4670' :
                        p.lm = '4210';
                        break;
                    case '4870' :
                        p.lm = '4390';
                        break;
                    case '3860' :
                        p.lm = '3480';
                        break;
                    case '3955' :
                        p.lm = '3565';
                        break;
                    case '4240' :
                        p.lm = '3820';
                        break;
                    case '3640' :
                        p.lm = '3280';
                        break;
                    case '3440' :
                        p.lm = '3100';
                        break;
                }

                console.log('modifica prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['E'], pos:6}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                switch (p.prezzo) {
                    case 169 :
                        p.prezzo = 159;
                        break;
                    case 190 :
                        p.prezzo = 182;
                        break;
                    case 197 :
                        p.prezzo = 182;
                        break;
                    case 197 :
                        p.prezzo = 182;
                        break;
                    case 189 :
                        p.prezzo = 179;
                        break;
                    case 210 :
                        p.prezzo = 202;
                        break;
                    case 237 :
                        p.prezzo = 202;
                        break;
                    case 217 :
                        p.prezzo = 202;
                        break;
                    case 204 :
                        p.prezzo = 194;
                        break;
                    case 225 :
                        p.prezzo = 217;
                        break;
                    case 252 :
                        p.prezzo = 217;
                        break;
                    case 232 :
                        p.prezzo = 217;
                        break;
                }

                console.log('modifica prezzo prodotto '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

