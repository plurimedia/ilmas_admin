
/*
   node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd241

node --max-old-space-size=20000 scripts/inserimenti/settembre/31_1222_TEN.js
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd241';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_29';
ObjectId = require('mongodb').ObjectID;


var transform1 = {
    tag : tag,
    query: query,
     
    filter: [process.filters.isGenerazione,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{len:8}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) { 
                
                if ( (process.hasCharAt(p.codice, 'I', 5)) ||
                (process.hasCharAt(p.codice, 'A', 5)) || 
                (process.hasCharAt(p.codice, 'B', 5)) ||
                (process.hasCharAt(p.codice, 'O', 5)) ||
                (process.hasCharAt(p.codice, 'S', 5)) ||
                (process.hasCharAt(p.codice, 'T', 5)) ){
                    p.prezzo=215   
                }

                if  (process.hasCharAt(p.codice, 'W', 5)) {
                    p.prezzo=225   
                }
                if  (process.hasCharAt(p.codice, 'Q', 5)) {
                    p.prezzo=250   
                }

                console.log('30.1 MODIFICA PREZZO:  '+ p.codice);
                return p;
            }
        }
    ]
};
   
var transform2 = {
    tag : tag,
    query: query,
     
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) { 
                
                if  ( 
                        ((process.hasCharAt(p.codice, 'D', 9)) || (process.hasCharAt(p.codice, 'L', 9)))  &&
                        ( 
                            (process.hasCharAt(p.codice, 'I', 5)) || (process.hasCharAt(p.codice, 'A',5 )) ||
                            (process.hasCharAt(p.codice, 'B',5 )) || (process.hasCharAt(p.codice, 'O',5 )) ||
                            (process.hasCharAt(p.codice, 'S',5 )) || (process.hasCharAt(p.codice, 'T',5 )) 
                        )
                    )  {
                        p.prezzo=247
                    }

                    if  ( 
                        ((process.hasCharAt(p.codice, 'D', 9)) || (process.hasCharAt(p.codice, 'L',9 )))  &&
                        ( 
                            (process.hasCharAt(p.codice, 'W', 5))  
                        )
                    )  {
                        p.prezzo=267
                    }

                    if  ( 
                        ((process.hasCharAt(p.codice, 'D', 9)) || (process.hasCharAt(p.codice, 'L',9 )))  &&
                        ( 
                            (process.hasCharAt(p.codice, 'Q', 5))  
                        )
                    )  {
                        p.prezzo=282
                    }

                console.log('30.1 MODIFICA PREZZO:  '+ p.codice);
                return p;
            }
        }
    ]
};
process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 31: MODIFICA PREZZI 1232 TEN
31.1 MODIFICA PREZZO:
TUTTI CODICI DEL MODELLO CON LUNGH MAX 8 CARATTERI AND 5° CARATTRE=(I OR A OR B OR O OR S OR T) = 215
TUTTI CODICI DEL MODELLO CON LUNGH MAX 8 CARATTERI AND 5° CARATTRE ” W “:  225
TUTTI CODICI DEL MODELLO CON LUNGH MAX 8 CARATTERI AND 5° CARATTRE ” Q “:  250

TUTTI CODICI DEL MODELLO CON 9°CARATTERI= “D OR L” AND 5° CARATTRE=(I OR A OR B OR O OR S OR T) = 247
TUTTI CODICI DEL MODELLO CON 9°CARATTERI= “D OR L” AND 5° CARATTRE “W”= 267
TUTTI CODICI DEL MODELLO CON 9°CARATTERI= “D OR L” AND 5° CARATTRE “Q”= 282
*/
