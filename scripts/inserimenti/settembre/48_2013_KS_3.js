
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 584542e550a2b304008c47a2

node --max-old-space-size=20000 scripts/inserimenti/settembre/48_2013_KS_3.js
*/

var process = require('./../massive.js');
var modello = '584542e550a2b304008c47a2';
var query = { modello : modello} ;
var tag = 'dani_20180903_03';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {  

                switch (p.prezzo) {
                    case 630 :
                        p.prezzo = 546;
                        break;
                    case 675 :
                        p.prezzo = 546;
                        break;
                    case 690 :
                        p.prezzo = 606;
                        break;
                    case 735 :
                        if(process.hasCharAt(p.codice, 'W', 5)) {
                            p.prezzo = 606;
                       } else {
                            if(process.hasCharAt(p.codice, 'Q', 5)) {
                                p.prezzo = 651;
                            }
                        }
                        break;
                    case 780 :
                        p.prezzo = 651;
                        break;
                    case 1020 :
                        p.prezzo = 960;
                        break;
                    case 1110 :
                        p.prezzo = 1005;
                        break;
                    case 1080 :
                        p.prezzo = 996;
                        break;
                    case 1125 :
                        p.prezzo = 996;
                        break;
                    case 1245 :
                        p.prezzo = 1056;
                        break;
                    case 1200 :
                        p.prezzo = 1056;
                        break;
                    case 1290 :
                        p.prezzo = 1101;
                        break;
                    case 1335 :
                        p.prezzo = 1101;
                        break;
                }

                console.log('modifica prezzo :   '+ p.codice);
                return p;
            }
        }
    ]
}; 
 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
 PUNTO 48: CAMBIO PREZZI 2013 KS/3
 3.1 MODIFICA PREZZO  :
 630546
 675546
 690606
 735 AND 5°CARATTERE “W”606
 735 AND 5°CARATTERE “Q”651
 780 651
 1020960
 11101005
 1080996
 1125996
 12451056
 12001056
 12901101
 13351101
*/
