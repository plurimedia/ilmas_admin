
/*
  node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd240

node --max-old-space-size=20000 scripts/inserimenti/settembre/30_1222_SIX.js 
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd240';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_29';
ObjectId = require('mongodb').ObjectID;


var transform1 = {
    tag : tag,
    query: query,
    
    filter: [process.filters.isGenerazione,],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],

    transform: [
        {
           // action: 'nuovo_inserimento',
            action: 'modifica_esistente',
            replace_rule: function (p) { 
 

                switch (p.prezzo) {
                    case 275 :
                        p.prezzo = 247;
                        break; 
                    case 284 :
                        p.prezzo = 267;
                        break; 
                    case 295 :
                           p.prezzo = 267; 
                        break; 
                    case 304 :
                        p.prezzo = 267;
                        break;  
                    case 310 :
                        p.prezzo = 282;
                        break;  
                        case 319 :
                        p.prezzo = 282;
                        break; 
                        case 290 :
                        p.prezzo = 247;
                        break; 
                        case 299 :
                        p.prezzo = 247;
                        break; 
                        case 310 :
                        p.prezzo = 267;
                        break; 

                        case 319 :
                        p.prezzo = 267;
                        break; 
                        case 325 :
                        p.prezzo = 282;
                        break; 
                        case 334 :
                        p.prezzo = 282;
                        break; 
                }

                console.log('30.1 MODIFICA PREZZO:  '+ p.codice);
                return p;
            }
        }
    ]
};
   

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 30: MODIFICA PREZZI 1222 SIX
30.1 MODIFICA PREZZO:
275247
284247
295267
304267
310282
319282
290247
299247
310267
319267
325282
334282
*/
