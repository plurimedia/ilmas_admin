
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd28e

node --max-old-space-size=20000 scripts/inserimenti/settembre/46_bis_0693.js 
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd28e';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_38';
ObjectId = require('mongodb').ObjectID;


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        { 
           // action: 'nuovo_inserimento',
            action: 'modifica_esistente',
            replace_rule: function (p) {    

                if( process.hasCharAt(p.codice, 'ZD', 5) ||
                    process.hasCharAt(p.codice, '7D', 5) ||
                    process.hasCharAt(p.codice, '8D', 5) ||
                    process.hasCharAt(p.codice, 'ZB', 5) ||
                    process.hasCharAt(p.codice, '7B', 5) ||
                    process.hasCharAt(p.codice, '8B', 5) ||
                    process.hasCharAt(p.codice, '1B', 5) ||
                    process.hasCharAt(p.codice, '2B', 5) ||
                    process.hasCharAt(p.codice, '3B', 5)
                    ) {

                    p = process.archiviaConNote(p, 'IN39')

                    console.log('archivia:  '+ p.codice);
                }
                
                return p;
            }
        }
    ]
};

 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO46_bis: METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO 0693 CHE HANNO AL 5° E 6° CARATTERE.
“ZD”
“7D”
“8D”
“ZB”
“7B”
“8B”
 “1B”
“2B”
“3B”
*/
