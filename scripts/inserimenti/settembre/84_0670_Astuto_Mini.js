
/*
      node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd24e

node --max-old-space-size=20000 scripts/inserimenti/settembre/84_0670_Astuto_Mini.js
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd24e';
var query = { modello : modello};
var tag = 'dani_20180905_15';


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) { 
                 
                switch (p.prezzo) {
                    case 149 :
                        p.prezzo = 141;
                        break; 
                    case 156 :
                        p.prezzo = 141; 
                        break;
                    case 169 :
                        p.prezzo = 161;
                        break;
                    case 176 :
                        p.prezzo = 141;
                        break;
                    case 184 :
                        p.prezzo = 176;
                        break;
                    case 191 :
                        p.prezzo = 176;
                        break;
                }

                console.log('modifica prezzo prodotto  '+ p.codice);
                return p;
            }
        }
    ]
};

 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*PUNTO 84: CAMBIO PREZZI 0670 ASTUTO MINI
84.1 MODIFICA PREZZO  :
149141
156141
169161
176161
184176
191176*/