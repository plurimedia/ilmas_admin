
/*

node --max-old-space-size=10000 scripts/inserimenti/settembre/bck.js 5821d5261164470300324b5c

node --max-old-space-size=20000 scripts/inserimenti/settembre/5_2006_KL_A_TRACK.js 
*/

var process = require('./../massive.js');
var modello = '5821d5261164470300324b5c';
var query = {  modello : modello,   criptato: { $ne: true }} ;
var tag = 'ame_20180829_05';

//5.1 --> METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO ESCLUSI FLAG CRIPTATI=TRUE and 5°carattere (W or Q) 
var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                if (process.hasCharsAt(p.codice, 'W', 5) || process.hasCharsAt(p.codice, 'Q', 5) ){
                
                   console.log('----5,1 NO archiviazione prodotto '+ p.codice);


                }
                else{
                    p = process.archiviaConNote(p, 'IN39'); 
                    console.log('5,1 archiviazione prodotto '+ p.codice);
                }
                return p;
            }
        }
    ]
};
 
/*
5.2--> MODIFICA PREZZO  dei restanti
         Prezzo:
         226-->224
*/
var query = {
    modello : modello
}
var transform2 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                if (p.prezzo==226)
                    p.prezzo==224

                //mi serve per intercettare i codici trattati in quest'operazione
                p.tag= '5821d5261164470300324b5c_2'

                console.log('5,2 modifica prezzo '+ p.codice);
                return p;
            }
        }
    ]
};

//5.3-> DUPLICARE TUTTI I CODICI DEL MODELLO IN ARCHIVIO (ESCLUSO FLAG CRIPTATI=TRUE) E MODIFICARE RECORD:

var query = { modello : modello, generazione:  'IN39', criptato: { $ne: true },   } ;
var transform3 = {
    tag : tag,
    query: query,
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p.tag= '5821d5261164470300324b5c_3'

                p = process.creaNuovoProdotto(p,'G6')

                if (process.hasCharAt(p.codice, 'R', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'I', 5);
                   
                }
                else if (process.hasCharAt(p.codice, '5', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'A', 5);
                   
                }
                else if (process.hasCharAt(p.codice, '6', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'B', 5);
                    
                }
                else if (process.hasCharAt(p.codice, 'Z', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'O', 5);
                   
                }
                else if (process.hasCharAt(p.codice, '7', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'S', 5);
                   
                }
                else if (process.hasCharAt(p.codice, '8', 5)) {
                    p.codice = process.replaceCharAt(p.codice, 'T', 5);
                  
                }

                switch (p.lm) {
                    case '2360' :
                        p.lm = '2675';
                        break;
                    case '2480' :
                        p.lm = '2745';
                        break;
                    case '2600' :
                        p.lm = '2860';
                        break;
                    case '3170' :
                        p.lm = '3640';
                        break;
                    case '3330' :
                        p.lm = '3735';
                        break;
                    case '3500' :
                        p.lm = '3890';
                        break;
                    case '3545' :
                        p.lm = '4100';
                        break;
                    case '3730' :
                        p.lm = '4210';
                        break;
                    case '3910' :
                        p.lm = '4390';
                        break; 
                    case '2020' :
                        p.lm = '2270';
                        break;
                    case '2125' :
                        p.lm = '2325';
                        break; 
                    case '2235' :
                        p.lm = '2490';
                        break; 
                    case '2710' :
                        p.lm = '3085';
                        break; 
                    case '2680' :
                        p.lm = '3160';
                        break;
                    case '3000' :
                        p.lm = '3390';
                        break; 
                    case '3035' :
                        p.lm = '3480';
                        break; 
                    case '3195' :
                        p.lm = '3565';
                        break; 
                    case '3360' :
                        p.lm = '3820';
                        break; 

                }

                switch (p.w) {
                    case '24' :
                        p.w = '25';
                        break;
                    case '28' :
                        p.w = '29';
                        break;
                } 


                switch (p.les) {
                    case 'SAM15' :
                        p.les = 'LES19';
                        break; 
                }

                p.generazione='G6'

                console.log('5,3 -DUPLICARE TUTTI I CODICI DEL MODELLO IN ARCHIVIO '+ p.codice);
                return p;
            }
        }
    ]
};

//5.4--> DUPLICARE TUTTI I CODICI AGGIORNATI NEL PUNTO 5.3 ESCLUSI QUELLI CON 6°CARATTERE=S:

var query = { modello : modello, tag: '5821d5261164470300324b5c_3' , } ;
var transform4 = {
    tag : tag,
    query: query,
    checkDuplicates:true,
    filter: [process.filters.hasNotCharAt],
    filterArguments : [{chars:["S"], pos:6}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) { 

                p = process.creaNuovoProdotto(p,'G6')
                //rimetto il tag giusto
                p.tag=tag
                process.dali(p)

                p.prezzo = p.prezzo+60

                console.log('5.4 '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform4)
   .then(function (err) { return process.execute(transform2) })
     .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
     .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

5.1 --> METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO ESCLUSI FLAG CRIPTATI=TRUE and 5°carattere (W or Q) , 

5.2--> MODIFICA PREZZO  dei restanti
         Prezzo:
         226-->224

5.3-> DUPLICARE TUTTI I CODICI DEL MODELLO IN ARCHIVIO (ESCLUSO FLAG CRIPTATI=TRUE) E MODIFICARE RECORD:
5° CARATTERE:
"R" --> "I"
"5" --> "A"
"6" --> "B"
"Z" --> "O"
"7" --> "S"
"8" --> "T"

FL. LUM. NOM:
2360-->2675
2480-->2745
2600-->2860
3170-->3640
3330-->3735
3500-->3890
3545-->4100
3730-->4210
3910-->4390
2020-->2270
2125-->2325
2235-->2490
2710-->3085
2680-->3160
3000-->3390
3035-->3480
3195-->3565
3360-->3820

POTENZA:
24-->25
28-->29

LES:
SAM15-->LES19

GENARAZIONE: 
D1-->G6

5.4--> DUPLICARE TUTTI I CODICI AGGIORNATI NEL PUNTO 5.3 ESCLUSI QUELLI CON 6°CARATTERE=S:
AGGIUNGERE A FINE CODICE LETTERA L
DRIVER: Driver Dali
FLAG DIMMERABILE= TRUE
PREZZO:  
AGGIUNGERE + 60€  A TUTTI I PREZZI        
*/
