
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd24a

node --max-old-space-size=2000 scripts/inserimenti/settembre/41_0787_HAMMER_Q_MIDI_160.js 
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd24a';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_38';
ObjectId = require('mongodb').ObjectID;


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
           // action: 'nuovo_inserimento',
            action: 'modifica_esistente',
            replace_rule: function (p) {   

                switch (p.prezzo) {
                    case 150 :
                        p.prezzo = 140;
                        break; 
                    case 171 :
                        p.prezzo = 163;
                        break; 
                    case 198 :
                           p.prezzo = 163; 
                        break; 
                    case 178 :
                        p.prezzo = 163; 
                     break;  
                    case 170 :
                         p.prezzo = 160; 
                         break;  
                         case 191 :
                         p.prezzo = 183; 
                         break; 
                         case 218 :
                         p.prezzo = 183; 
                         break; 

                         case 198 :
                         p.prezzo = 183; 
                         break; 
      
                         case 185 :
                         p.prezzo = 175; 
                         break; 
                         case 206 :
                         p.prezzo = 198; 
                         break; 
                         case 233 :
                         p.prezzo = 198; 
                         break; 
                         case 213 :
                         p.prezzo = 198; 
                         break; 
 
             
                }
                console.log('archivia:  '+ p.codice);
                return p;
            }
        }
    ]
};

 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 41: CAMBIO PREZZI 0787 HAMMER  Q MIDI 160
7.1 MODIFICA PREZZO :
150140
171163
198163
178163
170160
191183
218183
198183
185175
206198
233198
213198
*/
