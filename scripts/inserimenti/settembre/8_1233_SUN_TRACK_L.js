
/*

node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 59ae704a6ce53e0400a8f7f9

node --max-old-space-size=20000 scripts/inserimenti/settembre/8_1233_SUN_TRACK_L.js
*/

var process = require('./../massive.js');
var modello = '59ae704a6ce53e0400a8f7f9';
var query = { modello : modello} ;
var tag = 'ame_20180829_08';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE']}],
     transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                

                switch (p.prezzo) {
                    case 217 :
                        p.prezzo = 189;
                        break; 
                    case 237 :
                        p.prezzo = 209;
                        break; 
                    case 252 :
                        if( (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'B', 6) ) || 
                            (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'C', 6) ) ){
                            p.prezzo = 224;
                       }
                       if( (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'B', 6) ) || 
                            (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'C', 6) ) ){
                            p.prezzo = 209;
                        }
                        break; 
                    case 232 :
                        p.prezzo = 189;
                        break; 
                    case 267 :
                        p.prezzo = 224;
                        break; 
                } 

                console.log('8.1 MODIFICA PREZZO:   '+ p.codice);
                return p;
            }
        }
    ]
}; 
 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 8: MODIFICA PREZZI 1233 SUN/ TRACK L
8.1 MODIFICA PREZZO:
217189
237209
252 AND “QB” OR “QC” 224
232189
252 AND “WB” OR “WC” 209
267224 
*/
