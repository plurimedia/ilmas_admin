/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd25a

 node --max-old-space-size=20000 scripts/inserimenti/settembre/52_0608_Bob_Std_Led.js;
 */

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd25a';
var query = {modello:modello };
var tag = 'dani_20180904_01';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                if (p.prezzo == 160)
                    p.prezzo = 155;
                else if (p.prezzo == 180)
                    p.prezzo = 175;
                else if (p.prezzo == 195)
                    p.prezzo = 190;
                else if (process.hasCharAt(p.codice, 'E', 9)) {
                    if (process.hasCharsAt(p.codice, 'IB', 5) ||
                        process.hasCharsAt(p.codice, 'IC', 5) ||
                        process.hasCharsAt(p.codice, 'AB', 5) ||
                        process.hasCharsAt(p.codice, 'AC', 5) ||
                        process.hasCharsAt(p.codice, 'BB', 5) ||
                        process.hasCharsAt(p.codice, 'BC', 5) ||
                        process.hasCharsAt(p.codice, 'OB', 5) ||
                        process.hasCharsAt(p.codice, 'OC', 5) ||
                        process.hasCharsAt(p.codice, 'SB', 5) ||
                        process.hasCharsAt(p.codice, 'SC', 5) ||
                        process.hasCharsAt(p.codice, 'TB', 5) ||
                        process.hasCharsAt(p.codice, 'TC', 5)) {
                        p.prezzo = 175;
                    } else if (process.hasCharsAt(p.codice, 'ID', 5) ||
                        process.hasCharsAt(p.codice, 'IE', 5) ||
                        process.hasCharsAt(p.codice, 'AD', 5) ||
                        process.hasCharsAt(p.codice, 'AE', 5) ||
                        process.hasCharsAt(p.codice, 'BD', 5) ||
                        process.hasCharsAt(p.codice, 'BE', 5) ||
                        process.hasCharsAt(p.codice, 'OD', 5) ||
                        process.hasCharsAt(p.codice, 'OE', 5) ||
                        process.hasCharsAt(p.codice, 'SD', 5) ||
                        process.hasCharsAt(p.codice, 'SE', 5) ||
                        process.hasCharsAt(p.codice, 'TD', 5) ||
                        process.hasCharsAt(p.codice, 'TE', 5)) {
                        p.prezzo = 184;
                    } else  if (process.hasCharsAt(p.codice, 'WB', 5) ||
                        process.hasCharsAt(p.codice, 'WC', 5)) {
                        p.prezzo = 195;
                    } else  if (process.hasCharsAt(p.codice, 'WD', 5) ||
                        process.hasCharsAt(p.codice, 'WE', 5)) {
                        p.prezzo = 204;
                    } else if (process.hasCharsAt(p.codice, 'QB', 5) ||
                        process.hasCharsAt(p.codice, 'QC', 5)) {
                        p.prezzo = 210;
                    } else if (process.hasCharsAt(p.codice, 'QD', 5) ||
                        process.hasCharsAt(p.codice, 'WE', 5)) {
                        p.prezzo = 219;
                    }
                } else if (process.hasCharAt(p.codice, 'L', 9) || process.hasCharAt(p.codice, 'D', 9)) {
                    if (process.hasCharAt(p.codice, 'I', 5) ||
                        process.hasCharAt(p.codice, 'A', 5) ||
                        process.hasCharAt(p.codice, 'B', 5) ||
                        process.hasCharAt(p.codice, 'O', 5) ||
                        process.hasCharAt(p.codice, 'S', 5) ||
                        process.hasCharAt(p.codice, 'T', 5)) {
                        p.prezzo = 207;
                    } else if (process.hasCharAt(p.codice, 'W', 5)) {
                        p.prezzo = 227;
                    } else if (process.hasCharAt(p.codice, 'Q', 5)) {
                        p.prezzo = 242;
                    }
                }

                console.log('cambio prezzo prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:['2F','3F'],pos:5},
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.process.ifCharAtReplaceCharAt(p.codice, '2', '5', 5);
                p.process.ifCharAtReplaceCharAt(p.codice, '3', '6', 5);

                switch (p.lm) {
                    case '3300' :
                        p.lm = '4300';
                        break;
                    case '3600' :
                        p.lm = '4500';
                        break;
                }

                switch (p.prezzo) {
                    case 150 :
                        p.prezzo = 129;
                        break;
                    case 198 :
                        p.prezzo = 161;
                        break;
                }

                console.log('nella seconda funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};

/*var transform3 = {
    tag : tag,
    query: {modello:modello, insertRule:2, insertTag:tag },
    filter: [process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:['1F','2F','3F'], pos:5},{len:15}],
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);
                p.prezzo = 198;

                delete p.insertRule;

                console.log('nella terza funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};*/


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    //.then(function (err) { return process.execute(transform3) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 52: MODIFICA PREZZI 0608 BOB STD LED
7.1 MODIFICA PREZZO:
160155
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTRE=(IB OR IC OR AB OR AB OR BB OR BC OR OB OR OC OR SB OR SC OR TB OR TC) = 175
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTRE=(ID OR IE OR AD OR AE OR BD OR BE OR OD OR OE OR SD OR SE OR TD OR TE) = 184
180175
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(WB OR WC):  195
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(WD OR WE):  204
195190
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(QB OR QC):  210
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(QD OR WE):  219
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=L OR D    AND 5° CARATTRE=(I OR A OR B  OR O OR S OR T) = 207
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=L OR D    AND 5° CARATTRE=W    =227
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=L OR D    AND 5° CARATTRE=Q    =242
7.2  DUPLICARE I CODICI DEL MODELLO CON 5° AND 6° CARATTERE= 2F AND 3F , MODIFICARE I SEGUENTI CAMPI:
5°CARATTERE:	
2 5
36
FL. LUM. :
33004300
36004500
PREZZO: 
150129
198161
SOTTOCATEGORIA: ARLED COB OSRAM
MODULO LED: ARLED COB OSRAM (DA CREARE IN MENù A TENDINA IN B/E)
7.3 METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO CON 5° AND 6° CARATTERE=  1F OR 2F OR 3F
7.4  PER I CODICI DEL MODELLO, ESCLUSO CRIPTATO=TRUE,  CHE HANNO AL 5°AND6°CARATTERE (2X OR 3X OR 2Y OR 3Y OR 7W OR 8W ) AND LUNGH. MAX 8 , MODIFICARE IL SEGUENTE CAMPO:
NOTA PUBBLICHE =” VUOTO”
CORRENTE:    
mA: “VUOTO”
V = 230 */