
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd25d

 node --max-old-space-size=20000 scripts/inserimenti/settembre/55_0611_Bob_Std_Led.js;
 */

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd25d';
var query = {modello:modello };
var tag = 'dani_20180904_04';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'S', 6);
                p.w = '4x29';
                p.ma = '800'

                switch (p.lm) {
                    case '18200' :
                        p.lm = '16400';
                        break;
                    case '18680' :
                        p.lm = '16840';
                        break;
                    case '19480' :
                        p.lm = '17560';
                        break;
                    case '15440' :
                        p.lm = '13920';
                        break;
                    case '15820' :
                        p.lm = '14260';
                        break;
                    case '16960' :
                        p.lm = '15280';
                        break;
                    case '14560' :
                        p.lm = '13120';
                        break;
                    case '13760' :
                        p.lm = '12400';
                        break;
                }

                console.log('cambio prezzo prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['E'],pos:6}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};


var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:['2F','3F']},
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                if (p.prezzo == 564)
                    p.prezzo = 544;
                else if (p.prezzo == 644)
                    p.prezzo = 624;
                else if (p.prezzo == 704)
                    p.prezzo = 684;
                else if (process.hasCharAt(p.codice, 'E', 9)) {
                    if (process.hasCharsAt(p.codice, 'IB', 5) ||
                        process.hasCharsAt(p.codice, 'IC', 5) ||
                        process.hasCharsAt(p.codice, 'AB', 5) ||
                        process.hasCharsAt(p.codice, 'AC', 5) ||
                        process.hasCharsAt(p.codice, 'BB', 5) ||
                        process.hasCharsAt(p.codice, 'BC', 5) ||
                        process.hasCharsAt(p.codice, 'OB', 5) ||
                        process.hasCharsAt(p.codice, 'OC', 5) ||
                        process.hasCharsAt(p.codice, 'SB', 5) ||
                        process.hasCharsAt(p.codice, 'SC', 5) ||
                        process.hasCharsAt(p.codice, 'TB', 5) ||
                        process.hasCharsAt(p.codice, 'TC', 5)) {
                        p.prezzo = 624;
                    } else if (process.hasCharsAt(p.codice, 'ID', 5) ||
                        process.hasCharsAt(p.codice, 'IE', 5) ||
                        process.hasCharsAt(p.codice, 'AD', 5) ||
                        process.hasCharsAt(p.codice, 'AE', 5) ||
                        process.hasCharsAt(p.codice, 'BD', 5) ||
                        process.hasCharsAt(p.codice, 'BE', 5) ||
                        process.hasCharsAt(p.codice, 'OD', 5) ||
                        process.hasCharsAt(p.codice, 'OE', 5) ||
                        process.hasCharsAt(p.codice, 'SD', 5) ||
                        process.hasCharsAt(p.codice, 'SE', 5) ||
                        process.hasCharsAt(p.codice, 'TD', 5) ||
                        process.hasCharsAt(p.codice, 'TE', 5)) {
                        p.prezzo = 660;
                    } else if (process.hasCharsAt(p.codice, 'WB', 5) ||
                        process.hasCharsAt(p.codice, 'WC', 5)) {
                        p.prezzo = 704;
                    } else if (process.hasCharsAt(p.codice, 'WD', 5) ||
                        process.hasCharsAt(p.codice, 'WE', 5)) {
                        p.prezzo = 740;
                    } else if (process.hasCharsAt(p.codice, 'QB', 5) ||
                        process.hasCharsAt(p.codice, 'QC', 5)) {
                        p.prezzo = 764;
                    } else if (process.hasCharsAt(p.codice, 'QD', 5) ||
                        process.hasCharsAt(p.codice, 'WE', 5)) {
                        p.prezzo = 740;
                    }
                } else if (process.hasCharAt(p.codice, 'L', 9) || process.hasCharAt(p.codice, 'D', 9)) {
                    if (process.hasCharAt(p.codice, 'I', 5) ||
                        process.hasCharAt(p.codice, 'A', 5) ||
                        process.hasCharAt(p.codice, 'B', 5) ||
                        process.hasCharAt(p.codice, 'O', 5) ||
                        process.hasCharAt(p.codice, 'S', 5) ||
                        process.hasCharAt(p.codice, 'T', 5)) {
                        p.prezzo = 752;
                    } else if (process.hasCharAt(p.codice, 'W', 5)) {
                        p.prezzo = 832;
                    } else if (process.hasCharAt(p.codice, 'Q', 5)) {
                        p.prezzo = 892;
                    }
                }

                console.log('cambio prezzo prodotto ' + p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:['2F','3F'],pos:5},
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.process.ifCharAtReplaceCharAt(p.codice, '2', '5', 5);
                p.process.ifCharAtReplaceCharAt(p.codice, '3', '6', 5);

                switch (p.lm) {
                    case '13200' :
                        p.lm = '17200';
                        break;
                    case '14400' :
                        p.lm = '18000';
                        break;
                }

                switch (p.prezzo) {
                    case 533 :
                        p.prezzo = 449;
                        break;
                    case 725 :
                        p.prezzo = 577;
                        break;
                }

                console.log('modifica prodotto ' + p.codice);
                return p;
            }
        }
    ]
};



/*var transform3 = {
    tag : tag,
    query: {modello:modello, insertRule:2, insertTag:tag },
    filter: [process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:['1F','2F','3F'], pos:5},{len:15}],
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);
                p.prezzo = 198;

                delete p.insertRule;

                console.log('nella terza funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};*/


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*PUNTO 55: MODIFICA PREZZI  0611 BOB STD LED 
10.1 DUPLICARE TUTTI I CODICI DEL MODELLO , ESCLUSO FLAG CRIPTATO= TRUE, MODIFICANDO I SEGUENTI CAMPI:
6°CARATTERE :S
POTENZA: 4  X 29
CORRENTE: 800
FL. LUM.:
1820016400
1868016840
1948017560
1544013920
1582014260
1696015280
1456013120
1376012400
10.2 METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO CON AL 6°CARATTERE=E
10.3  MODIFICA PREZZO:
564-544
644624
704684
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTRE=(IB OR IC OR AB OR AB OR BB OR BC OR OB OR OC OR SB OR SC OR TB OR TC) = 624
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTRE=(ID OR IE OR AD OR AE OR BD OR BE OR OD OR OE OR SD OR SE OR TD OR TE) = 660
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(WB OR WC):  704
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(WD OR WE):  740
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(QB OR QC):  764
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(QD OR WE):  740
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=L OR D    AND 5° CARATTRE=(I OR A OR B  OR O OR S OR T) = 752
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=L OR D    AND 5° CARATTRE=W    =832
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=L OR D    AND 5° CARATTRE=Q    =892
10.4  DUPLICARE I CODICI DEL MODELLO CON 5° AND 6° CARATTERE= 2F AND 3F , MODIFICARE I SEGUENTI CAMPI:
5°CARATTERE:
2 5
36
FL. LUM. :
1320017200
1440018000
PREZZO: 
533449
725577
SOTTOCATEGORIA: ARLED COB OSRAM
MODULO LED: ARLED COB OSRAM 
10.5 METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO CON 5° AND 6° CARATTERE=  1F OR 2F OR 3F
10.6  PER I CODICI DEL MODELLO, ESCLUSO CRIPTATO=TRUE,  CHE HANNO AL 5°AND6°CARATTERE (2X OR 3X OR 2Y OR 3Y OR 7W OR 8W )AND LUNGH. MAX 8 , MODIFICARE IL SEGUENTE CAMPO:
NOTA PUBBLICHE =” VUOTO”
CORRENTE:    
mA: “VUOTO”
V = 230
*/