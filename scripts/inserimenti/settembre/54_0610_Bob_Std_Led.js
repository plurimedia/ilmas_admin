
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd25c

 node --max-old-space-size=20000 scripts/inserimenti/settembre/54_0610_Bob_Std_Led.js;
 */

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd25c';
var query = {modello:modello };
var tag = 'dani_20180904_03';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'S', 6);
                p.w = '3x29';
                p.ma = '800'

                switch (p.lm) {
                    case '13650' :
                        p.lm = '12300';
                        break;
                    case '14010' :
                        p.lm = '12630';
                        break;
                    case '14610' :
                        p.lm = '13170';
                        break;
                    case '11580' :
                        p.lm = '10440';
                        break;
                    case '11865' :
                        p.lm = '10695';
                        break;
                    case '12720' :
                        p.lm = '11460';
                        break;
                    case '10920' :
                        p.lm = '9840';
                        break;
                    case '10320' :
                        p.lm = '9300';
                        break;
                }

                console.log('cambio prezzo prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['E'],pos:6}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};


var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:['2F','3F']},
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                if (p.prezzo == 432)
                    p.prezzo = 417;
                else if (p.prezzo == 492)
                    p.prezzo = 477;
                else if (p.prezzo == 537)
                    p.prezzo = 522;
                else if (process.hasCharAt(p.codice, 'E', 9)) {
                    if (process.hasCharsAt(p.codice, 'IB', 5) ||
                        process.hasCharsAt(p.codice, 'IC', 5) ||
                        process.hasCharsAt(p.codice, 'AB', 5) ||
                        process.hasCharsAt(p.codice, 'AC', 5) ||
                        process.hasCharsAt(p.codice, 'BB', 5) ||
                        process.hasCharsAt(p.codice, 'BC', 5) ||
                        process.hasCharsAt(p.codice, 'OB', 5) ||
                        process.hasCharsAt(p.codice, 'OC', 5) ||
                        process.hasCharsAt(p.codice, 'SB', 5) ||
                        process.hasCharsAt(p.codice, 'SC', 5) ||
                        process.hasCharsAt(p.codice, 'TB', 5) ||
                        process.hasCharsAt(p.codice, 'TC', 5)) {
                        p.prezzo = 477;
                    } else if (process.hasCharsAt(p.codice, 'ID', 5) ||
                        process.hasCharsAt(p.codice, 'IE', 5) ||
                        process.hasCharsAt(p.codice, 'AD', 5) ||
                        process.hasCharsAt(p.codice, 'AE', 5) ||
                        process.hasCharsAt(p.codice, 'BD', 5) ||
                        process.hasCharsAt(p.codice, 'BE', 5) ||
                        process.hasCharsAt(p.codice, 'OD', 5) ||
                        process.hasCharsAt(p.codice, 'OE', 5) ||
                        process.hasCharsAt(p.codice, 'SD', 5) ||
                        process.hasCharsAt(p.codice, 'SE', 5) ||
                        process.hasCharsAt(p.codice, 'TD', 5) ||
                        process.hasCharsAt(p.codice, 'TE', 5)) {
                        p.prezzo = 504;
                    } else if (process.hasCharsAt(p.codice, 'WB', 5) ||
                        process.hasCharsAt(p.codice, 'WC', 5)) {
                        p.prezzo = 537;
                    } else if (process.hasCharsAt(p.codice, 'WD', 5) ||
                        process.hasCharsAt(p.codice, 'WE', 5)) {
                        p.prezzo = 564;
                    } else if (process.hasCharsAt(p.codice, 'QB', 5) ||
                        process.hasCharsAt(p.codice, 'QC', 5)) {
                        p.prezzo = 582;
                    } else if (process.hasCharsAt(p.codice, 'QD', 5) ||
                        process.hasCharsAt(p.codice, 'WE', 5)) {
                        p.prezzo = 609;
                    }
                } else if (process.hasCharAt(p.codice, 'L', 9) || process.hasCharAt(p.codice, 'D', 9)) {
                    if (process.hasCharAt(p.codice, 'I', 5) ||
                        process.hasCharAt(p.codice, 'A', 5) ||
                        process.hasCharAt(p.codice, 'B', 5) ||
                        process.hasCharAt(p.codice, 'O', 5) ||
                        process.hasCharAt(p.codice, 'S', 5) ||
                        process.hasCharAt(p.codice, 'T', 5)) {
                        p.prezzo = 573;
                    } else if (process.hasCharAt(p.codice, 'W', 5)) {
                        p.prezzo = 633;
                    } else if (process.hasCharAt(p.codice, 'Q', 5)) {
                        p.prezzo = 678;
                    }
                }

                console.log('cambio prezzo prodotto ' + p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:['2F','3F'],pos:5},
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.process.ifCharAtReplaceCharAt(p.codice, '2', '5', 5);
                p.process.ifCharAtReplaceCharAt(p.codice, '3', '6', 5);

                switch (p.lm) {
                    case '9900' :
                        p.lm = '12900';
                        break;
                    case '10800' :
                        p.lm = '13500';
                        break;
                }

                switch (p.prezzo) {
                    case 409 :
                        p.prezzo = 346;
                        break;
                    case 553 :
                        p.prezzo = 442;
                        break;
                }

                console.log('modifica prodotto ' + p.codice);
                return p;
            }
        }
    ]
};



/*var transform3 = {
    tag : tag,
    query: {modello:modello, insertRule:2, insertTag:tag },
    filter: [process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:['1F','2F','3F'], pos:5},{len:15}],
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);
                p.prezzo = 198;

                delete p.insertRule;

                console.log('nella terza funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};*/


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*PUNTO 54: MODIFICA PREZZI 0610 BOB STD LED 
9.1 DUPLICARE TUTTI I CODICI DEL MODELLO , ESCLUSO FLAG CRIPTATO= TRUE, MODIFICANDO I SEGUENTI CAMPI:
6°CARATTERE :S
POTENZA: 3  X 29
CORRENTE: 800
FL. LUM.:
1365012300
1401012630
1461013170
1158010440
1186510695
1272011460
109209840
103209300
9.2 METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO CON AL 6°CARATTERE=E
9.3  MODIFICA PREZZO:
432417
492477
537522
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTRE=(IB OR IC OR AB OR AB OR BB OR BC OR OB OR OC OR SB OR SC OR TB OR TC) = 477
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTRE=(ID OR IE OR AD OR AE OR BD OR BE OR OD OR OE OR SD OR SE OR TD OR TE) = 504
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(WB OR WC):  537
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(WD OR WE):  564
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(QB OR QC):  582
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(QD OR WE):  609
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=L OR D    AND 5° CARATTRE=(I OR A OR B  OR O OR S OR T) = 573
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=L OR D    AND 5° CARATTRE=W    =633
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=L OR D    AND 5° CARATTRE=Q    =678
9.4  DUPLICARE I CODICI DEL MODELLO CON 5° AND 6° CARATTERE= 2F AND 3F , MODIFICARE I SEGUENTI CAMPI:
5°CARATTERE:
2 5
36
FL. LUM. :
990012900
1080013500
PREZZO: 
409346
553442
SOTTOCATEGORIA: ARLED COB OSRAM
MODULO LED: ARLED COB OSRAM 
9.5 METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO CON 5° AND 6° CARATTERE=  1F OR 2F OR 3F
9.6  PER I CODICI DEL MODELLO, ESCLUSO CRIPTATO=TRUE,  CHE HANNO AL 5°AND6°CARATTERE (2X OR 3X OR 2Y OR 3Y OR 7W OR 8W ) AND LUNGH. MAX 8 , MODIFICARE IL SEGUENTE CAMPO:
NOTA PUBBLICHE =” VUOTO”
CORRENTE:    
mA: “VUOTO”
V = 230*/