/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 59b26ed2e889e40400ced241

 node --max-old-space-size=20000 scripts/inserimenti/settembre/95_2918_Cristal_2.js;
 */

var process = require('./../massive.js');
var modello = '59b26ed2e889e40400ced241';
var query = {modello:modello };
var tag = 'dani_20180906_07';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                switch (p.prezzo) {
                    case 181 :
                        p.prezzo = 173;
                        break;
                    case 188 :
                        p.prezzo = 173;
                        break;
                    case 201 :
                        p.prezzo = 193;
                        break;
                    case 208 :
                        p.prezzo = 193;
                        break;
                    case 216 :
                        p.prezzo = 208;
                        break;
                    case 223 :
                        p.prezzo = 208;
                        break;
                }

                console.log('cambio prezzo prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['2X','3X','2Y','3Y','7W','8W'],pos:5},{len:8}],
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                delete p.note;
                delete p.ma;
                p.v = '230';
                console.log('modifica prodotto '+ p.codice);
                return p;
            }
        }
    ]
};



process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

