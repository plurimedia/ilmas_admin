
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd252

 node --max-old-space-size=20000 scripts/inserimenti/settembre/87_0660_Dino_Mini.js;
 */

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd252';
var query = {modello:modello };
var tag = 'dani_20180905_18';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['C'], pos:6}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                switch (p.prezzo) {
                    case 168 :
                        p.prezzo = 160;
                        break;
                    case 175 :
                        p.prezzo = 160;
                        break;
                    case 188 :
                        p.prezzo = 180;
                        break;
                    case 195 :
                        p.prezzo = 180;
                        break;
                    case 203 :
                        p.prezzo = 195;
                        break;
                    case 210 :
                        p.prezzo = 195;
                        break;
                }

                console.log('modifica prezzo prodotto '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*PUNTO 87: ARCHIVIO E CAMBIO PREZZI 0660 DINO MINI
87.1 METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO CON  AL 6°CARATTERE=C, ESCLUSO FLAG CRIPATO=TRUE
87.1 MODIFICA PREZZO CODICI RIMASTI  :
168160
175160
188180
195180
203195
210195*/