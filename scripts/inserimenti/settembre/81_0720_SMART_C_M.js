
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 5b5f2a9a6cbb3f0400f4320f

 node --max-old-space-size=2000 scripts/inserimenti/settembre/81_0720_SMART_C_M.js;
 */

var process = require('./../massive.js');
var modello = '5b476228d0e2320400eb55e1';
var query = {modello:modello };
var tag = 'dani_20180906_08';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.modello = ObjectId('5b5f2a9a6cbb3f0400f4320f');
                p.codice = process.replaceCharAt(p.codice, '2', 3);
                p.codice = process.replaceCharAt(p.codice, '0', 4);

                switch (p.prezzo) {
                    case 80 :
                        p.prezzo = 220;
                        break;
                    case 100 :
                        if(process.hasCharAt(p.codice, 'E', 5)) {
                            p.prezzo = 190;
                        } else {
                            if(p.codice.length < 9) {
                                p.prezzo = 190;
                            }
                        }
                        break;
                    case 109 :
                        p.prezzo = 307;
                        break;
                    case 132 :
                        p.prezzo = 376;
                        break;
                    case 120 :
                        p.prezzo = 340;
                        break;
                    case 152 :
                        p.prezzo = 436;
                        break;
                    case 115 :
                        p.prezzo = 325;
                        break;
                    case 135 :
                        p.prezzo = 385;
                        break;
                    case 167 :
                        p.prezzo = 481;
                        break;
                }

                switch (p.w) {
                    case '12' :
                        p.w = '3x12';
                        break;
                    case '17' :
                        p.w = '3x17';
                        break;
                    case '24' :
                        p.w = '3x24';
                        break;
                }

                switch (p.lm) {
                    case '1705' :
                        p.lm = '5115';
                        break;
                    case '1790' :
                        p.lm = '5370';
                        break;
                    case '1880' :
                        p.lm = '5640';
                        break;
                    case '2360' :
                        p.lm = '7080';
                        break;
                    case '2480' :
                        p.lm = '7440';
                        break;
                    case '2600' :
                        p.lm = '7800';
                        break;
                    case '3170' :
                        p.lm = '9510';
                        break;
                    case '3330' :
                        p.lm = '9990';
                        break;
                    case '3500' :
                        p.lm = '10500';
                        break;
                    case '1460' :
                        p.lm = '4380';
                        break;
                    case '1535' :
                        p.lm = '4605';
                        break;
                    case '1610' :
                        p.lm = '4830';
                        break;
                    case '2125' :
                        p.lm = '6375';
                        break;
                    case '2235' :
                        p.lm = '6705';
                        break;
                    case '2710' :
                        p.lm = '8130';
                        break;
                    case '2860' :
                        p.lm = '8580';
                        break;
                    case '3000' :
                        p.lm = '9000';
                        break;
                    case '1530' :
                        p.lm = '4590';
                        break;
                    case '2140' :
                        p.lm = '6420';
                        break;
                    case '1450' :
                        p.lm = '4350';
                        break;
                    case '2020' :
                        if(process.hasCharAt(p.codice, 'Z', 5)) {
                            p.lm = '6060';
                        } else if(process.hasCharAt(p.codice, 'Q', 5)) {
                            p.lm = '6060';
                        }
                        break;
                }


                console.log('cambio prezzo prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });


/*
PUNTO 81: CODICI 0720 SMART C/M
81.1 DUPLICARE TUTTI I CODICI DEL MODELLO : 0718 SMART C/M, ESCLUSI CRIPTATI=,  MODIFICANDO I SEGUENTI RECORD:
MODELLO: 0720 SMART C/S
CODICE:3°AND 4° CARATTERE= 20
PREZZI:
80220
100 AND 9°CARATTER= E280
109307
132376
100 AND LUNGH=8 CARATTERI280
120340
152436
115325
135385
167481

POTENZA:
12      3 X 12
17      3 X 17
24    3 X 24
FL. LUM. NOM.:
17055115
17905370
18805640
23607080
24807440
26007800
31709510
33309990
350010500
14604380
15354605
16104830
2020 AND 5° CARATTERE “Z”6060
21256375
22356705
27108130
28608580
30009000
15304590
214046420
1450430
2020 AND 5° CARATTERE “Q”6060
!!!!NOTA NICOLE: CAMBIARE IMMAGINE MODELLE E DISEGNO TECNICO!!!!
 */