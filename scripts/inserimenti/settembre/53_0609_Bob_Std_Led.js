
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd25b

 node --max-old-space-size=20000 scripts/inserimenti/settembre/53_0609_Bob_Std_Led.js;
 */

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd25b';
var query = {modello:modello };
var tag = 'dani_20180904_02';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {

                p = process.creaNuovoProdotto(p, 'G6')

                p.codice = process.replaceCharAt(p.codice, 'S', 6);

                p.w = '2x29';
                p.ma = '800'

                switch (p.lm) {
                    case '9100' :
                        p.lm = '8200';
                        break;
                    case '9340' :
                        p.lm = '8420';
                        break;
                    case '9740' :
                        p.lm = '8780';
                        break;
                    case '7720' :
                        p.lm = '6960';
                        break;
                    case '7910' :
                        p.lm = '7130';
                        break;
                    case '8480' :
                        p.lm = '7640';
                        break;
                    case '7280' :
                        p.lm = '6560';
                        break;
                    case '6880' :
                        p.lm = '6200';
                        break;
                }

                console.log('trasform 1 '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['E'],pos:6}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('trasform 2 '+ p.codice);
                return p;
            }
        }
    ]
};


var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:['2F','3F']},
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                if (p.prezzo == 296)
                    p.prezzo = 286;
                else if (p.prezzo == 336)
                    p.prezzo = 326;
                else if (p.prezzo == 366)
                    p.prezzo = 356;
                else if (process.hasCharAt(p.codice, 'E', 9)) {
                    if (process.hasCharsAt(p.codice, 'IB', 5) ||
                        process.hasCharsAt(p.codice, 'IC', 5) ||
                        process.hasCharsAt(p.codice, 'AB', 5) ||
                        process.hasCharsAt(p.codice, 'AC', 5) ||
                        process.hasCharsAt(p.codice, 'BB', 5) ||
                        process.hasCharsAt(p.codice, 'BC', 5) ||
                        process.hasCharsAt(p.codice, 'OB', 5) ||
                        process.hasCharsAt(p.codice, 'OC', 5) ||
                        process.hasCharsAt(p.codice, 'SB', 5) ||
                        process.hasCharsAt(p.codice, 'SC', 5) ||
                        process.hasCharsAt(p.codice, 'TB', 5) ||
                        process.hasCharsAt(p.codice, 'TC', 5)) {
                        p.prezzo = 326;
                    } else if (process.hasCharsAt(p.codice, 'ID', 5) ||
                        process.hasCharsAt(p.codice, 'IE', 5) ||
                        process.hasCharsAt(p.codice, 'AD', 5) ||
                        process.hasCharsAt(p.codice, 'AE', 5) ||
                        process.hasCharsAt(p.codice, 'BD', 5) ||
                        process.hasCharsAt(p.codice, 'BE', 5) ||
                        process.hasCharsAt(p.codice, 'OD', 5) ||
                        process.hasCharsAt(p.codice, 'OE', 5) ||
                        process.hasCharsAt(p.codice, 'SD', 5) ||
                        process.hasCharsAt(p.codice, 'SE', 5) ||
                        process.hasCharsAt(p.codice, 'TD', 5) ||
                        process.hasCharsAt(p.codice, 'TE', 5)) {
                        p.prezzo = 344;
                    } else if (process.hasCharsAt(p.codice, 'WB', 5) ||
                        process.hasCharsAt(p.codice, 'WC', 5)) {
                        p.prezzo = 366;
                    } else if (process.hasCharsAt(p.codice, 'WD', 5) ||
                        process.hasCharsAt(p.codice, 'WE', 5)) {
                        p.prezzo = 384;
                    } else if (process.hasCharsAt(p.codice, 'QB', 5) ||
                        process.hasCharsAt(p.codice, 'QC', 5)) {
                        p.prezzo = 396;
                    } else if (process.hasCharsAt(p.codice, 'QD', 5) ||
                        process.hasCharsAt(p.codice, 'WE', 5)) {
                        p.prezzo = 414;
                    }
                } else if (process.hasCharAt(p.codice, 'L', 9) || process.hasCharAt(p.codice, 'D', 9)) {
                    if (process.hasCharAt(p.codice, 'I', 5) ||
                        process.hasCharAt(p.codice, 'A', 5) ||
                        process.hasCharAt(p.codice, 'B', 5) ||
                        process.hasCharAt(p.codice, 'O', 5) ||
                        process.hasCharAt(p.codice, 'S', 5) ||
                        process.hasCharAt(p.codice, 'T', 5)) {
                        p.prezzo = 390;
                    } else if (process.hasCharAt(p.codice, 'W', 5)) {
                        p.prezzo = 430;
                    } else if (process.hasCharAt(p.codice, 'Q', 5)) {
                        p.prezzo = 460;
                    }
                }

                console.log('trasform 3 ' + p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:['2F','3F'],pos:5},
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.process.ifCharAtReplaceCharAt(p.codice, '2', '5', 5);
                p.process.ifCharAtReplaceCharAt(p.codice, '3', '6', 5);

                switch (p.lm) {
                    case '6600' :
                        p.lm = '8600';
                        break;
                    case '7200' :
                        p.lm = '9000';
                        break;
                }

                switch (p.prezzo) {
                    case 280 :
                        p.prezzo = 238;
                        break;
                    case 376 :
                        p.prezzo = 334;
                        break;
                }

                console.log('trasform 4 ' + p.codice);
                return p;
            }
        }
    ]
};



/*var transform3 = {
    tag : tag,
    query: {modello:modello, insertRule:2, insertTag:tag },
    filter: [process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
    filterArguments : [{chars:['1F','2F','3F'], pos:5},{len:15}],
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.dali(p);
                p.prezzo = 198;

                delete p.insertRule;

                console.log('nella terza funzione di trasformazione ' + p.codice);
                return p;
            }
        }
    ]
};*/


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*PUNTO 53: MODIFICA PREZZI 0609 BOB STD LED 
8.1 DUPLICARE TUTTI I CODICI DEL MODELLO , ESCLUSO FLAG CRIPTATO= TRUE, MODIFICANDO I SEGUENTI CAMPI:
6°CARATTERE :S
POTENZA: 2 X 29
CORRENTE: 800
FL. LUM.:
91008200
93408420
97408780
77206960
79107130
84807640
72806560
68806200
8.2 METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO CON AL 6°CARATTERE=E
8.3  MODIFICA PREZZO:
296286
336326
366356
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTRE=(IB OR IC OR AB OR AB OR BB OR BC OR OB OR OC OR SB OR SC OR TB OR TC) = 326
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTRE=(ID OR IE OR AD OR AE OR BD OR BE OR OD OR OE OR SD OR SE OR TD OR TE) = 344
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(WB OR WC):  366
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(WD OR WE):  384
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(QB OR QC):  396
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=E    AND 5° AND 6° CARATTERE=(QD OR WE):  414
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=L OR D    AND 5° CARATTRE=(I OR A OR B  OR O OR S OR T) = 390
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=L OR D    AND 5° CARATTRE=W    =430
TUTTI CODICI DEL MODELLO CON  9° CARATTERE=L OR D    AND 5° CARATTRE=Q    =460
8.4  DUPLICARE I CODICI DEL MODELLO CON 5° AND 6° CARATTERE= 2F AND 3F , MODIFICARE I SEGUENTI CAMPI:
5°CARATTERE:
2 5
36
FL. LUM. :
66008600
72009000
PREZZO: 
280238
376334
SOTTOCATEGORIA: ARLED COB OSRAM
MODULO LED: ARLED COB OSRAM 
8.5 METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO CON 5° AND 6° CARATTERE=  1F OR 2F OR 3F
8.6  PER I CODICI DEL MODELLO, ESCLUSO CRIPTATO=TRUE,  CHE HANNO AL 5°AND6°CARATTERE (2X OR 3X OR 2Y OR 3Y OR 7W OR 8W )AND LUNGH. MAX 8 , MODIFICARE IL SEGUENTE CAMPO:
NOTA PUBBLICHE =” VUOTO”
CORRENTE:    
mA: “VUOTO”
V = 230*/