
/*

node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 5b476228d0e2320400eb55e1

 node --max-old-space-size=2000 scripts/inserimenti/settembre/79_0718_SMART_C_M.js;
 */

var process = require('./../massive.js');
var modello = '5b5f21406cbb3f0400f431e2';
var query = {modello:modello };
var tag = 'dani_20180906_07';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.modello = ObjectId('5b476228d0e2320400eb55e1');
                p.codice = process.replaceCharAt(p.codice, '1', 3);

                switch (p.prezzo) {
                    case 70 :
                        p.prezzo = 80;
                        break;
                    case 90 :
                        p.prezzo = 100;
                        break;
                    case 122 :
                        p.prezzo = 132;
                        break;
                }

                console.log('cambio prezzo prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

query = {modello:'5b476228d0e2320400eb55e1' };
var transform2 = {
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments: [{gen: ['D1', 'D2', 'V1', 'G6', '3HE', '']},{chars:'C',pos:6}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'D', 6);

                switch (p.prezzo) {
                    case 100 :
                        p.prezzo = 109;
                        break;
                }
                p.ma = '700'
                p.w = '24';

                switch (p.lm) {
                    case '2360' :
                        p.lm = '3170';
                        break;
                    case '2480' :
                        p.lm = '3330';
                        break;
                    case '2600' :
                        p.lm = '3500';
                        break;
                    case '2020' :
                        p.lm = '2710';
                        break;
                    case '2125' :
                        p.lm = '2860';
                        break;
                    case '2235' :
                        p.lm = '3000';
                        break;
                }
            }
        }
    ]
}

var transform3 = {
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt,process.filters.hasCharAt],
    filterArguments: [{gen: ['D1', 'D2', 'V1', 'G6', '3HE', '']},{chars:'7',pos:5},{chars:['B','C'],pos:6}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'W', 5);
                p.sottocategoria = 'Fashion';
                p.k = '3250';

                switch (p.prezzo) {
                    case 80 :
                        p.prezzo = 100;
                        break;
                    case 100 :
                        p.prezzo = 120;
                        break;
                    case 132 :
                        p.prezzo = 152;
                        break;
                }

                switch (p.lm) {
                    case '1535' :
                        p.lm = '1530';
                        break;
                    case '2125' :
                        p.lm = '2140';
                        break;
                }
            }
        }
    ]
}

var transform4 = {
    tag: tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt,process.filters.hasCharAt],
    filterArguments: [{gen: ['D1', 'D2', 'V1', 'G6', '3HE', '']},{chars:'7',pos:5},{chars:['B','C'],pos:6}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'Q', 5);
                p.sottocategoria = 'Art';
                p.irc = '98';

                switch (p.prezzo) {
                    case 80 :
                        p.prezzo = 115;
                        break;
                    case 100 :
                        p.prezzo = 135;
                        break;
                    case 132 :
                        p.prezzo = 167;
                        break;
                }

                switch (p.lm) {
                    case '1535' :
                        p.lm = '1450';
                        break;
                    case '2125' :
                        p.lm = '2020';
                        break;
                }
            }
        }
    ]
}

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });


/*PUNTO79: CODICI  0718 SMART C/S
79.1 DUPLICARE TUTTI I CODICI DEL MODELLO : 0708 SMART C/S, ESCLUSI CRIPTATI=,  MODIFICANDO I SEGUENTI RECORD:
MODELLO: 0718 SMART C/M
CODICE: 3° CARATTERE= 1
PREZZI:
7080
90100
122132

79.2 DUPLICARE  CODICI DEL MODELLO CON 6°CARATTERE=C, MODIFICANDO I SEGUENTI RECORD:
6°CARATTERE= D
PREZZO: 
100109
CORRENTE: 700mA 
POTENZA= 24
FL. LUM. NOM.:
23603170
24803330
26003500
20202710
21252860
22353000




79.3 DUPLICARE  CODICI DEL MODELLO CON 5° CARATTERE =7 AND 6°CARATTERE=(B OR C), MODIFICANDO I SEGUENTI RECORD:
5°CARATTERE= W
SOTTOCATEGORIA= FASHION 
TEMP. DI COLORE= 3250
PREZZO:
80100
100120
132152
FL. LUM. NOM.:
15351530
21252140
79.4DUPLICARE  CODICI DEL MODELLO CON 5° CARATTERE =7 AND 6°CARATTERE=(B OR C), MODIFICANDO I SEGUENTI RECORD:
5°CARATTERE= Q
SOTTOCATEGORIA= ART 
CRI= 98
PREZZO:
80115
100135
132167
FL. LUM. NOM.:
15351450
21252020 */