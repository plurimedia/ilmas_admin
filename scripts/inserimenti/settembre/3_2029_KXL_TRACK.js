
/*
node --max-old-space-size=10000 scripts/inserimenti/settembre/bck.js 59b9208ed5eaa0040053d67c

node --max-old-space-size=5000 scripts/inserimenti/settembre/3_2029_KXL_TRACK.js 
*/

var process = require('./../massive.js');
var modello = '59b9208ed5eaa0040053d67c';
var query = { modello : modello} ;
var tag = 'ame_20180829_03';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                switch (p.prezzo) {
                    case 256 :
                        p.prezzo = 228;
                        break; 
                    case 296 :
                        p.prezzo = 228;
                        break; 
                    case 276 :
                        p.prezzo = 248;
                        break; 
                    case 316 :
                        p.prezzo = 248;
                        break; 
                    case 291 :
                        p.prezzo = 263;
                        break; 
                    case 331 :
                        p.prezzo = 263;
                        break; 
                } 

                console.log('cambio prezzo '+ p.codice);
                return p;
            }
        }
    ]
};
 



process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 3: CAMBIO PREZZI 2029 KXL/TRACK

Prezzo:
256-->228
296-->228
276-->248
316-->248
291-->263
331-->263
*/
