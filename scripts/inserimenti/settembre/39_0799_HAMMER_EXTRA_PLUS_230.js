
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd248

node --max-old-space-size=20000 scripts/inserimenti/settembre/39_0799_HAMMER_EXTRA_PLUS_230.js
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd248';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_38';
ObjectId = require('mongodb').ObjectID;


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasCharAt,process.filters.isGenerazione],
    filterArguments : [{chars:["H"], pos:6},{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
           // action: 'nuovo_inserimento',
            action: 'modifica_esistente',
            replace_rule: function (p) {   
                
                p = process.archiviaConNote(p, 'IN39')

                console.log('archivia:  '+ p.codice);
                return p;
            }
        }
    ]
};

 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO39: MODIFCA 0799 HAMMER  EXTRA PLUS 230
4.1 METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO CON 6°CARATTERE=H (ESCLUSO FLAG CRIPTATI=TRUE)

*/
