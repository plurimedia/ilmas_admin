
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd23b

node --max-old-space-size=2000 scripts/inserimenti/settembre/13_1272_FORM.js 
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd23b';
var query = { modello : modello} ;
var tag = 'ame_20180829_13';

var transform1 = {
    tag : tag,
    query: query,
    filter: [ process.filters.isGenerazione],
    filterArguments : [ {gen:['D1','D2','V1','G6', '3HE', '']}],
   
     transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {  

 
                if (process.hasCharsAt(p.codice, '1F', 5)){
                    
                    p = process.archiviaConNote(p, "IN39")                 

                    console.log('13.1- METTERE IL ARCHIVIO CODICI DEL ....:   '+ p.codice);
                }
                return p;
            }
        }
    ]
}; 

//13.2DUPLICARE I CODICI CHE HANNO AL 5° AND 6° CARATTERE: “2F” OR “3F” MODIFICANDO LE CARATTERISTICHE DEI RECORD COME SEGUE:

var transform2 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [ {gen:['D1','D2','V1','G6', '3HE', '']}],

    filter: [process.filters.hasCharAt, process.filters.isGenerazione],
    filterArguments : [{chars:["2F"], pos:5}, {gen:['D1','D2','V1','G6', '3HE', '']}],

    checkDuplicates:true,
     transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {  

                
                    p = process.creaNuovoProdotto(p,'G6')
                    
                    if (process.hasCharAt(p.codice, '2', 5)){
                        p.codice = process.replaceCharAt(p.codice, '5', 5);
                    }
                    if (process.hasCharAt(p.codice, '3', 5)){
                        p.codice = process.replaceCharAt(p.codice, '6', 5);
                    }

                    if (p.lm=='3300'){
                        p.lm = '4300'
                    }
                    if (p.lm=='3600'){
                        p.lm = '4500'
                    }

                    if (p.prezzo==178){
                        p.prezzo = 157
                    }
                    if (p.prezzo==238){
                        p.prezzo = 185
                    }
                    if (p.prezzo==253){
                        p.prezzo = 185
                    }

                    console.log('13.2- dUPLICARE I CODICI CHE HANNO AL 5° AND 6° CARATT ....:   '+ p.codice);

                    return p;

                
            }
        }
    ]
}; 


var transform3 = {
    tag : tag,
    query: query,
    
    filter: [ process.filters.hasCharAt, process.filters.isGenerazione],
    filterArguments : [{chars:["3F"], pos:5} , {gen:['D1','D2','V1','G6', '3HE', '']}],

    checkDuplicates:true,
     transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {  

                
                    p = process.creaNuovoProdotto(p,'G6')
                    
                    if (process.hasCharAt(p.codice, '2', 5)){
                        p.codice = process.replaceCharAt(p.codice, '5', 5);
                    }
                    if (process.hasCharAt(p.codice, '3', 5)){
                        p.codice = process.replaceCharAt(p.codice, '6', 5);
                    }

                    if (p.lm=='3300'){
                        p.lm = '4300'
                    }
                    if (p.lm=='3600'){
                        p.lm = '4500'
                    }

                    if (p.prezzo==178){
                        p.prezzo = 157
                    }
                    if (p.prezzo==238){
                        p.prezzo = 185
                    }
                    if (p.prezzo==253){
                        p.prezzo = 185
                    }

                    console.log('13.2- dUPLICARE I CODICI CHE HANNO AL 5° AND 6° CARATT ....:   '+ p.codice);

                    return p;

                
            }
        }
    ]
}; 
var transform4 = {
    tag : tag,
    query: query,
    filter: [ process.filters.hasCharAt, process.filters.isGenerazione],
    filterArguments : [{chars:["2F"], pos:5} , {gen:['D1','D2','V1','G6', '3HE', '']}],
   
     transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {  
              
                p = process.archiviaConNote(p, "IN39")                 

                console.log('13.3- METTERE IL ARCHIVIO CODICI DEL ....:   '+ p.codice);
                return p;
                
            }
        }
    ]
}; 

var transform5 = {
    tag : tag,
    query: query,
    filter: [ process.filters.hasCharAt, process.filters.isGenerazione],
    filterArguments : [{chars:["3F"], pos:5} , {gen:['D1','D2','V1','G6', '3HE', '']}],
   
     transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {  
              
                p = process.archiviaConNote(p, "IN39")                 

                console.log('13.3- METTERE IL ARCHIVIO CODICI DEL ....:   '+ p.codice);
                return p;
                
            }
        }
    ]
}; 

process.execute(transform1)
     .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function (err) { return process.execute(transform5) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 13: MODIFICA CODICI 1272 FORM
13.1- METTERE IL ARCHIVIO CODICI DEL MODELLO CON 5° AND 6° CARATTERE= “1F”
13.2DUPLICARE I CODICI CHE HANNO AL 5° AND 6° CARATTERE: “2F” OR “3F” MODIFICANDO LE CARATTERISTICHE DEI RECORD COME SEGUE:
5°CARATTERE:
“2” “5”
“3””6”

FL. LUM. NOM:
33004300
36004500

PREZZO:
178 157
238185
253185

13.3 METTERE IL ARCHIVIO CODICI DEL MODELLO CON 5° AND 6° CARATTERE= “2F” OR “3F”

*/
