
/*
node --max-old-space-size=10000 scripts/inserimenti/settembre/bck.js 57eb8b5d60ce4b0300262fe6

node --max-old-space-size=10000 scripts/inserimenti/settembre/4_2025_KS_A_TRACK.js 
*/

var process = require('./../massive.js');
var modello = '57eb8b5d60ce4b0300262fe6';
var query = { modello : modello} ;
var tag = 'ame_20180829_04';

var transform1 = {
    tag : tag,
    query: query,
     
    filter: [process.filters.hasCharAt,process.filters.isGenerazione],
    filterArguments : [{chars:["Q"], pos:6},{gen:['D1','D2','V1','G6', '3HE']}],


    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39'); 
                console.log('4,1 archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};
 
var query = { modello : modello } ;
var transform2 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasNotCharAt,process.filters.isGenerazione],
    filterArguments : [{chars:["Q"], pos:6}, {gen:['D1','D2','V1','G6', '3HE']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                if (p.prezzo==206)
                    p.prezzo==204

                console.log('4,2 modifica prezzo '+ p.codice);
                return p;
            }
        }
    ]
};

var query = { modello : modello, criptato: { $ne: true }  } ;
var transform3 = {
    tag : tag,
    query: query,
    checkDuplicates:true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE']}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.creaNuovoProdotto(p,'G6')
                p = process.dali(p); 
                p.prezzo = p.prezzo+60
                console.log('4,3 - creazione dali '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 4: CREAZIONE CODICI E CAMBIO PREZZI 2025 KS/A TRACK
4.1  --> METTERE IN ARCHIVIO CODICI CON 6° CARATTERE= Q
4.2--> MODIFICA PREZZO dei restanti
         Prezzo:
         206-->204

4.3--> DUPLICARE TUTTI I CODICI DEL MODELLO RIMASTI (ESCLUSO FLAG CRIPTATI=TRUE) E MODIFICARE RECORD:
AGGIUNGERE A FINE CODICE LETTERA L
DRIVER: Driver Dali
FLAG DIMMERABILE= TRUE
PREZZO:  
AGGIUNGERE + 60€  A TUTTI I PREZZI         
*/
