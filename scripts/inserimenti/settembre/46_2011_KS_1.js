
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 583c40df8957d2040071c192

node --max-old-space-size=20000 scripts/inserimenti/settembre/46_2011_KS_1.js
*/

var process = require('./../massive.js');
var modello = '583c40df8957d2040071c192';
var query = { modello : modello} ;
var tag = 'dani_20180903_01';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {  

                switch (p.prezzo) {
                    case 210 :
                        p.prezzo = 182;
                        break;
                    case 225 :
                        p.prezzo = 182;
                        break;
                    case 230 :
                        p.prezzo = 202;
                        break;
                    case 245 :
                        if(process.hasCharAt(p.codice, W, 5)) {
                            p.prezzo = 202;
                       } else {
                            if(process.hasCharAt(p.codice, Q, 5)) {
                                p.prezzo = 217;
                            }
                        }
                        break;
                    case 260 :
                        p.prezzo = 217;
                        break;
                    case 360 :
                        p.prezzo = 332;
                        break;
                    case 375 :
                        p.prezzo = 332;
                        break;
                    case 400 :
                        p.prezzo = 352;
                        break;
                    case 415 :
                        p.prezzo = 352;
                        break;
                    case 430 :
                        p.prezzo = 367;
                        break;
                    case 445 :
                        p.prezzo = 367;
                        break;
                    case 340 :
                        p.prezzo = 320;
                        break;
                    case 370 :
                        p.prezzo = 335;
                        break;
                }

                console.log('modifica prezzo :   '+ p.codice);
                return p;
            }
        }
    ]
}; 
 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
 PUNTO46: CAMBIO PREZZI 2011 KS/1

 1.1 MODIFICA PREZZO  :
 210  182
 225182
 230202
 245 AND 5°CARATTERE “W”202
 245 AND 5°CARATTERE “Q”217
 260217
 360332
 375332
 400352
 415352
 430367
 445367
 340320
 370335
*/
