
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd264

 node --max-old-space-size=2000 scripts/inserimenti/settembre/58_1710_Bob_Ext_Led.js;
 */

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd264';
var query = {modello:modello };
var tag = 'dani_20180905_03';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'S', 6);
                p.w = '3x29';
                p.ma = '800'

                switch (p.lm) {
                    case '13650' :
                        p.lm = '12300';
                        break;
                    case '14010' :
                        p.lm = '12630';
                        break;
                    case '14610' :
                        p.lm = '13170';
                        break;
                    case '11580' :
                        p.lm = '10440';
                        break;
                    case '11865' :
                        p.lm = '10695';
                        break;
                    case '12720' :
                        p.lm = '11460';
                        break;
                    case '10920' :
                        p.lm = '9840';
                        break;
                    case '10320' :
                        p.lm = '9300';
                        break;
                }

                console.log('cambio prezzo prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['E'],pos:6}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};


var transform3 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:['2F','3F']},
    checkDuplicates:true,
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                if (p.prezzo == 442)
                    p.prezzo = 427;
                else if (p.prezzo == 502)
                    p.prezzo = 487;
                else if (p.prezzo == 547)
                    p.prezzo = 532;
                else if (process.hasCharAt(p.codice, 'E', 9)) {
                    if (process.hasCharsAt(p.codice, 'IB', 5) ||
                        process.hasCharsAt(p.codice, 'IC', 5) ||
                        process.hasCharsAt(p.codice, 'AB', 5) ||
                        process.hasCharsAt(p.codice, 'AC', 5) ||
                        process.hasCharsAt(p.codice, 'BB', 5) ||
                        process.hasCharsAt(p.codice, 'BC', 5) ||
                        process.hasCharsAt(p.codice, 'OB', 5) ||
                        process.hasCharsAt(p.codice, 'OC', 5) ||
                        process.hasCharsAt(p.codice, 'SB', 5) ||
                        process.hasCharsAt(p.codice, 'SC', 5) ||
                        process.hasCharsAt(p.codice, 'TB', 5) ||
                        process.hasCharsAt(p.codice, 'TC', 5)) {
                        p.prezzo = 487;
                    } else if (process.hasCharsAt(p.codice, 'ID', 5) ||
                        process.hasCharsAt(p.codice, 'IE', 5) ||
                        process.hasCharsAt(p.codice, 'AD', 5) ||
                        process.hasCharsAt(p.codice, 'AE', 5) ||
                        process.hasCharsAt(p.codice, 'BD', 5) ||
                        process.hasCharsAt(p.codice, 'BE', 5) ||
                        process.hasCharsAt(p.codice, 'OD', 5) ||
                        process.hasCharsAt(p.codice, 'OE', 5) ||
                        process.hasCharsAt(p.codice, 'SD', 5) ||
                        process.hasCharsAt(p.codice, 'SE', 5) ||
                        process.hasCharsAt(p.codice, 'TD', 5) ||
                        process.hasCharsAt(p.codice, 'TE', 5)) {
                        p.prezzo = 514;
                    } else if (process.hasCharsAt(p.codice, 'WB', 5) ||
                        process.hasCharsAt(p.codice, 'WC', 5)) {
                        p.prezzo = 547;
                    } else if (process.hasCharsAt(p.codice, 'WD', 5) ||
                        process.hasCharsAt(p.codice, 'WE', 5)) {
                        p.prezzo = 574;
                    } else if (process.hasCharsAt(p.codice, 'QB', 5) ||
                        process.hasCharsAt(p.codice, 'QC', 5)) {
                        p.prezzo = 592;
                    } else if (process.hasCharsAt(p.codice, 'QD', 5) ||
                        process.hasCharsAt(p.codice, 'WE', 5)) {
                        p.prezzo = 619;
                    }
                } else if (process.hasCharAt(p.codice, 'L', 9) || process.hasCharAt(p.codice, 'D', 9)) {
                    if (process.hasCharAt(p.codice, 'I', 5) ||
                        process.hasCharAt(p.codice, 'A', 5) ||
                        process.hasCharAt(p.codice, 'B', 5) ||
                        process.hasCharAt(p.codice, 'O', 5) ||
                        process.hasCharAt(p.codice, 'S', 5) ||
                        process.hasCharAt(p.codice, 'T', 5)) {
                        p.prezzo = 583;
                    } else if (process.hasCharAt(p.codice, 'W', 5)) {
                        p.prezzo = 643;
                    } else if (process.hasCharAt(p.codice, 'Q', 5)) {
                        p.prezzo = 688;
                    }
                }

                console.log('cambio prezzo prodotto ' + p.codice);
                return p;
            }
        }
    ]
};

var transform4 = {
    tag : tag,
    query: query,
    filter: process.filters.hasCharAt,
    filterArguments : {chars:['2F','3F'],pos:5},
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.process.ifCharAtReplaceCharAt(p.codice, '2', '5', 5);
                p.process.ifCharAtReplaceCharAt(p.codice, '3', '6', 5);

                switch (p.lm) {
                    case '9900' :
                        p.lm = '12900';
                        break;
                    case '10800' :
                        p.lm = '13500';
                        break;
                }

                switch (p.prezzo) {
                    case 419 :
                        p.prezzo = 356;
                        break;
                    case 563 :
                        p.prezzo = 452;
                        break;
                }

                console.log('modifica prodotto ' + p.codice);
                return p;
            }
        }
    ]
};

/*var transform3 = {
 tag : tag,
 query: {modello:modello, insertRule:2, insertTag:tag },
 filter: [process.filters.hasCharAt,process.filters.hasLunghezzaCodiceMax],
 filterArguments : [{chars:['1F','2F','3F'], pos:5},{len:15}],
 checkDuplicates:true,
 transform: [
 {
 action: 'nuovo_inserimento',
 replace_rule: function (p) {
 p = process.dali(p);
 p.prezzo = 198;

 delete p.insertRule;

 console.log('nella terza funzione di trasformazione ' + p.codice);
 return p;
 }
 }
 ]
 };*/


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function (err) { return process.execute(transform4) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

