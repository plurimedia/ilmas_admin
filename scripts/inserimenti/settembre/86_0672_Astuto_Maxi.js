
/*
      node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd250

node --max-old-space-size=20000 scripts/inserimenti/settembre/86_0672_Astuto_Maxi.js
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd250';
var query = { modello : modello};
var tag = 'dani_20180905_17';


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                switch (p.prezzo) {
                    case 146 :
                        p.prezzo = 136;
                        break; 
                    case 167 :
                        p.prezzo = 159;
                        break;
                    case 174 :
                        p.prezzo = 159;
                        break;
                    case 194 :
                        p.prezzo = 159;
                        break;
                    case 166 :
                        p.prezzo = 156;
                        break;
                    case 187 :
                        p.prezzo = 179;
                        break;
                    case 214 :
                        p.prezzo = 179;
                        break;
                    case 194 :
                        p.prezzo = 179;
                        break;
                    case 181 :
                        p.prezzo = 171;
                        break;
                    case 202 :
                        p.prezzo = 194;
                        break;
                    case 229 :
                        p.prezzo = 194;
                        break;
                    case 209 :
                        p.prezzo = 194;
                        break;
                }

                console.log('modifica prezzo prodotto  '+ p.codice);
                return p;
            }
        }
    ]
};

 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*PUNTO 86: CAMBIO PREZZI 0672 ASTUTO MAXI
86.1 MODIFICA PREZZO  :
146136
167159
174159
194159
166156
187179
214179
194179
181171
202194
229194
209194*/