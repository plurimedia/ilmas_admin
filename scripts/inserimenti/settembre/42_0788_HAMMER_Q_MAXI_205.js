
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 5784ca8ac98fef0300305938

node --max-old-space-size=20000 scripts/inserimenti/settembre/42_0788_HAMMER_Q_MAXI_205.js
*/

var process = require('./../massive.js');
var modello = '5784ca8ac98fef0300305938';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_38';
ObjectId = require('mongodb').ObjectID;


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
           // action: 'nuovo_inserimento',
            action: 'modifica_esistente',
            replace_rule: function (p) {   
                
                switch (p.prezzo) {
                    case 157 :
                        p.prezzo = 147;
                        break; 
                    case 178 :
                        p.prezzo = 170;
                        break; 
                    case 205 :
                           p.prezzo = 170; 
                        break; 
                    case 185 :
                        p.prezzo = 170; 
                     break;  
                    case 177 :
                         p.prezzo = 167; 
                         break;  
                         case 198 :
                         p.prezzo = 190; 
                         break; 
                         case 225 :
                         p.prezzo = 190; 
                         break; 
                         case 205 :
                         p.prezzo = 190; 
                         break; 
                         case 192 :
                         p.prezzo = 182; 
                         break; 
                         case 213 :
                         p.prezzo = 205; 
                         break; 
                         case 240 :
                         p.prezzo = 205; 
                         break; 
                         case 220 :
                         p.prezzo = 205; 
                         break; 
 
             
                }
                console.log('archivia:  '+ p.codice);
                return p;
            }
        }
    ]
};

 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 42: CAMBIO PREZZI 0788 HAMMER  Q MAXI 205
8.1 MODIFICA PREZZO :
157147
178170
205170
185170
177167
198190
225190
205190
192182
213205
240205
220205
*/
