
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd236

node --max-old-space-size=20000 scripts/inserimenti/settembre/21_1332_FLY_MAXI.js 
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd236';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_21';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE','']}],
     transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                switch (p.prezzo) {
                    case 217 :
                        p.prezzo = 189;
                        break; 
                    case 227 :
                        p.prezzo = 189;
                        break; 
                    case 237 :
                           p.prezzo = 209; 
                        break; 
                    case 247 :
                        p.prezzo = 209;
                        break; 
                    case 252 :
                        if( (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'D', 6) ) ){
                            p.prezzo = 224;
                        }
                        if( (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'D', 6) ) ){
                            p.prezzo = 209;
                        }
                        break; 
                    case 262 :
                        if( (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'E', 6) ) ){
                            p.prezzo = 224;
                        }
                        if( (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'E', 6) ) ){
                            p.prezzo = 209;
                        }
                        break; 
                    case 232 :
                        p.prezzo = 189;
                        break; 
                    case 242 :
                        p.prezzo = 289;
                        break; 
                   
                    case 267 :
                        p.prezzo = 224;
                        break;
                    case 277 :
                        p.prezzo = 224;
                        break;

                } 

                console.log('21.1 MODIFICA PREZZO:  '+ p.codice);
                return p;
            }
        }
    ]
}; 
  
 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 21: MODIFICA PREZZI 1332 FLY MAXI  (USARE REGOLA PUNTO 11)
21.1 MODIFICA PREZZO:
217189
227189
237209
247209
252 AND “QD”224
262 AND “QE”224
232189
242289
252 AND “WD”209
262 AND “WE”209
267224
277224

*/
