
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd230

node --max-old-space-size=20000 scripts/inserimenti/settembre/16_1291_OVER_LED_PS_2_MIDI.js 
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd230';
var query = { modello : modello} ;
var tag = 'ame_20180829_16';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [ {gen:['D1','D2','V1','G6', '3HE','']}],
     transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                switch (p.prezzo) {
                    case 198 :
                        p.prezzo = 170;
                        break; 
                    case 207 :
                        p.prezzo = 170;
                        break; 
                    case 218 :
                           p.prezzo = 190; 
                        break; 
                    case 227 :
                        p.prezzo = 190;
                        break; 
                    case 233 :
                        p.prezzo = 205;
                        break; 
                    case 242 :
                        if( (process.hasCharAt(p.codice, 'Q', 5) && process.hasCharAt(p.codice, 'D', 6) ) ){
                            p.prezzo = 205;
                        }
                        break; 
                    case 213 :
                        p.prezzo = 170;
                        break; 
                    case 222 :
                        p.prezzo = 170;
                        break; 
                    case 233 :
                        p.prezzo = 190;
                        break;
                    case 242 :
                        if( (process.hasCharAt(p.codice, 'W', 5) && process.hasCharAt(p.codice, 'D', 6) ) ){
                            p.prezzo = 190;
                        }
                        break;
                    case 248 :
                        p.prezzo = 205;
                        break;
                    case 257 :
                        p.prezzo = 205;
                        break;

                } 

                console.log('10.1 MODIFICA PREZZO:   '+ p.codice);
                return p;
            }
        }
    ]
}; 

query = { modello : modello,  criptato: { $ne: true } } ;
var transform2 = {
    tag : tag,
    query: query,
    filter: [process.filters.hasCharAt,process.filters.isGenerazione],
    filterArguments : [{chars:["D"], pos:6},{gen:['D1','D2','V1','G6', '3HE']}],
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.creaNuovoProdotto(p,'G6')
                p.codice = process.replaceCharAt(p.codice, 'Q', 6);
                p.ma=600
                p.w=21
               
                
                switch (p.lm) {
                    case '3640' :
                        p.lm = '3165';
                        break; 
                    case '3735' :
                        p.lm = '3245';
                        break; 
                    case '3890' :
                        p.lm = '3385';
                        break; 
                    case '3085' :
                        p.lm = '2680';
                        break; 
                    case '3160' :
                        p.lm = '2750';
                        break; 
                    case '3390' :
                        p.lm = '2950';
                        break; 
                    case '2910' :
                        p.lm = '2530';
                        break; 
                    case '2750' :
                        p.lm = '2390';
                        break; 
                }

                console.log('10.2  --> DUPLICARE TUTTI I C...  '+ p.codice);
                return p;
            }
        }
    ]
}; 

var transform3 = {
    tag : tag,
    query: query,
     
    filter: [process.filters.hasCharAt,process.filters.isGenerazione],
    filterArguments : [{chars:["D"], pos:6},{gen:['D1','D2','V1','G6', '3HE']}],

    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) { 
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};
 

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 16: CAMBIO CODICI, VALORI E PREZZI 1291 OVER LED PS 2 MIDI (USARE REGOLA PUNTO 10)
16.1-> MODIFICA PREZZO 
         Prezzo:
 198170
207170
218190
227190
233205
“QD”AND 242205
213170
222170
233190
242 AND “WD” 190
248205
257205

16.2  --> DUPLICARE TUTTI I CODICI DEL MODELLO ESCLUSI FLAG CRIPTATI=TRUE
CON 6°CARATTERE D E MODIFICARE COSì I RECORD:
6°CARATTERE: Q
CORRENTE: 600
POTENZA: 21
FL. LUM. NOM:
36403165
37353245
38903385
30852680
31602750
33902950
29102530
27502390

16.3 METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO CON 6° CARATTERE=D
*/
