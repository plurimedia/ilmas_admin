
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd23b

node --max-old-space-size=2000 scripts/inserimenti/settembre/14_1245_OVER_LES_PS_MAXI_OSRAM.js 
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd23b';
var query = { modello : modello,moduloled:'Arled Osram' } ;
var tag = 'ame_20180829_14';
ObjectId = require('mongodb').ObjectID;

var transform1 = {
    tag : tag,
    query: query,
   filter: [ process.filters.isGenerazione],
   filterArguments : [ {gen:['D1','D2','V1','G6', '3HE','']}],
    checkDuplicates:true,
     transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {  
                p = process.creaNuovoProdotto(p,'G6')
                p.codice = process.replaceCharAt(p.codice, '4', 3);
                p.codice = process.replaceCharAt(p.codice, '5', 4);
                p.modello = ObjectId('5b685b251f29e90400ab1c90');

                console.log('14.1 DUPLICARE TUTTI I CODICI DEL MODELLO 12 ....:   '+ p.codice);
                return p;
            }
        }
    ]
}; 

 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 14:CREAZIONE  CODICI MODELLO 1245  OVER LES PS MAXI OSRAM
14.1 DUPLICARE TUTTI I CODICI DEL MODELLO 1272 CON MODULO LED=ARLED OSRAM   MODIFICANDO I RECORD:
3 AND 4° CARATTERE: 45
MODELLO: 1245 OVER LED PS MAXI OSRAM
*/
