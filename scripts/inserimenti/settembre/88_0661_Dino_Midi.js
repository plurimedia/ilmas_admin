
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd253

 node --max-old-space-size=20000 scripts/inserimenti/settembre/88_0661_Dino_Midi.js;
 */

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd253';
var query = {modello:modello };
var tag = 'dani_20180905_19';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['D'], pos:6}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.codice = process.replaceCharAt(p.codice, 'Q', 6);
                p.ma = '600';
                p.w = '21';

                switch (p.lm) {
                    case '3640' :
                        p.lm = '3165';
                        break;
                    case '3735' :
                        p.lm = '3245';
                        break;
                    case '3890' :
                        p.lm = '3385';
                        break;
                    case '3085' :
                        p.lm = '2680';
                        break;
                    case '3160' :
                        p.lm = '2750';
                        break;
                    case '3390' :
                        p.lm = '2950';
                        break;
                    case '2910' :
                        p.lm = '2530';
                        break;
                    case '2750' :
                        p.lm = '2390';
                        break;
                }

                console.log('modifica prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['D'], pos:6}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

var transform3 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                switch (p.prezzo) {
                    case 184 :
                        p.prezzo = 176;
                        break;
                    case 191 :
                        p.prezzo = 176;
                        break;
                    case 204 :
                        p.prezzo = 196;
                        break;
                    case 211 :
                        p.prezzo = 196;
                        break;
                    case 219 :
                        p.prezzo = 211;
                        break;
                    case 226 :
                        p.prezzo = 211;
                        break;
                }

                console.log('modifica prezzo prodotto '+ p.codice);
                return p;
            }
        }
    ]
};


process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function (err) { return process.execute(transform3) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });


    /*PUNTO 88: ARCHIVIO E CAMBIO PREZZI 0661 DINO MIDI
88.1 DUPLICARE TUTTI I CODICI DEL MODELLO CON 6° CARATTERE=D, MODIFICANDO I SEGUENTI RECORD:
CODICE: 6°CARATTERE=Q
CORRENTE: 600 mA
POTENZA: 21
FL. LUM. NOM.:
36403165
37353245
38903385
30852680
31602750
33902950
29102530
27502390

88.2METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO CON  AL 6°CARATTERE=D, ESCLUSO FLAG CRIPATO=TRUE
87.3 MODIFICA PREZZO CODICI RIMASTI  :
184176
191176
204196
211196
219211
226211*/