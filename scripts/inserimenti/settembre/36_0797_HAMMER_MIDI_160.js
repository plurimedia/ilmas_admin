
/*
      node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd244

node --max-old-space-size=20000 scripts/inserimenti/settembre/36_0797_HAMMER_MIDI_160.js
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd244';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_36';
ObjectId = require('mongodb').ObjectID;


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
           // action: 'nuovo_inserimento',
            action: 'modifica_esistente',
            replace_rule: function (p) { 
                 
                switch (p.prezzo) {
                    case 128 :
                        p.prezzo = 118;
                        break; 
                    case 149 :
                        p.prezzo = 141;
                        break; 
                    case 176 :
                           p.prezzo = 141; 
                        break; 
                    
                    case 156 :
                        p.prezzo = 141; 
                     break; 

                    case 176 :
                     p.prezzo = 141; 

                    case 148 :
                     p.prezzo = 138; 
                     break; 
           
                     case 169 :
                     p.prezzo = 161; 
                     break; 

                     case 196 :
                     p.prezzo = 161; 
                     break; 
                     case 176 :
                     p.prezzo = 138; 
                     break; 

                     case 163 :
                     p.prezzo = 153; 
                     break; 

                     case 184 :
                     p.prezzo = 176; 
                     break; 

                     case 211 :
                     p.prezzo = 176; 
                     break; 

                     case 191 :
                     p.prezzo = 176; 
                     break; 

             
                }

                console.log('32.1 MODIFICA PREZZO:  '+ p.codice);
                return p;
            }
        }
    ]
};

 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 36: CAMBIO PREZZI 0797 HAMMER MIDI 160
2.1 MODIFICA PREZZO:
 128118
149141
176141
156141
176141
148138
169161
196161
176161
163153
184176
211176
191176
*/
