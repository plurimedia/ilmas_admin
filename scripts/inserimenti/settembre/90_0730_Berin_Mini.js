
/*
      node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd256

node --max-old-space-size=20000 scripts/inserimenti/settembre/90_0730_Berin_Mini.js
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd256';
var query = { modello : modello};
var tag = 'dani_20180906_02';


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                switch (p.prezzo) {
                    case 165 :
                        p.prezzo = 157;
                        break; 
                    case 172 :
                        p.prezzo = 157;
                        break;
                    case 185 :
                        p.prezzo = 177;
                        break;
                    case 192 :
                        p.prezzo = 177;
                        break;
                    case 200 :
                        p.prezzo = 192;
                        break;
                    case 207 :
                        p.prezzo = 192;
                        break;
                }

                console.log('modifica prezzo prodotto  '+ p.codice);
                return p;
            }
        }
    ]
};

 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

