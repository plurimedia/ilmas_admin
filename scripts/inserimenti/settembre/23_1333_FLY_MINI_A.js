
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd235

node --max-old-space-size=20000 scripts/inserimenti/settembre/23_1333_FLY_MINI_A.js 
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd235';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_23';
ObjectId = require('mongodb').ObjectID;


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasNotCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE','']}, {chars:["D"], pos:9}],
    checkDuplicates:true,
     transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p = process.creaNuovoProdotto(p,'G6')
                p.modello = ObjectId('5b890b1786e4730400052834');
                p.codice = process.replaceCharAt(p.codice, '3', 4)

                if (p.prezzo==165)
                    p.prezzo=193
                else if (p.prezzo==185)
                    p.prezzo=213
                else if (p.prezzo==200)
                    p.prezzo=228

                console.log('23.1 MODIFICA PREZZO:  '+ p.codice);
                return p;
            }
        }
    ]
}; 
  
 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 23: CREAZIONE CODICI 1333 FLY MINI/A :
23.1 DUPLICARE TUTTI I CODICI DEL MODELLO 1331 FLY MINI, ESCLUDEDO FLAG CRIPTATI=TRUE AND 9°CARARTTERE=D, MODIFICANDO I SEGUENTI RECORD:
MODELLO: 1333 FLY MINI/A
4°CARATTERE: 3
PREZZO:
165193
185213
200228
*/
