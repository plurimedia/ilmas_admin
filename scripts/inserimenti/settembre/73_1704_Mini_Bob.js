/*

node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd353

 node --max-old-space-size=20000 scripts/inserimenti/settembre/73_1704_Mini_Bob.js;
 */

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd353';
var query = {modello:modello };
var tag = 'dani_20180906_03';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                switch (p.prezzo) {
                    case 80 :
                        p.prezzo = 60;
                        break;
                    case 100 :
                        if(process.hasCharAt(p.codice, 'E', 5)) {
                            p.prezzo = 80;
                        } else {
                            if(p.codice.length < 9) {
                                p.prezzo = 80;
                            }
                        }
                        break;
                    case 115 :
                        p.prezzo = 95;
                        break;
                    case 140 :
                        p.prezzo = 112;
                        break;
                    case 147 :
                        p.prezzo = 112;
                        break;
                    case 120 :
                        p.prezzo = 100;
                        break;
                    case 160 :
                        p.prezzo = 132;
                        break;
                    case 167 :
                        p.prezzo = 132;
                        break;
                    case 135 :
                        p.prezzo = 115;
                        break;
                    case 175 :
                        p.prezzo = 147;
                        break;
                    case 182 :
                        p.prezzo = 147;
                        break;
                }

                console.log('modifica prezzo '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

