/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd354

 node --max-old-space-size=20000 scripts/inserimenti/settembre/74_1706_Mini_Bob.js;
 */

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd354';
var query = {modello:modello };
var tag = 'dani_20180906_04';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                switch (p.prezzo) {
                    case 117 :
                        p.prezzo = 110;
                        break;
                    case 157 :
                        if(process.hasCharAt(p.codice, 'E', 5)) {
                            p.prezzo = 150;
                        } else {
                            if(p.codice.length < 9) {
                                p.prezzo = 150;
                            }
                        }
                        break;
                    case 187 :
                        p.prezzo = 180;
                        break;
                    case 237 :
                        p.prezzo = 214;
                        break;
                    case 251 :
                        p.prezzo = 214;
                        break;
                    case 197 :
                        p.prezzo = 190;
                        break;
                    case 277 :
                        p.prezzo = 254;
                        break;
                    case 291 :
                        p.prezzo = 254;
                        break;
                    case 227 :
                        p.prezzo = 220;
                        break;
                    case 307 :
                        p.prezzo = 284;
                        break;
                    case 321 :
                        p.prezzo = 284;
                        break;
                }

                console.log('modifica prezzo '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

