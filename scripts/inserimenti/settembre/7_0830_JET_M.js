
/*

node --max-old-space-size=10000 scripts/inserimenti/settembre/bck.js 59ae6797144c280400213f92


node --max-old-space-size=20000 scripts/inserimenti/settembre/7_0830_JET_M.js 
*/

var process = require('./../massive.js');
var modello = '59ae6797144c280400213f92';
var query = { modello : modello} ;
var tag = 'ame_20180829_07';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {

                switch (p.prezzo) {
                    case 197 :
                        p.prezzo = 189;
                        break; 
                    case 204 :
                        p.prezzo = 189;
                        break; 
                    case 217 :
                        p.prezzo = 209;
                        break; 
                    case 224 :
                        p.prezzo = 209;
                        break; 
                    case 232 :
                        p.prezzo = 224;
                        break; 
                    case 239 :
                        p.prezzo = 224;
                        break; 
                } 

                console.log('7.1 MODIFICA PREZZO:   '+ p.codice);
                return p;
            }
        }
    ]
}; 
 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 7: MODIFICA PREZZI 0830 JET  M
7.1 MODIFICA PREZZO:
197189
204189
217209
224209
232224
239224  
*/
