
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 583bfb098957d2040071c175

node --max-old-space-size=2000 scripts/inserimenti/settembre/49_2026_KL_1.js 
*/

var process = require('./../massive.js');
var modello = '583bfb098957d2040071c175';
var query = { modello : modello,  criptato: { $ne: true } } ;
var tag = 'dani_20180903_04';

var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']},{chars:['Q','W'], pos:5}],

    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) { 
                p = process.archiviaConNote(p, 'IN39');
                console.log('archiviazione prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

var transform2 = {
    tag : tag,
    query: query,
    filter: process.filters.isGenerazione,
    filterArguments : {gen:['IN39']},
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                p = process.creaNuovoProdotto(p, 'G6');

                p.codice = process.ifCharAtReplaceCharAt(p.codice, 'R', 'I', 5);
                p.codice = process.ifCharAtReplaceCharAt(p.codice, '5', 'A', 5);
                p.codice = process.ifCharAtReplaceCharAt(p.codice, '6', 'B', 5);
                p.codice = process.ifCharAtReplaceCharAt(p.codice, 'Z', 'O', 5);
                p.codice = process.ifCharAtReplaceCharAt(p.codice, '7', 'S', 5);
                p.codice = process.ifCharAtReplaceCharAt(p.codice, '8', 'T', 5);

                switch (p.lm) {
                    case '2360' :
                        p.lm = '2675';
                        break;
                    case '2480' :
                        p.lm = '2745';
                        break;
                    case '2600' :
                        p.lm = '2860';
                        break;
                    case '3170' :
                        p.lm = '3640';
                        break;
                    case '3330' :
                        p.lm = '3735';
                        break;
                    case '3500' :
                        p.lm = '3890';
                        break;
                    case '2020' :
                        p.lm = '2270';
                        break;
                    case '2125' :
                        p.lm = '2325';
                        break;
                    case '2235' :
                        p.lm = '2490';
                        break;
                    case '2710' :
                        p.lm = '3085';
                        break;
                    case '2680' :
                        p.lm = '3160';
                        break;
                    case '3000' :
                        p.lm = '3390';
                        break;
                }

                p.w = 25;
                p.les = 'LES 19';

                switch (p.prezzo) {
                    case 240 :
                        p.prezzo = 212;
                        break;
                    case 255 :
                        p.prezzo = 212;
                        break;
                    case 260 :
                        p.prezzo = 232;
                        break;
                    case 275 :
                        if(process.hasCharAt(p.codice, 'W', 5)) {
                            p.prezzo = 232;
                        } else {
                            if(process.hasCharAt(p.codice, 'Q', 5)) {
                                p.prezzo = 247;
                            }
                        }
                        break;
                    case 290 :
                        p.prezzo = 247;
                        break;
                    case 400 :
                        p.prezzo = 380;
                        break;
                    case 430 :
                        p.prezzo = 405;
                        break;
                    case 460 :
                        p.prezzo = 412;
                        break; 
                    case 475 :
                        p.prezzo = 412;
                        break;
                    case 490 :
                        p.prezzo = 437;
                        break;
                    case 505 :
                        p.prezzo = 437;
                        break; 
                }

                console.log('modifica prezzo '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function (err) { return process.execute(transform2) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*

 PUNTO 49: CAMBIO PREZZI E CODICI  2026 KL/1
 4.1  --> METTERE IN ARCHIVIO TUTTI I CODICI DEL MODELLO ESCLUSI FLAG CRIPTATI=TRUE
 4.2--> DUPLICARE TUTTI I CODICI DEL MODELLO IN ARCHIVIO (ESCLUSO FLAG CRIPTATI=TRUE) E MODIFICARE RECORD:
 5° CARATTERE:
 "R" --> "I"
 "5" --> "A"
 "6" --> "B"
 "Z" --> "O"
 "7" --> "S"
 "8" --> "T"

 FL. LUM. NOM:
 2360-->2675
 2480-->2745
 2600-->2860
 3170-->3640
 3330-->3735
 3500-->3890
 2020-->2270
 2125-->2325
 2235-->2490
 2710-->3085
 2680-->3160
 3000-->3390

 POTENZA:
 24-->25

 LES:
 SAM15-->LES19

 GENARAZIONE:
 D1-->G6

 Prezzo:
 240212
 255212
 260232
 275232
 275247
 290247
 400380
 430405
 460412
 475412
 490437
 505437
*/
