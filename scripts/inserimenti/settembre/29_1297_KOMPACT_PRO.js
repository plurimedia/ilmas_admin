
/*
 node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd23f

node --max-old-space-size=20000 scripts/inserimenti/settembre/29_1297_KOMPACT_PRO.js 
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd23f';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_29';
ObjectId = require('mongodb').ObjectID;

//29.1 MODIFICA PREZZI:
var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE','']}],
    limit:50,
    transform: [
        { 
            action: 'modifica_esistente',
            replace_rule: function (p) { 
 
                switch (p.prezzo) {
                    case 189 :
                        p.prezzo = 180;
                      //  console.log('28.1 MODIFICA PREZZO:  '+ p.codice);
                        break; 
                    case 185 :
                        p.prezzo = 180;
                       // console.log('28.1 MODIFICA PREZZO:  '+ p.codice);
                        break; 
                    case 194 :
                           p.prezzo = 180; 
                     //      console.log('28.1 MODIFICA PREZZO:  '+ p.codice);
                        break; 
                    case 204 :
                        p.prezzo = 180;
                 //       console.log('28.1 MODIFICA PREZZO:  '+ p.codice);
                        break;  
                    case 209 :
                        p.prezzo = 200;
                    //    console.log('28.1 MODIFICA PREZZO:  '+ p.codice);
                        break;  
                    case 205 :
                        p.prezzo = 200;
                    //    console.log('28.1 MODIFICA PREZZO:  '+ p.codice);
                        break; 
                    case 214 :
                        p.prezzo = 200;
                     //   console.log('28.1 MODIFICA PREZZO:  '+ p.codice);
                        break; 
                        
                    case 224 :
                            if (process.hasCharAt(p.codice, W, 5)){
                                p.prezzo = 200;
                       //         console.log('28.1 MODIFICA PREZZO:  '+ p.codice);
                            }
                        break; 
                    case 224 :
                        if (process.hasCharAt(p.codice, Q, 5)){
                            p.prezzo = 215;
                         //   console.log('28.1 MODIFICA PREZZO:  '+ p.codice);
                        }
                        break; 
                     case 220 :
                        p.prezzo = 215;
                       // console.log('28.1 MODIFICA PREZZO:  '+ p.codice);
                        break; 
                    case 229 :
                        p.prezzo = 215;
                       // console.log('28.1 MODIFICA PREZZO:  '+ p.codice);
                        break; 
                    case 239 :
                        p.prezzo = 215;
                       // console.log('28.1 MODIFICA PREZZO:  '+ p.codice);
                        break; 

                     

                        //console.log('28.1 MODIFICA PREZZO:  '+ p.codice);
                }

                
                return p;
            }
        }
    ]
};
   
var transform2 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione, process.filters.hasNotCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE','']}, {chars:["E"], pos:6}], 
    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) { 
                p = process.creaNuovoProdotto(p,'G6')
                p = process.dali(p);

                if (p.prezzo==180)
                    p.prezzo=240
                
                if (p.prezzo==200)
                    p.prezzo=260

                if (p.prezzo==215)
                    p.prezzo=275

         

                console.log('28.2  DUPLICARE TUTTI I COD....  '+ p.codice);
                return p;
            }
        }
    ]
};



var transform3 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione, process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}, {chars:["E"], pos:6}], 

    checkDuplicates:true,
    transform: [
        {
            action: 'nuovo_inserimento',
          
            replace_rule: function (p) { 
                p = process.creaNuovoProdotto(p,'G6')
                p.codice = exports.replaceCharAt(p.codice, 'T', 6);
                p.w=31
                p.ma=850 

                console.log(' DUPLICARE I CODICI DEL MODELLO CON 6°CARATTERE=E, ESCLUSI CRIPTATI....  '+ p.codice);
                return p;
            }
        }
    ]
};



var transform4 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione, process.filters.hasCharAt],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}, {chars:["E"], pos:6}],

     
    transform: [
        {
            //action: 'nuovo_inserimento',
             action: 'modifica_esistente',
            replace_rule: function (p) { 
                
                p = process.archiviaConNote(p, 'IN39')
                console.log('28.1 28.4 METTERE IN ARCHIVIO CODICI CO....  '+ p.codice);
                return p;
            }
        }
    ]
};
process.execute(transform1)
 //.then(function (err) { return process.execute(transform2) })
 //.then(function (err) { return process.execute(transform3) })
 //.then(function (err) { return process.execute(transform4) })
    .then(function(err) { console.log("finito!!"); })
    .fail(function(err) { console.log("fallito!!", err); });

/*
PUNTO 29: MODIFICA PREZZI  E CREAZIONE CODICI 1297 KOMPACT PRO
29.1 MODIFICA PREZZI:
189180
185180
194180
204180
209200
205200
214200
224 AND 5° CARATTERE W200
224 AND 5° CARATTERE Q215
220215
229215
239215

28.2  DUPLICARE TUTTI I CODICI DEL MODELLO, 
ESCLUSI CRIPTATI=TRUE 
AND 6°CARATTERE=E,

  MODIFICANDO I SEGUENTI RECORD:
9° CARATTERE= L
DRIVER:Driver Dali
FLAG DIMMERABILE: TRUE
PREZZI:
180240
200260
215275

28.3 DUPLICARE I CODICI DEL MODELLO CON 6°CARATTERE=E, ESCLUSI CRIPTATI=TRUE, MODIFICARE I SEGUENTI RECORD:
6° CARATTERE= T
POTENZA: 31
CORRENTE: 850
28.4 METTERE IN ARCHIVIO CODICI CON 6°CARATTERE=E
*/
