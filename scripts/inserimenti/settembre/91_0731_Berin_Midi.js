
/*
      node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd257

node --max-old-space-size=20000 scripts/inserimenti/settembre/91_0731_Berin_Midi.js
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd257';
var query = { modello : modello};
var tag = 'dani_20180906_03';


var transform1 = {
    tag : tag,
    query: query,
    filter: [process.filters.isGenerazione,],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                switch (p.prezzo) {
                    case 175 :
                        p.prezzo = 167;
                        break; 
                    case 182 :
                        p.prezzo = 167;
                        break;
                    case 195 :
                        p.prezzo = 187;
                        break;
                    case 202 :
                        p.prezzo = 187;
                        break;
                    case 210 :
                        p.prezzo = 202;
                        break;
                    case 217 :
                        p.prezzo = 202;
                        break;
                }

                console.log('modifica prezzo prodotto  '+ p.codice);
                return p;
            }
        }
    ]
};

 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

