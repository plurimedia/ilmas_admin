
/*
    node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd242

node --max-old-space-size=02000 scripts/inserimenti/settembre/34_1256_SEVEN.js
*/

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd242';
var query = { modello : modello,criptato: { $ne: true }} ;
var tag = 'ame_20180829_33';
ObjectId = require('mongodb').ObjectID;


var transform1 = {
    tag : tag,
    query: query,
    
    filter: [process.filters.isGenerazione,],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],

    transform: [
        {
           // action: 'nuovo_inserimento',
            action: 'modifica_esistente',
            replace_rule: function (p) { 
                
                if ((process.hasCharAt(p.codice, 'Z', 5) && process.hasCharAt(p.codice, 'Z', 6))  ||
                (process.hasCharAt(p.codice, '7', 5) && process.hasCharAt(p.codice, 'D', 6)) ||
                (process.hasCharAt(p.codice, '8', 5) && process.hasCharAt(p.codice, 'D', 6)) ||
                (process.hasCharAt(p.codice, 'Z', 5) && process.hasCharAt(p.codice, 'B', 6)) ||
                (process.hasCharAt(p.codice, '7', 5) && process.hasCharAt(p.codice, 'B', 6)) ||
                (process.hasCharAt(p.codice, '8', 5) && process.hasCharAt(p.codice, 'B', 6)) ||
                (process.hasCharAt(p.codice, '1', 5) && process.hasCharAt(p.codice, 'B', 6)) ||
                (process.hasCharAt(p.codice, '2', 5) && process.hasCharAt(p.codice, 'B', 6)) ||
                (process.hasCharAt(p.codice, '3', 5) && process.hasCharAt(p.codice, 'B', 6)) ||
                (process.hasCharAt(p.codice, 'U', 5) && process.hasCharAt(p.codice, 'B', 6)) )
                {
                    p = process.archiviaConNote(p, 'IN39')
                    console.log('34.1 MODIFICA PREZZO:  '+ p.codice);
                }

                
                return p;
            }
        }
    ]
};

 

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

/*
PUNTO 34: ARCHIVIO PRODOTTI 1256 SEVEN
METTERE IN ARCHIVIO PRODOTTI DEL MODELLO CON 5° AND 6° CARATTERE:
“ZD”
“7D”
“8D”
“ZB”
“7B”
“8B”
“1B”
“2B”
“3B”
“UB”
*/
