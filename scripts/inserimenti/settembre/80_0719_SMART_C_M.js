
/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 5b5f2a616cbb3f0400f4320b

 node --max-old-space-size=20000 scripts/inserimenti/settembre/80_0719_SMART_C_M.js;
 */

var process = require('./../massive.js');
var modello = '5b5f2a616cbb3f0400f4320b';
var query = {modello:modello };
var tag = 'dani_20180906_08';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'nuovo_inserimento',
            replace_rule: function (p) {
                p.modello = ObjectId('5b5f2a616cbb3f0400f4320b');
                p.codice = process.replaceCharAt(p.codice, '9', 4);

                switch (p.prezzo) {
                    case 80 :
                        p.prezzo = 150;
                        break;
                    case 100 :
                        if(process.hasCharAt(p.codice, 'E', 5)) {
                            p.prezzo = 190;
                        } else {
                            if(p.codice.length < 9) {
                                p.prezzo = 190;
                            }
                        }
                        break;
                    case 109 :
                        p.prezzo = 208;
                        break;
                    case 132 :
                        p.prezzo = 202;
                        break;
                    case 120 :
                        p.prezzo = 230;
                        break;
                    case 152 :
                        p.prezzo = 294;
                        break;
                    case 115 :
                        p.prezzo = 220;
                        break;
                    case 135 :
                        p.prezzo = 260;
                        break;
                    case 167 :
                        p.prezzo = 324;
                        break;
                }

                switch (p.w) {
                    case '12' :
                        p.w = '2x12';
                        break;
                    case '17' :
                        p.w = '2x17';
                        break;
                    case '24' :
                        p.w = '2x24';
                        break;
                }

                switch (p.lm) {
                    case '1705' :
                        p.lm = '3410';
                        break;
                    case '1790' :
                        p.lm = '3580';
                        break;
                    case '1880' :
                        p.lm = '3760';
                        break;
                    case '2360' :
                        p.lm = '4720';
                        break;
                    case '2480' :
                        p.lm = '4960';
                        break;
                    case '2600' :
                        p.lm = '5200';
                        break;
                    case '3170' :
                        p.lm = '6340';
                        break;
                    case '3330' :
                        p.lm = '6660';
                        break;
                    case '3500' :
                        p.lm = '7000';
                        break;
                    case '1460' :
                        p.lm = '2920';
                        break;
                    case '1535' :
                        p.lm = '3070';
                        break;
                    case '1610' :
                        p.lm = '3220';
                        break;
                    case '2125' :
                        p.lm = '4250';
                        break;
                    case '2235' :
                        p.lm = '4470';
                        break;
                    case '2710' :
                        p.lm = '5420';
                        break;
                    case '2860' :
                        p.lm = '5720';
                        break;
                    case '3000' :
                        p.lm = '6000';
                        break;
                    case '1530' :
                        p.lm = '3060';
                        break;
                    case '2140' :
                        p.lm = '4280';
                        break;
                    case '1450' :
                        p.lm = '2900';
                        break;
                    case '2020' :
                        if(process.hasCharAt(p.codice, 'Z', 5)) {
                            p.lm = '4040';
                        } else if(process.hasCharAt(p.codice, 'Q', 5)) {
                            p.lm = '4040';
                        }
                        break;
                }


                console.log('cambio prezzo prodotto '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });




    /*PUNTO 80: CODICI  0719 SMART C/M
80.1 DUPLICARE TUTTI I CODICI DEL MODELLO : 0718 SMART C/M, ESCLUSI CRIPTATI=,  MODIFICANDO I SEGUENTI RECORD:
MODELLO: 0719 SMART C/S
CODICE: 4° CARATTERE= 9
PREZZI:
80150
100 AND 9°CARATTER= E190
109208
132202
100 AND LUNGH=8 CARATTERI190
120230
152294
115220
135260
167324

POTENZA:
12      2 X 12
17      2 X 17
24     2 X 24
FL. LUM. NOM.:
17053410
17903580
18803760
23604720
24804960
26005200
31706340
33306660
35007000
14602920
15353070
1610
2020 AND 5° CARATTERE “Z”4040
21254250
22354470
27105420
28605720
30006000
15303060
21404280
14502900
2020 AND 5° CARATTERE “Q”4040 */