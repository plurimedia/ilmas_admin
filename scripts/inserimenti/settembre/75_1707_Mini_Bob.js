/*
node --max-old-space-size=20000 scripts/inserimenti/settembre/bck.js 56b8aaa90de23c95600bd355

 node --max-old-space-size=2000 scripts/inserimenti/settembre/75_1707_Mini_Bob.js;
 */

var process = require('./../massive.js');
var modello = '56b8aaa90de23c95600bd355';
var query = {modello:modello };
var tag = 'dani_20180906_05';

var transform1 = {
    tag : tag,
    query: query,
    checkDuplicates: true,
    filter: [process.filters.isGenerazione],
    filterArguments : [{gen:['D1','D2','V1','G6', '3HE', '']}],
    transform: [
        {
            action: 'modifica_esistente',
            replace_rule: function (p) {
                switch (p.prezzo) {
                    case 168 :
                        p.prezzo = 160;
                        break;
                    case 228 :
                        if(process.hasCharAt(p.codice, 'E', 5)) {
                            p.prezzo = 220;
                        } else {
                            if(p.codice.length < 9) {
                                p.prezzo = 220;
                            }
                        }
                        break;
                    case 273 :
                        p.prezzo = 265;
                        break;
                    case 348 :
                        p.prezzo = 316;
                        break;
                    case 396 :
                        p.prezzo = 316;
                        break;
                    case 288 :
                        p.prezzo = 280;
                        break;
                    case 408 :
                        p.prezzo = 376;
                        break;
                    case 429 :
                        p.prezzo = 376;
                        break;
                    case 333 :
                        p.prezzo = 325;
                        break;
                    case 453 :
                        p.prezzo = 421;
                        break;
                    case 474 :
                        p.prezzo = 421;
                        break;
                }

                console.log('modifica prezzo '+ p.codice);
                return p;
            }
        }
    ]
};

process.execute(transform1)
    .then(function(err) { console.log("finito!!"); })
    .fail(function() { console.log("fallito!!"); });

