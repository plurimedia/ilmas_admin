var CONFIG = require(process.cwd() + '/server/config.js'),
    express = require('express'), // express
    multipart = require('connect-multiparty'), // per uplaod files
    cors = require('cors'), // stabilisce i cors per le api
    jwks = require('jwks-rsa'),
    jwt = require('express-jwt'),
    bodyParser = require('body-parser'), // body parser middleware
    app = express(), // express app
    _ = require('underscore'),
    /* @@@@@@ models @@@@@@ */
    ComuneSchema = require('./server/models/comune'),
    AttivitaSchema = require('./server/models/attivita'),
    AgenteSchema = require('./server/models/agente'),
    EventoSchema = require('./server/models/evento'),
    CategoriaSchema = require('./server/models/categoria'),
    SottocategoriaSchema = require('./server/models/sottocategoria'),
    ContattoSchema = require('./server/models/contatto'),
    ImmagineSchema = require('./server/models/immagine'),
    ModelloSchema = require('./server/models/modello'),
    NazioneSchema = require('./server/models/nazione'),
    OffertaSchema = require('./server/models/offerta'),
    ProdottoSchema = require('./server/models/prodotto'),
    CodiceEanSchema = require('./server/models/codiceEan'),
    SettingsSchema = require('./server/models/settings'),
    ComponenteSchema = require('./server/models/componente'),
    LineaCommercialeSchema = require('./server/models/lineaCommerciale'),
    LineaCommercialeInfoProdottoSchema = require('./server/models/lineaCommercialeInfoProdotto'),
    DwhEtichettaSchema = require('./server/models/dwhEtichetta'),
    MailingListSchema = require('./server/models/mailingList'),
    DwhLogSchema = require('./server/models/dwhlog'),
    DwhRicercaSchema = require('./server/models/dwhRicerca'),
    DwhSchedaTecnica = require('./server/models/dwhSchedaTecnica'),
    DwhSchedaDettaglio = require('./server/models/dwhSchedaDettaglio'),
    DashboardSchema = require('./server/models/dashboard'),
    ApiLog = require('./server/models/apiLog'),
    ConfigTipologieAssociazioniDSSchema = require('./server/models/configTipologieAssociazioniDS'),
    CreazioneDistinteSchema = require('./server/models/creazioneDistinte'),
    InstallazioneSchema = require('./server/models/installazione'),
    NewsSchema = require('./server/models/news'),
    ColoriSchema = require('./server/models/colori'),

    /* @@@@@@ middleware @@@@@@ */
    Authz = require(process.cwd() + '/server/middleware/authz.js'), // gestisce le logiche di autorizzazione per le routes
    /* @@@@@@ routes @@@@@@ */
    Comune = require('./server/routes/comune'),
    Nazione = require('./server/routes/nazione'),
    Contatto = require('./server/routes/contatto'),
    Prodotto = require('./server/routes/prodotto'),
    Modello = require('./server/routes/modello'),
    Categoria = require('./server/routes/categoria'),
    Agente = require('./server/routes/agente'),
    Evento = require('./server/routes/evento'),
    Attivita = require('./server/routes/attivita'),
    Offerta = require('./server/routes/offerta'),
    Immagine = require('./server/routes/immagine'),
    Pdf = require('./server/routes/pdf'),
    Email = require('./server/routes/email'),
    S3 = require('./server/routes/s3'),
    ImportGmail = require('./server/routes/importgmail'),
    ImportProdotti = require('./server/routes/importprodotti'),
    Settings = require('./server/routes/settings'),
    Componente = require('./server/routes/componente'),
    LineaCommerciale = require('./server/routes/lineaCommerciale'),
    DwhEtichetta = require('./server/routes/dwhEtichetta'), 
    MailingList = require('./server/routes/mailingList'), 
    DwhLog = require('./server/routes/dwhLog'),
    DwhRicerca = require('./server/routes/dwhRicerca'),
    DwhSchedaTecnica = require('./server/routes/dwhSchedaTecnica'),
    DwhSchedaDettaglio = require('./server/routes/dwhSchedaDettaglio'),
    Utils = require('./server/routes/utils'),
    Dashboard = require('./server/routes/dashboard'),
    ConfigTipologieAssociazioniDS = require('./server/routes/configTipologieAssociazioniDS'),
    CreazioneDistinte = require('./server/routes/creazioneDistinte'),
    Installazione = require('./server/routes/installazione'),
    News = require('./server/routes/news'),
    Colori = require('./server/routes/colori'),
    serieIlmas = 'aProdotti',
    serieEstro = 'aComponenti',

    /* @@@@@@ api @@@@@@ */
    Api = require('./server/api/api_v1');

require('better-logging')(console, {
  format: ctx => `${ctx.time} ${ctx.date} ${ctx.type} ${ctx.unix} ${ctx.msg}`
})

// DB
const db = require('./server/db')
db.setup()

// req.protocol = http invece di https???
app.enable('trust proxy')

// Prometheus endpoint
const { collectDefaultMetrics, register } = require('prom-client')
collectDefaultMetrics({
	timeout: 10000,
	gcDurationBuckets: [0.001, 0.01, 0.1, 1, 2, 5], // These are the default buckets.
})
app.get('/metrics', async (req, res) => {
  let str
  register.metrics().then(str => {
    console.log(str)
    res.status(200).send({ metrics: 'logged' })
  })
})

//variabili di utilita
app.use(function (req, res, next) {
    req.serieIlmas = serieIlmas
    req.serieEstro = serieEstro
    next();
  });

// cors origins
var whitelist = ['http://localhost:8080', 'https://www.ilmas.com', 'https://ilmas-sito-prod.herokuapp.com', 'https://ilmas-sito-test.herokuapp.com', 
        'https://ilmas-sito-test.plurimedia.it', 'https://ilmas-sito-prod.plurimedia.it', 'https://ilmas-configuratore-test.plurimedia.it', 'https://ilmas-configuratore.plurimedia.it',
        'https://estroconfig.ilmas.com', 'https://ilmas-sito.herokuapp.com', 'https://ilmas-configuratore.herokuapp.com']

corsOptions = {
   origin: function(origin, callback) {
      var originIsWhitelisted = whitelist.indexOf(origin) !== -1;
      callback(null, originIsWhitelisted);
   }
};

// token di auth0
/* var secretKey = new Buffer(CONFIG.AUTH0_CLIENT_SECRET, 'base64');
var authenticate = jwt({
    secret: secretKey
}); */
var authenticate = jwt({
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://${CONFIG.AUTH0_DOMAIN}/.well-known/jwks.json`
  }),
  audience: `https://${CONFIG.AUTH0_DOMAIN}/api/v2/`,
  issuer: `https://${CONFIG.AUTH0_DOMAIN}/`,
  algorithms: ['RS256']
});

const sistemaUser = function (req, res, next) {
  const namespace = 'https://plurimedia-auth0/' // See Auth0 rules

  try {
    req.user.roles = req.user[namespace + 'app_metadata'].roles
    req.user.tenant = req.user[namespace + 'app_metadata'].tenant
    req.user.app_metadata = req.user[namespace + 'app_metadata']
    req.user.user_metadata = req.user[namespace + 'user_metadata']
  } catch (e) {

  }
  next()
}

app.get('/auth_config.json', (req, res, next) => {
  res.status(200).send({
    "domain": "plurimedia.eu.auth0.com",
    "clientId": CONFIG.AUTH0_CLIENT_ID,
    "scope": "openid email user_metadata app_metadata picture",
    "responseType": "token id_token"
  })
})

app.use('/secure', authenticate, sistemaUser);
app.use(cors(corsOptions));
  
// use body parser
app.use(bodyParser.json({limit: '10000kb', extended: true}))
app.use(bodyParser.urlencoded({limit: '10000kb', extended: true}))

app.use(express.static('.tmp'));
app.use(express.static('client'));

app.use(multipart({
    uploadDir: '.tmp'
}));

/* @@@@@@@@@@@@@@ ROUTES @@@@@@@@@@@@@@  */

app.get('/secure/comune/search', Comune.search); // cerca comuni
app.get('/secure/nazioni', Nazione.query); // lista nazioni

/*====CONTATTO======*/
app.post('/secure/contatto', Contatto.save); // salva un contatto
app.delete('/secure/contatto/:id', Contatto.delete); // elimina un contatto
app.put('/secure/contatto/:id', Contatto.update); // aggiorna contatto
app.get('/secure/contatto/:id', Contatto.searchById); // recupera un contatto per id
app.get('/secure/contatti/last', Contatto.last); // recupera gli ultimi contatti aggiornati
app.get('/secure/contatti/search', Contatto.search); // cerca contatti
app.get('/secure/contatti/aziende', Contatto.aziende); // cerca aziende nei contatti
app.get('/secure/contatti/searchemail', Contatto.searchByEmail); // cerca contatto per mail
app.get('/secure/contatti/tags', Contatto.tags); // cerca tag nei contatti
app.get('/secure/contatti/doppi', Contatto.doppi); // recupera i contatti doppi
app.get('/secure/contatti/nazioni', Contatto.nazioni); // recupera le nazioni dei contatti
app.get('/secure/contatti/province', Contatto.province); // recupera le province dei contatti

/*====PRODOTTO======*/
app.post('/secure/prodotto', function (req, res, next) { // salvataggio prodotto: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, Prodotto.save);
app.delete('/secure/prodotto/:id', function (req, res, next) { // elimina prodotto: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, Prodotto.delete);
app.put('/secure/prodotto/:id', function (req, res, next) { // modifica prodotto: può farlo solo l'admin prodotti 
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, Prodotto.update); // aggiorna prodotto


app.get('/secure/prodotti/last/:generazione', function (req, res, next) { // salvataggio prodotto: può farlo solo l'admin prodotti
  if (!Authz(req.user).adminProdotti())
      return res.sendStatus(401);
  next();
}, Prodotto.last); // recupera gli ultimi prodotti aggiornati
app.get('/secure/prodotti/last/', Prodotto.last); // recupera gli ultimi prodotti aggiornati

app.get('/secure/prodotto/:id', Prodotto.get); // recupera il singolo prodotto
app.get('/secure/prodotto/accessori/:id', Prodotto.accessori); // recupera agli accessori di un prodotto
app.get('/secure/prodotti/sottocategorie', Prodotto.sottocategorie); // cerca sottocategorie nei prodotti
app.get('/prodotti/search', Prodotto.search); // cerca prodotti
app.get('/prodotti/profestro', Prodotto.profili_estro); // elenco Codice profilo
app.get('/prodotti/fontestro', Prodotto.fonti_estro); // elenco Codice fonte luminosa
app.get('/colori/finiestro', Colori.finiture_estro); // elenco Finitura
app.get('/secure/prodotti/search/codice', Prodotto.searchcodice); // cerca codice prodotto
app.get('/secure/prodotti/search/codiceValido', Prodotto.searchCodiceValido); // cerca codice prodotto 
app.get('/secure/prodotto/codice/:codice', Prodotto.getByCodice); // cerca codice prodotto
app.put('/secure/prodotto/image/update', Prodotto.updateImage); // aggiorna un tipo di immagine
app.put('/secure/prodotto/ldt/update', Prodotto.batchUpdateLdt); // aggiorna in blocco tutti i codici simili con lo stesso file ldt
app.get('/secure/prodottomoduli', Prodotto.moduli); // trova i moduli di un prodotto di un particolare modello
app.get('/secure/prodottocolori', Prodotto.colori); // trova i colori di un prodotto di un particolare modello
app.get('/secure/prodottofasci', Prodotto.fasci); // trova i fasci di un prodotto di un particolare modello
app.get('/secure/prodotti/csvmetel/:anno', Prodotto.csvMetel);
app.get('/prodotti/csvDistintaBase', Prodotto.csvDistintaBase);
app.get('/prodotti/xlsProdottiKite', Prodotto.xlsProdottiKite);
app.put('/secure/prodotto/categoria/update', Prodotto.batchUpdateCategoria); // aggiorna in blocco tutti i codici dsi una categoria
app.get('/secure/prodotti/prodottiinmodello/:id', Prodotto.prodottiInModello); // conta i prodotti di un modello
app.get('/secure/prodotto/:idprodotto/schedatecnica/:lingua/', Pdf.prodottoSchedaTecnica);
app.get('/secure/prodotti/codiciean', Prodotto.codiciean); // cerca prodotti con codice ean
app.get('/secure/prodotti/getEanByidCodice', Prodotto.getEanByidCodice); // cerca prodotti con codice ean

app.get('/secure/prodotti/countean', Prodotto.countean);
app.get('/secure/prodotti/count', Prodotto.count);
app.get('/secure/modelli/count', Modello.count);
app.get('/secure/categorie/count', Categoria.count);
app.get('/secure/componenti/count', Componente.count);
app.get('/secure/dwhEtichetta/count', DwhEtichetta.count);
app.get('/secure/contatti/count', Contatto.count);
app.get('/secure/offerte/count', Offerta.count);
app.get('/secure/attivita/count', Attivita.count);

/*====MODELLO======*/
app.get('/secure/modelli', Modello.modelli); // cerca modelli dei prodotti
app.get('/secure/modelli/last', Modello.last);
app.get('/secure/modello/:id', Modello.get); // recupera il singolo modello
app.get('/secure/modello/:id/distinta', Modello.generaDistinta); // genera la distinta base dei prodotti di quel modello
app.get('/secure/modelli/:query', Modello.cerca); // cerca modello
app.post('/secure/modello', function (req, res, next) { // salvataggio modello: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, Modello.save);
app.put('/secure/modello/:id', function (req, res, next) { // update modello: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, Modello.update);
app.delete('/secure/modello/:id', function (req, res, next) { // elimina modello: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, Modello.delete);

/*====CATEGORIA======*/
app.get('/secure/categorie', Categoria.list); // restituisce tutte le catgorie in ordine di data mod
app.get('/secure/categorie/last', Categoria.last);
app.post('/secure/categoria', function (req, res, next) { // salvataggio categoria: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, Categoria.save);
app.put('/secure/categoria/:id', function (req, res, next) { // update categoria: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, Categoria.update);
app.get('/secure/categoria/:id', Categoria.get); // recupera la singola categoria
app.delete('/secure/categoria/:id', function (req, res, next) { // elimina categoria: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, Categoria.delete);
app.get('/secure/categorie/:query', Categoria.cerca);

/*====ATTIVITA======*/
app.get('/secure/attivita', Attivita.list); // restituisce tutte le catgorie in ordine di data mod
app.get('/secure/attivita/last', Attivita.last);
app.post('/secure/attivita', function (req, res, next) {
    if (!Authz(req.user).adminProdotti() && !Authz(req.user).admin())
        return res.sendStatus(401);
    next();
}, Attivita.save);

app.put('/secure/attivita/:id', function (req, res, next) {
    if (!Authz(req.user).adminProdotti() && !Authz(req.user).admin())
        return res.sendStatus(401);
    next();
}, Attivita.update);
app.get('/secure/attivita/:id', function (req, res, next) { 
    if (!Authz(req.user).adminProdotti() && !Authz(req.user).admin())
        return res.sendStatus(401);
    next();
}, Attivita.delete);

 app.get('/attivita/search', Attivita.cerca); // cerca atività


/*====OFFERTA======*/
app.post('/secure/offerta', Offerta.save); // salva un'offerta
app.put('/secure/offerta/:id', Offerta.update); // aggiorna offerta
app.get('/secure/offerte/last', Offerta.last); // recupera le ultime offerte aggiornate
app.get('/secure/offerte/search', Offerta.search); // cerca offerte
app.delete('/secure/offerta/:id', Offerta.delete); // elimina un offerta

/*====IMMAGINE======*/
app.post('/secure/image/upload', multipart(), function (req, res, next) { // upload immagine: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, Immagine.upload);

app.delete('/secure/image', Immagine.delete); // delete immagine
app.put('/secure/image/:id', function (req, res, next) { // modifica immagine: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, Immagine.update);
app.get('/secure/image/:id', Immagine.get); // recupera il singolo prodotto
app.get('/secure/image/last', Immagine.last); // lista delle ultime immagini caricate
app.get('/secure/image/last/:tipo', Immagine.lastTipo); // lista delle ultime immagini caricate per tipo
app.post('/secure/image/search', Immagine.search); // cerca immagini
app.post('/secure/image/batchupdate', Immagine.batchupdate); // aggiorna in blocco le immagini su prodotti, modelli e categorie
app.post('/secure/pdf', Pdf.create);
app.post('/secure/email', Email.send); // invia una mail
app.get('/secure/email/:id', Email.stato); // controlla lo stato di una mail
app.post('/s3', S3.getSignedUrl); // genera un signed url di s3 per fare un upload lato client
app.post('/secure/import/gmail', multipart(), ImportGmail.import); // import csv contatti da gmail
app.post('/secure/import/prodotti/:tipo', multipart(), ImportProdotti.import); // import csv prodotti
app.post('/pdf', Pdf.create); // crea un pdf

/*====SETTING======*/
app.get('/secure/settings', Settings.get); // recupera le impostazioni
app.post('/secure/settings', Settings.save); // salva le impostazioni

/*====COMPONENTE======*/
app.get('/secure/componenti/last', Componente.last); // recupera gli ultimi prodotti aggiornati
app.get('/secure/componente/:id', Componente.get); // recupera il singolo prodotto
app.get('/secure/componenti/search', Componente.search); // cerca prodotti
app.get('/secure/componenti/search/codice', Componente.searchcodice); // cerca codice prodotto
app.post('/secure/componente', function (req, res, next) { // salvataggio componente: può farlo solo l'admin prodotti
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).commerciale()))
        return res.sendStatus(401);
    next();
}, Componente.save);
app.delete('/secure/componente/:id', function (req, res, next) { // elimina componente: può farlo solo l'admin prodotti
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).commerciale()))
        return res.sendStatus(401);
    next();
}, Componente.delete);
app.put('/secure/componente/:id', function (req, res, next) { // modifica componente: può farlo solo l'admin prodotti
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).commerciale()))
        return res.sendStatus(401);
    next();
}, Componente.update); // aggiorna componente

/*====LINEA COMMERCIALE======*/
app.get('/secure/lineeCommerciali/last', LineaCommerciale.last); // recupera le ultime LineaCommerciale aggiornate
app.get('/secure/lineaCommerciale/:id', LineaCommerciale.get); // recupera la  singola LineaCommerciale
app.get('/secure/lineeCommerciali/search', LineaCommerciale.search); // cerca LineaCommerciale
app.get('/secure/lineeCommerciali/cercaProdotto/:idLinea/:codiceProdotto', LineaCommerciale.cercaProdotto);
app.post('/secure/lineaCommerciale', function (req, res, next) { // salvataggio LineaCommerciale: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, LineaCommerciale.save);
app.delete('/secure/lineaCommerciale/:id', function (req, res, next) { // elimina LineaCommerciale: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, LineaCommerciale.delete);
app.put('/secure/lineaCommerciale/:id', function (req, res, next) { // modifica LineaCommerciale: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, LineaCommerciale.update); // aggiorna LineaCommerciale
app.post('/secure/lineaCommerciale/creaAssociazione', LineaCommerciale.creaAssociazione); // aggiorna in blocco le immagini su prodotti, modelli e categorie
app.get('/secure/lineaCommerciale/creaAssociazioneMassiva', LineaCommerciale.creaAssociazioneMassiva);
app.post('/secure/lineaCommerciale/eliminaAssociazione', LineaCommerciale.eliminaAssociazione); // aggiorna in blocco le immagini su prodotti, modelli e categorie
app.get('/secure/lineeCommerciali/getAll', LineaCommerciale.getAll);

/*====DWH ETICHETTA======*/
app.get('/secure/dwhEtichetta/etichetteGrigie/:anno', DwhEtichetta.etichetteGrigie);// recupera le ultime riga del data ware house etichetta
app.get('/secure/dwhEtichetta/etichetteBianche/:anno', DwhEtichetta.etichetteBianche);// recupera le ultime riga del data ware house etichetta
app.get('/secure/dwhEtichetta/etichetteEstro/:anno', DwhEtichetta.etichetteEstro);// recupera le ultime riga del data ware house etichetta
app.post('/secure/dwhEtichetta', DwhEtichetta.save);  
app.get('/secure/dwhEtichetta/mesi/:anno', DwhEtichetta.mesi);// recupera tutti i mesi della tabella
app.get('/secure/dwhEtichetta/etichetteGrigieInMesi/:anno', DwhEtichetta.etichetteGrigieInMesi);// recupera tutti i dati per mese
app.get('/secure/dwhEtichetta/etichetteBiancheInMesi/:anno', DwhEtichetta.etichetteBiancheInMesi);// recupera tutti i dati per mese
app.get('/secure/dwhEtichetta/etichetteEstroInMesi/:anno', DwhEtichetta.etichetteEstroInMesi);// recupera tutti i dati per mese

/*====MAILING LIST======*/
app.get('/secure/mailingList/last', MailingList.last); // list modelli dei prodotti
app.get('/secure/mailingList/:id',  MailingList.get); // recupera il singolo record
app.get('/secure/mailingList/:query', MailingList.search); // cerca
app.post('/secure/mailingList', function (req, res, next) { // salvataggio modello: può farlo solo il commerciale
    if (!Authz(req.user).commerciale())
        return res.sendStatus(401);
    next();
}, MailingList.save);
app.put('/secure/mailingList/:id', function (req, res, next) { // update mail: può farlo solo il commerciale
    if (!Authz(req.user).commerciale())
        return res.sendStatus(401);
    next();
}, MailingList.update);
app.delete('/secure/mailingList/:id', function (req, res, next) { // elimina mail: può farlo solo il commerciale
    if (!Authz(req.user).commerciale())
        return res.sendStatus(401);
    next();
}, MailingList.delete);

/*====DWH RICERCHE======*/
app.get('/secure/dwhRicerche/:anno', DwhRicerca.dwhRicerche);

/*====DWH SCHEDE TECNICHE======*/
app.get('/secure/dwhSchedeTecniche/:anno', DwhSchedaTecnica.dwhSchedeTecniche);

/*====DWH SCHEDE DETTAGLIO======*/
app.get('/secure/dwhSchedeDettaglio/:anno', DwhSchedaDettaglio.dwhSchedeDettaglio);

/*====PING======*/
app.get('/prodotti/ping', Prodotto.ping); // chiamata per capire se esiste la session

/*====LOG======*/
app.post('/secure/dwhLog', DwhLog.save);
app.get('/secure/dwhLog/last', DwhLog.last);

/*====UTIL======*/
app.get('/utils/environment', Utils.environment);

/*====DASHBOARD======*/
app.post('/secure/dashboard', Dashboard.save);
app.get('/secure/dashboard', Dashboard.get);


/**====CONFIG TipologieAssociazioni Distinta BASE======*/

app.get('/secure/configTipologieAssociazioniDS', ConfigTipologieAssociazioniDS.list); // restituisce tutte le catgorie in ordine di data mod
app.post('/secure/configTipologieAssociazioniDS', function (req, res, next) {
    if (!Authz(req.user).adminProdotti() && !Authz(req.user).admin())
        return res.sendStatus(401);
    next();
}, ConfigTipologieAssociazioniDS.save);

app.put('/secure/configTipologieAssociazioniDS/:id', function (req, res, next) {
    if (!Authz(req.user).adminProdotti() && !Authz(req.user).admin())
        return res.sendStatus(401);
    next();
}, ConfigTipologieAssociazioniDS.update);
app.get('/secure/configTipologieAssociazioniDS/:id', Attivita.get);//questa riga è palesemente sbagliata? Dani 19/02/2020
app.delete('/secure/configTipologieAssociazioniDS/:id', function (req, res, next) { 
    if (!Authz(req.user).adminProdotti() && !Authz(req.user).admin())
        return res.sendStatus(401);
    next();
}, ConfigTipologieAssociazioniDS.delete);

/**====CONFIG colori prodotti======*/

app.get('/secure/colori', Colori.list); // restituisce tutte le catgorie in ordine di data mod
app.get('/secure/colori', Colori.list); // restituisce tutte le catgorie in ordine di data mod
app.post('/secure/colori', function (req, res, next) {
    if (!Authz(req.user).adminProdotti() && !Authz(req.user).admin())
        return res.sendStatus(401);
    next();
}, Colori.save);

app.put('/secure/colori/:id', function (req, res, next) {
    if (!Authz(req.user).adminProdotti() && !Authz(req.user).admin())
        return res.sendStatus(401);
    next();
}, Colori.update);
app.get('/secure/colori/:id', Colori.get);
app.get('/secure/colore/codice/:codice', Colori.getByCodice);
app.delete('/secure/colori/:id', function (req, res, next) { 
    if (!Authz(req.user).adminProdotti() && !Authz(req.user).admin())
        return res.sendStatus(401);
    next();
}, Colori.delete);
app.get('/colori/search', Colori.cerca); // cerca atività

//AGENTE

app.get('/secure/agente/last', Agente.last); 
app.get('/secure/agente/:id', Agente.get); 
app.post('/secure/agente', function (req, res, next) { // salvataggio agente: può farlo solo l'admin prodotti
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).admin()))
        return res.sendStatus(401);
    next();
}, Agente.save);
app.delete('/secure/agente/:id', function (req, res, next) { // elimina componente: può farlo solo l'admin prodotti
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).admin()))
        return res.sendStatus(401);
    next();
}, Agente.delete);
app.put('/secure/agente/:id', function (req, res, next) { // modifica agente: può farlo solo l'admin prodotti
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).admin()))
        return res.sendStatus(401);
    next();
}, Agente.update); 

app.get('/secure/evento/last', Evento.last); 
app.get('/secure/evento/:id', Evento.get); 
app.post('/secure/evento', function (req, res, next) {
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).admin()))
        return res.sendStatus(401);
    next();
}, Evento.save);
app.delete('/secure/evento/:id', function (req, res, next) { 
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).admin()))
        return res.sendStatus(401);
    next();
}, Evento.delete);
app.put('/secure/evento/:id', function (req, res, next) {
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).admin()))
        return res.sendStatus(401);
    next();
}, Evento.update); 
app.post('/secure/imageevento/upload', multipart(), function (req, res, next) { // upload immagine: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, Evento.upload);

app.get('/secure/installazione/last', Installazione.last); 
app.get('/secure/installazione/search', Installazione.cerca); // cerca prodotti
app.get('/secure/installazione/:id', Installazione.get); 
app.post('/secure/installazione', function (req, res, next) {
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).admin()))
        return res.sendStatus(401);
    next();
}, Installazione.save);
app.delete('/secure/installazione/:id', function (req, res, next) { 
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).admin()))
        return res.sendStatus(401);
    next();
}, Installazione.delete);
app.put('/secure/installazione/:id', function (req, res, next) {
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).admin()))
        return res.sendStatus(401);
    next();
}, Installazione.update);

app.post('/secure/imageinstallazione/upload', multipart(), function (req, res, next) { // upload immagine: può farlo solo l'admin prodotti
    if (!Authz(req.user).adminProdotti())
        return res.sendStatus(401);
    next();
}, Installazione.upload);


app.get('/secure/news/last', News.last); 
app.get('/secure/news/search', News.cerca); 
app.get('/secure/news/:id', News.get); 
app.post('/secure/news', function (req, res, next) {
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).admin()))
        return res.sendStatus(401);
    next();
}, News.save);
app.delete('/secure/news/:id', function (req, res, next) { 
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).admin()))
        return res.sendStatus(401);
    next();
}, News.delete);
app.put('/secure/news/:id', function (req, res, next) {
    if ((!Authz(req.user).adminProdotti()) && (!Authz(req.user).admin()))
        return res.sendStatus(401);
    next();
}, News.update);
 

//api utilizzate dal sito
app.get('/api/v1/prodotto/:idprodotto/', Api.prodottoSingolo);
app.get('/api/v1/prodotto/:idprodotto/schedatecnica/:lingua/', Api.prodottoSchedaTecnica);
app.get('/api/v1/prodotto/:idprodotto/schedatecnica/:lingua/:provenienza', Api.prodottoSchedaTecnica);
app.get('/api/v1/prodotto/:idprodotto/schedatecnica/:lingua/infoLinea/:codiceLinea', Pdf.prodottoSchedaTecnicaInfoLinea);//restituisce l'url della scheda tecnica di quel prodotto sostituendo i dati con quelli della linea
app.get('/api/v1/prodotto/:search', Api.searchProdotti); //cerca il codice della scheda tecnica, la chiamata arriva da ilmas sito
app.get('/api/v1/prodotti/search', Api.search); // cerca prodotti, la chiamata arriva dall'app
app.post('/api/v1/prodotti/searchv2', Api.searchv2); // cerca prodotti, la chiamata arriva dall'app
app.get('/api/v1/prodotto/:idprodotto/:provenienza', Api.dettaglioProdotto);

app.get('/api/v1/categorie', Api.categoriaList); // restituisce tutte le catgorie in ordine di data mod
app.get('/api/v1/modelli/:categoria', Api.modelliCategoria); // restituisce tutti i modelli di una categoria
app.get('/api/v1/prodotti/:modello', Api.codiciModello); // restituisce tutti i codici prodotto di un modello
app.get('/api/v1/modello', Api.cercaModello); // cerca un modello per nome

app.get('/api/v1/agenti', Api.agentiList); // restituisce la lista degli agenti in ordine di Ragione Sociale
app.get('/api/v1/agente/:idAgente', Api.getAgente); // restituisce l'agente

app.get('/api/v1/eventi', Api.eventiList); // restituisce la lista degli eventi in ordine di Ragione Sociale
app.get('/api/v1/evento/:idEvento', Api.getEvento); // restituisce l'evento

app.get('/api/v1/installazioni/:limit', Api.installazioniList); // restituisce la lista delle installazioni
app.get('/api/v1/installazioni', Api.installazioniList); // restituisce la lista delle installazioni
app.get('/api/v1/installazione/:idInstallazione', Api.getInstallazione); // restituisce l'evento

app.get('/api/v1/news', Api.newsList); // restituisce la lista delle news
app.get('/api/v1/news/:idNews', Api.getNews); // restituisce la news

app.get('/api/v1/autocomplete/prodotti', Api.getProdottiAutocomplete)
app.get('/api/v1/autocomplete/finiture', Api.getFinitureAutocomplete)
app.post('/api/v1/estro/schedatecnicaestro/', Api.prodottoSchedaTecnicaEstro);

//api utilizzate da KITE
//app.get('/api/v1/distintaBase/china1', Api.distintaBaseChina1);
//app.get('/api/v1/modelli', Api.tabellaModelli);
//app.get('/api/v1/categorie', Api.tabellaCategorie);
//app.get('/api/v1/prodotti', Api.tabellaProdotti);

app.get('/barcode', Pdf.barcode); //Barcode generator

/*====CSV======*/
app.get('/export/csv/modelli', Modello.csvModelli);

/*====stato creazioni distinte base======*/
app.get('/secure/creazionedistinte/last', CreazioneDistinte.last);
//app.get('/secure/creazionedistinte/create', CreazioneDistinte.create);
//app.post('/secure/creazionedistinte/close/:id', CreazioneDistinte.close);

/***** API Configuratore *****/
app.get('/api/v1/estro/id-profili', Api.getCategoriaIdProfili);
app.get('/api/v1/estro/id-fonti', Api.getCategoriaIdFonti);
app.get('/api/v1/estro/applicazioni', Api.getApplicazioni);
app.get('/api/v1/estro/profili', Api.getProfili);
app.get('/api/v1/estro/diffusori', Api.getDiffusori);//prendo i modelli associati ai prodotti (quindi partendo dai prodotti)
app.get('/api/v1/estro/led-temperatura', Api.getLedTemperatura);
app.get('/api/v1/estro/led-potenza', Api.getLedPotenza);
app.get('/api/v1/estro/led-fascio', Api.getLedFascio);
app.get('/api/v1/estro/fonte', Api.getFonteLuminosaCode);
app.get('/api/v1/estro/finiture', Api.getFiniture);
app.post('/api/mail/configuratore-send', Email.configuratoreSend);

app.get('/api/v1/prova', Prodotto.prova_kite);

// listen on...
 var server = app.listen(process.env.PORT || 3000);

server.timeout = 1000000; 
