const mongoose = require('mongoose'),
  XLSX = require('xlsx'),
  db = 'mongodb://ibm_cloud_c24790b0_9ea8_4e6a_9948_652fdccbdc08:1f40077a47e414d422627e02dc313b2c51a2a4490e91831ec6dcebac45c58139@37b3e0eb-8c5d-44cf-8261-b8295337a97a-0.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149,37b3e0eb-8c5d-44cf-8261-b8295337a97a-1.br38q28f0334iom5lv4g.databases.appdomain.cloud:31149/ilmas_test?authSource=admin&ssl=true&replicaSet=replset',
  ModelloSchema = require(process.cwd() + '/server/models/modello'),
  CategoriaSchema = require(process.cwd() + '/server/models/categoria'),
  CodiceEanSchema = require(process.cwd() + '/server/models/codiceEan'),
  ProdottoSchema = require(process.cwd() + '/server/models/prodotto'),
  Modello = mongoose.model('Modello'),
  Categoria = mongoose.model('Categoria'),
  CodiceEan = mongoose.model('CodiceEan'),
  Prodotto = mongoose.model('Prodotto');

let mongoOptions = {
  sslValidate: true,
  tls: true,
  tlsCAFile: 'ibm_mongodb_ca.pem',
  useUnifiedTopology: true,
  useNewUrlParser: true
}

const objFindOne = (obj, query) => {
  return new Promise((resolve, reject) => {
    obj.findOne(query, (err, result) => {
      if (err) reject(err)
      else resolve(result)
    })
  })
}

const insertOne = (p, idx) => {
  return new Promise((resolve, reject) => {
    let prodotto = new Prodotto(p)
    prodotto.mdate = new Date()
    prodotto.codice = prodotto.codice.toUpperCase().trim()
    prodotto.serie = 'aProdotti'
    prodotto.save(function(err, doc)
    {
      if (err)
      {
        console.log('Riga ' + (idx + 2) + ' ERROR creating', err)
        reject(err)
      }
      else
      {
        console.log('Riga ' + (idx + 2) + ' created')
        resolve(doc)
      }
    })
  })
}

const updateOne = (prodotto, p, idx) => {
  return new Promise((resolve, reject) => {
    p.serie = 'aProdotti'
    p.codice = p.codice.toUpperCase().trim()
    prodotto.updateOne(p, { runValidators: true }, function(err, doc) {
      if (err)
      {
        console.log('Riga ' + (idx + 2) + ' ERROR updating', err)
        reject(err)
      }
      else
      {
        console.log('Riga ' + (idx + 2) + ' updated')
        resolve(doc)
      }
    })
  })
}

mongoose.connect(db, mongoOptions);

console.log('connected')

mongoose.connection.once('open', async () =>
{
  let workbook = XLSX.readFile(process.cwd() + '/import.xlsx'),
    sheet_name_list = workbook.SheetNames;

  dati = XLSX.utils.sheet_to_json(workbook.Sheets[sheet_name_list[0]])

  // console.log(dati[1])
  console.log('File letto')

  let pid, modello, categoria, dbResult, prodotto
  for (let [idx, p] of dati.entries())
  {
    try {
      prodotto = await objFindOne(Prodotto, { codice: p.codice })
    } catch {
      prodotto = null
    }

    if (!modello)
    {
      dbResult = await objFindOne(Modello, { nome: p.modello })
      if (dbResult) modello = dbResult._id
      else throw(new Error('Modello non trovato: ' + p.modello))

      dbResult = await objFindOne(Categoria, { nome: p.categoria })
      if (dbResult) categoria = dbResult._id
      else throw(new Error('Categoria non trovata: ' + p.categoria))
    }
    p.modello = modello
    p.categoria = categoria
    // console.log(p)
    if (prodotto)
      await updateOne(prodotto, p, idx)
    else
      await insertOne(p, idx)
  }
  console.log('Fine import')
})
