app
    .directive('loading', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'html/directives/loading.html',
            scope: {
                testo: "@"
            }
        }
    })
    .directive('immagineprodotto', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'html/directives/immagineprodotto.html',
            scope: {
                foto: "@",
                width: "@",
                height: "@"
            }
        }
    })
    .directive('offertastato', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'html/directives/offertastato.html',
            scope: {
                stato: "@"
            }
        }
    })
    .directive('statomail', function () {
        return {
            restrict: 'E',
            replace: true,
            templateUrl: 'html/directives/statomail.html',
            scope: {
                stato: "@"
            }
        }
    });
