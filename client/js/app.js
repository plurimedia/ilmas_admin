var app = angular.module( 'app', ['bootcomplete', 'ngRoute', 'ngResource', 'ngSanitize', 'ngNotify', 'ngFileUpload', 'ngAnimate', 'ngTagsInput', 'mgcrea.ngStrap', 'summernote', 'angular-jwt', 'angular-cloudinary', 'angularTree']) 
.config( function ( 
    $routeProvider, 
    $httpProvider,
    $locationProvider, 
    jwtOptionsProvider  
     ) {
  $routeProvider
  .when('/contatti', {
    controller: 'contatti',
    templateUrl: '/html/views/contatti.html',
    requiresLogin: true
  }) 
  .when('/prodotti/:generazione', {
    controller: 'prodotti',
    templateUrl: '/html/views/prodotti.html',
    requiresLogin: true
  }) 
  .when('/prodotti', {
    controller: 'prodotti',
    templateUrl: '/html/views/prodotti.html',
    requiresLogin: true
  })  
  .when('/prodottiEstro/:generazione', {
    controller: 'prodotti',
    templateUrl: '/html/views/prodotti.html',
    requiresLogin: true,
    serie: 'aComponenti'
  }) 
  .when('/prodottiEstro', {
    controller: 'prodotti',
    templateUrl: '/html/views/prodotti.html',
    requiresLogin: true,
    serie: 'aComponenti'
  })  
  .when('/distintaBase', {
    controller: 'prodotti',
    templateUrl: '/html/views/distintaBase.html',
    requiresLogin: true
  }) 
  .when('/codiciEan', {
    controller: 'codiciEan',
    templateUrl: '/html/views/codiciEan.html',
    requiresLogin: true
  }) 
  .when('/etichette', {
    controller: 'etichette',
    templateUrl: '/html/views/etichette.html',
    requiresLogin: true
  })
  .when('/etichetteEstro', {
    controller: 'etichetteEstro',
    templateUrl: '/html/views/etichetteEstro.html',
    requiresLogin: true
  })
  .when('/dwhEtichette', {
    controller: 'dwhEtichette',
    templateUrl: '/html/views/dwhEtichette.html',
    requiresLogin: true
  })
  .when('/categorie', {
    controller: 'categorie',
    templateUrl: '/html/views/categorie.html',
    requiresLogin: true
  })
   .when('/modelli', {
    controller: 'modelli',
    templateUrl: '/html/views/modelli.html',
    requiresLogin: true
  })
  .when('/categorieEstro', {
    controller: 'categorie',
    templateUrl: '/html/views/categorie.html',
    requiresLogin: true,
    serie: 'aComponenti'
  })
   .when('/modelliEstro', {
    controller: 'modelli',
    templateUrl: '/html/views/modelli.html',
    requiresLogin: true,
    serie: 'aComponenti'
  })
  .when('/immagini', {
    controller: 'immagini',
    templateUrl: '/html/views/immagini.html',
    requiresLogin: true
  })
  .when('/offerte', {
    controller: 'offerte',
    templateUrl: '/html/views/offerte.html',
    requiresLogin: true
  })
  .when('/login', {
    controller: 'login',
    templateUrl: '/html/views/login.html'
  })
  .when('/settings', {
    controller: 'settings',
    templateUrl: '/html/views/settings.html'
  })
  .when('/componenti', {
    controller: 'componenti',
    templateUrl: '/html/views/componenti.html',
    requiresLogin: true
  })
  .when('/lineeCommerciali', { //CRUD LINEA COMMERCIALE
    controller: 'lineeCommerciali',
    templateUrl: '/html/views/lineeCommerciali.html',
    requiresLogin: true
  })
  .when('/lineaCommercialeAssociazioni/:id', { // //CRUD ASSOCIAZIONE LINEA COMMERCIALE
    controller: 'lineaCommercialeAssociazioni',
    templateUrl: '/html/views/lineaCommercialeAssociazioni.html',
    requiresLogin: true
  })
  .when('/mailingList', {
    controller: 'mailingList',
    templateUrl: '/html/views/mailingList.html',
    requiresLogin: true
  })
  .when('/dwhLogs', {
    controller: 'dwhLogs',
    templateUrl: '/html/views/dwhLogs.html',
    requiresLogin: true
  })
  .when('/home', {
    controller: 'home',
    templateUrl: '/html/views/home.html',
    requiresLogin: true
  })
  .when('/dwhRicerche', {
    controller: 'dwhRicerche',
    templateUrl: '/html/views/dwhRicerche.html',
    requiresLogin: true
  })
  .when('/dwhSchedeTecniche', {
    controller: 'dwhSchedeTecniche',
    templateUrl: '/html/views/dwhSchedeTecniche.html',
    requiresLogin: true
  })
  .when('/dwhSchedeDettaglio', {
    controller: 'dwhSchedeDettaglio',
    templateUrl: '/html/views/dwhSchedeDettaglio.html',
    requiresLogin: true
  })
  .when('/schedaTecnicaEstro', {
    controller: 'schedaTecnicaEstro',
    templateUrl: '/html/views/schedaTecnicaEstro.html',
    requiresLogin: true
  })
  .when('/elencoattivita', {
    controller: 'attivita', 
    templateUrl: '/html/views/elencoattivita.html',
    requiresLogin: true
  })
  .when('/configTipologieAssociazioniDS', {
    controller: 'configTipologieAssociazioniDS', 
    templateUrl: '/html/views/configTipologieAssociazioniDS.html',
    requiresLogin: false
  })
  .when('/agenti', {
    controller: 'agenti',
    templateUrl: '/html/views/agenti.html',
    requiresLogin: true
  })
  .when('/eventi', {
    controller: 'eventi',
    templateUrl: '/html/views/eventi.html',
    requiresLogin: true
  })
  .when('/installazioni', {
    controller: 'installazioni',
    templateUrl: '/html/views/installazioni.html',
    requiresLogin: true
  })
  .when('/news', {
    controller: 'news',
    templateUrl: '/html/views/news.html',
    requiresLogin: true
  })
  .when('/colori', {
    controller: 'colori',
    templateUrl: '/html/views/colori.html',
    requiresLogin: true
  })

  // We're annotating this function so that the `store` is injected correctly when this file is minified
      jwtOptionsProvider.config({
        tokenGetter: function($rootScope) {
          return localStorage.getItem('id_token');
        }
      });
  
      $httpProvider.interceptors.push('jwtInterceptor');
})  

.run(function 
  ($rootScope, jwtHelper, $location, $timeout, PRODOTTO,SCHEDATECNICA, CONFIG, DWHLOG, UTILS) {

  $rootScope.heightMax = window.screen.availHeight;
  $rootScope.user = JSON.parse(localStorage.getItem('profile')) || {};

  /**
     * Retrieves the auth configuration from the server
     */
   const fetchAuthConfig = () => fetch("/auth_config.json");
    
   /**
    * Initializes the Auth0 client
    */
   const configureClient = async () => {
     const response = await fetchAuthConfig();
     const config = await response.json();
   
     auth0 = await createAuth0Client({
       domain: config.domain,
       client_id: config.clientId,
       audience: 'https://' + config.domain + '/api/v2/',
       issuer: 'https://' + config.domain + '/',
       scope: config.scope,
       responseType: 'token id_token'
     });
     $rootScope.auth0 = auth0
     // console.log('configureClient', window.location.origin)
   };

  /* $rootScope.lock = new Auth0Lock('f0vmDzASGGZMFGnm3pVF0m7UI2NSiodP', 'plurimedia.eu.auth0.com', {
    auth: {
      responseType: 'token',
      redirectUrl: window.location.protocol + "//" + window.location.host + "/#login",
      params: {
        scope: 'openid email user_metadata app_metadata picture'
      }
  }}) */

  document.addEventListener('authenticated', async function (e) { 
    console.log('mi è arrivato l evento authenticated', e.detail)
    const profile = e.detail.profile
    $rootScope.auth0 = e.detail.auth0

    const namespace = "https://plurimedia-auth0/"; // See Auth0 rules
    profile.roles = profile[namespace + "app_metadata"].roles
    profile.tenant = profile[namespace + "app_metadata"].tenant
    profile.name = profile[namespace + "user_metadata"].name
    profile.app_metadata = profile[namespace + "app_metadata"]
    profile.user_metadata = profile[namespace + "user_metadata"]
    profile.expires = 36000 * 1000 + new Date().getTime(); // 12h modificare il 36000 in base all'expired impostato su Auth0
    var user_profile = JSON.stringify(profile),
      json_user_profile = JSON.parse(user_profile),
      roles = json_user_profile.roles;
    const tok = await $rootScope.auth0.getTokenSilently()
    localStorage.setItem('id_token', tok);
    $rootScope.isAutenticated = true;
    localStorage.setItem('profile', user_profile);
    $rootScope.user = json_user_profile

    if($rootScope.user.roles && $rootScope.user.roles.indexOf('agente') == 0)
      window.location = window.location.origin + '/#/contatti'
    else if($rootScope.user.roles && $rootScope.user.roles.indexOf('prodotti') == 0)
      window.location = window.location.origin + '/#/prodotti' // $location.path('/prodotti');
    else
      window.location = window.location.origin + '/#/home'

  /* $rootScope.lock.on('authenticated', function (response) {
    $rootScope.lock.hide()
    $rootScope.lock.getUserInfo(response.accessToken, function (err, profile) {
      if (err) {
        console.log('errore login', err)
      } else {
        const namespace = "https://brianzacque-backend-it/"; // See Auth0 rules
        profile.roles = profile[namespace + "app_metadata"].roles
        profile.tenant = profile[namespace + "app_metadata"].tenant
        profile.name = profile[namespace + "user_metadata"].name
        profile.app_metadata = profile[namespace + "app_metadata"]
        profile.user_metadata = profile[namespace + "user_metadata"]
        profile.expires = 36000 * 1000 + new Date().getTime();
        var user_profile = JSON.stringify(profile),
          json_user_profile = JSON.parse(user_profile),
          roles = json_user_profile.roles;
        localStorage.setItem('id_token', response.idToken);
        $rootScope.isAutenticated = true;
        localStorage.setItem('profile', user_profile);
        $rootScope.user = json_user_profile

        if($rootScope.user.roles && $rootScope.user.roles.indexOf('agente') == 0)
          $location.path('/contatti');
        else if($rootScope.user.roles && $rootScope.user.roles.indexOf('prodotti') == 0)
          $location.path('/prodotti');
        else
          $location.path('/home');

        //Salva il log in DB
        $rootScope.dwhLog("Login");
      }
    }) */
  })
 
    // This events gets triggered on refresh or URL change
    $rootScope.$on('$locationChangeStart', function() {
        /* recupero l'utente e imposto alcune proprietÃ  utili sui permessi*/

        $rootScope.user = JSON.parse(localStorage.getItem('profile'))

        function checkExpiredToken (obj) {
          const dateNow = new Date().getTime();
          return dateNow <= obj.expires
        }

        $timeout(function (){
          $rootScope.user = JSON.parse(localStorage.getItem('profile'))
            if(!$rootScope.user || !checkExpiredToken($rootScope.user) ) {
              localStorage.removeItem('profile')
              $location.path('/login');
            }
        },500)


  $rootScope.admin = $rootScope.user && $rootScope.user.roles && $rootScope.user.roles.indexOf('admin') !== -1 ? true : false;
  $rootScope.adminProdotti = $rootScope.user && $rootScope.user.roles && $rootScope.user.roles.indexOf('prodotti') !== -1 ? true : false;
  $rootScope.operatore = $rootScope.user && $rootScope.user.roles && $rootScope.user.roles.indexOf('operatore') !== -1 ? true : false;
  $rootScope.operatoreEstro = $rootScope.user && $rootScope.user.roles && $rootScope.user.roles.indexOf('operatoreEstro') !== -1 ? true : false;
  $rootScope.agente = $rootScope.user && $rootScope.user.roles && $rootScope.user.roles.indexOf('agente') !== -1 ? true : false;
  $rootScope.commerciale = $rootScope.user && $rootScope.user.roles && $rootScope.user.roles.indexOf('commerciale') !== -1 ? true : false;


    /*
    in auth0 non viene censito nessun RUOLO.
    questo utente può visualizzare tutto ma non può modificare nulla
    */
  $rootScope.genericUser = !$rootScope.user || !$rootScope.user.roles ? true : false;
  })
 

  $rootScope.$on('$routeChangeSuccess', function (scope, current, pre) {
    if($location.path()=='/login'){
      $rootScope.login = true;
    }
    else
      $rootScope.login = false;
  });

  $rootScope.logout = function (){

    //Salva il log in DB
    var log = 
    DWHLOG.save(
    {
        user_id:    $rootScope.user.user_id,
        user_email: $rootScope.user.email,
        user_nome:  $rootScope.user.user_metadata.nome,
        path: "LogOut"
    },
    async function (response) {
      localStorage.removeItem('id_token');
      localStorage.removeItem('profile');
      $rootScope.login = true;
      $rootScope.isAutenticated = false;
      $rootScope.user = null;
      
      if (!$rootScope.auth0) {
        await configureClient()
      }
      $rootScope.auth0.logout({
        returnTo: window.location.origin
      });
      
      /* localStorage.removeItem('id_token');
      localStorage.removeItem('profile');
      $rootScope.login = true;
      $rootScope.isAutenticated = false;

      $rootScope.user = null;
      $location.path('/login') */
    },
    function (err) {
      console.log(err)
    })

  }

  // autocomplete menu 
  $rootScope.cercaScheda = function (chiave) {
      return PRODOTTO.searchCodice({
          nome: chiave,
          limit:10
      }).$promise;
  } 

  $rootScope.sceltaScheda = function(value)
  {
    SCHEDATECNICA.schedaProdotto({
          id: value._id
      }, function (data) { 
         window.open(data.url, '_blank')
      });
  }

  $rootScope.isActive  = function (check) {
    if ($location.path()==check)
      return true; 
  }

  $rootScope.abilitaVoceMenu = function (root) {
    if (Array.isArray(root)) {
      for (var i in root) {
        if (_abilitaVoceMenu(root[i]))
          return true
      }      
      return false
    } else return _abilitaVoceMenu(root)
  }

  var _abilitaVoceMenu = function (root) {
    /*RUOLO DI OPERATORE ESTRO*/
    if ($rootScope.user && $rootScope.user.roles && $rootScope.user.roles.indexOf('operatoreEstro') !== -1)
    {
      if (root=='home'          ||
          root=='divider1'      ||
          root=='divider4'      ||
          root=='divider7'      ||
          root=='etichette'     ||
          root=='etichetteEstro'||
          root=='cercaScheda'   ||
          root=='link') 
        return true; 
    }
    /*RUOLO DI OPERATORE*/
    if ($rootScope.user && $rootScope.user.roles && $rootScope.user.roles.indexOf('operatore') !== -1)
    {
      if (root=='home'          ||
          root=='divider1'      ||
          root=='divider4'      ||
          root=='divider7'      ||
          root=='etichette'     ||
          root=='etichetteIlmas'||
          root=='cercaScheda'   ||
          root=='link') 
        return true; 
    }
    /*RUOLO DI ADMIN*/
    else if ($rootScope.user && $rootScope.user.roles && $rootScope.user.roles.indexOf('admin') !== -1)
    { 
      if (
          root=='home'          ||
          root=='componenti'    ||
          root=='distintaBase'  ||
          root=='codiciEan'     ||
          root=='prodotti'      || 
          root=='prodottiEstro' || 
          root=='modelli'       || 
          root=='modelliEstro'  || 
          root=='categorie'     ||  
          root=='categorieEstro'||  
          root=='immagini'      ||
          root=='lineeCommerciali' ||
          root=='schedaTecnicaEstro' ||
          root=='etichetteEstro'     ||
          root=='etichetteilmas'     ||
          root=='agenti'        ||
          root=='eventi'        ||
          root=='installazioni'   ||
          root=='news'          ||          
          root=='contatti'      ||
          root=='mailingList'   ||
          root=='offerte'       || 
          root=='settings'      ||
          root=='etichette'     ||
          root=='report'        ||
          root=='cercaScheda'   ||
          root=='divider1'      ||
          root=='divider2'      ||
          root=='divider3'      ||
          root=='divider4'      ||
          root=='divider5'      ||
          root=='divider6'      ||
          root=='divider7'      ||
          root=='divider8'      ||
          root=='divider9'      ||
          root=='link'          || 
          root=='attivita'      || 
          root=='dwhLogs'       ||
          root=='colori'       ||
          root=='configurazione'
          )
        return true;
    }
    /*RUOLO DI ADMINPRODOTTI*/
    else if ($rootScope.user && $rootScope.user.roles && $rootScope.user.roles.indexOf('prodotti') !== -1)
    { 
      if (
          root=='home'          ||
          root=='componenti'    ||
          root=='distintaBase'  ||
          root=='codiciEan'     ||
          root=='prodotti'      || 
          root=='prodottiEstro' || 
          root=='categorie'     ||
          root=='categorieEstro'||
          root=='modelli'       || 
          root=='modelliEstro'  || 
          root=='immagini'      ||
          root=='lineeCommerciali' ||
          root=='schedaTecnicaEstro' ||
          root=='etichette'     ||
          root=='etichetteIlmas'     ||
          root=='etichetteEstro'     ||
          root=='agenti'        ||
          root=='eventi'        ||
          root=='installazioni'   ||
          root=='news'          ||
          root=='contatti'      ||
          root=='mailingList'   ||
          root=='offerte'       || 
          root=='settings'      ||
          root=='cercaScheda'   ||
          root=='divider1'      ||
          root=='divider2'      ||
          root=='divider3'      ||
          root=='divider4'      ||
          root=='divider5'      ||
          root=='divider6'      ||
          root=='divider7'      ||
          root=='divider8'      ||
          root=='divider9'      ||
          root=='link'          ||
          root=='colori'          ||
          root=='configurazione')
        return true;
    }
    /*RUOLO DI AGENTE*/
    else if ($rootScope.user && $rootScope.user.roles && $rootScope.user.roles.indexOf('agente') !== -1)
    { 
      if ( 
          root=='contatti'      ||
          root=='divider6'      ||
          root=='divider7'      ||
          root=='offerte'
          )
        return true;
    } 
     /*RUOLO DI COMMERCIALE*/
    else if ($rootScope.user && $rootScope.user.roles && $rootScope.user.roles.indexOf('commerciale') !== -1)
    { 
      if ( 
          root=='home'          ||
          root=='componenti'    ||
          root=='distintaBase'  ||
          root=='codiciEan'     ||
          root=='prodotti'      || 
          root=='prodottiEstro' || 
          root=='categorie'     ||
          root=='categorieEstro'||
          root=='schedaTecnicaEstro' ||
          root=='modelli'       || 
          root=='modelliEstro'  || 
          root=='lineeCommerciali' ||
          root=='agenti'        ||
          root=='eventi'        ||
          root=='installazioni' ||
          root=='news'          ||
          root=='contatti'      ||
          root=='mailingList'   ||
          root=='offerte'       || 
          root=='settings'      ||
          root=='cercaScheda'   ||
          root=='divider1'      ||
          root=='divider2'      ||
          root=='divider3'      ||
          root=='divider4'      ||
          root=='divider6'      ||
          root=='divider7'      ||
          root=='divider8'      ||
          root=='link'
          )
        return true;
    } 
    /*RUOLO DI GENERIC USER*/
    else if (!$rootScope.user || !$rootScope.user.roles)
    { 
      if (
          root=='home'          ||
          root=='componenti'    ||
          root=='distintaBase'  ||
          root=='codiciEan'     ||
          root=='prodotti'      ||
          root=='prodottiEstro' || 
          root=='modelli'       || 
          root=='modelliEstro'  || 
          root=='categorie'     ||
          root=='categorieEstro'||
          root=='lineeCommerciali' ||
          root=='agenti'        ||
          root=='eventi'        ||
          root=='installazioni' ||
          root=='news'          ||
          root=='contatti'      ||
          root=='offerte'       ||
          root=='etichette'     ||
          root=='etichetteIlmas'     ||
          root=='etichetteEstro'     ||
          root=='cercaScheda'   ||
          root=='divider1'      ||
          root=='divider2'      ||
          root=='divider3'      ||
          root=='divider4'      ||
          root=='divider5'      ||
          root=='divider6'      ||
          root=='divider7'      ||
          root=='divider8'      ||
          root=='divider9'      ||
          root=='link'
          )
        return true;  
      }
  }

  $rootScope.elencoAnni = function ()
  { 
    var elencoAnni = [];
    for (var i = CONFIG.annoStartProject; i <= new Date().getFullYear(); i++) {
      elencoAnni.push(i);
    }
    return  elencoAnni;
  }

  $rootScope.elencoLes = function ()
  {
    return CONFIG.elenchi.elencoLes;
  }
 
  $rootScope.elencoMadeIn = function ()
  {
    return CONFIG.elenchi.made_in;
  }

  $rootScope.elencoLed = function ()
  {
    return CONFIG.elenchi.elencoLed;
  }

  $rootScope.elencoIrc = function ()
  {
    return CONFIG.elenchi.elencoIrc;
  }


  $rootScope.generazioni = function ()
  {
    return CONFIG.elenchi.generazioni;
  }
  $rootScope.paramTest = true
  $rootScope.testBottone = function ()
  {
    if ($rootScope.paramTest)
      $rootScope.classeSideBar = "active"
    else
      $rootScope.classeSideBar = "disabled"

    $rootScope.paramTest = !$rootScope.paramTest
    
  }

  $rootScope.dwhLog = function (path)
  { 
    if ($rootScope.user){
      var log = {
          user_id:    $rootScope.user.user_id,
          user_email: $rootScope.user.email,
          user_nome:  $rootScope.user.user_metadata.nome ,
          path: path
      }
      DWHLOG.save(log);
    }
  }

  _env = function ()
  {
    return UTILS.environment({}, 
      function (data) { 
        return data
      });
  }

  $rootScope.env = _env();

})
.config(function ($modalProvider) {
  angular.extend($modalProvider.defaults, {
    animation: 'am-fade-and-scale'
  });
})
.config(function (cloudinaryProvider) {
  cloudinaryProvider.config({
    upload_endpoint: 'https://api.cloudinary.com/v1_1/',
    cloud_name: 'plurimedia'
  });
})
.config(function($datepickerProvider) {
  angular.extend($datepickerProvider.defaults, {
    dateFormat: 'dd/MM/yyyy',
    startWeek: 1,
    templateUrl: 'html/templates/datepicker.tpl.html'
  });
})

