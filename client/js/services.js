app
/* cerca tag in tutti i contatti */
.factory('TAGS', function ($resource) {
    return $resource('/secure/contatti/tags/');
})
/* cerca comuni */
.factory('COMUNI', function ($resource) {
    return $resource('/secure/comune', {},{
        'cerca': {method: 'GET', isArray:true, url: '/secure/comune/search'}
    });
})
/* cerca nazioni */
.factory('NAZIONI', function ($resource) {
    return $resource('/secure/nazioni');
})
/* impostazioni  */
.factory('SETTINGS', function ($resource) {
    return $resource('/secure/settings/:id', {id:'@id'});
})
/* CRUD contatto  */
.factory('CONTATTO', function ($resource) {
    return $resource('/secure/contatto/:id', {id:'@id'},{
        'update': {method: 'PUT'}, // il put va messo non c'è di default
        'ultimi': {method: 'GET', isArray:true, url: '/secure/contatti/last'},
        'cerca': {method: 'GET', isArray:true, url: '/secure/contatti/search'},
        'cercaemail': {method: 'GET', isArray:true, url: '/secure/contatti/searchemail'},
        'aziende': {method: 'GET', isArray:true, url: '/secure/contatti/aziende'},
        'doppi': {method: 'GET',   url: '/secure/contatti/doppi'},
        'nazioni': {method: 'GET',  isArray:true,  url: '/secure/contatti/nazioni'},
        'province': {method: 'GET',  isArray:true,  url: '/secure/contatti/province'},
        'count': {method: 'GET',  url: '/secure/contatti/count'}
     });
})
/* CRUD prodotto  */
.factory('PRODOTTO', function ($resource) {
    return $resource('/secure/prodotto/:id', {id:'@id'},
    {
        'update': {method: 'PUT'},
        'ultimi': {method: 'GET', isArray:true, url: '/secure/prodotti/last/:generazione/'},
        'cerca': {method: 'GET', isArray:true, url: '/prodotti/search'},
        'profili': {method: 'GET', isArray:true, url: '/prodotti/profestro'},
        'fonti': {method: 'GET', isArray:true, url: '/prodotti/fontestro'},
        'searchCodice': {method: 'GET', isArray:true, url: '/secure/prodotti/search/codice'},
        'codiceValido': {method: 'GET', isArray:true, url: '/secure/prodotti/search/codiceValido'},
        'codice': {method: 'GET', url: '/secure/prodotto/codice/:codice'},
        'updateimage': {method: 'PUT',url: '/secure/prodotto/image/update'},
        'moduli': {method: 'GET', isArray:true, url: '/secure/prodottomoduli'}, // recupero i moduli di un prodotto per modello
        'colori': {method: 'GET', isArray:true, url: '/secure/prodottocolori'}, // recupero i colori di un modello
        'fasci': {method: 'GET', isArray:true, url: '/secure/prodottofasci'}, // recupero i fasci di un modello
        'updateLdt':{method: 'PUT',url: '/secure/prodotto/ldt/update'}, // aggiorna in blocco tutti i codici simili con lo stesso file ldt
        'accessori': {method: 'GET', isArray:true, url: '/secure/prodotto/accessori/:id'},
        'getAll': {method: 'GET', isArray:true, url: '/secure/prodotti/getAll'},
        'csvmetel': {method: 'GET',  url: '/secure/prodotti/csvmetel/:anno'},
        'csvDistintaBase': {method: 'GET',  url: '/prodotti/csvDistintaBase'},
        'excelProdottiKite': {method: 'GET',  url: '/prodotti/xlsProdottiKite'},
        'prodottiinmodello': {method: 'GET', isArray:true, url: '/secure/prodotti/prodottiinmodello/:id'},
        'updateCategoria':{method: 'PUT',url: '/secure/prodotto/categoria/update'}, // aggiorna in blocco tutti i codici di un modello
        'count': {method: 'GET',  url: '/secure/prodotti/count'},
        'codiciEan': {method: 'GET', isArray:true, url: '/secure/prodotti/codiciean'},
        'getEanByidCodice': {method: 'GET', isArray:false, url: '/secure/prodotti/getEanByidCodice'},
        'countCodEan': {method: 'GET',  url: '/secure/prodotti/countean'}
    });
})
/* CRUD agenti  */
.factory('AGENTE', function ($resource) {
    return $resource('/secure/agente/:id', {id:'@id'},
    {
        'last'  : {method: 'GET', isArray:true, url: '/secure/agente/last'},
        'update': {method: 'PUT'},
        'save': {method: 'POST'},
        'cerca' : {method: 'GET', isArray:true, url: '/secure/agente/cerca'}, 
     });
})
/* CRUD eventi  */
.factory('EVENTO', function ($resource) {
    return $resource('/secure/evento/:id', {id:'@id'},
    {
        'last'  : {method: 'GET', isArray:true, url: '/secure/evento/last'},
        'update': {method: 'PUT'},
        'save': {method: 'POST'},
        'cerca' : {method: 'GET', isArray:true, url: '/secure/evento/cerca'}, 
     });
})
/* CRUD Modello  */
.factory('MODELLO', function ($resource) {
    return $resource('/secure/modello/:id', {id:'@id'},{
        'list': {method: 'GET', isArray:true, url: '/secure/modelli'},
        'last': {method: 'GET', isArray:true, url: '/secure/modelli/last'},
        'update': {method: 'PUT'},
        'cerca': {method: 'GET', isArray:true, url: 'secure/modelli/cerca'},
        'count': {method: 'GET',  url: '/secure/modelli/count'},
        'distinta': {method: 'GET',  url: '/secure/modello/:id/distinta'}
    });
})

/* CRUD Categoria  */
.factory('CATEGORIA', function ($resource) {
    return $resource('/secure/categoria/:id', {id:'@id'},
    {
        'list': {method: 'GET', isArray:true, url: '/secure/categorie'}, 
        'last': {method: 'GET', isArray:true, url: '/secure/categorie/last'}, 
        'count': {method: 'GET',  url: '/secure/categorie/count'},
        'update': {method: 'PUT'},
        'cerca': {method: 'GET', isArray:true, url: '/secure/categorie/cerca'},
    });
})
/* cerca sottocategorie in tutti i prodotti */
.factory('SOTTOCATEGORIE', function ($resource) {
    return $resource('/secure/prodotti/sottocategorie/');
}) 
/* import csv gmail */
.factory('CSVGMAIL', function (ngNotify,Upload,CONFIG,$rootScope) {

    // servizio che riceve un file csv lo carica e attende che venga processato lato server
    var CSVGMAIL = {};

    CSVGMAIL.upload = function (file,callback) {

        if(file && file.$error)
        {
            ngNotify.set(CONFIG.messages.wrongcsv, 'warn');
        }

        if(file && !file.$error)
        {
            file.upload = Upload.upload({
                url: '/secure/import/gmail',
                file: file,
                method: 'POST',
                headers: {'Content-Type': file.type}
            });

            file.upload.then(function (response) {
                if(response.status === 200) {
                    ngNotify.set(CONFIG.messages.import_success, 'success');
                    callback(response.data);
                }
            });

            file.upload.progress(function (evt) {
               /* TODO mostrare progress */
            });

        }

    };
      return CSVGMAIL;
})
/* Image upload su cloudinary */
.factory('IMAGEUPLOAD', function (ngNotify,Upload,$http,CONFIG) {

    return function (files,callback) {

        // cambia il filename con quello messo in input
        _.map(files, function (f) {
            Upload.rename(f, f.nome+'|'+f.tipo); // mando nome pipe tipo
        })

        files.upload = Upload.upload({
            url: '/secure/image/upload',
            file: files,
            method: 'POST',
            headers: {'Content-Type': 'image/jpeg'}
        });

        files.upload.then(function (response) {
            if(response.status === 200) {
                ngNotify.set(CONFIG.messages.import_success, 'success');
                console.log("response service ", response)
                callback(response.data);
            }
        });

    };
})

.factory('IMAGEUPLOADEVENTO', function (ngNotify,Upload,$http,CONFIG) {

    return function (files,callback) {

        // cambia il filename con quello messo in input
        _.map(files, function (f) {
            Upload.rename(f, f.nome+'|'+f.tipo); // mando nome pipe tipo
        })

        files.upload = Upload.upload({
            url: '/secure/imageevento/upload',
            file: files,
            method: 'POST',
            headers: {'Content-Type': 'image/jpeg'}
        });

        files.upload.then(function (response) {
            if(response.status === 200) {
                ngNotify.set(CONFIG.messages.import_success, 'success');
                callback(response.data);
            }
        });

    };
})
/* CRUD immagine  */
.factory('IMAGE', function ($resource) {
    return $resource('/secure/image/:id', {id: '@id'},{
        'update': {method: 'PUT'},
        'ultime': {method: 'GET', isArray:true, url: '/secure/image/last'},
        'cerca': {method: 'POST',isArray:true, url: '/secure/image/search'},
        'ultimetipo': {method: 'GET', isArray:true, url: '/secure/image/last/:tipo'},
        'batchupdate':  {method:'POST', url: '/secure/image/batchupdate'},
    });
})
/* PDF  */
.factory('PDF', function ($resource) {
    return $resource('/secure/pdf/:lingua',{lingua: '@lingua'});
})
/* Mail  */
.factory('EMAIL', function ($resource) {
    return $resource('/secure/email/:id',{id: '@id'},{
        'send': {method: 'POST', isArray:true},  // madrill torna un array...
        'stato': {method: 'GET'}
    });
})
/* import csv gmail */
.factory('CSVPRODOTTI', function (ngNotify,Upload,CONFIG,$rootScope) {

    // servizio che riceve un file csv lo carica e attende che venga processato lato server
    var CSVPRODOTTI = {};

    CSVPRODOTTI.upload = function (file,callback) {

        if(file && file.$error)
        {
            ngNotify.set(CONFIG.messages.wrongcsv, 'warn');
        }

        if(file && !file.$error)
        {
            file.upload = Upload.upload({
                url: '/secure/import/prodotti/'+$rootScope.tipoimport,
                file: file,
                method: 'POST',
                headers: {'Content-Type': file.type}
            });

            file.upload.then(function (response) {
                if(response.status === 200) {
                    ngNotify.set(CONFIG.messages.import_success, 'success');
                    callback(response.data);
                }
            });

            file.upload.progress(function (evt) {
               /* TODO mostrare progress */
            });

        }

    };
      return CSVPRODOTTI;
})
/* CRUD componenti  */
.factory('COMPONENTE', function ($resource) 
{
    return $resource('/secure/componente/:id', {id:'@id'},{
        'update': {method: 'PUT'},
        'ultimi': {method: 'GET', isArray:true, url: '/secure/componenti/last'},
        'cerca': {method: 'GET', isArray:true, url: '/secure/componenti/search'},
        'count': {method: 'GET',  url: '/secure/componenti/count'}
     });
})
/* CRUD Linee Commerciali  */
.factory('LINEACOMMERCIALE', function ($resource) 
{
    return $resource('/secure/lineaCommerciale/:id', {id:'@id'},{
        'update': {method: 'PUT'},
        'last': {method: 'GET', isArray:true, url: '/secure/lineeCommerciali/last'},
        'cerca': {method: 'GET', isArray:true, url: '/secure/lineeCommerciali/search'},
        'cercaProdotto': {method: 'GET', isArray:true, url: '/secure/lineeCommerciali/cercaProdotto/:idLinea/:codiceProdotto'},
        'creaAssociazione':  {method:'POST', url: '/secure/lineaCommerciale/creaAssociazione'},
        'creaAssociazioneMassiva':  {method:'POST', url: '/secure/lineaCommerciale/creaAssociazioneMassiva'},
        'eliminaAssociazione':  {method:'POST', url: '/secure/lineaCommerciale/eliminaAssociazione'},
        'getAll': {method: 'GET', isArray:true, url: '/secure/lineeCommerciali/getAll'}
     });
}) 
/* CRUD DWHETICHETTA  */
.factory('DWHETICHETTA', function ($resource) 
{
    return $resource('/secure/dwhEtichetta/:id', {id:'@id'},
    {
         'etichetteBianche': {method: 'GET', isArray:true, url: '/secure/dwhEtichetta/etichetteBianche/:anno'} ,
         'etichetteGrigie': {method: 'GET', isArray:true, url: '/secure/dwhEtichetta/etichetteGrigie/:anno'} ,
         'etichetteEstro': {method: 'GET', isArray:true, url: '/secure/dwhEtichetta/etichetteEstro/:anno'} ,
         'mesi': {method: 'GET', isArray:true, url: '/secure/dwhEtichetta/mesi/:anno'},
         'etichetteGrigieInMesi': {method: 'GET', isArray:true, url: '/secure/dwhEtichetta/etichetteGrigieInMesi/:anno'},
         'etichetteBiancheInMesi': {method: 'GET', isArray:true, url: '/secure/dwhEtichetta/etichetteBiancheInMesi/:anno'},
         'etichetteEstroInMesi': {method: 'GET', isArray:true, url: '/secure/dwhEtichetta/etichetteEstroInMesi/:anno'},
         'count': {method: 'GET',  url: '/secure/dwhEtichetta/count'}
    });
})
/* CRUD offerta  */
.factory('OFFERTA', function ($resource) {
    return $resource('/secure/offerta/:id', {id:'@id'},{
        'update': {method: 'PUT'}, // il put va messo non c'è di default
        'ultime': {method: 'GET', isArray:true, url: '/secure/offerte/last'},
        'cerca': {method: 'GET', isArray:true, url: '/secure/offerte/search'},
        'count': {method: 'GET',  url: '/secure/offerte/count'}
    });
})
/* CRUD MAILING LIST  */
.factory('MAILINGLIST', function ($resource)
{
    return $resource('/secure/mailingList/:id', {id:'@id'},{
        'last': {method: 'GET', isArray:true, url: '/secure/mailingList/last'},
        'update': {method: 'PUT'},
        'search': {method: 'GET', isArray:true, url: 'secure/mailingList/search'},
    }); 
})
.factory('PING', function ($resource) {
    return $resource('/prodotti/ping');
})
.factory('DWHLOG', function ($resource)
{
    return $resource('/secure/dwhlog/:id', {id:'@id'},{
        'last': {method: 'GET', isArray:true, url: '/secure/dwhlog/last'}
    }); 
})
.factory('SCHEDATECNICA', function ($resource) {
    return $resource(
      '/secure/schedatecnica/:id', {
        id: '@_id'
    }, 
    {
    'schedaProdotto': {method: 'GET', url: '/secure/prodotto/:id/schedatecnica/it/'},
    });
})
.factory('DWHRICERCA', function ($resource) 
{
    return $resource('/secure/dwhRicerca/:id', {id:'@id'},
    {
         'dwhRicerche': {method: 'GET', url: '/secure/dwhRicerche/:anno'} ,
    });
})
.factory('DWHSCHEDATECNICA', function ($resource) 
{
    return $resource('/secure/dwhSchedeTecniche/:id', {id:'@id'},
    {
         'dwhSchedeTecniche': {method: 'GET', url: '/secure/dwhSchedeTecniche/:anno'} ,
    });
})
.factory('DWHSCHEDADETTAGLIO', function ($resource) 
{
    return $resource('/secure/dwhSchedeDettaglio/:id', {id:'@id'},
    {
         'dwhSchedeDettaglio': {method: 'GET', url: '/secure/dwhSchedeDettaglio/:anno'} ,
    });
})
.factory('UTILS', function ($resource) {
      
    
 //  return 'environment': {method: 'GET', isArray:false, url: '/utils/environment'},
    
    return $resource('/utils/environment', {},{
        'environment': {method: 'GET',  url: 'utils/environment'}
    });


})
.factory('DASHBOARD', function ($http) {
    /*return $resource('/secure/dashboard/:id', {id:'@id'}, {
        'get' : {method: 'GET',isArray:false}
    });*/
        return {
            get: function () { return $http.get('/secure/dashboard/'); },
            save: function (obj) { return $http.post('/secure/dashboard/', obj);}
        }
})
.factory('ATTIVITA', function ($resource) {
    return $resource('/secure/attivita/:id', {id:'@id'},
    {
        'list': {method: 'GET', isArray:true, url: '/secure/attivita'}, 
        'last': {method: 'GET', isArray:true, url: '/secure/attivita/last'}, 
        'update': {method: 'PUT'},
        'cerca': {method: 'GET', isArray:true, url: '/attivita/search'},
        'count': {method: 'GET',  url: '/secure/attivita/count'}
     });
})
.factory('CONFIGTIPOLOGIEASSOCIAZIONIDS', function ($resource) {
    return $resource('/secure/configTipologieAssociazioniDS/:id', {id:'@id'},
    {
        'list': {method: 'GET', isArray:true, url: '/secure/configTipologieAssociazioniDS'}, 
        'update': {method: 'PUT'}
     });
})
.factory('COLORI', function ($resource) {
    return $resource('/secure/colori/:id', {id:'@id'},
    {
        'codice': {method: 'GET', url: '/secure/colore/codice/:codice'}, 
        'list': {method: 'GET', isArray:true, url: '/secure/colori'}, 
        'cerca': {method: 'GET', isArray:true, url: '/colori/search'},
        'finiture': {method: 'GET', isArray:true, url: '/colori/finiestro'},
        'update': {method: 'PUT'}
     });
})
.factory('STATOCREAZIONEDISTINTE', function ($resource) {
    return $resource('/secure/creazionedistinte/last', {},{
        'last': {method: 'GET',  url: 'secure/creazionedistinte/last'},
        //'create': {method: 'GET',  url: 'secure/creazionedistinte/create'},
        //'close': {method: 'POST',  url: 'secure/creazionedistinte/close'}
    });
})
/* CRUD installazione  */
.factory('INSTALLAZIONE', function ($resource) {
    return $resource('/secure/installazione/:id', {id:'@id'},{
        'update': {method: 'PUT'}, // il put va messo non c'è di default
        'last': {method: 'GET', isArray:true, url: '/secure/installazione/last'},
        'cerca': {method: 'GET', isArray:true, url: '/secure/installazione/search'},        
    });
})
.factory('IMAGEUPLOADINSTALLAZIONE', function (ngNotify,Upload,$http,CONFIG) {

    return function (files,callback) {

        // cambia il filename con quello messo in input
        _.map(files, function (f) {
            Upload.rename(f, f.nome+'|'+f.tipo); // mando nome pipe tipo
        })

        files.upload = Upload.upload({
            url: '/secure/imageinstallazione/upload',
            file: files,
            method: 'POST',
            headers: {'Content-Type': 'image/jpeg'}
        });

        files.upload.then(function (response) {
            if(response.status === 200) {
                ngNotify.set(CONFIG.messages.import_success, 'success');
                callback(response.data);
            }
        });

    };
})
/* CRUD installazione  */
.factory('NEWS', function ($resource) {
    return $resource('/secure/news/:id', {id:'@id'},{
        'update': {method: 'PUT'}, // il put va messo non c'è di default
        'last': {method: 'GET', isArray:true, url: '/secure/news/last'},
        'cerca': {method: 'GET', isArray:true, url: '/secure/news/search'}
    });
})
