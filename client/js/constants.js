app.constant('CONFIG', {
    appName : 'Ilmas',
    messages: { 
        wrongcsv: 'Formato non valido, serve un file con estensione .csv',
        import_success: 'Import avvenuto con successo!',
        wrongimage: 'Formato non valido, serve un file con estensione .jpg o png',
        alert_delete_confirm: 'Ok elimina!',
        alert_delete_undo: 'Annulla',
        search_min_length: 'Inserisci almeno 3 caratteri per effettuare la ricerca',
        search_min2_length: 'Inserisci almeno 2 caratteri per effettuare la ricerca',
        image_updated: 'Immagine modificata con successo',
        image_update_error: 'Errore durante la modifica dell\'immagine, riprova',
        image_deleted: 'Immagine eliminata con successo',
        image_delete_error: 'Errore durante la cancellazione dell\'immagine, riprova',
        image_import_success: 'Immagini caricate con successo',
        image_delete_alert_title: 'Sicuro di voler eliminare questa foto?',
        image_delete_alert_message: 'Potrebbero esserci prodotti, modelli e categorie con questa immagine. Dopo l\'eliminazione potrai scaricare un file con gli elementi che l\'hanno persa.',
        image_updated_images: 'Immagini aggiornate con sucesso',
        image_update_images_error: 'Errore durante la modifica delle immagini, riprova',
        nocontacts: 'Non ci sono contatti, aggiungine uno!', 
        contatto_saved: 'Contatto inserito con successo',
        contatto_save_error: 'Errore durante il salvataggio del contatto, riprova',
        contatto_delete_alert_title: 'Sicuro di voler eliminare questo contatto?',
        contatto_delete_alert_message: 'Una volta cancellato non sarà più recuperabile',
        contatto_deleted: 'Contatto eliminato',
        contatto_delete_error: 'Errore durante la cancellazione del contatto, riprova',
        contatto_updated: 'Contatto aggiornato',
        contatto_update_error: 'Errore durante la modifica del contatto, riprova',
        prodotto_saved: 'Prodotto inserito con successo',
        prodotto_save_error: 'Errore durante il salvataggio del prodotto, riprova',
        prodotto_delete_alert_title: 'Sicuro di voler eliminare questo prodotto?',
        prodotto_delete_alert_message: 'Una volta cancellato non sarà più recuperabile',
        prodotto_deleted: 'Prodotto eliminato',
        prodotto_delete_error: 'Errore durante la cancellazione del prodotto, riprova',
        prodotto_updated: 'Prodotto aggiornato',
        prodotto_update_error: 'Errore durante la modifica del prodotto, riprova',
        modello_saved: 'Modello inserito con successo',
        modello_save_error: 'Errore durante il salvataggio del modello, riprova',
        modello_updated: 'Modello aggiornato',
        modello_update_error: 'Errore durante la modifica del modello, riprova',
        categoria_updated: 'Categoria aggiornata',
        categoria_update_error: 'Errore durante la modifica della categoria, riprova',
        categoria_saved: 'Categoria inserita con successo',
        categoria_save_error: 'Errore durante il salvataggio della categoria, riprova',
        colore_updated: 'Colore aggiornato',
        colore_update_error: 'Errore durante la modifica dell colore, riprova',
        colore_saved: 'Colore inserito con successo',
        colore_save_error: 'Errore durante il salvataggio del colore, riprova',
        taglio_updated: 'Taglio aggiornato',
        taglio_update_error: 'Errore durante la modifica dell taglio, riprova',
        taglio_saved: 'Taglo inserito con successo',
        taglio_save_error: 'Errore durante il salvataggio del taglio, riprova',
        agente_updated: 'Agente aggiornato',
        agente_update_error: 'Errore durante la modifica, riprova',
        agente_saved: 'Agente inserito con successo',
        agente_save_error: 'Errore durante il salvataggio, riprova',
        agente_delete_alert_title: 'Sicuro di voler eliminare questo agente?',
        agente_delete_alert_message: 'Una volta cancellato non sarà più recuperabile',
        
        evento_updated: 'Evento aggiornato',
        evento_update_error: 'Errore durante la modifica, riprova',
        evento_saved: 'Evento inserito con successo',
        evento_save_error: 'Errore durante il salvataggio, riprova',
        evento_delete_alert_title: 'Sicuro di voler eliminare questo evento?',
        evento_delete_alert_message: 'Una volta cancellato non sarà più recuperabile',

        installazioni_updated: 'Installazione aggiornato',
        installazioni_update_error: 'Errore durante la modifica, riprova',
        installazioni_saved: 'Installazione inserito con successo',
        installazioni_save_error: 'Errore durante il salvataggio, riprova',
        installazioni_delete_alert_title: 'Sicuro di voler eliminare questa installazione?',
        installazioni_delete_alert_message: 'Una volta cancellato non sarà più recuperabile',
        installazioni_deleted: 'Installazione eliminata con successo',
        installazioni_delete_error: 'Errore eliminazione installazione',

        news_updated: 'News aggiornata',
        news_update_error: 'Errore durante la modifica, riprova',
        news_saved: 'News inserita con successo',
        news_save_error: 'Errore durante il salvataggio, riprova',
        news_delete_alert_title: 'Sicuro di voler eliminare questa news?',
        news_delete_alert_message: 'Una volta cancellata non sarà più recuperabile',
        news_deleted: 'News eliminata con successo',
        news_delete_error: 'Errore eliminazione news',

        offerta_save_error: 'Errore durante il salvataggio dell\' offerta, riprova',
        offerta_descrizione_prodotto_save: 'Descrizione del prodotto in offerta modificata',
        offerta_delete_alert_title: 'Sicuro di voler eliminare quest\' offerta?',
        offerta_delete_alert_message: 'Una volta cancellata non sarà più recuperabile',
        offerta_deleted: 'Offerta eliminata',
        offerta_delete_error: 'Errore durante la cancellazione dell\' offerta, riprova',
        email_error: 'Errore durante l\'invio della mail, riprova',
        email_offerta_sent: 'Mail spedita',
        email_upload_max_size: 'Massimo 15MB per file',
        settings_save: 'Impostazioni salvate',
        settings_save_error: 'Errore durante il salvataggio delle impostazioni, riprova',

        categoria_delete_alert_title: 'Sicuro di voler eliminare questa categoria?',
        categoria_delete_alert_message: 'Una volta cancellata non sarà più recuperabile',
        categoria_deleted: 'Categoria eliminata', 
        categoria_delete_error: 'Errore durante la cancellazione della categoria, riprova',
        modelliIncategoria_delete_error: 'Impossibile eliminare questa categoria, spostare o eliminare i modelli collegati ad essa e poi riprovare.',

        modello_delete_alert_title: 'Sicuro di voler eliminare questo modello?',
        modello_delete_alert_message: 'Una volta cancellata non sarà più recuperabile',
        modello_deleted: 'Modello eliminata', 
        modello_delete_error: 'Errore durante la cancellazione del modello, riprova',
        prodottiInModello_delete_error: 'Impossibile eliminare questo modello, spostare o eliminare i prodotti collegati ad esso e poi riprovare.',

        //MSG DEI COMPONENTI
        componente_saved: 'Componente inserito con successo',
        componente_save_error: 'Errore durante il salvataggio del componente, riprova',
        componente_updated: 'Componente aggiornato',
        componente_update_error: 'Errore durante la modifica del componente, riprova',
        componente_delete_alert_title: 'Sicuro di voler eliminare questo componente?',
        componente_delete_alert_message: 'Una volta cancellato non sarà più recuperabile',
        componente_deleted: 'Componente eliminato',
        componente_delete_error: 'Errore durante la cancellazione del componente, riprova',

        //MSG DELLE LINEE COMMERCIALI
        lineaCommerciale_saved: 'Linea inserito con successo',
        lineaCommerciale_save_error: 'Errore durante il salvataggio della linea, riprova',
        lineaCommerciale_updated: 'Linea aggiornata con sucesso',
        lineaCommerciale_update_error: 'Errore durante la modifica della linea, riprova',
        lineaCommerciale_delete_alert_title: 'Sicuro di voler eliminare questa linea?',
        lineaCommerciale_delete_alert_message: 'Una volta cancellato non sarà più recuperabile',
        lineaCommerciale_deleted: 'Linea eliminata',
        lineaCommerciale_delete_error: 'Errore durante la cancellazione della linea, riprova',

        lineaCommercialeAss_delete_alert_title: 'Sicuro di voler eliminare quest\'associazione?',
        lineaCommercialeAss_delete_alert_message:'Una volta cancellata non sarà più recuperabile',
 
        session_error: 'Errore connessione rete. Se l\'errore persiste è necessario effettuare il login.',

        distinta_create_alert_annulla:    'Annulla',
        distinta_create_alert_confirm:    'Conferma',
        distinta_create_alert_title:    'Creazione Distinta Base',
        distinta_create_alert_message:  'Stai per lanciare la procedura di creazione della distinta base, proseguire?',
        distinta_create_success_message:'Operazione effettuata con successo. Ti avviseremo via mail quando il processo di creazione distinta base sarà terminato'
    },
    elenchi: {
        elencoLes: [
            "LES 13",
            "LES 15",
            "LES 17",
            "LES 22",
            "LES 23",
            "LES 19",
            "LES 15",
            "LES 10",
            "SAM 15",
            "SAM 14",
            "SAM 10"
        ],
        elencoLed: [
            "AR111",
            "Arled Osram",
            'Arled Cob Osram',
            "Diled Osram",
            "F050",
            "F111",
            "F112", 
            "SL/A",
            "Toshiba AR 111",
            "Toshiba GU10",
            "V0",
            "V1",
            "V1 Plus",
            "V2",
            "V3 Plus",
            "V6 Plus"
        ],
        generazioni: [
            "3HE",
            "D1",
            "D2",
            "D3",
            "G4",
            "G5",
            "G6",
            "G7",
            "IN39",
            "IN40",
            "T1",
            "V1",
            "V2"
        ],
        elencoIrc: [
            "0",
            "80",
            ">80",
            "90",
            ">90",
            "95",
            ">95",
            "97,6",
            "98",
            ">98",
            "VIVID"
        ],
        made_in: [
            "Italy",
            "EU",
            "Norway",
            "PRC",
            "Russian",
            "Taiwan",
            "Thailand",
            "Switzerland",
            "UK",
            "USA"
        ]
    },
    serieIlmas: 'aProdotti',
    serieEstro: 'aComponenti',
    estroClassificazioneBinari: {
        'O': { modello: 'OPAL', unita: 'm' },
        'T': { modello: 'MP', unita: 'm' },
        'P': { modello: 'POWER', unita: 'mod' },
        'A': { modello: 'COMFORT C1', unita: 'mod' },
        'B': { modello: 'COMFORT C2', unita: 'mod' },
        'C': { modello: 'COMFORT C3', unita: 'mod' },
        'E': { modello: 'EXCEL', unita: 'mod' },
        'R': { modello: 'REFLECTA', unita: 'm' },
        'S': { modello: 'SPOT', unita: 'mod' },
        'I': { modello: 'BLIND', unita: 'm' }
    },
    id_binari: //utilizzato per trovare i modelli di tipo binario che utilizzano una scheda tecnica adHoc
        [
            { _id: "5711f38e5f09a60300e387b0"},
            { _id: "5711f4135f09a60300e387b2"},
            { _id: "5711f4485f09a60300e387b5"},
            { _id: "5711f3f85f09a60300e387b1"},
            {_id: "59cbae665a6aad0400841465"},
            {_id: "59db30769b066b0400cd5fb4"},
            {_id: "59dc8b3439173e040027a22f"},
            {_id: "59db543239173e040027a161"},
            {_id: "59db52f839173e040027a159"},
            {_id: "59db52d139173e040027a158"},
            {_id: "59db4c2b39173e040027a148"},
            {_id: "59db4b4c39173e040027a141"},
            {_id: "59db3c8c9b066b0400cd6005"},
            {_id: "59ccc39650af6204004504a1"},
            {_id: "59ccb0b450af62040045046b"},
            {_id: "59ccaf8a50af62040045045e"},
            {_id: "57e4d90c3e3a3b030076c98a"}
        ],

    timeToPing: 600000, 
 
    annoStartProject: 2016
    
})


app.run(['$rootScope', 'CONFIG', function($rootScope, CONFIG) {
    // metto le costanti nel rootscope
    $rootScope.CONFIG = CONFIG;
}]);
