app.controller('modelli', [
  '$scope',
  '$modal',
  'ngNotify',
  'CONFIG',
  'MODELLO',
  'CATEGORIA',
  'PRODOTTO',
  'IMAGE',
  'PING',
  'COMPONENTE',
  '$interval',
  '$rootScope',
  '$route',
  '$http',
  'Upload',
  'CONFIGTIPOLOGIEASSOCIAZIONIDS',
  'STATOCREAZIONEDISTINTE',
  function ($scope, $modal, ngNotify, CONFIG, MODELLO, CATEGORIA, PRODOTTO, IMAGE, PING, COMPONENTE, $interval, $rootScope, $route, $http, Upload, CONFIGTIPOLOGIEASSOCIAZIONIDS, STATOCREAZIONEDISTINTE) {

    //Salva il log in DB
    $rootScope.dwhLog("Modelli");

    $scope.serie = $route.current.$$route.serie;
    $scope.isEstro = $scope.serie === CONFIG.serieEstro

    //effettua un ping verso il server, ogni x millisencondi, per capire se esiste la connessione
    var _ping = function () {
      PING.query(
        function (response) { },
        function (err) {
          ngNotify.set(CONFIG.messages.session_error, {
            type: 'error',
            sticky: true
          });
        });
    }
    $interval(_ping, CONFIG.timeToPing);
    $scope.elencoMadeIn = $rootScope.elencoMadeIn();

    $scope.modalModello = $scope.serie && $scope.serie === CONFIG.serieEstro ? $modal(
      {
        scope: $scope,
        title: 'Nuovo modello Estro',
        templateUrl: 'html/modals/modelloEstro.html',
        show: false,
        id: 'modal-categoria'
      })
      : $modal({
        scope: $scope,
        title: 'Nuovo modello',
        templateUrl: 'html/modals/modello.html',
        show: false,
        id: 'modal-modello',
        backdrop: 'static',
        keyboard: false
      });

    modalImmagineModello = $modal({
      scope: $scope,
      title: 'Nuova Linea',
      templateUrl: 'html/modals/modelloImmagine.html',
      show: false,
      id: 'modal-modello-immagine',
      backdrop: 'static',
      keyboard: false
    })


    //$scope.onlyNumbers = /^\d+$/;
    $scope.onlyNumbers = /^\d+(\.\d+)*$/;

    $scope.modelli = ($scope.serie && $scope.serie === CONFIG.serieEstro ? MODELLO.last({ serie: CONFIG.serieEstro }) : MODELLO.last());
    $scope.categorie = ($scope.serie && $scope.serie === CONFIG.serieEstro ? CATEGORIA.list({ serie: CONFIG.serieEstro }) : CATEGORIA.list());
    $scope.configTipologieAssociazioniDS = CONFIGTIPOLOGIEASSOCIAZIONIDS.list();

    if ($scope.serie && $scope.serie === CONFIG.serieEstro)
      $scope.tabs = [
        {
          title: "Info generali",
          template: '/html/includes/modello-tab-principaleEstro.html',
          activeTab: true
        },
        {
          title: "Dati tecnici",
          template: '/html/includes/modello-tab-info.html'
        },
        {
          title: "Accessori",
          template: '/html/includes/modello-tab-accessori.html'
        },
        {
          title: "Files",
          template: '/html/includes/modello-tab-filesEstro.html'
        },
        {
          title: "Contabilità",
          template: '/html/includes/modello-tab-contabilita.html'
        }

      ];
    else {
      $scope.tabs = [
        {
          title: "Info generali",
          template: '/html/includes/modello-tab-principale.html',
          activeTab: true
        },
        {
          title: "Dati tecnici",
          template: '/html/includes/modello-tab-info.html'
        },
        {
          title: "Accessori",
          template: '/html/includes/modello-tab-accessori.html'
        },
        {
          title: "Files",
          template: '/html/includes/modello-tab-files.html'
        },
        {
          title: "Contabilità",
          template: '/html/includes/modello-tab-contabilita.html'
        }
      ];

      if (!$rootScope.commerciale) $scope.tabs.push({ title: "Dati distinta", template: '/html/includes/modello-tab-distinta.html' });
    }
    var lastCheckedImportato = null;
    $scope.uncheck = function (event) {
      if (lastCheckedImportato && lastCheckedImportato === event.target.value) {
        lastCheckedImportato = null;
        $scope.modello.importato = null;
      } else lastCheckedImportato = event.target.value;
    }

    // salva modello
    $scope.salvaModello = function (modello) {
      if (!modello._id) {
        MODELLO.save(modello,
          function (response) {
            ngNotify.set(CONFIG.messages.modello_saved, 'success');
            $scope.modalModello.hide();
            $scope.resetSearch();
            $scope.modelli = ($scope.serie && $scope.serie === CONFIG.serieEstro ? MODELLO.last({ serie: CONFIG.serieEstro }) : MODELLO.last());
          },
          function (err) {
            ngNotify.set(CONFIG.messages.modello_save_error, 'error');
          });
      }
      else {

        MODELLO.update({
          id: modello._id,
          classe_energetica: modello.classe_energetica
        }, modello,
          function (response) {

            /*
            nel caso in cui viene modificata la categoria di questo modello
            bisogna aggiornare la categoria di tutti i prodotti che hanno questo come modello
            */
            if ($scope.modelloCategoriaOld._id !== modello.categoria._id) {
              console.log("modificare i prodotti");

              var data = {
                modelloId: modello._id,
                categoriaId: modello.categoria._id,
              };

              PRODOTTO.updateCategoria(
                data,
                function (response) {
                  ngNotify.set(CONFIG.messages.modello_updated, 'success');
                  $scope.modalModello.hide();
                  $scope.modelli = ($scope.serie && $scope.serie === CONFIG.serieEstro ? MODELLO.last({ serie: CONFIG.serieEstro }) : MODELLO.last());
                  $scope.resetSearch();
                },
                function (err) {
                  ngNotify.set(CONFIG.messages.modello_save_error, 'error');
                });
            }
            else {
              ngNotify.set(CONFIG.messages.modello_updated, 'success');
              $scope.modalModello.hide();
              $scope.modelli = ($scope.serie && $scope.serie === CONFIG.serieEstro ? MODELLO.last({ serie: CONFIG.serieEstro }) : MODELLO.last());
              $scope.resetSearch();
            }

          },
          function (err) {
            ngNotify.set(CONFIG.messages.modello_update_error, 'error');
          });
      }
    }

    $scope.nuovoModello = function (modello) {
      $scope.modello = $scope.serie && $scope.serie === CONFIG.serieEstro ? { serie: $scope.serie, serie: CONFIG.serieEstro, linea: 'estro' } : { serie: CONFIG.serieIlmas };
      $scope.modalModello.show();
    }

    $scope.modificaModello = function (modello) {
      STATOCREAZIONEDISTINTE.last({}, function (stato) {
        $rootScope.statoDistinte = stato;

        $scope.modalModello.show();
        MODELLO.get({ id: modello._id }, function (modello) {
          $scope.modello = _.clone(modello);
          $scope.modelloCategoriaOld = $scope.modello.categoria
        }, function (err) {
          console.log(err)
        });
      });
    }

    /* autocomplete selezione prodotto */
    $scope.addAccessorio = function (prodotto) {
      $scope.modello.accessori.push(prodotto);
    }

    /* elimina accessorio da prodotto */
    $scope.eliminaAccessorio = function (index) {
      $scope.modello.accessori.splice(index, 1);
    }

    /* autocomplete prodotti */
    $scope.cercaAccessori = function (search) {
      return PRODOTTO.cerca({ search: search, ultimeGenerazioni: true }).$promise;
    }

    $scope.zoomImmagineModello = function (modello) {
      $scope.modello = modello;
      $scope.immagineModello =
        IMAGE.get({ id: modello.foto });
      modalImmagineModello.show();
    }

    /* Elimina categoria */
    $scope.eliminaModello = function (modello) {
      PRODOTTO.prodottiinmodello({
        id: modello._id
      },
        function (result) {
          if (result.length == 0) {
            bootbox.confirm({
              title: CONFIG.messages.modello_delete_alert_title,
              message: CONFIG.messages.modello_delete_alert_message,
              buttons: {
                'cancel': {
                  label: CONFIG.messages.alert_delete_undo,
                  className: 'btn-default'
                },
                'confirm': {
                  label: CONFIG.messages.alert_delete_confirm,
                  className: 'btn-primary'
                }
              },
              callback: function (ok) {
                if (ok) {
                  MODELLO.delete({
                    id: modello._id
                  },
                    function (response) {
                      $scope.modalModello.hide();
                      ngNotify.set(CONFIG.messages.modello_deleted, 'success');
                      $scope.modelli = ($scope.serie && $scope.serie === CONFIG.serieEstro ? MODELLO.last({ serie: CONFIG.serieEstro }) : MODELLO.last());
                      $scope.resetSearch();
                    },
                    function (err) {
                      ngNotify.set(CONFIG.messages.modello_delete_error, 'error');
                    });
                }
              }
            });
          }
          else {
            ngNotify.set(CONFIG.messages.prodottiInModello_delete_error, {
              type: 'error',
              duration: 10000
            });
          }
        },
        function (err) {
          console.log(err)
        }
      );
    }

    //autocomplete nome modello
    $scope.cercaModello = function (chiave) {
      var query = { nome: chiave }
      if ($scope.serie && $scope.serie === 'estro')
        query.serie = 'aComponenti';

      return MODELLO.cerca(query).$promise;
    }

    //ricerca modelli in view
    $scope.cercaModelli = function () {
      if ($scope.search && $scope.search != '') {
        if ($scope.search.length < 2) {
          ngNotify.set(CONFIG.messages.search_min_length, 'warn');
        }
        else {
          var query = { nome: $scope.search }
          if ($scope.serie && $scope.serie === CONFIG.serieEstro)
            query.serie = CONFIG.serieEstro;

          $scope.modelli = MODELLO.cerca(
            query, function () {
              $scope.searchresults = $scope.search;
            });
        }
      } else {
        $scope.modelli = MODELLO.last();
        $scope.resetSearch();
      }
    }

    $scope.resetSearch = function () {
      $scope.searchresults = false;
      $scope.search = '';
    }

    // carica file modello
    $scope.caricaFileProdotto = function (file, tipo) {
      var filetype = file.type !== "" ? file.type : 'application/octet-stream', // mi serve per il signed url
        percorso = $scope.modello._id + '/' + tipo + '/' + file.name;

      $scope.uploadingfilePdf = true;

      $http
        ({
          url: '/s3',
          data: {
            percorso: percorso,
            tipo: filetype,
            dimensione: file.size
          },
          method: 'POST',
          skipAuthorization: true // non passo dal autorizzazione con token, s3 si incazza
        })
        .success(function (url) {
          // ho un signed url per l'upload su amazon
          file.upload = Upload.http({
            url: url,
            data: file,
            method: 'PUT',
            skipAuthorization: true, // non passo dal autorizzazione con token di auth0, s3 si incazza
            headers: {
              'Content-Type': filetype
            }
          });

          file.upload.then(function (response) {
            if (response.status === 200) {
              console.log("caricato sto aggiornando")
              // file caricato, aggiorno il prodotto
              $scope.modello['file_' + tipo] = {};
              $scope.modello['file_' + tipo].url = response.config.url.split('?')[0]; // salvo l'url diretto di s3
              $scope.modello['file_' + tipo].nome = file.name;

              $scope.uploadingfilePdf = false;

            }
          });
        });
    }

    $scope.rimuoviFile = function (tipo) {
      $scope.modello.file_pdf = null;
      $scope.modello['file_' + tipo] = null;
      //  _aggiornaProdotto($scope.prodotto);
    }



    // duplica prodotto
    $scope.duplicaModello = function (modello) {
      var nuovo = _.omit(modello, '_id'); // duplico senza id cosi la modale lo considera come nuovo
      nuovo.nome = modello.nome + '-copia';
      nuovo.pubblicato = false;
      $scope.modello = nuovo;
      delete nuovo.accessori;
      delete nuovo.file_pdf
      $scope.modalModello.show();
    }

    $scope.aggiungiElse = function (conf, regola) {
      console.log(conf.casoElse)

      if (conf.casoElse)
        conf.regole.push({
          lunghezzaMaxCodice: null,
          regoleCodice: {},
          componenti: []
        });
      else
        conf.regole.pop()
    }

    /* autocomplete componenti */
    $scope.cercaComponenti = function (search) {
      return COMPONENTE.cerca(
        {
          search: search,
          //fornitore:$scope.prodotto.distintabase
        }
      ).$promise;
    }

    /* autocomplete selezione componenti */
    $scope.addComponente = function (componente) {
      _findRiga = function () {
        var riga
        for (let conf of $scope.modello.configDistintaBase) {
          riga = _.find(conf.regole, function (el) {
            return !!el.cercaComponente
          })
          if (riga) {
            delete riga.cercaComponente
            return riga;
          }
        }
      }

      riga = _findRiga();

      if (!riga.componenti) {
        riga.componenti = [];
      }
      riga.componenti.push({ id: componente._id, codice: componente.codice, qt: 1 });
    }

    /* elimina accessorio da prodotto */
    $scope.eliminaComponente = function (coll, index) {
      coll.splice(index, 1);
    }

    $scope.generaRegolaDistinta = function (conf) {
      if (conf.casoElse) //se esiste un caso else la regola deve essere aggiunta come penultima, perchè l'ultima è sempre il caso else
        conf.regole.splice(conf.regole.length - 1, 0, {
          lunghezzaMaxCodice: null,
          regoleCodice: {},
          componenti: []
        })
      else
        conf.regole.push({
          lunghezzaMaxCodice: null,
          regoleCodice: {},
          componenti: []
        });
    }

    $scope.generaRigaDistinta = function (conf) {
      var d = {
        // tipoAssociazione    : 'Corpo', // default
        tipoEsterno: false, //interno false o esterna true
        regole: []

      };
      $scope.generaRegolaDistinta(d);
      $scope.modello.configDistintaBase.push(d);
    }

    $scope.cancellaRegolaDistinta = function (conf, index, parentIndex) {
      conf.regole.splice(index, 1);
      if (conf.regole.length === 0 || (conf.regole.length === 1 && !!conf.casoElse)) // se è l'ultima regola, o se è rimasta solo quella del caso else cancello proprio la riga della configurazione
        $scope.modello.configDistintaBase.splice(parentIndex, 1)
    }

    $scope.addDistinta = function (modello) {
      $scope.viewCercaDistinta = false;
      for (conf of modello.configDistintaBase) {
        conf.tipoEsterno = true;
        conf.nomeModelloEsterno = modello.nome;
        conf.tipoAssociazione = "";
        $scope.modello.configDistintaBase.push(conf);
      }

    }

    /* non riesco a capire perchè non riesco a filtrare i risultati
    $scope.cercaModelloWR = function (chiave) {
        $scope.cercaModello(chiave)
            .then(function(r) {
                var ret = [];
                for (modello of r) {
                    console.log(modello._id, $scope.modello._id)
                    if (modello._id != $scope.modello._id)
                        ret.push(modello);
                }
                return ret;
            });
    } */

    $scope.eliminaConf = function (nome) { // sono andato per nome modello qui....
      $scope.modello.configDistintaBase = _.filter($scope.modello.configDistintaBase, function (el) {
        return !el.nomeModelloEsterno || el.nomeModelloEsterno != nome;
      })
    }

    $scope.toggleCercaDistinta = function () {
      $scope.viewCercaDistinta = true;
    }

    $scope.bgStyle = function (conf) {
      if (conf.tipoEsterno) return "bgGrey";
    }

    $scope.checkDuplicates = function (innerIdx) {
      //controllo se ci sono duplicati raggruppando le regole per tipoAssociazione e vedendo se i gruppi hanno più di una posizione
      var a = _.groupBy($scope.modello.configDistintaBase, function (el) {
        if (el.tipoEsterno) return 'esterno'; else return el.tipoAssociazione;
      })
      for (gr of _.keys(a)) {
        if (a[gr].length > 1 && gr != 'esterno') {
          $scope.modello.configDistintaBase[innerIdx].tipoAssociazione = "";
          ngNotify.set("Questo tipo di associazione è gia presente in questa distinta", 'warn');
        }
      }
    }

    $scope.generaDistinta = function () {
      if (!$rootScope.statoDistinte.dt_fine)
        return;

      bootbox.confirm({
        title: CONFIG.messages.distinta_create_alert_title,
        message: CONFIG.messages.distinta_create_alert_message,
        buttons: {
          'cancel': {
            label: CONFIG.messages.distinta_create_alert_annulla,
            className: 'btn-default'
          },
          'confirm': {
            label: CONFIG.messages.distinta_create_alert_confirm,
            className: 'btn-primary'
          }
        },
        callback: function (ok) {
          if (ok) {
            $rootScope.statoDistinte.dt_fine = false;
            MODELLO.distinta({
              id: $scope.modello._id
            }, function (result) {
              ngNotify.set(CONFIG.messages.distinta_create_success_message, { type: 'success', duration: 10000 });
              $scope.modalModello.hide();
            });
          }
        }
      });

    }

  }]);
