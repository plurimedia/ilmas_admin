app.controller('login', [
  '$rootScope',
  '$scope',
  function($rootScope, $scope) {

    $scope.login = function () {
      $rootScope.lock.show({
        language: 'it',
        allowSignUp: false,
        languageDictionary: {
          title: 'Ilmas Backend'
        },
        theme: {
          logo: '/img/logo/logo-login.svg',
          primaryColor: '#e2001a'
        }
      })
    }

  }
]);
