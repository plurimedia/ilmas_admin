app.controller('colori', [
    '$scope',
    '$modal',
    'ngNotify',
    'CONFIG', 
    'PING', 
    '$interval', 
    '$rootScope','$http','COLORI',
    function ($scope,$modal,ngNotify,CONFIG,PING,$interval,$rootScope,$http,COLORI) {

    //Salva il log in DB
    $rootScope.dwhLog("colori");

    //effettua un ping verso il server, ogni x millisencondi, per capire se esiste la connessione
    var _ping = function(){
        PING.query(
        function (response) {},
        function (err) {
             ngNotify.set(CONFIG.messages.session_error, {
                type: 'error',
                sticky: true
             });
        }); 
    }
    $interval(_ping, CONFIG.timeToPing); 

    $scope.modalTipo = $modal(
        {
            scope: $scope,
            title: 'Nuovo',
            templateUrl: 'html/modals/colore.html',
            show: false,
            backdrop: 'static',
            id: 'modal-attivita'
        });
        
    $scope.colori = COLORI.list();
   
    $scope.nuovoColore = function () 
    {   
        $scope.colore = {}
        $scope.modalTipo.show();
    }

    $scope.dettaglio = function (c) 
    {
        $scope.modalTipo.show();
        $scope.colore = _.clone(c);
    }


    $scope.salvaColore = function (obj) 
    {  
        if(obj) $scope.colore = obj;
        
        if($scope.colore && $scope.colore._id) {
            COLORI.update({
                    id: $scope.colore._id
                }, $scope.colore,
                function (response) 
                {
                    $scope.colori = COLORI.list();
                    $scope.modalTipo.hide();
                    ngNotify.set(CONFIG.messages.colore_saved, 'success');
                },
                function (err) 
                {
                    ngNotify.set(CONFIG.messages.colore_update_error, 'error');
                });

        }
        else 
        {
            COLORI.save($scope.colore,
                function (response) 
                {
                    $scope.colori = COLORI.list();
                    $scope.modalTipo.hide();
                    ngNotify.set(CONFIG.messages.colore_saved, 'success');
                },
                function (err) {
                    ngNotify.set(CONFIG.messages.colore_save_error, 'error');
                });

        }
    } 

    
    $scope.eliminaColore = function (id) 
    {
        bootbox.confirm({
            title: CONFIG.messages.componente_delete_alert_title,
            message: CONFIG.messages.componente_delete_alert_message,
            buttons: {
                'cancel': 
                {
                    label: CONFIG.messages.alert_delete_undo,
                    className: 'btn-default'
                },
                'confirm': 
                {
                    label: CONFIG.messages.alert_delete_confirm,
                    className: 'btn-primary'
                }
            },
            callback: function (ok) 
            {
                if (ok) 
                {
                    COLORI.delete({
                            id: id
                        },
                        function (response) 
                        {
                            $scope.colori = COLORI.list();
                            $scope.modalTipo.hide();
                            ngNotify.set(CONFIG.messages.colore_saved, 'success');
                        },
                        function (err) 
                        {
                            ngNotify.set(CONFIG.messages.componente_delete_error, 'error');
                        }
                    );
                }
            }
        })
    }

}]);
