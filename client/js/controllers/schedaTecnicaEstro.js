app.controller('schedaTecnicaEstro', ['$rootScope', '$scope', 'PRODOTTO', 'PDF', 'COLORI', 'CONFIG', '$window',
function($rootScope, $scope, PRODOTTO, PDF, COLORI, CONFIG, $window)
{
   if (!$rootScope.profili || !$rootScope.profili.length)
      $rootScope.profili = PRODOTTO.profili();

   if (!$rootScope.fonti || !$rootScope.fonti.length)
      $rootScope.fonti = PRODOTTO.fonti();

   if (!$rootScope.finiture || !$rootScope.finiture.length)
      $rootScope.finiture = COLORI.finiture();

   $scope.schedaTecnicaEstro = function(en)
   {
      var tipoScheda = "schedatecnicaEstro";
      if ($scope.prodotto.codice && $scope.prodotto.fonte && ($scope.prodotto.lunghezza || $scope.prodotto.moduli) && $scope.prodotto.finitura && $scope.prodotto.finitura.length === 2)
      {
         var params = {
            tipopdf: tipoScheda,
            anteprima: true,
         }
         var data = {
            profilo: $scope.prodotto.codice,
            fonte: $scope.prodotto.fonte,
            finitura: $scope.prodotto.finitura,
            lunghezza: $scope.prodotto.lunghezza,
            numero_moduli: $scope.prodotto.moduli,
            codice_finito: $scope.codiceEstroFinito(),
            cover: CONFIG.estroClassificazioneBinari[$scope.prodotto.fonte.substr(0, 1)]
         }

         $scope.pdf = true;

         // in inglese?
         if (en) params.en = true;

         $scope.pdf = PDF.save(params, data, function(response)
         {
            $scope.pdf = false;
            $window.open(response.file, '_blank'); // apri il pdf in una nuova finestra
         })
      }
   }

   $scope.azzera = function()
   {
      $scope.prodotto = {};
      $scope.search = {};
   }

   $scope.codiceEstroFinito = function()
   {
      String.prototype.lpad = function(padString, length)
      {
         var str = this;
         while (str.length < length)
            str = padString + str;
         return str;
      }

      let ret = ''

      if ($scope.prodotto.codice && $scope.prodotto.fonte && ($scope.prodotto.lunghezza || $scope.prodotto.moduli) && $scope.prodotto.finitura && $scope.prodotto.finitura.length === 2)
      {
         ret += $scope.prodotto.codice + $scope.prodotto.fonte
         ret += $scope.prodotto.lunghezza ? $scope.prodotto.lunghezza.toString().lpad('0', 5) : $scope.prodotto.moduli.toString().lpad('0', 5)
         ret += $scope.prodotto.finitura
      }

      return ret;
   }

   $scope.azzera();
}]);
