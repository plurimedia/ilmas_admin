app.controller('agenti', [
    '$scope',
    '$modal',
    'ngNotify',
    'CONFIG',
    'AGENTE',
    'PING', 
    '$interval', 
    '$rootScope','COMUNI', 'NAZIONI',
    function ($scope,$modal,ngNotify,CONFIG,AGENTE,PING,$interval,$rootScope, COMUNI, NAZIONI,) {

    //Salva il log in DB
    $rootScope.dwhLog("Agente");

    //effettua un ping verso il server, ogni x millisencondi, per capire se esiste la connessione
    var _ping = function(){
        PING.query(
        function (response) {},
        function (err) {
             ngNotify.set(CONFIG.messages.session_error, {
                type: 'error',
                sticky: true
             });
        }); 
    }
    $interval(_ping, CONFIG.timeToPing); 

    $scope.contatto = {}
    $scope.indiceContatto=-1

    $scope.coordinata = {}
    $scope.indiceCoordinata=-1

    $scope.modalAgente = $modal(
    {
        scope: $scope,
        title: 'Nuovo agente',
        templateUrl: 'html/modals/agente.html',
        show: false,
        id: 'modal-agente',
        backdrop: 'static',
        keyboard: false
    });

    $scope.tabs = [
        {
            title: "Info generali",
            template: '/html/includes/agente-tab-principale.html',
            activeTab: true
        },
        {
            title: "Recapito",
            template: '/html/includes/agente-tab-recapito.html',
            activeTab: true
        },
        {
            title: "Coordinate",
            template: '/html/includes/agente-tab-coordinate.html',
            activeTab: true
        },
        {
            title: "Contatti",
            template: '/html/includes/agente-tab-contatti.html',
            activeTab: false
        }
    ];

    $scope.nuovoAgente = function (agente) 
    {
        $scope.agente = {};
        $scope.modalAgente.show();
    }

    $scope.modificaAgente = function (agente) 
    {
        $scope.modalAgente.show();
        $scope.agente = _.clone(agente);
    }

    $scope.salvaAgente = function (agente) 
    {
        if (!agente.coordinates || !agente.coordinates.length){
            ngNotify.set(CONFIG.messages.agente_save_error, 'error');
            return;
        }

        if(!agente._id) 
        {
            AGENTE.save(agente,
            function (response) 
            {
                ngNotify.set(CONFIG.messages.agente_saved, 'success');
                $scope.modalAgente.hide();
                $scope.agenti = AGENTE.last();
            },
            function (err) {
                ngNotify.set(CONFIG.messages.agente_save_error, 'error');
            });
        }
        else 
        {
            AGENTE.update({
                id: agente._id
            }, agente,
            function (response) 
            {
                ngNotify.set(CONFIG.messages.agente_updated, 'success');
                $scope.modalAgente.hide();
                $scope.agenti = AGENTE.last();
            },
            function (err) 
            {
                ngNotify.set(CONFIG.messages.agente_update_error, 'error');
            });
        }
    }

  
    $scope.eliminaAgente = function (agente) 
    {  
        bootbox.confirm({
            title: CONFIG.messages.agente_delete_alert_title,
            message: CONFIG.messages.agente_delete_alert_message,
            buttons: {
                'cancel': {
                    label: CONFIG.messages.alert_delete_undo,
                    className: 'btn-default'
                },
                'confirm': {
                    label: CONFIG.messages.alert_delete_confirm,
                    className: 'btn-primary'
                }
            },
            callback: function (ok) 
            {
                if (ok) 
                {
                    AGENTE.delete({
                        id: agente._id
                    },
                    function (response) 
                    {
                        $scope.modalAgente.hide();
                        ngNotify.set(CONFIG.messages.agente_deleted, 'success');
                        $scope.agenti = AGENTE.last()
                    },
                    function (err) 
                    {
                        ngNotify.set(CONFIG.messages.agente_delete_error, 'error');
                    });
                }
            }
        });
    }

   
    $scope.resetSearch = function () 
    {
        $scope.searchresults = false;
        $scope.search = '';
    }

    $scope.agenti = AGENTE.last();
/*
    $scope.currentPage = 1;
    $scope.itemsPerPage = 1000;
    $scope.totalItems = $scope.agenti.length;
    
    $scope.$watch("currentPage", function() {
        setPagingData($scope.currentPage);
    });

    function setPagingData(page) {
        var pagedData = $scope.agenti.slice(
            (page - 1) * $scope.itemsPerPage,
            page * $scope.itemsPerPage
        );
        $scope.pageAgenti = pagedData;
    };
    */

    
   $scope.cercaComune = function (chiave) 
   {
       return COMUNI.cerca({
           nome: chiave
       }).$promise;
   }

   $scope.sceltaComune = function (value) 
   {
       $('#cap').focus(); // passa al cap
       $scope.agente.comune = value.nome;
       $scope.agente.prov = value.provincia;
       $scope.agente.cap = value.cap;
       $scope.agente.nazione = 'Italia';
   };

   $scope.salvaContato = function(c){ 
       if ($scope.indiceContatto>-1)
            $scope.agente.contatti[$scope.indiceContatto]=c
        else{
            if (!$scope.agente.contatti)
                $scope.agente.contatti = [];

            $scope.agente.contatti.push(c)
        }
        $scope.indiceContatto=-1
            
        $scope.contatto = {}
   }

    $scope.prepModContato = function(c, indice){ 
        $scope.indiceContatto = indice
        $scope.contatto = _.clone(c)
   }

   $scope.deleteContato = function(indice){
    $scope.agente.contatti.splice(indice, 1);
   }

   $scope.salvaCoordinata = function(c){ 
    if ($scope.indiceCoordinata>-1)
         $scope.agente.coordinates[$scope.indiceCoordinata]=c
     else{
         if (!$scope.agente.coordinates)
             $scope.agente.coordinates = [];

         $scope.agente.coordinates.push(c)
     }
     $scope.indiceCoordinata=-1
         
     $scope.coordinata = {}
    }

    $scope.prepModCoordinata = function(c, indice){ 
        $scope.indiceCoordinata = indice
        $scope.coordinata = _.clone(c)
    }

    $scope.deleteCoordinata = function(indice){
    $scope.agente.coordinates.splice(indice, 1);
    }

   
}]);
