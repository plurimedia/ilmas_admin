app.controller('prodotti', ['$scope', '$rootScope', '$http', '$window', '$modal', 'Upload', 'IMAGE', 'CONFIG', '$routeParams', '$route', 'PRODOTTO', 'MODELLO', 'CATEGORIA', 'SOTTOCATEGORIE', 'CSVPRODOTTI', 'PDF', 'COLORI', 'ngNotify', 'COMPONENTE', 'PING', '$interval', 'STATOCREAZIONEDISTINTE',
  function ($scope, $rootScope, $http, $window, $modal, Upload, IMAGE, CONFIG, $routeParams, $route, PRODOTTO, MODELLO, CATEGORIA, SOTTOCATEGORIE, CSVPRODOTTI, PDF, COLORI, ngNotify, COMPONENTE, PING, $interval, STATOCREAZIONEDISTINTE) {
    $scope.serie = $route.current.$$route.serie;

    //Salva il log in DB
    $rootScope.dwhLog("Prodotti");

    var _ping = function () {
      PING.query(
        function (response) { },
        function (err) {
          ngNotify.set(CONFIG.messages.session_error, {
            type: 'error',
            sticky: true
          });
        });
    }
    $interval(_ping, CONFIG.timeToPing);

    var modalProdotto = $modal({
      scope: $scope,
      title: 'Nuovo prodotto',
      templateUrl: ($scope.serie && $scope.serie === CONFIG.serieEstro ? 'html/modals/prodottoEstro.html' : 'html/modals/prodotto.html'),
      show: false,
      id: 'modal-prodotto',
      backdrop: 'static',
      keyboard: false
    }),
      modalFoto = $modal({
        scope: $scope,
        title: 'Scegli una foto',
        templateUrl: 'html/modals/foto.html',
        show: false,
        id: 'modal-foto',
        backdrop: 'static',
        keyboard: false
      }),
      modalImport = $modal({
        scope: $scope,
        title: 'Import prodotti',
        templateUrl: 'html/modals/import_prodotti.html',
        show: false,
        id: 'modal-import-prodotti'
      }),
      modalImmagineProdotto = $modal({
        scope: $scope,
        title: 'Nuova Linea',
        templateUrl: 'html/modals/prodottoImmagine.html',
        show: false,
        id: 'modal-prodotto-immagine',
        backdrop: 'static',
        keyboard: false
      }),
      modalExportFileKite = $modal({
        scope: $scope,
        title: 'Esporta file per KITE',
        templateUrl: 'html/modals/export_fileKite.html',
        show: false,
        id: 'modal-prodotto'
      }),
      modalComponente = $modal(
        {
          scope: $scope,
          title: 'Nuovo Componente',
          templateUrl: 'html/modals/componente.html',
          show: false,
          id: 'modal-componente',
          backdrop: 'static',
          keyboard: false
        }),
      modalDistinta = $modal({
        scope: $scope,
        title: 'Fistinta Base',
        templateUrl: 'html/modals/distinta.html',
        show: false,
        id: 'modal-distinta',
        backdrop: 'static',
        keyboard: false
      }),

      _salvaProdotto = function (prodotto) {

        if (prodotto.fascio)
          prodotto.fascio = prodotto.fascio.replace("°", "")

        delete prodotto.keywords

        PRODOTTO.save(prodotto,
          function (response) {
            modalProdotto.hide();
            ngNotify.set(CONFIG.messages.prodotto_saved, 'success');
            $scope.prodotti = ($scope.serie && $scope.serie === CONFIG.serieEstro ? PRODOTTO.ultimi({ generazione: $scope.generazione, serie: CONFIG.serieEstro }) : PRODOTTO.ultimi({ generazione: $scope.generazione }));
            $scope.resetSearch();
          },
          function (err) {
            console.log(err)
            ngNotify.set(CONFIG.messages.prodotto_save_error, 'error');
          });
      },
      _aggiornaProdotto = function (prodotto) {
        if (prodotto.fascio)
          prodotto.fascio = prodotto.fascio.replace("°", "")

        delete prodotto.keywords

        PRODOTTO.update({
          id: prodotto._id
        }, prodotto,
          function (response) {
            modalProdotto.hide();
            ngNotify.set(CONFIG.messages.prodotto_updated, 'success');
            $scope.prodotti = ($scope.serie && $scope.serie === CONFIG.serieEstro ? PRODOTTO.ultimi({ generazione: $scope.generazione, serie: CONFIG.serieEstro }) : PRODOTTO.ultimi({ generazione: $scope.generazione }));
            $scope.resetSearch();
          },
          function (err) {
            console.log(err)
            ngNotify.set(CONFIG.messages.prodotto_update_error, 'error');
          });
      };

    $scope.open = false

    $scope.apri = function () {
      $scope.open = true
    }

    $scope.chiudi = function () {
      $scope.open = false
    }

    $scope.generazione = $routeParams.generazione;
    if (!$scope.generazione)
      $scope.generazione = 'D3' // 'G7'

    $scope.pageTitle = "";
    $scope.modalitaArchivio = false;

    if ($scope.generazione == 'G5' || $scope.generazione == 'G4') {
      $scope.pageTitle = ($scope.serie && $scope.serie === $scope.CONFIG.serieEstro ? 'Archivio componenti G5 - G4' : 'Archivio prodotti G5 - G4')
      $scope.modalitaArchivio = true;
    }
    else
      $scope.pageTitle = ($scope.serie && $scope.serie === $scope.CONFIG.serieEstro ? 'Componenti G7 - G6 - D1 - D2 - V1 - 3HE - " space " ' : 'Prodotti G7 - G6 - D1 - D2 - V1 - 3HE - " space " ')

    $scope.elencoAnni = $rootScope.elencoAnni();
    $scope.lesses = $rootScope.elencoLes();
    $scope.modelliLed = $rootScope.elencoLed();
    $scope.elencoIrc = $rootScope.elencoIrc();
    $scope.generazioni = $rootScope.generazioni();

    $scope.prodotti = ($scope.serie && $scope.serie === CONFIG.serieEstro ? PRODOTTO.ultimi({ generazione: $scope.generazione, serie: CONFIG.serieEstro }) : PRODOTTO.ultimi({ generazione: $scope.generazione }));
    $scope.modelli = ($scope.serie && $scope.serie === CONFIG.serieEstro ? MODELLO.list({ serie: CONFIG.serieEstro }) : MODELLO.list());
    $scope.categorie = ($scope.serie && $scope.serie === CONFIG.serieEstro ? CATEGORIA.list({ serie: CONFIG.serieEstro }) : CATEGORIA.list());

    $scope.batch = [];

    var K_TITLETAB_EAN = "Bar codes - EAN 13";

    $scope.tabs = [
      {
        title: "Informazioni",
        template: '/html/includes/prodotto-tab-informazioni.html',
        activeTab: true
      },
      {
        title: "Immagini",
        template: '/html/includes/prodotto-tab-foto.html'
      },
      {
        title: "Files",
        template: '/html/includes/prodotto-tab-files.html'
      },
      {
        title: K_TITLETAB_EAN,
        template: '/html/includes/prodotto-tab-ean13.html'
      },
      {
        title: "Accessori",
        template: '/html/includes/prodotto-tab-accessori.html'
      },
      {
        title: "Distinta Base",
        template: '/html/includes/prodotto-tab-componenti.html'
      },
      {
        title: "Dati vs KITE",
        template: '/html/includes/prodotto-tab-kite.html'
      },
      {
        title: "Contabilità",
        template: '/html/includes/prodotto-tab-contabilita.html'
      },
    ];

    if ($scope.serie && $scope.serie === CONFIG.serieEstro) {
      $scope.tabs = [
        {
          title: "Informazioni",
          template: '/html/includes/prodotto-tab-informazioniEstro.html',
          activeTab: true
        },
        {
          title: "Immagini",
          template: '/html/includes/prodotto-tab-foto.html'
        },
        {
          title: "Files",
          template: '/html/includes/prodotto-tab-files.html'
        },
        {
          title: "Accessori",
          template: '/html/includes/prodotto-tab-accessori.html'
        },
        {
          title: "Dati vs KITE",
          template: '/html/includes/prodotto-tab-kite.html'
        },
        {
          title: "Scheda tecnica",
          template: '/html/includes/prodotto-tab-schedaTecnica.html'
        }
      ];
    }

    $scope.tabsDistinta = [
      {
        title: "Distinta Base",
        template: '/html/includes/prodotto-tab-distinta.html',
        activeTab: true
      }
    ];

    /* autocomplete selezione prodotto */
    $scope.addAccessorio = function (prodotto) {
      $scope.prodotto.accessori.push(prodotto);
    }

    /* elimina accessorio da prodotto */
    $scope.eliminaAccessorio = function (index) {
      $scope.prodotto.accessori.splice(index, 1);
    }

    $scope.apriModaleProdotto = function (prodotto) {
      modalProdotto.show();
      if (prodotto) {
        $scope.prodotto = _.clone(prodotto);
        console.log(prodotto)
      }
      else {
        //creo un nuovo prodotto generazione G6
        $scope.prodotto = { generazione: 'G7', serie: CONFIG.serieIlmas };
        if ($scope.serie && $scope.serie === CONFIG.serieEstro) { $scope.prodotto.serie = CONFIG.serieEstro; $scope.prodotto.linea = 'estro' }
      }
      //$scope.calcolaClasseEnergetica();
    }

    $scope.apriDettaglioProdotto = function (prodotto) {
      var query = { id: prodotto._id }
      if ($scope.serie && $scope.serie === CONFIG.serieEstro)
        query.serie = CONFIG.serieEstro

      PRODOTTO.get(query, function (prodotto) {
        // prodotto.codEan = "001234567890";
        $scope.prodotto = prodotto

        $scope.apriModaleProdotto($scope.prodotto)
        $scope.manageDistinta();

      }, function (err) {
        console.log(err)
      })
    }

    $scope.apriDettaglioDistintaBase = function (prodotto) {
      PRODOTTO.get({ id: prodotto._id }, function (prodotto) {
        $scope.prodotto = prodotto;
        $scope.manageDistinta();
        modalDistinta.show();

      }, function (err) {
        console.log(err)
      })
    }

    $scope.manageDistinta = function () {
      if ($scope.prodotto.componenti) {
        $scope.model = [{
          _id: $scope.prodotto._id,
          nome: $scope.prodotto.nome,
          codice: $scope.prodotto.codice,
          children: []
        }]

        $scope.prodotto.componenti.forEach(function (c) {
          var child = { _id: c._id._id, nome: c._id.nome, codice: c._id.codice, tipoAssociazione: c.tipoAssociazione, qt: c.qt };
          if (c._id.componenti_figlio && c._id.componenti_figlio.length)
            child.children = c._id.componenti_figlio.map(function (f) {
              return { _id: f._id._id, nome: f._id.nome, codice: f._id.codice, qt: f.qt }
            })
          $scope.model[0].children.push(child)
        })

        distintaBaseTmp = _.groupBy($scope.model[0].children,
          function (e) {
            return e.tipoAssociazione
          });

        var chiavi = Object.keys(distintaBaseTmp)

        var distintaBase = [];
        chiavi.forEach(function (c) {
          var obj = {
            tipoAssociazioneGruppo: c,
            children: distintaBaseTmp[c]
          }
          distintaBase.push(obj)
        })
        $scope.model[0].children = distintaBase
      }
    }


    $scope.apriModaleAccessorio = function (prodotto) {
      modalProdotto.hide();

      PRODOTTO.get({ id: prodotto._id }, function (prodotto) {
        $scope.prodotto = prodotto;
      });

      $scope.apriModaleProdotto(prodotto);
    }

    $scope.cercaSottocategorie = function (chiave) {
      return SOTTOCATEGORIE.query({
        nome: chiave
      }).$promise;
    }

    // selezione typeahead sottocategoria
    $scope.sceltaSottocategoria = function (value) {
      $scope.prodotto.sottocategoria = value.nome;
    };

    /* salvataggio nuovo prodotto */
    $scope.salvaProdotto = function () {

      if (!$scope.prodotto._id)
        _salvaProdotto($scope.prodotto);
      else
        _aggiornaProdotto($scope.prodotto);
    }

    /* Elimina prodotto */
    $scope.eliminaProdotto = function (id) {
      bootbox.confirm({
        title: CONFIG.messages.prodotto_delete_alert_title,
        message: CONFIG.messages.prodotto_delete_alert_message,
        buttons: {
          'cancel': {
            label: CONFIG.messages.alert_delete_undo,
            className: 'btn-default'
          },
          'confirm': {
            label: CONFIG.messages.alert_delete_confirm,
            className: 'btn-primary'
          }
        },
        callback: function (ok) {
          if (ok) {
            PRODOTTO.delete({
              id: id
            },
              function (response) {
                modalProdotto.hide();
                ngNotify.set(CONFIG.messages.prodotto_deleted, 'success');
                $scope.resetSearch();
                $scope.prodotti = PRODOTTO.ultimi({ generazione: $scope.generazione });
              },
              function (err) {
                ngNotify.set(CONFIG.messages.prodotto_delete_error, 'error');
              });
          }
        }
      });
    }


    /* cerca prodotti */
    $scope.cercaProdotti = function () {
      if ($scope.search && $scope.search != '') {
        if ($scope.search.length < 2) {
          ngNotify.set(CONFIG.messages.search_min_length, 'warn');
        } else {
          if ($scope.serie && $scope.serie === CONFIG.serieEstro)
            $scope.prodotti = PRODOTTO.cerca(
              {
                search: $scope.search, generazione: $scope.generazione, serie: CONFIG.serieEstro
              }, function () {

              });
          else
            $scope.prodotti = PRODOTTO.cerca(
              {
                search: $scope.search, generazione: $scope.generazione
              }, function () {

              });
        }
      } else {
        $scope.prodotti = ($scope.serie && $scope.serie === CONFIG.serieEstro ? PRODOTTO.ultimi({ generazione: $routeParams.generazione, serie: CONFIG.serieEstro }) : PRODOTTO.ultimi({ generazione: $routeParams.generazione }));
        $scope.resetSearch();
      }
    }



    /* autocomplete prodotti */
    $scope.cercaAccessori = function (search) {
      return PRODOTTO.cerca({ search: search, generazione: $scope.generazione }).$promise;
    }

    $scope.resetSearch = function () {
      $scope.searchresults = false;
      $scope.search = '';
    }

    /* Cerca una foto per tipo */
    $scope.scegliFoto = function (tipo) {
      $scope.tipoFoto = tipo;
      IMAGE.ultimetipo({
        tipo: tipo
      }, function (result, err) {
        modalProdotto.hide();
        modalFoto.show();
        $scope.immagini = result;
      })
    }

    // cerca immagine
    $scope.cercaImmagini = function (searchimg) {
      var search = {
        tipo: $scope.tipoFoto,
        nome: searchimg
      }
      if (searchimg && searchimg != '') {
        if (searchimg.length < 2) {
          ngNotify.set(CONFIG.messages.search_min2_length, 'warn');
        } else {
          $scope.immagini = IMAGE.cerca({
            cerca: search
          });
        }
      } else {
        $scope.immagini = IMAGE.ultimetipo({
          tipo: search.tipo
        });
      }
    }

    // associa una foto di un tipo ad un prodotto
    $scope.associaFoto = function (foto, tipo) {
      var update = {
        foto: foto,
        tipo: tipo,
        prodotto: $scope.prodotto._id
      };
      PRODOTTO.updateimage(update,
        function (resp) {
          $scope.prodotto[tipo] = foto;
          modalFoto.hide();
          modalProdotto.show();
          $scope.tabs.activeTab = "Immagini";
          ngNotify.set(CONFIG.messages.prodotto_updated_images, 'success');
        },
        function (err) {
          ngNotify.set(CONFIG.messages.prodotto_update_images_error, 'error');
        }
      )
    }

    /* upload massivo prodotti*/
    $scope.import = function () {
      modalImport.show();
    }

    // stabilisce quale import fare (da migliorare)
    $scope.tipoImport = function (linea) {
      $rootScope.tipoimport = linea;
    }

    $scope.uploadCsv = function (file) {
      $scope.prodotti.$resolved = false;
      modalImport.hide();
      CSVPRODOTTI.upload(file, function (data) {
        // rileggo i dati
        $scope.prodotti = ($scope.serie && $scope.serie === CONFIG.serieEstro ? PRODOTTO.ultimi({ generazione: $scope.generazione, serie: CONFIG.serieEstro }) : PRODOTTO.ultimi({ generazione: $scope.generazione }));

      })
    }

    $scope.schedaTecnica = function (prodotto, en) {
      var tipoScheda = "schedatecnica";



      CONFIG.id_binari.forEach
        (
          function (idBinario) {
            if (prodotto.modello._id.indexOf(idBinario._id) != -1) {
              tipoScheda = "schedaTecnicaBinario";

              return;
            }
          }

        )


      PRODOTTO.accessori({ id: prodotto._id }, function (accessori) {
        $scope.prodotto.accessori = accessori;
        $scope.pdf = true;
        var params = {
          tipopdf: tipoScheda,
          anteprima: true,
        },
          data = $scope.prodotto;

        // in inglese ?
        if (en)
          params.en = true;

        $scope.pdf = PDF.save(params, data, function (response) {
          $scope.pdf = false;
          $window.open(response.file, '_blank'); // apri il pdf in una nuova finestra
        })
      });
    }

    // carica file
    $scope.caricaFileProdotto = function (file, tipo) {
      var filetype = file.type !== "" ? file.type : 'application/octet-stream', // mi serve per il signed url
        percorso = $scope.prodotto._id + '/' + tipo + '/' + file.name;

      if (tipo.toLowerCase() == 'ldt')
        $scope.uploadingfile = true;
      else if (tipo.toLowerCase() == 'pdf')
        $scope.uploadingfilePdf = true;

      $http
        ({
          url: '/s3',
          data: {
            percorso: percorso,
            tipo: filetype,
            dimensione: file.size
          },
          method: 'POST',
          skipAuthorization: true // non passo dal autorizzazione con token, s3 si incazza
        })
        .success(function (url) {

          // ho un signed url per l'upload su amazon
          file.upload = Upload.http({
            url: url,
            data: file,
            method: 'PUT',
            skipAuthorization: true, // non passo dal autorizzazione con token di auth0, s3 si incazza
            headers: {
              'Content-Type': filetype
            }
          });

          file.upload.then(function (response) {
            if (response.status === 200) {

              // file caricato, aggiorno il prodotto
              $scope.prodotto['file_' + tipo] = {};
              $scope.prodotto['file_' + tipo].url = response.config.url.split('?')[0]; // salvo l'url diretto di s3
              $scope.prodotto['file_' + tipo].nome = file.name;

              if (tipo.toLowerCase() == 'ldt')
                $scope.uploadingfile = false;
              else if (tipo.toLowerCase() == 'pdf')
                $scope.uploadingfilePdf = false;

              //if(tipo==='ldt') { // devo aggiornare tutti i prodotti con lo stesso codice ma colore diverso (la fotometria non cambia)
              //    PRODOTTO.updateLdt($scope.prodotto, function (err, result){
              //        ngNotify.set('File fotometrico aggiunto con successo', 'success');
              //   })
              //}
              //else {
              //_aggiornaProdotto($scope.prodotto);
              //}
            }
          });
        });
    }

    // rimuovo un file di un tipo
    $scope.rimuoviFile = function (tipo) {
      delete $scope.prodotto['file_' + tipo];
      //  _aggiornaProdotto($scope.prodotto);
    }

    // duplica prodotto
    $scope.duplicaProdotto = function (prodotto) {
      var nuovoprodotto = _.omit(prodotto, '_id'); // duplico senza id cosi la modale lo considera come nuovo
      nuovoprodotto.codice = prodotto.codice + '-copia';
      nuovoprodotto.pubblicato = false;
      nuovoprodotto.duplica = true;
      delete nuovoprodotto.padre
      delete nuovoprodotto.codiceEan
      delete nuovoprodotto.trascodifica

      $scope.apriModaleProdotto(nuovoprodotto);
    }

    $scope.esportaXlsMetel = function (anno) {
      $scope.prodotti.$resolved = false;
      PRODOTTO.csvmetel({ anno: anno }, function (response) {
        $window.open(response.file, '_blank');
        $scope.prodotti.$resolved = true;
      })
    }

    /* autocomplete componenti */
    $scope.cercaComponenti = function (search) {
      return COMPONENTE.cerca(
        {
          search: search,
          //fornitore:$scope.prodotto.distintabase
        }
      ).$promise;
    }

    //autocomplete componente binario
    $scope.cercaBinari = function (search) {
      var query = { 'categoria.tipo': 'profili', serie: 'aComponenti', search: search }

      return PRODOTTO.cerca(query).$promise;
    }

    //autocomplete componente luminoso
    $scope.cercaFontiLuminose = function (search) {
      var query = { 'categoria.tipo': 'fonti_luminose', serie: 'aComponenti', search: search }

      return PRODOTTO.cerca(query).$promise;
    }

    //autocomplete finiture
    $scope.cercaColori = function (search) {
      var query = { 'categoria.tipo': 'fonti_luminose', serie: 'aComponenti', search: search }

      return COLORI.cerca(query).$promise;
    }

    $scope.sceltaComponenteBin = function (value) {
      $scope.prodotto.st_codice_binario = value.codice;
      $scope.st_id = value._id
    };

    $scope.sceltaComponenteLum = function (value) {
      $scope.prodotto.st_codice_fonteluminosa = value.codice;
      $scope.st_id = value._id
    };

    $scope.sceltaColore = function (value) {
      $scope.prodotto.st_finitura = value.codice;
    };

    $scope.schedaTecnicaEstro = function (en) {
      var tipoScheda = "schedatecnicaEstro";
      if ($scope.st_id && $scope.prodotto && $scope.prodotto.st_finitura)
        PRODOTTO.get({ id: $scope.st_id }, function (prodotto) {
          $scope.pdf = true;
          let fonte, profilo

          if ($scope.prodotto.categoria.tipo === 'fonti_luminose') {
            profilo = prodotto
            fonte = $scope.prodotto
          } else {
            profilo = $scope.prodotto
            fonte = prodotto
          }

          let finitura
          COLORI.codice({ codice: $scope.prodotto.st_finitura }, function (colore) {
            finitura = colore

            var params = {
              tipopdf: tipoScheda,
              anteprima: true,
            },
              data = {
                profilo: profilo,
                fonte: fonte,
                lunghezza: $scope.prodotto.st_lunghezza,
                numero_moduli: $scope.prodotto.st_numero_moduli,
                finitura: finitura,
                codice_finito: $scope.codiceEstroFinito(),
                cover: CONFIG.estroClassificazioneBinari[fonte.codice.substr(0, 1)]
              }

            // in inglese ?
            if (en)
              params.en = true;

            $scope.pdf = PDF.save(params, data, function (response) {
              $scope.pdf = false;
              $window.open(response.file, '_blank'); // apri il pdf in una nuova finestra
            })
          })

        }, function (err) {
          console.log(err)
        });

    }

    $scope.codiceEstroFinito = function () {
      String.prototype.lpad = function (padString, length) {
        var str = this;
        while (str.length < length)
          str = padString + str;
        return str;
      }

      let ret = ''
      if (($scope.prodotto.st_codice_binario || $scope.prodotto.st_codice_fonteluminosa) && ($scope.prodotto.st_lunghezza || $scope.prodotto.st_numero_moduli) && $scope.prodotto.st_finitura && $scope.prodotto.st_finitura.length === 2) {
        if ($scope.prodotto.categoria.tipo === 'fonti_luminose') {
          ret += $scope.prodotto.st_codice_binario + $scope.prodotto.codice
        } else {
          ret += $scope.prodotto.codice + $scope.prodotto.st_codice_fonteluminosa
        }
        ret += $scope.prodotto.st_lunghezza ? $scope.prodotto.st_lunghezza.toString().lpad('0', 5) : $scope.prodotto.st_numero_moduli.toString().lpad('0', 5)
        ret += $scope.prodotto.st_finitura
        console.log("API DEBUG - prodotti.js:768 - ret value: %s", ret)
        return ret
      } else return ''
    }

    /* autocomplete selezione componenti */
    $scope.addComponente = function (componente) {
      $scope.prodotto.distintabase = componente.fornitore
      if (!$scope.prodotto.componenti) {
        $scope.prodotto.componenti = [];
      }
      $scope.prodotto.componenti.push(componente);
    }

    /* elimina accessorio da prodotto */
    $scope.eliminaComponente = function (index) {

      $scope.prodotto.componenti.splice(index, 1);
      if ($scope.prodotto.componenti.length == 0)
        $scope.prodotto.distintabase = null;
    }

    $scope.esportaXlsChina1 = function () {
      $scope.prodotti.$resolved = false;
      PRODOTTO.csvDistintaBase(function (response) {
        $window.open(response.file, '_blank');
        $scope.prodotti.$resolved = true;
      })
    }

    $scope.apriEsportaXlsProdottiKite = function () {
      PRODOTTO.excelProdottiKite(function (response) {
        modalExportFileKite.show();
      })
    }

    $scope.chiudiEsportaXlsProdottiKite = function () {
      modalExportFileKite.hide();
    }

    $scope.updateCategorie = function () {
      $scope.prodotto.categoria = $scope.prodotto.modello.categoria;
    }

    $scope.zoomImmagine = function (prodotto) {
      $scope.prodotto = prodotto;

      $scope.immagineProdotto =
        IMAGE.get({ id: prodotto.foto });

      modalImmagineProdotto.show();
    }

    $scope.eliminaFotoProdotto = function () {
      $scope.prodotto.foto = null;
    }

    $scope.eliminaFotoQuotataProdotto = function () {
      $scope.prodotto.fotoQuotata = null;
    }

    $scope.eliminaDisegnoProdotto = function () {
      $scope.prodotto.disegno = null;
    }

    $scope.eliminaFotometriaProdotto = function () {
      $scope.prodotto.fotometria = null;
    }

    // $scope.calcolaClasseEnergetica = function ()
    // {
    //     if ($scope.prodotto)
    //       $scope.prodotto.classeEnergetica=null

    //     //ATTENZIONE : questa funzione esiste sia su client che su server perchè il calcolo deve essere dinamico rispetto ai dati (cambiabili) della form
    //     if ($scope.prodotto && $scope.prodotto.w && $scope.prodotto.lm)
    //     {
    //         var Pcor = $scope.prodotto.w;
    //         // if ($scope.prodotto.luci>1)
    //             // x=$scope.prodotto.w * $scope.prodotto.luci;

    //         var Pref = 0.88 * Math.sqrt($scope.prodotto.lm) + 0.049 * ($scope.prodotto.lm)

    //         var classe = Pcor / Pref

    //         if (!$scope.prodotto.fascio || isNaN($scope.prodotto.fascio) || parseInt($scope.prodotto.fascio) > 60)
    //         {
    //             if (classe > 0 && classe < 0.13)
    //                 $scope.prodotto.classeEnergetica = 'A2'
    //             else if (classe >= 0.13 && classe < 0.18)
    //                 $scope.prodotto.classeEnergetica = 'A1'
    //             else if (classe >= 0.18)
    //                 $scope.prodotto.classeEnergetica = 'A0'
    //         }
    //         else
    //         {
    //             if (classe > 0 && classe < 0.11)
    //                 $scope.prodotto.classeEnergetica = 'A2'
    //             else if (classe >= 0.11 && classe < 0.17)
    //                 $scope.prodotto.classeEnergetica = 'A1'
    //             else if (classe >= 0.17)
    //                 $scope.prodotto.classeEnergetica = 'A0'
    //         }
    //     }
    // }

    $scope.manageCriptato = function () {
      if ($scope.prodotto.criptato == true) {
        $scope.prodotto.exportMetel = false;
        $scope.prodotto.pubblicato = false;
      }
    }

    $scope.apriModaleComponente = function (componente) {
      if ($scope.prodotto._id === componente) return;

      $rootScope.componenteReadOnly = true;

      modalComponente.show();

      var query = {
        id: componente._id
      }
      if (typeof componente === 'string') {
        query = {
          id: componente
        }
      }

      COMPONENTE.get(query,
        function (response) {
          $scope.componente = response;
        },
        function (err) {
          //ngNotify.set(CONFIG.messages.componente_delete_error, 'error');
        }
      )
    }

    $scope.$watch('tabs.activeTab', function (tab) {
      if (tab && tab == K_TITLETAB_EAN)
        $scope.getCodiceEan()
    })

    // $scope.$watchGroup(['prodotto.w', 'prodotto.lm', 'prodotto.fascio'], function()
    // {
    //     $scope.calcolaClasseEnergetica();
    // })

    $scope.getCodiceEan = function () {
      PRODOTTO.getEanByidCodice(
        {
          idCodice: $scope.prodotto._id
        },
        function (res) {
          if (res) {
            $scope.codiceEan = res
            $scope.prodotto.codiceEan = $scope.codiceEan.codiceEan
          }
        }
      )
    }

    STATOCREAZIONEDISTINTE.last({},
      function (stato) {
        $rootScope.statoDistinte = stato;
      })
  }]);
