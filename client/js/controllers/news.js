app.controller('news', [
    '$scope',
    '$modal',
    'ngNotify',
    'CONFIG',
    'NEWS',
    'PING', 
    '$interval', 
    '$rootScope',
    '$http',
    'Upload',
    function ($scope,$modal,ngNotify,CONFIG,NEWS,PING,$interval,$rootScope, $http, Upload) {

    //Salva il log in DB
    $rootScope.dwhLog("news");

    //effettua un ping verso il server, ogni x millisencondi, per capire se esiste la connessione
    var _ping = function(){
        PING.query(
        function (response) {},
        function (err) {
             ngNotify.set(CONFIG.messages.session_error, {
                type: 'error',
                sticky: true
             });
        }); 
    }
    $interval(_ping, CONFIG.timeToPing); 

    $scope.editorOptions = {
        focus: true,
        toolbar: [
            ['style', ['bold', 'italic']]
        ],
        height: 100
    };

    $scope.tabs = [
        {
            title: "Info generali",
            template: '/html/includes/news-tab-principale.html',
            activeTab: true
        }
    ];

    $scope.modalNews = $modal(
    {
        scope: $scope,
        title: 'Nuova news',
        templateUrl: 'html/modals/news.html',
        show: false,
        id: 'modal-news',
        backdrop: 'static',
        keyboard: false
    });


    $scope.nuovaNews = function () 
    { 
        $scope.singleNews = {}
        $scope.modalNews.show();
    }

    $scope.modifica = function (obj) 
    {   
        NEWS.get({ id:obj._id }, function (res)
        {
            $scope.modalNews.show();
            $scope.singleNews = _.clone(res);
        }, function(err)
        {
            console.log(err)
        })
    } 

    $scope.salva = function (singleNews) 
    { 
        if(!singleNews._id) 
        {
            NEWS.save(singleNews,
            function (response) 
            {
                ngNotify.set(CONFIG.messages.news_saved, 'success');
                $scope.modalNews.hide();
                $scope.news = NEWS.last();
            },
            function (err) {
                ngNotify.set(CONFIG.messages.news_save_error, 'error');
            });
        }
        else 
        {
            NEWS.update({
                id: singleNews._id
            }, singleNews,
            function (response) 
            {
                ngNotify.set(CONFIG.messages.news_updated, 'success');
                $scope.modalNews.hide();
                $scope.news = NEWS.last();
            },
            function (err) 
            {
                ngNotify.set(CONFIG.messages.news_update_error, 'error');
            });
        }
    }

    /* Elimina prodotto */
    $scope.eliminaNews = function (obj)
    {
        bootbox.confirm({
            title: CONFIG.messages.news_delete_alert_title,
            message: CONFIG.messages.news_delete_alert_message,
            buttons: {
                'cancel': {
                    label: CONFIG.messages.alert_delete_undo,
                    className: 'btn-default'
                },
                'confirm': {
                    label: CONFIG.messages.alert_delete_confirm,
                    className: 'btn-primary'
                }
            },
            callback: function (ok) {
                if (ok) {
                    NEWS.delete({
                            id: obj._id
                    },
                    function (response) {
                        $scope.modalNews.hide();
                        ngNotify.set(CONFIG.messages.news_deleted, 'success');
                        
                        $scope.news = NEWS.last();
                    },
                    function (err) {
                        ngNotify.set(CONFIG.messages.news_delete_error, 'error');
                    });
                }
            }
        });
    }

    
    $scope.cercaNews = function ()
    {
        if ($scope.search && $scope.search != '') {
            if ($scope.search.length < 3) {
                ngNotify.set(CONFIG.messages.search_min_length, 'warn');
            } else {
                $scope.news = NEWS.cerca(
                {
                    search: $scope.search
                }, function () {
                     
                });
            }
        } else {
            $scope.news = NEWS.last();
            $scope.resetSearch();
        }
    }

    $scope.resetSearch = function ()
    {
        $scope.searchresults = false;
        $scope.search = '';
    }

    // carica file modello
    $scope.caricaFileProdotto = function (file, tipo) {
        var filetype = file.type !== "" ? file.type : 'application/octet-stream', // mi serve per il signed url
            percorso = $scope.singleNews._id + '/' + tipo + '/' + file.name;
 
            $scope.uploadingfilePdf = true;

        $http
            ({
                url: '/s3',
                data: {
                    percorso: percorso,
                    tipo: filetype,
                    dimensione: file.size
                },
                method: 'POST',
                skipAuthorization: true // non passo dal autorizzazione con token, s3 si incazza
            })
            .success(function (url) {
                // ho un signed url per l'upload su amazon
                file.upload = Upload.http({
                    url: url,
                    data: file,
                    method: 'PUT',
                    skipAuthorization: true, // non passo dal autorizzazione con token di auth0, s3 si incazza
                    headers: {
                        'Content-Type': filetype
                    }
                });

                file.upload.then(function (response) {
                    if (response.status === 200) {

                        // file caricato, aggiorno il prodotto
                        $scope.singleNews['file_' + tipo] = {};
                        $scope.singleNews['file_' + tipo].url = response.config.url.split('?')[0]; // salvo l'url diretto di s3
                        $scope.singleNews['file_' + tipo].nome = file.name;
                         
                        $scope.uploadingfilePdf = false; 
                        
                    }
                });
            });
    }

    $scope.rimuoviFile = function (tipo)
    {
        $scope.singleNews.file_pdf = null;
        $scope.singleNews['file_' + tipo] = null;
    } 
    
    $scope.news = NEWS.last();
}]);
