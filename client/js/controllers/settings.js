app.controller('settings',[
    '$scope',
    '$rootScope',
    'CONFIG',
    'SETTINGS',
    'ngNotify', 
    'PING', '$interval',
    function ($scope,$rootScope,CONFIG,SETTINGS,ngNotify,PING,$interval) {
    
    //Salva il log in DB
    $rootScope.dwhLog("Settings");

    var _ping = function(){
            PING.query(
            function (response) {},
            function (err) {
                 ngNotify.set(CONFIG.messages.session_error, {
                    type: 'error',
                    sticky: true
                 });
            }); 
        } 
    $interval(_ping, CONFIG.timeToPing);

	$scope.editorOptions = {
	    focus: true,
	    toolbar: [
	            ['style', ['bold', 'italic']]
	        ]
  	};
        
    $rootScope.impostazioni = SETTINGS.get();
        
    $scope.salvaImpostazioni = function (){
        SETTINGS.save($scope.impostazioni,
				function (response) { 
                    ngNotify.set(CONFIG.messages.settings_save, 'success');
                    $rootScope.impostazioni = response;
				},
				function (err){ 
					ngNotify.set(CONFIG.messages.settings_save_error, 'error');
				}
        );
    }
      
}]);
