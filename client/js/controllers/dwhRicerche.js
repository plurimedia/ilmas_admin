app.controller(
    'dwhRicerche', [
    '$scope',
    '$rootScope',
    'CONFIG',
    'DWHRICERCA','ngNotify', 'PING', '$interval',
    function ($scope, $rootScope, CONFIG, DWHRICERCA, ngNotify, PING, $interval) {

        //Salva il log in DB
        $rootScope.dwhLog("DwhRicerche");

    	var _ping = function()
        {
            PING.query(
            function (response) {},
            function (err) {
                 ngNotify.set(CONFIG.messages.session_error, {
                    type: 'error',
                    sticky: true
                 });
            }); 
        } 
        $interval(_ping, CONFIG.timeToPing);   

        if (!$scope.annoReport)
            $scope.annoReport = new Date().getFullYear().toString();;

        $scope.elencoAnni = $rootScope.elencoAnni(); 

        $scope.eseguiReport = function(){

            DWHRICERCA.dwhRicerche(
                {anno:$scope.annoReport}, 
                function (dwhRicerche) 
                { 
                    $scope.ricerche = dwhRicerche.result;
                    $scope.totaleRicerche = dwhRicerche.totaleRicerche;  
                }
            );
        }

       $scope.eseguiReport(); 
  
}]);
