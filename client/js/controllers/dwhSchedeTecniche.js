app.controller(
    'dwhSchedeTecniche', [
    '$scope',
    '$rootScope',
    'CONFIG',
    'DWHSCHEDATECNICA','ngNotify', 'PING', '$interval',
    function ($scope, $rootScope, CONFIG, DWHSCHEDATECNICA, ngNotify, PING, $interval) {

        //Salva il log in DB
        $rootScope.dwhLog("DwhSchedeTecniche");

    	var _ping = function()
        {
            PING.query(
            function (response) {},
            function (err) {
                 ngNotify.set(CONFIG.messages.session_error, {
                    type: 'error',
                    sticky: true
                 });
            }); 
        } 
        $interval(_ping, CONFIG.timeToPing);   

        if (!$scope.annoReport)
            $scope.annoReport = new Date().getFullYear().toString();;

        $scope.elencoAnni = $rootScope.elencoAnni();

        $scope.eseguiReport = function(){

            DWHSCHEDATECNICA.dwhSchedeTecniche(
                {anno:$scope.annoReport}, 
                function (dwhSchedeTecniche) 
                { 
                    $scope.ricerche = dwhSchedeTecniche.result;
                    $scope.totaleRicerche = dwhSchedeTecniche.totaleRicerche;  
                }
            );
        }

       $scope.eseguiReport(); 
  
}]);
