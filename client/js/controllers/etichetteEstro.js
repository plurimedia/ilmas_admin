app.controller(
    'etichetteEstro', [
    '$scope',
    '$rootScope',
    '$http',
    '$window',
    '$modal',
    'CONFIG',
    'PRODOTTO',
    'PDF',
    'DWHETICHETTA',
    'COLORI',
    'ngNotify', 'PING', '$interval',
    function ($scope, $rootScope, $http, $window, $modal, CONFIG, PRODOTTO, PDF, DWHETICHETTA, COLORI, ngNotify, PING, $interval) {
        //Salva il log in DB
        $rootScope.dwhLog("EtichetteEstro");
        $scope.prodotto = {}
        $scope.libera = {}
        $scope.mono_bi = ''
        $scope.anteprima = false

        var _ping = function () {
            PING.query(
                function (response) { },
                function (err) {
                    ngNotify.set(CONFIG.messages.session_error, {
                        type: 'error',
                        sticky: true
                    });
                });
        }

        $interval(_ping, CONFIG.timeToPing);

        $scope.dataEtichetta = new Date();
        $scope.dataEtichettaModificata = false;
        if ($rootScope.user)
            $scope.operatoreEtichetta = $rootScope.user.user_metadata.sigla;

        $scope.$watch(function () {
            return $scope.dataEtichetta;
        }, function (newDate) {

            var dateN = getMeseAnno(newDate);
            var dateO = getMeseAnno(new Date());

            if (dateN != dateO)
                $scope.dataEtichettaModificata = true;
            else
                $scope.dataEtichettaModificata = null;

        }, true);

        function getMeseAnno(date) {
            if (date) {
                var mese = date.getMonth();
                var anno = date.getFullYear();
                return mese + "" + anno;
            }
        }

        //autocomplete componente binario
        $scope.cercaBinari = function (search) {
            var query = { 'categoria.tipo': 'profili', serie: 'aComponenti', search: search }

            return PRODOTTO.cerca(query).$promise;
        }

        //autocomplete componente luminoso
        $scope.cercaFontiLuminose = function (search) {
            var query = { 'categoria.tipo': 'fonti_luminose', serie: 'aComponenti', search: search }

            return PRODOTTO.cerca(query).$promise;
        }

        //autocomplete finiture
        $scope.cercaColori = function (search) {
            var query = { 'categoria.tipo': 'fonti_luminose', serie: 'aComponenti', search: search }

            return COLORI.cerca(query).$promise;
        }

        $scope.sceltaComponenteBin = function (value) {
            $scope.prodotto.st_codice_binario = value.codice;
            $scope.st_id = value._id
        };

        $scope.sceltaComponenteLum = function (value) {
            $scope.prodotto.st_codice_fonteluminosa = value.codice;
            $scope.st_id = value._id
        };

        $scope.sceltaColore = function (value) {
            $scope.prodotto.st_finitura = value.codice;
        };

        $scope.etichettaEstroLazy = function (en, grigia) {
            var tipoScheda = "etichettaEstroLibera"
            $scope.pdfLazy = true

            var d = $scope.dataEtichetta
            var mese = d.getMonth()+1
            var anno = d.getFullYear()

            var params = {
                tipopdf: tipoScheda,
                anteprima: true,
            },
            data = { 
                prodotto: $scope.libera,
                dataEtichettaModificata: $scope.dataEtichettaModificata, 
                dataEtichetta: mese + '' + anno, 
                operatore: $scope.operatoreEtichetta,
            }

            // in inglese ?
            if (en)
                params.en = true;

            $scope.pdfLazy = PDF.save(params, data, function (response) {
                //salva il record in DWH etichetta
                var dwhEtichetta = {
                    mdate: new Date(),
                    tipo: "etichetta_estro"
                };

                DWHETICHETTA.save(dwhEtichetta,
                    function (etichetta) {
                        $scope.pdfLazy = false;
                        $window.open(response.file, '_blank'); // apri il pdf in una nuova finestra
                    },
                    function (err) {
                        ngNotify.set(CONFIG.messages.prodotto_save_error, 'error');
                    }
                );
            })
        }

        $scope.etichettaEstro = function (en) {
            var codice1 = $scope.codiceBinario(),
                codice2 = $scope.codiceFonteLuminosa(),
                lunghezza = $scope.lunghezza(),
                finitura = $scope.codiceFinitura(),
                tipoScheda = "etichettaEstroGrigia"

            var d = $scope.dataEtichetta
            var mese = d.getMonth()+1
            var anno = d.getFullYear()

            if (mese <10 )
                mese = "0"+mese

            if (codice1) {
                $scope.pdf = true
                PRODOTTO.codice({ codice: codice1 }, function (prodotto1) {
                        PRODOTTO.codice({ codice: codice2 }, function (prodotto2) {
                            COLORI.codice({ codice: finitura }, function (colore) {
                                var params = {
                                    tipopdf: tipoScheda,
                                    anteprima: true,
                                },
                                data = { 
                                    profilo: prodotto1, 
                                    fonte: prodotto2, 
                                    lunghezza: lunghezza, 
                                    finitura: colore, 
                                    codice_finito: $scope.codiceFinito, 
                                    dataEtichettaModificata: $scope.dataEtichettaModificata, 
                                    dataEtichetta: mese + '' + anno, 
                                    operatore: $scope.operatoreEtichetta,
                                    mono_bi: $scope.mono_bi,
                                    cover: CONFIG.estroClassificazioneBinari[codice2.substr(0, 1)]
                                }
            
                                // in inglese ?
                                if (en)
                                    params.en = true;

                                $scope.pdf = PDF.save(params, data, function (response) {
                                    //salva il record in DWH etichetta
                                    var dwhEtichetta = {
                                        mdate: new Date(),
                                        tipo: "etichetta_estro"
                                    };

                                    DWHETICHETTA.save(dwhEtichetta,
                                        function (etichetta) {
                                            $scope.pdf = false;
                                            $window.open(response.file, '_blank'); // apri il pdf in una nuova finestra
                                        },
                                        function (err) {
                                            ngNotify.set(CONFIG.messages.prodotto_save_error, 'error');
                                        }
                                    );
                                })
                
                            }, function(err) { console.log('Errore ricerca finitura '+finitura, err.data.error) })
                        }, function(err) { console.log('Errore ricerca fonte luminosa '+codice2, err.data.error) })
                }, function(err) { console.log('Errore ricerca profilo '+codice1, err.data.error) })
            }
        }

        $scope.creaAnteprima = function () {
            const codice2 = $scope.codiceFonteLuminosa()

            if (codice2) {
                PRODOTTO.codice({ codice: codice2 }, function (prodotto2) {
                    $scope.cover = CONFIG.estroClassificazioneBinari[codice2.substr(0, 1)].modello
                    $scope.w = prodotto2.w
                    $scope.k = prodotto2.k
                    $scope.lm = prodotto2.lm
                    $scope.irc = prodotto2.irc
                    $scope.ip = prodotto2.ip
                    $scope.anteprima = true
                }, function(err) { console.log('Errore ricerca fonte luminosa '+codice2, err.data.error) })
            }

        }

        $scope.invalidaAnteprima = function () {
            $scope.cover = ''
            $scope.w = ''
            $scope.k = ''
            $scope.lm = ''
            $scope.irc = ''
            $scope.ip = ''
            $scope.anteprima = false
        }
        $scope.invalidaAnteprima()

        $scope.codiceBinario = function () {
            if ($scope.codiceFinito && $scope.codiceFinito.length > 12) {
                if ($scope.codiceFinito.length === 13) return $scope.codiceFinito.substr(0, 2)
                else if ($scope.codiceFinito.length === 14) return $scope.codiceFinito.substr(0, 3)
            } else return ''
        }

        $scope.codiceFonteLuminosa = function () {
            if ($scope.codiceFinito && $scope.codiceFinito.length > 12) {
                if ($scope.codiceFinito.length === 13) return $scope.codiceFinito.substr(2, 4)
                else if ($scope.codiceFinito.length === 14) return $scope.codiceFinito.substr(3, 4)
            } else return ''
        }

        $scope.lunghezza = function () {
            if ($scope.codiceFinito && $scope.codiceFinito.length > 12) {
                if ($scope.codiceFinito.length === 13) return $scope.codiceFinito.substr(6, 5)
                else if ($scope.codiceFinito.length === 14) return $scope.codiceFinito.substr(7, 5)
            } else return ''
        }

        $scope.codiceFinitura = function () {
            if ($scope.codiceFinito && $scope.codiceFinito.length > 12) {
                if ($scope.codiceFinito.length === 13) return $scope.codiceFinito.substr(11, 2)
                else if ($scope.codiceFinito.length === 14) return $scope.codiceFinito.substr(12, 2)
            } else return ''
        }

    }]);
