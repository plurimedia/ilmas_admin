app.controller('lineaCommercialeAssociazioni', [
    '$scope',  
    '$modal',
    'CONFIG',
    'LINEACOMMERCIALE', 
    'ngNotify',
    '$routeParams',
    'MODELLO',
    'CATEGORIA',
    'PRODOTTO',
    'PING', '$interval','$rootScope',
    function (
            $scope, 
            $modal,
            CONFIG,
            LINEACOMMERCIALE,
            ngNotify,
            $routeParams,
            MODELLO,
            CATEGORIA,
            PRODOTTO,
            PING,$interval,$rootScope) {
        
    //Salva il log in DB
    $rootScope.dwhLog("LineaCommercialeAssociazioni");

    var _ping = function(){
        PING.query(
        function (response) {},
        function (err) {
             ngNotify.set(CONFIG.messages.session_error, {
                type: 'error',
                sticky: true
             });
        }); 
    } 
    $interval(_ping, CONFIG.timeToPing);

    $scope.modelli = MODELLO.list();
    $scope.categorie = CATEGORIA.list();

    var modalAssociazione = $modal({
        scope: $scope,
        title: 'Associa Linea Commerciale',
        templateUrl: 'html/modals/lineaCommercialeAssociazione.html',
        show: false
    });

    $scope.modalModello = $modal({
        scope: $scope,
        title: 'Modello',
        templateUrl: 'html/modals/modelloReadOnly.html',
        show: false,
        id: 'modal-modello'
    });

    $scope.modalCategoria = $modal({
        scope: $scope,
        title: 'Categoria',
        templateUrl: 'html/modals/categoriaReadOnly.html',
        show: false,
        id: 'modal-categoria'
    });

    $scope.modalProdotto = $modal({
        scope: $scope,
        title: 'Prodotto',
        templateUrl: 'html/modals/prodottoReadOnly.html',
        show: false,
        id: 'modal-prodotto'
    });

    $scope.apriModaleModello = function (_idModello) {
        $scope.modalModello.show();
        MODELLO.get({id:_idModello}, function (modello) { 
             $scope.modello = _.clone(modello);
        }, function(err){
            console.log(err)
        });
    } 

     $scope.apriModaleCategoria = function (_idCategoria)
     {
        $scope.modalCategoria.show();
        CATEGORIA.get({id:_idCategoria}, function (categoria) { 
             $scope.categoria = _.clone(categoria);
        }, function(err){
            console.log(err)
        });
    } 

     $scope.apriModaleProdotto = function (_idProdotto) 
     {
        $scope.modalProdotto.show();
        PRODOTTO.get({id:_idProdotto}, function (prodotto) { 
             $scope.prodotto = _.clone(prodotto);
        }, function(err){
            console.log(err)
        }); 

    } 

    LINEACOMMERCIALE.get({id:$routeParams.id}, 
        function (lineaCommerciale)
        {
            $scope.lineaCommerciale = lineaCommerciale;
            $scope.initiElementiDaEliminare();
        },
        function(err){
            console.log(err)
        }
    ); 

    $scope.tabs = [
        {
            title: "Categorie",
            template: '/html/includes/lineaCommercialeAssociazione-tab-categoria.html',
            activeTab: true
        },
        {
            title: "Modelli",
            template: '/html/includes/lineaCommercialeAssociazione-tab-modello.html'
        },
        {
            title: "Prodotti",
            template: '/html/includes/lineaCommercialeAssociazione-tab-prodotto.html'
        }
    ];

  

    $scope.apriAssociazione = function (lineaCommerciale) {
        modalAssociazione.show();
        $scope.modelli = MODELLO.list();
        $scope.categorie = CATEGORIA.list();
        $scope.codici = [];
        //$scope.batch = {tipo:immagine.tipo, foto: immagine.id};
        $scope.batch = {};
    }

    $scope.cambiaAssocia = function (cosa) {
        $scope.batch.cosa = cosa;
        $scope.batch.codici = [];
    }

    $scope.sceltaCategoria = function (categoria) {
        $scope.batch.codici = [categoria._id];
    }

    $scope.sceltaModello = function (value) {
         $scope.batch.codici = [];
         if(value)
            $scope.batch.codici.push(value._id) 
    }

     // cerca codici per associare immagini
    $scope.cercaCodici = function (chiave) {
        if (chiave && chiave.length >= 3)
            return PRODOTTO.codiceValido({
                nome: chiave
            }).$promise;
    }

    // seleziona tutti in cima al typeahead
    $scope.selezionaTutti = function (results) {
        $scope.batch.codici = _.unique(_.union($scope.batch.codici,_.pluck(_.pluck(results,'value'),'_id')));
        $scope.codici =  _.unique(_.union($scope.codici,_.pluck(results,'value')), function(v){return v._id;});
    }

    // selezione codice
    $scope.$on('$typeahead.select', function (event, value, index, elem) { 
        if($scope.batch.codici.indexOf(value._id)===-1)
            $scope.batch.codici.push(value._id);
        if(!_.find($scope.codici,function(c){return c._id === value._id}))
            $scope.codici.push(value);
        
        $scope.$apply();
    });

    $scope.associaLinea = function (){
        $scope.associando = true;
        $scope.batch.lineaCommerciale_id = $scope.lineaCommerciale._id
       
        LINEACOMMERCIALE.creaAssociazione($scope.batch,
            function (resp) 
            {
                
                ngNotify.set(CONFIG.messages.lineaCommerciale_updated, 'success');
                modalAssociazione.hide(); 


                LINEACOMMERCIALE.get({id:$routeParams.id}, 
                    function (lineaCommerciale)
                    {
                        $scope.associando = false
                        $scope.lineaCommerciale = lineaCommerciale;
                    },
                    function(err){
                        console.log(err)
                    }
                ); 

            },
            function (err) 
            {
                if (err){
                    ngNotify.set(CONFIG.messages.lineaCommerciale_updated_error, 'error');
                } 
            }
        ); 
     } 


     $scope.associaLinea_Massiva = function (){
        
        LINEACOMMERCIALE.creaAssociazioneMassiva($scope.batch,
            function (resp) 
            {
                
                ngNotify.set(CONFIG.messages.lineaCommerciale_updated, 'success');
                modalAssociazione.hide(); 


                LINEACOMMERCIALE.get({id:$routeParams.id}, 
                    function (lineaCommerciale)
                    {
                        $scope.associando = false
                        $scope.lineaCommerciale = lineaCommerciale;
                    },
                    function(err){
                        console.log(err)
                    }
                ); 

            },
            function (err) 
            {
                if (err){
                    ngNotify.set(CONFIG.messages.lineaCommerciale_updated_error, 'error');
                } 
            }
        ); 
     } 

     $scope.eliminaAssociazioneCategoria = function (index, _idCategoria) { 
        bootbox.confirm({
                title: CONFIG.messages.lineaCommercialeAss_delete_alert_title,
                message: CONFIG.messages.lineaCommercialeAss_delete_alert_message,
                buttons: {
                    'cancel': {
                        label: CONFIG.messages.alert_delete_undo,
                        className: 'btn-default'
                    },
                    'confirm': {
                        label: CONFIG.messages.alert_delete_confirm,
                        className: 'btn-primary'
                    }
                },
                callback: function (ok) {
                    if (ok) {
                        $scope.lineaCommerciale.categorieAssociate.splice(index,1);
                        $scope.categorieDaEliminare.push(_idCategoria);
                        $scope.eliminaAssociazioni();
                   }
                }
            }
        );
     }
 
     $scope.eliminaAssociazioneModello = function (index, _idModello) { 
        bootbox.confirm({
                title: CONFIG.messages.lineaCommercialeAss_delete_alert_title,
                message: CONFIG.messages.lineaCommercialeAss_delete_alert_message,
                buttons: {
                    'cancel': {
                        label: CONFIG.messages.alert_delete_undo,
                        className: 'btn-default'
                    },
                    'confirm': {
                        label: CONFIG.messages.alert_delete_confirm,
                        className: 'btn-primary'
                    }
                },
                callback: function (ok) {
                    if (ok) {
                        $scope.lineaCommerciale.modelliAssociati.splice(index,1);
                        $scope.modelliDaEliminare.push(_idModello);
                        $scope.eliminaAssociazioni();
                   }
                }
            }
        );
     }

    
    $scope.eliminaAssociazioneProdotto = function (index, _idProdotto) { 
        bootbox.confirm({
                title: CONFIG.messages.lineaCommercialeAss_delete_alert_title,
                message: CONFIG.messages.lineaCommercialeAss_delete_alert_message,
                buttons: {
                    'cancel': {
                        label: CONFIG.messages.alert_delete_undo,
                        className: 'btn-default'
                    },
                    'confirm': {
                        label: CONFIG.messages.alert_delete_confirm,
                        className: 'btn-primary'
                    }
                },
                callback: function (ok) {
                    if (ok) {
                        $scope.prodotti=[];
                        $scope.lineaCommerciale.prodottiAssociati.splice(index,1);
                        $scope.prodottiDaEliminare.push(_idProdotto); 
                        $scope.eliminaAssociazioni();
                   }
                }
            }
        );
     }

    $scope.eliminaAssociazioni = function (){ 

        $scope.associando = true; 
        var param = {};
        param.categorieDaEliminare  = $scope.categorieDaEliminare;
        param.modelliDaEliminare    = $scope.modelliDaEliminare;
        param.prodottiDaEliminare   = $scope.prodottiDaEliminare;
        param.lineaCommerciale_id   = $scope.lineaCommerciale._id;
       

       LINEACOMMERCIALE.eliminaAssociazione(param, 
        function (lineaCommerciale)
        {
            
            LINEACOMMERCIALE.get({id:$routeParams.id}, 
                function (lineaCommerciale)
                {
                    $scope.associando = false
                    $scope.lineaCommerciale = lineaCommerciale; 
                    $scope.initiElementiDaEliminare();
                },
                function(err){
                    console.log(err)
                }
            );  

        },
        function(err){
            console.log(err)
        }
        );  
    } 

    $scope.initiElementiDaEliminare = function (){ 
        $scope.categorieDaEliminare =[];
        $scope.modelliDaEliminare   =[];
        $scope.prodottiDaEliminare  =[];
    }

    $scope.annullaAssociazione = function (){ 
        LINEACOMMERCIALE.get({id:$routeParams.id}, 
                function (lineaCommerciale)
                {
                    $scope.associando = false
                    $scope.lineaCommerciale = lineaCommerciale; 
                    $scope.initiElementiDaEliminare();
                },
                function(err){
                    console.log(err)
                }
            );  
    }

     $scope.cercaProdottoLineeCommerciali = function (codice) 
     { 

        if (codice && codice != '')
        {
            if (codice.length < 3)
            {
                ngNotify.set(CONFIG.messages.search_min_length, 'warn');
            } 
            else 
            {
                 
                $scope.prodotti = 
                    LINEACOMMERCIALE.cercaProdotto(
                    {
                        idLinea : $scope.lineaCommerciale._id,
                        codiceProdotto: codice
                    },
                    function () {
                    $scope.ricercaEffettuata = true;
                    }
                )
            }
        }
    }

      

    
     
 
}]);
