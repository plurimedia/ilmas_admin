app.controller('codiciEan', [
    '$scope',
    '$rootScope',
    '$http',
    '$window',
    '$modal',
    'Upload',
    'IMAGE',
    'CONFIG',
    '$routeParams',
    'PRODOTTO',
    'MODELLO',
    'CATEGORIA',
    'SOTTOCATEGORIE',
    'CSVPRODOTTI',
    'PDF',
    'ngNotify',
    'COMPONENTE','PING', '$interval' ,
    function ($scope, $rootScope, $http, $window, $modal, Upload, IMAGE, CONFIG,$routeParams,
     PRODOTTO, MODELLO, CATEGORIA, SOTTOCATEGORIE, CSVPRODOTTI, PDF, ngNotify, COMPONENTE,PING,$interval) {

 

        //Salva il log in DB
        $rootScope.dwhLog("CodiciEan");

        var _ping = function(){
            PING.query(
            function (response) {},
            function (err) {
                 ngNotify.set(CONFIG.messages.session_error, {
                    type: 'error',
                    sticky: true
                 });
            });
        }
        $interval(_ping, CONFIG.timeToPing);

        var modalProdotto = $modal({
            scope: $scope,
            title: 'Nuovo prodotto',
            templateUrl: 'html/modals/codiceEan.html',
            show: false,
            id: 'modal-codiceEan'
        })

        $scope.tabs = [
            {
                title: "Bar codes - EAN 13",
                template: '/html/includes/prodotto-tab-ean13.html'
            } 
        ];


        $scope.lastCodici=true
        $scope.prodotti = PRODOTTO.codiciEan()
        console.log("$scope.prodotti ", $scope.prodotti.length)


          /* cerca prodotti */
        $scope.cercaProdotti = function ()
        {
            if ($scope.search && $scope.search != '') {
                  if ($scope.search.length < 3) {
                      ngNotify.set(CONFIG.messages.search_min_length, 'warn');
                  } else {
                      $scope.prodotti = PRODOTTO.codiciEan(
                      {
                          search: $scope.search
                      }, function () {
                        $scope.searchresults = $scope.search;
                          $scope.lastCodici = false
                      });
                  }
            }
            else {
                $scope.prodotti = PRODOTTO.codiciEan();
                $scope.resetSearch();
            }
        }

        $scope.apriDettaglioProdotto = function (prodotto)
        {
            $scope.prodotto = prodotto
            modalProdotto.show();
        }
        
        
}]);
 