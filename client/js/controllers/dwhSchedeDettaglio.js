app.controller(
    'dwhSchedeDettaglio', [
    '$scope',
    '$rootScope',
    'CONFIG',
    'DWHSCHEDADETTAGLIO','ngNotify', 'PING', '$interval',
    function ($scope, $rootScope, CONFIG, DWHSCHEDADETTAGLIO, ngNotify, PING, $interval) {

        //Salva il log in DB
        $rootScope.dwhLog("DwhDettaglio");

    	var _ping = function()
        {
            PING.query(
            function (response) {},
            function (err) {
                 ngNotify.set(CONFIG.messages.session_error, {
                    type: 'error',
                    sticky: true
                 });
            }); 
        } 
        $interval(_ping, CONFIG.timeToPing);   

        if (!$scope.annoReport)
            $scope.annoReport = new Date().getFullYear().toString();;

        $scope.elencoAnni = $rootScope.elencoAnni();

        $scope.eseguiReport = function()
        {
            DWHSCHEDADETTAGLIO.dwhSchedeDettaglio(
                {anno:$scope.annoReport}, 
                function (dwhSchedeDettaglio) 
                { 
                    $scope.ricerche = dwhSchedeDettaglio.result;
                    $scope.totaleRicerche = dwhSchedeDettaglio.totaleRicerche;  
                }
            );
        }

       $scope.eseguiReport(); 
  
}]);
