app.controller('contatti', [
    '$scope',
    '$rootScope',
    '$http',
    '$location',
    '$window',
    '$modal',
    'CSVGMAIL',
    'CONTATTO',
    'COMUNI',
    'NAZIONI',
    'TAGS',
    'CONFIG',
    'ngNotify',
    'PING','$interval',
    function ($scope, $rootScope, $http, $location,$window, $modal, CSVGMAIL, CONTATTO, COMUNI, NAZIONI, TAGS, CONFIG, ngNotify,PING,$interval) {

        //Salva il log in DB
        $rootScope.dwhLog("Contatti");

        var _ping = function()
        {
            PING.query(
            function (response) {},
            function (err) 
            {
                 ngNotify.set(CONFIG.messages.session_error, 
                 {
                    type: 'error',
                    sticky: true
                 });
            }); 
        }

        $interval(_ping, CONFIG.timeToPing); 

        var modalContatto = $modal(
        {
            scope: $scope,
            title: 'Nuovo contatto',
            templateUrl: 'html/modals/contatto.html',
            show: false,
            keyboard:false
        }),

        _encodeTags = function (tags) 
        {
            return _.pluck(tags, 'text');
        };

        /* se arrivo da una richiesta di completamento, apri la modale di quel contatto */
        if ($location.search().completa) 
        {
            CONTATTO.get({
                id: $location.search().completa
            }, 
            function (data) 
            {
                $scope.apriModaleContatto(data)
            })
        }

        $scope.nazioni = NAZIONI.query();
        $scope.contatti = CONTATTO.ultimi();

        // autocomplete comuni
        $scope.cercaComune = function (chiave) 
        {
            return COMUNI.cerca({
                nome: chiave
            }).$promise;
        }

        // autocomplete aziende
        $scope.cercaAzienda = function (chiave) 
        {
            return CONTATTO.aziende({
                nome: chiave
            }).$promise;
        }

        // selezione typeahead comune
        $scope.sceltaComune = function (value) 
        {
            $('#contattoCap').focus(); // passa al cap
            $scope.contatto.comune = value.nome;
            $scope.contatto.provincia = value.provincia;
            $scope.contatto.cap = value.cap;
            $scope.contatto.nazione = 'Italia';
        };

        // selezione typeahead azienda
        $scope.sceltaAzienda = function (value) 
        {
            $scope.contatto.azienda = value.nome;
        };

        // cerca tags
        $scope.cercaTags = function (chiave) 
        {
            return TAGS.query(
            {
                nome: chiave
            }).$promise;
        }

        $scope.apriModaleContatto = function (contatto) {
            modalContatto.show();
            if (!contatto)
                $scope.contatto = {
                    nazione: 'Italia'
                };
            else
                $scope.contatto = _.clone(contatto); // per non interferire con la lista
        }

        $scope.chiudiModaleContatto = function (contatto) {
            $scope.duplica = false;
            $scope.contatto = false;
            modalContatto.hide();
        }

        /* salvataggio nuovo contatto */
        $scope.salvaContatto = function (form) {

            $scope.resetSearch();
            $scope.contatto.tags = _encodeTags($scope.contatto.tags); // metto i tags come semplice array di stringhe
            $scope.duplica = false

            if (!$scope.contatto._id) {
                CONTATTO.save($scope.contatto,
                    function (response) {
                        modalContatto.hide();
                        $scope.contatti = CONTATTO.ultimi()
                        ngNotify.set(CONFIG.messages.contatto_saved, 'success');
                    },
                    function (err) {
                        ngNotify.set(CONFIG.messages.contatto_save_error, 'error');
                    });
            } else {

                CONTATTO.update({
                        id: $scope.contatto._id
                    }, $scope.contatto,
                    function (response) {
                        modalContatto.hide();
                        $scope.contatti = CONTATTO.ultimi()
                        ngNotify.set(CONFIG.messages.contatto_updated, 'success');
                        // se l'ho aggiornato arrivando da un'offerta rimando all'offerta con quel contatto selezionato
                        if ($location.search().completa)
                            $location.path('/offerte').search('contatto', $location.search().completa).search('completa', null)
                    },
                    function (err) {
                        ngNotify.set(CONFIG.messages.contatto_update_error, 'error');
                    });

            }
        }

        /* Elimina contatto */

        $scope.eliminaContatto = function (id) {

            bootbox.confirm({
                title: CONFIG.messages.contatto_delete_alert_title,
                message: CONFIG.messages.contatto_delete_alert_message,
                buttons: {
                    'cancel': {
                        label: CONFIG.messages.alert_delete_undo,
                        className: 'btn-default'
                    },
                    'confirm': {
                        label: CONFIG.messages.alert_delete_confirm,
                        className: 'btn-primary'
                    }
                },
                callback: function (ok) {
                    if (ok) {

                        CONTATTO.delete({
                                id: id
                            },
                            function (response) {
                                modalContatto.hide();
                                $scope.contatti = CONTATTO.ultimi()
                                $scope.resetSearch();
                                ngNotify.set(CONFIG.messages.contatto_deleted, 'success');
                            },
                            function (err) {
                                ngNotify.set(CONFIG.messages.contatto_delete_error, 'error');
                            });
                    }
                }
            });
        }

        $scope.duplicaContatto = function (contatto) {
            var nuovocontatto = _.omit(contatto, '_id'); // duplico senza id cosi la modale lo considera come nuovo
            $scope.duplica = true;
            $scope.apriModaleContatto(nuovocontatto);
        }

        /* importa da gmail */
        $scope.uploadCsv = function (file) {
            $scope.contatti.$resolved = false;
            CSVGMAIL.upload(file, function (data) {
                var msg = data.inserted + ' contatti importati ';
                if (data.existing > 0)
                    msg += data.existing + ' già presenti ';
                if (data.rejected.count > 0) {
                    msg += data.rejected.count + ' non validi ';
                    msg += '<br /><a href="' + data.rejected.log + '"><i class="fa fa-download"></i> Scarica il csv con i contatti errati</a>';
                }
                ngNotify.set(msg, {
                    html: true,
                    type: 'success',
                    sticky: true
                });
                // rileggo i dati
                $scope.contatti = CONTATTO.ultimi();
            })
        }

        /* cerca contatti */
        $scope.cercaContatti = function () {

            if ($scope.search && $scope.search != '') {
                if ($scope.search.length < 3) {
                    ngNotify.set(CONFIG.messages.search_min_length, 'warn');
                } else {
                    $scope.contatti = CONTATTO.cerca({
                        search: $scope.search
                    }, function () {
                        $scope.searchresults = $scope.search;
                    });
                }
            } else {
                $scope.contatti = CONTATTO.ultimi();
                $scope.resetSearch();
            }
        }

        $scope.resetSearch = function () {
            $scope.searchresults = false;
            $scope.search = '';
        }


        $scope.gestioneContattiDoppi = function ()
        { 
            CONTATTO.doppi(function (response) {  
                $window.open(response.file, '_blank');
            })
        }

}]);
