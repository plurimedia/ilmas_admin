app.controller('mailingList', [
    '$scope',
    '$rootScope',
    '$http',
    '$location',
    '$modal',
    'Upload',
    'CONTATTO',
    'EMAIL',
    'MAILINGLIST',
    'TAGS',
    'CONFIG',
    'ngNotify','PING', '$interval',
     function ($scope, $rootScope, $http, $location, $modal, Upload, CONTATTO, EMAIL, MAILINGLIST, 
        TAGS, CONFIG, ngNotify,PING,$interval) {

        //Salva il log in DB
        $rootScope.dwhLog("MailingList");

        var _ping = function(){
            PING.query(
            function (response) {},
            function (err) {
                 ngNotify.set(CONFIG.messages.session_error, {
                    type: 'error',
                    sticky: true
                 });
            }); 
        } 
        $interval(_ping, CONFIG.timeToPing);

        var _encodeTags = function (tags)
            {
                return _.pluck(tags, 'text');
            } 

            _decodeTags = function (tags) 
            {  
                return _.map(tags, function (t){
                    return {text:t};
                })
            },

            modalEmail = $modal({
                scope: $scope,
                title: 'Nuova Email',
                templateUrl: 'html/modals/mailingList-mail.html',
                show: false,
                id: 'modal-mailing-email' 
            }) 

            _encodeDestinatari = function()
            {  
                $scope.email.a = _encodeTags($scope.email.a);
                $scope.email.cc = _encodeTags($scope.email.cc);
                $scope.email.ccn = _encodeTags($scope.email.ccn);
            }

            _decodeDestinatari = function()
            { 
                $scope.email.a = _decodeTags($scope.email.a);
                $scope.email.cc = _decodeTags($scope.email.cc);
                $scope.email.ccn = _decodeTags($scope.email.ccn);
            }

            _mittenteUserId = $rootScope.user.user_metadata.user_id
            _mittenteEmail  = "commerciale@ilmas.com";
            _mittenteNome   = $rootScope.user.user_metadata.nome;

            $scope.mailingList = MAILINGLIST.last();
            $scope.nazioni = CONTATTO.nazioni(); 
            $scope.province = CONTATTO.province(); 
 
            _inviaMail = function () 
            { 
                EMAIL.send(
                    {tipo: 'mailingList'},
                    {contenuto: $scope.email, email: $scope.email}, 
                    function (response) 
                    { 
                        $scope.email.email_id = response[0]._id; 

                        $scope.email.stato = 1;  
   
                        _encodeDestinatari(); 

                        MAILINGLIST.update(
                            {id:$scope.email._id},
                            $scope.email,
                            function (response) 
                            {  
                                $scope.mailingList = MAILINGLIST.last();  
                                $scope.email = {};  
                                $scope.inviando = false; 
                                modalEmail.hide();
                                ngNotify.set(CONFIG.messages.email_success, 'success'); 
                            },
                            function (err)
                            {
                                ngNotify.set(CONFIG.messages.offerta_save_error, 'error');
                                $scope.inviando = false;
                            }
                        )
                    },
                    function (err) 
                    {
                        $scope.inviando = false;
                        ngNotify.set(CONFIG.messages.email_error, 'error');
                    }
                )
            }  

            $scope.resetSearch = function () 
            {
                $scope.searchresults = false;
                $scope.search = ''; 
            } 
        
            $scope.nuovaEmail = function (email) 
            {  
                if (!email)
                { 
                    $scope.email = {};
                    $scope.files = [];  
                    $scope.nazioni.forEach(
                        function(nazione)
                        {    
                            nazione.checked=false; 
                        }
                    )  
                    $scope.province.forEach(
                        function(provincia)
                        {    
                            provincia.checked=false; 
                        }
                    ) 
                } 
                modalEmail.show();
            }

            /* cerca mail nella modale di invio*/
            $scope.cercaEmail = function (chiave) 
            {
                return CONTATTO.cercaemail({search:chiave}).$promise.then(function (r) {
                     return _decodeTags(_.pluck(r,'email')) // li metto nel formato dal tagsinput
                });
            }

            /* allega files alla mail*/
            $scope.uploadFiles = function (files)
            {
                if(!_.find(files,function (f){return f.$error})) // se non supera i 15mb
                    $scope.files = _.union($scope.files,files); // vai in aggiunta ogni volta
                else
                    ngNotify.set(CONFIG.messages.email_upload_max_size, 'warn');
            } 

            $scope.eliminaAllegato = function (index) 
            {
                $scope.files.splice(index,1);
            }

            $scope.processMail = function (email) 
            {  
 
            /*
            salva in DB la mail  
              upload file con _id email
              invio mail
              update field "stato" "id_email" di mandrill e allegati  
            */

            delete email._id
            $scope.inviando = true; 
            $scope.email = email

            _encodeDestinatari();

            $scope.email.from_email = _mittenteEmail;
            $scope.email.from_nome  = _mittenteNome
            $scope.email.i_user_id  = _mittenteUserId 
            $scope.email.stato = 0; //salvo inizialmente lo stato bozza in DB
 

            //menage STATI
            if (!$scope.email.nazioni)
                $scope.email.nazioni = [];
    
            $scope.nazioni.forEach(
                function(nazione)
                {
                   if (nazione.checked === true) { 
                         $scope.email.nazioni.push(nazione._id.nome);  
                    }
                }
            )

            $scope.email.nazioni = _.uniq($scope.email.nazioni)
 
            //menage PROVINCE
            if (!$scope.email.province)
                $scope.email.province = [];
    
            $scope.province.forEach(
                function(provincia)
                {
                   if (provincia.checked === true) { 
                     $scope.email.province.push(provincia._id.nome); 
                    } 
                }
            )

            $scope.email.province = _.uniq($scope.email.province)
 
 
            //SALVO LA MAIL IN DB
            $scope.email = MAILINGLIST.save($scope.email,
                function (response) 
                {
                //recupero il mail_id per poter gestire il path degli allegati
                $scope.email._id =  response._id  

                //UPLOAD ALLEGATI 
                async.series([
                function(cb)
                {
                    if($scope.files && $scope.files.length)
                    {
                        console.log('upload allegati su s3');
                        var filenames = _.pluck($scope.files,'name');

                        var d = new Date();
                        var n = d.getTime();

                        $scope.files.forEach(function (file, idf)
                        {

                            var filetype= file.type!=="" ? file.type : 'application/octet-stream', // mi serve per il signed url
                                percorso = $scope.email._id+'/'+file.name; 
 
                            $http
                            ({
                                url: '/s3',
                                data: { percorso:percorso, tipo:filetype, dimensione: file.size },
                                method: 'POST',
                                skipAuthorization: true // non passo dall autorizzazione con token, s3 si incazza
                            })
                            .success(function (url)
                            {
                                // ho un signed url per l'upload su amazon
                                file.upload = Upload.http({
                                    url: url,
                                    data: file,
                                    method: 'PUT',
                                    skipAuthorization: true, // non passo dal autorizzazione con token di auth0, s3 si incazza
                                    headers: {'Content-Type': filetype}
                                });

                                file.upload.then(
                                    function (response) {

                                    if(response.status === 200) {
                                        if(idf==$scope.files.length -1) { // caricati tutti
                                            console.log('caricati tutti su s3') 

                                            $scope.email.files = filenames;
                                            cb();
                                        }
                                       
                                    }
                                });
                            });
                        })
                    }
                    else
                    {
                        _inviaMail($scope.email)
                    }
                }],
                function(err)
                { 
                    if (err)
                    {
                        console.log("err", err); 
                        $scope.inviando = false;
                        ngNotify.set(CONFIG.messages.email_error, 'error');
                    }
                    else
                    {   
                        _inviaMail($scope.email)
                    }
                }) 
            }) 
        }

        /* annulla l'invio della mail */
        $scope.annullaModifica = function () {
            modalEmail.hide(); 
        }


         /* apri dettaglio mail */
        $scope.dettaglioMail = function (mail) {
            
            $scope.email = _.clone(mail)

            //Definisce i check box delle nazioni da settare e checkbox
            $scope.email.nazioni.forEach(
                function(nazioneChecked)
                { 
                    $scope.nazioni.forEach(
                        function(nazione)
                        { 
                            if (nazioneChecked == nazione._id.nome){
                                nazione.checked=true;
                            }
                        }
                    )  
                }
            )

            //Definisce i check box delle province da settare e checkbox
            $scope.email.province.forEach(
                function(provinciaChecked)
                { 
                    $scope.province.forEach(
                        function(provincia)
                        { 
                            if (provinciaChecked == provincia._id.nome){
                                provincia.checked=true;
                            }
                        }
                    )  
                }
            ) 

            modalEmail.show();
        }   
        
        $scope.setDescInvioStati = function (nazione) { 
            if (!$scope.email.nazioneDesc)
                $scope.email.nazioneDesc = "";

            if (nazione.checked){
                $scope.email.nazioneDesc = $scope.email.nazioneDesc + " " + nazione._id.nome

                //disabilito la scelta delle province
                $scope.email.provinciaDesc = ""
                $scope.email.province = [];

                $scope.province.forEach(
                    function(provincia)
                    { 
                       if (provincia.checked === true) { 
                             provincia.checked=false 
                        }
                    }
                )  
            }
            else {
                 $scope.email.nazioneDesc =  $scope.email.nazioneDesc.replace(nazione._id.nome, "")
            }  
        }

        $scope.setDescInvioProvince = function (provincia) { 
            if (!$scope.email.provinciaDesc)
                $scope.email.provinciaDesc = "";

            if (provincia.checked){
                $scope.email.provinciaDesc = $scope.email.provinciaDesc + " " + provincia._id.nome
                $scope.email.nazioneDesc = ""
                $scope.email.nazioni = [];

                $scope.nazioni.forEach(
                    function(nazione)
                    { 
                       if (nazione.checked === true) { 
                             nazione.checked=false 
                        }
                    }
                ) 
            }
            else {
                 $scope.email.provinciaDesc =  $scope.email.provinciaDesc.replace(provincia._id.nome, "")
            }  
        } 

}]);
