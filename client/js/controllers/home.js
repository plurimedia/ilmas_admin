app.controller('home', [
    '$scope','$rootScope', 'CONFIG','PING','$interval','$rootScope',
        'PRODOTTO','MODELLO','CATEGORIA','COMPONENTE','DWHETICHETTA','CONTATTO','OFFERTA',
        'DASHBOARD',
    function ($scope,$rootScope,CONFIG,PING,$interval,$rootScope,
        PRODOTTO,MODELLO,CATEGORIA,COMPONENTE,DWHETICHETTA,CONTATTO,OFFERTA,
              DASHBOARD
              ) {

    //Salva il log in DB
    $rootScope.dwhLog("Home");

    //effettua un ping verso il server, ogni x millisencondi, per capire se esiste la connessione
    var _ping = function(){
        PING.query(
        function (response) {},
        function (err) {
             ngNotify.set(CONFIG.messages.session_error, {
                type: 'error',
                sticky: true
             });
        }); 
    }
    $interval(_ping, CONFIG.timeToPing);  
        DASHBOARD.get().then(function(response){ 

            response = response.data;

             //console.log("1)", moment(response.mdate, 'DD/MM/YYYY').format('DD/MM/YYYY'), moment().format('DD/MM/YYYY'));
            // if (moment(response.mdate, 'DD/MM/YYYY').format('DD/MM/YYYY') === moment().format('DD/MM/YYYY')) {
                // console.log('oggi abbiamo i conteggi', response)
                $scope.totModelli = {countModelli: response.countModelli};

                $scope.totCategorie = {countCategorie: response.countCategorie};

                $scope.totModelliEstro = {countModelli: response.countModelliEstro};

                $scope.totCategorieEstro = {countCategorie: response.countCategorieEstro};

                $scope.totComponenti = {
                    countCorpi: response.countCorpi,
                    countLed: response.countLed,
                    countDriver: response.countDriver,
                    countComponenti: response.countComponenti
                };
                //console.log(response, $scope.totModelli,$scope.totCategorie,$scope.totComponenti);

                $scope.totEtichette = {
                    countBianche: response.countBianche,
                    countGrigie: response.countGrigie,
                    countEtichette: response.countEtichette
                };

                $scope.totContatti = {countContatti: response.countContatti};

                $scope.totOfferte = {countOfferte: response.countOfferte};

                $scope.totProdotti = {
                    countG6: response.countG6,
                    countG5: response.countG5,
                    countG4: response.countG4,
                    countD1: response.countD1,
                    countProdotti: response.countProdotti
                };

                $scope.totProdottiEstro = {
                    countG6: response.countG6Estro,
                    countG5: response.countG5Estro,
                    countG4: response.countG4Estro,
                    countD1: response.countD1Estro,
                    countProdotti: response.countProdottiEstro
                };

                $scope.totCodEan = {countEan: response.countEan}

                $scope.iDate = response.idate;
            /* } else {
                var objectToSave;
                // console.log('oggi NON abbiamo i conteggi', response)
                async.series([
                    function(cb) { MODELLO.count(function(res) { $scope.totModelli = res; cb(); }) },
                    function(cb) { MODELLO.count({serie:'aComponenti'}, function(res) { $scope.totModelliEstro = res; cb(); }) },
                    function(cb) { CATEGORIA.count(function(res) { $scope.totCategorie = res; cb(); }) },
                    function(cb) { CATEGORIA.count({serie:'aComponenti'}, function(res) { $scope.totCategorieEstro = res; cb(); }) },
                    function(cb) { COMPONENTE.count(function(res) { $scope.totComponenti = res; cb(); }) },
                    function(cb) { DWHETICHETTA.count(function(res) { $scope.totEtichette = res; cb(); }) },
                    function(cb) { CONTATTO.count(function(res) { $scope.totContatti = res; cb(); }) },
                    function(cb) { OFFERTA.count(function(res) { $scope.totOfferte = res; cb(); }) },
                    function(cb) { PRODOTTO.countCodEan(function(res) { $scope.totCodEan = res; cb(); }) },
                    function(cb) { PRODOTTO.count(function(res) { $scope.totProdotti = res; cb(); }) },
                    function(cb) { PRODOTTO.count({serie:'aComponenti'}, function(res) { $scope.totProdottiEstro = res; cb(); }) }],
                    function(err)
                    { 
                        if (err)
                        { console.log(err); } //la gestione dell'errore non funziona in realtà, qualcosa mi sfugge, ma sono count, dovrebbero sempre andare a buon fine
                        objectToSave = {
                            countEan:$scope.totCodEan.countEan,
                            countModelli:$scope.totModelli.countModelli,
                            countModelliEstro:$scope.totModelliEstro.countModelli,
                            countCategorie:$scope.totCategorie.countCategorie,
                            countCategorieEstro:$scope.totCategorieEstro.countCategorie,
                            countCorpi:$scope.totComponenti.countCorpi,
                            countLed:$scope.totComponenti.countLed,
                            countDriver:$scope.totComponenti.countDriver,
                            countComponenti:$scope.totComponenti.countComponenti,
                            countBianche:$scope.totEtichette.countBianche,
                            countGrigie:$scope.totEtichette.countGrigie,
                            countEtichette:$scope.totEtichette.countEtichette,
                            countContatti:$scope.totContatti.countContatti,
                            countOfferte:$scope.totOfferte.countOfferte,
                            countG6:$scope.totProdotti.countG6,
                            countG5:$scope.totProdotti.countG5,
                            countG4:$scope.totProdotti.countG4,
                            countD1:$scope.totProdotti.countD1,
                            countProdotti:$scope.totProdotti.countProdotti,
                            countG6Estro:$scope.totProdottiEstro.countG6,
                            countG5Estro:$scope.totProdottiEstro.countG5,
                            countG4Estro:$scope.totProdottiEstro.countG4,
                            countD1Estro:$scope.totProdottiEstro.countD1,
                            countProdottiEstro:$scope.totProdottiEstro.countProdotti,
                            
                        };
                        $scope.iDate = new Date();

                        console.log(objectToSave);

                        var salvataggio = DASHBOARD.save(objectToSave, 
                            function (response) {
                                //console.log("ho salvato ho salvatoho salvatoho salvato");
                            }, function (err) {
                                console.log(err);
                            }
                        )
                    }
                )

            } */
        },function(err){
            console.log(err);
        })

     
}]);
