app.controller('installazioni', [
    '$scope',
    '$modal',
    'ngNotify',
    'CONFIG',
    'INSTALLAZIONE',
    'IMAGEUPLOADINSTALLAZIONE',
    'PING', 
    '$interval', 
    '$rootScope',
    function ($scope,$modal,ngNotify,CONFIG,INSTALLAZIONE,IMAGEUPLOADINSTALLAZIONE,PING,$interval,$rootScope) {

    //Salva il log in DB
    $rootScope.dwhLog("Installazioni");

    //effettua un ping verso il server, ogni x millisencondi, per capire se esiste la connessione
    var _ping = function(){
        PING.query(
        function (response) {},
        function (err) {
             ngNotify.set(CONFIG.messages.session_error, {
                type: 'error',
                sticky: true
             });
        }); 
    }
    $interval(_ping, CONFIG.timeToPing); 

    $scope.modalInstallazione = $modal(
    {
        scope: $scope,
        title: 'Nuova installazione',
        templateUrl: 'html/modals/installazione.html',
        show: false,
        id: 'modal-installazione',
        backdrop: 'static',
        keyboard: false
    });

    $scope.tabs = [
        {
            title: "Info generali",
            template: '/html/includes/installazione-tab-principale.html',
            activeTab: true
        }
        ,
        {
            title: "Foto",
            template: '/html/includes/installazione-tab-immagini.html',
            activeTab: true
        },
        {
            title: "Progetti simili",
            template: '/html/includes/installazione-tab-progetti-simili.html',
            activeTab: true
        }
        
    ];

    $scope.nuovaInstallazione = function (installazione) 
    {
        $scope.installazione = {};
        $scope.modalInstallazione.show();
    }

    $scope.modificaInstallazione = function (installazione) 
    {
        $scope.modalInstallazione.show();
        $scope.installazione = _.clone(installazione);
    }

    $scope.salvaInstallazione = function (installazione) 
    {
         let foundPrin = 0

         if ($scope.installazione.immagini){
                $scope.installazione.immagini.forEach( //aumento il contatore fotoPrin per lr nuove foto caricate
                function(element) {
                    if (element.tipo=='principale'){
                        foundPrin = foundPrin+1
                    } 
                }
            )
         }

         if ($scope.installazione.foto){
            $scope.installazione.foto.forEach( //aumento il contatore fotoPrin per le foto già caricate in precedenza
                function(element) {                
                    if (element.tipo=='principale'){
                        foundPrin = foundPrin+1
                    } 
                }
            )
        }
        else{
            $scope.installazione.foto = []
        }

        if (foundPrin>1){
            ngNotify.set("Inserire una sola foto principale", 'error');
            return
        }
        if (foundPrin==0){
            ngNotify.set("Inserire almeno una foto principale", 'error');
            return
        }

        $scope.uploading = true;
        
        if ($scope.installazione.immagini){
        IMAGEUPLOADINSTALLAZIONE($scope.installazione.immagini,
            function (response) {
               
                $scope.uploading = false;

                var imgUploades = response;
                
                imgUploades.forEach(function(element) {
                    $scope.installazione.foto.push(
                        {
                            tipo:element.tipo,
                            foto:element.id
                        }
                    )
                });

                console.log("$scope.installazione.foto ", $scope.installazione.foto)
                if(!installazione._id) 
                {
                    INSTALLAZIONE.save(installazione,
                    function (response) 
                    {
                        ngNotify.set(CONFIG.messages.installazione_saved, 'success');
                        $scope.modalInstallazione.hide();
                        $scope.installazioni = INSTALLAZIONE.last();
                    },
                    function (err) {
                        ngNotify.set(CONFIG.messages.installazione_save_error, 'error');
                    });
                }
                else 
                {
                    INSTALLAZIONE.update({
                        id: installazione._id
                    }, installazione,
                    function (response) 
                    {
                        ngNotify.set(CONFIG.messages.installazione_updated, 'success');
                        $scope.modalInstallazione.hide();
                        $scope.installazioni = INSTALLAZIONE.last();
                    },
                    function (err) 
                    {
                        ngNotify.set(CONFIG.messages.installazione_update_error, 'error');
                    });
                }
            })  

        }
        else{
            if(!installazione._id) 
                {
                    INSTALLAZIONE.save(installazione,
                    function (response) 
                    {
                        ngNotify.set(CONFIG.messages.installazione_saved, 'success');
                        $scope.modalInstallazione.hide();
                        $scope.installazioni = INSTALLAZIONE.last();
                    },
                    function (err) {
                        ngNotify.set(CONFIG.messages.installazione_save_error, 'error');
                    });
                }
                else 
                {
                    INSTALLAZIONE.update({
                        id: installazione._id
                    }, installazione,
                    function (response) 
                    {
                        ngNotify.set(CONFIG.messages.installazione_updated, 'success');
                        $scope.modalInstallazione.hide();
                        $scope.installazioni = INSTALLAZIONE.last();
                    },
                    function (err) 
                    {
                        ngNotify.set(CONFIG.messages.installazione_update_error, 'error');
                    });
                }
        }
    }

  
    $scope.eliminaInstallazione = function (installazione) 
    {  
        bootbox.confirm({
            title: CONFIG.messages.installazione_delete_alert_title,
            message: CONFIG.messages.agente_installazione_alert_message,
            buttons: {
                'cancel': {
                    label: CONFIG.messages.alert_delete_undo,
                    className: 'btn-default'
                },
                'confirm': {
                    label: CONFIG.messages.alert_delete_confirm,
                    className: 'btn-primary'
                }
            },
            callback: function (ok) 
            {
                if (ok) 
                {
                    INSTALLZIONE.delete({
                        id: agente._id
                    },
                    function (response) 
                    {
                        $scope.modalAgente.hide();
                        ngNotify.set(CONFIG.messages.agente_deleted, 'success');
                        $scope.installazioni = INSTALLZIONE.last()
                    },
                    function (err) 
                    {
                        ngNotify.set(CONFIG.messages.agente_delete_error, 'error');
                    });
                }
            }
        });
    }
   
    $scope.resetSearch = function () 
    {
        $scope.searchresults = false;
        $scope.search = '';
    }

    $scope.installazioni = INSTALLAZIONE.last();

    $scope.rimuoviFoto = function (index) {
        $scope.installazione.immagini.splice(index, 1);
    }

    $scope.rimuoviImmagineSalvata = function (index) {
        $scope.installazione.foto.splice(index, 1);
    }

    $scope.eliminaInstallazione = function (installazione) 
    {  
        bootbox.confirm({
            title: CONFIG.messages.installazioni_delete_alert_title,
            message: CONFIG.messages.installazioni_delete_alert_message,
            buttons: {
                'cancel': {
                    label: CONFIG.messages.alert_delete_undo,
                    className: 'btn-default'
                },
                'confirm': {
                    label: CONFIG.messages.alert_delete_confirm,
                    className: 'btn-primary'
                }
            },
            callback: function (ok) 
            {
                if (ok) 
                {
                    INSTALLAZIONE.delete({
                        id: installazione._id
                    },
                    function (response) 
                    {
                        $scope.modalInstallazione.hide();
                        ngNotify.set(CONFIG.messages.installazioni_deleted, 'success');
                        $scope.installazioni = INSTALLAZIONE.last()
                    },
                    function (err) 
                    {
                        ngNotify.set(CONFIG.messages.installazioni_delete_error, 'error');
                    });
                }
            }
        });
    }


    $scope.cercaInstallazione = function () 
    {
        console.log("$scope.search ", $scope.search)

        if ($scope.search && $scope.search != '') 
        {   
            if ($scope.search.length < 3) 
                ngNotify.set(CONFIG.messages.search_min_length, 'warn');
            else
            {
                console.log("sono qui if parte ricerca")

                $scope.installazioni = INSTALLAZIONE.cerca(
                {
                    search: $scope.search
                }, function () {
                    $scope.searchresults = $scope.search;
                })
            }
        } 
        else 
        {
            console.log("sono qui else")
            $scope.elencoattivita = INSTALLAZIONE.last();
            $scope.resetSearch();
        }
    }

    $scope.cercaProgetti = function (search)
    {
        return INSTALLAZIONE.cerca({search:search}).$promise;
    }

    $scope.addProgetto = function (progetto) {
        if (!$scope.installazione.progetti_simili)
            $scope.installazione.progetti_simili = []
        $scope.installazione.progetti_simili.push(progetto);
    }

   
    $scope.eliminaProgetto = function (index) {
        $scope.installazione.progetti_simili.splice(index,1);
    }
}]);
