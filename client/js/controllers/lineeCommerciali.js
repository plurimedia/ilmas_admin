app.controller('lineeCommerciali', [
    '$scope', 
    '$modal',
    'CONFIG',
    'LINEACOMMERCIALE', 
     'ngNotify','PING', '$interval','$rootScope',
    function (
            $scope, 
            $modal,
            CONFIG,
            LINEACOMMERCIALE,
            ngNotify,PING,$interval,$rootScope) {

        //Salva il log in DB
        $rootScope.dwhLog("LineaCommerciale");

        var _ping = function(){
            PING.query(
            function (response) {},
            function (err) {
                 ngNotify.set(CONFIG.messages.session_error, {
                    type: 'error',
                    sticky: true
                 });
            }); 
        } 
        $interval(_ping, CONFIG.timeToPing);

        var modalLineaCommerciale = $modal({ 
                scope: $scope,
                title: 'Nuova Linea',
                templateUrl: 'html/modals/lineaCommerciale.html',
                show: false,
                id: 'modal-linea' 
            }),

            modalImmagineLineaCommercialeImmagine = $modal({ 
                scope: $scope,
                title: 'Nuova Linea',
                templateUrl: 'html/modals/lineaCommercialeImmagine.html',
                show: false,
                id: 'modal-linea-immagine' 
            }),

             
            _salvaLineaCommerciale = function (lineaCommerciale) {
                LINEACOMMERCIALE.save(lineaCommerciale,
                    function (response) {
                        modalLineaCommerciale.hide();
                        ngNotify.set(CONFIG.messages.lineaCommerciale_saved, 'success');
                        $scope.lineeCommerciali = LINEACOMMERCIALE.last();
                        $scope.resetSearch();
                    },
                    function (err) {
                        ngNotify.set(CONFIG.messages.lineaCommerciale_save_error, 'error');
                    });
            },
            _aggiornaLineaCommerciale = function (lineaCommerciale) {
                console.log(lineaCommerciale)
                LINEACOMMERCIALE.update({
                        id: lineaCommerciale._id
                    }, lineaCommerciale,
                    function (response) {
                        modalLineaCommerciale.hide();
                        ngNotify.set(CONFIG.messages.lineaCommerciale_updated, 'success');
                        $scope.lineeCommerciali = LINEACOMMERCIALE.last();
                        $scope.resetSearch();
                    },
                    function (err) {
                        ngNotify.set(CONFIG.messages.lineaCommerciale_update_error, 'error');
                    });
            };             

        $scope.lineeCommerciali = LINEACOMMERCIALE.last();  

        $scope.apriModaleLineaCommerciale = function (lineaCommerciale) {
            modalLineaCommerciale.show(); 
            $scope.lineaCommerciale = _.clone(lineaCommerciale);
        }

        $scope.zoomImmagine = function (lineaCommerciale) {
            $scope.lineaCommerciale = lineaCommerciale;
            modalImmagineLineaCommercialeImmagine.show(); 
         }
 

        /* salvataggio nuovo componente */
        $scope.salvaLineaCommerciale = function (lineaCommerciale) {

            $scope.lineaCommerciale = lineaCommerciale
    
            if (!$scope.lineaCommerciale._id)
                _salvaLineaCommerciale($scope.lineaCommerciale);
            else
                _aggiornaLineaCommerciale($scope.lineaCommerciale);
        }

        /* Elimina componente */
        $scope.eliminaLineaCommerciale = function (id) {

            bootbox.confirm({
                title: CONFIG.messages.lineaCommerciale_delete_alert_title,
                message: CONFIG.messages.lineaCommerciale_delete_alert_message,
                buttons: {
                    'cancel': {
                        label: CONFIG.messages.alert_delete_undo,
                        className: 'btn-default'
                    },
                    'confirm': {
                        label: CONFIG.messages.alert_delete_confirm,
                        className: 'btn-primary'
                    }
                },
                callback: function (ok) {
                    if (ok) {
                        LINEACOMMERCIALE.delete({
                                id: id
                            },
                            function (response) {
                                modalLineaCommerciale.hide();
                                ngNotify.set(CONFIG.messages.lineaCommerciale_deleted, 'success');
                                $scope.resetSearch();
                                $scope.lineeCommerciali = LINEACOMMERCIALE.last();
                            },
                            function (err) {
                                ngNotify.set(CONFIG.messages.lineaCommerciale_delete_error, 'error');
                            });
                    }
                }
            });
        }

        $scope.cercaLineeCommerciali = function () { 
            if ($scope.search && $scope.search != '') {
                if ($scope.search.length < 3) {
                    ngNotify.set(CONFIG.messages.search_min_length, 'warn');
                } else {
                    $scope.lineeCommerciali = LINEACOMMERCIALE.cerca({
                        search: $scope.search
                    }, function () {
                        $scope.searchresults = $scope.search;
                    });
                }
            } else {
                $scope.lineeCommerciali = LINEACOMMERCIALE.last();
                $scope.resetSearch();
            }
        }

        $scope.resetSearch = function () {
            $scope.searchresults = false;
            $scope.search = ''; 
        } 

        $scope.editorOptions = {
        focus: true,
        toolbar: [
                ['style', ['bold', 'italic']]
            ]
        };  

       
 
}]);
