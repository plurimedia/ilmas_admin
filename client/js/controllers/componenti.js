app.controller('componenti', [
    '$scope',
    '$rootScope',
    '$http',
    '$window',
    '$modal',
    'CONFIG',
    'COMPONENTE', 
    'ngNotify',
    'PING', 
    '$interval',
    function (
            $scope,
            $rootScope,
            $http,
            $window,
            $modal,
            CONFIG,
            COMPONENTE,
            ngNotify,
            PING,$interval) {

        //Salva il log in DB
        $rootScope.dwhLog("Componenti");

           //effettua un ping verso il server, ogni x millisencondi, per capire se esiste la connessione
        var _ping = function(){
            PING.query(
            function (response) {},
            function (err) {
                 ngNotify.set(CONFIG.messages.session_error, {
                    type: 'error',
                    sticky: true
                 });
            }); 
        } 
        $interval(_ping, CONFIG.timeToPing); 

        var modalComponente = $modal(
        {   
            scope: $scope,
            title: 'Nuovo Componente',
            templateUrl: 'html/modals/componente.html',
            show: false,
            id: 'modal-componente',
            backdrop: 'static',
            keyboard: false
        })
             
        _salvaComponente = function (componente) 
        {
            COMPONENTE.save(componente,
                function (response) 
                {
                    modalComponente.hide();
                    ngNotify.set(CONFIG.messages.componente_saved, 'success');
                    $scope.componenti = COMPONENTE.ultimi();
                    $scope.resetSearch();
                },
                function (err) 
                {
                    ngNotify.set(CONFIG.messages.componente_save_error, 'error');
                }
            );
        },
        _aggiornaComponente = function (componente) 
        {
            COMPONENTE.update(
                {
                    id: componente._id
                }, componente,
                function (response) 
                {
                    modalComponente.hide();
                    ngNotify.set(CONFIG.messages.componente_updated, 'success');
                    $scope.componenti = COMPONENTE.ultimi();
                    $scope.resetSearch();
                },
                function (err) 
                {
                    ngNotify.set(CONFIG.messages.componente_update_error, 'error');
                }
            );
        };             

        $scope.componenti = COMPONENTE.ultimi();

        $scope.apriModaleComponente = function (componente) 
        {
            $rootScope.componenteReadOnly=false;

            modalComponente.show();

            if (!componente)
                $scope.componente = {
                    pubblicato: true
                };
            else{
                COMPONENTE.get({
                    id: componente._id
                },
                function (response) 
                {
                    $scope.componente = response;
                },
                function (err) 
                {
                    //ngNotify.set(CONFIG.messages.componente_delete_error, 'error');
                }
            )

            }
        } 
   
        $scope.salvaComponente = function () 
        {
            if (!$scope.componente._id)
                _salvaComponente($scope.componente);
            else
                _aggiornaComponente($scope.componente);
        }

         /* elimina accessorio da prodotto */
        $scope.eliminaComponenteFiglio = function (index) {
            $scope.componente.componenti_figlio.splice(index,1);
            $scope.aggiornaPrezzo();
        }

        /* Elimina componente */
        $scope.eliminaComponente = function (id) 
        {
            bootbox.confirm({
                title: CONFIG.messages.componente_delete_alert_title,
                message: CONFIG.messages.componente_delete_alert_message,
                buttons: {
                    'cancel': 
                    {
                        label: CONFIG.messages.alert_delete_undo,
                        className: 'btn-default'
                    },
                    'confirm': 
                    {
                        label: CONFIG.messages.alert_delete_confirm,
                        className: 'btn-primary'
                    }
                },
                callback: function (ok) 
                {
                    if (ok) 
                    {
                        COMPONENTE.delete({
                                id: id
                            },
                            function (response) 
                            {
                                modalComponente.hide();
                                ngNotify.set(CONFIG.messages.componente_deleted, 'success');
                                $scope.resetSearch();
                                 $scope.componenti = COMPONENTE.ultimi();
                            },
                            function (err) 
                            {
                                ngNotify.set(CONFIG.messages.componente_delete_error, 'error');
                            }
                        );
                    }
                }
            })
        }

        /* cerca componenti */
        $scope.findComponenti = function () 
        {

            console.log(" $scope.search ",  $scope.search)

            if ($scope.search && $scope.search != '') 
            {
                if ($scope.search.length < 3)
                    ngNotify.set(CONFIG.messages.search_min_length, 'warn');
                else 
                {
                    console.log(" $scope.search ",  $scope.search)
                    $scope.componenti = COMPONENTE.cerca(
                    {
                        search: $scope.search
                    }, 
                    function () 
                    {
                        $scope.searchresults = $scope.search;
                    });

                }
            } 
            else 
            {
                $scope.componenti = COMPONENTE.ultimi();
                $scope.resetSearch();
            }
        }

        $scope.resetSearch = function () {
            $scope.searchresults = false;
            $scope.search = '';
        }

        // duplica componente
        $scope.duplicaComponente = function (componente) 
        {
            COMPONENTE.get({
                id: componente._id
            },
            function (response) 
            {
                var nuovoComponente = _.clone(response); 

                nuovoComponente = _.omit(response, 'codice_interno','mdate', 'last_user'); 
                nuovoComponente = _.omit(nuovoComponente, 'mdate'); 
                nuovoComponente = _.omit(nuovoComponente, 'last_user'); 
                nuovoComponente = _.omit(nuovoComponente, '_id'); 

                nuovoComponente.codice = nuovoComponente.codice + '-copia';
                
                nuovoComponente.duplica = true;
                
                $scope.componente = _.clone(nuovoComponente)
    
                $rootScope.componenteReadOnly=false;
    
                modalComponente.show();
            },
            function (err) 
            {
                //ngNotify.set(CONFIG.messages.componente_delete_error, 'error');
            })

            

        }
        $scope.editorOptions =
        {
            focus: true,
            toolbar: [
                    ['style', ['bold', 'italic']]
                ]
        }

        /* autocomplete componenti */
        $scope.cercaComponenti = function (search) 
        {
            return COMPONENTE.cerca(
            {
                search:search 
            }
            ).$promise;
        }

        $scope.aggiornaPrezzo = function(){ 
            if (!$scope.componente.prezzofix){
                if ($scope.componente.componenti_figlio.length>0){
                    $scope.componente.prezzo = 0;
                    $scope.componente.componenti_figlio.forEach(function(item) {
                     var itemPrezzo = item._id.prezzo;
                     if (isNaN(itemPrezzo))
                        itemPrezzo = item.prezzo;
                    $scope.componente.prezzo = $scope.componente.prezzo + (itemPrezzo * item.qt)
                    });
                }
            }
        }

        /* autocomplete selezione componenti */
        $scope.addComponenteFiglio = function (componente_figlio) 
        { 
            if (!$scope.componente.componenti_figlio){
                $scope.componente.componenti_figlio = [];
            }
            $scope.componente.componenti_figlio.push(componente_figlio); 
        }
        
}]);
