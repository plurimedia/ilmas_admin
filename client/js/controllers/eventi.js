app.controller('eventi', [
    '$scope',
    '$modal',
    'ngNotify',
    'CONFIG',
    'EVENTO',
    'PING', 
    '$interval', 
    '$rootScope',
    'IMAGEUPLOADEVENTO',
    function ($scope,$modal,ngNotify,CONFIG,EVENTO,PING,$interval,$rootScope, IMAGEUPLOADEVENTO) {

    //Salva il log in DB
    $rootScope.dwhLog("Evento");

    //effettua un ping verso il server, ogni x millisencondi, per capire se esiste la connessione
    var _ping = function(){
        PING.query(
        function (response) {},
        function (err) {
             ngNotify.set(CONFIG.messages.session_error, {
                type: 'error',
                sticky: true
             });
        }); 
    }
    $interval(_ping, CONFIG.timeToPing); 

    $scope.tabs = [
        {
            title: "Info generali",
            template: '/html/includes/evento-tab-principale.html',
            activeTab: true
        },
        {
            title: "Immagini",
            template: '/html/includes/evento-tab-immagini.html',
            activeTab: true
        }
    ];

    $scope.modalEvento = $modal(
    {
        scope: $scope,
        title: 'Nuovo evento',
        templateUrl: 'html/modals/evento.html',
        show: false,
        id: 'modal-evento',
        backdrop: 'static',
        keyboard: false
    });

    $scope.eventi = EVENTO.last();

    $scope.nuovoEvento = function () 
    {
        $scope.evento = {
            immagini: []
        };
        $scope.modalEvento.show();
    }

    $scope.modifica = function (evento) 
    {
        
        EVENTO.get({ id:evento._id }, function (evento)
        {
            $scope.modalEvento.show();
            $scope.evento = _.clone(evento);
            
        }, function(err)
        {
            console.log(err)
        })
    }
    
    
  

    $scope.salva = function (evento) 
    { 
if ($scope.evento.immagini){

        let foundPrin = 0
        let foundLista = 0
        $scope.evento.immagini.forEach(
            function(element) {
                if (element.tipo=='principale'){
                    foundPrin = foundPrin+1
                }
                else if (element.tipo=='lista'){
                    foundLista = foundLista + 1
                }
            }
        )

        if ($scope.evento.imgPrincipale)
            foundPrin = foundPrin+1

        if ($scope.evento.imgLista)
            foundLista = foundLista+1

        if (foundPrin>1){
            ngNotify.set("Inserire una sola foto principale", 'error');
            return
        }
        if (foundLista>1){
            ngNotify.set("Inserire una sola foto per la lista", 'error');
            return
        }

        $scope.uploading = true;
        IMAGEUPLOADEVENTO($scope.evento.immagini,
            function (response) {
                
                $scope.uploading = false;

                var imgUploades = response;

                imgUploades.forEach(function(element) {
                    if (element.tipo=='principale'){
                        evento.imgPrincipale = element.id
                    }
                    else if (element.tipo=='lista'){
                        evento.imgLista = element.id
                    }
                  }); 

                if(!evento._id) 
                {
                    EVENTO.save(evento,
                    function (response) 
                    {
                        ngNotify.set(CONFIG.messages.evento_saved, 'success');
                        $scope.modalEvento.hide();
                        $scope.eventi = EVENTO.last();
                    },
                    function (err) {
                        ngNotify.set(CONFIG.messages.evento_save_error, 'error');
                    });
                }
                else 
                {
                    EVENTO.update({
                        id: evento._id
                    }, evento,
                    function (response) 
                    {
                        ngNotify.set(CONFIG.messages.agente_updated, 'success');
                        $scope.modalEvento.hide();
                        $scope.eventi = EVENTO.last();
                    },
                    function (err) 
                    {
                        ngNotify.set(CONFIG.messages.evento_update_error, 'error');
                    });
                }

            }
        )
    
}
else {
    if(!evento._id) 
                {
                    EVENTO.save(evento,
                    function (response) 
                    {
                        ngNotify.set(CONFIG.messages.evento_saved, 'success');
                        $scope.modalEvento.hide();
                        $scope.eventi = EVENTO.last();
                    },
                    function (err) {
                        ngNotify.set(CONFIG.messages.evento_save_error, 'error');
                    });
                }
                else 
                {
                    EVENTO.update({
                        id: evento._id
                    }, evento,
                    function (response) 
                    {
                        ngNotify.set(CONFIG.messages.agente_updated, 'success');
                        $scope.modalEvento.hide();
                        $scope.eventi = EVENTO.last();
                    },
                    function (err) 
                    {
                        ngNotify.set(CONFIG.messages.evento_update_error, 'error');
                    });
                }
}
}
   
$scope.rimuoviImmagineSalvata = function (tipo) {
    if (tipo=='principale'){
        $scope.evento.imgPrincipale=null;
    }
    else{
        $scope.evento.imgLista=null;
    }
}

$scope.rimuoviFoto = function (index) {
    $scope.evento.immagini.splice(index, 1);
}

}]);
