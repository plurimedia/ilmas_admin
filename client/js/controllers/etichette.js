app.controller(
    'etichette', [
    '$scope',
    '$rootScope',
    '$http',
    '$window',
    '$modal',
    'CONFIG',
    'PRODOTTO',
    'PDF',
    'DWHETICHETTA',
    'ngNotify','PING', '$interval',
    function ($scope, $rootScope, $http, $window, $modal, CONFIG,  PRODOTTO, PDF, DWHETICHETTA, ngNotify,PING, $interval) {
        //Salva il log in DB
        $rootScope.dwhLog("Etichette");

        var _ping = function(){
            PING.query(
            function (response) {},
            function (err) {
                 ngNotify.set(CONFIG.messages.session_error, {
                    type: 'error',
                    sticky: true
                 });
            }); 
        } 

        $interval(_ping, CONFIG.timeToPing); 
        
        $scope.dataEtichetta = new Date();
        $scope.dataEtichettaModificata = false;
        if ($rootScope.user)
        $scope.operatoreEtichetta = $rootScope.user.user_metadata.sigla;

        $scope.$watch(function(){
            return $scope.dataEtichetta;
        }, function(newDate){

            var dateN = getMeseAnno(newDate);
            var dateO = getMeseAnno(new Date());
 
           if (dateN != dateO)
               $scope.dataEtichettaModificata = true;
           else
                $scope.dataEtichettaModificata=null;

          },true);

        function getMeseAnno(date){
            if (date){
                var mese = date.getMonth();
                var anno = date.getFullYear();
                return  mese+""+anno;
            }
        } 

        /* cerca prodotti */
        $scope.cercaProdotti = function () {
            if ($scope.search.length < 3) {
                ngNotify.set(CONFIG.messages.search_min_length, 'warn');
            } else {
                $scope.stoLavorando=true
                $scope.prodotti = PRODOTTO.cerca({
                    search: $scope.search
                 }, function () {
                    $scope.stoLavorando=false
                    $scope.searchresults = $scope.search;
                });
             }
        };

         $scope.resetSearch = function () {
            $scope.searchresults = false;
            $scope.search = '';
        };

        $scope.apriModaleStampaEtichette = function (prodotto, tipoEtichetta) {
            $scope.prodotto = _.clone(prodotto);
            if (tipoEtichetta === 'classe-energetica') {
                $scope.createPdfClasseEnergetica($scope.prodotto);
            } else {
                PRODOTTO.getEanByidCodice(
                    {
                        idCodice: $scope.prodotto._id
                    }, 
                    function (res) {
                        if (res){
                        
                            codiceEan =res
                            $scope.prodotto.codiceEan=codiceEan.codiceEan
                            $scope.tipoEtichetta=tipoEtichetta;
                            $scope.createPdf($scope.prodotto, true);
                        }
                        else{
                            $scope.tipoEtichetta=tipoEtichetta;
                            $scope.createPdf($scope.prodotto, true);
                        }
                    }
                )
            }
            
        }; 

        $scope.stoLavorando=false

        $scope.createPdfClasseEnergetica = function (objectData) {
            $scope.stoLavorando=true

            $scope.pdf = true;
            var params = {
                    tipopdf: "etichetta_classeEnergetica"
                 },
                data = objectData;
                params.en = true;

                $scope.pdf = PDF.save(params, data, 
                    function (response) {
    
                        //salva il record in DWH etichetta
                        var dwhEtichetta = {
                            mdate:new Date(),
                            tipo:"etichetta_classeEnergetica",
                            prodotto:$scope.prodotto._id
                        };
    
                        DWHETICHETTA.save(dwhEtichetta,
                            function (etichetta) 
                            {
                                $scope.pdf = false;
                                var zoom ='';'?#page=1&zoom=200,250,100';
                                $scope.stoLavorando=false
                                $window.open(response.file+zoom, '_blank'); // apri il pdf in una nuova finestra
    
                            },
                            function (err) {
                                ngNotify.set(CONFIG.messages.prodotto_save_error, 'error');
                            }
                        ); 
                    }
                )
        }

        // objectData = prodotto, cartone
        // anteprima = true, false
        $scope.createPdf = function (objectData, anteprima) {
            $scope.stoLavorando=true
            var d = $scope.dataEtichetta;
            var mese = d.getMonth()+1;
            var anno = d.getFullYear();

           if (mese <10 )
               mese = "0"+mese;

            objectData.dataEtichetta = mese + '' + anno;
            objectData.dataEtichettaModificata = $scope.dataEtichettaModificata;
            objectData.operatore = $scope.operatoreEtichetta;

            $scope.pdf = true;
            var params = {
                    tipopdf: "etichetta_"+$scope.tipoEtichetta
                 },
                data = objectData;
                params.en = true;


            $scope.pdf = PDF.save(params, data, 
                function (response) {

                    //salva il record in DWH etichetta
                    var dwhEtichetta = {
                        mdate:new Date(),
                        tipo:"etichetta_"+$scope.tipoEtichetta,
                        prodotto:$scope.prodotto._id
                    };

                    DWHETICHETTA.save(dwhEtichetta,
                        function (etichetta) 
                        {
                            $scope.pdf = false;
                            var zoom = '?#page=1&zoom=800,250,100';
                            if ($scope.tipoEtichetta=='bianca'){
                                zoom = '?#page=1&zoom=150,250,100';
                            } 
                            else if ($scope.tipoEtichetta=='barCode'){
                                zoom ='';'?#page=1&zoom=200,250,100';
                            }
                            $scope.stoLavorando=false
                            $window.open(response.file+zoom, '_blank'); // apri il pdf in una nuova finestra

                        },
                        function (err) {
                            ngNotify.set(CONFIG.messages.prodotto_save_error, 'error');
                        }
                    ); 
                }
            )
        }; 

}]);
