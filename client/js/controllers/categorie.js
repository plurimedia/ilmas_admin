app.controller('categorie', [
    '$scope',
    '$modal',
    'ngNotify',
    'CONFIG',
    'MODELLO',
    'CATEGORIA',
    'PRODOTTO',
    'IMAGE',
    'PING', 
    '$interval', 
    '$rootScope',
    '$route',
    function ($scope,$modal,ngNotify,CONFIG,MODELLO,CATEGORIA,PRODOTTO,IMAGE, PING,$interval,$rootScope,$route) {

    //Salva il log in DB
    $rootScope.dwhLog("Categorie");

    $scope.serie = $route.current.$$route.serie;

    //effettua un ping verso il server, ogni x millisencondi, per capire se esiste la connessione
    var _ping = function(){
        PING.query(
        function (response) {},
        function (err) {
             ngNotify.set(CONFIG.messages.session_error, {
                type: 'error',
                sticky: true
             });
        }); 
    }
    $interval(_ping, CONFIG.timeToPing); 

    $scope.modalCategoria = $scope.serie && $scope.serie === 'aComponenti' ? $modal(
        {
            scope: $scope,
            title: 'Nuova categoria Estro',
            templateUrl: 'html/modals/categoriaEstro.html',
            show: false,
            id: 'modal-categoria'
        })
    : $modal(
    {
        scope: $scope,
        title: 'Nuova categoria',
        templateUrl: 'html/modals/categoria.html',
        show: false,
        id: 'modal-categoria'
    });

    modalImmagineCategoria = $modal(
    { 
        scope: $scope,
        title: 'Nuova Linea',
        templateUrl: 'html/modals/categoriaImmagine.html',
        show: false,
        id: 'modal-categoria-immagine'
    }),

    $scope.onlyNumbers = /^\d+$/;

    $scope.categorie = ($scope.serie && $scope.serie === CONFIG.serieEstro ? CATEGORIA.last({serie:CONFIG.serieEstro}) : CATEGORIA.last());
    
    $scope.nuovaCategoria = function (modello) 
    {
        $scope.categoria = $scope.serie && $scope.serie === CONFIG.serieEstro ? { serie:$scope.serie, serie: CONFIG.serieEstro, linea:'estro' } : { serie: CONFIG.serieIlmas };
        $scope.modalCategoria.show();
    }

    $scope.modificaCategoria = function (categoria) 
    {
        $scope.modalCategoria.show();
        $scope.categoria = _.clone(categoria);
    }

    $scope.salvaCategoria = function (categoria) 
    {

        if(!categoria._id) 
        {
            CATEGORIA.save(categoria,
                function (response) 
                {
                    ngNotify.set(CONFIG.messages.categoria_saved, 'success');
                    $scope.modalCategoria.hide();
                    $scope.categorie = ($scope.serie && $scope.serie === 'aComponenti' ? CATEGORIA.last({serie:'aComponenti'}) : CATEGORIA.last());
                },
                function (err) {
                    ngNotify.set(CONFIG.messages.categoria_save_error, 'error');
                });
        }
        else 
        {
            CATEGORIA.update({
                id: categoria._id
            }, categoria,
            function (response) 
            {
                ngNotify.set(CONFIG.messages.categoria_updated, 'success');
                $scope.modalCategoria.hide();
                $scope.categorie = ($scope.serie && $scope.serie === 'aComponenti' ? CATEGORIA.last({serie:'aComponenti'}) : CATEGORIA.last());
            },
            function (err) 
            {
                ngNotify.set(CONFIG.messages.categoria_update_error, 'error');
            });
        }
    }

    $scope.zoomImmagine = function (categoria) 
    {
        $scope.categoria = categoria;
        $scope.immagineCategoria =  
            IMAGE.get({id:categoria.foto});
        modalImmagineCategoria.show(); 
     }
     
    /* Elimina categoria */
    $scope.eliminaCategoria = function (categoria) 
    { 
        var modelliInCategoria = [];
        modelliInCategoria = _.filter($scope.modelli, 
            function(m){
              return  m.categoria._id==categoria._id 
            }
        );
        if (modelliInCategoria.length==0)
        {
            bootbox.confirm({
                title: CONFIG.messages.categoria_delete_alert_title,
                message: CONFIG.messages.categoria_delete_alert_message,
                buttons: {
                    'cancel': {
                        label: CONFIG.messages.alert_delete_undo,
                        className: 'btn-default'
                    },
                    'confirm': {
                        label: CONFIG.messages.alert_delete_confirm,
                        className: 'btn-primary'
                    }
                },
                callback: function (ok) 
                {
                    if (ok) 
                    {
                        CATEGORIA.delete({
                            id: categoria._id
                        },
                        function (response) 
                        {
                            $scope.modalCategoria.hide();
                            ngNotify.set(CONFIG.messages.categoria_deleted, 'success');
                            $scope.categorie = ($scope.serie && $scope.serie === 'aComponenti' ? CATEGORIA.last({serie:'aComponenti'}) : CATEGORIA.last());
                        },
                        function (err) 
                        {
                            ngNotify.set(CONFIG.messages.categoria_delete_error, 'error');
                        });
                    }
                }
            });
        }
        else
        {
            ngNotify.set(CONFIG.messages.modelliIncategoria_delete_error, 
            {
                type: 'error',
                duration: 10000
            });
        }
    }

    //ricerca modelli in view
    $scope.cercaCategorie = function () 
    {
        if ($scope.search && $scope.search != '') 
        {
            if ($scope.search.length < 3) 
                ngNotify.set(CONFIG.messages.search_min_length, 'warn');
            else
            {
                var query = { nome: $scope.search }
                if ($scope.serie && $scope.serie === 'aComponenti')
                    query.serie = 'aComponenti';

                $scope.categorie = CATEGORIA.cerca(query, 
                function () {
                    $scope.searchresults = $scope.search;
                });
            }
        } 
        else 
        {
            $scope.categorie = CATEGORIA.last();
            $scope.resetSearch();
        }
    }

    $scope.resetSearch = function () 
        {
            $scope.searchresults = false;
            $scope.search = '';
        }
      
}]);
