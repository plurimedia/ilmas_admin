app.controller(
    'dwhEtichette', [
    '$scope',
    '$rootScope',
    'CONFIG',
    'DWHETICHETTA','ngNotify', 'PING', '$interval',
    function ($scope, $rootScope, CONFIG, DWHETICHETTA, ngNotify, PING, $interval) {

        //Salva il log in DB
        $rootScope.dwhLog("DwhEtichette");

    	var _ping = function()
        {
            PING.query(
            function (response) {},
            function (err) {
                 ngNotify.set(CONFIG.messages.session_error, {
                    type: 'error',
                    sticky: true
                 });
            }); 
        } 
        $interval(_ping, CONFIG.timeToPing);   

        $scope.elencoAnni = $rootScope.elencoAnni(); 

        $scope.manageDwh = function()
        {
            $scope.etichetteBianche = DWHETICHETTA.etichetteBianche({anno:$scope.annoReport});
            $scope.etichetteGrigie  = DWHETICHETTA.etichetteGrigie({anno:$scope.annoReport});  
            $scope.etichetteEstro  = DWHETICHETTA.etichetteEstro({anno:$scope.annoReport});  

            DWHETICHETTA.etichetteBiancheInMesi(
                {anno:$scope.annoReport},
                function(etichetteBiancheInMesi){ 
                    $scope.etichetteBiancheInMesi = etichetteBiancheInMesi;

                    DWHETICHETTA.etichetteGrigieInMesi(
                        {anno:$scope.annoReport},
                        function(etichetteGrigieInMesi){
                            $scope.etichetteGrigieInMesi = etichetteGrigieInMesi;

                            DWHETICHETTA.etichetteEstroInMesi(
                                {anno:$scope.annoReport},
                                function(etichetteEstroInMesi){
                                    $scope.etichetteEstroInMesi = etichetteEstroInMesi;
        
                                     DWHETICHETTA.mesi(
                                        {anno:$scope.annoReport},
                                        function(mesi){ 

                                            $scope.mesi = mesi; 

                                            var config = {
                                                type: 'line',
                                                data: {
                                                    labels: $scope.mesi,
                                                    datasets: [ {
                                                        label: "Etichette Bianche",
                                                        data: $scope.etichetteBiancheInMesi ,
                                                        fill: false,
                                                        backgroundColor: 'red',
                                                        borderColor:'red'
                                                    },
                                                    {
                                                        label: "Etichette Grigie",
                                                        data: $scope.etichetteGrigieInMesi,
                                                        fill: false,
                                                        backgroundColor: '#156aab',
                                                        borderColor:'#156aab'
                                                    },
                                                    {
                                                        label: "Etichette Estro",
                                                        data: $scope.etichetteEstroInMesi,
                                                        fill: false,
                                                        backgroundColor: 'purple',
                                                        borderColor:'purple'
                                                    }]
                                                },
                                                options: {
                                                    responsive: true,
                                                    legend: {
                                                            display: true,
                                                            labels:  
                                                            {
                                                                fontColor: 'black'
                                                            } 
                                                        },
                                                    hover: {
                                                        mode: 'label'
                                                    },
                                                    scales: {
                                                        xAxes: [{
                                                            display: true,
                                                            scaleLabel: {
                                                                display: true,
                                                                labelString: 'Mese'
                                                            }
                                                        }],
                                                        yAxes: [{
                                                            display: true,
                                                            scaleLabel: {
                                                                display: true,
                                                                labelString: 'Numero'
                                                            }
                                                        }]
                                                    },
                                                    title: {
                                                        display: true,
                                                        text: ''
                                                    }
                                                }
                                            }; 
            
                                            var ctx = document.getElementById("graficoEtichette").getContext("2d");
                                            var myChart = new Chart(ctx, config); 

                                        },
                                        function(err){
                                            console.log(err)
                                        }
                                    )
                                },
                                function(err){
                                    console.log(err)
                                }
                            ); 
                        },
                        function(err){
                            console.log(err)
                        }
                    ); 
                },
                function(err){
                    console.log(err)
                }
            ); 
        }   

        if (!$scope.annoReport)
            $scope.annoReport = new Date().getFullYear().toString();;

        $scope.manageDwh();
  
}]);
