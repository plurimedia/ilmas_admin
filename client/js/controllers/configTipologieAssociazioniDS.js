app.controller('configTipologieAssociazioniDS', [
    '$scope',
    '$modal',
    'ngNotify',
    'CONFIG', 
    'PING', 
    '$interval', 
    '$rootScope','$http','CONFIGTIPOLOGIEASSOCIAZIONIDS',
    function ($scope,$modal,ngNotify,CONFIG,PING,$interval,$rootScope,$http,CONFIGTIPOLOGIEASSOCIAZIONIDS) {

    //Salva il log in DB
    $rootScope.dwhLog("configTipologieAssociazioniDS");

    //effettua un ping verso il server, ogni x millisencondi, per capire se esiste la connessione
    var _ping = function(){
        PING.query(
        function (response) {},
        function (err) {
             ngNotify.set(CONFIG.messages.session_error, {
                type: 'error',
                sticky: true
             });
        }); 
    }
    $interval(_ping, CONFIG.timeToPing); 

    $scope.modalTipo = $modal(
        {
            scope: $scope,
            title: 'Nuovo',
            templateUrl: 'html/modals/tipoogiaAssociazioneDS.html',
            show: false,
            id: 'modal-attivita'
        });
        
    $scope.configTipologieAssociazioniDS = CONFIGTIPOLOGIEASSOCIAZIONIDS.list();
   
    $scope.nuovoTipo = function () 
    {   
        $scope.tipologiaAssociazioniDS = {}
        $scope.modalTipo.show();
    }

    $scope.dettaglio = function (tipologia) 
    {
        $scope.modalTipo.show();
        $scope.tipologiaAssociazioniDS = _.clone(tipologia);
    }


    $scope.salva = function () 
    {  
 
        if($scope.tipologiaAssociazioniDS && $scope.tipologiaAssociazioniDS._id) {
                CONFIGTIPOLOGIEASSOCIAZIONIDS.update({
                    id: $scope.tipologiaAssociazioniDS._id
                }, $scope.tipologiaAssociazioniDS,
                function (response) 
                {
                    $scope.configTipologieAssociazioniDS = CONFIGTIPOLOGIEASSOCIAZIONIDS.list();
                    $scope.modalTipo.hide();
                    ngNotify.set(CONFIG.messages.categoria_saved, 'success');
                },
                function (err) 
                {
                    ngNotify.set(CONFIG.messages.categoria_update_error, 'error');
                });

        }
        else 
        {
            CONFIGTIPOLOGIEASSOCIAZIONIDS.save($scope.tipologiaAssociazioniDS,
                function (response) 
                {
                    $scope.configTipologieAssociazioniDS = CONFIGTIPOLOGIEASSOCIAZIONIDS.list();
                    $scope.modalTipo.hide();
                    ngNotify.set(CONFIG.messages.categoria_saved, 'success');
                },
                function (err) {
                    ngNotify.set(CONFIG.messages.categoria_save_error, 'error');
                });

        }
    } 

    
    $scope.elimina = function (id) 
    {
        bootbox.confirm({
            title: CONFIG.messages.componente_delete_alert_title,
            message: CONFIG.messages.componente_delete_alert_message,
            buttons: {
                'cancel': 
                {
                    label: CONFIG.messages.alert_delete_undo,
                    className: 'btn-default'
                },
                'confirm': 
                {
                    label: CONFIG.messages.alert_delete_confirm,
                    className: 'btn-primary'
                }
            },
            callback: function (ok) 
            {
                if (ok) 
                {
                    CONFIGTIPOLOGIEASSOCIAZIONIDS.delete({
                            id: id
                        },
                        function (response) 
                        {
                            $scope.configTipologieAssociazioniDS = CONFIGTIPOLOGIEASSOCIAZIONIDS.list();
                            $scope.modalTipo.hide();
                            ngNotify.set(CONFIG.messages.categoria_saved, 'success');
                        },
                        function (err) 
                        {
                            ngNotify.set(CONFIG.messages.componente_delete_error, 'error');
                        }
                    );
                }
            }
        })
    }

}]);
