app.controller('offerte',[
    '$scope',
    '$filter',
    '$rootScope',
    '$http',
    '$window',
    '$modal',
    '$tooltip',
    '$location',
    'Upload',
    'CONTATTO',
    'PRODOTTO',
    'OFFERTA',
    'PDF',
    'EMAIL',
    'CONFIG',
    'SETTINGS',
    'ngNotify', 'PING', '$interval',
    function ($scope,$filter,$rootScope,$http,$window,$modal,$tooltip,$location,Upload,CONTATTO,
        PRODOTTO,OFFERTA,PDF,EMAIL,CONFIG,SETTINGS,ngNotify,PING,$interval) {

    //Salva il log in DB
    $rootScope.dwhLog("Offerte");

    var _ping = function(){
            PING.query(
            function (response) {},
            function (err) {
                 ngNotify.set(CONFIG.messages.session_error, {
                    type: 'error',
                    sticky: true
                 });
            }); 
        } 
        $interval(_ping, CONFIG.timeToPing);

        $scope.allegaScheda = [];

    var modalOfferta = $modal({scope: $scope, title: 'Nuova offerta', templateUrl: 'html/modals/offerta.html',
        show: false, keyboard:false,
        backdrop: 'static'
        }),
        modalDettaglioProdotto = $modal({scope: $scope, title: 'Dettaglio prodotto', templateUrl: 'html/modals/offerta-dettaglio-prodotto.html', show: false,keyboard:false,backdrop: 'static'}),
        modalOffertaMail = $modal({scope: $scope, title: 'Invia offerta', templateUrl: 'html/modals/offerta-mail.html', show: false, keyboard:false,backdrop: 'static'}),
        modalDettaglioMail = $modal({scope: $scope, title: 'Dettagli spedizione', templateUrl: 'html/modals/offerta-dettaglio-spedizione.html', show: false, keyboard:false,backdrop: 'static'}),
          _roundToTwo =function (num) {
              return +(Math.round(num + "e+2") + "e-2");
          },
        _encodeTags = function (tags) {
            return _.pluck(tags,'text');
        },
        _decodeTags = function (tags) {
            return _.map(tags, function (t){
                return {text:t};
            })
        },
        _generaPdf = function (data,tipo,anteprima,cb) { // (dati da renderizzare, tipo di pdf (offerta, ordine), se è anteprima o meno)
              
            var params = {tipopdf: tipo};

            if($scope.offerta && $scope.offerta.linguaOfferta=='en')
                params.en = true; 

            $scope.filegenerato = false;
            if(anteprima)
                _.extend(params, {anteprima:1}); // mi serve per stabilire come generare il pdf lato server: anteprima == sempre stesso nome in .tmp altrimenti va su s3

              $scope.pdf = PDF.save(params,data, function (response){
                  $scope.pdf = false;
                  $scope.filegenerato = response.file;
                  if(anteprima)
                      $window.open(response.file, '_blank'); // apri il pdf in una nuova finestra
                  else
                      cb()
              })
          },
        /* controllo in modo asincrono lo stato delle mail da Mandrill*/
        _controllaStatoMail = function () {
             async.each(_.pluck(_.filter($scope.offerte, function (o) {return o.email_id}),'email_id'),
                function (id, callback) {
                    EMAIL.stato({id:id},
                        function (response) {
                            // aggiorna lo stato di quell'offerta
                            _.find($scope.offerte, function (o){return o.email_id === id}).emailStatus = response;
                            callback();
                        },
                        function (err) {
                            callback();
                        }
                    )
                },
                function (err){
                    // finito
                    if(err)
                        console.log(err)
                }
            );
        },

        _inviaMail = function () {
               // configuro i dati di invio della mail
              $scope.email.a = _encodeTags($scope.email.a);
              $scope.email.cc = _encodeTags($scope.email.cc);
              $scope.email.ccn = _encodeTags($scope.email.ccn);

              //$scope.email.from_email = $scope.offerta.user_email;
              //$scope.email.from_name = $scope.offerta.user_nome;


                $scope.email.from_email = "offerte@ilmas.com";
                $scope.email.from_name = $scope.offerta.user_nome;

              $scope.email.reply_to = $scope.offerta.user_email;

            // se voglio mandare gli ldt
            //if($scope.email.ldt)
            //$scope.email.ldt_files = $scope.offerta.ldt_files

            manageFileLdtfDaAllegare();
            $scope.email.ldt_files = $scope.fileLdtfDaAllegare;
    
             if($scope.offerta.linguaOfferta=='en')
                var lingua = true;
            
            EMAIL.send(
                {tipo: 'offerta',en: lingua},
                {contenuto: $scope.offerta,email: $scope.email}, //quando mando la mail separo contenuto dai dati di invio
                function (response) {
                    // mi salvo l'id della mail di mandrill nell'offerta
                    $scope.offerta.email_id = response[0]._id;
                    $scope.offerta.stato = 1;
                    /* aggiorno l'offerta con lo stato */
                    OFFERTA.update({id:$scope.offerta._id},$scope.offerta,
                        function (response) {
                            $scope.offerta = {};
                            modalOffertaMail.hide();
                            ngNotify.set(CONFIG.messages.email_offerta_sent, 'success');
                            $scope.inviando = false;
                            $scope.pdf=null;
                            $scope.offerte = OFFERTA.ultime(function (offerte){
                                  // tolgo l'email id dell'offerta aggiornata perchè non ha senso controllarla
                                  _.find($scope.offerte, function (o){return o._id===response._id}).email_id = false;
                                  _controllaStatoMail();
                              });

                        },
                        function (err){
                            ngNotify.set(CONFIG.messages.offerta_save_error, 'error');
                            $scope.inviando = false;
                        }
                    );

                },
                function (err) {
                    $scope.inviando = false;
                    ngNotify.set(CONFIG.messages.email_error, 'error');
                }
            )
          },
        _impostaDestinatario = function (value) {

            $scope.contatto = value;
            $scope.offerta.destinatario = value.email;
            $scope.offerta.destinatario_nome = value.nome; // per ricerche
            $scope.offerta.destinatario_azienda = value.azienda; // per ricerche

            /* provo a suggerire l'intestazione con i dati che ho di quel contatto */
            var suggerimentoIndirizzo = '';
            if($scope.contatto.indirizzo && $scope.contatto.cap && $scope.contatto.comune && $scope.contatto.provincia)
                suggerimentoIndirizzo = $scope.contatto.indirizzo+'<br />'+$scope.contatto.cap +' '+ $scope.contatto.comune+' '+$scope.contatto.provincia+'<br />';

            //$scope.offerta.attenzione = '<b>Spett.le</b> <br />';
            $scope.offerta.attenzione = $scope.contatto.azienda ? $scope.contatto.azienda+'<br />' : $scope.contatto.nome +'<br />';
            if(suggerimentoIndirizzo)
                $scope.offerta.attenzione += suggerimentoIndirizzo;
            if($scope.contatto.nome)
                $scope.offerta.attenzione += 'all\'attenzione di: '+$scope.contatto.nome;

            /* se manca uno di questi dati, suggerisci all'utente di completare il contatto */
            if(!$scope.contatto.indirizzo || !$scope.contatto.comune || !$scope.contatto.indirizzo || !$scope.contatto.nome)
                $scope.offerta.contattoIncompleto = $scope.contatto._id;
            else
                 $scope.offerta.contattoIncompleto = false;

            $('#autoContatti').val(value.email);
        },
        _impostaDescrizione = function (prodotto) { 

            var suggerimento='';
            
            //per i modelli binari il cliente chiede una descrizione ad hoc
            if (prodotto.modello.nome && prodotto.modello.nome.indexOf('Binari')!=-1)
            {
                suggerimento = '<strong>'+prodotto.codice+'</strong><br />';

                suggerimento += ' '+ prodotto.modello.nome 

                if(prodotto.lunghezza)
                     suggerimento +=   ' '+$filter('m')(prodotto.lunghezza)

                if(prodotto.colore)
                     suggerimento += ' '+ prodotto.colore;

            }
            else
            { 

                suggerimento = '<strong>'+prodotto.codice+'</strong><br />';

                if(prodotto.linea === 'toshiba')
                    suggerimento += 'Toshiba';

                if(prodotto.modello)
                    suggerimento += ' '+ prodotto.modello.nome;

                if(prodotto.colore)
                     suggerimento += ' '+ prodotto.colore;

                //questo test serve a stampare '1X' per i prodotti che non hanno un numero di luci specificato 
                //perche' ILMAS specifica il numero di luci (prodotto.luci) solo su prodotti con piu' di una luce --LG
                if(prodotto.luci)
                    suggerimento += ' ' + prodotto.luci + 'X';
                else 
                    suggerimento += ' ';

                suggerimento += $filter('W')(prodotto.w)+' '+$filter('K')(prodotto.k)+' '+$filter('LM')(prodotto.lm)+' '+$filter('IRC')(prodotto.irc);

                if(prodotto.sottocategoria)
                     suggerimento += ' '+prodotto.sottocategoria;

                if(prodotto.moduloled)
                     suggerimento += ' '+prodotto.moduloled;

                if(prodotto.fascio)
                     suggerimento += ' '+$filter('gradi')(prodotto.fascio);

                if(prodotto.alimentatore)
                    suggerimento += ' '+ prodotto.alimentatore;

                if(prodotto.dimmerabile)
                     suggerimento += ' Dimmerabile';

            }
            return suggerimento;
        };

    /* recupero le impostazioni */
    $rootScope.impostazioni = SETTINGS.get();

    /* se arrivo da un completamento di un contatto, apri la modale offerta con quel contatto selezionato */
    if($location.search().contatto){
        CONTATTO.get({id:$location.search().contatto}, function (data) {
            $scope.apriModaleOfferta();
            $scope.offerta = $rootScope.bozzaOfferta;
            _impostaDestinatario(data);
        })
    }


    $scope.editorOptions = {
        focus: true,
        toolbar: [
                ['style', ['bold', 'italic']]
            ]
      };

      $scope.offerte = OFFERTA.ultime(function (offerte){
          _controllaStatoMail();
      });

    /* chiude la modale offerta*/
    $scope.annullaOfferta = function (){
        modalOfferta.hide();
        $scope.offerta = {};
        $location.search('contatto',null);
    }
    
      /* apre e inizializza l'offerta */
     $scope.apriModaleOfferta = function () {

        $scope.salvataggioOfferta=false;
        $scope.pdf=false;

         $scope.offerta = {};
         $scope.offerta.stato = 0;
         $scope.offerta.user = $rootScope.user; // tengo tutto comunque
         $scope.offerta.user_id = $rootScope.user.user_id; // di auth0
         $scope.offerta.user_email = $rootScope.user.email; // la tengo per le ricerche
         $scope.offerta.user_nome = $rootScope.user.user_metadata.nome; // la tengo per le ricerche
         $scope.offerta.data = new Date();
         $scope.offerta.dal = new Date();
         $scope.offerta.al = moment(new Date()).add(30, 'days');
         $scope.offerta.prodotti = [];
         $scope.offerta.termini =  $rootScope.impostazioni.offerte.termini;
         $scope.offerta.termini_en =  $rootScope.impostazioni.offerte.termini_en;

         modalOfferta.show();
         /* trucchetto per far aprire il calendario sul click dell'addon */
         $('[bs-datepicker] + .input-group-addon').click(function(){
            $(this).prev().trigger('focus');
        })
     }

     /* autocomplete contatti */
     $scope.cercaContatti = function (search) {
         return CONTATTO.cerca({search:search}).$promise;
     }
     /* se non trovo risultati, metti cmq il value */
     $scope.controllaRisultati = function (value) {
         if(value!=="")
             $scope.offerta.destinatario = value;
     }

     /* autocomplete selezione contatto */
     $scope.scegliContatto = function (contatto) {;
        _impostaDestinatario(contatto);
     }

    /* manda alla sezione contatti per completare il contatto */
    $scope.completaContatto = function (){
        modalOfferta.hide();
        $rootScope.bozzaOfferta = $scope.offerta; // tengo in memoria
        $location.path('/contatti').search('completa', $scope.offerta.contattoIncompleto)
    }

     /* autocomplete prodotti */
     $scope.cercaProdotti = function (search) {

        var isAgente = false;
        try{
            isAgente = $rootScope.user && 
                        $rootScope.user.app_metadata.roles && 
                        $rootScope.user.app_metadata.roles.indexOf('agente') !== -1 ? true : false;
        }
        catch(e){}

        return PRODOTTO.cerca({search:search, isAgente: isAgente, isOfferta: true}).$promise;
     }

     /* autocomplete selezione prodotto */
     $scope.scegliProdotto = function (value) {
        // componi prima la descrizione che appare in offerta
         value.descrizione_offerta = _impostaDescrizione(value);
         value.accessori=null
         value.componenti=null
         value.keywords=null
         value.last_user=null
         console.log(value);
         $scope.offerta.prodotti.push(value);
     }

     /* inizializzazione qta in offerta: dipende se il prodotto ha una qta minima ordinabile o no */
     $scope.initQta = function (prodotto) {
         if(!prodotto.qta) { // vale solo per le nuove offerte
             if(prodotto.qtamin)
                 prodotto.qta = prodotto.qtamin;
             else
                 prodotto.qta = 1;
         }
     }

     /* mostra o meno l'informazione della qta minima ordinabile al focus */
     $scope.tooltipQta = function ($event,prodotto) {
         if(prodotto.qtamin) {
             var compTooltip = $tooltip(angular.element($event.target), {title: 'qta minima ordinabile: '+prodotto.qtamin, placement:'bottom', show:true});
         }
     }


     /* togli prodotto da offerta */
     $scope.eliminaProdotto = function (index) {
         $scope.offerta.prodotti.splice(index,1);
     }

    /* vedi dettagli prodotto */
    $scope.dettaglioProdotto = function (prodotto,index) {
        $scope.dettaglio = _.clone(prodotto); // si sa mai
        $scope.dettaglio_index = index;
        $scope.rigaprodotto = prodotto.descrizione_offerta; // pre imposta la descrizione prodotto
        modalDettaglioProdotto.show();
    }

    /* vedi dettagli prodotto */
    $scope.move = function (from, to) {
        var element = $scope.offerta.prodotti[from];
        $scope.offerta.prodotti.splice(from, 1);
        $scope.offerta.prodotti.splice(to, 0, element);
    }

    /* modifica  la descrizione del prodotto in offerta */
    $scope.modificaDescrizione = function (descrizione, indiceprodotto) { 
        $scope.offerta.prodotti[indiceprodotto].descrizione_offerta = descrizione;
        modalDettaglioProdotto.hide();
        ngNotify.set(CONFIG.messages.offerta_descrizione_prodotto_save, 'success');
    }

     /* totale per riga */
     $scope.totaleProdotto = function (prodotto) {

         var sconto = prodotto.sconto || 0, // se l'input type number è invalido === sconto nullo (es: 1001)
         tot;

         if(!prodotto.qta) // se mette nell'input un valore non valido
             return 0;

         if(sconto === 0)
             tot = prodotto.prezzo * prodotto.qta;
         if(sconto=== 100)
             tot = 0;
         else
             tot = _roundToTwo((prodotto.prezzo - prodotto.prezzo/100 * sconto) * prodotto.qta);

         // attacco la proprietà al prodotto
         $scope.offerta.prodotti[$scope.offerta.prodotti.indexOf(prodotto)].totale = tot;

         return tot;
     }

     /* totale prodotti offerta (senza sconto ulteriore finale)  */
     $scope.totaleProdotti = function () {

         var totaleprodotti = _.reduce(
                     _.map(_.clone($scope.offerta.prodotti), function (p){ return $scope.totaleProdotto(p)}),
                     function (memo, num){
                         return memo + num;
                     },0);
         $scope.offerta.totaleprodotti = totaleprodotti; //segno
         return totaleprodotti;
     }

     /* totale offerta */
     $scope.totaleOfferta = function () {

         var totalesenzasconto = $scope.totaleProdotti(),
             sconto = $scope.offerta.sconto || 0; //vedi sopra

         var totaleofferta;
         if(sconto === 0)
             totaleofferta = totalesenzasconto;
         if(sconto === 100)
             totaleofferta = 0;
         else
             totaleofferta = _roundToTwo(totalesenzasconto - totalesenzasconto/100 * sconto);

         $scope.offerta.totaleofferta = totaleofferta; //segno


         return totaleofferta;
     }

     /* anteprima pdf*/
     $scope.anteprimaPdf = function (offerta) {
         // (dati da renderizzare, tipo di pdf (offerta, ordine), se è anteprima o meno)
         _generaPdf(offerta,'offerta',true);
     }

     /* cerca mail nella modale di invio*/
      $scope.cercaEmail = function (chiave) {
         return CONTATTO.cercaemail({search:chiave}).$promise.then(function (r) {
             return _decodeTags(_.pluck(r,'email')) // li metto nel formato dal tagsinput
         });
     }

    /* controllo se i prodotti in offerta hanno il file ldt per dare l'indicazione durante la scrittura della mail */
    /* tengo codice e file */

    $scope.$watchCollection('offerta.prodotti', function (newVal, oldVal){
         if($scope.offerta) {
            $scope.offerta.ldt_files = _.map(_.filter(newVal, function(prodotto){
                return prodotto.file_ldt;
            }), function(p){
                return {
                    codice: p.codice,
                    ldt: p.file_ldt
                }
            })
            $scope.fileLdtfDaAllegare = _.clone($scope.offerta.ldt_files);

            $scope.listafileLdtfDaAllegare = new Array();

            for (item in $scope.fileLdtfDaAllegare) {
                $scope.listafileLdtfDaAllegare[item] = {
                    "codice":$scope.fileLdtfDaAllegare[item].codice,
                    "allegaFile": false
                };
            }
        }
    })


    $scope.eliminaAllegatoFileLdt = function (index) {
         $scope.listafileLdtfDaAllegare[index] = {
           "codice":$scope.listafileLdtfDaAllegare[index].codice,
           "allegaFile": $scope.listafileLdtfDaAllegare[index].allegaFile
        };
    }

      $scope.eliminaAllegatoSchedaTecnica = function (index) {
       $scope.listaCodiciSchedeTecniche[index] = {
           "codice":$scope.listaCodiciSchedeTecniche[index].codice,
           "allegaScheda": $scope.listaCodiciSchedeTecniche[index].allegaScheda
       };
      }

    function manageFileLdtfDaAllegare(){
        var listToDelete = new Array();
        for (item in $scope.listafileLdtfDaAllegare) {
            $scope.codiceFile = $scope.listafileLdtfDaAllegare[item];
            if ($scope.codiceFile.allegaFile==false){
                listToDelete.push($scope.codiceFile.codice);
            }
        }
        $scope.fileLdtfDaAllegare.reduceRight(function(acc, obj, idx) {
            if (listToDelete.indexOf(obj.codice) > -1)
                $scope.fileLdtfDaAllegare.splice(idx,1);
        }, 0);
     }


     /* salva e invia la mail*/
     $scope.scriviMail = function () {

        $scope.salvataggioOfferta = false;
        $scope.pdf=false;

         /* primo salvataggio */
         if(!$scope.offerta._id) {

            $scope.salvataggioOfferta = OFFERTA.save($scope.offerta,
                function (response) {
                   
                    $scope.offerta = response; // aggiorno l'offerta
                    // a questo punto genero il pdf
                    _generaPdf($scope.offerta,'offerta',false, function (){
                        $scope.email = {}; // inzializzo mail
                        modalOfferta.hide(); // nascondo la modale offerta
                        $scope.email.a = [{text:$scope.offerta.destinatario}]  // col suo destinatario
                        $scope.email.pdf = $scope.filegenerato; // col pdf dell'offerta creato

                        $scope.clonaSchedeTecniche();
                        modalOffertaMail.show(); // alla fine apri la modale
                    });
                },
                function (err){
                    ngNotify.set(CONFIG.messages.offerta_save_error, 'error');
                }
            );
         }
         /* modifica*/
        else {
            $scope.salvataggioOfferta = OFFERTA.update({id:$scope.offerta._id},$scope.offerta,
                function (response) {
                    $scope.offerta = response; // aggiorno l'offerta
                    // a questo punto ri-genero il pdf
                    _generaPdf($scope.offerta,'offerta',false, function () {
                        $scope.email = {}; // inzializzo mail
                         $scope.email.a = [{text:$scope.offerta.destinatario}]  // col suo nuovo destinatario
                         $scope.email.pdf = $scope.filegenerato; // col pdf dell'offerta modificato

                        $scope.clonaSchedeTecniche();

                        modalOfferta.hide(); // nascondo la modale offerta
                        modalOffertaMail.show(); // riapro la mail
                    });
                },
                function (err){
                    ngNotify.set(CONFIG.messages.offerta_save_error, 'error');
                });
        }
     }

     /* allega files alla mail*/
    $scope.uploadFiles = function (files) {
        if(!_.find(files,function (f){return f.$error})) // se non supera i 15mb
            $scope.files = _.union($scope.files,files); // vai in aggiunta ogni volta
        else
            ngNotify.set(CONFIG.messages.email_upload_max_size, 'warn');
    }

     /* togli allegato */
     $scope.eliminaAllegato = function (index) {
         $scope.files.splice(index,1);
     }

    /* annulla l'invio della mail e torna alla modifica dell'offerta */
    $scope.annullaModifica = function () {

        $scope.salvataggioOfferta=false
        $scope.pdf=false

        modalOffertaMail.hide();
        modalOfferta.show();
 
    }


    /* Invia la mail */
    $scope.inviaMail = function () {

        $scope.inviando = true;
        async.series([
            // se carica allegati extra
            function(cb){
                if($scope.files && $scope.files.length) {
                    console.log('carico allegati su s3');
                    var filenames = _.pluck($scope.files,'name'); // quello che terrò nel db
                    $scope.files.forEach(function (file, idf) {

                        var filetype= file.type!=="" ? file.type : 'application/octet-stream', // mi serve per il signed url
                            percorso = $scope.offerta._id+'/'+file.name;

                        $http
                        ({
                            url: '/s3',
                            data: { percorso:percorso, tipo:filetype, dimensione: file.size },
                            method: 'POST',
                            skipAuthorization: true // non passo dal autorizzazione con token, s3 si incazza
                        })
                        .success(function (url)
                        {
                            // ho un signed url per l'upload su amazon
                            file.upload = Upload.http({
                                url: url,
                                data: file,
                                method: 'PUT',
                                skipAuthorization: true, // non passo dal autorizzazione con token di auth0, s3 si incazza
                                headers: {'Content-Type': filetype}
                            });

                            file.upload.then(function (response) {
                                if(response.status === 200) {
                                    if(idf==$scope.files.length -1) { // caricati tutti
                                        console.log('caricati tutti su s3')
                                        $scope.email.files = filenames; // salvo quelli caricati
                                        cb();
                                    }
                                }

                            });
                        });
                    })
                }
                else {
                    cb();
                }

            },
            // se vuole caricare anche le schede tecniche
            function(cb){
                //if($scope.offerta.allegaschede){
                manageSchedeTecnicheDaAllegare();


                if($scope.schedeTecnicheAllegate){
                    console.log('allego schede tecniche');
                    var params = {tipopdf: 'schedatecnica', anteprima: false};
                    // devo generare un pdf per ogni prodotto e metterlo su s3
 
                    async.each($scope.schedeTecnicheAllegate,
                        function(prodotto,cb) { 
 
                            var params = {tipopdf: 'schedatecnica'},
                                data = prodotto; 
                                data._id = $scope.offerta._id; // da migliorare: così le mette nella cartella dell'offerta
                            
                            if($scope.offerta && $scope.offerta.linguaOfferta=='en')
                                params.en = true;

                            $scope.pdf = PDF.save(params,data, function (response){
                                cb();
                            })
                        },
                        function(err){
                            if(err)
                                console.log(err)

                             $scope.email.schede = _.map(_.pluck($scope.schedeTecnicheAllegate,'codice'), function(s){ return s+'.pdf'}); /* TODO: migliorare*/
                            cb();  
                        }
                    );
                }
                else {
                    cb();
                }
            }],
            function(err){
                if(err)
                    console.log(err)

                console.log('finito tutti, invio la mail');
                _inviaMail(); // invio la mail
            }
        );

    }

    /* apri dettaglio mail */
    $scope.apirModaleDettaglioMail = function (offerta) {
        $scope.salvataggioOfferta = false;
        $scope.pdf=false;
        $scope.dettaglio = offerta;
        modalDettaglioMail.show();
    }

    /* modifica offerta */
    $scope.modificaOfferta = function (offerta) {
        $scope.salvataggioOfferta = false;
        $scope.pdf=false;
        $scope.offerta = offerta;
        modalOfferta.show();
    }

    $scope.clonaSchedeTecniche = function(){
        $scope.schedeTecnicheAllegate = _.clone($scope.offerta.prodotti)

        var mySubArray = _.uniq($scope.schedeTecnicheAllegate, 'codice');
        $scope.schedeTecnicheAllegate = _.clone(mySubArray)


        $scope.listaCodiciSchedeTecniche = new Array();
        for (item in $scope.schedeTecnicheAllegate) { 

            $scope.codiceToStamp = $scope.schedeTecnicheAllegate[item];

            var allega = false;
            var viewDato = true;

            if ($scope.schedeTecnicheAllegate[item].prodottoNoListino){
                allega=false;
                viewDato=false
            }
            
            $scope.listaCodiciSchedeTecniche[item] = {
                "codice":$scope.codiceToStamp.codice,
                "allegaScheda": allega,
                "prodottoNoListino":viewDato
            }; 
        }

     }

   function manageSchedeTecnicheDaAllegare(){
    
    for (var i=0;i<$scope.listaCodiciSchedeTecniche.length;i++){
        var schedaDaAllegare = $scope.listaCodiciSchedeTecniche[i];

        if (!schedaDaAllegare.selected){
            codiceDaEliminare = schedaDaAllegare.codice;

            for (var j=0;j<$scope.schedeTecnicheAllegate.length;j++){
                var codiceSchedaDaControllare = $scope.schedeTecnicheAllegate[j].codice;
                if (codiceDaEliminare==codiceSchedaDaControllare){
                    $scope.schedeTecnicheAllegate.splice(j,1)
                }
            }
        }
    }

/*
        var listToDelete = new Array();
        for (item in $scope.listaCodiciSchedeTecniche) { 
 
                 $scope.codiceToStamp = $scope.listaCodiciSchedeTecniche[item];
                if ($scope.codiceToStamp.allegaScheda==false){
                    listToDelete.push($scope.codiceToStamp.codice);
                }
            
        }

        

        $scope.myArr = _.clone(_.uniq($scope.schedeTecnicheAllegate, 'codice'));
        
        console.log(" $scope.listToDelete ", $scope.listToDelete)
        console.log(" $scope.codiceToStamp ", $scope.codiceToStamp)
        console.log(" $scope.myArr ", $scope.myArr)

        $scope.schedeTecnicheAllegate.reduceRight(function(acc, obj, idx) {
            if (listToDelete.indexOf(obj.codice) > -1)
                $scope.schedeTecnicheAllegate.splice(idx,1);
        }, 0);
*/
       
    }

    /* cerca offerte */
    $scope.cercaOfferte = function () {
        if($scope.search && $scope.search!='') {
            if($scope.search.length <3) {
                ngNotify.set(CONFIG.messages.search_min_length, 'warn');
            }
            else {
                $scope.offerte = OFFERTA.cerca({search:$scope.search}, function(){
                    $scope.searchresults = $scope.search;
                    _controllaStatoMail();
                });
            }
        }
        else {
            $scope.offerte = OFFERTA.ultime(function () {
                _controllaStatoMail();
            });
            $scope.resetSearch();
        }
    }

    $scope.resetSearch = function () {
        $scope.searchresults = false;
        $scope.search = '';
    }

     

    /* modifica offerta */
    $scope.duplicaOfferta = function (offerta) {
        offerta = _.omit(offerta, '_id');
        offerta.numero = "";
        $scope.offerta = offerta;
        $scope.salvataggioOfferta = false;
        $scope.pdf=false;
        modalOfferta.show();
    }

    $scope.eliminaOfferta = function (id) {

            bootbox.confirm({
                title: CONFIG.messages.offerta_delete_alert_title,
                message: CONFIG.messages.offerta_delete_alert_message,
                buttons: {
                    'cancel': {
                        label: CONFIG.messages.alert_delete_undo,
                        className: 'btn-default'
                    },
                    'confirm': {
                        label: CONFIG.messages.alert_delete_confirm,
                        className: 'btn-primary'
                    }
                },
                callback: function (ok) {
                    if (ok) {

                        OFFERTA.delete({
                                id: id
                            },
                            function (response) {
                                modalOfferta.hide();
                                $scope.offerte = OFFERTA.ultime(function () {
                                    _controllaStatoMail();
                                });
                                $scope.resetSearch();
                                ngNotify.set(CONFIG.messages.offerta_deleted, 'success');
                            },
                            function (err) {
                                ngNotify.set(CONFIG.messages.offerta_delete_error, 'error');
                            });
                    }
                }
            });
        }

        $scope.addProdNoIlMas = function () {
            var prodotto = {
                totale:0,
                prezzo:0,
                prodottoNoListino:true
            };
            $scope.offerta.prodotti.push(prodotto) 
        }

     $scope.impostaDescrizioneNoIlMas = function (index,codice) {
        $scope.offerta.prodotti[index].descrizione_offerta=codice;
     }

}]);
