app.controller('immagini', [
	'$scope',
	'$rootScope',
	'$http',
	'$modal',
	'IMAGEUPLOAD',
	'IMAGE',
    'MODELLO',
    'CATEGORIA',
    'PRODOTTO',
    'LINEACOMMERCIALE',
	'CONFIG',
	'ngNotify','PING', '$interval',
    function ($scope, $rootScope, $http, $modal, IMAGEUPLOAD, IMAGE, MODELLO ,CATEGORIA,PRODOTTO,
        LINEACOMMERCIALE,CONFIG, ngNotify,PING,$interval) { 

        modalImmagineProdotto = $modal({ 
                scope: $scope,
                title: 'Nuova Linea',
                templateUrl: 'html/modals/immagine.html',
                show: false,
                id: 'modal-prodotto-immagine'
            })

        //Salva il log in DB
        $rootScope.dwhLog("Immagini");

        var _ping = function(){
            PING.query(
            function (response) {},
            function (err) {
                 ngNotify.set(CONFIG.messages.session_error, {
                    type: 'error',
                    sticky: true
                 });
            }); 
        } 
        $interval(_ping, CONFIG.timeToPing); 
        
        var modalImmagine = $modal({
                scope: $scope,
                title: 'Associa immagine',
                templateUrl: 'html/modals/immagine-associa.html',
                show: false
            });

        $scope.immagini = IMAGE.ultime();

        // rimuovo foto dalla preview
        $scope.rimuoviFoto = function (index) {
            $scope.files.splice(index, 1);
        }

        // fa l'upload delle foto del batch
        $scope.caricaFoto = function (form) {
            $scope.uploading = true;
            IMAGEUPLOAD($scope.files,
                function (response) {
                    $scope.files = [];
                    $scope.uploading = false;
                    $scope.immagini = IMAGE.ultime();
                }
            );
        }

        // modifica immagine
        $scope.modificaImmagine = function (immagine) {
            IMAGE.update({
                id: immagine._id
            }, immagine,
            function (response) {
                ngNotify.set(CONFIG.messages.image_updated, 'success');
                $scope.immagini = IMAGE.ultime();
            },
            function (err) {
                ngNotify.set(CONFIG.messages.image_update_error, 'error');
            });
        }
        
        // elimina immagine
        
        $scope.eliminaImmagine = function (immagine) {

            bootbox.confirm({
                title: CONFIG.messages.image_delete_alert_title,
                message: CONFIG.messages.image_delete_alert_message,
                buttons: {
                    'cancel': {
                        label: CONFIG.messages.alert_delete_undo,
                        className: 'btn-default'
                    },
                    'confirm': {
                        label: CONFIG.messages.alert_delete_confirm,
                        className: 'btn-primary'
                    }
                },
                callback: function (ok) {
                    if (ok) {
                        IMAGE.delete({
                                idcloudinary: immagine.id,
                                idimmagine: immagine._id,
                                tipo: immagine.tipo
                            },
                            function (data) {
                                // preparo un messaggio che tiene conto di quelli eliminati con un link al log
                                var msg = CONFIG.messages.image_deleted;
                                if (data.modified.count > 0)
                                    msg += '<br />' + data.modified.count + ' elementi avevano questa foto';
                                if (data.modified.count > 0) {
                                    msg += '<br /><a href="' + data.modified.log + '"><i class="fa fa-download"></i> Scarica il csv con gli elementi che non hanno più questa foto</a>';
                                }
                                ngNotify.set(msg, {
                                    html: true,
                                    type: 'success',
                                    sticky: true
                                });
                                $scope.immagini = IMAGE.ultime();
                            },
                            function (err) {
                                ngNotify.set(CONFIG.messages.image_delete_error, 'error');
                            });
                    }
                }
            });
        }
        
        // setta il tipo di ricerca
        $scope.tipoRicerca = function (tipo) {
            $scope.tipo = tipo;
        }
        
        // cerca immagine
        $scope.cercaImmagini = function () {
            
            $scope.search = {};
            if($scope.tipo)
                $scope.search.tipo = $scope.tipo;
            if($scope.nome) {
                if ($scope.nome.length < 2) {
                    ngNotify.set(CONFIG.messages.search_min2_length, 'warn');
                    return false;
                }
                else {
                    $scope.search.nome = $scope.nome;
                }   
            }
            
            $scope.immagini = IMAGE.cerca({cerca:$scope.search}, function () {
                $scope.searchresults = $scope.search;
            });           
        }

        $scope.resetSearch = function () {
            $scope.searchresults = false;
            $scope.search = '';
        }
        
        // associazione immagini bacth

        $scope.mostraAssociaImmagine = function (immagine) {

            console.log("immagine ",immagine);

            $scope.immagine = immagine
            modalImmagine.show();
            $scope.modelli = MODELLO.list();
            $scope.categorie = CATEGORIA.list();
            $scope.modelliEstro = MODELLO.list({serie:CONFIG.serieEstro});
            $scope.categorieEstro = CATEGORIA.list({serie:CONFIG.serieEstro});
            $scope.lineeCommerciali = LINEACOMMERCIALE.getAll();

            console.log("$scope.lineeCommerciali", $scope.lineeCommerciali);

            $scope.batch = {tipo:immagine.tipo, foto: immagine.id};
            $scope.codici = [];
        }
    
        
        $scope.cambiaAssocia = function (cosa) {
            $scope.batch.cosa = cosa;
            $scope.batch.codici = [];
    
        }
        
        $scope.sceltaCategoria = function (categoria) {
            $scope.batch.codici = [categoria._id];
        }

        $scope.sceltaLineaCommerciale = function (lineaCommerciale) {
            $scope.batch.codici = [lineaCommerciale._id];
        }
        
        // cerca codici per associare immagini
        $scope.cercaCodici = function (chiave) {
            if (chiave && chiave.length >= 6)
                return PRODOTTO.searchCodice({
                    nome: chiave
                }).$promise;
        }
        
        $scope.sceltaModello = function (value) {
             $scope.batch.codici = [];
             if(value)
                $scope.batch.codici.push(value._id)
                
            if($scope.batch.cosa==='modulo') {  // sono nella selezione dei moduli di un modello, devo recuperare i moduli di un modello
               $scope.moduli=[];
 
                $scope.codicimodulo = 
                PRODOTTO.moduli(
                    {modello:$scope.batch.codici[0]}, 
                    function (resp){ 
                        $scope.moduli = _.uniq(_.pluck($scope.codicimodulo,'moduloled')) // per riempire la select
                    }
                ) 
            } 

            if($scope.batch.cosa==='colore') {  // sono nella selezione dei colori di un modello, devo recuperare i colori di un modello
               $scope.colorimodulo = PRODOTTO.colori({modello:$scope.batch.codici[0]}, function (){
                   $scope.colori = _.uniq(_.pluck($scope.colorimodulo,'colore')) // per riempire la select
               })
            }

            if($scope.batch.cosa==='fascio') {  // sono nella selezione fascio di un modello, devo recuperare i fasci di un modello
               $scope.fascimodulo = PRODOTTO.fasci({modello:$scope.batch.codici[0]}, function (){
                   $scope.fasci = _.uniq(_.pluck($scope.fascimodulo,'fascio')) // per riempire la select
               })
            }
        }
        
        $scope.sceltaModulo = function (modulo) {
            // metto in batch i codici di quel modulo
            $scope.batch.codici = _.pluck(_.filter($scope.codicimodulo, function (c){return c.moduloled===modulo}),'_id');
        }
        
        $scope.sceltaColore = function (colore) {
            // metto in batch i colori di quel modulo
            $scope.batch.codici = _.pluck(_.filter($scope.colorimodulo, function (c){return c.colore===colore}),'_id');
        }
        
        $scope.eliminaCodice = function (codice) {
            $scope.codici.splice($scope.codici.indexOf(codice.codice),1);
            $scope.batch.codici.splice($scope.codici.indexOf(codice._id),1);
        }
        
        // selezione codice
        $scope.$on('$typeahead.select', function (event, value, index, elem) { 
            if($scope.batch.codici.indexOf(value._id)===-1)
                $scope.batch.codici.push(value._id);
            if(!_.find($scope.codici,function(c){return c._id === value._id}))
                $scope.codici.push(value);
            
            $scope.$apply();
        });
        
        // seleziona tutti in cima al typeahead
        $scope.selezionaTutti = function (results) {

            $scope.batch.codici = _.unique(_.union($scope.batch.codici,_.pluck(_.pluck(results,'value'),'_id')));
            $scope.codici =  _.unique(_.union($scope.codici,_.pluck(results,'value')), function(v){return v._id;});
        }
        
        $scope.associaImmagine = function (){
            $scope.associando = true;
            IMAGE.batchupdate($scope.batch,
                function (resp) {
                    $scope.associando = false;
                    ngNotify.set(CONFIG.messages.image_updated_images, 'success');
                    modalImmagine.hide();
                },
                function (err) {
                    ngNotify.set(CONFIG.messages.image_update_images_error, 'error');
                }
            );
        }

        $scope.zoomImmagine = function (immagine) 
        { 
            $scope.immagine =  immagine 
            modalImmagineProdotto.show(); 
        }


}]);