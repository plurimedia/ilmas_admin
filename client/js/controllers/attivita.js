app.controller('attivita', [
    '$scope',
    '$modal',
    'ngNotify',
    'CONFIG', 
    'PING', 
    '$interval', 
    '$rootScope','$http', 'Upload', 'ATTIVITA','EMAIL',
    function ($scope,$modal,ngNotify,CONFIG,PING,$interval,$rootScope,$http,Upload,ATTIVITA,EMAIL) {

    $scope.modalAttivita = $modal(
    {
        scope: $scope,
        title: 'Nuova attività',
        templateUrl: 'html/modals/attivita.html',
        show: false,
        id: 'modal-attivita'
    });

    $scope.tabs = [
        {
            title: "Informazioni",
            template: '/html/includes/attivita-tab-informazioni.html',
            activeTab: true
        },
        {
            title: "Allegati",
            template: '/html/includes/attivita-tab-allegati.html'
        }
    ];

    //Salva il log in DB
    $rootScope.dwhLog("Attivita");

    //effettua un ping verso il server, ogni x millisencondi, per capire se esiste la connessione
    var _ping = function(){
        PING.query(
        function (response) {},
        function (err) {
             ngNotify.set(CONFIG.messages.session_error, {
                type: 'error',
                sticky: true
             });
        }); 
    }
    $interval(_ping, CONFIG.timeToPing); 

    $scope.elencoattivita = ATTIVITA.last();

    $scope.nuovaAttivita = function (attivita) 
    {
        ATTIVITA.count(function(res){
            $scope.attivita = {
                codice:$scope.createCodiceAtt(res.countAttivita)
            }
            $scope.modalAttivita.show();
        })
    }

    $scope.createCodiceAtt = function(count){
        var mese = parseFloat(new Date().getMonth()) + 1
        return new Date().getFullYear() + '.' + mese+ '_' + count
    }

    $scope.setDataApprovazione = function(){
        if ($scope.attivita.approvata)
            $scope.attivita.data_approvazione = new Date();
        else
            $scope.attivita.data_approvazione = null
    }

    // carica file
    $scope.caricaAllegato = function (file) {
        if (file){
        var filetype = file.type ? file.type : 'application/octet-stream', // mi serve per il signed url
            percorso = $scope.attivita.codice + "/" + $scope.attivita._id + '/' + file.name; 
             
        $scope.uploadingfile = true;

        $http
            ({
                url: '/s3',
                data: {
                    percorso: percorso,
                    tipo: filetype,
                    dimensione: file.size
                },
                method: 'POST',
                skipAuthorization: true // non passo dal autorizzazione con token, s3 si incazza
            })
            .success(function (url) {
                // ho un signed url per l'upload su amazon
                file.upload = Upload.http({
                    url: url,
                    data: file,
                    method: 'PUT',
                    skipAuthorization: true, // non passo dal autorizzazione con token di auth0, s3 si incazza
                    headers: {
                        'Content-Type': filetype
                    }
                });

                file.upload.then(function (response) {
                    if (response.status === 200) {

                        var allegato = {
                            nome:file.name,
                            url:response.config.url.split('?')[0]
                        }

                        if  (! $scope.attivita.allegati)
                            $scope.attivita.allegati = [];

                        $scope.attivita.allegati.push(allegato); 

                        $scope.uploadingfile = false;

                    }
                });
            });
        }
    }

    $scope.dettaglioAttivita = function (attivita) 
    {
        $scope.modalAttivita.show();
        $scope.attivita = _.clone(attivita);
    }

    $scope.editorOptions =
    {
        focus: true,
        toolbar: [
                ['style', ['bold', 'italic']]
            ]
    }

    $scope.salvaAttivita = function (attivita) 
    {  
        if(!$scope.attivita._id) 
        {
            ATTIVITA.save($scope.attivita,
                function (response) 
                {
                    ngNotify.set(CONFIG.messages.categoria_saved, 'success');
                    $scope.modalAttivita.hide();
                    $scope.elencoattivita = ATTIVITA.last();
                },
                function (err) {
                    ngNotify.set(CONFIG.messages.categoria_save_error, 'error');
                });
        }
        else 
        {
            ATTIVITA.update({
                id: $scope.attivita._id
            }, $scope.attivita,
            function (response) 
            {
                ngNotify.set(CONFIG.messages.categoria_updated, 'success');
                $scope.modalAttivita.hide();
                $scope.elencoattivita = ATTIVITA.last();
            },
            function (err) 
            {
                ngNotify.set(CONFIG.messages.categoria_update_error, 'error');
            });
        }
    } 

    $scope.cercaAttivita = function () 
    {
        if ($scope.search && $scope.search != '') 
        {   
            if ($scope.search.length < 3) 
                ngNotify.set(CONFIG.messages.search_min_length, 'warn');
            else
            {
                $scope.elencoattivita = ATTIVITA.cerca(
                {
                    search: $scope.search
                }, function () {
                    $scope.searchresults = $scope.search;
                })
            }
        } 
        else 
        {
            $scope.elencoattivita = ATTIVITA.last();
            $scope.resetSearch();
        }
    }

    $scope.resetSearch = function () 
        {
            $scope.searchresults = false;
            $scope.search = '';
        }
      
}]);
