app.controller('dwhLogs', [
    '$scope','$rootScope','PING','$interval','CONFIG','ngNotify',
    'DWHLOG',
    function ($scope,$rootScope,PING,$interval,CONFIG,ngNotify,
        DWHLOG) {

    //Salva il log in DB
    $rootScope.dwhLog("Dwh Logs");

    var _ping = function()
    {
        PING.query(
        function (response) {},
        function (err) {
             ngNotify.set(CONFIG.messages.session_error, {
                type: 'error',
                sticky: true
             });
        }); 
    } 
    $interval(_ping, CONFIG.timeToPing);  

    $scope.dwhLogs = DWHLOG.last();
 
}]);
