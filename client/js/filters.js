numeral.language('it');

app
.filter('prezzo', function () {
	return function (input) {
		return numeral(input).format('$0,0.00');
	}
})
.filter('gradi', function () {
	return function (input) {
		return input ? input+ '°' : '';
	}
})
.filter('K', function () {
	return function (input) {
		return input ? input+ 'K' : '';
	}
})
.filter('W', function () {
	return function (input) {
		return input ? input+ 'W' : '';
	}
})
.filter('LM', function () {
	return function (input) {
		return input ? input+ 'lm' : '';
	}
})
.filter('IRC', function () {
	return function (input) {
		return input ? 'IRC'+input : '';
	}
})
.filter('ore', function () {
	return function (input) {
        if(input)
		  return input ? input+ ' ore' : '';
	}
})
.filter('boolean', function () {
	return function (input) {
		  return input ? 'SI' : 'NO';
	}
})
.filter('m', function () {
	return function (input) {
        if(input)
		  return input ? input+ ' m' : '';
	}
})
.filter('X', function () {
	return function (input) {
        if(input)
		  return input ? input+ 'X' : '';
	}
})
.filter('bytes', function () {
  return function(bytes, precision) {
    if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
    if (typeof precision === 'undefined') precision = 1;
    var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
      number = Math.floor(Math.log(bytes) / Math.log(1024));
    return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
  }
})
.filter('truncate', function(){
    return function(obj, limit) {
      if(obj.length > limit)
        return obj.substr(0,limit)+'...';
      else
        return obj;
    };
})


