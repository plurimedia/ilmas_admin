FROM centos:7
USER root

# environment pre-requirements (nodejs 11 repo)
# nodejs12 causes "primordials" modules error
RUN curl -sL https://rpm.nodesource.com/setup_14.x | bash -

# Aggiunti: freetype* fontconfig urw-fonts
# Soluzione per write EPIPE error
RUN yum -y install nodejs git bzip2 wget freetype* fontconfig urw-fonts && yum clean all -y

RUN echo "Code will be pulled from $REPOSITORY branch"
RUN git clone --branch $REPOSITORY https://plurimedia-services:r8qjcgEdvkbvb3cNjsAA@bitbucket.org/plurimedia/ilmas_admin.git /opt/ilmas

# creating non-root user and setting its permissions
RUN useradd nodejs && \
    chown -R nodejs:nodejs /opt && \
    chmod g+rwX -R /opt

# PDF Fix Permission
RUN mkdir -p /opt/ilmas/.temp && \
    chmod 777 -R /opt/ /opt/ilmas/.temp

EXPOSE $PORT || EXPOSE 3000

USER nodejs

# set prefix to nodejs directory, no need to use sudo on -g flag
RUN mkdir /home/nodejs/.npm-global &&\
    npm config set prefix '/home/nodejs/.npm-global'

# npm install dependencies
RUN cd /opt/ilmas && \
    npm install -g bower && \
    bower install && \
    npm install

WORKDIR /opt/ilmas
CMD node app.js
